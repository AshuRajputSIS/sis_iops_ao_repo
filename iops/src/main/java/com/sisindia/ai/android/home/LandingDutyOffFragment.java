package com.sisindia.ai.android.home;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LandingDutyOffFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LandingDutyOffFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private DutyChangeListener dutyChangeListener;
    private RelativeLayout landingDutyoffLayout;


    public LandingDutyOffFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static LandingDutyOffFragment newInstance(String param1, String param2) {
        LandingDutyOffFragment fragment = new LandingDutyOffFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        dutyChangeListener = (DutyChangeListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.landing_duty_off, container, false);
        landingDutyoffLayout = (RelativeLayout) view.findViewById(R.id.landing_dutyoff_layout);
        landingDutyoffLayout.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        dutyChangeListener.onDutyChange(true);
    }
}
