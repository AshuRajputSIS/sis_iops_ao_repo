package com.sisindia.ai.android.mybarrack;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.R.color;
import com.sisindia.ai.android.R.id;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackInspectionQuestionOptionMO;
import com.sisindia.ai.android.mybarrack.modelObjects.QuestionsAndOptionsMO;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shankar on 27/7/16.
 */
public class TabSpaceFragment extends BaseFragment implements OnCheckedChangeListener {
    @Bind(id.baseLayout)
    LinearLayout baseLayout;
    @Bind(id.scrollView)
    ScrollView scrollView;
    View view;
    private LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> questionOptionMap = new LinkedHashMap<>();
    public List<QuestionsAndOptionsMO> questionsAndOptionsMOList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tab_space_fragment, container, false);
        ButterKnife.bind(this, view);
        Util.hideKeyboard(getActivity());
        questionOptionMap = IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                getBarrackInspectionQuestionOption(3);

        dynamicRadioGroup(questionOptionMap);


        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_space_fragment;
    }

    private void dynamicRadioGroup(LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> questionOptionMap) {
        if (null != questionOptionMap) {
            LinearLayout linearLayout = new LinearLayout(this.getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT));
            linearLayout.setPadding(10, 10, 10, 20);

            for (Entry<Integer, List<BarrackInspectionQuestionOptionMO>> e : questionOptionMap.entrySet()) {
                Integer key = e.getKey();
                TextView questionTxt = new TextView(this.getActivity());
                questionTxt.setPadding(10, 10, 10, 10);
                questionTxt.setTextSize(18);
                questionTxt.setId(key);
                questionTxt.setTextColor(Color.BLACK);
                questionTxt.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));
                linearLayout.addView(questionTxt);


                // Initialize a new RadioGroup
                RadioGroup rg = new RadioGroup(this.getActivity());
                rg.setOrientation(RadioGroup.HORIZONTAL);
                rg.setPadding(10, 10, 10, 0);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,1f);
                rg.setLayoutParams(lp);
                rg.setOnCheckedChangeListener(this);


                QuestionsAndOptionsMO questionsAndOptionsMO = new QuestionsAndOptionsMO();

                for (int i = 0; i < e.getValue().size(); i++) {
                    questionTxt.setText(e.getValue().get(i).getQuestion());
                    if (e.getValue().get(i).getControlType().equalsIgnoreCase("MultipleChoice")) {

                        // Create a Radio Button for RadioGroup
                        RadioButton option = new RadioButton(this.getActivity());
                        option.setText(e.getValue().get(i).getOptions());
                        option.setPadding(10, 10, 10 ,10);
                        option.setTextColor(getResources().getColor(color.color_dark_grey));
                        option.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT));
                        option.setId(View.generateViewId());
                       // option.la
                        option.setTag(e.getValue().get(i));
                        option.setChecked(false);
                        if (i == 0) {
                            option.setChecked(true);
                            questionsAndOptionsMO.setOptionId(e.getValue().get(i).getOptionId());
                            questionsAndOptionsMO.setQuestionId(e.getValue().get(i).getQuestionId());
                            questionsAndOptionsMO.setRemarks(null);
                            questionsAndOptionsMOList.add(questionsAndOptionsMO);
                        }

                        rg.addView(option);
                        rg.setOrientation(LinearLayout.VERTICAL);
                        rg.setWeightSum(2);

                    } else {

                        EditText editText = new EditText(this.getActivity());
                        editText.setHint("Enter remarks");
                        editText.setPadding(10, 30, 10, 30);
                        editText.setTextColor(getResources().getColor(color.color_dark_grey));
                        editText.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                                LayoutParams.WRAP_CONTENT));
                        editText.setId(View.generateViewId());
                        editText.setBackground(getResources().getDrawable(R.drawable.border_editext));
                        editText.setTag(e.getValue().get(i));
                        editText.setMaxLines(2);
                        editText.setFocusable(false);
                        editText.setFocusableInTouchMode(true);
                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                        questionsAndOptionsMO.setOptionId(null);
                        questionsAndOptionsMO.setQuestionId(e.getValue().get(i).getQuestionId());
                        questionsAndOptionsMO.setRemarks(editText.getText().toString().toString());
                        questionsAndOptionsMOList.add(questionsAndOptionsMO);
                        linearLayout.addView(editText);
                    }
                }

                // rg.getChildAt(0).setSelected(true);
                linearLayout.addView(rg);

                View view = new View(this.getActivity());
                view.setPadding(10, 30, 10, 30);
                view.setBackgroundColor(getResources().getColor(color.color_grey));
                view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        2));
                linearLayout.addView(view);
            }


            baseLayout.addView(linearLayout);
            ((BarrackInspectionTabActivity) getActivity()).checkSpaceTabEntry(linearLayout);

        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup mGroup, int checkedRadioButtonId) {

        mGroup.check(checkedRadioButtonId);
        for (int m = 0; m < mGroup.getChildCount(); m++) {

            if (checkedRadioButtonId == mGroup.getChildAt(m).getId()) {


                BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO = (BarrackInspectionQuestionOptionMO)
                        mGroup.getChildAt(m).getTag();


                if (null != questionsAndOptionsMOList && questionsAndOptionsMOList.size() != 0) {
                    for (int i = 0; i < questionsAndOptionsMOList.size(); i++) {

                        QuestionsAndOptionsMO questionsAndOptionsMO = new QuestionsAndOptionsMO();

                        if (questionsAndOptionsMOList.get(i).getQuestionId() == barrackInspectionQuestionOptionMO.getQuestionId()) {

                            questionsAndOptionsMOList.remove(i);
                            questionsAndOptionsMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                            questionsAndOptionsMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                            questionsAndOptionsMO.setRemarks(null);
                            questionsAndOptionsMOList.add(i, questionsAndOptionsMO);
                        }

                    }
                }

            }
        }
    }
}
