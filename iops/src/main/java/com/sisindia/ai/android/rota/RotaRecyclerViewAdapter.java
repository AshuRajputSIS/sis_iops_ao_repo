package com.sisindia.ai.android.rota;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.moninputandbs.MonInputAndBillSubmissionScreen;
import com.sisindia.ai.android.rota.daycheck.NoTaskMO;
import com.sisindia.ai.android.rota.models.KitDistributionStatusMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.moninputandbs.MonInputAndBSModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 04-03-2016.
 */
public class RotaRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    public static final int TYPE_NO_TASK = 3;
    public static final int TYPE_DATE_HEADER = 4;
    public static final int TYPE_BS_MI_HEADER = 5;
    public static final int TYPE_AKR_COUNT_HEADER = 6;
    ArrayList<Object> rotaModelList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String selectedDate = "";

    public RotaRecyclerViewAdapter(Context context) {
        this.context = context;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setRotaData(ArrayList<Object> rotaModelList) {
        this.rotaModelList = rotaModelList;
    }

    public void setRotaData(ArrayList<Object> rotaModelList, String selectedDate) {
        this.rotaModelList = rotaModelList;
        this.selectedDate = selectedDate;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView;
        switch (viewType) {

            case TYPE_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.header_row, parent, false);
                recycleViewHolder = new HeaderViewHolder(itemView);
                break;
            case TYPE_ROW:
                itemView = LayoutInflater.from(context).inflate(R.layout.recycler_rota_row, parent, false);
                recycleViewHolder = new RotaViewHolder(itemView);
                break;
            case TYPE_NO_TASK:
                itemView = LayoutInflater.from(context).inflate(R.layout.notask_for_day, parent, false);
                recycleViewHolder = new NoTaskViewHolder(itemView);
                break;
            case TYPE_DATE_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.date_rota_row, parent, false);
                recycleViewHolder = new DateViewHolder(itemView);
                break;
            case TYPE_BS_MI_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.header_bs_moninput, parent, false);
                recycleViewHolder = new MIAndBSViewHolder(itemView);
                break;
            case TYPE_AKR_COUNT_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.header_bs_moninput, parent, false);
                recycleViewHolder = new MIAndBSViewHolder(itemView);
                break;

        }
        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.headerTextview.setText((String) rotaModelList.get(position));
                break;
            case TYPE_ROW:
                RotaViewHolder rowDataHolder = (RotaViewHolder) holder;

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskTypeId() == 3) {
                    rowDataHolder.addressTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getBarrackName());
                } else {
                    rowDataHolder.addressTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getUnitName());
                }

                rowDataHolder.checkingTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getTypeChecking());
                String startTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(((RotaTaskModel) rotaModelList.get(position)).getStartTime());
                String endTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(((RotaTaskModel) rotaModelList.get(position)).getEndTime());

                rowDataHolder.timeTextview.setText(startTime + "-" + endTime);
                rowDataHolder.typeIcon.setImageResource(((RotaTaskModel) rotaModelList.get(position)).getIcon());

                if (TextUtils.isEmpty(((RotaTaskModel) rotaModelList.get(position)).getUnitCode()))
                    rowDataHolder.unitCode.setVisibility(View.GONE);
                else
                    rowDataHolder.unitCode.setText(((RotaTaskModel) rotaModelList.get(position)).getUnitCode());

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatusId() == 3)
                    rowDataHolder.nextIcon.setImageResource(R.drawable.greentick);
                else if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatusId() == 4)
                    rowDataHolder.nextIcon.setImageResource(R.drawable.red_tick);
                else
                    rowDataHolder.nextIcon.setImageResource(R.drawable.arrow_right);

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("InActive") ||
                        ((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("In Active")) {
                    rowDataHolder.approvedStatus.setText(context.getResources().getString(R.string.COMPLETED_AND_SYNCED));
                } else if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("InProgress") ||
                        ((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("In Progress")) {
                    rowDataHolder.approvedStatus.setText(context.getResources().getString(R.string.INPROGRESS));
                } else
                    rowDataHolder.approvedStatus.setText(((RotaTaskModel) rotaModelList.get(position)).getTaskStatus());

                break;
            case TYPE_NO_TASK:
                NoTaskViewHolder noTaskViewHolder = (NoTaskViewHolder) holder;
                noTaskViewHolder.noTaskTextview.setText(((NoTaskMO) rotaModelList.get(position)).getMessage());
                break;
            case TYPE_DATE_HEADER:
                DateViewHolder dateViewHolder = (DateViewHolder) holder;
                dateViewHolder.currentdateTv.setText(((DateRotaModel) rotaModelList.get(position)).getCurrentDateStr());
                dateViewHolder.taskStatusTv.setText(((DateRotaModel) rotaModelList.get(position)).getTaskStatus());
                dateViewHolder.rotaDate.setText("RotaDate - " + ((DateRotaModel) rotaModelList.get(position)).getRotaDateStr());
                break;
            case TYPE_BS_MI_HEADER:
                MIAndBSViewHolder miBsHolder = (MIAndBSViewHolder) holder;
                if (((MonInputAndBSModel) rotaModelList.get(position)).getTypeMIorBS() == 1)
                    miBsHolder.monInputAndBSTextView.setText("Mon Input : " + ((MonInputAndBSModel) rotaModelList.get(position)).getPendingCountOfMiOrBS() + "/" +
                            ((MonInputAndBSModel) rotaModelList.get(position)).getTotalCountOfMiOrBS());
                else if (((MonInputAndBSModel) rotaModelList.get(position)).getTypeMIorBS() == 2)
                    miBsHolder.monInputAndBSTextView.setText("Bill Submission : " + ((MonInputAndBSModel) rotaModelList.get(position)).getPendingCountOfMiOrBS() + "/" +
                            ((MonInputAndBSModel) rotaModelList.get(position)).getTotalCountOfMiOrBS());
                break;
            case TYPE_AKR_COUNT_HEADER:
                MIAndBSViewHolder miBsAkrHolder = (MIAndBSViewHolder) holder;
                miBsAkrHolder.mibsakrLayout.setBackgroundColor(context.getResources().getColor(R.color.background_red));

                String akrCount = "Pending Kits_for distribution : " + ((KitDistributionStatusMO) rotaModelList.get(position)).getPendingCountOfKitsForDistribution()
                        + " (out of total " + ((KitDistributionStatusMO) rotaModelList.get(position)).getTotalCountOfKitsForDistribution() + ")";
                miBsAkrHolder.monInputAndBSTextView.setText(akrCount);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (rotaModelList.get(position) instanceof String)
            return TYPE_HEADER;
        else if (rotaModelList.get(position) instanceof RotaTaskModel)
            return TYPE_ROW;
        else if (rotaModelList.get(position) instanceof NoTaskMO)
            return TYPE_NO_TASK;
        else if (rotaModelList.get(position) instanceof DateRotaModel)
            return TYPE_DATE_HEADER;
        else if (rotaModelList.get(position) instanceof MonInputAndBSModel)
            return TYPE_BS_MI_HEADER;
        else if (rotaModelList.get(position) instanceof KitDistributionStatusMO)
            return TYPE_AKR_COUNT_HEADER;
        return -1;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (rotaModelList != null) {
            count = rotaModelList.size() == 0 ? 0 : rotaModelList.size();
        }
        return count;
    }

    public class RotaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView timeTextview, addressTextview, checkingTextview, approvedStatus, unitCode;
        ImageView typeIcon, nextIcon;

        public RotaViewHolder(View view) {
            super(view);
            this.timeTextview = (CustomFontTextview) view.findViewById(R.id.tv_time);
            this.addressTextview = (CustomFontTextview) view.findViewById(R.id.address);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkType);
            this.approvedStatus = (CustomFontTextview) view.findViewById(R.id.approved_status);
            this.typeIcon = (ImageView) view.findViewById(R.id.dayCheck);
            this.nextIcon = (ImageView) view.findViewById(R.id.nextIcon);
            this.unitCode = (TextView) view.findViewById(R.id.unit_code_tv);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTextview;

        public HeaderViewHolder(View view) {
            super(view);
            this.headerTextview = (CustomFontTextview) view.findViewById(R.id.recycle_header);
        }
    }

    public class NoTaskViewHolder extends RecyclerView.ViewHolder {
        TextView noTaskTextview;

        public NoTaskViewHolder(View view) {
            super(view);
            this.noTaskTextview = (CustomFontTextview) view.findViewById(R.id.noTaskTextview);
        }
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {
        TextView currentdateTv, taskStatusTv, rotaDate;

        public DateViewHolder(View view) {
            super(view);
            this.currentdateTv = (TextView) view.findViewById(R.id.currentdate_Tv);
            this.taskStatusTv = (TextView) view.findViewById(R.id.taskStatus_Tv);
            this.rotaDate = (CustomFontTextview) view.findViewById(R.id.rota_date);
        }
    }

    static class ViewHolder {
        @Bind(R.id.recycle_header)
        CustomFontTextview recycleHeader;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public class MIAndBSViewHolder extends RecyclerView.ViewHolder {

        CustomFontTextview monInputAndBSTextView;
        RelativeLayout mibsakrLayout;

        public MIAndBSViewHolder(View view) {
            super(view);
            this.mibsakrLayout = (RelativeLayout) view.findViewById(R.id.mibsakrLayout);
            this.monInputAndBSTextView = (CustomFontTextview) view.findViewById(R.id.monInputAndBSTextView);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (rotaModelList.get(getLayoutPosition()) instanceof MonInputAndBSModel) {
                        context.startActivity(new Intent(context, MonInputAndBillSubmissionScreen.class)
                                .putExtra("CALENDAR_SELECTED_DATE", selectedDate)
                                .putExtra("CALL_TYPE_MI_BS", ((MonInputAndBSModel) rotaModelList.get(getLayoutPosition())).getTypeMIorBS()));
                    } else {
                        AppPreferences appPreferences = new AppPreferences(context);
                        if (appPreferences.getDutyStatus())
                            context.startActivity(new Intent(context, AKRActivity.class));
                        else {
                            try {
                                Toast.makeText(context, "Please turn on duty to go further", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Crashlytics.logException(e);
                            }
                        }
                        appPreferences = null;
                    }
                }
            });
        }
    }
}
