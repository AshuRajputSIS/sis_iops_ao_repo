package com.sisindia.ai.android.issues.complaints;

import com.sisindia.ai.android.issues.IssueCountMO;
import com.sisindia.ai.android.issues.IssuesMO;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 10-11-2016.
 */
public class ComplaintsData {

    private  int openComplaintsCount;
    private ArrayList<IssuesMO> complaintsList;

    public ArrayList<IssuesMO> getComplaintsList() {
        return complaintsList;
    }

    public void setComplaintsList(ArrayList<IssuesMO> complaintsList) {
        this.complaintsList = complaintsList;
    }

    public int getOpenComplaintsCount() {
        return openComplaintsCount;
    }

    public void setOpenComplaintsCount(int openComplaintsCount) {
        this.openComplaintsCount = openComplaintsCount;
    }
}
