package com.sisindia.ai.android.issues.grievances;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.IdCardMO;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.ActionPlanAdapter;
import com.sisindia.ai.android.issues.IssueTypeEnum;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.response.UnitEmployee;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.AudioActivity;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity.BARCODE_CAPTURE_REQUEST_CODE;

//import android.util.Log;

/**
 * Created by Durga Prasad on 27-07-2016.
 */
public class AddIssuesGrievanceActivity extends DayCheckBaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    //    private static final String TAG = "AddIssuesGrievanceActivity";
    private static final int VERBAL_MODE = 3;   //mode of grievance is grievance by default;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.employee_id_number)
    AutoCompleteTextView employeeIdAutoCompleteTextview;
    @Bind(R.id.editv_guard_id)
    CustomFontTextview mGuardName;
    @Bind(R.id.unit_name)
    CustomFontTextview unitName;
    @Bind(R.id.spinner_greievance_category)
    Spinner spinnerGreievanceCategory;
    @Bind(R.id.spinner_grievance)
    Spinner spinnerGrievance;
    @Bind(R.id.guard_parent_layout)
    LinearLayout guardParentLayout;
    @Bind(R.id.add_grievance_description)
    EditText grievanceDescription;

    @Bind(R.id.issue_spinner)
    Spinner spinnerAddActionPlan;

    @Bind(R.id.spinnerUnitName)
    Spinner spinnerUnitName;

    @Bind(R.id.audio_img)
    ImageView audioImg;
    @Bind(R.id.unitNameOrBarrackNameLabel)
    CustomFontTextview unitNameOrBarrackNameLabel;
    @Bind(R.id.target_action_date)
    CustomFontTextview targetActionDate;
    @Bind(R.id.action_calender)
    ImageView actionCalender;
    @Bind(R.id.calenderLayout)
    LinearLayout calenderLayout;
    @Bind(R.id.edittext_target_date)
    CustomFontTextview edittextTargetDate;
    @Bind(R.id.parent_layout)
    LinearLayout parentLayout;
    @Bind(R.id.target_action_date_view)
    View calendarLayout;
    @Bind(R.id.spinner_title)
    CustomFontTextview spinnerTitle;
    @Bind(R.id.grievanceCategory_tv)
    CustomFontTextview grievanceCategorytv;
    @Bind(R.id.grievanceNature_tv)
    CustomFontTextview grievanceNatureTv;
    @Bind(R.id.phone_number)
    CustomFontEditText phoneNumber;

    private LookUpSpinnerAdapter natureOfGrievanceAdapter;
    private IssuesMO issuesMO;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ArrayList<UnitEmployee> arrayListEmployeeIds;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<LookupModel> lookupModelIssueCategoryList, lookupModelGrievanceNatureList;
    private ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
    private ActionPlanAdapter actionPlanAdapter;
    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private String targetdateStr;
    private boolean isCategorySelected;
    private boolean isSubCategorySelected;
    private int unitId;
    private boolean fromMyBarrack;
    private int barrackId;
    private int taskId;
    private boolean fromIssueFragment;
    private int dropDownItemSelected;
    private AttachmentMetaDataModel attachmentMetaDataModel = null;
    //    private Resources resources;
//    private int issueId;
    private UnitEmployee unitEmployeeDetails;
    //    private GuardFineRewardMO mGuardFineRewardMO = new GuardFineRewardMO();
    private boolean fromAPICall;
    private ArrayList<UnitEmployee> receivedUnitDetails = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_grievance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.add_grievance_done) {
            validateFields();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_grievances_activity);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.add_greivance));
        issuesMO = new IssuesMO();
        fromIssueFragment = false;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        if (getIntent() != null) {
            fromMyBarrack = getIntent().getBooleanExtra("FromMyBarrack", false);
            fromIssueFragment = getIntent().getBooleanExtra(TableNameAndColumnStatement.ISSUES_FRAGMENT, false);

            if (fromMyBarrack) {
                employeeIdAutoCompleteTextview.setEnabled(true);
                employeeIdAutoCompleteTextview.setFocusable(true);
                employeeIdAutoCompleteTextview.setSelected(true);
                employeeIdAutoCompleteTextview.setClickable(true);
                employeeIdAutoCompleteTextview.setFocusableInTouchMode(true);

                barrackId = getIntent().getIntExtra("BarrackId", 0);
                unitId = getIntent().getIntExtra(TableNameAndColumnStatement.UNIT_ID, 0);
                taskId = getIntent().getIntExtra("taskId", 0);

                unitName.setVisibility(View.GONE);

            } else {
                if (!fromIssueFragment) {
                    employeeIdAutoCompleteTextview.setEnabled(false);
                    employeeIdAutoCompleteTextview.setFocusable(false);
                    employeeIdAutoCompleteTextview.setSelected(false);
                    employeeIdAutoCompleteTextview.setClickable(false);
                    employeeIdAutoCompleteTextview.setFocusableInTouchMode(false);
                    spinnerUnitName.setVisibility(View.GONE);

                    int guardEmployeeId = getIntent().getIntExtra(TableNameAndColumnStatement.employee_id, 0);
                    String guardEmployeeNo = getIntent().getStringExtra(TableNameAndColumnStatement.EMPLOYEE_NO);
                    String guardName = getIntent().getStringExtra(TableNameAndColumnStatement.guard_name);
                    String unitNames = getIntent().getStringExtra(TableNameAndColumnStatement.unitName);
                    taskId = getIntent().getIntExtra(TableNameAndColumnStatement.TASK_ID, 0);
                    unitId = getIntent().getIntExtra(TableNameAndColumnStatement.UNIT_ID, 0);

                    employeeIdAutoCompleteTextview.setText(guardEmployeeNo);
                    mGuardName.setText(guardName);
                    unitName.setText(unitNames);

                    issuesMO.setIssueId(0);
                    //issuesMO.setEmployeeId(guardEmployeeNo);
                    issuesMO.setIssueStatusId(GrievanceStatusEnum.Reported.getValue());
                    issuesMO.setUnitId(unitId);
                    issuesMO.setGuardName(guardName);
                    issuesMO.setGuardCode(guardEmployeeNo);
                    issuesMO.setSourceTaskId(taskId);
                    issuesMO.setGuardId(guardEmployeeId);
                } else {
                    // from issue module
                    //  callbackCode = Constants.ISSUE_GREIVANCE_CODE;
                    unitName.setVisibility(View.GONE);
                    Util.reSetGrievanceModelHolder();
                }
            }
        }
        setUpViews();
        setUnitSpinnerData();
    }

    private void setUpViews() {
//        resources = getResources();
        LookUpSpinnerAdapter grievanceCategoryAdapter = new LookUpSpinnerAdapter(this);
        natureOfGrievanceAdapter = new LookUpSpinnerAdapter(this);
        issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
        edittextTargetDate.setText(dateTimeFormatConversionUtil.getCurrentDate());

        if (appPreferences.getForcedGrievanceFlag())
            lookupModelIssueCategoryList = IOPSApplication.getInstance().getSelectionStatementDBInstance()
                    .getLookupData(TableNameAndColumnStatement.IssueCategory, "1");
        else
            lookupModelIssueCategoryList = IOPSApplication.getInstance().getSelectionStatementDBInstance()
                    .getLookupDataWithNoGrievance(TableNameAndColumnStatement.IssueCategory, "1");

        grievanceCategoryAdapter.setSpinnerData(lookupModelIssueCategoryList);
        spinnerGreievanceCategory.setAdapter(grievanceCategoryAdapter);
        spinnerGreievanceCategory.setOnItemSelectedListener(this);
        spinnerAddActionPlan.setOnItemSelectedListener(this);
        spinnerGrievance.setOnItemSelectedListener(this);
        spinnerUnitName.setOnItemSelectedListener(this);
//        setDescription();
//        setupPhoneNumber();

        // issuesMO.setBarrackId(barrackId);
        issuesMO.setClosureEndDate("");
        issuesMO.setClosureRemarks("");
        issuesMO.setIssueTypeId(IssueTypeEnum.Grievance.getValue());
        autoCompleteEmployeeInfo();
        if (util != null) {
            grievanceCategorytv.setText(util.createSpannableStringBuilder(getResources().getString(R.string.Grievances_category)));
            grievanceNatureTv.setText(util.createSpannableStringBuilder(getResources().getString(R.string.Grievance)));
            spinnerTitle.setText(util.createSpannableStringBuilder(getResources().getString(R.string.add_action_plan)));
        }
    }

    private void setUnitSpinnerData() {
        if (fromIssueFragment || fromMyBarrack) {
            try {
                String[] unitNameArray;
                receivedUnitDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().getUnitDetails();
                if (receivedUnitDetails != null && receivedUnitDetails.size() > 0) {

                    unitNameArray = new String[receivedUnitDetails.size() + 1];
                    unitNameArray[0] = "Select Unit";

                    for (int i = 0; i < receivedUnitDetails.size(); i++) {
                        unitNameArray[i + 1] = receivedUnitDetails.get(i).getUnitName();
                    }

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, unitNameArray);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerUnitName.setAdapter(spinnerArrayAdapter);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
        }
    }

    private void autoCompleteEmployeeInfo() {
        try {
            String[] receivedGuardCodes = IOPSApplication.getInstance().getSelectionStatementDBInstance().getGuardsCode();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, receivedGuardCodes);
            employeeIdAutoCompleteTextview.setAdapter(adapter);
            employeeIdAutoCompleteTextview.setThreshold(2);

            employeeIdAutoCompleteTextview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    try {
                        String selectedGuardCode = employeeIdAutoCompleteTextview.getText().toString();
                        updateGuardNameUI(selectedGuardCode.trim());
//                        updateEmployeeInfo(selectedGuardCode.trim());
                    } catch (Exception e) {
                    }
                }
            });

            employeeIdAutoCompleteTextview.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    setDefaultValuesToGuardDetails(false);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() == 9) {
                        try {
                            updateGuardNameUI(s.toString().trim());
                            employeeIdAutoCompleteTextview.dismissDropDown();
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }
                    }
                }
            });

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void updateGuardNameUI(String guardCode) {

        List<UnitEmployee> employees = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAllEmployeeDetails(guardCode);

        if (null != employees && employees.size() != 0) {
            unitEmployeeDetails = employees.get(0);

            mGuardName.setText(unitEmployeeDetails.getEmployeeFullName());
            Util.setmGuard_Name(unitEmployeeDetails.getEmployeeFullName());
            Util.mGuard_Id = unitEmployeeDetails.getEmployeeId();

            issuesMO.setIssueStatusId(GrievanceStatusEnum.Reported.getValue());
            issuesMO.setIssueId(0);

            issuesMO.setGuardId(unitEmployeeDetails.getEmployeeId());
            issuesMO.setGuardName(unitEmployeeDetails.getEmployeeFullName());
            issuesMO.setGuardCode(unitEmployeeDetails.getEmployeeNo());
        } else {
            setDefaultValuesToGuardDetails(true);
        }
    }

    private void setDefaultValuesToGuardDetails(boolean isFullGuardCodeTyped) {

        issuesMO.setIssueStatusId(GrievanceStatusEnum.Reported.getValue());
        issuesMO.setIssueId(0);

        Util.mGuard_Id = 0;
        issuesMO.setGuardId(0);

        if (isFullGuardCodeTyped) {
            mGuardName.setText("NA");
            Util.setmGuard_Name("");
            issuesMO.setGuardName("");
            if (employeeIdAutoCompleteTextview != null)
                issuesMO.setGuardCode(employeeIdAutoCompleteTextview.getText().toString());

        } else {
            mGuardName.setText("");
            Util.setmGuard_Name("");
            issuesMO.setGuardName("");
            issuesMO.setGuardCode("");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int spinnerId = parent.getId();

        switch (spinnerId) {
            case R.id.spinnerUnitName:
                if (position > 0) {
                    if (receivedUnitDetails != null && receivedUnitDetails.size() > 0) {

                        unitName.setText(receivedUnitDetails.get(position - 1).getUnitName());
                        int selectedUnitID = receivedUnitDetails.get(position - 1).getUnitId();

//                        Log.e("AddIssue", "UnitName :" + receivedUnitDetails.get(position - 1).getUnitName() + "Id " + receivedUnitDetails.get(position - 1).getUnitId());

                        if (!fromIssueFragment) {
                            if (fromMyBarrack)
                                issuesMO.setBarrackId(barrackId);
                            else {
                                if (selectedUnitID != 0) issuesMO.setUnitId(selectedUnitID);
                            }
                            issuesMO.setSourceTaskId(taskId);
                        } else {
                            if (selectedUnitID != 0) issuesMO.setUnitId(selectedUnitID);
                        }
                    }

                } else {
                    snackBarWithMesg(employeeIdAutoCompleteTextview, "Its mandatory to select unit");
                }
                break;

            case R.id.spinner_greievance_category:
                if (0 != position) {
                    dropDownItemSelected = dropDownItemSelected + 1;
                    if (!TextUtils.isEmpty(employeeIdAutoCompleteTextview.getText().toString().trim())) {
                        lookupModelGrievanceNatureList = IOPSApplication.getInstance().getSelectionStatementDBInstance()
                                .getLookupData(TableNameAndColumnStatement.GrievanceType, lookupModelIssueCategoryList.get(position).getLookupIdentifier().toString());
                        if (lookupModelGrievanceNatureList.size() == 0) {
                            spinnerGrievance.setEnabled(false);
                        } else {
                            spinnerGrievance.setEnabled(true);
                        }

                        natureOfGrievanceAdapter.setSpinnerData(lookupModelGrievanceNatureList);
                        spinnerGrievance.setAdapter(natureOfGrievanceAdapter);
                        spinnerAddActionPlan.setOnItemSelectedListener(this);
                        spinnerGrievance.setOnItemSelectedListener(this);
                        issuesMO.setIssueCategoryId(lookupModelIssueCategoryList.get(position).getLookupIdentifier());
                        isCategorySelected = true;
                        if (isCategorySelected && isSubCategorySelected) {
                            populateImprovementPlanSpinner(issuesMO.getIssueCategoryId(), issuesMO.getIssueNatureId());
                        } else {
                            snackBarWithMesg(guardParentLayout, "Please select sub category");
                        }
                    } else {
                        showValidationMessage();
                    }
                }
                break;
            case R.id.spinner_grievance:
                if (0 != position) {
                    dropDownItemSelected = dropDownItemSelected + 1;
                    if (employeeIdAutoCompleteTextview.getText().length() > 0) {
                        issuesMO.setIssueNatureId(lookupModelGrievanceNatureList.get(position).getLookupIdentifier());
                        isSubCategorySelected = true;
                        if (isCategorySelected && isSubCategorySelected) {
                            populateImprovementPlanSpinner(issuesMO.getIssueCategoryId(), issuesMO.getIssueNatureId());
                        } else {
                            snackBarWithMesg(guardParentLayout, getString(R.string.please_select_category));
                        }
                    } else {
                        showValidationMessage();
                    }
                }
                break;
            case R.id.issue_spinner:
                if (0 != position) {
                    try {
                        if (improvementActionPlanList.get(position).getName().equalsIgnoreCase("No Action Plan")) {
                            issuesMO.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                            issuesMO.setActionPlanId(improvementActionPlanList.get(position).getId());
                            calenderLayout.setVisibility(View.INVISIBLE);
                            edittextTargetDate.setText("NA");
                            issuesMO.setTargetActionEndDate("");
                        } else {
                            actionCalender.setVisibility(View.VISIBLE);
                            issuesMO.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                            issuesMO.setActionPlanId(improvementActionPlanList.get(position).getId());
                            displayActionDate(improvementActionPlanList.get(position).getTurnAroundTime());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*actionCalender.setVisibility(View.VISIBLE);
                    issuesMO.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                    issuesMO.setActionPlanId(improvementActionPlanList.get(position).getId());
                    displayActionDate(improvementActionPlanList.get(position).getTurnAroundTime());*/

                } else {
                    issuesMO.setIssueMatrixId(0);
                    issuesMO.setActionPlanId(0);
                    actionCalender.setVisibility(View.INVISIBLE);
                }
                calendarLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showValidationMessage() {
        if (dropDownItemSelected > 2) {
            snackBarWithMesg(employeeIdAutoCompleteTextview, getResources().getString(R.string.please_fill_mandatory));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        int spinnerId = parent.getId();

        switch (spinnerId) {
            case R.id.spinner_greievance_category:
                isCategorySelected = false;
                break;
            case R.id.spinner_grievance:
                isSubCategorySelected = false;
                break;
        }
    }

    private void validateFields() {

        if (employeeIdAutoCompleteTextview.getText().toString().trim().isEmpty() || employeeIdAutoCompleteTextview.getText().toString().trim().length() < 9)
            snackBarWithMesg(guardParentLayout, "Please enter a valid guard code");
//        if (!Util.isValidPhoneNumber(issuesMO.getPhoneNumber()))
        else if (spinnerUnitName.isShown() && spinnerUnitName.getSelectedItemPosition() == 0)
            snackBarWithMesg(guardParentLayout, "Its mandatory to select unit");
        else if (!Util.isValidPhoneNumber(phoneNumber.getText().toString()))
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.mobile_validation));
        else if (appPreferences.getForcedGrievanceFlag() && null == attachmentMetaDataModel)
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.ISSUE_AUDIO_ERR_MSG));
        else if (issuesMO != null && issuesMO.getIssueCategoryId() == 0)
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.ISSUE_CATEGORY_ERR_MSG));
        else if (issuesMO != null && issuesMO.getIssueNatureId() == 0)
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.GRIEVANCE_NATURE__ERR_MSG));
        else if (issuesMO != null && issuesMO.getActionPlanId() == 0)
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.ACTION_PLAN_ID_ERR_MSG));
        else if (issuesMO != null && issuesMO.getTargetActionEndDate() == null)
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.TARGET_ACTION_DATE_ERR_MSG));
        else {
            if (fromIssueFragment) {
                setGrievanceData();
            } else {
                ArrayList<IssuesMO> grivanceListMo = Util.getmGrievanceModelHolder();
                if (!improvementPlanValidation(grivanceListMo)) {
                    setGrievanceData();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.ACTION_PLAN_VALIDATION_MESG), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean improvementPlanValidation(ArrayList<IssuesMO> grivanceListMo) {
        if (null != grivanceListMo && grivanceListMo.size() != 0) {
            for (int i = 0; i < grivanceListMo.size(); i++) {
                if (grivanceListMo.get(i).getGuardCode().equalsIgnoreCase(issuesMO.getGuardCode())) {
                    if (grivanceListMo.get(i).getIssueCategoryId() == issuesMO.getIssueCategoryId()) {
                        if (grivanceListMo.get(i).getIssueNatureId() == issuesMO.getIssueNatureId()) {
                            if (grivanceListMo.get(i).getActionPlanId() == issuesMO.getActionPlanId()) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private void setGrievanceData() {
        issuesMO.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
        issuesMO.setIssueModeId(VERBAL_MODE);
        issuesMO.setCreatedById(appPreferences.getAppUserId());

        issuesMO.setRemarks(grievanceDescription.getText().toString().trim());
        issuesMO.setPhoneNumber(phoneNumber.getText().toString().trim());

        Util.setmGrievanceModelHolder(issuesMO);
        if (!fromMyBarrack) {
            if (!fromIssueFragment) {
                setMetadataForActivity();
                if (null != attachmentMetaDataModel) {
                    setAttachmentMetaDataModel(attachmentMetaDataModel);
                }
                setResult(RESULT_OK, new Intent().putExtra("IS_GRIEVANCE_ADDED", true));
                this.finish();
            } else {
                IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(Util.getmGrievanceModelHolder());
                setMetaDataForIssue();
                triggerSyncAdapter();
                Util.reSetGrievanceModelHolder();
                Util.mAudioSequenceNumber = 0;
                appPreferences.setIssueType(Constants.NavigationFlags.TYPE_GRIEVANCE);
                // IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_greivance));
                setResult(RESULT_OK, new Intent().putExtra("IS_GRIEVANCE_ADDED", true));
                this.finish();
            }
        } else {
            if (!TextUtils.isEmpty(employeeIdAutoCompleteTextview.getText().toString().trim())
                    && (employeeIdAutoCompleteTextview.getText().toString().trim().length() == 10 ||
                    employeeIdAutoCompleteTextview.getText().toString().trim().length() == 9)) {
                setMetadataForActivity(); // attachment is being inserted at baracklevel.

                Intent intent = new Intent();
                intent.putExtra("IS_GRIEVANCE_ADDED", true);
                intent.putParcelableArrayListExtra("Grievance", Util.getmGrievanceModelHolder());
                setResult(RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(mActivity, R.string.please_enter_proper_guard_no, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }

    @Override
    public void onClick(View v) {
        showCalendar();
    }

    private void populateImprovementPlanSpinner(int issuecategoryId, int issueSubCategoryId) {
        improvementActionPlanList.clear();

        if (!((-1 == issuecategoryId) && (-1 == issueSubCategoryId))) {
            improvementActionPlanList.addAll(issueLevelSelectionStatementDB.
                    getImprovementActionPlan(issuesMO.getUnitId(), 1, issuecategoryId, issueSubCategoryId, fromMyBarrack, fromAPICall));
        }

        if (actionPlanAdapter == null) {
            actionPlanAdapter = new ActionPlanAdapter(this);
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            spinnerAddActionPlan.setAdapter(actionPlanAdapter);
        } else {
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            spinnerAddActionPlan.setAdapter(actionPlanAdapter);
        }

        if (improvementActionPlanList != null && improvementActionPlanList.size() == 1 && isCategorySelected && isSubCategorySelected) {
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.SLECTION_WARNING_MSG));
        }
    }

    private void displayActionDate(int days) {
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        targetdateStr = dateTimeFormatConversionUtil.addDaysToCurrentDate(days);
        issuesMO.setTargetActionEndDate(targetdateStr);
        edittextTargetDate.setText(targetdateStr);
        calenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });
    }

    public void showCalendar() {
        Date mindate = dateTimeFormatConversionUtil.convertStringToDateFormat(dateTimeFormatConversionUtil.getCurrentDate());
        Date maxDate = dateTimeFormatConversionUtil.convertStringToDateFormat(targetdateStr);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        DateRangeCalender selectDateFragment = DateRangeCalender.newInstance(mindate, maxDate);
        selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                String formatDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                edittextTargetDate.setText(formatDate);
                issuesMO.setTargetActionEndDate(formatDate);
            }
        });
    }

    public void doScanBarcode(View view) {
        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intent, BARCODE_CAPTURE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_CAPTURE_REQUEST_CODE && resultCode == RESULT_OK) {
            String barcodeScanValue = data.getStringExtra(BarcodeCaptureActivity.BARCODE_VALUE);
            IdCardMO idCard = new IdCardMO(barcodeScanValue);
            showIdCardInfo(idCard);
            employeeIdAutoCompleteTextview.setText(idCard.getRegNumber());
            employeeIdAutoCompleteTextview.dismissDropDown();
            if (NetworkUtil.isConnected) {
                // getGuardNameByEmployeeNum(barcodeScanValue);
            } else {
                updateGuardNameUI(barcodeScanValue);
            }
        } else if (requestCode == Constants.AUDIO_FILE_CODE && resultCode == RESULT_OK) {
            attachmentMetaDataModel = null;
            attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
            Timber.d("attachmentMetaDataModel Audio %s", attachmentMetaDataModel);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.audio_img)
    public void onAudioCapture() {
        Bundle bundle = new Bundle();
        bundle.putInt(TableNameAndColumnStatement.TASK_ID, taskId);
        bundle.putInt(TableNameAndColumnStatement.UNIT_ID, issuesMO.getUnitId());
        bundle.putInt(TableNameAndColumnStatement.GUARD_ID, Util.mGuard_Id);
        bundle.putBoolean(TableNameAndColumnStatement.ISSUES_FRAGMENT, fromIssueFragment);
        Intent i = new Intent(this, AudioActivity.class);
        i.putExtras(bundle);
        startActivityForResult(i, Constants.AUDIO_FILE_CODE);
    }

    /**
     * Adding the metadata from the issue grievance module
     */
    private void setMetaDataForIssue() {
        if (attachmentMetaDataModel != null) {
            attachmentMetaDataModel.setAttachmentSourceTypeId(Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, this));
            attachmentMetaDataModel.setAttachmentSourceId(IOPSApplication.getInstance().getIssueSelectionDBInstance()
                    .getSequenceId(TableNameAndColumnStatement.ISSUES_TABLE, TableNameAndColumnStatement.ISSUE_ID));
            attachmentMetaDataModel.setAttachmentId(0);
            IOPSApplication.getInstance().getInsertionInstance().insertIntoAttachmentDetails(attachmentMetaDataModel);
        }
    }

    /**
     * Adding the metadata from the BarrackInspection, daycheck or nightcheck
     */
    private void setMetadataForActivity() {
        if (attachmentMetaDataModel != null) {
            attachmentMetaDataModel.setAttachmentSourceTypeId(Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, this));
            attachmentMetaDataModel.setAttachmentSourceId(taskId);
            attachmentMetaDataModel.setAttachmentId(0);
        }
    }

}