package com.sisindia.ai.android.commons;

import android.view.View;

/**
 * Created by Durga Prasad on 20-03-2016.
 */
public interface OnRecyclerViewItemClickListener {

    void onRecyclerViewItemClick(View v, int position);
}