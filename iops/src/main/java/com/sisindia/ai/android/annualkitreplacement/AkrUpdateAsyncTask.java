package com.sisindia.ai.android.annualkitreplacement;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.DeletionStatementDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.KitDistributionItemOR;
import com.sisindia.ai.android.utils.Util;

import java.util.List;

import timber.log.Timber;

/**
 * Created by compass on 6/1/2017.
 */

public class AkrUpdateAsyncTask extends AsyncTask<String, String, String> {

    private final AkrSyncListener akrSyncListener;
    private Context mContext;
    private BaseAkrSync akrSyncResponse;

    public AkrUpdateAsyncTask(Context context, BaseAkrSync akrSyncResponse, AkrSyncListener akrSyncListener) {
        this.mContext = context;
        this.akrSyncListener = akrSyncListener;
        this.akrSyncResponse = akrSyncResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        akrSyncListener.akrSyncListener(true);
    }

    @Override
    protected String doInBackground(String... params) {
        SQLiteDatabase sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
        sqlite.beginTransaction();
        boolean isKitDistributionItemDeleted = kitDistributionTableDeletion(mContext, TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE, sqlite);
        boolean isKitDistributionDeleted = kitDistributionTableDeletion(mContext, TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, sqlite);

        if (isKitDistributionItemDeleted && isKitDistributionDeleted) {
            boolean isInsertSuccess = insertUpdateKitDistributionTables(sqlite);
            if (isInsertSuccess)
                sqlite.setTransactionSuccessful();
            else
                Timber.e("Error While Inserting Issues and improvement tables");

        } else
            Timber.e("Error While Deleting Issues and Improvement plan tables");

        sqlite.endTransaction();
        closeDb(sqlite);
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        hideprogressbar(true);
    }

    private void hideprogressbar(boolean isCompleted) {
        Util.dismissProgressBar(isCompleted);
    }

    private boolean insertUpdateKitDistributionTables(SQLiteDatabase sqlite) {
        boolean isSuccessful = true;
        try {
            for (FCMAkrDistributionData akrDistributionData : akrSyncResponse.fcmAkrDistributionData) {
                insertKitDistribution(akrDistributionData, sqlite);
                insertKitDistributionItemTable(akrDistributionData.getKitDistributionItem(), sqlite);
            }
        } catch (Exception exception) {
            isSuccessful = false;
            Crashlytics.logException(exception);
            Timber.e(exception, " Error inserting AKR Data : ");
        }
        return isSuccessful;
    }

    public void insertKitDistribution(FCMAkrDistributionData kitDistributionOR, SQLiteDatabase sqlite) {
        try {
            if (kitDistributionOR != null) {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionOR.getId());
                values.put(TableNameAndColumnStatement.ISSUE_OFFICER_ID, kitDistributionOR.getIssuingOfficerId());
                values.put(TableNameAndColumnStatement.UNIT_ID, kitDistributionOR.getUnitId());
                values.put(TableNameAndColumnStatement.DISTRIBUTION_STATUS, kitDistributionOR.getDistributionStatus());
                values.put(TableNameAndColumnStatement.RECIPIENT_ID, kitDistributionOR.getRecipientId());
                values.put(TableNameAndColumnStatement.KIT_TYPE_ID, kitDistributionOR.getKitTypeId());
                values.put(TableNameAndColumnStatement.BRANCH_ID, kitDistributionOR.getBranchId());
                values.put(TableNameAndColumnStatement.GUARD_CODE, kitDistributionOR.getGuardNo());
                values.put(TableNameAndColumnStatement.GUARD_NAME, kitDistributionOR.getGuardName());
                values.put(TableNameAndColumnStatement.DISTRIBUTION_DATE, kitDistributionOR.getDistributionDate());
                values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, kitDistributionOR.getCreatedDateTime());
                sqlite.insertOrThrow(TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, null, values);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void insertKitDistributionItemTable(List<KitDistributionItemOR> kitDistributionORList, SQLiteDatabase sqlite) {

        try {
            if (kitDistributionORList != null && kitDistributionORList.size() > 0) {
                for (KitDistributionItemOR kitDistributionItemOR : kitDistributionORList) {

                    if (kitDistributionItemOR != null) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionItemOR.getKitDistributionId());
                        values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID, kitDistributionItemOR.getId());
                        values.put(TableNameAndColumnStatement.IS_ISSUED, kitDistributionItemOR.getIsIssued());
                        values.put(TableNameAndColumnStatement.IS_UNPAID, kitDistributionItemOR.getIsUnPaid());

                        if (kitDistributionItemOR.getNonIssueReasonId() == null)
                            values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID, 0);
                        else
                            values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID, kitDistributionItemOR.getNonIssueReasonId());

                        values.put(TableNameAndColumnStatement.ISSUED_DATE, kitDistributionItemOR.getIssuedDate());
                        values.put(TableNameAndColumnStatement.KIT_ITEM_ID, kitDistributionItemOR.getKitItemId());
                        values.put(TableNameAndColumnStatement.KIT_SIZE_ID, kitDistributionItemOR.getKitApparelSizeId());
                        values.put(TableNameAndColumnStatement.QUANTITY, kitDistributionItemOR.getQuantity());
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                        sqlite.insertOrThrow(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE, null, values);
                    }
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    private static synchronized boolean kitDistributionTableDeletion(Context mContext, String tableName, SQLiteDatabase sqlite) {
        DeletionStatementDB deletionStatementDB = new DeletionStatementDB(mContext);
        boolean isDeletionSuccess = true;
        if (!deletionStatementDB.IsTableDeleted(tableName, sqlite))
            isDeletionSuccess = false;

        return isDeletionSuccess;
    }

    private static void closeDb(SQLiteDatabase sqlite) {
        if (null != sqlite && sqlite.isOpen()) {
            sqlite.close();
        }
    }

}