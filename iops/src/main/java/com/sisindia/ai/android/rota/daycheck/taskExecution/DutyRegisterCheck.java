package com.sisindia.ai.android.rota.daycheck.taskExecution;


import com.google.gson.annotations.SerializedName;

public class DutyRegisterCheck {

@SerializedName("AvailableAtPost")

private String availableAtPost;
@SerializedName("SOPAvailable")

private String uptoDate;

/**
* 
* @return
* The availableAtPost
*/
public String getAvailableAtPost() {
return availableAtPost;
}

/**
* 
* @param availableAtPost
* The AvailableAtPost
*/
public void setAvailableAtPost(String availableAtPost) {
this.availableAtPost = availableAtPost;
}

/**
* 
* @return
* The uptoDate
*/
public String getUptoDate() {
return uptoDate;
}

/**
* 
* @param uptoDate
* The UptoDate
*/
public void setUptoDate(String uptoDate) {
this.uptoDate = uptoDate;
}

}