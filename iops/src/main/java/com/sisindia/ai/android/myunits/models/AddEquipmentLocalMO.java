package com.sisindia.ai.android.myunits.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Saruchi on 10-05-2016.
 */
public class AddEquipmentLocalMO implements Parcelable {
    private String equipmentType;
    private String equipmentCode;
    private int equipmentTypId;
    private int equipmentCount;
    private int isSynced;
    private int isNew;


    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public int getIsSynced() {
        return isSynced;
    }

    public void setIsSynced(int isSynced) {
        this.isSynced = isSynced;
    }

    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        this.equipmentCode = equipmentCode;
    }



    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getEquipmentTypId() {
        return equipmentTypId;
    }

    public void setEquipmentTypId(int equipmentTypId) {
        this.equipmentTypId = equipmentTypId;
    }

    public int getEquipmentCount() {
        return equipmentCount;
    }

    public void setEquipmentCount(int equipmentCount) {
        this.equipmentCount = equipmentCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.equipmentType);
        dest.writeString(this.equipmentCode);
        dest.writeInt(this.equipmentTypId);
        dest.writeInt(this.equipmentCount);
        dest.writeInt(this.isSynced);
        dest.writeInt(this.isNew);
    }

    public AddEquipmentLocalMO() {
    }

    protected AddEquipmentLocalMO(Parcel in) {
        this.equipmentType = in.readString();
        this.equipmentCode = in.readString();
        this.equipmentTypId = in.readInt();
        this.equipmentCount = in.readInt();
        this.isSynced = in.readInt();
        this.isNew = in.readInt();
    }

    public static final Parcelable.Creator<AddEquipmentLocalMO> CREATOR = new Parcelable.Creator<AddEquipmentLocalMO>() {
        @Override
        public AddEquipmentLocalMO createFromParcel(Parcel source) {
            return new AddEquipmentLocalMO(source);
        }

        @Override
        public AddEquipmentLocalMO[] newArray(int size) {
            return new AddEquipmentLocalMO[size];
        }
    };
}