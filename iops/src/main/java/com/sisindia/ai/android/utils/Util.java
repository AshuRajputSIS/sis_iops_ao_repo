package com.sisindia.ai.android.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.AttendanceSyncTriggerReceiver;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.NightDutyOnOffReceiver;
import com.sisindia.ai.android.commons.ScheduleDeleteTriggerReceiver;
import com.sisindia.ai.android.commons.UserBlockingReceiver;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.unitDTO.UnitLevelSelectionQuery;
import com.sisindia.ai.android.database.unitDTO.UnitPostGeoCoordinates;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.grievances.PhotoUpdateListener;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.OnStepperUpdateListener;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.utils.bottomsheet.BottomSheetAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 27-02-2016.
 */
public class Util {
    public static final String REPLACE_KIT = "replace_kit";
    public static final String GUARD = "guard";
    public static final String POST_CHECK = "post_check";
    public static final String ADD_GREIVANCE = "greivance";
    public static final String SITE_CHECK = "site_check";
    public static final String DUTY_REGISTER_CHECK = "duty_register_check";
    public static final String CLIENT_REGISTER_CHECK = "client_register_check";
    public static final String VEHICLE_REGISTER_CHECK = "vehicle_register_check";//21
    public static final String VISITOR_REGISTER_CHECK = "visitor_register_check";//22
    public static final String MATERIAL_REGISTER_CHECK = "material_register_check";//23
    public static final String MAINTENANCE_REGISTER_CHECK = "maintenance_register_check";//24
    public static final int CAPTURE_SIGNATURE = 1;
    public static final String GUARD_DETAIL_FULL_IMAGE = "guard_detail_full_image";
    public static final String GUARD_DETAIL_OVER_TURN_OUT = "guard_detail_over_turn_out";
    public static final String BILL_SUBMISSION = "bill_submission";
    public static final String BILL_COLLECTION = "bill_collection";
    public static final String CLIENT_COORDINATION = "client_coordination";
    public static final String CLIENT_COORDINATION_SELFIE = "ccr_selfie";
    public static final String NIGHT_CHECK_SELFIE = "night_check_selfie";
    public static final String BARRACK_BEDDING = "barrack_bedding";
    public static final String BARRACK_KIT = "barrack_kit";
    public static final String BARRACK_MESS = "barrack_mess";
    public static final String BARRACK_OTHERS = "barrack_others";
    public static final String SECURITY_RISK = "security_risk";
    public static final String OTHER_ACTIVITY = "other_activity";
    public static final String KIT_ITEM_REQUEST = "kit_item_request";
    public static final String KIT_ITEM_SIGNATURE = "kit_item_signature";
    public static final String KIT_DISTRIBUTION_ITEM_IMAGE = "kit_distribution_item_image";
    public static final String SETTINGS_AI_PIC = "settings_ai_pic";
    final static String EDIT_TASK_ACTIVITY = "EditTaskActivity";
    public static final String BARRACK_LANDLORD = "barrack_landlord";
    public static final String BARRACK_CUSTODIAN = "barrck_custodian";
    public static final String UNIT_RAISING_DETAILS = "unit_raising_details";
    public static final int CAPTURE_UNIT_RAISING_CODE = 1001;
    public static final String UNIT_RAISING_CANCELLED = "unit_raising_cancelled";
    public static final String UNIT_RAISING_DEFERRED = "unit_raising_deferred";
    public static final String BARRACK = "barrack";
    public static final int BARRACK_CODE = 101;

    public static int currentTab = 0;
    public static boolean showKitDetails;
    private Locale selectedLocale;
    public static String editTextValue;
    public static String improvementPlan;
    public static boolean showOptionMenu;
    public static boolean showAssignedTo;
    private static String sendPhotoImageTo;
    private static PhotoUpdateListener mPhotoUpdateListener;
    public static final int REQUEST_FOR_ACTIVITY_CODE = 1;
    private static boolean incrementSequenceNumber = false;
    public static int mSequenceNumber = 1;
    public static int mAudioSequenceNumber = 0;
    private static ArrayList<AttachmentMetaDataModel> metaDataModelArrayList;
    private static int referenceTypeId;
    private static RecyclerView mguard_details_recyclerview;
    private static ArrayList<String> mGuardName;
    private static String captureImageScreenTitle;
    private static String mGuard_Name;
    public static int mGuard_Id = 0;
    private static String GUARD_CODE = "";
    public static OnStepperUpdateListener mOnStepperUpdateListener;
    private static ArrayList<SecurityRiskModelMO> mSecurityRiskModelHolder;
    private static ArrayList<String> mCheckedGuardIdHolder = new ArrayList<>();
    private static ArrayList<IssuesMO> mComplaintModelHolder = new ArrayList<>();
    private static ArrayList<IssuesMO> mGrievanceModelHolder = new ArrayList<>();
    private static boolean isFromRecuirmentFragment = false;
    public static ArrayList<GuardDetail> mGuardDetailsHolder = new ArrayList<>();
    private static Bitmap mSignature;
    private static ProgressDialog mPgDialog;
    private static Handler mPgBarHandler = new Handler();
    private static String mpostName;
    private static int mPostData;
    static Intent status = null;
    private BottomSheetBehavior<LinearLayout> bottomSheetBehavior;

    public static ArrayList<IssuesMO> getmGrievanceModelHolder() {
        return mGrievanceModelHolder;
    }

    public static void setmGrievanceModelHolder(IssuesMO mGrievanceModel) {
        mGrievanceModelHolder.add(mGrievanceModel);
    }

    public static ArrayList<GuardDetail> getmGuardDetailsHolder() {
        return mGuardDetailsHolder;
    }

    public static void setmGuardDetailsHolder(GuardDetail mGuardDetails) {
        mGuardDetailsHolder.add(mGuardDetails);
    }

    public static void refreshGuardDetailsHolder() {
        if (mGrievanceModelHolder != null && mGrievanceModelHolder.size() > 0) {
            mGrievanceModelHolder.clear();
        }
    }

    public static boolean isFromRecuirmentFragment() {
        return isFromRecuirmentFragment;
    }

    public static void setIsFromRecuirmentFragment(boolean isFromRecuirmentFragment) {
        Util.isFromRecuirmentFragment = isFromRecuirmentFragment;
    }

    public static void reSetGrievanceModelHolder() {
        if (mGrievanceModelHolder != null) {
            mGrievanceModelHolder.clear();
        }
    }

    public static ArrayList<IssuesMO> getmComplaintModelHolder() {
        return mComplaintModelHolder;
    }

    public static void setmComplaintModelHolder(IssuesMO mComplaintModel) {
        mComplaintModelHolder.add(mComplaintModel);
    }

    public static void reSetComplaintModelHolder() {
        if (mComplaintModelHolder != null) {
            mComplaintModelHolder.clear();
        }
    }

    public static boolean getmCheckedGuardIdHolder(String guardCode) {
        return mCheckedGuardIdHolder.contains(guardCode);
    }

    public static void removeGuardIdHolder(String guardCode) {
        if (mCheckedGuardIdHolder != null && mCheckedGuardIdHolder.contains(guardCode)) {
            mCheckedGuardIdHolder.remove(guardCode);
        }
    }

    public static void setmCheckedGuardIdHolder(String guardCode) {
        if (null != mCheckedGuardIdHolder && mCheckedGuardIdHolder.size() != 0) {
            if (mCheckedGuardIdHolder.contains(guardCode)) {
                for (int i = 0; i < mCheckedGuardIdHolder.size(); i++) {
                    if (mCheckedGuardIdHolder.get(i) == guardCode) {
                        mCheckedGuardIdHolder.remove(i);
                    }
                }
            } else {
                mCheckedGuardIdHolder.add(guardCode);
            }
        } else {
            mCheckedGuardIdHolder.add(guardCode);
        }
    }

    public static void refreshCheckedGuardIdHolder() {
        if (mCheckedGuardIdHolder != null) {
            mCheckedGuardIdHolder.clear();
        }
    }

    public static ArrayList<SecurityRiskModelMO> getmSecurityRiskModelHolder() {
        return mSecurityRiskModelHolder;
    }

    public static void setmSecurityRiskModelHolder(ArrayList<SecurityRiskModelMO> msecurityRiskModelHolder) {
        if (mSecurityRiskModelHolder == null) {
            mSecurityRiskModelHolder = new ArrayList<>();
        }
        mSecurityRiskModelHolder = msecurityRiskModelHolder;
    }

    public static String getMpostName() {
        return mpostName;
    }

    public static void setMpostName(String mpostName) {
        Util.mpostName = mpostName;
    }

    public static byte[] convertImageToByte(Context context, Uri uri) {
        byte[] data = null;
        try {
            ContentResolver cr = context.getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static boolean isautomaticTimeChaged(Context context) {
        String timeSettings = Settings.System.getString(context.getContentResolver(), Settings.Global.AUTO_TIME);
        String timeZone_Settings = Settings.System.getString(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE);
        if (timeSettings.equalsIgnoreCase("0")) {
            return true;
        } else if (timeZone_Settings.equalsIgnoreCase("0") && timeSettings.equalsIgnoreCase("0")) {
            return true;
        }
        return false;
    }

    public static String getDate(long milli, String screen) {
        DateFormat dateFormat = null;
        Date date = new Date(milli);
        if (screen.equals(EDIT_TASK_ACTIVITY)) {
            dateFormat = new SimpleDateFormat("EEE, MMM dd yyyy");
        } else if (screen.equals("rotaFragment")) {
            dateFormat = new SimpleDateFormat("EEEE, MMM dd");
        } else {
            dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        }
        String datein = dateFormat.format(date);

        return datein;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void showProgressBar(final Context context, final int message) {
        if (mPgDialog != null && mPgDialog.isShowing()) {
            return;
        }
        try {
            mPgBarHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mPgDialog != null) {
                            mPgDialog.dismiss();
                        }
                        mPgDialog = new ProgressDialog(context, R.style.StyledDialog);
                        mPgDialog.setIndeterminate(true);
                        mPgDialog.setMessage(context.getString(message));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            mPgDialog.setIndeterminateDrawable(context.getResources().
                                    getDrawable(R.drawable.custom_progress_background, context.getTheme()));
                        }
                        mPgDialog.setCanceledOnTouchOutside(false);
                        mPgDialog.setCancelable(false);
                        mPgDialog.show();
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                        mPgDialog = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        mPgDialog = null;
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * dismissing the ProgressBar
     */
    public static void dismissProgressBar(final boolean isDestoryed) {
        if (null != mPgDialog && mPgDialog.isShowing()) {

            try {
                mPgBarHandler.post(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            mPgDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                            mPgDialog = null;
                        }


                    }
                });
            } catch (IllegalArgumentException illegalArgumentException) {
                Crashlytics.logException(illegalArgumentException);
                mPgDialog = null;
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                mPgDialog = null;
            }
        }
    }

    /**
     * Regex to validate the mobile number
     * mobile number should be of 10 digits length
     *
     * @param mobile
     * @return
     */
    public static boolean isValidPhoneNumber(String mobile) {
        if (!TextUtils.isEmpty(mobile)) {
            String regEx = "^[6789]\\d{9}$";
            return mobile.matches(regEx);
        } else {
            return false;
        }
    }

    public static void hideKeyboard(Activity mcontext) {
        // Check if no view has focus:
        View view = mcontext.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mcontext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * this mehtod is used to return the id for thr source type which
     * is used for reference of attachment media i.e for which type it is capture for as
     * all media attachments ar stored in one database(attachmen_metadata) table rather than its own table.
     * these are ids are  mapped with the server ids while sending the attchment to server
     * Table refrenec Attachment Refrence Type ------->AttachmentSourceType
     * Attachmnt refrence id ----> postid , guard id, etc
     *
     * @param sourcename
     * @param context
     * @return
     */
    public static int getAttachmentSourceType(String sourcename, Context context) {

        int id = 0;
        Resources resources = context.getResources();
        if (TableNameAndColumnStatement.UNIT_POST.equals(sourcename)) {
            id = 1;
        } else if (TableNameAndColumnStatement.GUARD.equals(sourcename)) {
            id = 2;
        } else if (TableNameAndColumnStatement.GRIEVENCE.equals(sourcename)) {
            id = 3;
        } else if (TableNameAndColumnStatement.COMPLAIN.equals(sourcename)) {
            id = 4;
        } else if (TableNameAndColumnStatement.TASK_TABLE.equals(sourcename)) {
            id = 5;
        } else if (TableNameAndColumnStatement.DUTY_REGISTER_CHECK.equals(sourcename)) {
            id = 6;
        } else if (TableNameAndColumnStatement.CLIENT_REGISTER_CHECK.equals(sourcename)) {
            id = 7;
        } else if (TableNameAndColumnStatement.BILL_SUBMISSION.equals(sourcename)) {
            id = 8;
        } else if (TableNameAndColumnStatement.SIGNATURE.equals(sourcename)) {
            id = 9;
        } else if (TableNameAndColumnStatement.SECURITY_RISK.equals(sourcename)) {
            id = 10;
        } else if (TableNameAndColumnStatement.IMPROVEMENT_PLAN.equals(sourcename)) {
            id = 11;
        } else if (TableNameAndColumnStatement.BARRACK_BEDDING.equals(sourcename)) {
            id = 12;
        } else if (TableNameAndColumnStatement.BARRACK_KIT.equals(sourcename)) {
            id = 13;
        } else if (TableNameAndColumnStatement.BARRACK_MESS.equals(sourcename)) {
            id = 14;
        } else if (TableNameAndColumnStatement.BARRACK_OTHERS.equals(sourcename)) {
            id = 15;
        } else if (TableNameAndColumnStatement.client_coordination.equals(sourcename)) {
            id = 16;
        } else if (TableNameAndColumnStatement.KIT_ITEM_REQUEST.equals(sourcename)) {
            id = 18;
        } else if (TableNameAndColumnStatement.KIT_ITEM_SIGNATURE.equals(sourcename)) {
            id = 19;
        } else if (TableNameAndColumnStatement.BILL_COLLECTION.equals(sourcename)) {
            id = 20;
        } else if (TableNameAndColumnStatement.VEHICLE_REGISTER_CHECK.equals(sourcename)) {
            id = 21;
        } else if (TableNameAndColumnStatement.VISITOR_REGISTER_CHECK.equals(sourcename)) {
            id = 22;
        } else if (TableNameAndColumnStatement.MATERIAL_REGISTER_CHECK.equals(sourcename)) {
            id = 23;
        } else if (TableNameAndColumnStatement.MAINTENANCE_REGISTER_CHECK.equals(sourcename)) {
            id = 24;
        } else if (TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_IMAGE.equals(sourcename)) {
            id = 25;
        } else if (TableNameAndColumnStatement.DISTRIBUTION_SIGANTURE.equals(sourcename)) {
            id = 26;
        } else if (TableNameAndColumnStatement.SETTINGS_AI_PIC.equals(sourcename)) {
            id = 27;
        } else if (TableNameAndColumnStatement.BARRACK_CUSTODIAN.equals(sourcename)) {
            id = 28;
        } else if (TableNameAndColumnStatement.BARRACK_LANDLORD.equals(sourcename)) {
            id = 29;
        } else if (TableNameAndColumnStatement.UNIT_RAISING.equals(sourcename)) {
            id = 30;
        } else if (TableNameAndColumnStatement.UNIT_RAISING_DEFERRED.equals(sourcename)) {
            id = 31;
        } else if (TableNameAndColumnStatement.UNIT_RAISING_CANCELLED.equals(sourcename)) {
            id = 32;
        } else if (TableNameAndColumnStatement.BARRACK.equals(sourcename)) {
            id = 33;
        } else if (TableNameAndColumnStatement.selfie_bh.equals(sourcename)) {
            id = 34;
        }

        return id;
    }


    /**
     * returns the id for the type of attachemnt
     * i.e wheteh it is picture, video, Audio
     *
     * @param sourcename
     * @param context
     * @return
     */
    public static int getAttachmentType(String sourcename, Context context) {
        int id = 0;
        Resources resources = context.getResources();
        if (TableNameAndColumnStatement.picture.equals(sourcename))
            id = 1;
        else if (TableNameAndColumnStatement.audio_str.equals(sourcename))
            id = 2;
        else if (TableNameAndColumnStatement.video.equals(sourcename))
            id = 3;

        return id;
    }

    /**
     * returns the id for the type of Equipment list
     *
     * @param sourcename
     * @param context
     * @return
     */
    public static int getEquipmentTypeId(String sourcename, Context context) {

        int id = 0;
        Resources resources = context.getResources();
        if (TableNameAndColumnStatement.equipment_vehical.equals(sourcename))
            id = 1;
        else if (TableNameAndColumnStatement.equipment_motorCycle.equals(sourcename))
            id = 2;
        else if (TableNameAndColumnStatement.equipment_metalDetector.equals(sourcename))
            id = 3;
        else if (TableNameAndColumnStatement.equipment_cycle.equals(sourcename))
            id = 4;
        else if (TableNameAndColumnStatement.equipment_wireless_handheld.equals(sourcename))
            id = 5;
        else if (TableNameAndColumnStatement.equipment_wireless_on_vehical.equals(sourcename))
            id = 6;
        else if (TableNameAndColumnStatement.equipment_wireless_base_station.equals(sourcename))
            id = 7;
        else if (TableNameAndColumnStatement.equipment_cleaning_equipment.equals(sourcename))
            id = 8;
        return id;
    }

    public static long getMillisFromDateAndTime(String dateAndTime) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM dd yyyy HH:mm aa");
        Date date = simpleDateFormat.parse(dateAndTime);
        return date.getTime();
    }

    public static void showToast(Context mcontext, String message) {
        Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static String getSendPhotoImageTo() {
        return sendPhotoImageTo;
    }

    public static void setSendPhotoImageTo(String sendPhotoImageTo) {
        Util.sendPhotoImageTo = sendPhotoImageTo;
    }

    public static void printRetorfitError(String errorTag, RetrofitError error) {
        try {
            if (error != null && error.getResponse() != null) {
                Timber.d(errorTag + " error %s", error.getResponse().getStatus());
                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                Timber.d("failure  error %s", json.toString());
            } else {
                Timber.d("failure  error %s", "Recieving Error as null from server");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean incrementSequenceNumber() {
        return incrementSequenceNumber;
    }

    public static void setIncrementSequenceNumber(boolean incrementSequenceNumber) {
        Util.incrementSequenceNumber = incrementSequenceNumber;
    }

    public static int getmSequenceNumber() {
        return mSequenceNumber;
    }

    public static void setmSequenceNumber(int mSequenceNumber) {
        Util.mSequenceNumber = mSequenceNumber;
    }

    public static void initMetaDataArray(boolean clearData) {
        if (metaDataModelArrayList == null) {
            metaDataModelArrayList = new ArrayList<>();
        }
        if (clearData) {
            metaDataModelArrayList.clear();
        }
    }

    public static void addDataToMetaDataArray(AttachmentMetaDataModel dataModel) {
        metaDataModelArrayList.add(dataModel);
    }

    public static ArrayList<AttachmentMetaDataModel> getMetaDataModelArrayList() {
        return metaDataModelArrayList;
    }

    public static int getReferenceTypeId() {
        return referenceTypeId;
    }

    public static void setReferenceTypeId(int referenceTypeId) {
        Util.referenceTypeId = 0;
        Util.referenceTypeId = referenceTypeId;
    }

    public static String getEquipmentBasedOnId(int sourcename, Context context) {
        Resources resources = context.getResources();
        switch (sourcename) {
            case 1:
                return TableNameAndColumnStatement.equipment_vehical;
            case 2:
                return TableNameAndColumnStatement.equipment_motorCycle;
            case 3:
                return TableNameAndColumnStatement.equipment_metalDetector;
            case 4:
                return TableNameAndColumnStatement.equipment_cycle;
            case 5:
                return TableNameAndColumnStatement.equipment_wireless_handheld;
            case 6:
                return TableNameAndColumnStatement.equipment_wireless_on_vehical;
            case 7:
                return TableNameAndColumnStatement.equipment_wireless_base_station;
            case 8:
                return TableNameAndColumnStatement.equipment_cleaning_equipment;
        }
        return null;
    }

    public static int getmPostData() {
        return mPostData;
    }

    public static void setmPostData(int mPostData) {
        Util.mPostData = mPostData;
    }

    public static RecyclerView getMguard_details_recyclerview() {
        return mguard_details_recyclerview;
    }

    public static void setMguard_details_recyclerview(RecyclerView mguard_details_recyclerview) {
        Util.mguard_details_recyclerview = mguard_details_recyclerview;
    }

    public static ArrayList<String> getmGuardName() {
        if (mGuardName != null) {
            mGuardName = new ArrayList<>();
        }
        return mGuardName;
    }

    public static void setmGuardName(ArrayList<String> mGuardName) {
        Util.mGuardName = mGuardName;
    }

    public static String getCaptureImageScreenTitle() {
        return captureImageScreenTitle;
    }

    public static void setCaptureImageScreenTitle(String captureImageScreenTitle) {
        Util.captureImageScreenTitle = captureImageScreenTitle;
    }

    public static String getmGuard_Name() {
        return mGuard_Name;
    }

    public static void setmGuard_Name(String mGuard_Name) {
        Util.mGuard_Name = mGuard_Name;
    }

    public static OnStepperUpdateListener getmHandlerInstance() {
        return mOnStepperUpdateListener;
    }

    public static void setmHandlerInstance(OnStepperUpdateListener onStepperUpdateListener) {
        Util.mOnStepperUpdateListener = onStepperUpdateListener;
    }

    public static Bitmap getmSignature() {
        return mSignature;
    }

    public static void setmSignature(Bitmap mSignature) {
        Util.mSignature = mSignature;
    }

    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String phone2) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone2)) {
            if (phone2.length() < 6 || phone2.length() > 13) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static void customSnackBar(View view, Context mcontext, String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorNavRed));
        snack.show();
    }

    public static String getCommaSeparatedAmount(int amount) {
        DecimalFormat formatter = new DecimalFormat("#,##,###");
        return formatter.format(amount);
    }

    public static long getDateTimeDifference(String reportedDate) {
        DateTimeFormatConversionUtil dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        Date d1 = null;
        Date d2 = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            try {
                d1 = format.parse(dateTimeFormatConversionUtil.getCurrentDateTime());
                d2 = format.parse(reportedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        long diff = d1.getTime() - d2.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return TimeUnit.MILLISECONDS.toDays(diffDays);
    }

    //private method of your class
    public static int getIndex(String myString, Spinner spinner, ArrayList<LookupModel> issueStatus) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (issueStatus.get(i).getName().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            System.out.println("bitmap=" + image.getWidth() + " x " + image.getHeight());
            return image;
        } else {
            System.out.println("bitmap=" + image.getWidth() + " x " + image.getHeight());
            return image;
        }
    }

    public static String getGuardCode() {
        return GUARD_CODE;
    }

    public static void setGuardCode(String guardCode) {
        GUARD_CODE = guardCode;
    }

    public static boolean isServiceRunning(Context context, Class<?> clazz) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {

            if (clazz.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static boolean isProcessRunning(Context context, Class<?> clazz) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            if (clazz.getName().equals(processInfo.processName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) IOPSApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static int getBatteryLevel(Context context) {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        status = context.registerReceiver(null, intentFilter);
        int level = status.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        return level;
    }

    public static String getSqlSupportedDateFormat(String columnName) {
        return " substr(" + columnName + ",7,4) ||'-'||" +
                " substr(" + columnName + ",4,2) ||'-'||" +
                " substr(" + columnName + ",1,2) ";
    }

    public static String getSqlSupportedDateTimeFormat(String columnName) {
        return " substr(" + columnName + ",7,4) ||'-'||" +
                " substr(" + columnName + ",4,2) ||'-'||" +
                " substr(" + columnName + ",1,2) ||' '||" +
                " substr(" + columnName + ",12,2) ||':'||" +
                " substr(" + columnName + ",15,2) ||':'||" +
                " substr(" + columnName + ",18,2) ";
    }

    private void setTimeAgo(String dateStr, final TextView timeagoTextview) {
        if (!TextUtils.isEmpty(dateStr)) {
            Log.d("Detail Test", dateStr);
            DateFormat format = new SimpleDateFormat("y-M-d h:m:s", Locale.ENGLISH);
            try {
                Date date = format.parse(dateStr);
                Calendar instance = Calendar.getInstance();
                instance.setTime(date);
                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(instance.getTimeInMillis(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                timeagoTextview.setText(timeAgo);
            } catch (ParseException e) {
                e.printStackTrace();
                timeagoTextview.setText(dateStr);
            }
        }
    }

    public String getTime(String date) {
        SimpleDateFormat actualFormatDate = new SimpleDateFormat("dd-mm-yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(actualFormatDate.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int hour = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int am_pm = calendar.get(Calendar.AM_PM);
        return "";
    }

    public String getDate(SimpleDateFormat dateFormat) {
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
        Date temp = currentCalendar.getTime();
        String newFormmattedDate = dateFormat.format(temp);
        String[] split = newFormmattedDate.split(" ");
        newFormmattedDate = split[0].toString();
        return newFormmattedDate;
    }

    public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public PhotoUpdateListener getmPhotoUpdateListener() {
        return mPhotoUpdateListener;
    }

    public static void setmPhotoUpdateListener(PhotoUpdateListener mPhotoUpdateListener) {
        mPhotoUpdateListener = mPhotoUpdateListener;
    }

    public BottomSheetBehavior<LinearLayout> showBottomSheet(Context context, LinearLayout bottomSheetViewgroup) {

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetViewgroup);
        bottomSheetBehavior.setPeekHeight(Util.dpToPx(250));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });
        return bottomSheetBehavior;
    }

    public void setBottomSheetAdapter(Context context, ArrayList<String> recylerData, RecyclerView recyclerView, BottomSheetAdapter bottomSheetAdapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        bottomSheetAdapter.setBottomSheetData(recylerData);
        recyclerView.setAdapter(bottomSheetAdapter);
    }

    public String getDateTime(SimpleDateFormat dateFormat) {
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());
        Date temp = currentCalendar.getTime();
        String newFormmattedDate = dateFormat.format(temp);

        return newFormmattedDate;
    }

    public boolean isImagePathExist(String imagePath) {

        File file = new File(imagePath);
        return file.exists();
    }

    public SpannableStringBuilder createSpannableStringBuilder(String labelName) {
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(labelName);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return builder;

    }

    public void changeImageNames(Context context, String tempFileName, String fileName,
                                 int attachmentTypeId) {
        File mediaStorageDir = null;
        if (attachmentTypeId == 3) {
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), context.getString(R.string.app_name));
        } else if (attachmentTypeId == 1) {
            mediaStorageDir = new File(Constants.AUDIO_FILE_PATH);
        }


        if (mediaStorageDir != null && mediaStorageDir.isDirectory()) {
            File[] files = mediaStorageDir.listFiles();
            if (files != null && files.length != 0) {
                for (File file : files) {
                    if (file.getName().equalsIgnoreCase(tempFileName)) {
                        file.renameTo(new File(fileName));
                    }
                }
            }
        }
    }


    public boolean isNearToUnitPost(int unit_id) {
        List<UnitPostGeoCoordinates> unitPostGeoCoordinatesList = null;
        UnitLevelSelectionQuery unitLevelSelectionQuery = IOPSApplication.getInstance()
                .getUnitLevelSelectionQueryInstance();
        float[] distanceResults = new float[3];
        if (unitLevelSelectionQuery != null) {
            unitPostGeoCoordinatesList = unitLevelSelectionQuery
                    .getPostGeoCoordinates(unit_id);
        }
        if (unitPostGeoCoordinatesList != null && unitPostGeoCoordinatesList.size() > 0) {
            for (UnitPostGeoCoordinates unitPostGeoCoordinate : unitPostGeoCoordinatesList) {
                Location.distanceBetween(unitPostGeoCoordinate.latitude,
                        unitPostGeoCoordinate.longitude,
                        GpsTrackingService.latitude, GpsTrackingService.longitude, distanceResults);
                if (distanceResults[0] < 300.0f) {
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public static void setAlarmManager(Context context, String operationType) {
        if (Constants.DELETE_SCHEDULE.equalsIgnoreCase(operationType)) {
            long currentMillis = 0l;
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            currentMillis = calendar.getTimeInMillis();

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, ScheduleDeleteTriggerReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.ALARM_SERVICE_ID, intent, 0);
            alarmManager.cancel(pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, currentMillis, AlarmManager.INTERVAL_DAY, pendingIntent);

        } else if (Constants.ATTENDANCE_SCHEDULE.equalsIgnoreCase(operationType)) {
            long currentMillis = Calendar.getInstance().getTimeInMillis();
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, AttendanceSyncTriggerReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.ALARM_SERVICE_ID_FOR_ATTENDANCE, intent, 0);
            alarmManager.cancel(pendingIntent);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, currentMillis, AlarmManager.INTERVAL_HOUR, pendingIntent);
        } else if (Constants.SYNC_TRIGGER_TIME_AFTER_BLOCKING_AI.equalsIgnoreCase(operationType)) {
            long currentMillis = Calendar.getInstance().getTimeInMillis();
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, UserBlockingReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.ALARM_SERVICE_ID_FOR_SYNC_TRIGGER_TIME, intent, 0);
            alarmManager.cancel(pendingIntent);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, currentMillis + AlarmManager.INTERVAL_HALF_HOUR, pendingIntent);
        } else if (Constants.NIGHT_DUTY_ON_OFF.equalsIgnoreCase(operationType)) {

//            Date dat = new Date();//initializes to now
//            Calendar cal_now = Calendar.getInstance();
//            cal_now.setTime(dat);
//            alarmTimeCalendar.setTime(dat);

            Calendar alarmTimeCalendar = Calendar.getInstance();
            alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, 23);
            alarmTimeCalendar.set(Calendar.MINUTE, 59);
            alarmTimeCalendar.set(Calendar.SECOND, 57);

            /*alarmTimeCalendar.set(Calendar.HOUR_OF_DAY, 17);
            alarmTimeCalendar.set(Calendar.MINUTE, 27);
            alarmTimeCalendar.set(Calendar.SECOND, 57);*/

            /*if (alarmTimeCalendar.before(cal_now)) {
                //if its in the past increment
                alarmTimeCalendar.add(Calendar.DATE, 1);
            }*/

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, NightDutyOnOffReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.NIGHT_DUTY_ON_OFF_VARIABLE, intent, 0);
            alarmManager.cancel(pendingIntent);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTimeCalendar.getTimeInMillis(), pendingIntent);
        }

    }

    public static void scheduleJob(Context context, String operationType) {
        ComponentName component = new ComponentName(context, SyncStartUpServiceAfterUserBlock.class);
        JobScheduler jobScheduler = (JobScheduler) context
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);
        int result = jobScheduler.schedule(getJobBuilder(component));
        if (result == JobScheduler.RESULT_SUCCESS) {
            Timber.d(" Schedule Job :" + result);
        }
    }

    private static JobInfo getJobBuilder(ComponentName componentName) {
        final JobInfo jobInfo;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            jobInfo = new JobInfo.Builder(Constants.ALARM_SERVICE_ID_FOR_SYNC_TRIGGER_TIME,
                    componentName)
                    .setMinimumLatency(TimeUnit.HOURS.toMillis(1))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();
        } else {
            jobInfo = new JobInfo.Builder(Constants.ALARM_SERVICE_ID_FOR_SYNC_TRIGGER_TIME,
                    componentName)
                    .setPeriodic(TimeUnit.HOURS.toMillis(1))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();
        }

        return jobInfo;
    }

}