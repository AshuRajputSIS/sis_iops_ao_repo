package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.FileUploadMO;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.FileUploadResponse;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */

public class ImageSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private int taskId;
    private int issueId;
    private int kitDistributionId;
    private int unitPostId;
    ArrayList<FileUploadMO> fileUploadMOArrayList;
    private int lookUpType;
    private int unitRaisingDetailId;
    private int unitRaisingCancellationId;
    private int unitRaisingDeferredId;
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private int barrackId;

    public ImageSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        fileUploadMOArrayList = new ArrayList<>();
        appPreferences = new AppPreferences(mcontext);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
        uploadImagesToServer();
    }

    public void setTaskIdWithIssues(int taskId, int lookUpType) {
        this.taskId = taskId;
        this.lookUpType = lookUpType;
        uploadImagesToServer();
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
        uploadImagesToServer();
    }

    public void setUnitPostId(int unitPostId) {
        this.unitPostId = unitPostId;
        uploadImagesToServer();
    }

    public void setKitDistributionId(int kitDistributionId) {
        this.kitDistributionId = kitDistributionId;
        uploadImagesToServer();
    }


    public void setUnitRaisingId(int unitRaisingDetailId) {
        this.unitRaisingDetailId = unitRaisingDetailId;
        uploadImagesToServer();
    }

    public void setUnitRaisingCancellationId(int unitRaisingCancellationId) {
        this.unitRaisingCancellationId = unitRaisingCancellationId;
        uploadImagesToServer();
    }

    public void setUnitRaisingDeferredId(int unitRaisingDeferredId) {
        this.unitRaisingDeferredId = unitRaisingDeferredId;
        uploadImagesToServer();
    }


    public void updateImageSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =
                dependentSyncDb.getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                dependentSyncsStatus.isImagesSynced = count;
                if (taskId != 0) {
                    if (taskId == dependentSyncsStatus.taskId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.TASK_RELATED_SYNC,
                                taskId);
                    }
                }
                if (issueId != 0) {
                    if (issueId == dependentSyncsStatus.issueId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.ISSUE_RELATED_SYNC,
                                issueId);
                    }
                }
                if (unitPostId != 0) {
                    if (unitPostId == dependentSyncsStatus.unitPostId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.UNIT_POST_RELATED_SYNC,
                                unitPostId);
                    }
                }
                if (kitDistributionId != 0) {
                    if (kitDistributionId == dependentSyncsStatus.kitDistributionId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.KIT_DISTRIBUTION_RELATED_SYNC,
                                kitDistributionId);
                    }
                }
                if (unitRaisingDetailId != 0) {
                    if (unitRaisingDetailId == dependentSyncsStatus.unitRaisingId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_SYNC,
                                unitRaisingDetailId);
                    }
                }
                if (unitRaisingCancellationId != 0) {
                    if (unitRaisingCancellationId == dependentSyncsStatus.unitRaisingCancellationId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_CANCELLATION_SYNC,
                                unitRaisingCancellationId);
                    }
                }
                if (unitRaisingDeferredId != 0) {
                    if (unitRaisingDeferredId == dependentSyncsStatus.unitRaisingDeferredId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_DEFERRED_SYNC,
                                unitRaisingDeferredId);
                    }
                }
                if (barrackId != 0) {
                    if (barrackId == dependentSyncsStatus.barrackId) {
                        updateImageSyncCount(dependentSyncsStatus, Constants.BARRACK_POST_SYNC,
                                barrackId);
                    }
                }

            }
        }
    }

    private void updateImageSyncCount(DependentSyncsStatus dependentSyncsStatus, int syncTYpe, int id) {
        dependentSyncDb.updateDependentSyncDetails(id, syncTYpe,
                TableNameAndColumnStatement.IS_IMAGES_SYNCED,
                dependentSyncsStatus.isImagesSynced, dependentSyncsStatus.taskTypeId);
    }


    private synchronized void uploadImagesToServer() {
        fileUploadMOArrayList = getAttachmentFilesToUpload();
        Timber.d("upload map  %s" + taskId, fileUploadMOArrayList.size());
        if (fileUploadMOArrayList != null && fileUploadMOArrayList.size() != 0) {
            final Set<String> synchronizedAttachmentIds = Collections
                    .synchronizedSet(appPreferences.getAttachmentRequestIds());
            if (synchronizedAttachmentIds != null) {
                Timber.i("Api ImageSync : ---------startTime@" + dateTimeFormatConversionUtil.getCurrentDateTime());
                updateImageSyncCount(synchronizedAttachmentIds.size());
                for (final String attachmentId : synchronizedAttachmentIds) {
                    for (final FileUploadMO fileUploadMO : fileUploadMOArrayList) {
                        if (String.valueOf(fileUploadMO.getAttachmentID()).equals(attachmentId)) {
                            Map<String, TypedFile> files = new HashMap<String, TypedFile>();
                            final File file = new File(fileUploadMO.getFilePath().trim());
                            final TypedFile typedFile = new TypedFile("multipart/form-data", file);
                            files.put("image" + String.valueOf(fileUploadMO.getAttachmentID()), typedFile);
                            Timber.d(Constants.TAG_SYNCADAPTER + "files object  %s", files);
                            //Timber.i("Api ImageSync : ---------file@"+ fileUploadMO.getServerFilePath());
                            sisClient.getApi().fileUploads(files, fileUploadMO.getAttachmentID(),
                                    new Callback<FileUploadResponse>() {
                                        @Override
                                        public void success(FileUploadResponse imageStatusResponse, Response response) {
                                            Timber.d(Constants.TAG_SYNCADAPTER + "FileUploadResponse %s", imageStatusResponse);
                                            switch (imageStatusResponse.getStatusCode()) {
                                                case HttpURLConnection.HTTP_OK:
                                                    if (updateIsFileUploadMetadataTable(fileUploadMO.getAttachmentID())) {
                                                        if (file.exists() && fileUploadMO.getIsSave() == 0) {
                                                            file.delete();
                                                            synchronizedAttachmentIds.remove(attachmentId);
                                                            if (synchronizedAttachmentIds.size() == 0) {
                                                                Timber.i("Api ImageSync : ---------endTime@" + dateTimeFormatConversionUtil.getCurrentDateTime());
                                                            }
                                                            appPreferences.setAttachmentRequestIds(synchronizedAttachmentIds);
                                                        }
                                                    }
                                            }
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            Util.printRetorfitError("FileUploadResponse Error", error);
                                            if (error.getCause() instanceof FileNotFoundException) {
                                                if (fileUploadMO != null) {
                                                    Timber.i("FileUploadResponse Error %s", fileUploadMO.getAttachmentID());
                                                    updateIsFileUploadMetadataTable(fileUploadMO.getAttachmentID());
                                                }
                                            }
                                        }
                                    });
                        }
                    }

                }
            }
        } else {

            if (taskId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(taskId, TableNameAndColumnStatement.TASK_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (issueId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(issueId, TableNameAndColumnStatement.ISSUE_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (kitDistributionId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(kitDistributionId, TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (unitPostId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(unitPostId, TableNameAndColumnStatement.UNIT_POST_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (unitRaisingDetailId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(unitRaisingDetailId, TableNameAndColumnStatement.UNIT_RAISING_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (unitRaisingCancellationId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(unitRaisingCancellationId, TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID,
                        TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (unitRaisingDeferredId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(unitRaisingDeferredId, TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID,
                        TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            } else if (barrackId != 0) {
                if (dependentSyncDb.isSpecificColumnSynced(barrackId, TableNameAndColumnStatement.BARRACK_ID, TableNameAndColumnStatement.IS_METADATA_SYNCED)) {
                    updateImageSyncCount(0);
                }
            }
        }
    }


    private ArrayList<FileUploadMO> getAttachmentFilesToUpload() {

        SQLiteDatabase sqlite = null;
        String query = "";
        String attachmentReferenceId = "";
        if (issueId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + issueId;
        } else if (unitPostId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitPostId;
        } else if (kitDistributionId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + kitDistributionId;
        } else if (unitRaisingDeferredId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingDeferredId;
        } else if (unitRaisingDetailId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingDetailId;
        } else if (unitRaisingCancellationId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingCancellationId;
        } else if (barrackId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + barrackId;
        }

        if (taskId == 0 && !TextUtils.isEmpty(attachmentReferenceId)) {
            query = " select " + TableNameAndColumnStatement.ATTACHMENT_ID + ", " + TableNameAndColumnStatement.IS_SAVE
                    + ", " + TableNameAndColumnStatement.FILE_PATH + " from " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                    " where " + TableNameAndColumnStatement.IS_NEW + " = " + 0 + " and " +
                    TableNameAndColumnStatement.IS_SYNCED + " = " + 1 + " and " +
                    attachmentReferenceId + " and " +
                    TableNameAndColumnStatement.IS_FILE_Upload + " = " + 0;
        } else {
            if (lookUpType == Constants.LOOKUP_TYPE_AUDIO) {
                query = " select " + TableNameAndColumnStatement.ATTACHMENT_ID
                        + ", " + TableNameAndColumnStatement.IS_SAVE
                        + ", " + TableNameAndColumnStatement.FILE_PATH
                        + " from " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                        " where " + TableNameAndColumnStatement.IS_NEW + " = " + 0 + " and " +
                        TableNameAndColumnStatement.IS_SYNCED + " = " + 1 + " and " +
                        TableNameAndColumnStatement.TASK_ID + " = " + taskId + " and " +
                        TableNameAndColumnStatement.ATTACHMENT_TYPE_ID + " = " + lookUpType + " and " +
                        TableNameAndColumnStatement.IS_FILE_Upload + " = " + 0;
            } else {
                query = " select " + TableNameAndColumnStatement.ATTACHMENT_ID
                        + ", " + TableNameAndColumnStatement.IS_SAVE
                        + ", " + TableNameAndColumnStatement.FILE_PATH
                        + " from " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                        " where " + TableNameAndColumnStatement.IS_NEW + " = " + 0 + " and " +
                        TableNameAndColumnStatement.IS_SYNCED + " = " + 1 + " and " +
                        TableNameAndColumnStatement.TASK_ID + " = " + taskId + " and " +
                        TableNameAndColumnStatement.IS_FILE_Upload + " = " + 0;
            }
        }

        Set<String> attachmentIds = appPreferences.getAttachmentRequestIds();

        Timber.d("InputFileUpload selectQuery  %s", query);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        FileUploadMO fileUploadMO = new FileUploadMO();
                        fileUploadMO.setAttachmentID(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_ID)));
                        fileUploadMO.setFilePath(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_PATH)));
                        fileUploadMO.setIsSave(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_SAVE)));
                        fileUploadMOArrayList.add(fileUploadMO);
                        attachmentIds.add(String.valueOf(fileUploadMO.getAttachmentID()));
                    } while (cursor.moveToNext());
                    appPreferences.setAttachmentRequestIds(attachmentIds);
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (sqlite != null) {
                sqlite.close();
                Timber.d(Constants.TAG_SYNCADAPTER + "InputFileUpload attachment_id  %s", fileUploadMOArrayList);
            }
        }

        return fileUploadMOArrayList;
    }

    public boolean updateIsFileUploadMetadataTable(int attach_id) {
        SQLiteDatabase sqlite = null;
        boolean isUpdateSuccessfully = false;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " SET " + TableNameAndColumnStatement.IS_FILE_Upload + " = " + 1 +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_ID + " = " + attach_id + " and " +
                TableNameAndColumnStatement.IS_NEW + " = " + 0 + " and " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 1;
        Timber.d(Constants.TAG_SYNCADAPTER + "updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            sqlite.execSQL(updateQuery);
            isUpdateSuccessfully = true;
            updateStatus();
        } catch (Exception exception) {
            isUpdateSuccessfully = false;
            exception.getCause();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isUpdateSuccessfully;
    }

    private void updateStatus() {
        if (taskId != 0) {
            updateSyncCountAndStatus(taskId, TableNameAndColumnStatement.TASK_ID);

        } else if (issueId != 0) {
            updateSyncCountAndStatus(issueId, TableNameAndColumnStatement.ISSUE_ID);
        } else if (kitDistributionId != 0) {
            updateSyncCountAndStatus(kitDistributionId, TableNameAndColumnStatement.KIT_DISTRIBUTION_ID);
        } else if (unitPostId != 0) {
            updateSyncCountAndStatus(unitPostId, TableNameAndColumnStatement.UNIT_POST_ID);
        } else if (unitRaisingDetailId != 0) {
            updateSyncCountAndStatus(unitRaisingDetailId, TableNameAndColumnStatement.UNIT_RAISING_ID);
        } else if (unitRaisingDeferredId != 0) {
            updateSyncCountAndStatus(unitRaisingDeferredId, TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID);
        } else if (unitRaisingCancellationId != 0) {
            updateSyncCountAndStatus(unitRaisingCancellationId, TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID);
        } else if (barrackId != 0) {
            updateSyncCountAndStatus(unitRaisingCancellationId, TableNameAndColumnStatement.BARRACK_ID);
        }
    }


    private void updateSyncCountAndStatus(int id, String columName) {
        int count = dependentSyncDb.getSyncCount(id, columName,
                TableNameAndColumnStatement.IS_IMAGES_SYNCED);

        if (count != 0) {
            count = count - 1;
        }
        Timber.i("NewSync %s", " Image Sync count update in -----" + taskId + "--------" + count);
        updateImageSyncCount(count);
    }

    public void setBarrackId(int barrackId) {
        this.barrackId = barrackId;
        uploadImagesToServer();
    }


}


/*     sisClient.getApi().fileUploads(files, fileUploadMO.getAttachmentID(),
             new Callback<FileUploadResponse>() {
@Override
public void success(FileUploadResponse imageStatusResponse, Response response) {
        Timber.d(Constants.TAG_SYNCADAPTER + "FileUploadResponse %s", imageStatusResponse);
        switch (imageStatusResponse.getStatusCode()) {
        case HttpURLConnection.HTTP_OK:
        if(updateIsFileUploadMetadataTable(fileUploadMO.getAttachmentID())) {
        if (file.exists() && fileUploadMO.getIsSave() == 0) {
        file.delete();
        attachmentIds.remove(attachmentId);
        appPreferences.setAttachmentRequestIds(attachmentIds);
        }
        }
        }

        }

@Override
public void failure(RetrofitError error) {
        Util.printRetorfitError("FileUploadResponse Error", error);
        if(error.getCause() instanceof FileNotFoundException){
        if(fileUploadMO != null) {
        Timber.i("FileUploadResponse Error %s", fileUploadMO.getAttachmentID());
        updateIsFileUploadMetadataTable(fileUploadMO.getAttachmentID());
        }
        }
        }
        });*/
