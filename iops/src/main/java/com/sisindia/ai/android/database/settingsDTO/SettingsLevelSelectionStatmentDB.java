package com.sisindia.ai.android.database.settingsDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionUnitAtRiskMO;

import timber.log.Timber;

/**
 * Created by Shushrut on 16-09-2016.
 */
public class SettingsLevelSelectionStatmentDB extends SISAITrackingDB {

    public SettingsLevelSelectionStatmentDB(Context context) {
        super(context);
    }

    public AppUserData getAIPhotoWithAddress(int UnitId) {
        // Select All Query
        String selectQuery = "SELECT "+ TableNameAndColumnStatement.AI_PHOTO +","+ TableNameAndColumnStatement.FULL_ADDRESS
                +","+ TableNameAndColumnStatement.ALTERNATE_CONTACT_NO +" FROM " + TableNameAndColumnStatement.APP_USER_TABLE
                + " where " + TableNameAndColumnStatement.EMPLOYEE_ID + "='" + UnitId + "'";
        Timber.d("getAIPhotoWithAddress selectQuery %s" ,selectQuery);
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        AppUserData mAppUserData = new AppUserData();

        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        mAppUserData.setmUserAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FULL_ADDRESS)));
                        mAppUserData.setmUserAlternateNumber(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ALTERNATE_CONTACT_NO)));
                        mAppUserData.setmImageUrl(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.AI_PHOTO)));
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return mAppUserData;
    }
}
