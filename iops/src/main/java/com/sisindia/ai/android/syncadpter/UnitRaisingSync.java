package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.GeoLocation;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;



public class UnitRaisingSync extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, UnitRaisingOR> unitRaisingList;
    private AppPreferences appPreferences;

    public UnitRaisingSync(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mcontext);
        unitRaisingSync();
    }

    private  void unitRaisingSync() {
        unitRaisingList = getUnitRaisingDetails();
        if (unitRaisingList != null && unitRaisingList.size() != 0) {
            for (Integer key : unitRaisingList.keySet()) {
                UnitRaisingOR unitRaisingOR = unitRaisingList.get(key);
                unitRaisingApiCall(unitRaisingOR, key);
                Timber.d(" UnitRaisingSync  %s", "key: " + key + " value: " + unitRaisingList.get(key));
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+" UnitRaisingSync --> nothing to sync ");
            dependentFailureSync();
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                Timber.d(Constants.TAG_SYNCADAPTER+" UnitRaisingSync # dependentSyncs --> "+dependentSyncsStatus.unitRaisingId );
                if(dependentSyncsStatus != null && dependentSyncsStatus.unitRaisingId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {

        if(dependentSyncsStatus.isUnitRaisingStrengthSynced != Constants.DEFAULT_SYNC_COUNT) {
            if(dependentSyncsStatus.unitRaisingId != 0) {
                new UnitRaisingStrengthSync(mContext,
                        dependentSyncsStatus.unitRaisingId);
            }
        }


        if(dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            if(dependentSyncsStatus.unitRaisingId != 0) {
                metaDataSyncing.setUnitRaisingId(dependentSyncsStatus.unitRaisingId);
            }
        }
        else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                if(dependentSyncsStatus.unitRaisingId != 0) {
                    imageSyncing.setUnitRaisingId(dependentSyncsStatus.unitRaisingId);
                }
            }
        }
    }

    private HashMap<Integer, UnitRaisingOR> getUnitRaisingDetails() {
        HashMap<Integer, UnitRaisingOR> unitRaisingList = new HashMap<>();
        SQLiteDatabase sqlite = null;

        String selectQuery = "SELECT  ur.id as id,u.unit_id,u.unit_name,u.unit_code as unit_code," +
                " u.geo_latitude as geo_latitude, u.geo_longitude as geo_longitude,ur.raising_details," +
                "ur.remarks as remarks,ur.unit_raising_location as unit_raising_location " +
                ",ur.CreatedDateTime as CreatedDateTime" +
                " FROM " + TableNameAndColumnStatement.UNIT_RAISING +
                " ur inner join  "+TableNameAndColumnStatement.UNIT_TABLE +" u  on u.unit_id = ur.unit_id"+
                " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0;
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingDetailsSync selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitRaisingOR unitRaisingOR = new
                                UnitRaisingOR();
                        unitRaisingOR.unitId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        unitRaisingOR.unitCode =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE));
                        unitRaisingOR.raisingDetail =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.RAISING_DETAILS));
                        unitRaisingOR.unitGeoLocation = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE))
                                + "," +cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE));
                        String unitRaisingLocation= cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RAISING_LOCATION));
                        unitRaisingOR.unitGeoLocation = unitRaisingLocation;//This one newly added unit so both unit raising location and unit location is same
                        String[] latLon = unitRaisingLocation.split(",");
                        GeoLocation geoLocation = new GeoLocation();
                        if(latLon != null && latLon.length != 0){
                            geoLocation.setLattitude(Float.valueOf(latLon[0]));
                            geoLocation.setLongitude(Float.valueOf(latLon[1]));
                        }
                        unitRaisingOR.unitRaisedGeoLocation = geoLocation;
                        unitRaisingOR.remarks =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS));
                        unitRaisingOR.raisedDateTime =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME));
                        unitRaisingOR.raisedBy  =  appPreferences.getAppUserId();
                        unitRaisingList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)),
                                unitRaisingOR);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"addPostList object  %s",
                    IOPSApplication.getGsonInstance().toJson(unitRaisingList));

        }
        return unitRaisingList;
    }



    private void updateUnitRaisingDetails(int unitRaisingId, int localId) {
        SQLiteDatabase sqlite = null;
        String updateQuery = " UPDATE " + TableNameAndColumnStatement.UNIT_RAISING +
                " SET " + TableNameAndColumnStatement.UNIT_RAISING_ID + "= " + unitRaisingId +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= " + localId;
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdateUnitRaisingDetails updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }




    private void updateUnitStrengthTable(UnitRaisingResponse unitRaisingResponse, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = " UPDATE " + TableNameAndColumnStatement.UNIT_RAISING_STRENGTH_DETAIL +
                " SET " + TableNameAndColumnStatement.UNIT_RAISING_ID + "= " + unitRaisingResponse
                .data.unitRaisingDetailId +
                " WHERE " + TableNameAndColumnStatement.UNIT_RAISING_ID + " = " + id;
        Timber.d(Constants.TAG_SYNCADAPTER+"updateUnitStrengthTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }

    private void unitRaisingApiCall(final UnitRaisingOR  unitRaisingOR, final int key) {
        sisClient.getApi().saveUnitRaising(unitRaisingOR, new Callback<UnitRaisingResponse>() {
            @Override
            public void success(UnitRaisingResponse unitRaisingResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingApiCall response  %s", unitRaisingResponse);
                switch (unitRaisingResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        updateUnitRaisingDetails(unitRaisingResponse.data.unitRaisingDetailId, key);  //updating PostID in UnitPost Table
                        updateUnitStrengthTable(unitRaisingResponse, key); //updating PostID in UnitEquipment Table
                        updateUnitRaisingMetaData(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,
                                unitRaisingResponse.data.unitRaisingDetailId , key, Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING, mContext));
                        updateStatus(unitRaisingResponse,unitRaisingOR);
                        //updating PostID in Metadata Table
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UNIT_POST_SYNC", error);
            }
        });

        Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingApiCall end count   %s", "count: " + count);


    }

    private void updateStatus(UnitRaisingResponse unitRaisingResponse, UnitRaisingOR unitRaisingOR) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.unitRaisingId = unitRaisingResponse.data.unitRaisingDetailId;
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isUnitRaisingStrengthSynced = Constants.NEG_DEFAULT_COUNT;


            if(unitRaisingResponse.data.unitRaisingDetailId != 0){
                insertDependentSyncData(dependentSyncsStatus,true);
            }
        new UnitRaisingStrengthSync(mContext,
                dependentSyncsStatus.unitRaisingId);
        MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
        metaDataSyncing.setUnitRaisingId(unitRaisingResponse.data.unitRaisingDetailId);
    }

    private void insertDependentSyncData(DependentSyncsStatus dependentSyncsStatus,
                                        boolean isNewlyCreated) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.unitRaisingId,
                TableNameAndColumnStatement.UNIT_RAISING_ID, isNewlyCreated)){
            if(isNewlyCreated){
                dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
            }
        }
    }
}

