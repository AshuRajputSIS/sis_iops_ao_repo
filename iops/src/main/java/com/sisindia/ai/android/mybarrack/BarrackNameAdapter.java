package com.sisindia.ai.android.mybarrack;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BarrackNameAdapter extends BaseAdapter {

    private final List<Barrack> barrackList;

    private Context context;
    private LayoutInflater layoutInflater;



    public BarrackNameAdapter(Context addRotaTaskActivity, List<Barrack> barrackList) {
        this.context = addRotaTaskActivity;
        this.barrackList = barrackList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return barrackList.size();
    }

    @Override
    public Barrack getItem(int position) {
        return barrackList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.barrack_name_item, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Barrack barrackList, ViewHolder holder) {
        //TODO implement
        holder.barrackName.setText(barrackList.getName());
    }

    static class ViewHolder {
        @Bind(R.id.tv_barrack_name) CustomFontTextview barrackName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
