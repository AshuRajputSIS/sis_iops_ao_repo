package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 4/8/16.
 */
public class BarrackStrength implements Serializable{

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("ShiftId")
    @Expose
    private Integer shiftId;
    @SerializedName("RankId")
    @Expose
    private Integer rankId;
    @SerializedName("RankAbbrevation")
    @Expose
    private String rankAbbrevation;
    @SerializedName("RankCount")
    @Expose
    private Integer rankCount;
    @SerializedName("BarrackId")
    @Expose
    private Integer barrackId;
    @SerializedName("UnitName")
    @Expose
    private String unitName;
    @SerializedName("ShiftRankDetails")
    @Expose
    private ShiftRankDetails shiftRankDetails;
    @SerializedName("ShiftName")
    @Expose
    private String shiftName;
    @SerializedName("RankName")
    @Expose
    private String rankName;
    @SerializedName("BarrackName")
    @Expose
    private String barrackName;
    @SerializedName("BarrackStrength")
    @Expose
    private Integer strength;
    @SerializedName("IsArmed")
    @Expose
    private Boolean isArmed;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The shiftId
     */
    public Integer getShiftId() {
        return shiftId;
    }

    /**
     * @param shiftId The ShiftId
     */
    public void setShiftId(Integer shiftId) {
        this.shiftId = shiftId;
    }

    /**
     * @return The rankId
     */
    public Integer getRankId() {
        return rankId;
    }

    /**
     * @param rankId The RankId
     */
    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    /**
     * @return The rankAbbrevation
     */
    public String getRankAbbrevation() {
        return rankAbbrevation;
    }

    /**
     * @param rankAbbrevation The RankAbbrevation
     */
    public void setRankAbbrevation(String rankAbbrevation) {
        this.rankAbbrevation = rankAbbrevation;
    }

    /**
     * @return The rankCount
     */
    public Integer getRankCount() {
        return rankCount;
    }

    /**
     * @param rankCount The RankCount
     */
    public void setRankCount(Integer rankCount) {
        this.rankCount = rankCount;
    }

    /**
     * @return The barrackId
     */
    public Integer getBarrackId() {
        return barrackId;
    }

    /**
     * @param barrackId The BarrackId
     */
    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    /**
     * @return The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * @param unitName The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * @return The shiftRankDetails
     */
    public ShiftRankDetails getShiftRankDetails() {
        return shiftRankDetails;
    }

    /**
     * @param shiftRankDetails The ShiftRankDetails
     */
    public void setShiftRankDetails(ShiftRankDetails shiftRankDetails) {
        this.shiftRankDetails = shiftRankDetails;
    }

    /**
     * @return The shiftName
     */
    public String getShiftName() {
        return shiftName;
    }

    /**
     * @param shiftName The ShiftName
     */
    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    /**
     * @return The rankName
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * @param rankName The RankName
     */
    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    /**
     * @return The barrackName
     */
    public String getBarrackName() {
        return barrackName;
    }

    /**
     * @param barrackName The BarrackName
     */
    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }

    /**
     * @return The strength
     */
    public Integer getStrength() {
        return strength;
    }

    /**
     * @param strength The Strength
     */
    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    /**
     * @return The isArmed
     */
    public Boolean getIsArmed() {
        return isArmed;
    }

    /**
     * @param isArmed The IsArmed
     */
    public void setIsArmed(Boolean isArmed) {
        this.isArmed = isArmed;
    }

}
