package com.sisindia.ai.android.unitatriskpoa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 15/7/16.
 */


public class UARPendingFragment extends BaseFragment implements PendingCheckListItemListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CURRENT_TAB = "current_tab";
    private static final String UNIT_RISK_ID = "unitRiskId";
    @Bind(R.id.parent_layout)
    LinearLayout parentLayout;
    private String unitName;
    @Bind(R.id.dummyname)
    CustomFontTextview dummyname;
    private PendingCheckListAdapter pendingCheckListAdapter;
    private RecyclerView recyclerView;
    private Bundle bundle;
    public static UARPendingFragment fragment;
    private int unitRiskId;
    CustomFontTextview unitNameTxt;


    public UARPendingFragment() {

    }

    public UARPendingFragment(int riskId, String unitname) {
        // Required empty public constructor
        this.unitRiskId = riskId;
        this.unitName = unitname;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UARPendingFragment newInstance(String param1, String param2, int unitRiskId, int currentTab, String unitName) {
        // if (fragment == null) {
        fragment = new UARPendingFragment(unitRiskId, unitName);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putInt(CURRENT_TAB, currentTab);
        args.putInt(UNIT_RISK_ID, unitRiskId);

        fragment.setArguments(args);
        // }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.action_plan_list, container, false);

        initialiseView(view);
        initialiseDate();

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.action_plan_list;
    }


    private void initialiseDate() {

        List<ActionPlanMO> pendingActionPlanMOList = IOPSApplication.getInstance().
                getUARSelectionInstance().getUARPendingList(unitRiskId, 0);
        if (null != pendingActionPlanMOList) {
            dummyname.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            pendingCheckListAdapter = new PendingCheckListAdapter(getContext(), pendingActionPlanMOList);
            recyclerView.setAdapter(pendingCheckListAdapter);
            pendingCheckListAdapter.notifyDataSetChanged();
            pendingCheckListAdapter.setPendingCheckListItemListener(this);
        } else {
            dummyname.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        unitNameTxt.setText(unitName);
    }

    private void initialiseView(View view) {


        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        unitNameTxt = (CustomFontTextview) view.findViewById(R.id.unit_name);
        dummyname = (CustomFontTextview) view.findViewById(R.id.dummyname);
        dummyname.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        unitNameTxt = (CustomFontTextview) view.findViewById(R.id.unit_name);


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemClick(ActionPlanMO actionPlan) {
        ActionPlanMO actionPlanMO = actionPlan;

        if (appPreferences.getDutyStatus()) {

            int areaInspectorId = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAreaInspectorId();
            if (areaInspectorId == actionPlanMO.getAssignTo()) {
                Intent intent = new Intent(getActivity(), UpdateActionPlanActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("ActionPlanMO", actionPlanMO);
                intent.putExtra("UnitName", unitName.toString());
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            } else {
                snackBarWithMesg(parentLayout, getActivity().getResources().getString(R.string.AI_UAR_MESG));
            }


        } else {

            snackBarWithMesg(parentLayout, getActivity().getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE));

        }


    }


}
