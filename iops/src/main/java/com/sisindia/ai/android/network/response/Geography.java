package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class Geography {

    @SerializedName("CoordinateSystemId")
    private Integer CoordinateSystemId;
    @SerializedName("WellKnownText")
    private String WellKnownText;

    /**
     * @return The CoordinateSystemId
     */
    public Integer getCoordinateSystemId() {
        return CoordinateSystemId;
    }

    /**
     * @param CoordinateSystemId The CoordinateSystemId
     */
    public void setCoordinateSystemId(Integer CoordinateSystemId) {
        this.CoordinateSystemId = CoordinateSystemId;
    }

    /**
     * @return The WellKnownText
     */
    public String getWellKnownText() {
        return WellKnownText;
    }

    /**
     * @param WellKnownText The WellKnownText
     */
    public void setWellKnownText(String WellKnownText) {
        this.WellKnownText = WellKnownText;
    }

}