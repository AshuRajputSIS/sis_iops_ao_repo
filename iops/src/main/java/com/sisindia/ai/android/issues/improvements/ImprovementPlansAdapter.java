package com.sisindia.ai.android.issues.improvements;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.issues.models.ActionPlanCount;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Saruchi on 15-04-2016.
 */

public class ImprovementPlansAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    Context mContext;
    ArrayList<Object> mDataItems;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private AppPreferences appPreferences;


    public ImprovementPlansAdapter(Context mContext) {
        this.mContext = mContext;
        dateTimeFormatConversionUtil= new DateTimeFormatConversionUtil();
        appPreferences = new AppPreferences(mContext);
    }
    public void setImprovementDataList(ArrayList<Object> mDataItems) {
        this.mDataItems = mDataItems;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }
    public void setOnRecyclerViewClicked(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.openissues_layout, parent, false);
            return new OpenIssuesHolder(view);
        } else if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            view = LayoutInflater.from(mContext).inflate(R.layout.improvement_cardview, parent, false);
            return new ImprovementDetailsHolder(view);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mDataItems.get(position);
        if (holder instanceof OpenIssuesHolder && object instanceof ActionPlanCount) {
            // String dataItem = getItem(position);
            OpenIssuesHolder openIssuesHolder = (OpenIssuesHolder) holder;
            openIssuesHolder.lessThan48HoursTv.setText(String.valueOf(((ActionPlanCount) object).getCountLessthan2Days()));
            openIssuesHolder.greaterThan48HoursTV.setText(String.valueOf(((ActionPlanCount) object).getCountMorethan2Days()));
            openIssuesHolder.greaterThan7Days.setText(String.valueOf(((ActionPlanCount) object).getCountMorethan7Days()));


        }
        else if (holder instanceof ImprovementDetailsHolder && object instanceof  ImprovementPlanListMO) {
            ImprovementDetailsHolder improvementDetailsHolder = (ImprovementDetailsHolder) holder;
            final ImprovementPlanListMO improvementPlanListMO =(ImprovementPlanListMO)mDataItems.get(position);
            improvementDetailsHolder.actionPlanName.setText(improvementPlanListMO.getActionPlanName());
            improvementDetailsHolder.unitName.setText(improvementPlanListMO.getUnitName());
            improvementDetailsHolder.improvementStatus.setText(improvementPlanListMO.getImprovementStatus());
            improvementDetailsHolder.targetDate.setText(improvementPlanListMO.getExpectedClosureDate());
            improvementDetailsHolder.targetClosureDate.setText(improvementPlanListMO.getExpectedClosureDate());
            improvementDetailsHolder.targetDateTxt.setText(improvementPlanListMO.getExpectedClosureDate());
            improvementDetailsHolder.assignedToName.setText(improvementPlanListMO.getAssignedToName());

                if (!TextUtils.isEmpty(improvementPlanListMO.getExpectedClosureDate())) {
                    String date = dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(improvementPlanListMO.getExpectedClosureDate());
                    improvementDetailsHolder.targetDate.setText(date);
                    improvementDetailsHolder.targetClosureDate.setText(date);
                    improvementDetailsHolder.targetDateTxt.setText(date);
                } else {
                    improvementDetailsHolder.targetDate.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));
                    improvementDetailsHolder.targetClosureDate.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));
                    improvementDetailsHolder.targetDateTxt.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));
                }

                if (improvementPlanListMO.getUnitName() != null && !improvementPlanListMO.getUnitName().isEmpty() ) {
                    improvementDetailsHolder.unitName.setText(improvementPlanListMO.getUnitName());
                } else {
                    if (improvementPlanListMO.getBarrackName() != null && !improvementPlanListMO.getBarrackName().isEmpty() ) {
                        improvementDetailsHolder.unitName.setText(improvementPlanListMO.getBarrackName());
                    }
                    else{
                        improvementDetailsHolder.unitName.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));

                    }
                }
            if(null==improvementPlanListMO.getUnitId() ||0==improvementPlanListMO.getUnitId() ){
                if (improvementPlanListMO.getBarrackName() != null && !improvementPlanListMO.getBarrackName().isEmpty() ) {
                    improvementDetailsHolder.unitName.setText(improvementPlanListMO.getBarrackName());
                }
                else{
                    improvementDetailsHolder.unitName.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));

                }
            }

                if (improvementPlanListMO.getAssignedToName() != null && !improvementPlanListMO.getAssignedToName().isEmpty() ) {
                    improvementDetailsHolder.assignedToName.setText(improvementPlanListMO.getAssignedToName());
                } else {
                    improvementDetailsHolder.assignedToName.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));
                }

                if (improvementPlanListMO.getCreatedDateTime() != null && !improvementPlanListMO.getCreatedDateTime().isEmpty()) {
                    long millis = dateTimeFormatConversionUtil.convertStringToDate(improvementPlanListMO.getCreatedDateTime()).getTime();
                    improvementDetailsHolder.daysAgoTxt.setReferenceTime(millis);
                } else {
                    improvementDetailsHolder.daysAgoTxt.setText(mContext.getResources().getString(R.string.NOT_AVAILABLE));
                }


                    improvementDetailsHolder.viewDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            appPreferences.setIssueType(Constants.NavigationFlags.TYPE_IMPROVEMENT_PLAN);
                            Intent i = new Intent(mContext, ImprovementPlanEditActivity.class);
                            i.putExtra(mContext.getResources().getString(R.string.improvement_plan_edit), improvementPlanListMO);
                            mContext.startActivity(i);
                        }
                    });




            }



    }


    @Override
    public int getItemCount() {

        return mDataItems.size() ;

    }



    private boolean isPositionHeader(int position) {
        return position == 0;
    }

        private ImprovementPlanListMO getItem(int position) {
            if(mDataItems.get(position-1) instanceof  ImprovementPlanListMO)
        return (ImprovementPlanListMO)mDataItems.get(position-1);
            else
                return new ImprovementPlanListMO();
    }
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    public class ImprovementDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.lastInspectionUnitName)
        CustomFontTextview unitName;
        @Bind(R.id.guradName_or_clientName)
        CustomFontTextview actionPlanName;
        @Bind(R.id.timeDurationLeft)
        RelativeTimeTextView daysAgoTxt; // expected closure date
        @Bind(R.id.checking_grievance_complaint_cardview_complaint_type_txt)
        CustomFontTextview targetDate; // expected closure date
        @Bind(R.id.issueTargetClosureDate)
        CustomFontTextview targetClosureDate; // expected closure date
        @Bind(R.id.complaint_view_details_image_layout)
        LinearLayout viewDetails;
        @Bind(R.id.lastInspectionAssignedTo)
        CustomFontTextview assignedToName;
        @Bind(R.id.checking_improve_plan_targeted_closing_cardview__type_txt)
        CustomFontTextview targetDateTxt; // expected closure date
        @Bind(R.id.lastInspectionStatus)
        CustomFontTextview improvementStatus;


        public ImprovementDetailsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //viewDetails.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }

    }

    public class OpenIssuesHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lessThan48HoursTv)
        CustomFontTextview lessThan48HoursTv;
        @Bind(R.id.greaterThan48HoursTV)
        CustomFontTextview greaterThan48HoursTV;
        @Bind(R.id.greaterThan7Days)
        CustomFontTextview greaterThan7Days;

        public OpenIssuesHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
