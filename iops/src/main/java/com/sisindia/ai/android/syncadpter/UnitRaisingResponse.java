package com.sisindia.ai.android.syncadpter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by compass on 8/16/2017.
 */

public class UnitRaisingResponse {
    @SerializedName("StatusCode")
    public int statusCode;
    @SerializedName("StatusMessage")
    public String statusMessage;
    @SerializedName("Data")
    public UnitRaisingData  data;

}
