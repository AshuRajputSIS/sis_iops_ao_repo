package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.rota.DateRotaModel;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 08-11-2016.
 */

public class DutyAttendanceDB extends SISAITrackingDB {

    public DutyAttendanceDB(Context context) {
        super(context);
    }

    public synchronized void insertDutyAttendanceDetails(DutyAttendanceMo dutyAttendanceMo) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

            try {
                if (dutyAttendanceMo != null) {

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TableNameAndColumnStatement.DUTY_ON_DATE_TIME, dutyAttendanceMo.getDutyOnTime());
                    contentValues.put(TableNameAndColumnStatement.DUTY_OFF_DATE_TIME, dutyAttendanceMo.getDutyOffTime());
                    contentValues.put(TableNameAndColumnStatement.DUTY_REPONSE_ID, dutyAttendanceMo.getDutyResponseId());
                    contentValues.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                    sqlite.insert(TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE, null, contentValues);
                    sqlite.setTransactionSuccessful();
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {

                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void deleteDutyAttendance() {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        String deleteQuery = "delete from " + TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE
                + TableNameAndColumnStatement.DUTY_OFF_DATE_TIME + " is not null and "
                + TableNameAndColumnStatement.DUTY_REPONSE_ID + " is not null";
        Cursor cursor = null;

        synchronized (sqlite) {

            try {
                cursor = sqlite.rawQuery(deleteQuery, null);
                Timber.d("cursor detail object  %s", cursor.getCount());

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                Timber.e("SQLITE transaction ended");
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }

        }
    }

    public void updateDutyAttendanceId(int dutyResponseId, int seqId) {
        SQLiteDatabase sqlite = null;
        String dutyUpdateQuery = "update " + TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE
                + " SET " + TableNameAndColumnStatement.DUTY_REPONSE_ID + "='" + dutyResponseId + "' ,"
                + TableNameAndColumnStatement.IS_SYNCED + " = " + 1
                + " where " + TableNameAndColumnStatement.ID + " = " + seqId;

        Timber.d(" Duty updateQuery Id %s", dutyUpdateQuery);

        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(dutyUpdateQuery, null);
            Timber.d(" Duty updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void updateDutyOffTime(String dutyOffTime) {

//        String[] receivedTaskCounts = getTotalTaskAndCompletedTaskCount(dutyOffTime);
        DateRotaModel dateRotaModel = new DateRotaModel();
        try {
            dateRotaModel = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaAndTaskCount(dutyOffTime.split(" ")[0]);
        } catch (Exception e) {
            dateRotaModel.setRotaTotalCount(0);
            dateRotaModel.setRotaCompletedCount(0);
        }

        SQLiteDatabase sqlite = null;

        String dutyUpdateQueryOffTime = "update " + TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE
                + " SET " + TableNameAndColumnStatement.DUTY_OFF_DATE_TIME + "='" + dutyOffTime + "' , TaskAssigned='" + dateRotaModel.getRotaTotalCount() + "' , " +
                "TaskCompleted='" + dateRotaModel.getRotaCompletedCount() + "' , " +
                TableNameAndColumnStatement.IS_SYNCED + "= 0"
                + " where " + TableNameAndColumnStatement.DUTY_OFF_DATE_TIME + " is  null";

        Timber.d(" Duty updateQuery  %s", dutyUpdateQueryOffTime);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(dutyUpdateQueryOffTime, null);
            Timber.d(" Duty updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void insertIntoSyncRequest(String startDateTime) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

            try {
                if (!TextUtils.isEmpty(startDateTime)) {

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TableNameAndColumnStatement.SYNC_START_DATE_TIME, startDateTime);
                    sqlite.insert(TableNameAndColumnStatement.SYNC_REQUEST, null, contentValues);
                    sqlite.setTransactionSuccessful();
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {

                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }

        }
    }

    public String getLatestSyncRequestTime() {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = " select " + TableNameAndColumnStatement.SYNC_START_DATE_TIME + " from " + TableNameAndColumnStatement.SYNC_REQUEST + "  order by id desc limit 1";
        String startDateTime = "";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        try {
            if (cursor != null && cursor.getCount() != 0) {

                if (cursor.moveToFirst()) {
                    do {
                        startDateTime = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SYNC_START_DATE_TIME));
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getMessage();
            Crashlytics.logException(exception);
        }
        return startDateTime;
    }

    /*public DutyAttendanceMo getTotalTaskAndCompletedTaskCount(DutyAttendanceMo dutyAttendanceMo, String date) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        String query = "select count(case when (task_status_id in (3,4)) then id else null end) TaskCompleted, count(*) TaskAssigned from task where " +
                "substr(estimated_execution_start_date_time, 0, 11) = '" + date + "'";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        try {
            if (cursor != null && cursor.getCount() != 0) {

                if (cursor.moveToFirst()) {
                    do {
                        if (dutyAttendanceMo != null) {
//                            dutyAttendanceMo.setTaskAssigned(cursor.getString(cursor.getColumnIndex("TaskAssigned")));
//                            dutyAttendanceMo.setTaskCompleted(cursor.getString(cursor.getColumnIndex("TaskCompleted")));
                        }
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getMessage();
            Crashlytics.logException(exception);
        }
        return dutyAttendanceMo;
    }

    public String[] getTotalTaskAndCompletedTaskCount(String date) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        String[] taskCount = new String[2];

        try {
            String[] splitDate = date.split(" ");
            date = splitDate[0].trim();
        } catch (Exception e) {
        }

        String query = "select count(case when (task_status_id in (3,4)) then id else null end) TaskCompleted, count(*) TaskAssigned from task where " +
                "substr(estimated_execution_start_date_time, 0, 11) = '" + date + "'";

//        Timber.d("Task Assigned counter Query  %s", query);

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        try {
            if (cursor != null && cursor.getCount() != 0) {

                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getString(cursor.getColumnIndex("TaskAssigned")) == null || cursor.getString(cursor.getColumnIndex("TaskAssigned")).isEmpty())
                            taskCount[0] = "0";
                        else
                            taskCount[0] = cursor.getString(cursor.getColumnIndex("TaskAssigned"));

                        if (cursor.getString(cursor.getColumnIndex("TaskCompleted")) == null || cursor.getString(cursor.getColumnIndex("TaskCompleted")).isEmpty())
                            taskCount[1] = "0";
                        else
                            taskCount[1] = cursor.getString(cursor.getColumnIndex("TaskCompleted"));
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getMessage();
            Crashlytics.logException(exception);
        }
        return taskCount;
    }
*/
}

