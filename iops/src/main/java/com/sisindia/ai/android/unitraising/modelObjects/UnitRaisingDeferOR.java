package com.sisindia.ai.android.unitraising.modelObjects;


import com.google.gson.annotations.SerializedName;

public class UnitRaisingDeferOR {

@SerializedName("UnitId")

public Integer unitId;
@SerializedName("UnitCode")

public String unitCode;

@SerializedName("Remarks")

public String remarks;
@SerializedName("CreatedBy")

public Integer createdBy;
@SerializedName("ExpectedRaisingDate")

public String expectedRaisingDate;
@SerializedName("NewRaisingDate")

public String newRaisingDate;
@SerializedName("DeferredDateTime")

public String deferredDateTime;

    @SerializedName("Reason")

    public String deferredReason;

}