package com.sisindia.ai.android.issues.complaints;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.ComplaintCount;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.views.CustomFontButton;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ComplaintsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ComplaintsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ArrayList<Object> mDataValues = new ArrayList<>();
    CustomComplaintAdapter customIssuesAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String issueType;
    @Bind(R.id.add_complaint)
    CustomFontButton mAddComplaint;
    RecyclerView mComplaintRecyclerView;
    private ComplaintCount mcomplaintCount;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Context mContext;
    private boolean countUpdatedFromserver;
    private int openComplaintsCount;
    private ArrayList<IssuesMO> openComplaintsList;


    public ComplaintsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ComplaintsFragment newInstance(String param1, String param2) {
            ComplaintsFragment fragment = new ComplaintsFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            issueType = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this

        View view = inflater.inflate(R.layout.issues_homepage_complaints, container, false);
        ButterKnife.bind(this, view);
        mComplaintRecyclerView = (RecyclerView) view.findViewById(R.id.complaints_recyclerview);
        mSwipeRefreshLayout =(SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
      /*  ArrayList<IssuesMO> issuesList = IOPSApplication.getInstance().getIssueSelectionDBInstance().getComplaintsList();
        Log.e("Array value","Array Value:::"+issuesList);*/
        initComplaintsViews();

        return view;
    }

    @Override
    protected int getLayout() {
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initComplaintsViews() {
        ArrayList<Object> issuesList = new ArrayList<>();
        mComplaintRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ComplaintsData complaintsData = IOPSApplication.getInstance().getIssueSelectionDBInstance().getComplaintsList();

        if(complaintsData != null){
            openComplaintsList = complaintsData.getComplaintsList();
            mcomplaintCount = (ComplaintCount) IOPSApplication.getInstance().getIssueSelectionDBInstance().getOpenIssuesCount(2);
        }
        if(mcomplaintCount != null) {
            issuesList.add(mcomplaintCount);
        }
        else{
            ComplaintCount complaintCount = new ComplaintCount();
            complaintCount.setCountMorethan7Days(0);
            complaintCount.setCountMorethan2Days(0);
            complaintCount.setCountLessthan2Days(0);
            issuesList.add(complaintCount);
        }

        if(complaintsData != null) {
            issuesList.addAll(openComplaintsList);
        }

        customIssuesAdapter = new CustomComplaintAdapter(getContext(),issuesList, false, true, true, false);
        mComplaintRecyclerView.setAdapter(customIssuesAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }
 //TODO : Delete this method as functionality is taken care.


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void updateRecyclerview(ArrayList<Object> models) {
        mDataValues.addAll(models);
        customIssuesAdapter.notifyDataSetChanged();
    }
    @OnClick(R.id.add_complaint)
    public void addComplaint(){

        if(appPreferences.getDutyStatus()) {
            appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
            openOnBoardingActivity(OnBoardingFactory.COMPLINT,2);
        }
        else{
            snackBarWithMessage( getResources().getString(R.string.TURN_DUTY));
        }


    }

    public void setOpenComplaintCount(ComplaintCount grievanceCount) {
        mcomplaintCount = grievanceCount;
    }


    @Override
    public void onRefresh() {
        initComplaintsViews();
    }
    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(mContext, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        mContext.startActivity(intent);
    }
}
