package com.sisindia.ai.android.rota.nightcheck;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.daycheck.StepperDataMo;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Shushrut on 09-06-2016.
 */
public class NightCheckListAdapter extends RecyclerView.Adapter<NightCheckListAdapter.NightCheckViewHolder>  {

    int pos = 1;
    private Context mContext;
    private ArrayList<String>mDayCheckType;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private int positionToRefersh;
    private boolean isRefreshed;
    /*ImageView mnextarrow;*/
    public NightCheckListAdapter(Context dayCheckNavigationActivity, ArrayList<String>mDayCheckType) {
        mContext = dayCheckNavigationActivity;
        this.mDayCheckType = mDayCheckType;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener){
        this.onRecyclerViewItemClickListener = itemListener;
    }

    @Override
    public NightCheckViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_daycheck_row, parent, false);
        NightCheckViewHolder mDayCheckViewholder = new NightCheckViewHolder(view);
        return mDayCheckViewholder;
    }

    @Override
    public void onBindViewHolder(NightCheckListAdapter.NightCheckViewHolder holder, int position) {
        holder.mitemname.setText(mDayCheckType.get(position));
        holder.mitemname.setTag(position);
        //holder.mitemname.setOnClickListener(this);
        // Log.e("ArrayList","ArrayList :::"+StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().toString());
        if(isRefreshed){
            int pos = position;
            HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
            if(null!=mStepperDataHolder && mStepperDataHolder.size()!=0){
                if(mStepperDataHolder.containsValue(NightCheckEnum.valueOf(pos).getValue())){
                    holder.mnextarrow.setImageResource(R.drawable.greentick);
                }
            }

           /* if(StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().contains(pos+1)) {
                holder.mnextarrow.setImageResource(R.drawable.greentick);
            }*/

        }

        holder.mitem_count_value.setText(String.valueOf(position+1));
    }

    @Override
    public int getItemCount() {
        return mDayCheckType.size();
    }

   /* @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.itemname:
                Toast.makeText(mContext, "Pos :"+v.getTag(), Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    public void updateWithTickMark(int pos,boolean isRefreshed) {
        positionToRefersh = pos;
        this.isRefreshed = isRefreshed;

    }

    public class NightCheckViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomFontTextview mitemname;
        TextView mitem_count_value;
        ImageView mnextarrow;

        public NightCheckViewHolder(View itemView) {
            super(itemView);
            mitemname = (CustomFontTextview)itemView.findViewById(R.id.itemname);
            mitem_count_value = (TextView)itemView.findViewById(R.id.item_count_value);
            mnextarrow = (ImageView)itemView.findViewById(R.id.nextarrow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v,getLayoutPosition());
        }
    }

}
