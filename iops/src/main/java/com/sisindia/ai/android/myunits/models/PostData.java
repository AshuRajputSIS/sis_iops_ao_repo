package com.sisindia.ai.android.myunits.models;

import java.io.Serializable;

/**
 * Created by Durga Prasad on 25-03-2016.
 */
public class PostData implements Serializable {

    private String postName;
    private int icon;
    private int sequenceId;
    private int unitId;
    private int unitPostId;
    private boolean isActive;
    private String geoPoint;
    private String mainGateDistance;
    private String barrackDistance;
    private String unitOfficeDistance;
    private int armedStrength;
    private int unarmedStrength;
    private double latitude;
    private double longitude;
    private int postStatus;
    private int isMainGate;
    private String description;
    private String deviceNo;

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(int postStatus) {
        this.postStatus = postStatus;
    }

    public int getIsMainGate() {
        return isMainGate;
    }

    public void setIsMainGate(int isMainGate) {
        this.isMainGate = isMainGate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getArmedStrength() {
        return armedStrength;
    }

    public void setArmedStrength(int armedStrength) {
        this.armedStrength = armedStrength;
    }

    public int getUnarmedStrength() {
        return unarmedStrength;
    }

    public void setUnarmedStrength(int unarmedStrength) {
        this.unarmedStrength = unarmedStrength;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getUnitPostId() {
        return unitPostId;
    }

    public void setUnitPostId(int unitPostId) {
        this.unitPostId = unitPostId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(String geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getMainGateDistance() {
        return mainGateDistance;
    }

    public void setMainGateDistance(String mainGateDistance) {
        this.mainGateDistance = mainGateDistance;
    }

    public String getBarrackDistance() {
        return barrackDistance;
    }

    public void setBarrackDistance(String barrackDistance) {
        this.barrackDistance = barrackDistance;
    }

    public String getUnitOfficeDistance() {
        return unitOfficeDistance;
    }

    public void setUnitOfficeDistance(String unitOfficeDistance) {
        this.unitOfficeDistance = unitOfficeDistance;
    }

    public int getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }

    @Override
    public String toString() {
        return "PostData{" +
                "postName='" + postName + '\'' +
                ", icon=" + icon +
                ", sequenceId=" + sequenceId +
                ", unitId=" + unitId +
                ", unitPostId=" + unitPostId +
                ", isActive=" + isActive +
                ", geoPoint='" + geoPoint + '\'' +
                ", mainGateDistance='" + mainGateDistance + '\'' +
                ", barrackDistance='" + barrackDistance + '\'' +
                ", unitOfficeDistance='" + unitOfficeDistance + '\'' +
                ", armedStrength=" + armedStrength +
                ", unarmedStrength=" + unarmedStrength +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", postStatus=" + postStatus +
                ", isMainGate=" + isMainGate +
                ", deviceNo=" + deviceNo +
                '}';
    }
}