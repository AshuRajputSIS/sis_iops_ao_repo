package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.ContactMO;

import java.util.ArrayList;

public class ContactSpinnerAdapter extends ArrayAdapter<ContactMO> {

    // Your sent context
    private Context context;
    private LayoutInflater inflater;
    private ContactMO tempValues = null;
    private ArrayList<ContactMO> values = new ArrayList<ContactMO>();
    //private ContactMO[] values;

    public ContactSpinnerAdapter(Context context, int textViewResourceId,
                                 ArrayList<ContactMO> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return values.size();
    }

    public ContactMO getItem(int position) {
        return values.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        return getCustomView(position, convertView, parent);
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.commom_textview_row, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = values.get(position);

        TextView label = (TextView) row.findViewById(R.id.commonTV);


        // Set values for spinner each row
        label.setText(tempValues.getName());


        return row;
    }
}