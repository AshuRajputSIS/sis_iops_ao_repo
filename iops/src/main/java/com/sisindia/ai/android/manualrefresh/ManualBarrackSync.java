package com.sisindia.ai.android.manualrefresh;

import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.BarrackTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.ImprovementPlanTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.IssuesTableCountMO;
import com.sisindia.ai.android.issues.BaseIssuesSync;
import com.sisindia.ai.android.issues.IssuesSyncListener;
import com.sisindia.ai.android.issues.IssuesUpdateAsyncTask;
import com.sisindia.ai.android.mybarrack.BarrackSyncListener;
import com.sisindia.ai.android.mybarrack.MyBarrackUpdateAsyncTask;
import com.sisindia.ai.android.mybarrack.modelObjects.MyBarrackBaseMo;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 6/12/2017.
 */

public class ManualBarrackSync {

    public static void getBarrackApiCall(final BarrackSyncListener barrackSyncListener, final Context mContext, final boolean isDetroyed) {
        if (tablesToSync()) {
            showprogressbar(mContext);
            new SISClient(mContext).getApi().getAllBarracks(new Callback<MyBarrackBaseMo>() {
                @Override
                public void success(MyBarrackBaseMo myBarrackBaseMo, Response response) {
                    Timber.d("myBarrackBaseMo response  %s", myBarrackBaseMo);
                    switch (myBarrackBaseMo.statusCode) {
                        case HttpURLConnection.HTTP_OK:
                            new MyBarrackUpdateAsyncTask(mContext, myBarrackBaseMo, barrackSyncListener).execute();
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(mContext, mContext.getString
                                    (R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar(isDetroyed);
                    Crashlytics.logException(error);
                }
            });
        } else {
            Util.showToast(mContext, mContext.getString(R.string.please_sync_older_records));
        }
    }

    private static void showprogressbar(Context mContext) {
        Util.showProgressBar(mContext, R.string.loading_please_wait);
    }

    private static void hideprogressbar(boolean isDetroyed) {
        Util.dismissProgressBar(isDetroyed);
    }

    private synchronized static boolean tablesToSync() {
        BarrackTableCountMO barrackTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getBarrackTableCount();

        if (barrackTableCountMO != null &&
                barrackTableCountMO.getSyncCount() != 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * order in which tables will be deleted
     */
}
