package com.sisindia.ai.android.fcmnotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 12/11/16.
 */

public class NotificationReceivingMO implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("landing_page")
    @Expose
    private String landingPage;

    @SerializedName("callbackUrl")
    @Expose
    private String callbackUrl;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("priority")
    @Expose
    private String priority;

    @SerializedName("type")
    @Expose
    private Integer type;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("deleteId")
    private int deleteId;

    @SerializedName("unitlist")
    private String unitList;

    private String receivedDatTimeString;
    private long recievedDateTime;

    public long getRecievedDateTime() {
        return recievedDateTime;
    }

    public void setRecievedDateTime(long recievedDateTime) {
        this.recievedDateTime = recievedDateTime;
    }

    public int getDeleteId() {
        return deleteId;
    }

    public void setDeleteId(String deleteId) {
        deleteId = deleteId;
    }

    public int getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(int appUserId) {
        this.appUserId = appUserId;
    }

    private int appUserId;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The landingPage
     */
    public String getLandingPage() {
        return landingPage;
    }

    /**
     * @param landingPage The landing_page
     */
    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    /**
     * @return The callbackUrl
     */
    public String getCallbackUrl() {
        return callbackUrl;
    }

    /**
     * @param callbackUrl The callbackUrl
     */
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @param priority The priority
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "NotificationReceivingMO{" +
                "name='" + name + '\'' +
                ", landingPage='" + landingPage + '\'' +
                ", callbackUrl='" + callbackUrl + '\'' +
                ", id=" + id +
                ", priority='" + priority + '\'' +
                ", type=" + type +
                ", message='" + message + '\'' +
                ", title='" + title + '\'' +
                ", unitList='" + unitList + '\'' +
                ", recievedDateTime=" + recievedDateTime +
                ", DeleteId=" + deleteId +
                ", appUserId=" + appUserId +
                '}';
    }

    /**
     * @param title The title
     */

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnitList() {
        return unitList;
    }

    public void setUnitList(String unitList) {
        this.unitList = unitList;
    }

    public String getReceivedDatTimeString() {
        return receivedDatTimeString;
    }

    public void setReceivedDatTimeString(String receivedDatTimeString) {
        this.receivedDatTimeString = receivedDatTimeString;
    }
}
