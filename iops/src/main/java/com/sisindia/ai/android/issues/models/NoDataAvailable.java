package com.sisindia.ai.android.issues.models;

/**
 * Created by Durga Prasad on 12-05-2016.
 */
public class NoDataAvailable {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
