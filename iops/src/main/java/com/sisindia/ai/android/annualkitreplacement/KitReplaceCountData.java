package com.sisindia.ai.android.annualkitreplacement;
/**
 * Created by Durga Prasad on 23-09-2016.
 */

    import com.google.gson.annotations.Expose;
    import com.google.gson.annotations.SerializedName;

    import java.util.ArrayList;
    import java.util.List;

public class KitReplaceCountData {

        @SerializedName("AnnualKitReplacements")
        @Expose
        private List<AnnualKitReplacementMO> annualKitReplacements = new ArrayList<AnnualKitReplacementMO>();



    @SerializedName("AdHocKit")
    @Expose
    private List<AnnualKitReplacementMO> addHocKitList = new ArrayList<AnnualKitReplacementMO>();

        /**
         *
         * @return
         * The annualKitReplacements
         */
        public List<AnnualKitReplacementMO> getAnnualKitReplacements() {
            return annualKitReplacements;
        }

        /**
         *
         * @param annualKitReplacements
         * The AnnualKitReplacements
         */
        public void setAnnualKitReplacements(List<AnnualKitReplacementMO> annualKitReplacements) {
            this.annualKitReplacements = annualKitReplacements;
        }


    public List<AnnualKitReplacementMO> getAddHocKitList() {
        return addHocKitList;
    }

    public void setAddHocKitList(List<AnnualKitReplacementMO> addHocKitList) {
        this.addHocKitList = addHocKitList;
    }

    }

