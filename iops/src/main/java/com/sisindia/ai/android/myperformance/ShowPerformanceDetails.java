package com.sisindia.ai.android.myperformance;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.help.HelpFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 12/20/2017.
 */

public class ShowPerformanceDetails extends AppCompatActivity implements ToolbarTitleUpdateListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_performance_details);
        ButterKnife.bind(this);

        setUpToolBar();

        HelpFragment helpFragment = HelpFragment.newInstance("Help Tutorials");
        addFragment(helpFragment, getResources().getString(R.string.Help_videos));
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Help");
    }


    public void addFragment(final Fragment fragment, final String fragmentName) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.container_fragment, fragment, fragmentName);
//                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commitAllowingStateLoss();
                } catch (Exception exception) {
                    exception.printStackTrace();
                    Crashlytics.logException(exception);
                }
            }
        }, 300);
    }

    @Override
    public void changeToolbarTitle(String title) {

    }
}
