package com.sisindia.ai.android.rota.models;

/**
 * Created by Ashu Rajput on 6/23/2018.
 */

public class KitDistributionStatusMO {

    private int totalCountOfKitsForDistribution;
    private int pendingCountOfKitsForDistribution;

    public int getTotalCountOfKitsForDistribution() {
        return totalCountOfKitsForDistribution;
    }

    public void setTotalCountOfKitsForDistribution(int totalCountOfKitsForDistribution) {
        this.totalCountOfKitsForDistribution = totalCountOfKitsForDistribution;
    }

    public int getPendingCountOfKitsForDistribution() {
        return pendingCountOfKitsForDistribution;
    }

    public void setPendingCountOfKitsForDistribution(int pendingCountOfKitsForDistribution) {
        this.pendingCountOfKitsForDistribution = pendingCountOfKitsForDistribution;
    }
}
