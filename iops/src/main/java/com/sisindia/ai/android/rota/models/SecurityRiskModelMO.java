package com.sisindia.ai.android.rota.models;

import java.util.ArrayList;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskModelMO {
    String mSecurityTypeTitle;
    ArrayList<SecurityRiskOptionMO> mRiskTypeHolder;
    private Integer spinnerPosition=0;
    private String reasonTxt;
    private Integer cameraIconSelectionPosition;
    private String pictureUri;
    private Integer mOptionNameId;
    private Integer mIsMandatory;
    private String mControlType;
    private boolean dataFilled;

    private int mDropDownItem;
    public int getmDropDownItem() {
        return mDropDownItem;
    }

    public void setmDropDownItem(int mDropDownItem) {
        this.mDropDownItem = mDropDownItem;
    }

    public Integer getPictureCapturePos() {
        return pictureCapturePos;
    }

    public void setPictureCapturePos(Integer pictureCapturePos) {
        this.pictureCapturePos = pictureCapturePos;
    }

    private Integer pictureCapturePos;
    private Integer securityRiskQuestionId;
    public Integer getSecurityRiskQuestionId() {
        return securityRiskQuestionId;
    }

    public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
        this.securityRiskQuestionId = securityRiskQuestionId;
    }



    public Integer getmOptionNameId() {
        return mOptionNameId;
    }

    public void setmOptionNameId(Integer mOptionNameId) {
        this.mOptionNameId = mOptionNameId;
    }

    public Integer getmIsMandatory() {
        return mIsMandatory;
    }

    public void setmIsMandatory(Integer mIsMandatory) {
        this.mIsMandatory = mIsMandatory;
    }

    public String getmControlType() {
        return mControlType;
    }

    public void setmControlType(String controlType) {
        mControlType = controlType;
    }

    public boolean isDataFilled() {
        return dataFilled;
    }

    public void setDataFilled(boolean dataFilled) {
        this.dataFilled = dataFilled;
    }

    public Integer getCameraIconSelectionPosition() {
        return cameraIconSelectionPosition;
    }

    public void setCameraIconSelectionPosition(Integer cameraIconSelectionPosition) {
        this.cameraIconSelectionPosition = cameraIconSelectionPosition;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }

    public String getReasonTxt() {
        return reasonTxt;
    }

    public void setReasonTxt(String reasonTxt) {
        this.reasonTxt = reasonTxt;
    }

    private boolean editFieldVisible=false;


    private Integer pos;

    public boolean isEditFieldVisible() {
        return editFieldVisible;
    }

    public void setEditFieldVisible(boolean editFieldVisible) {
        this.editFieldVisible = editFieldVisible;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Integer getSpinnerPosition() {
        return spinnerPosition;
    }

    public void setSpinnerPosition(Integer spinnerPosition) {
        this.spinnerPosition = spinnerPosition;
    }
    public String getmSecurityTypeTitle() {
        return mSecurityTypeTitle;
    }

    public void setmSecurityTypeTitle(String mSecurityTypeTitle) {
        this.mSecurityTypeTitle = mSecurityTypeTitle;
    }

    public ArrayList<SecurityRiskOptionMO> getmRiskTypeHolder() {
        return mRiskTypeHolder;
    }

    public void setmRiskTypeHolder(ArrayList<SecurityRiskOptionMO> mRiskTypeHolder) {
        this.mRiskTypeHolder = mRiskTypeHolder;
    }

    @Override
    public String toString() {
        return "SecurityRiskModelMO{" +
                "mSecurityTypeTitle='" + mSecurityTypeTitle + '\'' +
                ", mRiskTypeHolder=" + mRiskTypeHolder +
                ", spinnerPosition=" + spinnerPosition +
                ", reasonTxt='" + reasonTxt + '\'' +
                ", editFieldVisible=" + editFieldVisible +
                ", pos=" + pos +
                '}';
    }
}
