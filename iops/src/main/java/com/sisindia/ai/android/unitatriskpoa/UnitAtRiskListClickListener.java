package com.sisindia.ai.android.unitatriskpoa;

import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;

/**
 * Created by shankar on 19/7/16.
 */

public interface UnitAtRiskListClickListener {
    void onItemClick(RiskDetailModelMO item);
}

