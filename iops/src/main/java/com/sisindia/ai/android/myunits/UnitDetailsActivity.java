package com.sisindia.ai.android.myunits;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.myunits.models.PostFragmentUpdateModel;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.R.mipmap.rota;

public class UnitDetailsActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener,
        UnitDetailsFragmentListener {

    @Bind(R.id.unitDetailsTabLayout)
    TabLayout checkingTabLayout;
    @Bind(R.id.unitDetailsViewpager)
    ViewPager viewPager;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    ArrayList<String> viewPagerData;
    private Resources resources;
    private ArrayList<String> viewPagerTabTitles;
    private FloatingActionButton addPostFloatingButton;

    private UnitDetailsViewpageAdapter unitDetailsViewpageAdapter;
    private ColorStateList colorStateList;
    private FragmentManager fragmentManager;
    private CoordinatorLayout coordinatorLayout;
    private UpdatePostTabListener mListener;
    private MyUnitHomeMO unitDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_details);
        ButterKnife.bind(this);
        resources = getResources();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        viewPagerTabTitles = new ArrayList<String>();
        viewPagerTabTitles.add(resources.getString(R.string.UNITDETAILS_GENERAL_TAB));
        viewPagerTabTitles.add(resources.getString(R.string.UNITDETAILS_STRENGTH_TAB));
        viewPagerTabTitles.add(resources.getString(R.string.UNITDETAILS_POSTS_TAB));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.UNIT_DETAILS_TITLE);
        setupViewPager(viewPager, new ArrayList<String>());
        checkingTabLayout.setupWithViewPager(viewPager);
        colorStateList = ContextCompat.getColorStateList(this, R.drawable.tab_label_indicator);
        checkingTabLayout.setTabTextColors(colorStateList);
        addPostFloatingButton = (FloatingActionButton) findViewById(R.id.post_add_floatting_button);
        addPostFloatingButton.setOnClickListener(this);
        //getUnitTaskModel();
    }

    private void getUnitTaskModel() {
        appPreferences = new AppPreferences(this);
        unitDetails = (MyUnitHomeMO) SingletonUnitDetails.getInstance().getUnitDetails();
        if (unitDetails != null) {
            if (unitDetails.getUnitId() == -1 || unitDetails.getUnitId() == -2) {
                addPostFloatingButton.setVisibility(View.GONE);
            } else {
                addPostFloatingButton.setVisibility(View.VISIBLE);
            }
        }
    }


    private void setupViewPager(ViewPager viewPager, ArrayList<String> PostData) {
        unitDetailsViewpageAdapter = new UnitDetailsViewpageAdapter(getSupportFragmentManager());
        unitDetailsViewpageAdapter.addFragment(new UnitDetailsGeneralFragment(), resources.getString(R.string.UNITDETAILS_GENERAL_TAB));
        unitDetailsViewpageAdapter.addFragment(new UnitDetailsStrengthFragment(), resources.getString(R.string.UNITDETAILS_STRENGTH_TAB));
        UnitDetailsPostFragment unitDetailsPostFragment = UnitDetailsPostFragment.newInstance("", 0);
        unitDetailsViewpageAdapter.addFragment(unitDetailsPostFragment, resources.getString(R.string.UNITDETAILS_POSTS_TAB));
        viewPager.setAdapter(unitDetailsViewpageAdapter);
        viewPager.addOnPageChangeListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.post_add_floatting_button:

                PostData postdata = null;
                Intent addPostIntent = new Intent(UnitDetailsActivity.this, UnitPostAddEditActivity.class);
                addPostIntent.putExtra(getResources().getString(R.string.post_action_type),
                        getResources().getString(R.string.post_new));
                addPostIntent.putExtra(getResources().getString(R.string.post_entity), postdata);
                PostFragmentUpdateModel.getInstance().setmListener(mListener);
                startActivityForResult(addPostIntent, Constants.MYUNITS_ADD_POST_REQUEST_CODE);


        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == Constants.NUMBER_TWO)
            addPostFloatingButton.setVisibility(View.VISIBLE);
        else
            addPostFloatingButton.setVisibility(View.GONE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void updatePostData(String postName) {

        checkingTabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        addPostFloatingButton.setVisibility(View.VISIBLE);
        toolbar.setVisibility(View.VISIBLE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.MYUNITS_ADD_POST_REQUEST_CODE == requestCode) {
            showprogressbar();
            // Execute some code after 2 seconds have passed
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ArrayList<Object> addPostData = new ArrayList<Object>();
                    if (data != null) {
                        addPostData.add(data.getExtras().getSerializable("postName"));
                        viewPager.setCurrentItem(2);
                        UnitDetailsPostFragment.newInstance("", 0).updatePostTab();
                        //PostFragmentUpdateModel.getInstance().getmListener().updatePostTab();
                    }
                    hideprogressbar();
                }
            }, 2000);

        } else if (Constants.MYUNITS_ADD_CONTACTS_REQUEST_CODE == requestCode) {
            if (data != null) {
                viewPager.setCurrentItem(0);
                UnitDetailsGeneralFragment.newInstance("", "");
            }
        }


    }


    private void showprogressbar() {
        Util.showProgressBar(this, R.string.loading_please_wait);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }
}
