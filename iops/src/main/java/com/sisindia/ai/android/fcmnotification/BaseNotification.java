package com.sisindia.ai.android.fcmnotification;

import com.google.gson.annotations.SerializedName;

/**
 * Created by compass on 6/22/2017.
 */

public class BaseNotification {
    @SerializedName("notification")
    NotificationReceivingMO notificationReceivingMO;

    public NotificationReceivingMO getNotificationReceivingMO()
    {
        return notificationReceivingMO;
    }

    public void setNotificationReceivingMO(NotificationReceivingMO notificationReceivingMO) {
        this.notificationReceivingMO = notificationReceivingMO;
    }
}
