package com.sisindia.ai.android.database.issueDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.complaints.ComplaintsData;
import com.sisindia.ai.android.issues.grievances.GrievanceData;
import com.sisindia.ai.android.issues.improvements.ImprovementPlanMO;
import com.sisindia.ai.android.issues.models.ActionPlanCount;
import com.sisindia.ai.android.issues.models.ComplaintCount;
import com.sisindia.ai.android.issues.models.GrievanceCount;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.ImprovementPlanDatesComparator;
import com.sisindia.ai.android.utils.IssueDatesComparator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 28/6/16.
 */

public class IssueLevelSelectionStatementDB extends SISAITrackingDB {
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public IssueLevelSelectionStatementDB(Context context) {
        super(context);
    }


    public GrievanceData getGrievanceList(int issueTypeId) {

        int openIssuesCount = 0;
        GrievanceData grievanceData = new GrievanceData();
        ArrayList<Object> issuesMOList = new ArrayList<>();
        ArrayList<Object> closedGrievanceList = new ArrayList<>();

        /*String issueQuery = "select i." + TableNameAndColumnStatement.ID + " AS " + " Id ,"
                + " u." + TableNameAndColumnStatement.UNIT_NAME + " AS " + " Name ,"
                + " l." + TableNameAndColumnStatement.LOOKUP_NAME + " AS " + " Nature_of_Grievance ,"
                + " l1." + TableNameAndColumnStatement.LOOKUP_NAME + " AS " + " Grievance_status ,"
                + " i." + TableNameAndColumnStatement.CLOSURE_END_DATE + " AS " + " Closure_Date ,"
                + " i." + TableNameAndColumnStatement.ISSUE_ID + " AS issue_id,"
                + " i." + TableNameAndColumnStatement.TARGET_ACTION_END_DATE + " AS " + " Action_Date ,"
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + " AS " + " Assigned_Name ,"
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_ID + " AS " + " Assigned_Id ,"
                + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + " AS " + " Created_Date_Time ,"
                + " i." + TableNameAndColumnStatement.ISSUE_NATURE_ID + " AS " + " Issue_nature_id ,"
                + " i." + TableNameAndColumnStatement.ISSUE_STATUS_ID + " AS " + " Issue_Status_id, "
                + " i." + TableNameAndColumnStatement.GUARD_CODE + " AS " + " Guard_Code ,"
                + " i." + TableNameAndColumnStatement.GUARD_NAME + " AS " + " Guard_Name ,"
                + " i." + TableNameAndColumnStatement.ISSUE_STATUS_ID + " AS " + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + ", ue." + TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + " AS " + " GUARD_FULL_NAME "
                + " from " + TableNameAndColumnStatement.UNIT_TABLE
                + " u inner join  " + TableNameAndColumnStatement.ISSUES_TABLE
                + " i  on  u." + TableNameAndColumnStatement.UNIT_ID
                + " = i." + TableNameAndColumnStatement.UNIT_ID
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l  on i." + TableNameAndColumnStatement.ISSUE_NATURE_ID
                + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 18 "
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l1 on i." + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + " = l1." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l1." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 23"
                + " inner join " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE
                + " ue on ue." + TableNameAndColumnStatement.EMPLOYEE_ID
                + " = i." + TableNameAndColumnStatement.GUARD_ID
                + " where i." + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = 1" +
                " union "

                + "select i." + TableNameAndColumnStatement.ID + " AS " + " Id ,"
                + " b." + TableNameAndColumnStatement.BARRACK_NAME + " AS " + " Name ,"
                + " l." + TableNameAndColumnStatement.LOOKUP_NAME + " AS " + " Nature_of_Grievance ,"
                + " l1." + TableNameAndColumnStatement.LOOKUP_NAME + " AS " + " Grievance_status ,"
                + " i." + TableNameAndColumnStatement.CLOSURE_END_DATE + " AS " + " Closure_Date ,"
                + " i." + TableNameAndColumnStatement.ISSUE_ID + "  AS issue_id ,"
                + " i." + TableNameAndColumnStatement.TARGET_ACTION_END_DATE + " AS " + " Action_Date ,"
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + " AS " + " Assigned_Name ,"
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_ID + " AS " + " Assigned_Id ,"
                + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + " AS " + " Created_Date_Time ,"
                + " i." + TableNameAndColumnStatement.ISSUE_NATURE_ID + " AS " + " Issue_nature_id ,"
                + " i." + TableNameAndColumnStatement.ISSUE_STATUS_ID + " AS " + " Issue_Status_id ,"
                + " i." + TableNameAndColumnStatement.GUARD_CODE + " AS " + " Guard_Code ,"
                + " i." + TableNameAndColumnStatement.GUARD_NAME + " AS " + " Guard_Name ,"
                + " i." + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + " , ue." + TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + " AS " + " GUARD_FULL_NAME "
                + " from " + TableNameAndColumnStatement.BARRACK_TABLE
                + " b inner join  " + TableNameAndColumnStatement.ISSUES_TABLE
                + " i  on  b." + TableNameAndColumnStatement.BARRACK_ID
                + " = i." + TableNameAndColumnStatement.BARRACK_ID
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l  on i." + TableNameAndColumnStatement.ISSUE_NATURE_ID
                + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 18 "
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l1 on i." + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + " = l1." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l1." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 23"
                + " inner join " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE
                + " ue on ue." + TableNameAndColumnStatement.EMPLOYEE_ID
                + " = i." + TableNameAndColumnStatement.GUARD_ID
                + " where i." + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = 1"
                + " order by  " + TableNameAndColumnStatement.ISSUE_STATUS_ID;*/

        String issueQuery = "select i.id AS Id , u.unit_name AS Name , l.lookup_name AS Nature_of_Grievance , l1.lookup_name AS Grievance_status , i.closure_end_date AS Closure_Date , i.issue_id AS issue_id, i.target_action_end_date AS Action_Date , i.assigned_to_name AS Assigned_Name , i.assigned_to_id AS Assigned_Id , i.CreatedDateTime AS Created_Date_Time , i.issue_nature_id AS Issue_nature_id , i.issue_status_id AS Issue_Status_id, i.guard_code AS Guard_Code , i.guard_name AS Guard_Name , " +
                "i.issue_status_id AS issue_status_id from unit u inner join issues i on u.unit_id = i.unit_id inner join lookup_table l on i.issue_nature_id = l.lookup_identifier " +
                "and l.lookup_type_id = 18 inner join lookup_table l1 on i.issue_status_id = l1.lookup_identifier and l1.lookup_type_id = 23 where i.issue_type_id = 1 and " +
                "Issue_category_Id not in (13)";

        Timber.i("Grievance Query : %s " + issueQuery);
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(issueQuery, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        IssuesMO issuesMO = new IssuesMO();
                        issuesMO.setpId(cursor.getInt(cursor.getColumnIndex("Id")));
                        issuesMO.setUnitName(cursor.getString(cursor.getColumnIndex("Name")));
                        issuesMO.setNatureOfGrievance(cursor.getString(cursor.getColumnIndex("Nature_of_Grievance")));
                        issuesMO.setStatus(cursor.getString(cursor.getColumnIndex("Grievance_status")));
                        issuesMO.setIssueStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_STATUS_ID)));
                        issuesMO.setClosureEndDate(cursor.getString(cursor.getColumnIndex("Closure_Date")));
                        issuesMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex("Created_Date_Time")));
                        issuesMO.setTargetActionEndDate(cursor.getString(cursor.getColumnIndex("Action_Date")));
                        if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex("Guard_Name")))) {
//                            issuesMO.setGuardName(cursor.getString(cursor.getColumnIndex("GUARD_FULL_NAME")));
                            issuesMO.setGuardName("NA");
                        } else {
                            issuesMO.setGuardName(cursor.getString(cursor.getColumnIndex("Guard_Name")));
                        }

                        issuesMO.setAssignedToName(cursor.getString(cursor.getColumnIndex("Assigned_Name")));
                        issuesMO.setAssignedToId(cursor.getInt(cursor.getColumnIndex("Assigned_Id")));
                        //issuesMO.setEmployeeId(cursor.getString(cursor.getColumnIndex("Guard_Code"))); due to this crashes happens in some situations so i(prasad) commented this filed
                        issuesMO.setGuardCode(cursor.getString(cursor.getColumnIndex("Guard_Code")));
                        issuesMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));

                        if (issuesMO.getIssueStatusId() == 3) {
                            closedGrievanceList.add(issuesMO);
                        } else {
                            issuesMOList.add(issuesMO);
                        }
                        if (issuesMO.getIssueStatusId() == 1 || issuesMO.getIssueStatusId() == 2) {
                            openIssuesCount = openIssuesCount + 1;
                        }

                    } while (cursor.moveToNext());
                }


            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        Collections.sort(closedGrievanceList, new IssueDatesComparator());
        Collections.sort(issuesMOList, new IssueDatesComparator());
        issuesMOList.addAll(closedGrievanceList);

        grievanceData.setGrievanceList(issuesMOList);
        grievanceData.setOpenIssueCount(openIssuesCount);
        return grievanceData;
    }

    public ComplaintsData getComplaintsList() {
        /**
         *  Adding the improvement plan
         * @param lookupTypeName
         * @param lookUpCode
         * @return
         */

        ArrayList<IssuesMO> issuesMOList = new ArrayList<>();
        ArrayList<IssuesMO> closedComplaintsList = new ArrayList<>();
        ComplaintsData complaintsData = new ComplaintsData();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        int openIssuesCount = 0;

        String issueQuery = "select u." + TableNameAndColumnStatement.UNIT_NAME + ","
                + " l." + TableNameAndColumnStatement.LOOKUP_NAME + " AS NatureOfComplaint,"
                + " l1." + TableNameAndColumnStatement.LOOKUP_NAME + " AS ComplaintStatus,"
                + " l2." + TableNameAndColumnStatement.LOOKUP_NAME + " AS ModeOfComplaint,"
                + " l3." + TableNameAndColumnStatement.LOOKUP_NAME + " AS CauseOfComplaint,"
                + " i." + TableNameAndColumnStatement.CLOSURE_END_DATE + ","
                + " i." + TableNameAndColumnStatement.ISSUE_ID + " AS issue_id,"
                + " i." + TableNameAndColumnStatement.TARGET_ACTION_END_DATE + ","
                + " uc." + TableNameAndColumnStatement.FIRST_NAME + ","
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ","
                + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + ","
                + " i." + TableNameAndColumnStatement.ISSUE_NATURE_ID + ","
                + " i." + TableNameAndColumnStatement.ACTION_PLAN_ID + ","
                + " i." + TableNameAndColumnStatement.ISSUE_CAUSE_ID + ","
                + " i." + TableNameAndColumnStatement.REMARKS + ","
                + " i." + TableNameAndColumnStatement.ID + ","
                + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + ","
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_ID + ","
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ", "
                + " i." + TableNameAndColumnStatement.ISSUE_STATUS_ID + " AS " + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + " from " + TableNameAndColumnStatement.UNIT_TABLE
                + " u inner join  " + TableNameAndColumnStatement.ISSUES_TABLE
                + " i  on  u." + TableNameAndColumnStatement.UNIT_ID
                + " = i." + TableNameAndColumnStatement.UNIT_ID
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l  on i." + TableNameAndColumnStatement.ISSUE_NATURE_ID
                + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 16"
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l1 on i." + TableNameAndColumnStatement.ISSUE_STATUS_ID
                + " = l1." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l1." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 23"
                + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l2 on i." + TableNameAndColumnStatement.ISSUE_MODE_ID
                + " = l2." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l2." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 21"
                + " left join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " l3 on i." + TableNameAndColumnStatement.ISSUE_CAUSE_ID
                + " = l3." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                + " and l3." + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                + " = 17"
                + " left outer join " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE
                + " uc on i." + TableNameAndColumnStatement.UNIT_CONTACT_ID
                + " =  uc." + TableNameAndColumnStatement.UNIT_CONTACT_ID
                + " where i." + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = 2 "
                + " order by  " + TableNameAndColumnStatement.ISSUE_STATUS_ID + " desc ";


        Timber.i("Complaints Query : %s " + issueQuery);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(issueQuery, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        IssuesMO issuesMO = new IssuesMO();

                        issuesMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        issuesMO.setActionPlanId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        issuesMO.setNatureOfComplaint(cursor.getString(cursor.getColumnIndex("NatureOfComplaint")));
                        issuesMO.setModeOfComplaint(cursor.getString(cursor.getColumnIndex("ModeOfComplaint")));
                        issuesMO.setCauseOfComplaint(cursor.getString(cursor.getColumnIndex("CauseOfComplaint")));
                        issuesMO.setStatus(cursor.getString(cursor.getColumnIndex("ComplaintStatus")));
                        issuesMO.setIssueStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_STATUS_ID)));
                        issuesMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        issuesMO.setUnitContactName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)));
                        issuesMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        issuesMO.setRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        issuesMO.setTargetActionEndDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_ACTION_END_DATE)));
                        issuesMO.setAssignedToName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        issuesMO.setAssignedToId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)));
                        issuesMO.setpId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        issuesMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));

                        if (issuesMO.getIssueStatusId() == 1 || issuesMO.getIssueStatusId() == 2) {
                            openIssuesCount = openIssuesCount + 1;
                        }

                        if (issuesMO.getIssueStatusId() == 3) {
                            closedComplaintsList.add(issuesMO);
                        } else {
                            issuesMOList.add(issuesMO);
                        }


                    } while (cursor.moveToNext());
                }


            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        Collections.sort(closedComplaintsList, new IssueDatesComparator());
        Collections.sort(issuesMOList, new IssueDatesComparator());
        issuesMOList.addAll(closedComplaintsList);

        complaintsData.setOpenComplaintsCount(openIssuesCount);
        complaintsData.setComplaintsList(issuesMOList);

        return complaintsData;
    }

    public ArrayList<LookupModel> getImprovementLookupData(String lookupTypeName, int lookUpCode) {

        ArrayList<LookupModel> lookupModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query = "select " + TableNameAndColumnStatement.LOOKUP_TYPE_ID + ","
                + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + ","
                + TableNameAndColumnStatement.LOOKUP_NAME + ","
                + TableNameAndColumnStatement.ID
                + " from " +
                TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " where "
                + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                + " = '" + lookupTypeName + "' AND "
                + TableNameAndColumnStatement.LOOKUP_CODE
                + " = '" + lookUpCode + "'";

        Timber.d("getLookupData Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        LookupModel lookupModel = new LookupModel();
                        lookupModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        lookupModel.setLookupIdentifier(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER)));
                        lookupModel.setLookupTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_ID)));
                        //lookupModel.setLookupTypeName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_NAME))));
                        lookupModel.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME)));
                        lookupModelList.add(lookupModel);
                    }
                    while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }

        }
        return lookupModelList;


    }

    public ArrayList<ImprovementActionPlanMO> getImprovementActionPlan(int unitId,
                                                                       int issuetypeId,
                                                                       int issuecategoryId,
                                                                       int issueSubCategoryId,
                                                                       boolean fromBarrackInspection, boolean fromAPICall) {

        ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query;
        if (fromBarrackInspection || fromAPICall) {
            /*query = "select " + "ap." + TableNameAndColumnStatement.NAME + ","
                    + "ap." + TableNameAndColumnStatement.ID + " as ACTION_PLAN_ID ,"
                    + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME
                    + ", im." + TableNameAndColumnStatement.ID + " AS " +
                    TableNameAndColumnStatement.ISSUE_MATRIX_ID
                    + " from " +
                    TableNameAndColumnStatement.ISSUE_MATRIX_TABLE +
                    " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                    "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                    TableNameAndColumnStatement.ID + " where im."
                    + TableNameAndColumnStatement.ISSUE_TYPE_ID
                    + " = '" + issuetypeId + "' AND im."
                    + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                    + " = '" + issuecategoryId + "' AND im."
                    + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                    + " = '" + issueSubCategoryId + "' AND im."
                    + TableNameAndColumnStatement.UNIT_TYPE_ID
                    + " is null";*/

            if (issuecategoryId == 13) {

                query = "select " + "ap." + TableNameAndColumnStatement.NAME + "," + "ap." + TableNameAndColumnStatement.ID + " as ACTION_PLAN_ID ,"
                        + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME + ", im." + TableNameAndColumnStatement.ID + " AS " +
                        TableNameAndColumnStatement.ISSUE_MATRIX_ID + " from " +
                        TableNameAndColumnStatement.ISSUE_MATRIX_TABLE + " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                        "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                        TableNameAndColumnStatement.ID + " where im." + TableNameAndColumnStatement.ISSUE_TYPE_ID
                        + " = '" + issuetypeId + "' AND im." + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                        + " = '" + issuecategoryId + "' AND im." + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                        + " = '" + issueSubCategoryId + "' group by ap.Name,ap.id ,im.turn_around_time";

            } else {
                query = "select " + "ap." + TableNameAndColumnStatement.NAME + "," + "ap." + TableNameAndColumnStatement.ID + " as ACTION_PLAN_ID ,"
                        + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME + ", im." + TableNameAndColumnStatement.ID + " AS " +
                        TableNameAndColumnStatement.ISSUE_MATRIX_ID + " from " +
                        TableNameAndColumnStatement.ISSUE_MATRIX_TABLE + " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                        "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                        TableNameAndColumnStatement.ID + " where im." + TableNameAndColumnStatement.ISSUE_TYPE_ID
                        + " = '" + issuetypeId + "' AND im." + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                        + " = '" + issuecategoryId + "' AND im." + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                        + " = '" + issueSubCategoryId + "' AND im." + TableNameAndColumnStatement.UNIT_TYPE_ID + " is null";
            }
        } else {
            query = "select " + "ap." + TableNameAndColumnStatement.NAME + "," + "ap." + TableNameAndColumnStatement.ID + " as ACTION_PLAN_ID ,"
                    + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME + ", im." + TableNameAndColumnStatement.ID + " AS " +
                    TableNameAndColumnStatement.ISSUE_MATRIX_ID + " from " +
                    TableNameAndColumnStatement.ISSUE_MATRIX_TABLE + " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                    "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                    TableNameAndColumnStatement.ID + " inner join " + TableNameAndColumnStatement.UNIT_TABLE + " u on u." +
                    TableNameAndColumnStatement.UNIT_TYPE_ID + " = im." + TableNameAndColumnStatement.UNIT_TYPE_ID + " and u." +
                    TableNameAndColumnStatement.UNIT_ID + " = " + unitId + " where im."
                    + TableNameAndColumnStatement.ISSUE_TYPE_ID + " = '" + issuetypeId + "' AND im."
                    + TableNameAndColumnStatement.ISSUE_CATEGORY_ID + " = '" + issuecategoryId + "' AND im."
                    + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID + " = '" + issueSubCategoryId + "'";
        }

        Timber.d("getImprovementActionPlan Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
                        improvementActionPlanMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.action_plan_id)));
                        improvementActionPlanMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementActionPlanMO.setName(cursor.getString((cursor.getColumnIndex(TableNameAndColumnStatement.NAME))));
                        improvementActionPlanMO.setTurnAroundTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TURN_AROUND_TIME)));
                        improvementActionPlanList.add(improvementActionPlanMO);
                    }
                    while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
        if (improvementActionPlanList.size() == 0) {


            improvementActionPlanMO.setName("No Action Plan Available");

        } else {
            improvementActionPlanMO.setName("Select Action Plan");
        }

        improvementActionPlanList.add(0, improvementActionPlanMO);
        return improvementActionPlanList;
    }

    public String getActionPlanName(int actionPlanId) {
        String actionPlanName = null;
        String query = "select map.Name from issue_matrix im inner join master_action_plan map on im.master_action_plan_id = map.id  where im.master_action_plan_id = " + actionPlanId;
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        actionPlanName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NAME));
                    }
                    while (cursor.moveToNext());
                }


            }

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        return actionPlanName;
    }

    public List<ImprovementActionPlanMO> getClientImprovementPlanList(int unitId,
                                                                      int issueTypeId, int issueCategoryId) {

        List<ImprovementActionPlanMO> improvementActionPlanMOList = new ArrayList<>();
        SQLiteDatabase sqlite = null;

        String query = "select " + "ap." + TableNameAndColumnStatement.NAME + ","
                + "ap." + TableNameAndColumnStatement.ID + " as " + TableNameAndColumnStatement.action_plan_id + ","
                + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME
                + ", im." + TableNameAndColumnStatement.ID + " AS " +
                TableNameAndColumnStatement.ISSUE_MATRIX_ID
                + " from " +
                TableNameAndColumnStatement.ISSUE_MATRIX_TABLE +
                " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                TableNameAndColumnStatement.ID + " inner join " +
                TableNameAndColumnStatement.UNIT_TABLE + " u on u." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " = im." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " and u." +
                TableNameAndColumnStatement.UNIT_ID + " = " + unitId
                + " where im."
                + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = '" + issueTypeId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                + " = '" + issueCategoryId + "'";

        Timber.d("getClientImprovementActionPlan Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
                        improvementActionPlanMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.action_plan_id)));
                        improvementActionPlanMO.setName(cursor.getString((cursor.getColumnIndex(TableNameAndColumnStatement.NAME))));
                        improvementActionPlanMO.setTurnAroundTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TURN_AROUND_TIME)));
                        improvementActionPlanMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementActionPlanMOList.add(improvementActionPlanMO);
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        return improvementActionPlanMOList;
    }


    public ImprovementPlanMO getImprovementPlanList() {

        ArrayList<Object> issuesMOList = new ArrayList<>();
        ArrayList<Object> closedImprovementList = new ArrayList<>();
        int openIssuesCount = 0;
        ImprovementPlanMO improvemenPlanMo = new ImprovementPlanMO();


        String issueQuery = "select u." + TableNameAndColumnStatement.UNIT_NAME + ", b." +
                TableNameAndColumnStatement.BARRACK_NAME + ","
                + " lkAps." + TableNameAndColumnStatement.LOOKUP_NAME + " AS "
                + TableNameAndColumnStatement.IMPROVEMENT_STATUS + ","
                + " lkIPC." + TableNameAndColumnStatement.LOOKUP_NAME + " AS "
                + TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY + "," +
                "lkCat.lookup_name as source_type,"
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_ID + ","
                + " i." + TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + ","
                + " i." + TableNameAndColumnStatement.REMARKS + ","
                + " i." + TableNameAndColumnStatement.ACTION_PLAN_ID + ","
                + " i." + TableNameAndColumnStatement.BUDGET + ","
                + " i." + TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID + ","
                + " i." + TableNameAndColumnStatement.ID + ","
                + " i." + TableNameAndColumnStatement.CLOSURE_DATE_TIME + ","
                + " i." + TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE + ","
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ","
                + " i." + TableNameAndColumnStatement.ISSUE_MATRIX_ID + ","
                + " i." + TableNameAndColumnStatement.UNIT_ID + ","
                + " i." + TableNameAndColumnStatement.BARRACK_ID + ","
                + " i." + TableNameAndColumnStatement.ISSUE_ID + ","
                + " i." + TableNameAndColumnStatement.TARGET_EMPLOYEE_ID + ","
                + " i." + TableNameAndColumnStatement.CLOSED_BY_ID + ","
                + " i." + TableNameAndColumnStatement.CLOSURE_COMMENT + ","
                + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ","
                + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + ","
                + " map." + TableNameAndColumnStatement.NAME + ","
                + " i." + TableNameAndColumnStatement.STATUS_ID
                + " from " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE
                + " i left outer join  " + TableNameAndColumnStatement.UNIT_TABLE
                + " u  on  u." + TableNameAndColumnStatement.UNIT_ID
                + " = i." + TableNameAndColumnStatement.UNIT_ID
                + " inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE
                + " map on i." + TableNameAndColumnStatement.ACTION_PLAN_ID
                + " = map." + TableNameAndColumnStatement.ID
                + " left join " + TableNameAndColumnStatement.BARRACK_TABLE
                + " b on b." + TableNameAndColumnStatement.BARRACK_ID
                + " = i." + TableNameAndColumnStatement.BARRACK_ID +
                "  left join " +
                " (Select l.lookup_name , l.lookup_identifier from lookup_table l where l.lookup_type_name = 'ActionPlanStatus') lkAps on lkAps.lookup_identifier = i.status_id LEFT JOIN" +
                " (Select l.lookup_name, l.lookup_identifier, l.lookup_code from [lookup_table] l where l.lookup_type_name = 'ImprovementPlanCategory') lkIPC on lkIPC.lookup_identifier = i.improvement_plan_category_id LEFT JOIN" +
                " (Select l.lookup_name, l.lookup_identifier as lookup_source_type from lookup_table l where l.lookup_type_name = 'IssueCategory') lkCat on lkCat.lookup_source_type = lkIPC.lookup_code" +
                " where i. " + TableNameAndColumnStatement.STATUS_ID + " in (1,2,3,4) "
                + " order by  i." + TableNameAndColumnStatement.STATUS_ID + " desc ";
//

        Timber.i("ImprovementPlanList Query : %s " + issueQuery);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(issueQuery, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {

                        ImprovementPlanListMO improvementMO = new ImprovementPlanListMO();
                        if (null != cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.Source_type))) {
                            improvementMO.setSourceType(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.Source_type)));

                        } else if (null != cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME))) {
                            improvementMO.setSourceType(TableNameAndColumnStatement.unit);
                        } else {
                            improvementMO.setSourceType(TableNameAndColumnStatement.barrack);
                        }
                        improvementMO.setBudget(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BUDGET)));
                        improvementMO.setRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        improvementMO.setSeqId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        improvementMO.setImprovementPlanCategoryId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID)));
                        improvementMO.setImprovementPlanCategoryName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY)));
                        improvementMO.setExpectedClosureDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE)));
                        improvementMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        improvementMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        improvementMO.setImprovementStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_STATUS)));
                        improvementMO.setImprovementStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.STATUS_ID)));
                        improvementMO.setClosureDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_DATE_TIME)));
                        improvementMO.setImprovementId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID)));
                        improvementMO.setAssignedToId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)));
                        improvementMO.setAssignedToName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        improvementMO.setActionPlanName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NAME)));
                        improvementMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        improvementMO.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        improvementMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementMO.setTargetEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID)));
                        improvementMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));
                        improvementMO.setClosedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSED_BY_ID)));
                        improvementMO.setClosureRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_COMMENT)));
                        improvementMO.setActionPlanId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        improvementMO.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));

                        if (improvementMO.getImprovementStatusId() == 1 || improvementMO.getImprovementStatusId() == 2) {
                            openIssuesCount = openIssuesCount + 1;
                        }

                        if (improvementMO.getImprovementStatusId() == 2) {
                            closedImprovementList.add(improvementMO);
                        } else {
                            issuesMOList.add(improvementMO);
                        }


                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
                Timber.d("ImprovementPlanList %s", IOPSApplication.getGsonInstance().toJson(issuesMOList));

            }
        }

        Collections.sort(closedImprovementList, new ImprovementPlanDatesComparator());
        Collections.sort(issuesMOList, new ImprovementPlanDatesComparator());
        issuesMOList.addAll(closedImprovementList);
        improvemenPlanMo.setOpenImporvementPlanCount(openIssuesCount);
        improvemenPlanMo.setImprovementPlanList(issuesMOList);

        return improvemenPlanMo;
    }

    public ArrayList<ImprovementActionPlanMO> getBarrackImprovementActionPlan(
            int issuetypeId, int issuecategoryId, int categorySubId) {

        ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
//        select ap.name,ap.id,im.turn_around_time from issue_matrix im
//        inner join master_action_plan ap on im.master_action_plan_id = ap.id
//        where im.issue_type_id =4
//        and im.issue_category_id = 11
//        and im.issue_sub_category_id = 5
        String query = "select " + "ap." + TableNameAndColumnStatement.NAME + ","
                + "ap." + TableNameAndColumnStatement.ID + ","
                + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME
                + ", im." + TableNameAndColumnStatement.ID + " AS " +
                TableNameAndColumnStatement.ISSUE_MATRIX_ID
                + " from " +
                TableNameAndColumnStatement.ISSUE_MATRIX_TABLE +
                " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                TableNameAndColumnStatement.ID
                + " where im."
                + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = '" + issuetypeId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                + " = '" + issuecategoryId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                + " = '" + categorySubId + "'";

        Timber.d("getBarrackImprovementActionPlan Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
                        improvementActionPlanMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        improvementActionPlanMO.setName(cursor.getString((cursor.getColumnIndex(TableNameAndColumnStatement.NAME))));
                        improvementActionPlanMO.setTurnAroundTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TURN_AROUND_TIME)));
                        improvementActionPlanMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementActionPlanList.add(improvementActionPlanMO);
                    }
                    while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
        if (improvementActionPlanList.size() == 0) {


            improvementActionPlanMO.setName("No Action Plan Available");

        } else {
            improvementActionPlanMO.setName("Select Action Plan");
        }

        improvementActionPlanList.add(0, improvementActionPlanMO);
        return improvementActionPlanList;
    }

    public ArrayList<ImprovementActionPlanMO> getImprovementActionPlanForComplaint(int unitId, int issuetypeId, int issueNatureId) {

        ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
//        select ap.name,ap.id,im.turn_around_time from issue_matrix im
//        inner join master_action_plan ap on im.master_action_plan_id = ap.id
//        inner join unit u on u.unit_type_id = im.unit_type_id
//        and u.unit_id = 538 where im.issue_type_id =4
//        and im.issue_category_id = 12
//        and im.issue_sub_category_id = 5

        String query = "select " + "ap." + TableNameAndColumnStatement.NAME + ","
              /*  + "ap." + TableNameAndColumnStatement.ID+ " as ACTION_PLAN_ID, "
                +"im."+ TableNameAndColumnStatement.ID+ " as ISSUE_MATRIX_ID, "*/
                + "ap." + TableNameAndColumnStatement.ID + " AS ACTION_PLAN_ID,"
                + "im." + TableNameAndColumnStatement.ID + " AS ISSUE_MATRIX_ID,"
                + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME
                + " from " +
                TableNameAndColumnStatement.ISSUE_MATRIX_TABLE +
                " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                TableNameAndColumnStatement.ID + " inner join " +
                TableNameAndColumnStatement.UNIT_TABLE + " u on u." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " = im." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " and u." +
                TableNameAndColumnStatement.UNIT_ID + " = " + unitId
                + " where im."
                + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = '" + issuetypeId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                + " = '" + issueNatureId + "'";

        Timber.d("getImprovementActionPlanForComplaints Query  %s", query);
        Cursor cursor = null;


        try {

            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {

                        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
                        improvementActionPlanMO.setId(cursor.getInt(cursor.getColumnIndex("ACTION_PLAN_ID")));
                        improvementActionPlanMO.setName(cursor.getString((cursor.getColumnIndex(TableNameAndColumnStatement.NAME))));
                        improvementActionPlanMO.setTurnAroundTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TURN_AROUND_TIME)));
                        improvementActionPlanMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex("ISSUE_MATRIX_ID")));
                        improvementActionPlanList.add(improvementActionPlanMO);
                    }
                    while (cursor.moveToNext());
                }

            }


        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
        if (improvementActionPlanList.size() == 0) {


            improvementActionPlanMO.setName("No Action Plan Available");

        } else {
            improvementActionPlanMO.setName("Select Action Plan");
        }

        improvementActionPlanList.add(0, improvementActionPlanMO);
        return improvementActionPlanList;
    }

    public ArrayList<ImprovementActionPlanMO> getActionPlanList(int unitId,
                                                                int issuetypeId, int issuecategoryId, int categorySubId) {

        ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query;

        query = "select " + "ap." + TableNameAndColumnStatement.NAME + ","
                + "ap." + TableNameAndColumnStatement.ID + " as ACTION_PLAN_ID ,"
                + "im." + TableNameAndColumnStatement.TURN_AROUND_TIME
                + ", im." + TableNameAndColumnStatement.ID + " AS " +
                TableNameAndColumnStatement.ISSUE_MATRIX_ID
                + " from " +
                TableNameAndColumnStatement.ISSUE_MATRIX_TABLE +
                " im inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + " ap on " +
                "im." + TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " = ap." +
                TableNameAndColumnStatement.ID + " inner join " +
                TableNameAndColumnStatement.UNIT_TABLE + " u on u." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " = im." +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " and u." +
                TableNameAndColumnStatement.UNIT_ID + " = " + unitId
                + " where im."
                + TableNameAndColumnStatement.ISSUE_TYPE_ID
                + " = '" + issuetypeId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_CATEGORY_ID
                + " = '" + issuecategoryId + "' AND im."
                + TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID
                + " = '" + categorySubId + "'";


        Timber.d("getImprovementActionPlan Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
                        improvementActionPlanMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.action_plan_id)));
                        improvementActionPlanMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementActionPlanMO.setName(cursor.getString((cursor.getColumnIndex(TableNameAndColumnStatement.NAME))));
                        improvementActionPlanMO.setTurnAroundTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TURN_AROUND_TIME)));
                        improvementActionPlanList.add(improvementActionPlanMO);
                    }
                    while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
        if (improvementActionPlanList.size() == 0) {


            improvementActionPlanMO.setName("No Action Plan Available");

        } else {
            improvementActionPlanMO.setName("Select Action Plan");
        }

        improvementActionPlanList.add(0, improvementActionPlanMO);
        return improvementActionPlanList;
    }

    /**
     * @return
     */
    public int getSequenceId(String tablename, String columnName) {
        int id = 0;
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + TableNameAndColumnStatement.ID + " from " +
                tablename +
                " where " + columnName + " = 0 order by "
                + TableNameAndColumnStatement.ID + " desc limit 1";
        Timber.d("SequencePostId updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        id = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            cursor.close();
            sqlite.close();
        }
        return id;
    }

    public Object getOpenIssuesCount(int issueTypeId) {

        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        long twoDaysMilliSec = dateTimeFormatConversionUtil.getTimeInMillisForNumberOfDays(2);
        long sevenDaysMilliSec = dateTimeFormatConversionUtil.getTimeInMillisForNumberOfDays(7);
        int lessThan2DaysCounter = 0;
        int greaterThan2DaysCounter = 0;
        int greaterThan7DaysCounter = 0;


        String issueQuery = " select CreatedDateTime from issues  where issue_type_id = " + issueTypeId + " and  " +
                " issue_status_id in (1,2) order by  CreatedDateTime desc ";

        Object object = null;


        Timber.i("Issues Query : %s " + issueQuery);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(issueQuery, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {

                        long diff = Calendar.getInstance().getTimeInMillis() -
                                dateTimeFormatConversionUtil.convertStringDateToMilliseconds
                                        (cursor.getString(cursor.getColumnIndex("CreatedDateTime")));
                        if (diff < twoDaysMilliSec) {
                            lessThan2DaysCounter++;
                        } else if (diff > twoDaysMilliSec && diff < sevenDaysMilliSec) {
                            greaterThan2DaysCounter++;
                        } else {
                            greaterThan7DaysCounter++;
                        }

                    } while (cursor.moveToNext());
                }


            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            if (issueTypeId == 1) {
                GrievanceCount grievanceCount = new GrievanceCount();
                grievanceCount.setCountLessthan2Days(lessThan2DaysCounter);
                grievanceCount.setCountMorethan2Days(greaterThan2DaysCounter);
                grievanceCount.setCountMorethan7Days(greaterThan7DaysCounter);
                object = grievanceCount;
            } else {
                ComplaintCount complaintCount = new ComplaintCount();
                complaintCount.setCountLessthan2Days(lessThan2DaysCounter);
                complaintCount.setCountMorethan2Days(greaterThan2DaysCounter);
                complaintCount.setCountMorethan7Days(greaterThan7DaysCounter);
                object = complaintCount;
            }
        }
        return object;
    }

    public ActionPlanCount getImprovementPlanOpenCount() {

        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        ActionPlanCount actionPlanCount = null;

        long twoDaysMilliSec = dateTimeFormatConversionUtil.getTimeInMillisForNumberOfDays(2);
        long sevenDaysMilliSec = dateTimeFormatConversionUtil.getTimeInMillisForNumberOfDays(7);
        int lessThan2DaysCounter = 0;
        int greaterThan2DaysCounter = 0;
        int greaterThan7DaysCounter = 0;


        String improvementPlanQuery = " select CreatedDateTime from improvement_plan  where  " +
                "status_id in (1,3) order by  CreatedDateTime desc ";


        Timber.i("Improvementplan Query : %s " + improvementPlanQuery);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(improvementPlanQuery, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        long diff = Calendar.getInstance().getTimeInMillis() -
                                dateTimeFormatConversionUtil.convertStringDateToMilliseconds
                                        (cursor.getString(cursor.getColumnIndex("CreatedDateTime")));
                        if (diff < twoDaysMilliSec) {
                            lessThan2DaysCounter++;
                        } else if (diff > twoDaysMilliSec && diff < sevenDaysMilliSec) {
                            greaterThan2DaysCounter++;
                        } else {
                            greaterThan7DaysCounter++;
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        actionPlanCount = new ActionPlanCount();
        actionPlanCount.setCountLessthan2Days(lessThan2DaysCounter);
        actionPlanCount.setCountMorethan2Days(greaterThan2DaysCounter);
        actionPlanCount.setCountMorethan7Days(greaterThan7DaysCounter);
        return actionPlanCount;
    }


}
