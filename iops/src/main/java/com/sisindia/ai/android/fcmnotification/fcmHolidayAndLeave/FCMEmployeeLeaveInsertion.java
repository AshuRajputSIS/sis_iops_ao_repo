package com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.EmployeeLeaveCalendar;

import java.util.ArrayList;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMEmployeeLeaveInsertion extends SISAITrackingDB {

    private SQLiteDatabase sqlite=null;
    private boolean isInsertedSuccessfully;

    public FCMEmployeeLeaveInsertion(Context context) {
        super(context);
    }
    public synchronized boolean insertEmployeeLeave(ArrayList<EmployeeLeaveCalendar> employeeLeaveCalendarList) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                if(employeeLeaveCalendarList != null) {
                    sqlite.beginTransaction();
                    insertEmployeeLeaveModel(employeeLeaveCalendarList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertEmployeeLeaveModel(ArrayList<EmployeeLeaveCalendar> employeeLeaveCalendarMOList) {

                if (null != employeeLeaveCalendarMOList && employeeLeaveCalendarMOList.size() != 0) {
                for(EmployeeLeaveCalendar employeeLeaveCalendar : employeeLeaveCalendarMOList){
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID, employeeLeaveCalendar.getEmployeeId());
                    values.put(TableNameAndColumnStatement.LEAVE_DATE, employeeLeaveCalendar.getLeaveDate());
                    String[] createdStringDate = employeeLeaveCalendar.getLeaveDate().split(" ");
                    values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                    GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().
                            getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE,
                            employeeLeaveCalendar.getEmployeeId(),
                            TableNameAndColumnStatement.ID
                            , TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE, TableNameAndColumnStatement.ID, sqlite);
                    if (genericUpdateTableMO.getIsAvailable() != 0) {
                        IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                deleteTableById(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE,
                                        genericUpdateTableMO.getId(),sqlite);
                        sqlite.insertOrThrow(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE, null, values);
                    } else {
                        sqlite.insertOrThrow(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE, null, values);
                    }
                }
            }
         }
}
