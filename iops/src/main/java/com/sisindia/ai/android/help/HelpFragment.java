package com.sisindia.ai.android.help;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.issues.IssuesViewPagerAdapter;
import com.sisindia.ai.android.rota.UpdateRotaTaskModelListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 26-10-2016.
 */

public class HelpFragment extends BaseFragment {

    private UpdateRotaTaskModelListener updateRotaTaskModelListener;
    private String toolbarTile;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private LocalVideoFragment localVideoFragment;
    private OnlineVideosFragment onlineVideosFragment;
    @Bind(R.id.helpTabLayout)
    TabLayout helpTabLayout;
    @Bind(R.id.help_viewpager)
    ViewPager helpViewpager;
    private Resources mResources;
    private ColorStateList colorStateList;

    public static HelpFragment newInstance(String toolbarTitle) {
        HelpFragment helpFragment = new HelpFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", toolbarTitle);
        helpFragment.setArguments(bundle);
        return helpFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            toolbarTile = getArguments().getString("title");
        }
        colorStateList = ContextCompat.getColorStateList(getActivity(), R.drawable.tab_label_indicator);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        /*if (context instanceof UpdateRotaTaskModelListener)
            updateRotaTaskModelListener = (UpdateRotaTaskModelListener) context;*/


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(toolbarTile);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.help_fragment, container, false);
        mResources = getResources();
        ButterKnife.bind(this, view);
        setupViewPager();

        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.help_fragment;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void setupViewPager() {
        IssuesViewPagerAdapter issuesViewPagerAdapter = new IssuesViewPagerAdapter(getChildFragmentManager());

        localVideoFragment = LocalVideoFragment.newInstance();
        onlineVideosFragment = OnlineVideosFragment.newInstance();
        issuesViewPagerAdapter.addFragment(localVideoFragment, getString(R.string.help_local_video_tab));
        issuesViewPagerAdapter.addFragment(onlineVideosFragment, getString(R.string.help_online_video_tab));
        helpViewpager.setAdapter(issuesViewPagerAdapter);
        helpTabLayout.setupWithViewPager(helpViewpager);
        //issuesTabLayout.setOnTabSelectedListener(this);
        helpTabLayout.setTabTextColors(colorStateList);
    }

}
