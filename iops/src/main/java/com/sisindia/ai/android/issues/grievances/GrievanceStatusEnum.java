package com.sisindia.ai.android.issues.grievances;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 25-05-2016.
 */
public enum GrievanceStatusEnum {
    Reported(1),
    InProgress(2),
    Resolved(3),
    Verified(4);

    private static Map map = new HashMap<>();

    static {
        for (GrievanceStatusEnum grievanceStatusEnum : GrievanceStatusEnum.values()) {
            map.put(grievanceStatusEnum.value, grievanceStatusEnum);
        }
    }

    private int value;

    GrievanceStatusEnum(int i) {
        value = i;
    }

    public static GrievanceStatusEnum valueOf(int positionValue) {
        return (GrievanceStatusEnum) map.get(positionValue);
    }

    public int getValue() {
        return value;
    }
}
