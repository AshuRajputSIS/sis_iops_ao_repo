package com.sisindia.ai.android.fcmnotification.fcmrota;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.job.AddRotaTaskFromRocc;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;
import com.sisindia.ai.android.rota.models.RotaMO;
import com.sisindia.ai.android.rota.models.RotaModelMO;
import com.sisindia.ai.android.rota.models.RotaTask;
import com.sisindia.ai.android.rota.models.UnitTask;

import java.util.List;

/**
 * Created by shankar on 24/11/16.
 */

public class FCMRotaModelInsertion extends SISAITrackingDB {

    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite = null;

    public FCMRotaModelInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertRotaModel(RotaMO rotaData) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if (rotaData != null) {
                    sqlite.beginTransaction();

                    if (rotaData.getRotaModelMO() != null)
                        insertFCMRotaModel(rotaData.getRotaModelMO());

                    insertFCMTask(rotaData.getUnitTasks());
                    insertFCMRotaTask(rotaData.getRotaTasks());

                    if (rotaData.getUnitMonthlyBillDetail() != null) {
                        IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTable(TableNameAndColumnStatement.UnitBillingStatusTable, sqlite);
                        insertFCMUnitBillingData(rotaData.getUnitMonthlyBillDetail());
                    }

                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                } else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertFCMUnitBillingData(List<UnitMonthlyBillDetail> billCollactionApiMOList) {

        ContentValues values = new ContentValues();
        for (int i = 0; i < billCollactionApiMOList.size(); i++) {
            values.put(TableNameAndColumnStatement.Bill_COLLACTION_ID, billCollactionApiMOList.get(i).getId());
            values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID, billCollactionApiMOList.get(i).getUnitId());
            values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_NAME, billCollactionApiMOList.get(i).getUnitName());
            values.put(TableNameAndColumnStatement.Bill_NUMBER, billCollactionApiMOList.get(i).getBillNo());
            values.put(TableNameAndColumnStatement.Bill_MONTH, billCollactionApiMOList.get(i).getBillMonth());
            values.put(TableNameAndColumnStatement.Bill_YEAR, billCollactionApiMOList.get(i).getBillYear());
            values.put(TableNameAndColumnStatement.OUTSTANDING_AMOUNT, billCollactionApiMOList.get(i).getOutstandingAmount());
            values.put(TableNameAndColumnStatement.OUTSTANDING_DAYS, billCollactionApiMOList.get(i).getOutstandingDays());
            values.put(TableNameAndColumnStatement.UPDATED_DATES, billCollactionApiMOList.get(i).getUpdatedDate());
            values.put(TableNameAndColumnStatement.IS_BILL_COLLECTED, false);
            sqlite.insertOrThrow(TableNameAndColumnStatement.UnitBillingStatusTable, null, values);
        }
    }

    private void insertFCMTask(List<UnitTask> rotaTaskList) {

        ContentValues values = new ContentValues();
        for (UnitTask rotaTask : rotaTaskList) {
            values.put(TableNameAndColumnStatement.TASK_ID, rotaTask.getId());
            values.put(TableNameAndColumnStatement.UNIT_ID, rotaTask.getUnitId());
            values.put(TableNameAndColumnStatement.UNIT_NAME, rotaTask.getUnitName());
            values.put(TableNameAndColumnStatement.TASK_TYPE_ID, rotaTask.getTaskTypeId());
            values.put(TableNameAndColumnStatement.ASSIGNEE_ID, rotaTask.getAssigneeId());
            values.put(TableNameAndColumnStatement.ASSIGNEE_NAME, rotaTask.getAssigneeName());
            values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME, rotaTask.getEstimatedExecutionTime());
            values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME, rotaTask.getActualExecutionTime());

            if (rotaTask.getTaskStatusId() == 4)
                values.put(TableNameAndColumnStatement.TASK_STATUS_ID, 3);
            else
                values.put(TableNameAndColumnStatement.TASK_STATUS_ID, rotaTask.getTaskStatusId());

            values.put(TableNameAndColumnStatement.DESCRIPTIONS, rotaTask.getDescription());
            values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME, rotaTask.getEstimatedTaskExecutionStartDateTime());
            values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME, rotaTask.getActualTaskExecutionStartDateTime());
            values.put(TableNameAndColumnStatement.IS_AUTO, rotaTask.getIsAutoCreation());
            values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, rotaTask.getCreatedDateTime());
            values.put(TableNameAndColumnStatement.CREATED_BY_ID, rotaTask.getCreatedById());
            values.put(TableNameAndColumnStatement.CREATED_BY_NAME, rotaTask.getCreatedByName());
            values.put(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED, rotaTask.getApprovedById());
            values.put(TableNameAndColumnStatement.ASSIGNED_BY_ID, rotaTask.getAssignedById());
            values.put(TableNameAndColumnStatement.BARRACK_ID, rotaTask.getBarrackId());
            values.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT, rotaTask.getTaskExecutionResult());
            values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME, rotaTask.getEstimatedTaskExecutionEndDateTime());
            values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME, rotaTask.getActualTaskExecutionEndDateTime());
            values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, rotaTask.getUnitTypeId());
            values.put(TableNameAndColumnStatement.IS_NEW, 0);
            values.put(TableNameAndColumnStatement.IS_SYNCED, 1);

            // for start task api now as update api and to be synced in adapter
            if (rotaTask.getTaskStatusId() == 1 || rotaTask.getTaskStatusId() == 2)
                values.put(TableNameAndColumnStatement.IS_TASK_STARTED, rotaTask.getTaskStatusId());
            else
                values.put(TableNameAndColumnStatement.IS_TASK_STARTED, RotaStartTaskEnum.InActive.getValue());

            String[] createdStringDate = rotaTask.getEstimatedTaskExecutionStartDateTime().split(" ");
            values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
            values.put(TableNameAndColumnStatement.SELECT_REASON_ID, rotaTask.getSelectReasonId());
            values.put(TableNameAndColumnStatement.OTHER_TASK_REASON_ID, rotaTask.getOtherReasonId());
            GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().
                    getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.TASK_ID, rotaTask.getId(),
                    TableNameAndColumnStatement.ID, TableNameAndColumnStatement.TASK_TABLE, TableNameAndColumnStatement.ID, sqlite);
            if (genericUpdateTableMO.getIsAvailable() != 0) {
                           /* sqlite.update(TableNameAndColumnStatement.TASK_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);*/
                int deleteStatus = IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTableByIdStatus(TableNameAndColumnStatement.TASK_TABLE,
                        genericUpdateTableMO.getId(), sqlite);
                if (deleteStatus > 0) {
                    sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
                }
            } else {
                sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
            }
        }
    }

    private void insertFCMRotaTask(List<RotaTask> rotaTaskList) {

        ContentValues values = new ContentValues();
        for (RotaTask rotaTask : rotaTaskList) {
            values.put(TableNameAndColumnStatement.ROTA_ID, rotaTask.getRotaId());
            values.put(TableNameAndColumnStatement.ROTA_TASK_ID, rotaTask.getRotaTaskId());
            values.put(TableNameAndColumnStatement.TASK_ID, rotaTask.getTaskId());
            values.put(TableNameAndColumnStatement.SOURCE_GEO_LATITUDE, rotaTask.getSourceGeoLatitude());
            values.put(TableNameAndColumnStatement.DESTINATION_GEO_LATITUDE, rotaTask.getDestinationGeoLatitude());
            values.put(TableNameAndColumnStatement.DESTINATION_GEO_LONGITUDE, rotaTask.getDestinationGeoLongitude());
            values.put(TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME, rotaTask.getEstimatedTravelTime());
            values.put(TableNameAndColumnStatement.ESTIMATED_DISTANCE, rotaTask.getEstimatedDistance());
            values.put(TableNameAndColumnStatement.TOTAL_ESTIMATED_TIME, rotaTask.getTotalEstimatedTime());
            values.put(TableNameAndColumnStatement.ACTUAL_TRAVEL_TIME, rotaTask.getActualTravelTime());
            values.put(TableNameAndColumnStatement.ACTUAL_DISTANCE, rotaTask.getActualDistance());
            values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME, rotaTask.getEstimatedTaskExecutionStartTime());
            values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_END_TIME, rotaTask.getEstimatedTaskExecutionEndTime());
            values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_START_TIME, String.valueOf(rotaTask.getActualTaskExecutionStartTime()));
            values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_END_TIME, String.valueOf(rotaTask.getActualTaskExecutionEndTime()));
            values.put(TableNameAndColumnStatement.TASK_SEQUENCE_NO, rotaTask.getTaskSequenceNo());
            sqlite.insertOrThrow(TableNameAndColumnStatement.ROTA_TASK_TABLE, null, values);
        }
    }

    private void insertFCMRotaModel(List<RotaModelMO> rotaModelMOList) {
        ContentValues values = new ContentValues();

        for (RotaModelMO rotaModelMO : rotaModelMOList) {
            values.put(TableNameAndColumnStatement.EMPLOYEE_ID, rotaModelMO.getEmployeeId());
            values.put(TableNameAndColumnStatement.ROTA_ID, rotaModelMO.getRotaId());
            values.put(TableNameAndColumnStatement.ROTA_WEEK, rotaModelMO.getRotaWeek());
            values.put(TableNameAndColumnStatement.ROTA_MONTH, rotaModelMO.getRotaMonth());
            values.put(TableNameAndColumnStatement.EMPLOYEE_ID, rotaModelMO.getEmployeeId());
            values.put(TableNameAndColumnStatement.ROTA_PUBLISHED_DATE_TIME, rotaModelMO.getRotaPublishedDateTime());
            values.put(TableNameAndColumnStatement.ROTA_STATUS_ID, rotaModelMO.getRotaStatusId());
            values.put(TableNameAndColumnStatement.ROTA_YEAR, rotaModelMO.getRotaYear());
            values.put(TableNameAndColumnStatement.ROTA_DATE, rotaModelMO.getRotaDate());
            GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().
                    getTableSeqId(TableNameAndColumnStatement.ROTA_ID, rotaModelMO.getRotaId(), TableNameAndColumnStatement.ID,
                            TableNameAndColumnStatement.ROTA_TABLE, TableNameAndColumnStatement.ID, sqlite);
            if (genericUpdateTableMO.getIsAvailable() != 0) {
                IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTableById(TableNameAndColumnStatement.ROTA_TABLE, genericUpdateTableMO.getId(), sqlite);
                sqlite.insertOrThrow(TableNameAndColumnStatement.ROTA_TABLE, null, values);
            } else {
                sqlite.insertOrThrow(TableNameAndColumnStatement.ROTA_TABLE, null, values);
            }

            IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteRotaTaskTableById(TableNameAndColumnStatement.ROTA_TASK_TABLE,
                    rotaModelMO.getRotaId(), sqlite);
        }
    }

    public boolean insertFCMAddTask(AddRotaTaskFromRocc addRotaTaskInput) {
        boolean isTaskAddedSuccessfully;
        sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();

                values.put(TableNameAndColumnStatement.TASK_ID, addRotaTaskInput.getId());
                values.put(TableNameAndColumnStatement.UNIT_ID, addRotaTaskInput.getUnitId());
                values.put(TableNameAndColumnStatement.TASK_TYPE_ID, addRotaTaskInput.getTaskTypeId());
                values.put(TableNameAndColumnStatement.ASSIGNEE_ID, addRotaTaskInput.getAssigneeId());

                values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME, addRotaTaskInput.getEstimatedExecutionTime());
                values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME, addRotaTaskInput.getActualExecutionTime());
                if (addRotaTaskInput.getTaskStatusId() == 4) {
                    values.put(TableNameAndColumnStatement.TASK_STATUS_ID, 3);
                } else {
                    values.put(TableNameAndColumnStatement.TASK_STATUS_ID, addRotaTaskInput.getTaskStatusId());

                }
                // for start task api now as update api and to be synced in adapter
                if (addRotaTaskInput.getTaskStatusId() == 1 || addRotaTaskInput.getTaskStatusId() == 2) {
                    values.put(TableNameAndColumnStatement.IS_TASK_STARTED, addRotaTaskInput.getTaskStatusId());

                } else {
                    values.put(TableNameAndColumnStatement.IS_TASK_STARTED, RotaStartTaskEnum.InActive.getValue());
                }
                values.put(TableNameAndColumnStatement.DESCRIPTIONS, addRotaTaskInput.getDescription());
                values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME, addRotaTaskInput.getEstimatedTaskExecutionStartDateTime());
                values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME, addRotaTaskInput.getActualTaskExecutionStartDateTime());
                if (addRotaTaskInput.getAutoCreation()) {
                    values.put(TableNameAndColumnStatement.IS_AUTO, 1);
                } else {
                    values.put(TableNameAndColumnStatement.IS_AUTO, 0);
                }

                values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, addRotaTaskInput.getCreatedDateTime());
                values.put(TableNameAndColumnStatement.CREATED_BY_ID, addRotaTaskInput.getCreatedById());

                values.put(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED, addRotaTaskInput.getApprovedById());
                values.put(TableNameAndColumnStatement.ASSIGNED_BY_ID, addRotaTaskInput.getAssignedById());
                values.put(TableNameAndColumnStatement.BARRACK_ID, addRotaTaskInput.getBarrackId());
                values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME, addRotaTaskInput.getEstimatedTaskExecutionEndDateTime());
                values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME, addRotaTaskInput.getActualTaskExecutionEndDateTime());

                values.put(TableNameAndColumnStatement.IS_NEW, 0);
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);

                String[] createdStringDate = addRotaTaskInput.getEstimatedTaskExecutionStartDateTime().split(" ");
                values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                //values.put(TableNameAndColumnStatement.SELECT_REASON_ID, addRotaTaskInput.getSelectReasonId());
                values.put(TableNameAndColumnStatement.OTHER_TASK_REASON_ID, addRotaTaskInput.getOtherReasonId());
                sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
                sqlite.setTransactionSuccessful();
                isTaskAddedSuccessfully = true;

            } catch (Exception exception) {
                isTaskAddedSuccessfully = false;
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return isTaskAddedSuccessfully;
    }

}