package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.UnitPostIR;
import com.sisindia.ai.android.network.response.UnitAddPostResponse;
import com.sisindia.ai.android.unitraising.UnitRaisingDeferredOrCancelledMo;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingDeferOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;




public class UnitRaisingDeferredSync extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, UnitRaisingDeferOR> unitRaisingDeferredList;
    private AppPreferences appPreferences;

    public UnitRaisingDeferredSync(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mcontext);
        //  unitEquipmentSync = new UnitEquipmentSyncing(mContext);
        unitRaisingDeferredSyncing();
    }

    private  void unitRaisingDeferredSyncing() {
        unitRaisingDeferredList = getUnitRaisingDeferredDetails();
        if (unitRaisingDeferredList != null && unitRaisingDeferredList.size() != 0) {
            for (Integer key : unitRaisingDeferredList.keySet()) {
                UnitRaisingDeferOR unitRaisingDeferOR =
                        unitRaisingDeferredList.get(key);
                unitRaisingDeferredApiCall(unitRaisingDeferOR, key);
                Timber.d("unitRaisingDeferredSyncing   %s", "key: " + key + " value: " +
                        unitRaisingDeferredList.get(key));
            }

        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingDeferredSyncing -->nothing to sync ");
            dependentFailureSync();
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                Timber.d(Constants.TAG_SYNCADAPTER+" UnitRaisingDeferredSync # dependentFailureSync --> "+dependentSyncsStatus.unitRaisingDeferredId );
                if(dependentSyncsStatus != null && dependentSyncsStatus.unitRaisingDeferredId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {

        if(dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            if(dependentSyncsStatus != null){
                if(dependentSyncsStatus.unitRaisingDeferredId != 0) {
                    metaDataSyncing.setUnitRaisingDeferredId(dependentSyncsStatus.unitRaisingDeferredId);
                }
            }
        }
        else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                if(dependentSyncsStatus != null) {
                    if(dependentSyncsStatus.unitRaisingDeferredId != 0) {
                        imageSyncing.setUnitRaisingDeferredId(dependentSyncsStatus.unitRaisingDeferredId);
                    }
                }
            }
        }
    }

    private HashMap<Integer, UnitRaisingDeferOR> getUnitRaisingDeferredDetails() {
        HashMap<Integer, UnitRaisingDeferOR> unitRaisingDeferredList = new HashMap<>();
        SQLiteDatabase sqlite = null;

        String selectQuery = "SELECT  urd.id as id,u.unit_id as unit_id,u.unit_code as unit_code," +
                "urd.expected_raising_date_time as expected_raising_date_time,urd.new_raising_date_time," +
                " urd.remarks as remarks,urd.CreatedDateTime as CreatedDateTime, urd.deferred_reason as deferred_reason" +
                " FROM " + TableNameAndColumnStatement.UNIT_RAISING_DEFERRED +
                " urd join unit u on u.unit_id = urd.unit_id where " + TableNameAndColumnStatement.IS_SYNCED + "=" + 0;
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingDeferred selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitRaisingDeferOR unitRaisingDeferOR = new
                                UnitRaisingDeferOR();
                        unitRaisingDeferOR.unitId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        unitRaisingDeferOR.unitCode =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE));
                        unitRaisingDeferOR.expectedRaisingDate =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME));
                        unitRaisingDeferOR.newRaisingDate =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NEW_RAISING_DATE_TIME));
                        unitRaisingDeferOR.remarks =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS));
                        unitRaisingDeferOR.deferredDateTime =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME));
                        unitRaisingDeferOR.deferredReason =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DEFERRED_REASON));
                        unitRaisingDeferOR.createdBy  =  appPreferences.getAppUserId();

                        unitRaisingDeferredList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)),
                                unitRaisingDeferOR);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"addPostList object  %s",
                    IOPSApplication.getGsonInstance().toJson(unitRaisingDeferredList));

        }
        return unitRaisingDeferredList;
    }


    private void updateUnitRaisingDeferred(int unitRaisingDeferredId, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_RAISING_DEFERRED +
                " SET " + TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + "= " +
                unitRaisingDeferredId +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + " = " + id ;
        Timber.d(Constants.TAG_SYNCADAPTER+"updateUnitRaisingDeferred updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }



    private void unitRaisingDeferredApiCall(final UnitRaisingDeferOR unitRaisingDeferOR, final int key) {
        sisClient.getApi().deferredUnitRaising(unitRaisingDeferOR, new Callback<UnitRaisingDeferredResponse>() {
            @Override
            public void success(UnitRaisingDeferredResponse unitRaisingDeferredResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingDeferredApiCall response  %s", unitRaisingDeferredResponse);
                switch (unitRaisingDeferredResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        updateUnitRaisingDeferred(unitRaisingDeferredResponse
                                .unitRaisingDeferredData.unitRaisingDeferredId, key);  //updating PostID in UnitPost Table
                        updateUnitRaisingMetaData(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,
                                unitRaisingDeferredResponse
                                        .unitRaisingDeferredData.unitRaisingDeferredId,key,  Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED, mContext));
                         updateStatus(unitRaisingDeferredResponse,unitRaisingDeferOR);
                        //updating PostID in Metadata Table
                        break;

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UNIT_POST_SYNC", error);
            }
        });

        Timber.d(Constants.TAG_SYNCADAPTER+"unitPostApiCAll end count   %s", "count: " + count);


    }



    private void updateStatus(UnitRaisingDeferredResponse unitRaisingDeferredResponse,
                              UnitRaisingDeferOR unitRaisingDeferOR) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.unitRaisingDeferredId = unitRaisingDeferredResponse
                .unitRaisingDeferredData.unitRaisingDeferredId;
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
        insertDepedentSyncData(dependentSyncsStatus);
        MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
        metaDataSyncing.setUnitRaisingDeferredId(dependentSyncsStatus.unitRaisingDeferredId);
    }

    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.unitRaisingDeferredId,
                TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID,true)){
            dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
        }
    }



}
