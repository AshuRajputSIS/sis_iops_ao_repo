package com.sisindia.ai.android.commons;

/**
 * Created by compass on 6/1/2017.
 */

public interface AttachmentMetaDataKeys {
    String[] GUARD_CHECK = {"guard_full_image", "turn_out_image", "turn_out_signature"};
    String[] ADD_KIT_REQUEST = {"guard_image_with_kit", "guard_image_with_signature"};
    String[] DUTY_REGISTER_CHECK = {"duty_register_image1", "duty_register_image2"};
    String[] CLIENT_REGISTER_CHECK = {"visitor", "material", "vehicle", "maintenance"};
    String[] SECURITY_RISK = {"security_check1", "security_check2", "security_check3",
            "security_check4", "security_check5", "security_check6", "security_check7",
            "security_check8", "security_check9", "security_check10"};
    String GRIEVANCE = "audio";

}
