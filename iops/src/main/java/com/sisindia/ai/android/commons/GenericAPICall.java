package com.sisindia.ai.android.commons;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by shankar on 12/11/16.
 */

public class GenericAPICall extends AsyncTask<String, Void, String> {

    private static final String TAG = GenericAPICall.class.getSimpleName();
    private final String notificationApi;
    private final String appToken;
    private String line;
    private String responseData;

    public GenericAPICall(String api, String token) {
        this.notificationApi = api;
        this.appToken = token;
    }

    @Override
    public String doInBackground(String... params) {
        try {
            /************** For getting response from HTTP URL start ***************/
            URL object = new URL(notificationApi);
            HttpURLConnection connection = (HttpURLConnection) object.openConnection();
            connection.setReadTimeout(60 * 1000);
            connection.setConnectTimeout(60 * 1000);
            connection.setRequestProperty("Authorization", "bearer " + appToken);
            connection.setRequestProperty("Content-Type", "application/json");
            int responseCode = connection.getResponseCode();
            Log.e(TAG, "DoJobBackground: " + responseCode);
            if (responseCode == 200) {
                InputStreamReader stream_reader = new InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(stream_reader);
                StringBuilder sb = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                responseData = sb.toString();
                Log.e(TAG, "DoJobBackground: " + responseData.toString());
                stream_reader.close();
                reader.close();

            } else {
                responseData = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseData = "";
        }
        return responseData;
    }
}
