package com.sisindia.ai.android.issues.improvements;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Shushrut on 15-04-2016.
 */
public class ImprovementListAdapter extends RecyclerView.Adapter<ImprovementListAdapter.ImprovmentHolder> implements View.OnClickListener {
    Context addImprovementPlanActivity;
    ArrayList<String> improvementItemList;

    public ImprovementListAdapter(Context addImprovementPlanActivity, ArrayList<String> improvementItemList) {
        this.addImprovementPlanActivity = addImprovementPlanActivity;
        this.improvementItemList = improvementItemList;
    }

    @Override
    public ImprovmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(addImprovementPlanActivity).inflate(R.layout.activity_layout_with_radio_textview, parent, false);
        ImprovmentHolder mHolder = new ImprovmentHolder(view);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(ImprovmentHolder holder, int position) {
        holder.mTextView.setText(improvementItemList.get(position));
        holder.mRadioButton.setOnClickListener(this);
        holder.mRadioButton.setTag(position);

    }

    @Override
    public int getItemCount() {
        return improvementItemList.size();
    }

    @Override
    public void onClick(View v) {
        Util.improvementPlan = "";
        int pos = (int) v.getTag();
        Util.improvementPlan = improvementItemList.get(pos);


    }

    public class ImprovmentHolder extends RecyclerView.ViewHolder {
        RadioButton mRadioButton;
        CustomFontTextview mTextView;

        public ImprovmentHolder(View itemView) {
            super(itemView);
            mRadioButton = (RadioButton) itemView.findViewById(R.id.radio_button_improvement);
            mTextView = (CustomFontTextview) itemView.findViewById(R.id.textview_improvement);
        }
    }
}
