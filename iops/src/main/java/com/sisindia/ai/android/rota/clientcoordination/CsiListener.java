package com.sisindia.ai.android.rota.clientcoordination;

/**
 * Created by shivam on 21/7/16.
 */

public interface CsiListener {

    void responseChange(int questionId, int response);

    void lowFeedback(int planId, int improvementId, String turnAroundTime, int issueMatrixId);

    void lowFeedbackRemoved(int questionId);

}
