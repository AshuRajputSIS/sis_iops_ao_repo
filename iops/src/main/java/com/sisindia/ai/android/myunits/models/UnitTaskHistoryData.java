package com.sisindia.ai.android.myunits.models;

/**
 * Created by Durga Prasad on 02-04-2016.
 */
public class UnitTaskHistoryData {

    String date, time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
