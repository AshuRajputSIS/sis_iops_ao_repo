package com.sisindia.ai.android.recruitment;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.AddRecruitmentIR;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Saruchi on 21-04-2016.
 */
public class AddRecruitActivity extends BaseActivity {
    private static final String DATE_SELECTION_MSG ="CHOOSE DATE";
    private static final String RECUIRTMENT_STATUS ="InProcess";
    private  AddRecruitmentIR recruitmentIR;
    private LinearLayout mParentRecuriment;
    private DateTimeFormatConversionUtil dateTimeUtils;
    @Bind(R.id.recuriment_choose_date)
    Button chooseDateBtn;
    @Bind(R.id.edit_recuriment_name)
    CustomFontEditText mEditRecurimentName;
    @Bind(R.id.edit_recuriment_phone)
    CustomFontEditText mEditRecurimentPhone;
    @Bind(R.id.dateTv)
    CustomFontTextview dateTv;
    @Bind(R.id.name_lable)
    CustomFontTextview nameLable;
    @Bind(R.id.phone_label)
    CustomFontTextview phoneLabel;
    private Util util;
    private Resources mResources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_recurit_activity);
        ButterKnife.bind(this);
        mParentRecuriment = (LinearLayout) findViewById(R.id.parent_recuriment);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recruitmentIR = new AddRecruitmentIR();
        dateTimeUtils = new DateTimeFormatConversionUtil();
        mResources= getResources();
        setUpUI();
    }
    private void setUpUI() {
        util = new Util();
            dateTv.setText(util.createSpannableStringBuilder(mResources.getString(R.string.date_of_birth_str)));
            nameLable.setText(util.createSpannableStringBuilder(mResources.getString(R.string.NAME)));
            phoneLabel.setText(util.createSpannableStringBuilder(mResources.getString(R.string.Ph_number_str)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.add_post_save) {
            Util.hideKeyboard(this);
            String name=mEditRecurimentName.getText().toString().trim();
            String phoneNumber = mEditRecurimentPhone.getText().toString();
            String dateStr = chooseDateBtn.getText().toString();
            if (TextUtils.isEmpty(name) ||name.length() < 3) {
                snackBarWithMesg(mParentRecuriment,
                        mResources.getString(R.string.ENTER_NAME_VALIDATION_MESG));
            } else if (TextUtils.isEmpty(phoneNumber)) {
                snackBarWithMesg(mParentRecuriment,mResources.getString(R.string.ENTER_MOBILE_NO_MESG));
            } else if (!Util.isValidPhoneNumber(phoneNumber)) {
                snackBarWithMesg(mParentRecuriment, mResources.getString(R.string.mobile_validation));
            } else if (TextUtils.isEmpty(dateStr) || dateStr.equalsIgnoreCase(DATE_SELECTION_MSG)) {
                snackBarWithMesg(mParentRecuriment, mResources.getString(R.string.DATE_SELECTION_MESG));
            } else {
                recruitmentIR.setContactNo(phoneNumber);
                recruitmentIR.setFullName(name);
                recruitmentIR.setRecruit_id(0);
                recruitmentIR.setStatus(RECUIRTMENT_STATUS);
                recruitmentIR.setDateOfBirth(dateTimeUtils.convertDayMonthDateYearToDateTimeFormat(dateStr));
                    Util.hideKeyboard(this);
                    IOPSApplication.getInstance().getRecurimentUpdateStatementDB().addNewRecruitment(recruitmentIR);
                    triggerSyncAdapter();
                    this.finish();
            }

        } else if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void triggerSyncAdapter() {
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }
    @OnClick(R.id.recuriment_choose_date)
    public void choosedate() {
            Util.setIsFromRecuirmentFragment(true);
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            SelectDateFragment selectDateFragment = new SelectDateFragment(mParentRecuriment, null);
            selectDateFragment.setPastDate(true);
            Util.hideKeyboard(this);
            selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
            selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                @Override
                public void changeTime(String updateData) {
                    chooseDateBtn.setText(updateData);
                }
            });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


}
