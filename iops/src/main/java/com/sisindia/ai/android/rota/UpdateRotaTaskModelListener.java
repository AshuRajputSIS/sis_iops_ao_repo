package com.sisindia.ai.android.rota;

/**
 * Created by Durga Prasad on 23-05-2016.
 */
public interface UpdateRotaTaskModelListener {

    void updateRotaTask(Object object);
}
