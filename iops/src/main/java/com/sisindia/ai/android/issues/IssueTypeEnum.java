package com.sisindia.ai.android.issues;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shankar on 24/5/16.
 */
public enum IssueTypeEnum {

    Grievance(1),
    Complaint(2);


    private static Map map = new HashMap<>();

    static {
        for (IssueTypeEnum issueType_em : IssueTypeEnum.values()) {
            map.put(issueType_em.value, issueType_em);
        }
    }

    private int value;

    IssueTypeEnum(int value) {
        this.value = value;
    }

    public static IssueTypeEnum valueOf(int issueType) {
        return (IssueTypeEnum) map.get(issueType);
    }

    public int getValue() {
        return value;
    }
}
