package com.sisindia.ai.android.unitatriskpoa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.DataMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by shankar on 13-07-2016.
 */
public class UnitAtRiskPOAAdapter extends
        RecyclerView.Adapter<UnitAtRiskPOAAdapter.UnitAtRiskViewHolder> {

    public void setUnitAtRiskListClickListener(UnitAtRiskListClickListener unitAtRiskListClickListener) {
        this.unitAtRiskListClickListener = unitAtRiskListClickListener;
    }

    private UnitAtRiskListClickListener unitAtRiskListClickListener;

    Context mcontext;
    DataMO mData;
    DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    public UnitAtRiskPOAAdapter(Context mcontext, DataMO mData) {
        this.mcontext = mcontext;
        this.mData = mData;
        this.dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }


    @Override
    public UnitAtRiskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.generic_risk_rowitem, parent, false);
        UnitAtRiskViewHolder unitAtRiskViewHolder = new UnitAtRiskViewHolder(view);

        return unitAtRiskViewHolder;
    }

    @Override
    public void onBindViewHolder(UnitAtRiskViewHolder holder, final int position) {
        UnitAtRiskViewHolder unitAtRiskViewHolder = holder;

        RiskDetailModelMO reRiskDetailModelMO = mData.getRiskDetailModels().get(position);

        unitAtRiskViewHolder.completedDate.setText(dateTimeFormatConversionUtil.
                convertDateTimeToDate(reRiskDetailModelMO.getCompletionDate()));
        unitAtRiskViewHolder.unitName.setText(reRiskDetailModelMO.getUnitName());

        unitAtRiskViewHolder.totalItemCount.setText(reRiskDetailModelMO.
                getTotalItems() + "");


    }

    @Override
    public int getItemCount() {
        return mData.getRiskDetailModels().size();

    }


    public class UnitAtRiskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @Bind(R.id.unit_name)
        CustomFontTextview unitName;
        @Bind(R.id.complete_date)
        CustomFontTextview completedDate;
        @Bind(R.id.total_item_count)
        CustomFontTextview totalItemCount;
        @Bind(R.id.view_checklist_item)
        LinearLayout viewCheckListItem;



        public UnitAtRiskViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            viewCheckListItem.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.view_checklist_item:

                    unitAtRiskListClickListener.onItemClick(mData.getRiskDetailModels().get(getLayoutPosition()));


            }
        }
    }
}

