package com.sisindia.ai.android.database.unitDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.UnitBillingCheckList;
import com.sisindia.ai.android.myunits.models.BarrackStrength;
import com.sisindia.ai.android.myunits.models.EmployeeRank;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.myunits.models.UnitBarrack;
import com.sisindia.ai.android.myunits.models.UnitContactIR;
import com.sisindia.ai.android.myunits.models.UnitContacts;
import com.sisindia.ai.android.myunits.models.UnitCustomers;
import com.sisindia.ai.android.myunits.models.UnitEmployees;
import com.sisindia.ai.android.myunits.models.UnitEquipments;
import com.sisindia.ai.android.myunits.models.UnitMessVendors;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.myunits.models.UnitPostGeoPoint;
import com.sisindia.ai.android.myunits.models.UnitShiftRank;

import java.util.List;

/**
 * Created by shankar on 25/6/16.
 */

public class UnitLevelInsertionStatementDB extends SISAITrackingDB {

    public UnitLevelInsertionStatementDB(Context context) {
        super(context);
    }

    public void insertUnitTable(List<Unit> unitList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (Unit unit : unitList) {
                    values.put(TableNameAndColumnStatement.UNIT_ID, unit.getId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unit.getName());
                    values.put(TableNameAndColumnStatement.UNIT_CODE, unit.getUnitCode());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unit.getDescription());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, unit.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.AREA_ID, unit.getAreaId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unit.getCustomerId());
                    values.put(TableNameAndColumnStatement.DAY_CHECK_FREQUENCY, unit.getDayCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.NIGHT_CHECK_FREQUENCY, unit.getNightCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.CLIENT_COORDINATION_FREQUENCY, unit.getClientCoordinationFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.BILLING_DATE, unit.getBillDate());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_DATE, unit.getBillCollectionDate());
                    values.put(TableNameAndColumnStatement.WAGE_DATE, unit.getWageDate());
                    values.put(TableNameAndColumnStatement.BILLING_MODE, unit.getBillingMode());
                    values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, unit.getMessVendorId());
                    values.put(TableNameAndColumnStatement.IS_BARRACK_PROVIDED, unit.getIsBarrackProvided());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_ID, unit.getUnitCommanderId());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_ID, unit.getAreaInspectorId());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_ID, unit.getBillSubmissionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_ID, unit.getBillCollectionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID, unit.getBillAuthoritativeUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE, unit.getUnitType());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_NAME, unit.getUnitCommanderName());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, unit.getAreaInspectorName());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_NAME, unit.getBillSubmissionResponsibleName());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_NAME, unit.getBillCollectionResponsibleName());
                    values.put(TableNameAndColumnStatement.UNIT_FULL_ADDRESS, unit.getUnitFullAddress());
                    values.put(TableNameAndColumnStatement.UNIT_STATUS, unit.getUnitStatus());
                    values.put(TableNameAndColumnStatement.COLLECTION_STATUS, unit.getCollectionStatus());
                    values.put(TableNameAndColumnStatement.BILLING_TYPE_ID, unit.getBillingTypeId());
                    values.put(TableNameAndColumnStatement.BILL_GENERATION_DATE, unit.getBillGenerationDate());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unit.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unit.getUnarmedStrength());
                    values.put(TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL, unit.getMainGatePhotoUrl());
                    values.put(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME, unit.getRaisingDate());
                    String unitBoundary = "";
                    if (null != unit.getUnitBoundary() && unit.getUnitBoundary().size() != 0) {
                        unitBoundary = IOPSApplication.getGsonInstance().toJson(unit.getUnitBoundary());
                    }
                    values.put(TableNameAndColumnStatement.UNIT_BOUNDARY, unitBoundary);
                    if (isManualSync) {
                        //  String count_Id,int id,String seq_id,String table_Name,String column_Name, SQLiteDatabase sqlite
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_ID, unit.getId(),
                                TableNameAndColumnStatement.ID
                                , TableNameAndColumnStatement.UNIT_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitBarrackList
     */
    public void insertUnitBarrack(List<UnitBarrack> unitBarrackList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                if (isManualSync) {
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().
                            deleteTable(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, sqlite);
                }
                ContentValues values = new ContentValues();
                for (UnitBarrack unitBarrack : unitBarrackList) {
                    values.put(TableNameAndColumnStatement.ID, unitBarrack.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitBarrack.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitBarrack.getBarrackId());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, unitBarrack.getActive());
                    sqlite.insert(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * UnitShiftRank Model inserted
     *
     * @param unitShiftRankList
     * @param isManualSync
     */
    public void insertUnitAuthorizedStrength(List<UnitShiftRank> unitShiftRankList, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (UnitShiftRank unitShiftRank : unitShiftRankList) {
                    values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, unitShiftRank.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitShiftRank.getUnitId());
                    values.put(TableNameAndColumnStatement.SHIFT_ID, unitShiftRank.getShiftId());
                    values.put(TableNameAndColumnStatement.RANK_ID, unitShiftRank.getRankId());
                    values.put(TableNameAndColumnStatement.RANK_COUNT, unitShiftRank.getRankCount());
                    values.put(TableNameAndColumnStatement.RANK_ABBREVATION, unitShiftRank.getRankAbbrevation());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitShiftRank.getBarrackId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unitShiftRank.getUnitName());
                    values.put(TableNameAndColumnStatement.SHIFT_NAME, unitShiftRank.getShiftName());
                    values.put(TableNameAndColumnStatement.RANK_NAME, unitShiftRank.getRankName());
                    values.put(TableNameAndColumnStatement.BARRACK_NAME, unitShiftRank.getBarrackName());
                    values.put(TableNameAndColumnStatement.STRENGTH, unitShiftRank.getStrength());
                    values.put(TableNameAndColumnStatement.ACTUAL, 0);
                    values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                    values.put(TableNameAndColumnStatement.IS_ARMED, unitShiftRank.getIsArmed());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    if (isManualSync) {
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID,
                                unitShiftRank.getId(),
                                TableNameAndColumnStatement.ID
                                , TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }


    /**
     * BarrackStrength Model inserted
     *
     * @param barrackStrength
     */
    public synchronized void insertUnitBarrackAuthorizedStrength(BarrackStrength barrackStrength, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, barrackStrength.getId());
                values.put(TableNameAndColumnStatement.SHIFT_ID, barrackStrength.getShiftId());
                values.put(TableNameAndColumnStatement.RANK_ID, barrackStrength.getRankId());
                values.put(TableNameAndColumnStatement.RANK_COUNT, barrackStrength.getRankCount());
                values.put(TableNameAndColumnStatement.RANK_ABBREVATION, barrackStrength.getRankAbbrevation());
                values.put(TableNameAndColumnStatement.BARRACK_ID, barrackStrength.getBarrackId());
                values.put(TableNameAndColumnStatement.UNIT_NAME, barrackStrength.getUnitName());
                values.put(TableNameAndColumnStatement.SHIFT_NAME, barrackStrength.getShiftName());
                values.put(TableNameAndColumnStatement.RANK_NAME, barrackStrength.getRankName());
                values.put(TableNameAndColumnStatement.BARRACK_NAME, barrackStrength.getBarrackName());
                values.put(TableNameAndColumnStatement.STRENGTH, barrackStrength.getStrength());
                values.put(TableNameAndColumnStatement.ACTUAL, 0);
                values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                values.put(TableNameAndColumnStatement.IS_ARMED, barrackStrength.getIsArmed());
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                if (isManualSync) {
                    //  String count_Id,int id,String seq_id,String table_Name,String column_Name, SQLiteDatabase sqlite
                    GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, barrackStrength.getId(),
                            TableNameAndColumnStatement.ID
                            , TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, TableNameAndColumnStatement.ID, sqlite);
                    if (genericUpdateTableMO.getIsAvailable() != 0) {
                        sqlite.update(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, values,
                                TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
                    }
                } else {
                    sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }


    /**
     * @param unitEmployeesList
     */

    public void insertUnitEmployees(List<UnitEmployees> unitEmployeesList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (UnitEmployees unitEmployee : unitEmployeesList) {

                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitEmployee.getUnitId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID,
                            unitEmployee.getEmployeeId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_NO,
                            unitEmployee.getEmployeeNo());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME,
                            unitEmployee.getEmployeeFullName());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_CONTACT_NO,
                            unitEmployee.getEmployeContactNo());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE,
                            unitEmployee.isActive());
                    values.put(TableNameAndColumnStatement.DEPLOYMENT_DATE,
                            unitEmployee.getDeploymentDate());
                    values.put(TableNameAndColumnStatement.WEEKLY_OFF_DAY,
                            unitEmployee.getWeeklyOffDay());
                    if (null != unitEmployee.getEmployeeRank() &&
                            unitEmployee.getEmployeeRank().size() != 0) {
                        for (EmployeeRank employeeRank : unitEmployee.getEmployeeRank()) {
                            values.put(TableNameAndColumnStatement.EMPLOYEE_RANK_ID,
                                    employeeRank.getRankId());
                            values.put(TableNameAndColumnStatement.BRANCH_ID,
                                    employeeRank.getBranchId());
                        }
                    }
                    if (isManualSync) {
                        //  String count_Id,int id,String seq_id,String table_Name,String column_Name, SQLiteDatabase sqlite
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.EMPLOYEE_ID,
                                unitEmployee.getEmployeeId(), TableNameAndColumnStatement.ID
                                , TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitMessVendor
     */
    public void insertUnitMessVendor(UnitMessVendors unitMessVendor, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.VENDOR_ID, unitMessVendor.getId());
                values.put(TableNameAndColumnStatement.VENDOR_NAME, unitMessVendor.getVendorName());
                values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitMessVendor.getDescription());
                values.put(TableNameAndColumnStatement.VENDOR_CONTACT_NAME, unitMessVendor.getVendorContactName());
                values.put(TableNameAndColumnStatement.VENDOR_FULL_ADDRESS, unitMessVendor.getVendorAddress());
                if (isManualSync) {
                    GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.VENDOR_ID, unitMessVendor.getId(), TableNameAndColumnStatement.ID
                            , TableNameAndColumnStatement.MESS_VENDOR_TABLE, TableNameAndColumnStatement.ID, sqlite);
                    if (genericUpdateTableMO.getIsAvailable() != 0) {
                        sqlite.update(TableNameAndColumnStatement.MESS_VENDOR_TABLE, values,
                                TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.MESS_VENDOR_TABLE, null, values);
                    }
                } else {
                    sqlite.insert(TableNameAndColumnStatement.MESS_VENDOR_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitContactList
     */
    public void insertUnitContacts(List<UnitContacts> unitContactList, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (UnitContacts unitContact : unitContactList) {
                    values.put(TableNameAndColumnStatement.IS_NEW, 0);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                            unitContact.getId());
                    values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
                    values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
                    values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
                    values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitContact.getUnitId());
                    values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
                    if (isManualSync) {
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                                unitContact.getId(), TableNameAndColumnStatement.ID
                                , TableNameAndColumnStatement.UNIT_CONTACT_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitCustomer
     */
    public void insertUnitCustomer(UnitCustomers unitCustomer, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.CUSTOMER_ID, unitCustomer.getId());
                values.put(TableNameAndColumnStatement.CUSTOMER_NAME, unitCustomer.getCustomerName());
                values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitCustomer.getCustomerDescription());
                values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_NAME, unitCustomer.getCustomerPrimaryContactName());
                values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_EMAIL, unitCustomer.getPrimaryContactEmail());
                values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_PHONE, unitCustomer.getPrimaryContactPhone());
                values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_NAME, unitCustomer.getCustomerSecondaryContactName());
                values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_EMAIL, unitCustomer.getSecondaryContactEmail());
                values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_PHONE, unitCustomer.getSecondaryContactPhone());
                values.put(TableNameAndColumnStatement.CAN_SEND_EMAIL, unitCustomer.isEmailToBeSent());
                values.put(TableNameAndColumnStatement.CAN_SEND_SMS, unitCustomer.isSmsToBeSent());
                values.put(TableNameAndColumnStatement.CUSTOMER_FULL_ADDRESS, unitCustomer.getCustomerHoAddress());
                if (isManualSync) {
                    GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.CUSTOMER_ID, unitCustomer.getId(), TableNameAndColumnStatement.ID
                            , TableNameAndColumnStatement.CUSTOMER_TABLE, TableNameAndColumnStatement.ID, sqlite);
                    if (genericUpdateTableMO.getIsAvailable() != 0) {
                        sqlite.update(TableNameAndColumnStatement.CUSTOMER_TABLE, values,
                                TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.CUSTOMER_TABLE, null, values);
                    }
                } else {
                    sqlite.insert(TableNameAndColumnStatement.CUSTOMER_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitPosts
     */

    public void insertUnitPost(List<UnitPost> unitPosts, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                String postName = "";
                for (UnitPost unitPost : unitPosts) {
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitPost.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitPost.getId());
                    if (unitPost.getUnitPostName().contains("'")) {
                        postName = unitPost.getUnitPostName();
                        postName = postName.replace("'", "\'");
                    } else {
                        postName = unitPost.getUnitPostName();
                    }
                    values.put(TableNameAndColumnStatement.UNIT_POST_NAME, postName);
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitPost.getDescription());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unitPost.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unitPost.getUnarmedStrength());
                    values.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, unitPost.getMainGateDistance());
                    values.put(TableNameAndColumnStatement.BARRACK_DISTANCE, unitPost.getBarrackDistance());
                    values.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, unitPost.getUnitOfficeDistance());
                    values.put(TableNameAndColumnStatement.IS_MAIN_GATE, unitPost.getIsMainGate());
                    values.put(TableNameAndColumnStatement.DEVICE_NO, unitPost.deviceNo);
                    values.put(TableNameAndColumnStatement.IS_NEW, unitPost.isNew());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, unitPost.isSynced());
                    if (unitPost.getUnitPostGeoPoint() != null && unitPost.getUnitPostGeoPoint().size() != 0) {
                        for (UnitPostGeoPoint unitPostGeoPint : unitPost.getUnitPostGeoPoint()) {
                            values.put(TableNameAndColumnStatement.GEO_LATITUDE, unitPostGeoPint.getLatitude());
                            values.put(TableNameAndColumnStatement.GEO_LONGITUDE, unitPostGeoPint.getLongitude());
                        }
                    }
                    values.put(TableNameAndColumnStatement.POST_STATUS, unitPost.getIsActive());
                    if (isManualSync) {
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_ID, unitPost.getUnitId(), TableNameAndColumnStatement.ID
                                , TableNameAndColumnStatement.UNIT_POST_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_POST_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_POST_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitEquipmentList
     */
    public void insertUnitEquipments(List<UnitEquipments> unitEquipmentList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (UnitEquipments unitEquipment : unitEquipmentList) {
                    values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID, unitEquipment.getId());
                    values.put(TableNameAndColumnStatement.UNIQUE_NO, unitEquipment.getEquipmentUniqueNo());
                    values.put(TableNameAndColumnStatement.MANUFACTURER, unitEquipment.getEquipmentManufacturer());
                    values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, unitEquipment.isCustomerProvided());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS, unitEquipment.getStatus());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE, unitEquipment.getEquipmentId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitEquipment.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitEquipment.getPostId());
                    values.put(TableNameAndColumnStatement.COUNT, unitEquipment.getQuantity());
                    values.put(TableNameAndColumnStatement.IS_NEW, Constants.IS_OLD); //  recored synced wih server
                    values.put(TableNameAndColumnStatement.IS_SYNCED, Constants.IS_SYNCED); // recored synced wih server
                    if (isManualSync) {
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance()
                                .getSelectionStatementDBInstance()
                                .getTableSeqId(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID,
                                        unitEquipment.getId(), TableNameAndColumnStatement.ID
                                        , TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE,
                                        TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            sqlite.update(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
                        }
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitContact
     */
    public void insetrUpdateUnitContacts(UnitContactIR unitContact, int seqid) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.IS_NEW, 1);
                values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                        unitContact.getId());
                values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
                values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
                values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
                values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
                values.put(TableNameAndColumnStatement.UNIT_ID,
                        unitContact.getUnitId());
                values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
                if (seqid == -1) {
                    sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
                } else {
                    sqlite.update(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, values, TableNameAndColumnStatement.ID + " = " + seqid, null);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    /**
     * @param unitBillingCheckList
     */
    public void insertUnitBillingCheckList(List<UnitBillingCheckList> unitBillingCheckList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                if (isManualSync) {
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().
                            deleteTable(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE, sqlite);
                }
                ContentValues values = new ContentValues();
                for (UnitBillingCheckList unitBilling : unitBillingCheckList) {
                    values.put(TableNameAndColumnStatement.ID, unitBilling.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitBilling.getUnitId());
                    values.put(TableNameAndColumnStatement.BILL_CHECK_LIST_ID, unitBilling.getBillingCheckListId());
                    sqlite.insert(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }
}