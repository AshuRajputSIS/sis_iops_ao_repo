package com.sisindia.ai.android.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LanguageActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.english_button)
    CustomFontButton englishButton;
    @Bind(R.id.hindi_button)
    CustomFontButton hindiButton;
    @Bind(R.id.language_selection_english)
    CustomFontTextview languageSelectionEnglish;
    @Bind(R.id.language_selection_hindi)
    CustomFontTextview languageSelectionHindi;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.activity_language)
    LinearLayout activityLanguage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.Language_Selection));
        toolbar.setNavigationIcon(null);
        englishButton.setOnClickListener(this);
        hindiButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.english_button:
                setLocale(Constants.LOCALE_ENGLISH_INDENTIFIER, Constants.ENGLISH_LANGUAGE_ID);
                appPreferences.setLanguageId(Constants.ENGLISH_LANGUAGE_ID);
                break;
            case R.id.hindi_button:
                setLocale(Constants.LOCALE_ENGLISH_INDENTIFIER, Constants.HINDI_LANGUAGE_ID);
                appPreferences.setLanguageId(Constants.HINDI_LANGUAGE_ID);
                break;
        }
        openOnBoardingActivity(OnBoardingFactory.REGISTRATION, 2);
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(LanguageActivity.this, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
        finish();
    }

}
