package com.sisindia.ai.android.rota.billsubmission.modelObjects;


import com.google.gson.annotations.SerializedName;

public class BillSubmissionBaseMO {

@SerializedName("BillSubmission")
private BillSubmissionMO billSubmission;

/**
* 
* @return
* The billSubmission
*/
public BillSubmissionMO getBillSubmission() {
return billSubmission;
}

/**
* 
* @param billSubmission
* The BillSubmission
*/
public void setBillSubmission(BillSubmissionMO billSubmission) {
this.billSubmission = billSubmission;
}

}


