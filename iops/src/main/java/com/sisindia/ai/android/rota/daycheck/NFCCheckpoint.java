package com.sisindia.ai.android.rota.daycheck;

import com.sisindia.ai.android.myunits.models.UnitPost;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hannan Shaik on 7/18/16.
 */
public class NFCCheckpoint {

    private static NFCCheckpoint instance;
    private final ArrayList<UnitPost> checkPointList = new ArrayList<>();

    public static NFCCheckpoint getInstance() {
        if (instance == null) {
            instance = new NFCCheckpoint();
        }
        return instance;
    }

    public void clearCheckpoints() {
        instance = null;
    }

    public void addCheckpoint(UnitPost unitPost) {
        if (unitPost != null && !checkPointList.contains(unitPost)) {
            checkPointList.add(unitPost);
        }
    }

    public List<UnitPost> getCheckpoints() {
        return checkPointList;
    }
}