package com.sisindia.ai.android.loadconfiguration;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class LoadConfigBaseMO {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    public LoadConfigurationData loadConfigurationData;

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * @return The data
     */
    public LoadConfigurationData getLoadConfigurationData() {
        return loadConfigurationData;
    }

    /**
     * @param data The LoadConfigurationData
     */
    public void setLoadConfigurationData(LoadConfigurationData data) {
        this.loadConfigurationData = data;
    }

}
