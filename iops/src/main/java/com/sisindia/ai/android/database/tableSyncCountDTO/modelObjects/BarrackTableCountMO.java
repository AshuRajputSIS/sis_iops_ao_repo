package com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects;

/**
 * Created by shankar on 19/10/16.
 */

public class BarrackTableCountMO {

    private int totalCount;
    private int syncCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getSyncCount() {
        return syncCount;
    }

    public void setSyncCount(int syncCount) {
        this.syncCount = syncCount;
    }

}
