package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseTaskIMO {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("unitId")
    @Expose
    private Integer unitId;
    @SerializedName("TaskTypeId")
    @Expose
    private Integer TaskTypeId;
    @SerializedName("AssigneeId")
    @Expose
    private Integer AssigneeId;
    @SerializedName("EstimatedExecutionTime")
    @Expose
    private Integer EstimatedExecutionTime;
    @SerializedName("ActualExecutionTime")
    @Expose
    private Integer ActualExecutionTime;
    @SerializedName("TaskStatusId")
    @Expose
    private Integer TaskStatusId;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("EstimatedTaskExecutionDateTime")
    @Expose
    private String EstimatedTaskExecutionDateTime;
    @SerializedName("ActualTaskExecutionDateTime")
    @Expose
    private String ActualTaskExecutionDateTime;
    @SerializedName("IsAutoCreation")
    @Expose
    private Boolean IsAutoCreation;
    @SerializedName("ExecutorId")
    @Expose
    private Integer ExecutorId;
    @SerializedName("CreatedDateTime")
    @Expose
    private String CreatedDateTime;
    @SerializedName("CreatedById")
    @Expose
    private Integer CreatedById;
    @SerializedName("ApprovedById")
    @Expose
    private Integer ApprovedById;
    @SerializedName("RelatedTaskId")
    @Expose
    private Integer RelatedTaskId;
    @SerializedName("UnitPostId")
    @Expose
    private Integer UnitPostId;
    @SerializedName("AdditionalComments")
    @Expose
    private String AdditionalComments;
    @SerializedName("AssignedById")
    @Expose
    private Integer AssignedById;
    @SerializedName("TaskExecutionResult")
    @Expose
    private TaskExecutionResultIMO TaskExecutionResult;
    @SerializedName("BarrackId")
    @Expose
    private Integer BarrackId;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The unitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The TaskTypeId
     */
    public Integer getTaskTypeId() {
        return TaskTypeId;
    }

    /**
     * @param TaskTypeId The TaskTypeId
     */
    public void setTaskTypeId(Integer TaskTypeId) {
        this.TaskTypeId = TaskTypeId;
    }

    /**
     * @return The AssigneeId
     */
    public Integer getAssigneeId() {
        return AssigneeId;
    }

    /**
     * @param AssigneeId The AssigneeId
     */
    public void setAssigneeId(Integer AssigneeId) {
        this.AssigneeId = AssigneeId;
    }

    /**
     * @return The EstimatedExecutionTime
     */
    public Integer getEstimatedExecutionTime() {
        return EstimatedExecutionTime;
    }

    /**
     * @param EstimatedExecutionTime The EstimatedExecutionTime
     */
    public void setEstimatedExecutionTime(Integer EstimatedExecutionTime) {
        this.EstimatedExecutionTime = EstimatedExecutionTime;
    }

    /**
     * @return The ActualExecutionTime
     */
    public Integer getActualExecutionTime() {
        return ActualExecutionTime;
    }

    /**
     * @param ActualExecutionTime The ActualExecutionTime
     */
    public void setActualExecutionTime(Integer ActualExecutionTime) {
        this.ActualExecutionTime = ActualExecutionTime;
    }

    /**
     * @return The TaskStatusId
     */
    public Integer getTaskStatusId() {
        return TaskStatusId;
    }

    /**
     * @param TaskStatusId The TaskStatusId
     */
    public void setTaskStatusId(Integer TaskStatusId) {
        this.TaskStatusId = TaskStatusId;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The EstimatedTaskExecutionDateTime
     */
    public String getEstimatedTaskExecutionDateTime() {
        return EstimatedTaskExecutionDateTime;
    }

    /**
     * @param EstimatedTaskExecutionDateTime The EstimatedTaskExecutionDateTime
     */
    public void setEstimatedTaskExecutionDateTime(String EstimatedTaskExecutionDateTime) {
        this.EstimatedTaskExecutionDateTime = EstimatedTaskExecutionDateTime;
    }

    /**
     * @return The ActualTaskExecutionDateTime
     */
    public String getActualTaskExecutionDateTime() {
        return ActualTaskExecutionDateTime;
    }

    /**
     * @param ActualTaskExecutionDateTime The ActualTaskExecutionDateTime
     */
    public void setActualTaskExecutionDateTime(String ActualTaskExecutionDateTime) {
        this.ActualTaskExecutionDateTime = ActualTaskExecutionDateTime;
    }

    /**
     * @return The IsAutoCreation
     */
    public Boolean getIsAutoCreation() {
        return IsAutoCreation;
    }

    /**
     * @param IsAutoCreation The IsAutoCreation
     */
    public void setIsAutoCreation(Boolean IsAutoCreation) {
        this.IsAutoCreation = IsAutoCreation;
    }

    /**
     * @return The ExecutorId
     */
    public Integer getExecutorId() {
        return ExecutorId;
    }

    /**
     * @param ExecutorId The ExecutorId
     */
    public void setExecutorId(Integer ExecutorId) {
        this.ExecutorId = ExecutorId;
    }

    /**
     * @return The CreatedDateTime
     */
    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    /**
     * @param CreatedDateTime The CreatedDateTime
     */
    public void setCreatedDateTime(String CreatedDateTime) {
        this.CreatedDateTime = CreatedDateTime;
    }

    /**
     * @return The CreatedById
     */
    public Integer getCreatedById() {
        return CreatedById;
    }

    /**
     * @param CreatedById The CreatedById
     */
    public void setCreatedById(Integer CreatedById) {
        this.CreatedById = CreatedById;
    }

    /**
     * @return The ApprovedById
     */
    public Integer getApprovedById() {
        return ApprovedById;
    }

    /**
     * @param ApprovedById The ApprovedById
     */
    public void setApprovedById(Integer ApprovedById) {
        this.ApprovedById = ApprovedById;
    }

    /**
     * @return The RelatedTaskId
     */
    public Integer getRelatedTaskId() {
        return RelatedTaskId;
    }

    /**
     * @param RelatedTaskId The RelatedTaskId
     */
    public void setRelatedTaskId(Integer RelatedTaskId) {
        this.RelatedTaskId = RelatedTaskId;
    }

    /**
     * @return The UnitPostId
     */
    public Integer getUnitPostId() {
        return UnitPostId;
    }

    /**
     * @param UnitPostId The UnitPostId
     */
    public void setUnitPostId(Integer UnitPostId) {
        this.UnitPostId = UnitPostId;
    }

    /**
     * @return The AdditionalComments
     */
    public String getAdditionalComments() {
        return AdditionalComments;
    }

    /**
     * @param AdditionalComments The AdditionalComments
     */
    public void setAdditionalComments(String AdditionalComments) {
        this.AdditionalComments = AdditionalComments;
    }

    /**
     * @return The AssignedById
     */
    public Integer getAssignedById() {
        return AssignedById;
    }

    /**
     * @param AssignedById The AssignedById
     */
    public void setAssignedById(Integer AssignedById) {
        this.AssignedById = AssignedById;
    }

    /**
     * @return The TaskExecutionResult
     */
    public TaskExecutionResultIMO getTaskExecutionResult() {
        return TaskExecutionResult;
    }

    /**
     * @param TaskExecutionResult The TaskExecutionResult
     */
    public void setTaskExecutionResult(TaskExecutionResultIMO TaskExecutionResult) {
        this.TaskExecutionResult = TaskExecutionResult;
    }

    /**
     * @return The BarrackId
     */
    public Integer getBarrackId() {
        return BarrackId;
    }

    /**
     * @param BarrackId The BarrackId
     */
    public void setBarrackId(Integer BarrackId) {
        this.BarrackId = BarrackId;
    }

    @Override
    public String toString() {
        return "BaseTaskIMO{" +
                "Id=" + Id +
                ", unitId=" + unitId +
                ", TaskTypeId=" + TaskTypeId +
                ", AssigneeId=" + AssigneeId +
                ", EstimatedExecutionTime=" + EstimatedExecutionTime +
                ", ActualExecutionTime=" + ActualExecutionTime +
                ", TaskStatusId=" + TaskStatusId +
                ", Description='" + Description + '\'' +
                ", EstimatedTaskExecutionDateTime='" + EstimatedTaskExecutionDateTime + '\'' +
                ", ActualTaskExecutionDateTime='" + ActualTaskExecutionDateTime + '\'' +
                ", IsAutoCreation=" + IsAutoCreation +
                ", ExecutorId=" + ExecutorId +
                ", CreatedDateTime='" + CreatedDateTime + '\'' +
                ", CreatedById=" + CreatedById +
                ", ApprovedById=" + ApprovedById +
                ", RelatedTaskId=" + RelatedTaskId +
                ", UnitPostId=" + UnitPostId +
                ", AdditionalComments='" + AdditionalComments + '\'' +
                ", AssignedById=" + AssignedById +
                ", TaskExecutionResult=" + TaskExecutionResult +
                ", BarrackId=" + BarrackId +
                '}';
    }
}