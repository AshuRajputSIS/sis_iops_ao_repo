package com.sisindia.ai.android.unitatriskpoa;

import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.DataMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitAtRiskBaseMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskPendingCountMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Shankar on 14-07-2016.
 */
public class UnitAtRiskFragment extends Fragment implements UnitAtRiskListClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.recyclerview_unit_risk)
    RecyclerView unitRisk_recyclerview;
    UnitAtRiskPOAAdapter mAdapter;
    @Bind(R.id.unit_risk_count)
    CustomFontTextview unitRiskCount;
    @Bind(R.id.unit_risk_textview)
    CustomFontTextview unitRiskTextview;
    @Bind(R.id.unit_pending_count)
    CustomFontTextview unitPendingCount;
    @Bind(R.id.unit_pending_textview)
    CustomFontTextview unitPendingTextview;
    @Bind(R.id.header_view_unit_risk)
    LinearLayout headerViewUnitRisk;

    private String mParam1;
    private String mParam2;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    //    private ColorStateList colorStateList;
    private SISClient sisClient;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    private int currentMonth;
    private int currentYear;
    private int currentDateNumber;
    private DataMO unitRiskDataMO;
    private UnitRiskPendingCountMO unitRiskPendingCountMO;
    private final Handler myHandler = new Handler();

    final Runnable updateRunnable = new Runnable() {
        public void run() {
            //call the activity method that updates the UI
            // updateCountUI();
            setUARCount();
        }
    };

    private void getCountFormDB() {
        //update the UI using the handler and the runnable
        myHandler.post(updateRunnable);
    }

    public UnitAtRiskFragment() {
        // Required empty public constructor
    }

    public static UnitAtRiskFragment newInstance(String param1, String param2) {
        UnitAtRiskFragment fragment = new UnitAtRiskFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
//        colorStateList = ContextCompat.getColorStateList(getActivity(), R.drawable.tab_label_indicator);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.unit_at_risk, container, false);
        ButterKnife.bind(this, view);
        sisClient = new SISClient(getActivity());
        unitRiskDataMO = new DataMO();
        unitRiskDataMO = getUnitRiskList();
        if (null == unitRiskDataMO.getUnitRiskCount() && unitRiskDataMO.getRiskDetailModels().size() == 0) {
            unitRiskAPICall();
        } else {
            setupRecyclerView(unitRisk_recyclerview);
        }
        return view;
    }

    private DataMO getUnitRiskList() {
        unitRiskDataMO = IOPSApplication.getInstance().getUARSelectionInstance().getUnitAtRiskPOA();
        return unitRiskDataMO;
    }

    private void setUARCount() {

        unitRiskPendingCountMO = new UnitRiskPendingCountMO();
        unitRiskPendingCountMO = IOPSApplication.getInstance().getUARSelectionInstance().getUARPendingCount();
        if (null != unitRiskDataMO.getUnitRiskCount() && unitRiskDataMO.getRiskDetailModels().size() != 0) {
            unitRiskCount.setText(unitRiskDataMO.getUnitRiskCount().getRiskCountByAreaInspector() + "" +
                    "/" + unitRiskDataMO.getUnitRiskCount().getRiskCount() + "");
        } else {
            unitRiskCount.setText("0/0");
        }

        if (null != unitRiskPendingCountMO)
            unitPendingCount.setText(unitRiskPendingCountMO.getCompletedCount() + "" + "/" + unitRiskPendingCountMO.getTotalCount() + "");
        else
            unitPendingCount.setText("0/0");
    }

    private synchronized void unitRiskAPICall() {
        if (NetworkUtil.isConnected) {

            showprogressbar(R.string.loading_please_wait);
            String currentDate = dateTimeFormatConversionUtil.getCurrentDate();
            String[] currentDateAry = currentDate.split("-");

            if (null != currentDateAry && currentDateAry.length != 0) {
                currentDateNumber = Integer.parseInt(currentDateAry[0]);
                currentMonth = Integer.parseInt(currentDateAry[1]);
                currentYear = Integer.parseInt(currentDateAry[2]);
            }
            if (currentDateNumber <= 12) {
                currentMonth = currentMonth - 1;
                if (currentMonth == 0) {
                    currentMonth = Calendar.DECEMBER + 1;
                    currentYear = currentYear - 1;
                }
            }

            sisClient.getApi().UnitAtRiskPOA(currentMonth, currentYear,
                    new Callback<UnitAtRiskBaseMO>() {
                        @Override
                        public void success(UnitAtRiskBaseMO unitAtRiskBaseMO, Response response) {
                            hideprogressbar();
                            if (null != unitAtRiskBaseMO && unitAtRiskBaseMO.getStatusCode() == 200) {
                                if (null != unitAtRiskBaseMO.getData() && null != unitAtRiskBaseMO
                                        .getData().getRiskDetailModels()) {
                                    IOPSApplication.getInstance().getUARInsertionInstance().insertUnitRiskTable(unitAtRiskBaseMO.getData()
                                                    .getRiskDetailModels(),
                                            unitAtRiskBaseMO.getData().getUnitRiskCount(), false);
                                }
                            } else {
                                Toast.makeText(getActivity(), "Somethings went wrong!", Toast.LENGTH_SHORT).show();
                            }
                            setupRecyclerView(unitRisk_recyclerview);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            hideprogressbar();
//                            setupRecyclerView(unitRisk_recyclerview);
                            Crashlytics.logException(error);
                        }
                    });
        } else {
            Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        unitRiskDataMO = getUnitRiskList();
        new Thread(new Runnable() {
            @Override
            public void run() {
                getCountFormDB();
            }
        }).start();

        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new UnitAtRiskPOAAdapter(getContext(), unitRiskDataMO);
            recyclerView.setAdapter(mAdapter);
            mAdapter.setUnitAtRiskListClickListener(this);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemClick(RiskDetailModelMO reRiskDetailModelMO) {

        if (null != reRiskDetailModelMO) {
            Intent intent = new Intent(getActivity(), UARActionPlanActivity.class);
            intent.putExtra("UnitRiskId", reRiskDetailModelMO.getUnitRiskId());
            intent.putExtra("UnitName", reRiskDetailModelMO.getUnitName());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.unit_at_risk_poa, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.syncUAR:
                IOPSApplication.getInstance().getUarDeletionInstance().deleteUnitRiskTable(TableNameAndColumnStatement.UNIT_RISK_TABLE);
                IOPSApplication.getInstance().getUarDeletionInstance().deleteUnitRiskTable(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE);
                unitRiskAPICall();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void showprogressbar(int mesg) {
        Util.showProgressBar(getActivity(), mesg);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isVisible());
    }

}

