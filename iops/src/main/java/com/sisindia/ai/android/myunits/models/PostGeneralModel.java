package com.sisindia.ai.android.myunits.models;

import android.widget.RadioGroup;

/**
 * Created by Shushrut on 17-05-2016.
 */
public class PostGeneralModel {
    public static PostGeneralModel mPostGeneralModel;
    public static UnitPost mUnitPost;
    public static RadioGroup rgSopAvailable;
    public static RadioGroup rgAttendance;


    public static PostGeneralModel getmPostGeneralModelInstance() {
        if (mPostGeneralModel == null) {
            mPostGeneralModel = new PostGeneralModel();
        }
        return mPostGeneralModel;
    }

    public static RadioGroup getRgSopAvailable() {
        return rgSopAvailable;
    }

    public static void setRgSopAvailable(RadioGroup rgSopAvailable) {
        PostGeneralModel.rgSopAvailable = rgSopAvailable;
    }

    public static RadioGroup getRgAttendance() {
        return rgAttendance;
    }

    public static void setRgAttendance(RadioGroup rgAttendance) {
        PostGeneralModel.rgAttendance = rgAttendance;
    }

    public UnitPost initUnitPostInstance() {
        if (mUnitPost == null) {
            mUnitPost = new UnitPost();
        }
        return mUnitPost;
    }

    public void setUnitPostModel(UnitPost mPost) {
        initUnitPostInstance();
        mUnitPost = mPost;

    }

    public UnitPost mGetUnitPostModel() {
        return mUnitPost;
    }
}

