package com.sisindia.ai.android.myunits.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Durga Prasad on 06-05-2016.
 */
public class UnitContactsMO implements Parcelable {


    private String contactFirstName;
    private String contactNumber;
    private String contactEmail;
    private int unitContactId;
    private String clientDesignation;
    private int id;
    private String contactLastName;
    private String unitId;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getClientDesignation() {
        return clientDesignation;
    }

    public void setClientDesignation(String clientDesignation) {
        this.clientDesignation = clientDesignation;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }


    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public int getUnitContactId() {
        return unitContactId;
    }

    public void setUnitContactId(int unitContactId) {
        this.unitContactId = unitContactId;
    }

    @Override
    public String toString() {
        return "UnitContactsMO{" +
                "contactFirstName='" + contactFirstName + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", unitContactId=" + unitContactId +
                ", clientDesignation='" + clientDesignation + '\'' +
                ", id=" + id +
                ", contactLastName='" + contactLastName + '\'' +
                ", unitId='" + unitId + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contactFirstName);
        dest.writeString(this.contactNumber);
        dest.writeString(this.contactEmail);
        dest.writeInt(this.unitContactId);
        dest.writeString(this.clientDesignation);
        dest.writeInt(this.id);
        dest.writeString(this.contactLastName);
        dest.writeString(this.unitId);
    }

    public UnitContactsMO() {
    }

    protected UnitContactsMO(Parcel in) {
        this.contactFirstName = in.readString();
        this.contactNumber = in.readString();
        this.contactEmail = in.readString();
        this.unitContactId = in.readInt();
        this.clientDesignation = in.readString();
        this.id = in.readInt();
        this.contactLastName = in.readString();
        this.unitId = in.readString();
    }

    public static final Creator<UnitContactsMO> CREATOR = new Creator<UnitContactsMO>() {
        @Override
        public UnitContactsMO createFromParcel(Parcel source) {
            return new UnitContactsMO(source);
        }

        @Override
        public UnitContactsMO[] newArray(int size) {
            return new UnitContactsMO[size];
        }
    };
}
