package com.sisindia.ai.android.appuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 16-09-2016.
 */
public class AppUserResponseData {
    @SerializedName("AddressId")
    @Expose
    private Integer addressId;

    /**
     *
     * @return
     * The addressId
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     *
     * @param addressId
     * The AddressId
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

}