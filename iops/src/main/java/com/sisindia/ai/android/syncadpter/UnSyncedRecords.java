package com.sisindia.ai.android.syncadpter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by compass on 8/22/2017.
 */

public class UnSyncedRecords {
    private Context context;

    public UnSyncedRecords(Context context) {
        this.context = context;
    }

    public void uploadUnSyncedImages() {
        ArrayList<Integer> unSyncedIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.TASK_ID + ","
                + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + ", "
                + TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE + " from " +
                TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where " +
                TableNameAndColumnStatement.IS_FILE_Upload + " = " + 0 + " or " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0;
        Timber.d(" uploadUnSyncedImages  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)) != 0) {
                            if (!unSyncedIdList.contains(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)))) {
                                unSyncedIdList.add(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            }
                        } else {
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)) != 0) {
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.ISSUE_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_POST, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.UNIT_POST_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_IMAGE, context) ||
                                        cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                                Util.getAttachmentSourceType(TableNameAndColumnStatement.DISTRIBUTION_SIGANTURE, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.UNIT_RAISING_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)) ==
                                        Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED, context)) {
                                    updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + " = " +
                                            cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                                }
                            }
                        }
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                if (unSyncedIdList != null && unSyncedIdList.size() != 0) {
                    for (Integer taskId : unSyncedIdList) {
                        updateTaskRelatedSyncsFlags(taskId, sqlite, TableNameAndColumnStatement.TASK_ID + " = " + taskId);
                    }
                }
                sqlite.close();
            }
        }
    }

    private void updateTaskRelatedSyncsFlags(int taskId, SQLiteDatabase sqlite, String whichColumn) {
        updateAllDependentModulesSynced(sqlite,
                TableNameAndColumnStatement.TASK_ID +
                        " = " + taskId);
    }

    private void updateAllDependentModulesSynced(SQLiteDatabase sqLiteDatabase, String whereClause) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED, 0);
            contentValues.put(TableNameAndColumnStatement.IS_METADATA_SYNCED, Constants.NEG_DEFAULT_COUNT);
            contentValues.put(TableNameAndColumnStatement.IS_IMAGES_SYNCED, Constants.NEG_DEFAULT_COUNT);
            sqLiteDatabase.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, contentValues, whereClause, null);

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void uploadUnSyncedIssues() {

        ArrayList<Integer> unSyncedIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.SOURCE_TASK_ID
                + " from " + TableNameAndColumnStatement.ISSUES_TABLE
                + " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0
                + " and " + TableNameAndColumnStatement.ISSUE_ID + " = " + 0;
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(
                                TableNameAndColumnStatement.SOURCE_TASK_ID)) != 0) {
                            if (!unSyncedIdList.contains(cursor.getInt(cursor.getColumnIndex(
                                    TableNameAndColumnStatement.SOURCE_TASK_ID)))) {
                                unSyncedIdList.add(cursor.getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.SOURCE_TASK_ID)));
                            }
                        }
                    } while (cursor.moveToNext());

                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                if (unSyncedIdList != null && unSyncedIdList.size() != 0) {
                    for (Integer taskId : unSyncedIdList) {
                        updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.TASK_ID +
                                " = " + taskId, TableNameAndColumnStatement.IS_ISSUES_SYNCED);
                    }
                }
                sqlite.close();
            }
        }

    }

    private void updateAllDependentModulesSynced(SQLiteDatabase sqLiteDatabase, String whereClause, String whichColumn) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED, 0);
            contentValues.put(whichColumn, Constants.NEG_DEFAULT_COUNT);
            sqLiteDatabase.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, contentValues, whereClause, null);

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void uploadUnsyncedStrength() {
        ArrayList<Integer> unSyncedIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.TASK_ID
                + " from " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE
                + " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0;
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance()
                    .getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(
                                TableNameAndColumnStatement.TASK_ID)) != 0) {
                            if (!unSyncedIdList.contains(cursor.getInt(cursor.getColumnIndex(
                                    TableNameAndColumnStatement.TASK_ID)))) {
                                unSyncedIdList.add(cursor.getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.TASK_ID)));
                            }
                        }
                    } while (cursor.moveToNext());

                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                if (unSyncedIdList != null && unSyncedIdList.size() != 0) {
                    for (Integer taskId : unSyncedIdList) {
                        updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.TASK_ID +
                                " = " + taskId, TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED);
                    }
                }
                sqlite.close();
            }
        }
    }

    public void uploadUnsyncedKitRequests() {
        ArrayList<Integer> unSyncedIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.SOURCE_TASK_ID
                + " from " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE
                + " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0
                + " and " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + " = " + 0;
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance()
                    .getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(
                                TableNameAndColumnStatement.SOURCE_TASK_ID)) != 0) {
                            if (!unSyncedIdList.contains(cursor.getInt(cursor.getColumnIndex(
                                    TableNameAndColumnStatement.SOURCE_TASK_ID)))) {
                                unSyncedIdList.add(cursor.getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.SOURCE_TASK_ID)));
                            }
                        }
                    } while (cursor.moveToNext());

                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                if (unSyncedIdList != null && unSyncedIdList.size() != 0) {
                    for (Integer taskId : unSyncedIdList) {
                        updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.TASK_ID +
                                " = " + taskId, TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED);
                    }
                }
                sqlite.close();
            }
        }
    }

    public void uploadUnsyncedImprovementPlans() {
        ArrayList<Integer> unSyncedIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.TASK_ID
                + " from " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE
                + " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0
                + " and " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + " = " + 0;
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance()
                    .getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(
                                TableNameAndColumnStatement.SOURCE_TASK_ID)) != 0) {
                            if (!unSyncedIdList.contains(cursor.getInt(cursor.getColumnIndex(
                                    TableNameAndColumnStatement.SOURCE_TASK_ID)))) {
                                unSyncedIdList.add(cursor.getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.SOURCE_TASK_ID)));
                            }
                        }
                    } while (cursor.moveToNext());

                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                if (unSyncedIdList != null && unSyncedIdList.size() != 0) {
                    for (Integer taskId : unSyncedIdList) {
                        updateAllDependentModulesSynced(sqlite, TableNameAndColumnStatement.TASK_ID +
                                " = " + taskId, TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED);
                    }
                }
                sqlite.close();
            }
        }
    }
}
