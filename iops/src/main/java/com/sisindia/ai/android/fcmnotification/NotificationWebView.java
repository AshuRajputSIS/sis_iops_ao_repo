package com.sisindia.ai.android.fcmnotification;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NotificationWebView extends BaseActivity {

    @Bind(R.id.notification_webview)
    WebView notificationWebview;
    @Bind(R.id.webview_msg_header)
    CustomFontTextview webviewMsgHeader;
    private String notificationUrl;
    private String notificationMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_web_view);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            notificationUrl = getIntent().getExtras().getString(getResources().getString(R.string.notification_url));
            notificationMsg = getIntent().getExtras().getString(getResources()
                    .getString(R.string.notification_msg));
        }
        if (!TextUtils.isEmpty(notificationMsg)) {
            webviewMsgHeader.setText(notificationMsg);
        }
        setWebView();
    }

    private void setWebView() {
        notificationWebview.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.contains("http")) {
                    try {
                        view.loadUrl(URLDecoder.decode(url.trim(), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                } else {
                    view.loadUrl("http://" + url);
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(NotificationWebView.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {

            }

            public void onPageFinished(WebView view, String url) {

                if (progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }
            }
        });
        // Javascript inabled on webview
        notificationWebview.getSettings().setJavaScriptEnabled(true);
        notificationWebview.getSettings().setLayoutAlgorithm(WebSettings
                .LayoutAlgorithm.TEXT_AUTOSIZING);
        notificationWebview.getSettings().setLoadWithOverviewMode(true);
        notificationWebview.getSettings().setUseWideViewPort(true);
        notificationWebview.setScrollBarStyle(WebView.SCROLL_AXIS_NONE);
        notificationWebview.setScrollbarFadingEnabled(false);
        notificationWebview.getSettings().setBuiltInZoomControls(false);
        notificationWebview.getSettings().setDisplayZoomControls(true);
        notificationWebview.getSettings().setSupportZoom(true);
        if (notificationUrl != null && notificationUrl.contains("http")) {
            try {
                notificationWebview.loadUrl(URLDecoder.decode(notificationUrl.trim(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } else {
            notificationWebview.loadUrl("http://" + notificationUrl);
        }

    }
}
