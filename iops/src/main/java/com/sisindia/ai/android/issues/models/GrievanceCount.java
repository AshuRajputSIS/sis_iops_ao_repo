package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.SerializedName;

public class GrievanceCount {

@SerializedName("CountMorethan7Days")
private Integer countMorethan7Days;
@SerializedName("CountMorethan2Days")
private Integer countMorethan2Days;
@SerializedName("CountLessthan2Days")
private Integer countLessthan2Days;

private Integer localCountLessthan2Days;


/**
* 
* @return
* The countMorethan7Days
*/
public Integer getCountMorethan7Days() {
return countMorethan7Days;
}

/**
* 
* @param countMorethan7Days
* The CountMorethan7Days
*/
public void setCountMorethan7Days(Integer countMorethan7Days) {
this.countMorethan7Days = countMorethan7Days;
}

/**
* 
* @return
* The countMorethan2Days
*/
public Integer getCountMorethan2Days() {
return countMorethan2Days;
}

/**
* 
* @param countMorethan2Days
* The CountMorethan2Days
*/
public void setCountMorethan2Days(Integer countMorethan2Days) {
this.countMorethan2Days = countMorethan2Days;
}

/**
* 
* @return
* The countLessthan2Days
*/
public Integer getCountLessthan2Days() {
return countLessthan2Days;
}

/**
* 
* @param countLessthan2Days
* The CountLessthan2Days
*/
public void setCountLessthan2Days(Integer countLessthan2Days) {
this.countLessthan2Days = countLessthan2Days;
}

    public Integer getLocalCountLessthan2Days() {
        return localCountLessthan2Days;
    }

    public void setLocalCountLessthan2Days(Integer localCountLessthan2Days) {
        this.localCountLessthan2Days = localCountLessthan2Days;
    }
}