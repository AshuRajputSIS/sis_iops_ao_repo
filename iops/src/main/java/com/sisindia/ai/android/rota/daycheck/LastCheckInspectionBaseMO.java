package com.sisindia.ai.android.rota.daycheck;


import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;

import java.util.ArrayList;

public class LastCheckInspectionBaseMO {
    private ArrayList<Object> lastCheckInspectionBaseMOList;
    private LastCheckInspectionHeaderMO lastCheckInspectionHeaderMO;
    private ArrayList<LastCheckInspectionGrievancesMO> lastInspectionGrievancesMOList;
    private ArrayList<LastCheckInspectionComplaintsMO> lastInspectionComplaintsMOList;
    private ArrayList<LastInspectionReviewAdapter.LastInspectionUnitAtRiskHolder> lastInspectionUnitAtRiskHolderArrayList;
    private ArrayList<KitItemRequestMO> lastInKitItemRequestMOList;

    public ArrayList<KitItemRequestMO> getLastInKitItemRequestMOList() {
        return lastInKitItemRequestMOList;
    }

    public void setLastInKitItemRequestMOList(ArrayList<KitItemRequestMO> lastInKitItemRequestMOList) {
        this.lastInKitItemRequestMOList = lastInKitItemRequestMOList;
    }




    public ArrayList<LastInspectionReviewAdapter.LastInspectionUnitAtRiskHolder> getLastInspectionUnitAtRiskHolderArrayList() {
        return lastInspectionUnitAtRiskHolderArrayList;
    }

    public void setLastInspectionUnitAtRiskHolderArrayList(ArrayList<LastInspectionReviewAdapter.LastInspectionUnitAtRiskHolder> lastInspectionUnitAtRiskHolderArrayList) {
        this.lastInspectionUnitAtRiskHolderArrayList = lastInspectionUnitAtRiskHolderArrayList;
    }



    public ArrayList<Object> getLastCheckInspectionBaseMOList() {
        return lastCheckInspectionBaseMOList;
    }

    public void setLastCheckInspectionBaseMOList(ArrayList<Object> lastCheckInspectionBaseMOList) {
        this.lastCheckInspectionBaseMOList = lastCheckInspectionBaseMOList;
    }

    public LastCheckInspectionHeaderMO getLastCheckInspectionHeaderMO() {
        return lastCheckInspectionHeaderMO;
    }

    public void setLastCheckInspectionHeaderMO(LastCheckInspectionHeaderMO lastCheckInspectionHeaderMO) {
        this.lastCheckInspectionHeaderMO = lastCheckInspectionHeaderMO;
    }



    public ArrayList<LastCheckInspectionComplaintsMO> getLastInspectionComplaintsMOList() {
        return lastInspectionComplaintsMOList;
    }

    public void setLastInspectionComplaintsMOList(ArrayList<LastCheckInspectionComplaintsMO> lastInspectionComplaintsMOList) {
        this.lastInspectionComplaintsMOList = lastInspectionComplaintsMOList;
    }

    public ArrayList<LastCheckInspectionGrievancesMO> getLastInspectionGrievancesMOList() {
        return lastInspectionGrievancesMOList;
    }

    public void setLastInspectionGrievancesMOList(ArrayList<LastCheckInspectionGrievancesMO> lastInspectionGrievancesMOList) {
        this.lastInspectionGrievancesMOList = lastInspectionGrievancesMOList;
    }
}
