package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseRecruitmentOR {

@SerializedName("StatusCode")
@Expose
private Integer statusCode;
@SerializedName("StatusMessage")
@Expose
private String statusMessage;
@SerializedName("Data")
@Expose
private RecuirtementMO data;

public Integer getStatusCode() {
return statusCode;
}

public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

public String getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

public RecuirtementMO getData() {
return data;
}

public void setData(RecuirtementMO data) {
this.data = data;
}

}