package com.sisindia.ai.android.unitraising;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sisindia.ai.android.commons.Constants.UNIT_RAISING_DEFERRED_OR_CANCELLED;

/**
 * Created by Ashu Rajput on 7/25/2017.
 */

public class UnitRaisingDeferredOrCancelled extends DayCheckBaseActivity implements View.OnClickListener {


    @Bind(R.id.unit_name)
    CustomFontTextview unitName;
    @Bind(R.id.unit_name_text)
    CustomFontTextview unitNameText;
    @Bind(R.id.reasonForChangeOrCancelLabel)
    CustomFontTextview reasonForChangeOrCancelLabel;
    @Bind(R.id.raisingDateTV)
    CustomFontTextview raisingDateTV;
    @Bind(R.id.raisingDateLayout)
    LinearLayout raisingDateLayout;
    @Bind(R.id.add_unit_image_tv)
    CustomFontTextview addUnitImageTv;
    @Bind(R.id.add_unit_image)
    RoundedCornerImageView addUnitImage;
    @Bind(R.id.raising_image_tv)
    CustomFontTextview raisingImageTv;
    @Bind(R.id.linearLayout_photos)
    LinearLayout linearLayoutPhotos;
    @Bind(R.id.remarks_edt)
    CustomFontEditText remarksEt;
    @Bind(R.id.reason_for_change_or_cancel)
    CustomFontEditText reasonForChangeOrCancel;
    @Bind(R.id.linearLayout)
    LinearLayout linearLayout;
    @Bind(R.id.planned_raising_date_tv)
    CustomFontTextview plannedRaisingDate;
    private UnitRaisingDeferredOrCancelledMo unitRaisingDeferredOrCancelledMo;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    private String receivedTitle = "";
    private RaisingDetailsMo raisingDetailsMo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unit_raising_deferred_cancelled);
        ButterKnife.bind(this);
        unitRaisingDeferredOrCancelledMo = new UnitRaisingDeferredOrCancelledMo();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        captureEditTextData();

        Bundle receivedBundle = getIntent().getExtras();

        if (receivedBundle != null) {
            if (receivedBundle.containsKey("UNIT_RAISING_TITLE")) {
                receivedTitle = receivedBundle.getString("UNIT_RAISING_TITLE");

                if (receivedTitle.equalsIgnoreCase("RAISING CANCELLED")) {
                    raisingDateLayout.setVisibility(View.GONE);
                    reasonForChangeOrCancelLabel.setText(getResources()
                            .getString(R.string.unitReasonForCancel));
                }
            }
            if(receivedBundle.containsKey(TableNameAndColumnStatement.RAISING_DETAILS)){
                raisingDetailsMo = (RaisingDetailsMo)receivedBundle.getSerializable(TableNameAndColumnStatement.RAISING_DETAILS);

            }
        }
        setUnitRaisingData();
        setUpToolBar(receivedTitle);
        addUnitImage.setOnClickListener(this);
    }

    private void setUnitRaisingData() {
        unitNameText.setText(raisingDetailsMo.unitName);
        plannedRaisingDate.setText(dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(
                raisingDetailsMo.expectedRaisingDate));
        unitRaisingDeferredOrCancelledMo.unitId = raisingDetailsMo.unitId;
        unitRaisingDeferredOrCancelledMo.plannedRaisingDate = raisingDetailsMo.expectedRaisingDate;
    }

    private void captureEditTextData() {
        reasonForChangeOrCancel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if("RAISING DEFERRED".equalsIgnoreCase(receivedTitle)){
                    unitRaisingDeferredOrCancelledMo.reasonForChange = s.toString();
                }
                if("RAISING CANCELLED".equalsIgnoreCase(receivedTitle)){
                    unitRaisingDeferredOrCancelledMo.reasonForCancel = s.toString();
                }
            }
        });
        remarksEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                unitRaisingDeferredOrCancelledMo.remarks = s.toString();
            }
        });
    }

    @OnClick(R.id.raisingDateTV)
    public void raisingDateClicker() {
        SelectDateFragment selectDateFragment = new SelectDateFragment(raisingDateTV, null);
        selectDateFragment.show(getSupportFragmentManager()
                .beginTransaction(), getResources().getString(R.string.DATE_PICKER_FRAGMENT_NAME));
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                raisingDateTV.setText(updateData);
                unitRaisingDeferredOrCancelledMo.newRaisingDate = dateTimeFormatConversionUtil
                        .convertDayMonthDateYearToDateFormat(updateData);
            }
        });
    }

    private void setUpToolBar(String receivedTitle) {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(receivedTitle);
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unit_raising, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.submit) {
            if(receivedTitle != null){
                if(receivedTitle.equalsIgnoreCase("RAISING DEFERRED")){
                    validateFields(false);
                }
                else if(receivedTitle.equalsIgnoreCase("RAISING CANCELLED")){
                    validateFields(true);
                }
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private void validateFields(boolean isCancellation) {
        if(isCancellation){
            if(TextUtils.isEmpty(unitRaisingDeferredOrCancelledMo.reasonForCancel)){
                snackBarWithMesg(linearLayout, getResources().getString(R.string.MSG_REASON_FOR_CANCEL));
                return;
            }
        }
        else {
            if(TextUtils.isEmpty(unitRaisingDeferredOrCancelledMo.reasonForChange)){
                snackBarWithMesg(linearLayout, getResources().getString(R.string.MSG_REASON_FOR_CHANGE));
                return;
            }
            if(TextUtils.isEmpty(unitRaisingDeferredOrCancelledMo.newRaisingDate)){
                snackBarWithMesg(linearLayout, getResources().getString(R.string.MSG_NEW_RAISING_DATE));
                return;
            }
        }
         if(isCancellation) {
                IOPSApplication
                        .getInstance()
                        .getUnitRaisingInsertionStatementDB()
                        .insertUnitRaisingCancelledDetails(unitRaisingDeferredOrCancelledMo);
                  IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().
                     updateUnitStatus(raisingDetailsMo.unitId,0);
             setupMetaData( IOPSApplication.getInstance().getUnitRaisingSelectStatementDB()
                     .getUnitRaisingDeferredOrCancellationLocalId(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED,
                             TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID));

            }
            else{
                IOPSApplication
                        .getInstance()
                        .getUnitRaisingInsertionStatementDB()
                        .insertUnitRaisingDeferredDetails(unitRaisingDeferredOrCancelledMo);
             setupMetaData( IOPSApplication.getInstance().getUnitRaisingSelectStatementDB()
                     .getUnitRaisingDeferredOrCancellationLocalId(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED,
                             TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID));

            }
            triggerSyncAdapter();
            finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if(attachmentMetaArrayList != null)
            attachmentMetaArrayList.clear();
            String imagePath = ((AttachmentMetaDataModel)
                    data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
            if (requestCode == UNIT_RAISING_DEFERRED_OR_CANCELLED) {
                addUnitImage.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent capturePictureActivity = new Intent(UnitRaisingDeferredOrCancelled.this,
                CapturePictureActivity.class);
        if (null != receivedTitle && receivedTitle.equalsIgnoreCase("RAISING DEFERRED")) {
            Util.setSendPhotoImageTo(Util.UNIT_RAISING_DEFERRED);
        } else if (null != receivedTitle && receivedTitle.equalsIgnoreCase("RAISING CANCELLED")) {
            Util.setSendPhotoImageTo(Util.UNIT_RAISING_CANCELLED);
        }

        Util.initMetaDataArray(true);
        capturePictureActivity.putExtra(TableNameAndColumnStatement.UNIT_ID, raisingDetailsMo.unitId);
        capturePictureActivity.putExtra(TableNameAndColumnStatement.UNIT_NAME, raisingDetailsMo.unitName);
        startActivityForResult(capturePictureActivity, UNIT_RAISING_DEFERRED_OR_CANCELLED);
    }
    public void setupMetaData(int rasingOrCancelDeferredId){
        if (attachmentMetaArrayList != null && attachmentMetaArrayList.size() > 0) {
            for(int i = 0; i < attachmentMetaArrayList.size() ; i++){
                if(attachmentMetaArrayList.get(i).getAttachmentSourceTypeId()
                        == Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED,this)||
                        attachmentMetaArrayList.get(i).getAttachmentSourceTypeId()
                                == Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED,this))
                {
                    attachmentMetaArrayList.get(i).setAttachmentSourceId(rasingOrCancelDeferredId);
                }
            }
            IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTableFromHashMap(
                    attachmentMetaArrayList);
            attachmentMetaArrayList.clear();
        }
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);

    }
}
