package com.sisindia.ai.android.database.issueDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.issues.models.IssueClosureMo;
import com.sisindia.ai.android.network.response.IssuesData;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 04-08-2016.
 */
public class IssueLevelUpdateStatementDB extends SISAITrackingDB {
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public IssueLevelUpdateStatementDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }
   public  void updateIssuesStatus(int id,IssueClosureMo issueClosureMo){

       String updateQuery;

       if(issueClosureMo != null && issueClosureMo.getStatusId() == 3){
           updateQuery = "UPDATE " + TableNameAndColumnStatement.ISSUES_TABLE+
                   " SET " + TableNameAndColumnStatement.ISSUE_STATUS_ID+ " = " + issueClosureMo.getStatusId()+
                   " ," + TableNameAndColumnStatement.IS_NEW+ " = " + 1 +
                   " ," + TableNameAndColumnStatement.IS_SYNCED+ " =  "+ 0 +
                   " ," + TableNameAndColumnStatement.CLOSURE_REMARKS+ "='"+ issueClosureMo.getClosureRemarks()+
                   "' ," + TableNameAndColumnStatement.CLOSURE_END_DATE+ "='"+ issueClosureMo.getClosureEndDate() +
                   "' WHERE " +
                   TableNameAndColumnStatement.ID+ " = " + id;
       }
       else {
           updateQuery = "UPDATE " + TableNameAndColumnStatement.ISSUES_TABLE+
                   " SET " + TableNameAndColumnStatement.ISSUE_STATUS_ID+ " = " + issueClosureMo.getStatusId()+
                   " ," + TableNameAndColumnStatement.IS_NEW+ " = " + 1 +
                   " ," + TableNameAndColumnStatement.IS_SYNCED+ " =  "+ 0 +
                   " WHERE " +
                   TableNameAndColumnStatement.ID+ " = " + id;
       }




        Timber.d("UpdateIssueTable  %s", updateQuery);
       Cursor cursor = null;
       SQLiteDatabase sqlite = null;
       try {
           sqlite = this.getWritableDatabase();
           cursor = sqlite.rawQuery(updateQuery, null);
           Timber.d("updateQuery  %s", cursor.getCount());
       } catch (Exception exception) {
           Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
           Crashlytics.logException(exception);
       }finally {
           if (null != sqlite && sqlite.isOpen()) {
               sqlite.close();
           }
       }

   }

    public synchronized void updateIssueId(int id, IssuesData actionPlanMO){


        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ISSUES_TABLE +
                " SET " + TableNameAndColumnStatement.ISSUE_ID + "='" + actionPlanMO.getIssueId() + "'" +
                "," + TableNameAndColumnStatement.ASSIGNED_TO_NAME  + "='"+ actionPlanMO.getAssigneeName() + "'"+
                " ," + TableNameAndColumnStatement.ASSIGNED_TO_ID + "='"+ actionPlanMO.getAssignedToId() + "'"+
                "," + TableNameAndColumnStatement.GUARD_ID + "='"+ actionPlanMO.getGuardId() + "'"+
                " ," + TableNameAndColumnStatement.GUARD_NAME + "='"+ actionPlanMO.getGuardName() + "'"+
                " ," + TableNameAndColumnStatement.IS_NEW + "= " +  0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED  + "= "+ 1 +
                " WHERE "+
                TableNameAndColumnStatement.ID + "='"+id+"'";

        Timber.d("UpdateIssueTable  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updateQuery  %s", cursor.getCount());
        }catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        }finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void updateImprovementPlanDetails(int taskId, Integer seqId, ActionPlanMO improvementPlanIRModel){
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            if(improvementPlanIRModel != null){
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID,improvementPlanIRModel.getIssueMatrixId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID,improvementPlanIRModel.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID,improvementPlanIRModel.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID,improvementPlanIRModel.getBarrackId());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME,dateTimeFormatConversionUtil.getCurrentDateTime());
                    values.put(TableNameAndColumnStatement.ISSUE_ID,improvementPlanIRModel.getIssueId());
                    values.put(TableNameAndColumnStatement.ACTION_PLAN_ID,improvementPlanIRModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE,improvementPlanIRModel.getExpectedClosureDate());
                    values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID,improvementPlanIRModel.getTargetEmployeeId());
                    values.put(TableNameAndColumnStatement.STATUS_ID,improvementPlanIRModel.getActionPlanStatusId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID,improvementPlanIRModel.getImprovementPlanCategoryId());
                    values.put(TableNameAndColumnStatement.REMARKS,improvementPlanIRModel.getRemarks());
                    values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME,improvementPlanIRModel.getExpectedClosureDate());
                    if(taskId != 0){
                        values.put(TableNameAndColumnStatement.TASK_ID,taskId);
                    }
                    //values.put(TableNameAndColumnStatement.CLOSED_BY_ID,improvementPlanIRModel.get);
                    values.put(TableNameAndColumnStatement.CLOSURE_COMMENT,improvementPlanIRModel.getClosureComment());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,improvementPlanIRModel.getAssigneeName());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID,improvementPlanIRModel.getAssignedToId());
                    values.put(TableNameAndColumnStatement.IS_NEW,0);
                    values.put(TableNameAndColumnStatement.IS_SYNCED,1);
                    sqlite.insert(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, null, values);
                    sqlite.setTransactionSuccessful();
                }
                catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                }
                finally {
                    if(sqlite != null) {
                        sqlite.endTransaction();
                        sqlite.close();
                    }
                }
            }
        }
    }

    public synchronized void updateIssueSyncStatus(int issueId){
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ISSUES_TABLE +
                " SET "+ TableNameAndColumnStatement.IS_NEW + " = " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + " = "+ 1 +
                " WHERE "+ TableNameAndColumnStatement.ISSUE_ID + "='"+issueId+"'";
        Timber.d("UpdateIssueSyncStatus  %s", updateQuery);
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        }finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }
}
