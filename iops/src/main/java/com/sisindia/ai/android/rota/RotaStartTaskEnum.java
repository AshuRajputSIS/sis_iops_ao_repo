package com.sisindia.ai.android.rota;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Saruchi on 27-01-2017.
 */

public enum RotaStartTaskEnum {
    Active(1),
    InProgress(2),
    InActive(3),
    OnCompleted(200);

    private static Map map = new HashMap<>();

    static {
        for (RotaStartTaskEnum rotaStartTask : RotaStartTaskEnum.values()) {
            map.put(rotaStartTask.value, rotaStartTask);
        }
    }

    private int value;

    RotaStartTaskEnum(int value) {
        this.value = value;
    }

    public static RotaStartTaskEnum valueOf(int rotaStartTask) {
        return (RotaStartTaskEnum) map.get(rotaStartTask);
    }

    public int getValue() {
        return value;
    }
}
