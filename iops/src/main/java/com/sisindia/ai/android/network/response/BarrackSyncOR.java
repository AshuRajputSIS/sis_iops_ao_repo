package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 8/9/16.
 */

public class BarrackSyncOR {
    @SerializedName("barrackId")
    @Expose
    private Integer barrackId;

    /**
     *
     * @return
     * The barrackId
     */
    public Integer getBarrackId() {
        return barrackId;
    }

    /**
     *
     * @param barrackId
     * The barrackId
     */
    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }
}
