package com.sisindia.ai.android.rota;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.daycheck.StepperDataMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OthersActivity extends DayCheckBaseActivity implements View.OnClickListener {

    private static final int OTHER_ACTIVITY_IMAGE_CODE = 100;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.other_reason)
    TextView OtherReasonTv;
    @Bind(R.id.description)
    EditText taskDescription;
    @Bind(R.id.photosLabel)
    CustomFontTextview photosLabel;
    @Bind(R.id.takePhoto)
    ImageView takePhoto;
    @Bind(R.id.linearLayout_photos)
    LinearLayout linearLayoutPhotos;
    @Bind(R.id.horizontal)
    HorizontalScrollView horizontal;
    @Bind(R.id.othersMainLayout)
    RelativeLayout othersMainLayout;
    private RoundedCornerImageView takenImageView;
    private ArrayList<String> imageUrlList;
    //    private LookUpSpinnerAdapter otherActivityReasonAdapter;
//    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    private OtherActivityIR otherActivityIR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.Others));
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        setUpViews();
        imageUrlList = new ArrayList<>();
    }

    private void setUpViews() {
        otherActivityIR = new OtherActivityIR();

        if (rotaTaskModel != null) {
//            issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
            String lookUpName = IOPSApplication.getInstance().getSelectionStatementDBInstance().getOtherReason(getResources().getString(R.string.OtherActivity), null,
                    rotaTaskModel.getOtherTaskReasonId());
            otherActivityIR.setReasonId(rotaTaskModel.getOtherTaskReasonId());

            if (!lookUpName.isEmpty()) {
                OtherReasonTv.setText(lookUpName);
            }
        }

        takePhoto.setOnClickListener(this);
    }

    private void loadThumbNailImages(ArrayList<String> imageUrls) {
        if (linearLayoutPhotos != null) {
            linearLayoutPhotos.removeAllViews();
            linearLayoutPhotos.addView(takePhoto);
        }

        if (imageUrls != null && imageUrls.size() > 0) {
            for (int index = imageUrls.size() - 1; index >= 0; index--) {
                ImageView imageView = createImageView(Uri.parse(imageUrls.get(index)));
                linearLayoutPhotos.addView(imageView);
            }
        }
    }

    public ImageView createImageView(Uri uri) {

        int marginBottom = Util.dpToPx(Constants.MARGIN_BOTTOM);
        int marginLeft = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginRight = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginTop = Util.dpToPx(Constants.MARGIN_TOP);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.HEIGT);
        params.width = Util.dpToPx(Constants.HEIGT);
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);

        takenImageView = new RoundedCornerImageView(this);
        takenImageView.setLayoutParams(params);
//        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.camer_taken_pic_image_border);
        takenImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        takenImageView.setImageURI(uri);
        return takenImageView;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OTHER_ACTIVITY_IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
            setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            imageUrlList.add(imagePath);
            loadThumbNailImages(imageUrlList);
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        Intent capturePictureActivity = null;
        Util.mSequenceNumber++;
        switch (viewId) {
            case R.id.takePhoto:
                capturePictureActivity = new Intent(OthersActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.OTHER_ACTIVITY);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, OTHER_ACTIVITY_IMAGE_CODE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_others, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done_menu) {
            validateFields();
            return true;
        } else if (id == android.R.id.home) {
            clearSdCardImages();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        clearSdCardImages();
        super.onBackPressed();
    }

    private void validateFields() {

        if (appPreferences.isEnableGps(OthersActivity.this)) {

            Util.showProgressBar(OthersActivity.this, R.string.saveMessage);
            insertUpdateLocalDBAndSync();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Util.dismissProgressBar(true);
                    Intent homeActivityIntent = new Intent(OthersActivity.this, HomeActivity.class);
                    homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeActivityIntent);
                    finish();
                }
            }, 2500);

        } else {
            showGpsDialog(getString(R.string.gps_configure_msg));
        }
    }

    private void insertUpdateLocalDBAndSync() {
        try {
            otherActivityIR.setDescription(taskDescription.getText().toString());
            OtherActivityBase otherActivityBase = new OtherActivityBase();
            String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
            otherActivityBase.setOtherActivity(otherActivityIR);
            IOPSApplication.getInstance().getUpdateStatementDBInstance().updateTaskResult(rotaTaskModel.getTaskId(),
                    IOPSApplication.getGsonInstance().toJson(otherActivityBase), RotaTaskStatusEnum.Completed.getValue(),
                    dateTimeFormatConversionUtil.getCurrentDateTime().toString(), geoLocation);

            IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);

            if (rotaTaskModel.getTaskId() < 0) {
                if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                    updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                            .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
                }
            }

            IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(new Bundle());

            IOPSApplication.getInstance().getMyPerformanceStatementsDB()
                    .UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(), dateTimeFormatConversionUtil.getCurrentTime(),
                            String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());

            insertLogActivity();

            StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().clear();
            if (attachmentMetaArrayList != null && attachmentMetaArrayList.size() > 0)
                attachmentMetaArrayList.clear();

            otherActivityBase = null;

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearSdCardImages() {
        if (attachmentMetaArrayList != null && attachmentMetaArrayList.size() > 0) {
            for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
                if (attachmentMetaDataModel != null) {
                    File file = new File(attachmentMetaDataModel.getAttachmentPath());
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                }
            }
        }
    }
}
