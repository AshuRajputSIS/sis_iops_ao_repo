package com.sisindia.ai.android.myunits;

import android.view.View;

/**
 * Created by Durga Prasad on 12-07-2016.
 */
public interface OnViewHistoryClickListener {

    void onViewHistoryClick(View view,int position,int taskTypeId);
}
