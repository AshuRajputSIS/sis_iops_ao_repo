package com.sisindia.ai.android.rota.clientcoordination;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionsData;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shivam on 18/7/16.
 */

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionsHolder> {
    private static final String TAG = QuestionsAdapter.class.getSimpleName();

    private Context mContext;
    private CsiListener csiListener;
    private List<QuestionsData> questions = new ArrayList<>();
    private DateTimeFormatConversionUtil dateTimeUtil;

    private int unitId;
    private String targetDate;

    private ImprovementActionPlanMO improvementPlan;

    QuestionsAdapter(Context context, CsiListener listener) {
        mContext = context;
        csiListener = listener;
    }

    @Override
    public QuestionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.client_questions_item, parent, false);
        return new QuestionsHolder(view);
    }

    @Override
    public void onBindViewHolder(final QuestionsHolder holder, final int position) {
        holder.tvQuestions.setText(questions.get(position).question);
        final RadioGroup rgUnitPerformance = holder.rgClientRating;
        final int questionId = questions.get(position).questionId;

        rgUnitPerformance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rgUnitPerformance.setTag(checkedId);

                switch (checkedId) {
                    case -1:
                        valueChanged(questionId, 0);
                        break;
                    case R.id.rb_one:
                        valueChanged(questionId, 1);
                        showLowFeedback(questionId, holder);
                        break;
                    case R.id.rb_two:
                        valueChanged(questionId, 2);
                        showLowFeedback(questionId, holder);
                        break;
                    case R.id.rb_three:
                        valueChanged(questionId, 3);
                        showLowFeedback(questionId, holder);
                        /*holder.llLowFeedback.setVisibility(View.GONE);
                        removeLowFeedback(questionId);*/
                        break;
                    case R.id.rb_four:
                        valueChanged(questionId, 4);
                        holder.llLowFeedback.setVisibility(View.GONE);
                        removeLowFeedback(questionId);
                        break;
                    case R.id.rb_five:
                        valueChanged(questionId, 5);
                        holder.llLowFeedback.setVisibility(View.GONE);
                        removeLowFeedback(questionId);
                        break;
                }
            }
        });

        if (rgUnitPerformance.getTag() != null) {
            int selectedId = (int) rgUnitPerformance.getTag();
            rgUnitPerformance.check(selectedId);
        }
    }

    private void removeLowFeedback(int questionId) {
        csiListener.lowFeedbackRemoved(questionId);
    }

    private void showLowFeedback(final int questionId, final QuestionsHolder holder) {

        IssueLevelSelectionStatementDB issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(mContext);
        final List<ImprovementActionPlanMO> improvementPlanList = issueLevelSelectionStatementDB
                .getClientImprovementPlanList(unitId, ClientCoordinationFragment.ISSUE_TYPE_ID, questionId);

        if (improvementPlanList == null || improvementPlanList.size() == 0) {
            Snackbar.make(holder.itemView, mContext.getString(R.string.improvement_plan_error), Snackbar.LENGTH_LONG);
            return;
        }

        if (improvementPlanList.size() == 1) {
            holder.spinnerIssues.setBackgroundColor(0);
            holder.spinnerIssues.setClickable(false);
        }

        final String[] improvementPlans = new String[improvementPlanList.size()];
        for (int i = 0; i < improvementPlanList.size(); i++) {
            improvementPlans[i] = improvementPlanList.get(i).getName();
        }
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, improvementPlans);

        improvementPlan = improvementPlanList.get(0);
        holder.spinnerIssues.setAdapter(spinnerAdapter);
        holder.spinnerIssues.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                improvementPlan = improvementPlanList.get(position);
                improvementPlanSelected(questionId, holder);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        improvementPlanSelected(questionId, holder);
    }

    private void improvementPlanSelected(int questionId, final QuestionsHolder holder) {
        holder.llLowFeedback.setVisibility(View.VISIBLE);

        targetDate = getTargetDate(this.improvementPlan.getTurnAroundTime());
        holder.tvByDate.setText(String.format(mContext.getString(R.string.improvement_plan_date), targetDate));

        holder.ivSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date minDate = dateTimeUtil.convertStringToDateFormat(dateTimeUtil.getCurrentDate());
                Date maxDate = dateTimeUtil.convertStringToDateFormat(targetDate);
                FragmentManager fragmentManager = ((AppCompatActivity) mContext).getSupportFragmentManager();
                DateRangeCalender dateFragment = DateRangeCalender.newInstance(minDate, maxDate);
                dateFragment.show(fragmentManager, TAG);
                dateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String date) {
                        targetDate = dateTimeUtil.convertDayMonthDateYearToDateFormat(date);
                        holder.tvByDate.setText(String.format(mContext.getString(R.string.improvement_plan_date), targetDate));
                    }
                });
            }
        });
        submitLowFeedback(this.improvementPlan.getId(), questionId, targetDate, this.improvementPlan.getIssueMatrixId());
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    private void valueChanged(int questionId, int value) {
        csiListener.responseChange(questionId, value);
    }

    private void submitLowFeedback(int planId, int questionId, String turnAroundTime, int issueMatrixId) {
        csiListener.lowFeedback(planId, questionId, turnAroundTime, issueMatrixId);
    }

    void updateQuestions(int unitId) {
        this.unitId = unitId;
        questions = IOPSApplication.getInstance().getSelectionStatementDBInstance().getClientCoordinationQuestions();
        notifyDataSetChanged();
    }

    private String getTargetDate(int days) {
        dateTimeUtil = new DateTimeFormatConversionUtil();
        return dateTimeUtil.addDaysToCurrentDate(days);
    }

    class QuestionsHolder extends RecyclerView.ViewHolder {

        private RadioGroup rgClientRating;
        private TextView tvQuestions;
        private LinearLayout llLowFeedback;
        private Spinner spinnerIssues;
        private TextView tvByDate;
        private ImageView ivSelectDate;

        QuestionsHolder(View view) {
            super(view);
            rgClientRating = (RadioGroup) view.findViewById(R.id.rg_client_questions);
            tvQuestions = (TextView) view.findViewById(R.id.tv_questions_text);
            llLowFeedback = (LinearLayout) view.findViewById(R.id.ll_improvement_plan);
            spinnerIssues = (Spinner) view.findViewById(R.id.spinner_issues);
            tvByDate = (TextView) view.findViewById(R.id.tv_by_date);
            ivSelectDate = (ImageView) view.findViewById(R.id.iv_select_date);
        }
    }
}
