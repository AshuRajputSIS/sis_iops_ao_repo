package com.sisindia.ai.android.rota.daycheck;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.GenericDialogFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.IdCardMO;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.rotaDTO.RotaTempTaskIdUpdateStatementDB;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.daycheck.taskExecution.SingletonDayNightChecking;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.nightcheck.NightCheckEnum;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 24-06-2016.
 */
public class DayCheckBaseActivity extends BaseActivity {
    public static final int GREEN_CIRCLE = R.drawable.green_circle;
    public static final int GREY_CIRCLE = R.drawable.grey_circle;
    public static final int GREEN_LINE = R.color.color_green_barchart;
    public static final int GREY_LINE = R.color.greyish;
    protected static ArrayList<AttachmentMetaDataModel> attachmentMetaArrayList = new ArrayList<>();
    public DayCheckListAdapter mAdapter;
    public HashMap<Integer, View> stepperHashMapList;
    public HashMap<Integer, View> stepperLineConnectorList;
    protected KitItemRequestMO kitItemRequestMO;
    protected RotaTaskModel rotaTaskModel;
    protected LinearLayout completedTaskCountLayout;
    protected ArrayList<IssuesMO> issueModelList = null;
    protected TimeLineModel timeLineModel;
    protected KitDeliveredItemsIR kitDeliveredItemsIR;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private String unitName;
    private TextView totalTasksTextView;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    protected static ArrayList<EmployeeAuthorizedStrengthMO> updatedActualStrength;
 /*   public static HashMap<String,AttachmentMetaDataModel> attachmentMetaDataModelHashMap =
            new HashMap<>();*/

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    public ArrayList<AttachmentMetaDataModel> getAttachmentMetaArrayList() {
        return attachmentMetaArrayList;
    }


    public void setUnitStrength(ArrayList<EmployeeAuthorizedStrengthMO> updatedActualStrength,
                                boolean isFromDayCheck) {
        DayCheckBaseActivity.updatedActualStrength = updatedActualStrength;

    }

    public void setAttachmentMetaDataModel(AttachmentMetaDataModel attachmentMetaDataModel) {
        attachmentMetaArrayList.add(attachmentMetaDataModel);
    }

   /* public void setAttachmentMetaDataModelInMap(String key ,
                                                AttachmentMetaDataModel attachmentMetaDataModel) {
        this.attachmentMetaDataModel = attachmentMetaDataModel;
         attachmentMetaDataModelHashMap.put(key,attachmentMetaDataModel);
    }*/


    public DayCheckBaseActivity() {
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        kitItemRequestMO = new KitItemRequestMO();
        Object object = SingletonUnitDetails.getInstance().getUnitDetails();
        if (object instanceof RotaTaskModel) {
            rotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        }

    }


    public void setHeaderUnitName(String unitName) {
        this.unitName = unitName;
    }


    public void updateDayCheckUIStepper(int pos, int circleState) {
        for (Map.Entry<Integer, View> entry : stepperHashMapList.entrySet()) {
            Integer key = entry.getKey();
            View value = entry.getValue();
            if (pos == key) {
                value.setBackgroundResource(circleState);
            }
        }
    }

    public void updateNightCheckUIStepper(int pos, int circleState) {
        for (Map.Entry<Integer, View> entry : stepperHashMapList.entrySet()) {
            Integer key = entry.getKey();
            View value = entry.getValue();
            if (pos == key) {
                value.setBackgroundResource(circleState);
            }
        }
    }

    public void updateStepperConnector(int pos, HashMap<Integer, View> stepperLineConnectorList, int lineState) {
        switch (pos) {
            case 1:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
            case 2:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
            case 3:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
            case 4:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
            case 5:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
            case 6:
                stepperLineConnectorList.get(pos).setBackgroundResource(lineState);
                break;
        }
    }

    public void getStepperViews(View mStepperViewParent) {

        stepperHashMapList = new HashMap<>();
        TextView unitNameTextview = (TextView) mStepperViewParent.findViewById(R.id.stepperTitle);
        if (unitName != null)
            unitNameTextview.setText(unitName);
        TextView completedTasksTextView = (TextView) mStepperViewParent.findViewById(R.id.completed_task_count);
        totalTasksTextView = (TextView) mStepperViewParent.findViewById(R.id.total_task_count);
        completedTasksTextView.setText(String.valueOf(StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().size()));
        completedTaskCountLayout = (LinearLayout) mStepperViewParent.findViewById(R.id.completed_task_count_layout);
        totalTasksTextView.setText(String.valueOf(DayCheckEnum.values().length));

        addStepperList(mStepperViewParent);

        addStepperLineList(mStepperViewParent);


        HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
        if (null != mStepperDataHolder && mStepperDataHolder.size() != 0) {
            for (Map.Entry<String, Integer> entry : mStepperDataHolder.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                updateDayCheckUIStepper(value, GREEN_CIRCLE);
                // checkStepperProgress(value,mProgressConnect);
                updateStepperConnector(value, stepperLineConnectorList, GREEN_LINE);
            }
        }


    }

    private void addStepperLineList(View mStepperViewParent) {
        stepperLineConnectorList = new HashMap<>();
        stepperLineConnectorList.put(DayCheckEnum.valueOf(1).getValue(), mStepperViewParent.findViewById(R.id.stepper_one_completed));
        stepperLineConnectorList.put(DayCheckEnum.valueOf(2).getValue(), mStepperViewParent.findViewById(R.id.stepper_two_completed));
        stepperLineConnectorList.put(DayCheckEnum.valueOf(3).getValue(), mStepperViewParent.findViewById(R.id.stepper_three_completed));
        stepperLineConnectorList.put(DayCheckEnum.valueOf(4).getValue(), mStepperViewParent.findViewById(R.id.stepper_four_completed));
        stepperLineConnectorList.put(DayCheckEnum.valueOf(5).getValue(), mStepperViewParent.findViewById(R.id.stepper_five_completed));
        stepperLineConnectorList.put(DayCheckEnum.valueOf(6).getValue(), mStepperViewParent.findViewById(R.id.stepper_six_completed));

    }

    private void addStepperList(View mStepperViewParent) {
        stepperHashMapList.put(DayCheckEnum.valueOf(1).getValue(), mStepperViewParent.findViewById(R.id.task_strenght));
        stepperHashMapList.put(DayCheckEnum.valueOf(2).getValue(), mStepperViewParent.findViewById(R.id.task_guard_check));
        stepperHashMapList.put(DayCheckEnum.valueOf(3).getValue(), mStepperViewParent.findViewById(R.id.task_duty_check));
        stepperHashMapList.put(DayCheckEnum.valueOf(4).getValue(), mStepperViewParent.findViewById(R.id.task_client_check));
        stepperHashMapList.put(DayCheckEnum.valueOf(5).getValue(), mStepperViewParent.findViewById(R.id.task_security_check));
        stepperHashMapList.put(DayCheckEnum.valueOf(6).getValue(), mStepperViewParent.findViewById(R.id.client_handshake));
    }


    protected void getTaskExecutionResult() {
        dayNightCheckingBaseMO = SingletonDayNightChecking.getInstance().getDayNightCheckData();
        if (dayNightCheckingBaseMO != null) {
            dayNightChecking = dayNightCheckingBaseMO.getDayNightChecking();
        }
    }

    protected void setTaskExecutionResult() {
        SingletonDayNightChecking.getInstance().setDayNightCheckData(dayNightCheckingBaseMO);
    }

    protected void updateTotalTaskCount() {
        totalTasksTextView.setText(String.valueOf(NightCheckEnum.values().length));
    }

    public synchronized void insertLogActivity() {
        if (rotaTaskModel != null) {
            timeLineModel = new TimeLineModel();
            timeLineModel.setActivity("Check Out");
            timeLineModel.setLocation(rotaTaskModel.getUnitName());
            timeLineModel.setTaskId(String.valueOf(rotaTaskModel.getTaskId()));
            timeLineModel.setStatus(RotaTaskStatusEnum.Completed.name());
            timeLineModel.setTaskName(RotaTaskTypeEnum.valueOf(rotaTaskModel.getTaskTypeId()).name().replace("_", " "));
            timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
            timeLineModel.setEndTime("");
            timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());
            IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
        }
    }

    protected void showIdCardInfo(IdCardMO idCardMO) {
        GenericDialogFragment genericDialogFragment = GenericDialogFragment.newInstance();
        View contentView = LayoutInflater.from(this).inflate(R.layout.guard_details_dialog, null);

        IdsCardInfoViewHolder cardInfoViewHolder = new IdsCardInfoViewHolder(contentView);
        cardInfoViewHolder.guardName.setText(idCardMO.getName());
        cardInfoViewHolder.guardRegisterNo.setText(idCardMO.getRegNumber());
        cardInfoViewHolder.guardDateOfBirth.setText(idCardMO.getDateOfBirth());
        cardInfoViewHolder.guardEducation.setText(idCardMO.getEducation());
        cardInfoViewHolder.guardHeight.setText(idCardMO.getHeight());
        cardInfoViewHolder.guardWeigth.setText(idCardMO.getWeight());
        cardInfoViewHolder.guardBloodGroup.setText(idCardMO.getBloodGroup());
        cardInfoViewHolder.guardJoiningDate.setText(idCardMO.getDateOfJoining());
        cardInfoViewHolder.guardExperience.setText(idCardMO.getExperience());
        cardInfoViewHolder.guardIDCardExpiryDate.setText(idCardMO.getDateOfIdExpiry());
        View titleView = LayoutInflater.from(this).inflate(R.layout.dialog_title, null);
        genericDialogFragment.setContentView(contentView);
        genericDialogFragment.setTitleView(titleView, "Guard Details");
        genericDialogFragment.setTitleTextColor(getResources().getColor(R.color.background_red));
        genericDialogFragment.setPositiveButtonLabel("OK");
        getSupportFragmentManager().beginTransaction().add(genericDialogFragment, TAG).commitAllowingStateLoss();
        //genericDialogFragment.show(getSupportFragmentManager(), TAG);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (rotaTaskModel == null) {
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("DayCheckBase Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    class IdsCardInfoViewHolder {

        @Bind(R.id.guardName)
        public CustomFontTextview guardName;
        @Bind(R.id.guardRegisterNo)
        public CustomFontTextview guardRegisterNo;
        @Bind(R.id.guardDateOfBirth)
        public CustomFontTextview guardDateOfBirth;
        @Bind(R.id.guardEducation)
        public CustomFontTextview guardEducation;
        @Bind(R.id.guardHeight)
        public CustomFontTextview guardHeight;
        @Bind(R.id.guardWeigth)
        public CustomFontTextview guardWeigth;
        @Bind(R.id.guardBloodGroup)
        public CustomFontTextview guardBloodGroup;
        @Bind(R.id.guardJoiningDate)
        public CustomFontTextview guardJoiningDate;
        @Bind(R.id.guardExperience)
        public CustomFontTextview guardExperience;
        @Bind(R.id.guardIDCardExpiryDate)
        public CustomFontTextview guardIDCardExpiryDate;

        IdsCardInfoViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    public void showProgressbar() {
        Util.showProgressBar(this, R.string.loading_please_wait);

    }

    public void hideProgressbar() {
        Util.dismissProgressBar(isDestroyed());
    }

    public void updateTaskRelatedTables(int serverTaskId, int localTaskId) {
        RotaTempTaskIdUpdateStatementDB rotaTempTaskIdUpdateStatementDB = new RotaTempTaskIdUpdateStatementDB(this);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE,
                TableNameAndColumnStatement.TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.ISSUES_TABLE,
                TableNameAndColumnStatement.SOURCE_TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE,
                TableNameAndColumnStatement.SOURCE_TASK_ID);
        rotaTempTaskIdUpdateStatementDB.sdCardFileNameUpdate(serverTaskId, localTaskId);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.ACTIVITY_LOG_TABLE,
                TableNameAndColumnStatement.TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE,
                TableNameAndColumnStatement.TASK_ID);
    }
}
