package com.sisindia.ai.android.myunits.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class UnitHistoryBaseOR {

    @SerializedName("StatusCode")
    private Integer statusCode;

    @SerializedName("StatusMessage")
    private String statusMessage;


    @SerializedName("Data")
    private List<UnitHistoryMO> data = new ArrayList<UnitHistoryMO>();

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public List<UnitHistoryMO> getData() {
        return data;
    }

    public void setData(List<UnitHistoryMO> data) {
        this.data = data;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

/**
 *
 * @return
 * The data
 */


}