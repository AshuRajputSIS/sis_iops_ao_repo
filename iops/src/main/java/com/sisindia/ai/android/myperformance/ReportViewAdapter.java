package com.sisindia.ai.android.myperformance;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.rota.rotaCompliance.RoccReportViewActivity;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shruti on 17/7/16.
 */
public class ReportViewAdapter extends RecyclerView.Adapter {

    int resourceicon[] =
            {R.drawable.performance,
                    R.drawable.work_time,
                    R.drawable.task_prformance,
                    R.drawable.rota_compliance_icon,
                    R.drawable.rota_compliance_icon};

//    R.drawable.conveyance

    private List<Object> reportViewModelList = new ArrayList<>();
    private Context mContext;
    private static final int REPORT_VIEW = 1;
    private static final int TIMELINE_VIEW = 2;
    private static final int HEADER = 3;

    public ReportViewAdapter(List<Object> reportViewModelList, Context mContext) {
        this.reportViewModelList = reportViewModelList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view = null;
        switch (viewType) {
            case REPORT_VIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myperformance_report, parent, false);
                viewHolder = new ReportViewHolder(view);
                break;
            case TIMELINE_VIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myperformance_timeline_view, parent, false);
                viewHolder = new TimeLineViewHolder(view);
                break;
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_row, parent, false);
                viewHolder = new HeaderViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReportViewHolder && reportViewModelList.get(position) instanceof ReportViewModel) {
            ReportViewHolder reportViewHolder = (ReportViewHolder) holder;
            ReportViewModel reportViewModel = (ReportViewModel) reportViewModelList.get(position);
            //Bitmap largeIcon = BitmapFactory.decodeResource(mContext.getResources(), resourceicon[position]);

            if (position == 0) {
                reportViewHolder.reportIcon.setImageDrawable(mContext.getResources().getDrawable(resourceicon[position]));
                reportViewHolder.heading.setText(reportViewModel.getReportHeading());
                reportViewHolder.value.setText("Miscellaneous/Click to view performance");
                reportViewHolder.progressBar.setVisibility(View.GONE);
            } else if (position == 4) {
                reportViewHolder.reportIcon.setImageDrawable(mContext.getResources().getDrawable(resourceicon[position]));
                reportViewHolder.heading.setText(reportViewModel.getReportHeading());
                reportViewHolder.value.setText("Click to view unit compliance");
                reportViewHolder.progressBar.setVisibility(View.GONE);
            } else {
                reportViewHolder.reportIcon.setImageDrawable(mContext.getResources().getDrawable(resourceicon[position]));
                reportViewHolder.value.setText(reportViewModel.getReportValue());
                reportViewHolder.heading.setText(reportViewModel.getReportHeading());
                reportViewHolder.progressBar.setMax(reportViewModel.getProgressMaxValue());
                reportViewHolder.progressBar.setProgress(reportViewModel.getProgressValue());
            }

            /**
             * We have different progress drawables for different reports (like performance,work_time etc)
             */
            reportViewHolder.progressBar.setProgressDrawable(reportViewModel.getProgressDrawable());
            if (reportViewModel.isRotaComplaince()) {
                reportViewHolder.deviationLayout.setVisibility(View.VISIBLE);
                reportViewHolder.approvedTv.setText(reportViewModel.getApprovedCount() + " Approved / " + reportViewModel.getNonApprovedCount() + " Non-Approved");
            }

        } else if (holder instanceof TimeLineViewHolder && reportViewModelList.get(position) instanceof TimeLineModel) {
            TimeLineViewHolder timeLineViewHolder = (TimeLineViewHolder) holder;
            TimeLineModel timeLineModel = (TimeLineModel) reportViewModelList.get(position);
            if (position + 1 == reportViewModelList.size()) {
                /**
                 * This is to prevent the vertical Line for the last element
                 */
                timeLineViewHolder.verticalLine.setVisibility(View.GONE);
            } else {
                /**
                 * To increase vertical line length based on time difference.
                 */
                checkDate(timeLineModel.getStartTime(), ((TimeLineModel) reportViewModelList.get(position + 1)).getStartTime(),
                        timeLineViewHolder.verticalLine);
            }
            timeLineViewHolder.timeTextView.setText(timeLineModel.getStartTime());
            timeLineViewHolder.primaryLabel.setText(timeLineModel.getActivity() + " " + timeLineModel.getLocation());
            timeLineViewHolder.secondaryLabel.setText(timeLineModel.getTaskName() + " " + timeLineModel.getStatus());
        } else if (holder instanceof HeaderViewHolder && reportViewModelList.get(position) instanceof String) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.recycleHeader.setText((String) reportViewModelList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (reportViewModelList.get(position) instanceof ReportViewModel)
            return REPORT_VIEW;
        else if (reportViewModelList.get(position) instanceof TimeLineModel)
            return TIMELINE_VIEW;
        else if (reportViewModelList.get(position) instanceof String)
            return HEADER;
        return -1;
    }

    @Override
    public int getItemCount() {
        return reportViewModelList.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {
        ImageView reportIcon;
        ProgressBar progressBar;
        TextView heading, value, rotaDeviation, approvedTv;
        RelativeLayout deviationLayout;

        public ReportViewHolder(View itemView) {
            super(itemView);
            reportIcon = (ImageView) itemView.findViewById(R.id.iv_report_icon);
            heading = (TextView) itemView.findViewById(R.id.tv_report_header);
            value = (TextView) itemView.findViewById(R.id.tv_report_value);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pb_report_prog_bar);
            rotaDeviation = (TextView) itemView.findViewById(R.id.rota_deviation);
            approvedTv = (TextView) itemView.findViewById(R.id.approved_tv);
            deviationLayout = (RelativeLayout) itemView.findViewById(R.id.deviation_layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() == 0)
                        mContext.startActivity(new Intent(mContext, ShowPerformanceDetails.class));
                    else if (getAdapterPosition() == 4)
                        mContext.startActivity(new Intent(mContext, RoccReportViewActivity.class));
                }
            });
        }
    }

    public class TimeLineViewHolder extends RecyclerView.ViewHolder {
        TextView timeTextView;
        TextView primaryLabel;
        TextView secondaryLabel;
        View verticalLine;

        public TimeLineViewHolder(View itemView) {
            super(itemView);
            timeTextView = (TextView) itemView.findViewById(R.id.tv_time_label);
            primaryLabel = (TextView) itemView.findViewById(R.id.tv_primary_label);
            secondaryLabel = (TextView) itemView.findViewById(R.id.tv_secondary_label);
            verticalLine = itemView.findViewById(R.id.vertical_line);
        }
    }

    private void checkDate(String currentTime, String nextTime, View verticalLine) {

        SimpleDateFormat currentTimeDateFormat = new SimpleDateFormat("h:mm");
        try {
            Date curDate = currentTimeDateFormat.parse(currentTime);
            Date nextDate = currentTimeDateFormat.parse(nextTime);
            /**
             * Subtract nextDate with current Date
             */
            long difference = nextDate.getTime() - curDate.getTime();
            int time = (int) (difference / 1000);
            time = time / 100;
            if (time > 0) {
                verticalLine.requestLayout();
                int actualHeight = (int) mContext.getResources().getDimension(R.dimen.vertical_line_height);
                verticalLine.getLayoutParams().height = actualHeight + time;
            }
        } catch (ParseException e) {
            Toast.makeText(mContext, "Failed To convert dates", Toast.LENGTH_SHORT).show();
        }

    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycle_header)
        CustomFontTextview recycleHeader;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
