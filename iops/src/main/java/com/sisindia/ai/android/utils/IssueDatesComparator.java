package com.sisindia.ai.android.utils;

import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.network.request.IssueIMO;

import java.util.Comparator;

/**
 * Created by compass on 3/15/2017.
 */

public class IssueDatesComparator implements Comparator<Object> {
    DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

     public  IssueDatesComparator(){
         dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }
    @Override
    public int compare(Object issueIMO1, Object issueIMO2) {

        return dateTimeFormatConversionUtil.convertStringToDate(((IssuesMO)issueIMO2).
                getCreatedDateTime()).compareTo( dateTimeFormatConversionUtil.
                convertStringToDate(((IssuesMO)issueIMO1).getCreatedDateTime()));
    }
}
