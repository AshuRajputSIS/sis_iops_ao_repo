package com.sisindia.ai.android.rota;

import com.sisindia.ai.android.network.request.RotaTaskInputRequestMO;

import java.util.HashMap;

/**
 * Created by compass on 4/20/2017.
 */

public class CompletedRotaTaskMo {
    private int  unSyncedCompletedTaks;
    private HashMap<Integer, RotaTaskInputRequestMO> unSyncedCompletedTasksMap;

    public int getUnSyncedCompletedTaks() {
        return unSyncedCompletedTaks;
    }

    public void setUnSyncedCompletedTaks(int unSyncedCompletedTaks) {
        this.unSyncedCompletedTaks = unSyncedCompletedTaks;
    }

    public HashMap<Integer, RotaTaskInputRequestMO> getUnSyncedCompletedTasksMap() {
        return unSyncedCompletedTasksMap;
    }

    public void setUnSyncedCompletedTasksMap(HashMap<Integer, RotaTaskInputRequestMO> unSyncedCompletedTasksMap) {
        this.unSyncedCompletedTasksMap = unSyncedCompletedTasksMap;
    }
}
