package com.sisindia.ai.android.database.unitRaisingDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.google.gson.reflect.TypeToken;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitAddressLatlong;
import com.sisindia.ai.android.network.response.UnitBoundary;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.unitraising.RaisingDetailsMo;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by compass on 8/2/2017.
 */

public class UnitRaisingSelectStatementDB  extends SISAITrackingDB{
    public UnitRaisingSelectStatementDB(Context context) {
        super(context);
    }


    public ArrayList<RaisingDetailsMo> getUnitRaisingDetails(){

        ArrayList<RaisingDetailsMo> unitRaisingMoList = new ArrayList<>();

        String  query = "select " + TableNameAndColumnStatement.UNIT_ID + "," +
                 TableNameAndColumnStatement.UNIT_NAME + "," +
                 TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME + "," +
                TableNameAndColumnStatement.UNIT_STATUS + "," +
                 TableNameAndColumnStatement.UNIT_CODE +
                " from unit where unit_status in (2)";

        Timber.i("Unit Raising %s", query);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            sqlite.beginTransaction();
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        RaisingDetailsMo raisingDetailsMo = new RaisingDetailsMo();
                        raisingDetailsMo.unitId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        raisingDetailsMo.unitName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME));
                        raisingDetailsMo.raisingStatus = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_STATUS));
                        raisingDetailsMo.expectedRaisingDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME));
                        raisingDetailsMo.unitCode = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE));
                        unitRaisingMoList.add(raisingDetailsMo);
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
          sqlite.setTransactionSuccessful();
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            sqlite.endTransaction();
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitRaisingMoList;
    }

    public int getUnitRaisingDeferredOrCancellationLocalId( String tableName,
                                                            String columnName) {

        int unitRaisingDeferredOrCancellationId = 0;

        String  query = "select " + TableNameAndColumnStatement.ID +
                " from "+ tableName +" where "+ columnName + " =  " +0 ;
        SQLiteDatabase sqlite = null;

        Timber.i("getUnitRaisingDeferredOrCancellationLocalId %s", query);

        try {
            sqlite = this.getReadableDatabase();
            sqlite.beginTransaction();
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        unitRaisingDeferredOrCancellationId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));

                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
                sqlite.setTransactionSuccessful();
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        finally {
            sqlite.endTransaction();
        }
        return  unitRaisingDeferredOrCancellationId;
    }

   public  int  getRaisingDeferredCount(int unitId) {


        String  query = "select count(" + TableNameAndColumnStatement.ID +
                ")  as count from "+TableNameAndColumnStatement.UNIT_RAISING_DEFERRED + " where "+
                TableNameAndColumnStatement.UNIT_ID + " = "+ unitId;
        SQLiteDatabase sqlite = null;

        Timber.i("getRaisingDeferredCount %s", query);

        try {
            sqlite = this.getReadableDatabase();
            sqlite.beginTransaction();
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {

                      return cursor.getInt(cursor.getColumnIndex("count"));

                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
                sqlite.setTransactionSuccessful();
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        finally {
            sqlite.endTransaction();
        }
        return  0;
    }
}
