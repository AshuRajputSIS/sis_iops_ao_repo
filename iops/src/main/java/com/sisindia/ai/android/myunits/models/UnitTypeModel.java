package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitTypeModel {

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("MinGuardSize")
    @Expose
    private Integer minGuardSize;

    @SerializedName("MaxGuardSize")
    @Expose
    private Integer maxGuardSize;

    @SerializedName("Description")
    @Expose
    private String description;

    @SerializedName("DayCheckFrequencyInMonth")
    @Expose
    private Integer dayCheckFrequencyInMonth;

    @SerializedName("NightCheckFrequencyInMonth")
    @Expose
    private Integer nightCheckFrequencyInMonth;

    @SerializedName("ClientCoordinationFrequencyInMonth")
    @Expose
    private Integer clientCoordinationFrequencyInMonth;

    @SerializedName("MinInspectorSize")
    @Expose
    private Integer minInspectorSize;

    @SerializedName("MaxInspectorSize")
    @Expose
    private Integer maxInspectorSize;

    @SerializedName("Priority")
    @Expose
    private Integer priority;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The minGuardSize
     */
    public Integer getMinGuardSize() {
        return minGuardSize;
    }

    /**
     * @param minGuardSize The MinGuardSize
     */
    public void setMinGuardSize(Integer minGuardSize) {
        this.minGuardSize = minGuardSize;
    }

    /**
     * @return The maxGuardSize
     */
    public Integer getMaxGuardSize() {
        return maxGuardSize;
    }

    /**
     * @param maxGuardSize The MaxGuardSize
     */
    public void setMaxGuardSize(Integer maxGuardSize) {
        this.maxGuardSize = maxGuardSize;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The dayCheckFrequencyInMonth
     */
    public Integer getDayCheckFrequencyInMonth() {
        return dayCheckFrequencyInMonth;
    }

    /**
     * @param dayCheckFrequencyInMonth The DayCheckFrequencyInMonth
     */
    public void setDayCheckFrequencyInMonth(Integer dayCheckFrequencyInMonth) {
        this.dayCheckFrequencyInMonth = dayCheckFrequencyInMonth;
    }

    /**
     * @return The nightCheckFrequencyInMonth
     */
    public Integer getNightCheckFrequencyInMonth() {
        return nightCheckFrequencyInMonth;
    }

    /**
     * @param nightCheckFrequencyInMonth The NightCheckFrequencyInMonth
     */
    public void setNightCheckFrequencyInMonth(Integer nightCheckFrequencyInMonth) {
        this.nightCheckFrequencyInMonth = nightCheckFrequencyInMonth;
    }

    /**
     * @return The clientCoordinationFrequencyInMonth
     */
    public Integer getClientCoordinationFrequencyInMonth() {
        return clientCoordinationFrequencyInMonth;
    }

    /**
     * @param clientCoordinationFrequencyInMonth The ClientCoordinationFrequencyInMonth
     */
    public void setClientCoordinationFrequencyInMonth(Integer clientCoordinationFrequencyInMonth) {
        this.clientCoordinationFrequencyInMonth = clientCoordinationFrequencyInMonth;
    }

    /**
     * @return The minInspectorSize
     */
    public Integer getMinInspectorSize() {
        return minInspectorSize;
    }

    /**
     * @param minInspectorSize The MinInspectorSize
     */
    public void setMinInspectorSize(Integer minInspectorSize) {
        this.minInspectorSize = minInspectorSize;
    }

    /**
     * @return The maxInspectorSize
     */
    public Integer getMaxInspectorSize() {
        return maxInspectorSize;
    }

    /**
     * @param maxInspectorSize The MaxInspectorSize
     */
    public void setMaxInspectorSize(Integer maxInspectorSize) {
        this.maxInspectorSize = maxInspectorSize;
    }

    /**
     * @return The priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority The Priority
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

}
