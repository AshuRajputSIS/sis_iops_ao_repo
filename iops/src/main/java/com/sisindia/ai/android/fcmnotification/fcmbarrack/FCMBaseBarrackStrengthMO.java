package com.sisindia.ai.android.fcmnotification.fcmbarrack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.myunits.models.BarrackStrength;

import java.io.Serializable;

/**
 * Created by shankar on 2/12/16.
 */

public class FCMBaseBarrackStrengthMO implements Serializable {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private BarrackStrength data;

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     *
     * @return
     * The data
     */
    public BarrackStrength getData() {
        return data;
    }

    /**
     *
     * @param data
     * The Data
     */
    public void setData(BarrackStrength data) {
        this.data = data;
    }


}
