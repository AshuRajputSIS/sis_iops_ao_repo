package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.IssuesViewPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 20-04-2016.
 */
public class AnnualKitReplacementTabsActivity extends BaseActivity  implements View.OnClickListener{
    @Bind(R.id.kitreplacement_tabLayout)
    TabLayout kitReplacementTablayout;
    @Bind(R.id.kitreplacement_viewpager)
    ViewPager kitReplacementViewpager;
    @Bind(R.id.kitreplacement_toolbar)
    Toolbar toolbar;
    @Bind(R.id.add_kit_floatting_button)
    FloatingActionButton addKitFloatingButton;
    private ColorStateList colorStateList;
    private int unitId;
    private int kitTypeId;
    private  String unitName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kit_replacement_activity);
        ButterKnife.bind(this);
        updateUi();
    }

    public void updateUi() {
        colorStateList = ContextCompat.getColorStateList(this, R.drawable.tab_label_indicator);
        setBaseToolbar(toolbar, getString(R.string.annual_kit_replacement));
        Intent intent = getIntent();
         if(intent.getExtras() != null){
             unitId = intent.getIntExtra(TableNameAndColumnStatement.UNIT_ID,0);
             kitTypeId = intent.getIntExtra(TableNameAndColumnStatement.KIT_TYPE_ID,0);
             unitName = intent.getStringExtra(TableNameAndColumnStatement.UNIT_NAME);
         }
        setupViewPager(kitReplacementViewpager);
        kitReplacementTablayout.setupWithViewPager(kitReplacementViewpager);
        kitReplacementTablayout.setTabTextColors(colorStateList);
        addKitFloatingButton.setOnClickListener(this);
    }


    private void setupViewPager(ViewPager viewPager) {
        IssuesViewPagerAdapter kitReplacePagerAdapter = new IssuesViewPagerAdapter(getSupportFragmentManager());
        KitAssignedFragment  kitAssignedFragment= new KitAssignedFragment().newInstance("", "");
        kitAssignedFragment.setUnitId(unitId);
        kitAssignedFragment.setKitTypeId(kitTypeId);
        kitAssignedFragment.setUnitName(unitName);
        KitDeliveredFragment  kitDeliveredFragment= new KitDeliveredFragment().newInstance("", "");
        kitDeliveredFragment.setUnitId(unitId);
        kitDeliveredFragment.setKitTypeId(kitTypeId);
        kitDeliveredFragment.setUnitName(unitName);
        kitReplacePagerAdapter.addFragment(kitAssignedFragment, getString(R.string.assigned));
        kitReplacePagerAdapter.addFragment(kitDeliveredFragment, getString(R.string.delivered));
        viewPager.setAdapter(kitReplacePagerAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.add_kit_floatting_button :
                Intent kitRepalcementIntent = new Intent(AnnualKitReplacementTabsActivity.this,AddKitRequestActivity.class);
                startActivity(kitRepalcementIntent);
                break;
        }

    }
}

