package com.sisindia.ai.android.notifications;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.fcmnotification.Config;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.fcmnotification.NotificationWebView;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.List;

import timber.log.Timber;


public class NotificationFragment extends Fragment implements OnRecyclerViewItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView notificationRecyclerView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    // private PerformanceDetailsRecyclerAdapter notificationRecyclerAdapter;
    private Context mContext;
    private NotificationListRecyclerAdapter notificationListRecyclerAdapter;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private TextView notificationMsgTextview;
    private List<NotificationReceivingMO> notificationReceivingList;
    private AppPreferences appPreferences;

    public NotificationFragment() {
        // Required empty public constructor
    }


    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.homepage_notification_list, container, false);
        notificationMsgTextview = (TextView) view.findViewById(R.id.no_notification_textview);
        notificationRecyclerView = (RecyclerView) view.findViewById(R.id.notification_recyclerview);
        appPreferences = new AppPreferences(getActivity());
        filterList();
        setViewVisibility();
        return view;
    }

    // TODO: Rename method, onDutyChange argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        notificationRecyclerView.setLayoutManager(linearLayoutManager);
        notificationRecyclerView.setItemAnimator(new DefaultItemAnimator());
        notificationListRecyclerAdapter = new NotificationListRecyclerAdapter(mContext);
        Drawable dividerDrawable = ContextCompat.getDrawable(mContext, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        notificationRecyclerView.addItemDecoration(dividerItemDecoration);
        notificationListRecyclerAdapter.setNotificationData(notificationReceivingList);
        notificationListRecyclerAdapter.setOnRecyclerViewItemClickListener(this);
        notificationRecyclerView.setAdapter(notificationListRecyclerAdapter);

    }

    private void setViewVisibility() {
        if (notificationReceivingList != null) {
            if (notificationReceivingList.size() == 0) {
                notificationMsgTextview.setVisibility(View.VISIBLE);
            } else {
                notificationMsgTextview.setVisibility(View.GONE);
                setAdapter();
            }
        }
    }

    /**
     * Removig previous day notification
     * Showing only current day notification
     */
    private void filterList() {
        notificationReceivingList = IOPSApplication.getInstance().getNotificationStatementDB().getNotificationToDisplay(null);
        if (notificationReceivingList != null && notificationReceivingList.size() != 0) {
            for (int i = 0; i < notificationReceivingList.size(); i++) {
                if (!DateUtils.isToday(notificationReceivingList.get(i).getRecievedDateTime())) {
                    IOPSApplication.getInstance().getNotificationStatementDB().deleteNotificationAfterOneDay(notificationReceivingList.get(i).getId());
                    Timber.d("Notification delete from list i = " + i);
                }
            }
            //query the latest notifications after removing the old data
            notificationReceivingList = IOPSApplication.getInstance().getNotificationStatementDB().getNotificationToDisplay(null);
        }

    }

    public void onRecyclerViewItemClick(View v, int position) {
        if (notificationReceivingList != null) {
            String notificationName = notificationReceivingList.get(position).getName();
            appPreferences.setFilterIds(notificationReceivingList.get(position).getUnitList());
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            if (!TextUtils.isEmpty(notificationName)) {
                if (notificationName.equalsIgnoreCase(Config.BILL_COLLECTION) ||
                        notificationName.equalsIgnoreCase(Config.BILL_SUBMISSION) ||
                        notificationName.equalsIgnoreCase(Config.BILL_COLLECTION_DUE) ||
                        notificationName.equalsIgnoreCase(Config.BILL_COLLECTION_OVERDUE)) {
                    intent.putExtra(HomeActivity.OPEN_MY_UNIT_FROM_NOTIFICATION, true);

                } else if (Config.AKR_DUE.equalsIgnoreCase(notificationName)) {
                    intent.putExtra(HomeActivity.OPEN_AKR_FROM_NOTIFICATION, true);
                } else if (Config.CUSTOM.equalsIgnoreCase(notificationReceivingList.get(position).getName())) {
                    intent = new Intent(getActivity(), NotificationWebView.class);
                    intent.putExtra(getResources().getString(R.string.notification_url),
                            notificationReceivingList.get(position).getCallbackUrl());
                    intent.putExtra(getResources().getString(R.string.notification_msg),
                            notificationReceivingList.get(position).getMessage());
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        }
    }
}
