package com.sisindia.ai.android.myperformance;

import java.util.HashMap;
import java.util.Map;

public enum MyPerformanceReportTypeEnum {


    CurrentDate(1),
    Week(2),
    Month(3),
    Year(4),
    SpecificDate(5);


    private static Map map = new HashMap<>();

    static {
        for (MyPerformanceReportTypeEnum myPerformanceReportTypeEnum : MyPerformanceReportTypeEnum.values()) {
            map.put(myPerformanceReportTypeEnum.value, myPerformanceReportTypeEnum);
        }
    }

    private int value;

    MyPerformanceReportTypeEnum(int value) {
        this.value = value;
    }

    public static MyPerformanceReportTypeEnum valueOf(int myPerformanceReportTypeEnum) {
        return (MyPerformanceReportTypeEnum) map.get(myPerformanceReportTypeEnum);
    }

    public int getValue() {
        return value;
    }

}
