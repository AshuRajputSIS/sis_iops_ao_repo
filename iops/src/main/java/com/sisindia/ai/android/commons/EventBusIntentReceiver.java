package com.sisindia.ai.android.commons;

/**
 * Created by shankar on 12/12/16.
 */

public class EventBusIntentReceiver {
    int mResult;
    String mResultValue;

   public EventBusIntentReceiver(int resultCode, String resultValue) {
        mResult = resultCode;
        mResultValue = resultValue;
    }

    public int getResult() {
        return mResult;
    }

    public String getResultValue() {
        return mResultValue;
    }
}
