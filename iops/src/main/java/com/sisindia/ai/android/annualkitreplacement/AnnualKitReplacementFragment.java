package com.sisindia.ai.android.annualkitreplacement;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.issues.IssuesViewPagerAdapter;
import com.sisindia.ai.android.manualrefresh.ManualAkrSync;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class AnnualKitReplacementFragment extends Fragment implements AkrSyncListener {

    @Bind(R.id.kitreplacement_tabLayout)
    TabLayout kitreplacementTabLayout;
    @Bind(R.id.kitreplacement_viewpager)
    ViewPager kitreplacementViewpager;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private ColorStateList colorStateList;
    private IssuesViewPagerAdapter kitReplacePagerAdapter;
    private KitReplaceCountData kitReplaceCountData;
    private AkrSyncListener akrSyncListener;

    public AnnualKitReplacementFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AnnualKitReplacementFragment.
     */

    public static AnnualKitReplacementFragment newInstance(String param1, String param2) {
        AnnualKitReplacementFragment fragment = new AnnualKitReplacementFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        akrSyncListener = this;
        setHasOptionsMenu(true);//for enable the menu in fragment
        colorStateList = ContextCompat.getColorStateList(getActivity(), R.drawable.tab_label_indicator);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.kit_replacement_fragment, null, false);
        viewPagerSetUp(view);
        return view;
    }

    @SuppressLint("ResourceType")
    public void viewPagerSetUp(View view) {
        colorStateList = ContextCompat.getColorStateList(getActivity(), R.drawable.tab_label_indicator);
        ButterKnife.bind(this, view);
        if (NetworkUtil.isConnected) {
            getAnnualKitItemsCountCall();
        } else {
            setupViewPager(kitreplacementViewpager);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        IssuesViewPagerAdapter kitReplacePagerAdapter = new IssuesViewPagerAdapter(getChildFragmentManager());
        AnnualKitFragment annualKitFragment = AnnualKitFragment.newInstance("", "");
        annualKitFragment.setAnnualKitCountData(kitReplaceCountData);
        AdhocKitFragment adhocKitFragment = AdhocKitFragment.newInstance("", "");
        adhocKitFragment.setAdhocKitCountData(kitReplaceCountData);
        kitReplacePagerAdapter.addFragment(annualKitFragment, getString(R.string.AnnualKit));
        kitReplacePagerAdapter.addFragment(adhocKitFragment, getString(R.string.AdhocKit));
        viewPager.setAdapter(kitReplacePagerAdapter);
        kitreplacementTabLayout.setupWithViewPager(kitreplacementViewpager);
        kitreplacementTabLayout.setTabTextColors(colorStateList);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mParam1 != null && !mParam1.equalsIgnoreCase(""))
            toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void getAnnualKitItemsCountCall() {
        if (getActivity() != null) {
            SISClient sisClient = new SISClient(getActivity());
            showprogressbar();
            sisClient.getApi().kitReplaceCount(new Callback<KitReplaceCountOR>() {
                @Override
                public void success(KitReplaceCountOR kitReplaceCountOR, Response response) {
                    switch (kitReplaceCountOR.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            kitReplaceCountData = kitReplaceCountOR.getData();
                            hideprogressbar();
                            updateUIOnTab();
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(getActivity(), getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            updateUIOnTab();
                            break;
                        default:
                            Toast.makeText(getActivity(), getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                            updateUIOnTab();
                            break;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar();
                    Util.printRetorfitError("SingleUnitActivity", error);
                    updateUIOnTab();
                }
            });
        }
    }

    private void updateUIOnTab() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setupViewPager(kitreplacementViewpager);
                }
            });
        }
    }

    private void showprogressbar() {
        Util.showProgressBar(getActivity(), R.string.loading_please_wait);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isVisible());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_akr_homepage, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.syncAkr) {
            if (NetworkUtil.isConnected) {
                ManualAkrSync.getAkrApiCall(akrSyncListener, getActivity(), isVisible());
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
            }
        }
        return true;
    }

    @Override
    public void akrSyncListener(boolean isSynced) {
        if (getActivity() != null) {
            if (isSynced) {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_str));
                updateUIOnTab();
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_error_str));
            }
        }

    }
}


