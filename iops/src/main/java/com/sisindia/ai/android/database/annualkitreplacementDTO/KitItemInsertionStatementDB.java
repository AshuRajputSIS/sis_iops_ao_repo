package com.sisindia.ai.android.database.annualkitreplacementDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 02-09-2016.
 */
public class KitItemInsertionStatementDB extends SISAITrackingDB {


    public KitItemInsertionStatementDB(Context context) {
        super(context);
    }


    public void insertKitItemRequest(KitItemRequestMO kitItemRequestMO){

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

                try {
                    if(kitItemRequestMO != null){
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.GUARD_ID,kitItemRequestMO.getGuardId());
                        values.put(TableNameAndColumnStatement.GUARD_CODE,kitItemRequestMO.getGuardCode());
                        values.put(TableNameAndColumnStatement.SOURCE_TASK_ID, kitItemRequestMO.getSourceTaskId());
                        values.put(TableNameAndColumnStatement.REQUESTED_ON,kitItemRequestMO.getRequestedOn() );
                        values.put(TableNameAndColumnStatement.REQUESTED_STATUS,kitItemRequestMO.getRequestedStatus() );
                        values.put(TableNameAndColumnStatement.REQUESTED_ITEMS,kitItemRequestMO.getRequestedItems() );
                        values.put(TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID,kitItemRequestMO.getKitItemRequstedId() );
                        values.put(TableNameAndColumnStatement.IS_NEW, 1);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                        sqlite.insert(TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE, null, values);
                        sqlite.setTransactionSuccessful();

                    }

                } catch (SQLiteConstraintException sqliteConstraintException) {
                    Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                    Crashlytics.logException(sqliteConstraintException);
                } catch (SQLiteException sqliteException) {
                    Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                    Crashlytics.logException(sqliteException);
                } catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                } finally {
                    sqlite.endTransaction();
                    if (null != sqlite && sqlite.isOpen()) {
                        sqlite.close();
                    }

            }

        }

    }






}