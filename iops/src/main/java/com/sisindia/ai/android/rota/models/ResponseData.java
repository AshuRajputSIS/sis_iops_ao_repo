package com.sisindia.ai.android.rota.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Shushrut on 20-09-2016.
 */
public class ResponseData {
    @SerializedName("EmployeeCode")
    @Expose
    private String employeeCode;
    @SerializedName("EmployeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("EmployeeFullName")
    @Expose
    private String employeeFullName;
    @SerializedName("Reward")
    @Expose
    private Integer reward;
    @SerializedName("Punishment")
    @Expose
    private Integer punishment;
    @SerializedName("LastAKRDue")
    @Expose
    private String lastAKRDue;
    @SerializedName("LastAKRIssue")
    @Expose
    private String lastAKRIssue;
    @SerializedName("LastFine")
    @Expose
    private Integer lastFine;

    /**
     *
     * @return
     * The employeeCode
     */
    public String getEmployeeCode() {
        return employeeCode;
    }

    /**
     *
     * @param employeeCode
     * The EmployeeCode
     */
    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    /**
     *
     * @return
     * The employeeId
     */
    public Integer getEmployeeId() {
        return employeeId;
    }

    /**
     *
     * @param employeeId
     * The EmployeeId
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    /**
     *
     * @return
     * The employeeFullName
     */
    public String getEmployeeFullName() {
        return employeeFullName;
    }

    /**
     *
     * @param employeeFullName
     * The EmployeeFullName
     */
    public void setEmployeeFullName(String employeeFullName) {
        this.employeeFullName = employeeFullName;
    }

    /**
     *
     * @return
     * The reward
     */
    public Integer getReward() {
        return reward;
    }

    /**
     *
     * @param reward
     * The Reward
     */
    public void setReward(Integer reward) {
        this.reward = reward;
    }

    /**
     *
     * @return
     * The punishment
     */
    public Integer getPunishment() {
        return punishment;
    }

    /**
     *
     * @param punishment
     * The Punishment
     */
    public void setPunishment(Integer punishment) {
        this.punishment = punishment;
    }

    /**
     *
     * @return
     * The lastAKRDue
     */
    public String getLastAKRDue() {
        return lastAKRDue;
    }

    /**
     *
     * @param lastAKRDue
     * The LastAKRDue
     */
    public void setLastAKRDue(String lastAKRDue) {
        this.lastAKRDue = lastAKRDue;
    }

    /**
     *
     * @return
     * The lastAKRIssue
     */
    public String getLastAKRIssue() {
        return lastAKRIssue;
    }

    /**
     *
     * @param lastAKRIssue
     * The LastAKRIssue
     */
    public void setLastAKRIssue(String lastAKRIssue) {
        this.lastAKRIssue = lastAKRIssue;
    }

    /**
     *
     * @return
     * The lastFine
     */
    public Integer getLastFine() {
        return lastFine;
    }

    /**
     *
     * @param lastFine
     * The LastFine
     */
    public void setLastFine(Integer lastFine) {
        this.lastFine = lastFine;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "employeeCode='" + employeeCode + '\'' +
                ", employeeId=" + employeeId +
                ", employeeFullName='" + employeeFullName + '\'' +
                ", reward=" + reward +
                ", punishment=" + punishment +
                ", lastAKRDue='" + lastAKRDue + '\'' +
                ", lastAKRIssue='" + lastAKRIssue + '\'' +
                ", lastFine=" + lastFine +
                '}';
    }
}
