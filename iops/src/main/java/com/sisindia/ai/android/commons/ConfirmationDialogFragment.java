package com.sisindia.ai.android.commons;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.sisindia.ai.android.R;

/**
 * Created by Durga Prasad on 25-06-2016.
 */
public class ConfirmationDialogFragment extends DialogFragment {


    private AdapterView.OnItemClickListener onItemClickListener;
    public static final String TAG = "CustomDialogFragment";
    private View view;
    private View titleView;
    private int pageNumber;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private DialogClickListener dialogClickListener;
    private String message;
    private int clickType;
    private int confirmationType;


    public ConfirmationDialogFragment() {
        // Required empty public constructor
    }


    public static ConfirmationDialogFragment newInstance() {
        ConfirmationDialogFragment confirmationDialogFragment = new ConfirmationDialogFragment();
        Bundle args = new Bundle();
        confirmationDialogFragment.setArguments(args);
        return confirmationDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if (context instanceof DialogClickListener) {
            dialogClickListener = (DialogClickListener) context;
        }
    }

    public void setLayout(int confirmationType) {
        this.confirmationType = confirmationType;
    }


    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View titleView = LayoutInflater.from(mContext).inflate(R.layout.dialog_title, null);
        TextView dialogTitle = (TextView) titleView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(getResources().getString(R.string.CONFIRMATION));
        builder.setCustomTitle(titleView);
        dialogTitle.setTextColor(getResources().getColor(R.color.black));
        builder = setPositiveNegativeButtons(builder, getResources().getString(R.string.YES), getResources().getString(R.string.NO));


        view = LayoutInflater.from(mContext).inflate(R.layout.confirmation_content, null);
        TextView dialogQueryMessage = (TextView) view.findViewById(R.id.navigationBackQuestionTv);
        TextView dialogContentMessage = (TextView) view.findViewById(R.id.navigationBackContentTv);

        switch (confirmationType) {
            case 1:
                builder.setView(view);
                break;
            case 2:
                dialogContentMessage.setText(getActivity().getResources().getString(R.string.generic_navigating_content_message));
                builder.setView(view);
                break;
            case 3:
                dialogContentMessage.setVisibility(View.GONE);
                dialogQueryMessage.setText(getActivity().getResources().getString(R.string.guard_deletion_confirmation_message));
                clickType = 2;
                builder.setView(view);
                break;
            case 5:
                dialogContentMessage.setText(getActivity().getResources().getString(R.string.bill_sumission_confirmation_msg));
                builder.setView(view);
                break;
            case 6:
                dialogContentMessage.setText(getActivity().getResources().getString(R.string.bill_collection_confirmation_msg));
                builder.setView(view);
                break;
            // BELOW TWO CONDITION(7&8) ARE FOR VALIDATING NFC CONFIG AT BARRACK
            case 7:
                dialogQueryMessage.setVisibility(View.GONE);
                dialogContentMessage.setText("You have not configured NFC at barrack.\n\nAre you sure you want to submit without configuring NFC");
                builder.setView(view);
                break;
            case 8:
                dialogQueryMessage.setVisibility(View.GONE);
                dialogContentMessage.setText("You have not touched NFC at barrack.\n\nAre you sure you want to submit without touching NFC");
                builder.setView(view);
                break;
        }
        return builder.create();
    }

    private AlertDialog.Builder setPositiveNegativeButtons(AlertDialog.Builder builder, String positiveButtonCaption, String negativeButtonCaption) {

        builder.setPositiveButton(positiveButtonCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogClickListener.onPositiveButtonClick(clickType);
            }
        });


        builder.setNegativeButton(negativeButtonCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder;
    }

}


