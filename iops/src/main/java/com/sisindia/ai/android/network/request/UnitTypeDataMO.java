package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashu Rajput on 9/27/2018.
 */

public class UnitTypeDataMO {

    @SerializedName("Id")
    public int Id;

    @SerializedName("Name")
    public String Name;

    @SerializedName("Description")
    public String Description;

    @SerializedName("MinGuardSize")
    public int MinGuardSize;

    @SerializedName("MaxGuardSize")
    public int MaxGuardSize;

    @SerializedName("CheckLimit")
    public int CheckLimit;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public int getMinGuardSize() {
        return MinGuardSize;
    }

    public int getMaxGuardSize() {
        return MaxGuardSize;
    }

    public int getCheckLimit() {
        return CheckLimit;
    }
}
