package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class UserProfile {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("EmployeeNo")
    @Expose
    private String employeeNo;
    @SerializedName("OrganizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("PrimaryAddressId")
    @Expose
    private Integer primaryAddressId;
    @SerializedName("ResidentialAddressId")
    @Expose
    private Integer residentialAddressId;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("AdminControllerId")
    @Expose
    private Integer adminControllerId;
    @SerializedName("IsReferral")
    @Expose
    private Boolean isReferral;
    @SerializedName("ReferralId")
    @Expose
    private Integer referralId;
    @SerializedName("ReferralSource")
    @Expose
    private String referralSource;
    @SerializedName("LanguagePreferenceId")
    @Expose
    private Integer languagePreferenceId;
    @SerializedName("EmployeeProfilePicId")
    @Expose
    private Integer employeeProfilePicId;
    @SerializedName("MiniatureProfilePicId")
    @Expose
    private Integer miniatureProfilePicId;
    @SerializedName("CompanyContactNumber")
    @Expose
    private String companyContactNumber;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("DateOfJoining")
    @Expose
    private String dateOfJoining;
    @SerializedName("FunctionalControllerId")
    @Expose
    private Integer functionalControllerId;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("EmergencyContactNo")
    @Expose
    private String emergencyContactNo;
    @SerializedName("AlternateContactNo")
    @Expose
    private String alternateContactNo;
    @SerializedName("BranchId")
    @Expose
    private Integer branchId;
    @SerializedName("EmployeeSignatureId")
    @Expose
    private Integer employeeSignatureId;
    @SerializedName("RankId")
    @Expose
    private Integer rankId;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("FullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("GeoLatitude")
    @Expose
    private String geoLatitude;
    @SerializedName("GeoLongitude")
    @Expose
    private String geoLongitude;
    @SerializedName("RegionId")
    @Expose
    private Integer regionId;
    @SerializedName("IsDefaultPassword")
    @Expose
    private Boolean isDefaultPassword;

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    @SerializedName("ProfilePhotoURL")
    @Expose
    private String profilePhotoUrl;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The FirstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName The MiddleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The LastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The employeeNo
     */
    public String getEmployeeNo() {
        return employeeNo;
    }

    /**
     * @param employeeNo The EmployeeNo
     */
    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    /**
     * @return The organizationId
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId The OrganizationId
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return The primaryAddressId
     */
    public Integer getPrimaryAddressId() {
        return primaryAddressId;
    }

    /**
     * @param primaryAddressId The PrimaryAddressId
     */
    public void setPrimaryAddressId(Integer primaryAddressId) {
        this.primaryAddressId = primaryAddressId;
    }

    /**
     * @return The residentialAddressId
     */
    public Integer getResidentialAddressId() {
        return residentialAddressId;
    }

    /**
     * @param residentialAddressId The ResidentialAddressId
     */
    public void setResidentialAddressId(Integer residentialAddressId) {
        this.residentialAddressId = residentialAddressId;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The adminControllerId
     */
    public Integer getAdminControllerId() {
        return adminControllerId;
    }

    /**
     * @param adminControllerId The AdminControllerId
     */
    public void setAdminControllerId(Integer adminControllerId) {
        this.adminControllerId = adminControllerId;
    }

    /**
     * @return The isReferral
     */
    public Boolean getIsReferral() {
        return isReferral;
    }

    /**
     * @param isReferral The IsReferral
     */
    public void setIsReferral(Boolean isReferral) {
        this.isReferral = isReferral;
    }

    /**
     * @return The referralId
     */
    public Integer getReferralId() {
        return referralId;
    }

    /**
     * @param referralId The ReferralId
     */
    public void setReferralId(Integer referralId) {
        this.referralId = referralId;
    }

    /**
     * @return The referralSource
     */
    public String getReferralSource() {
        return referralSource;
    }

    /**
     * @param referralSource The ReferralSource
     */
    public void setReferralSource(String referralSource) {
        this.referralSource = referralSource;
    }

    /**
     * @return The languagePreferenceId
     */
    public Integer getLanguagePreferenceId() {
        return languagePreferenceId;
    }

    /**
     * @param languagePreferenceId The LanguagePreferenceId
     */
    public void setLanguagePreferenceId(Integer languagePreferenceId) {
        this.languagePreferenceId = languagePreferenceId;
    }

    /**
     * @return The employeeProfilePicId
     */
    public Integer getEmployeeProfilePicId() {
        return employeeProfilePicId;
    }

    /**
     * @param employeeProfilePicId The EmployeeProfilePicId
     */
    public void setEmployeeProfilePicId(Integer employeeProfilePicId) {
        this.employeeProfilePicId = employeeProfilePicId;
    }

    /**
     * @return The miniatureProfilePicId
     */
    public Integer getMiniatureProfilePicId() {
        return miniatureProfilePicId;
    }

    /**
     * @param miniatureProfilePicId The MiniatureProfilePicId
     */
    public void setMiniatureProfilePicId(Integer miniatureProfilePicId) {
        this.miniatureProfilePicId = miniatureProfilePicId;
    }

    /**
     * @return The companyContactNumber
     */
    public String getCompanyContactNumber() {
        return companyContactNumber;
    }

    /**
     * @param companyContactNumber The CompanyContactNumber
     */
    public void setCompanyContactNumber(String companyContactNumber) {
        this.companyContactNumber = companyContactNumber;
    }

    /**
     * @return The dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth The DateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return The dateOfJoining
     */
    public String getDateOfJoining() {
        return dateOfJoining;
    }

    /**
     * @param dateOfJoining The DateOfJoining
     */
    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    /**
     * @return The functionalControllerId
     */
    public Integer getFunctionalControllerId() {
        return functionalControllerId;
    }

    /**
     * @param functionalControllerId The FunctionalControllerId
     */
    public void setFunctionalControllerId(Integer functionalControllerId) {
        this.functionalControllerId = functionalControllerId;
    }

    /**
     * @return The contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo The ContactNo
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return The emergencyContactNo
     */
    public String getEmergencyContactNo() {
        return emergencyContactNo;
    }

    /**
     * @param emergencyContactNo The EmergencyContactNo
     */
    public void setEmergencyContactNo(String emergencyContactNo) {
        this.emergencyContactNo = emergencyContactNo;
    }

    /**
     * @return The alternateContactNo
     */
    public String getAlternateContactNo() {
        return alternateContactNo;
    }

    /**
     * @param alternateContactNo The AlternateContactNo
     */
    public void setAlternateContactNo(String alternateContactNo) {
        this.alternateContactNo = alternateContactNo;
    }

    /**
     * @return The branchId
     */
    public Integer getBranchId() {
        return branchId;
    }

    /**
     * @param branchId The BranchId
     */
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    /**
     * @return The employeeSignatureId
     */
    public Integer getEmployeeSignatureId() {
        return employeeSignatureId;
    }

    /**
     * @param employeeSignatureId The EmployeeSignatureId
     */
    public void setEmployeeSignatureId(Integer employeeSignatureId) {
        this.employeeSignatureId = employeeSignatureId;
    }

    /**
     * @return The rankId
     */
    public Integer getRankId() {
        return rankId;
    }

    /**
     * @param rankId The RankId
     */
    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    /**
     * @return The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress The EmailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return The fullAddress
     */
    public String getFullAddress() {
        return fullAddress;
    }

    /**
     * @param fullAddress The FullAddress
     */
    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    /**
     * @return The geoLatitude
     */
    public String getGeoLatitude() {
        return geoLatitude;
    }

    /**
     * @param geoLatitude The GeoLatitude
     */
    public void setGeoLatitude(String geoLatitude) {
        this.geoLatitude = geoLatitude;
    }

    /**
     * @return The geoLongitude
     */
    public String getGeoLongitude() {
        return geoLongitude;
    }

    /**
     * @param geoLongitude The GeoLongitude
     */
    public void setGeoLongitude(String geoLongitude) {
        this.geoLongitude = geoLongitude;
    }

    /**
     * @return The regionId
     */
    public Integer getRegionId() {
        return regionId;
    }

    /**
     * @param regionId The RegionId
     */
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    /**
     * @return The isDefaultPassword
     */
    public Boolean getIsDefaultPassword() {
        return isDefaultPassword;
    }

    /**
     * @param isDefaultPassword The IsDefaultPassword
     */
    public void setIsDefaultPassword(Boolean isDefaultPassword) {
        this.isDefaultPassword = isDefaultPassword;
    }

}
