package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.DutyAttendanceDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.rota.DutyAttendanceIR;
import com.sisindia.ai.android.rota.DutyAttendanceOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 08-11-2016.
 */

public class DutyAttendanceSyncing extends CommonSyncDbOperation {
    private final DutyAttendanceDB dutyAttendanceDb;
    private SISClient sisClient;
    private List<DutyAttendanceIR> dutyAttendanceIRList;
    private AppPreferences appPreferences = null;

    public DutyAttendanceSyncing(Context mcontext) {
        super(mcontext);
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mcontext);
        dutyAttendanceDb = IOPSApplication.getInstance().getDutyAttendanceDB();
        this.dutyAttendanceIRList = new ArrayList<>();
        dutyAttendanceSyncing();
    }

    private void dutyAttendanceSyncing() {
        dutyAttendanceIRList = getDutyAttendance();
        if (dutyAttendanceIRList != null && dutyAttendanceIRList.size() != 0) {
            for (DutyAttendanceIR dutyAttendanceIR : dutyAttendanceIRList) {
                dutyAttendanceApiCall(dutyAttendanceIR);
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER + "Duty Attendance Syncing -->nothing to sync ");
        }
    }

    private List<DutyAttendanceIR> getDutyAttendance() {
        List<DutyAttendanceIR> dutyAttendanceList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE + " where " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        DutyAttendanceIR dutyAttendanceIR = new DutyAttendanceIR();
                        dutyAttendanceIR.setSeqId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        dutyAttendanceIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.DUTY_REPONSE_ID)));
                        dutyAttendanceIR.setDutyOnTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DUTY_ON_DATE_TIME)));
                        dutyAttendanceIR.setDutyOffTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DUTY_OFF_DATE_TIME)));

                        String receivedTaskAssigned = cursor.getString(cursor.getColumnIndex("TaskAssigned"));
                        String receivedTaskCompleted = cursor.getString(cursor.getColumnIndex("TaskCompleted"));
                        int taskAssignedCount = 0, taskCompletedCount = 0;

                        if (receivedTaskAssigned != null && !receivedTaskAssigned.isEmpty()) {
                            try {
                                taskAssignedCount = Integer.parseInt(receivedTaskAssigned);
                            } catch (Exception e) {
                            }
                        }
                        if (receivedTaskCompleted != null && !receivedTaskCompleted.isEmpty()) {
                            try {
                                taskAssignedCount = Integer.parseInt(receivedTaskCompleted);
                            } catch (Exception e) {
                            }
                        }

                        dutyAttendanceIR.setEmployeeId(appPreferences.getAppUserId());
                        dutyAttendanceIR.setTasksAssigned(taskAssignedCount);
                        dutyAttendanceIR.setTasksCompleted(taskCompletedCount);

                        Timber.d("Duty Attendance jsonObject  %s", IOPSApplication.getGsonInstance().toJson(dutyAttendanceIR));
                        dutyAttendanceList.add(dutyAttendanceIR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
//            Timber.d(Constants.TAG_SYNCADAPTER + "Duty Attendance object  %s", IOPSApplication.getGsonInstance().toJson(dutyAttendanceList));
        }
        return dutyAttendanceList;
    }


    private synchronized void dutyAttendanceApiCall(final DutyAttendanceIR dutyAttendanceIR) {
        sisClient.getApi().syncDutyAttendance(dutyAttendanceIR, new Callback<DutyAttendanceOR>() {
            @Override
            public void success(DutyAttendanceOR dutyAttendanceOR, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "Duty Attendance response  %s", dutyAttendanceOR);
                switch (dutyAttendanceOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        if (dutyAttendanceOR != null && dutyAttendanceOR.getDutyRepsonseData() != null) {
                            dutyAttendanceDb.updateDutyAttendanceId(dutyAttendanceOR.getDutyRepsonseData().getEmployeeDutyId(), dutyAttendanceIR.getSeqId());
                        }
                        Timber.d(Constants.TAG_SYNCADAPTER + "duty Attendance count   %s", "");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("Duty Attendance Syncing", error);
            }
        });
    }
}

