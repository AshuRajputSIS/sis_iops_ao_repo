package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/7/16.
 */
public class BarrackInspectionQuestionMO {


    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Question")
    @Expose
    private String question;
    @SerializedName("Description")
    @Expose
    private String description;

    @SerializedName("ControlType")
    @Expose
    private String controlType;
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;

    private Integer sequenceId;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }
}
