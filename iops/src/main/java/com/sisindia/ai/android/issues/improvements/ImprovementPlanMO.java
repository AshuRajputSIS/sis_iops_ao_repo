package com.sisindia.ai.android.issues.improvements;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 10-11-2016.
 */
public class ImprovementPlanMO {

    private  int openImporvementPlanCount;
    private ArrayList<Object> improvementPlanList;

    public ArrayList<Object> getImprovementPlanList() {
        return improvementPlanList;
    }

    public void setImprovementPlanList(ArrayList<Object> improvementPlanList) {
        this.improvementPlanList = improvementPlanList;
    }

    public int getOpenImporvementPlanCount() {
        return openImporvementPlanCount;
    }

    public void setOpenImporvementPlanCount(int openImporvementPlanCount) {
        this.openImporvementPlanCount = openImporvementPlanCount;
    }
}
