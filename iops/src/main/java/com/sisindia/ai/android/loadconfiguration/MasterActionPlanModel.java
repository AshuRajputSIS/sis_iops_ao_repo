package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterActionPlanModel {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ActionPlanName")
    @Expose
    private String actionPlanName;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The actionPlanName
     */
    public String getActionPlanName() {
        return actionPlanName;
    }

    /**
     * @param actionPlanName The ActionPlanName
     */
    public void setActionPlanName(String actionPlanName) {
        this.actionPlanName = actionPlanName;
    }

    /**
     * @return The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "MasterActionPlanModel{" +
                "id=" + id +
                ", actionPlanName='" + actionPlanName + '\'' +
                ", description=" + description +
                ", isActive=" + isActive +
                '}';
    }
}
