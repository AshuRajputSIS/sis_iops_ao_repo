package com.sisindia.ai.android.commons;

/**
 * Created by compass on 4/17/2017.
 */

public interface SyncResponseListener {
    void syncSuccessful();
    void syncFailure();
}
