package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Branch {

    @SerializedName("Id")
    @Expose
    public Integer Id;
    @SerializedName("Name")
    @Expose
    public String Name;
    @SerializedName("RegionId")
    @Expose
    public Integer RegionId;
    @SerializedName("Description")
    @Expose
    public String Description;
    @SerializedName("IsActive")
    @Expose
    public Boolean IsActive;
    @SerializedName("BranchCode")
    @Expose
    public String BranchCode;
    @SerializedName("BranchAddressId")
    @Expose
    public Integer BranchAddressId;
    @SerializedName("BranchAddress")
    @Expose
    public String BranchAddress;
    @SerializedName("GeoLatitude")
    @Expose
    public Double GeoLatitude;
    @SerializedName("GeoLongitude")
    @Expose
    public Double GeoLongitude;

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public Integer getBranchAddressId() {
        return BranchAddressId;
    }

    public void setBranchAddressId(Integer branchAddressId) {
        BranchAddressId = branchAddressId;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Double getGeoLatitude() {
        return GeoLatitude;
    }

    public void setGeoLatitude(Double geoLatitude) {
        GeoLatitude = geoLatitude;
    }

    public Double getGeoLongitude() {
        return GeoLongitude;
    }

    public void setGeoLongitude(Double geoLongitude) {
        GeoLongitude = geoLongitude;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getRegionId() {
        return RegionId;
    }

    public void setRegionId(Integer regionId) {
        RegionId = regionId;
    }
}