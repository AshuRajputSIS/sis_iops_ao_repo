
package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.SerializedName;

public class KitApparelSizeOR {

@SerializedName("Id")

private Integer id;
@SerializedName("SizeCode")

private String sizeCode;
@SerializedName("SizeName")

private String sizeName;
@SerializedName("SizeValue")

private Float sizeValue;
@SerializedName("IsActive")

private Boolean isActive;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The sizeCode
*/
public String getSizeCode() {
return sizeCode;
}

/**
* 
* @param sizeCode
* The SizeCode
*/
public void setSizeCode(String sizeCode) {
this.sizeCode = sizeCode;
}

/**
* 
* @return
* The sizeName
*/
public String getSizeName() {
return sizeName;
}

/**
* 
* @param sizeName
* The SizeName
*/
public void setSizeName(String sizeName) {
this.sizeName = sizeName;
}

/**
* 
* @return
* The sizeValue
*/
public Float getSizeValue() {
return sizeValue;
}

/**
* 
* @param sizeValue
* The SizeValue
*/
public void setSizeValue(Float sizeValue) {
this.sizeValue = sizeValue;
}

/**
* 
* @return
* The isActive
*/
public Boolean getIsActive() {
return isActive;
}

/**
* 
* @param isActive
* The IsActive
*/
public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

}
