package com.sisindia.ai.android.issues;

/**
 * Created by compass on 3/6/2017.
 */

public interface IssuesSyncListener {
    void issuesSyncing(boolean isSyned);
}
