package com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.EmployeeLeaveCalendar;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by shankar on 2/12/16.
 */

public class FCMBaseLeaveMO implements Serializable{
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private ArrayList<EmployeeLeaveCalendar> employeeLeaveCalendarList;

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<EmployeeLeaveCalendar> getEmployeeLeaveCalendarList() {
        return employeeLeaveCalendarList;
    }

    public void setEmployeeLeaveCalendarList(ArrayList<EmployeeLeaveCalendar> employeeLeaveCalendarList) {
        this.employeeLeaveCalendarList = employeeLeaveCalendarList;
    }
}
