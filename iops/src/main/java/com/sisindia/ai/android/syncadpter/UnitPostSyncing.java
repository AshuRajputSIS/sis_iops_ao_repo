package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.UnitPostIR;
import com.sisindia.ai.android.network.response.UnitAddPostResponse;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */

public class UnitPostSyncing extends CommonSyncDbOperation {
    private UnitEquipmentSyncing unitEquipmentSync;
    private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, UnitPostIR> unitPostRequestList;

    public UnitPostSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
     //  unitEquipmentSync = new UnitEquipmentSyncing(mContext);
        unitPostSyncing();
    }

    private  void unitPostSyncing() {
         unitPostRequestList = getPostDetails();
        if (unitPostRequestList != null && unitPostRequestList.size() != 0) {
            for (Integer key : unitPostRequestList.keySet()) {
                UnitPostIR unitPostIMO = unitPostRequestList.get(key);
                unitPostApiCAll(unitPostIMO, key);

                Timber.d(Constants.TAG_SYNCADAPTER+"unitPostSyncing count   %s", "count: " + count);
                Timber.d("unitPostSyncing   %s", "key: " + key + " value: " + unitPostRequestList.get(key));
            }

        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitAddPostData -->nothing to sync ");
            dependentFailureSync();
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(dependentSyncsStatus != null && dependentSyncsStatus.unitPostId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {
        if(dependentSyncsStatus.isUnitEquipmentSynced != Constants.DEFAULT_SYNC_COUNT) {
            if(dependentSyncsStatus != null){
                if(dependentSyncsStatus.unitPostId != 0) {
                    new UnitEquipmentSyncing(mContext, dependentSyncsStatus.unitPostId);
                }
            }
        }


        if(dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            if(dependentSyncsStatus != null){
                if(dependentSyncsStatus.unitPostId != 0) {
                    metaDataSyncing.setUnitPostId(dependentSyncsStatus.unitPostId);
                }
            }
        }
        else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                if(dependentSyncsStatus != null) {
                    if(dependentSyncsStatus.unitPostId != 0) {
                        imageSyncing.setUnitPostId(dependentSyncsStatus.unitPostId);
                    }
                }
            }
        }
    }




    /**
     * being used in the postcheck fragment for syncing the add post data to server
     *
     * @return
     */
    private HashMap<Integer, UnitPostIR> getPostDetails() {
        HashMap<Integer, UnitPostIR> unitPostRequestList = new HashMap<>();
        SQLiteDatabase sqlite = null;

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                " where " + TableNameAndColumnStatement.IS_NEW + "=" + 1 + " OR " +
                TableNameAndColumnStatement.IS_SYNCED + "=" + 0;
        Timber.d(Constants.TAG_SYNCADAPTER+"PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitPostIR postData = new UnitPostIR();
                        postData.setIsMainGate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_MAIN_GATE)));
                        postData.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        postData.setUnitPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                        postData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        String geoLocation = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) + "," +
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE));
                        postData.setDescription("");
                        postData.setIsActive(1);
                        postData.setGeoPoint(geoLocation);
                        postData.setMainGateDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.MAIN_GATE_DISTANCE)));
                        postData.setBarrackDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_DISTANCE)));
                        postData.setUnitOfficeDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE)));
                        postData.setArmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ARMED_STRENGTH)));
                        postData.setUnarmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNARMED_STRENGTH)));
                        postData.setDeviceNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DEVICE_NO)));

                        unitPostRequestList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), postData);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"addPostList object  %s", IOPSApplication.getGsonInstance().toJson(unitPostRequestList));

        }
        return unitPostRequestList;
    }

    /**
     * @param unit_post_id
     * @param id
     */
    private void updatePostidUnitPostTable(int unit_post_id, int id) {
        SQLiteDatabase sqlite = null;



        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                " SET " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + unit_post_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdatePostidUnitPostTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }

    /**
     *
     *
     * @param id
     */
    private void updatePostidUnitEquipmentTable(UnitAddPostResponse unitAddPostResponse, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " SET " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + unitAddPostResponse
                .getData().getPostId() + "'" +
                " WHERE " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdatePostidUnitEquipmentTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());



        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }

    private void unitPostApiCAll(final UnitPostIR unitPostIMO, final int key) {
        sisClient.getApi().syncUnitPost(unitPostIMO, new Callback<UnitAddPostResponse>() {
            @Override
            public void success(UnitAddPostResponse unitResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"UnitAddPostData response  %s", unitResponse);
                switch (unitResponse.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        updatePostidUnitPostTable(unitResponse.getData().getPostId(), key);  //updating PostID in UnitPost Table
                        updatePostidUnitEquipmentTable(unitResponse, key); //updating PostID in UnitEquipment Table
                        updatePostidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, 0,
                                unitResponse.getData().getPostId(), key, Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.UNIT_POST, mContext));
                        updateStatus(unitResponse,unitPostIMO);
                        //updating PostID in Metadata Table


                        break;

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UNIT_POST_SYNC", error);
            }
        });

        Timber.d(Constants.TAG_SYNCADAPTER+"unitPostApiCAll end count   %s", "count: " + count);


    }

    private void updateStatus(UnitAddPostResponse unitResponse, UnitPostIR unitPostIMO) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.unitPostId = unitResponse.getData().getPostId();
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isUnitEquipmentSynced = Constants.NEG_DEFAULT_COUNT;

        if(unitPostIMO.getId() == null ){
            insertDepedentSyncData(dependentSyncsStatus,true);
        }
        else
        {
            if(unitPostIMO.getId() == 0){
                insertDepedentSyncData(dependentSyncsStatus,true);
            }
            else{
                insertDepedentSyncData(dependentSyncsStatus,false);
            }

        }
        unitEquipmentSync = new UnitEquipmentSyncing(mContext,unitResponse.getData().getPostId());
        MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
        metaDataSyncing.setUnitPostId(unitResponse.getData().getPostId());
    }

    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus,boolean isNewlyCreated) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.unitPostId,
                TableNameAndColumnStatement.UNIT_POST_ID, isNewlyCreated)){
            if(isNewlyCreated){
                dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
            }
            else{
                dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,2);
            }

        }
    }

}
