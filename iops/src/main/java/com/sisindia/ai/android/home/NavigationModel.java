package com.sisindia.ai.android.home;

/**
 * Created by Saruchi on 19-03-2016.
 */
public class NavigationModel {

    private String title;
    private int Icon;
    private int count;
    private boolean isNotificationVisible;

    NavigationModel() {

    }

    public NavigationModel(String title, int Icon, int count, boolean isNotificationVisible) {
        this.title = title;
        this.Icon = Icon;
        this.count = count;
        this.isNotificationVisible = isNotificationVisible;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String Title) {
        this.title = Title;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        this.Icon = icon;
    }

    public int getNotificationCount() {
        return count;
    }

    public void setNotificationCount(int count) {
        this.count = count;
    }

    public boolean getisNotificationVisible() {
        return isNotificationVisible;
    }

    public void setisNotificationVisible(boolean isNotificationVisible) {
        this.isNotificationVisible = isNotificationVisible;
    }
}
