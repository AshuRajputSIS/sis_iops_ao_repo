package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RewardFineIMO {

    @SerializedName("RewardType")
    @Expose
    private String RewardType;
    @SerializedName("FineAmount")
    @Expose
    private String FineAmount;
    @SerializedName("Reason")
    @Expose
    private String Reason;

    /**
     * @return The RewardType
     */
    public String getRewardType() {
        return RewardType;
    }

    /**
     * @param RewardType The RewardType
     */
    public void setRewardType(String RewardType) {
        this.RewardType = RewardType;
    }

    /**
     * @return The FineAmount
     */
    public String getFineAmount() {
        return FineAmount;
    }

    /**
     * @param FineAmount The FineAmount
     */
    public void setFineAmount(String FineAmount) {
        this.FineAmount = FineAmount;
    }

    /**
     * @return The Reason
     */
    public String getReason() {
        return Reason;
    }

    /**
     * @param Reason The Reason
     */
    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    @Override
    public String toString() {
        return "RewardFineIMO{" +
                "RewardType='" + RewardType + '\'' +
                ", FineAmount='" + FineAmount + '\'' +
                ", Reason='" + Reason + '\'' +
                '}';
    }
}