package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SingleUnitBaseOR {

    @SerializedName("StatusCode")
    private Integer statusCode;
    @SerializedName("StatusMessage")
    private String statusMessage;
    @SerializedName("Data")
    private List<SingleUnitMO> singleUnitMOList = new ArrayList<>();

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * @return The data
     */
    public List<SingleUnitMO> getData() {
        return singleUnitMOList;
    }

    /**
     * @param data The Data
     */
    public void setData(List<SingleUnitMO> data) {
        this.singleUnitMOList = data;
    }

}