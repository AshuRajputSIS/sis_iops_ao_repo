package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.rota.AddTaskRecyclerAdapter;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 07-05-2016.
 */
public class AddContactCardViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<UnitContactsMO> addContactList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public AddContactCardViewAdapter(Context context) {
        this.context = context;

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    public void setContactList(ArrayList<UnitContactsMO> addContactList) {
        this.addContactList = addContactList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView = null;

        itemView = LayoutInflater.from(context).inflate(R.layout.contact_list_item, parent, false);
        recycleViewHolder = new CardViewHolder(itemView);

        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CardViewHolder viewHolder = (AddContactCardViewAdapter.CardViewHolder) holder;
        UnitContactsMO unitContactsMO =addContactList.get(position);

        viewHolder.clientNameTxt.setText(unitContactsMO.getContactFirstName()+" "+unitContactsMO.getContactLastName());
        viewHolder.clientRoleTxt.setText(unitContactsMO.getClientDesignation());
        viewHolder.contactPhone.setText(unitContactsMO.getContactNumber());
        if(unitContactsMO.getContactEmail().trim().length()!=0){
            viewHolder.contactEmail.setText(unitContactsMO.getContactEmail());
        }
        else{
            viewHolder.contactEmail.setText("NA");
        }


    }

    @Override
    public int getItemCount() {
        int count = addContactList == null ? 0 : addContactList.size();
        return count;
    }


    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.client_name_txt)
        CustomFontTextview clientNameTxt;
        @Bind(R.id.client_role_txt)
        CustomFontTextview clientRoleTxt;
        @Bind(R.id.contact_phone)
        CustomFontTextview contactPhone;
        @Bind(R.id.contact_email)
        CustomFontTextview contactEmail;
        @Bind(R.id.edit_contact_label_txt)
        CustomFontTextview editContactLabelTxt;
        @Bind(R.id.imageEdit)
        ImageView editImage;


        public CardViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            editContactLabelTxt.setOnClickListener(this);
            editImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.edit_contact_label_txt || view.getId() == R.id.imageEdit)
                onRecyclerViewItemClickListener.onRecyclerViewItemClick(view, getLayoutPosition());
        }
    }
}

