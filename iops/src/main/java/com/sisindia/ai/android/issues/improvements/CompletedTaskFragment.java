package com.sisindia.ai.android.issues.improvements;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CompletedTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompletedTaskFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CURRENT_TAB = "current_tab";
    public static CompletedTaskFragment fragment;
    static boolean isfromUnitRisk;
    ArrayList<Object> mDataValues = new ArrayList<>();
    CustomPlanTaskAdapter customIssuesAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int mCurrentPos;

    public CompletedTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompletedTaskFragment newInstance(String param1, String param2, boolean isFromUnitRisk, int currentTab) {
        if (fragment == null) {
            fragment = new CompletedTaskFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            args.putInt(CURRENT_TAB, currentTab);
            isfromUnitRisk = isFromUnitRisk;
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mCurrentPos = getArguments().getInt(CURRENT_TAB);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.compeleted_plans_fragment, container, false);
        initComplaintsViews(view);
        return view;
    }

    private void initComplaintsViews(View view) {
        RecyclerView mgrievance_recyclerview = (RecyclerView) view.findViewById(R.id.compeleted_improvement_plan_recyclerview);
        mgrievance_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (isfromUnitRisk) {
            customIssuesAdapter = new CustomPlanTaskAdapter(getContext(), mDataValues, false, isfromUnitRisk, 1);
        } else {
            customIssuesAdapter = new CustomPlanTaskAdapter(getContext(), mDataValues, false, isfromUnitRisk, 1);
        }
        mgrievance_recyclerview.setAdapter(customIssuesAdapter);
    }

    public void updateRecyclerview(ArrayList<Object> models) {
        mDataValues.addAll(models);
        customIssuesAdapter.notifyDataSetChanged();
    }
}
