package com.sisindia.ai.android.appupdate;

/**
 * Created by hannan on 01-04-2014.
 */
public interface AppUpdateListener {
    void onDownloadSuccess();
    void onDownloadFailure(String message);
}
