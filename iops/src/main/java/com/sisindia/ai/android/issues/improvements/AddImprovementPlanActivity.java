package com.sisindia.ai.android.issues.improvements;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.ActionPlanAdapter;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Saruchi on 15-04-2016.
 */
public class AddImprovementPlanActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private final static int UNIT_LOOKUP = 12;
    private final static int BARRACK_LOOKUP = 11;
    private final static int IMPROVEMENTPLAN_ISSUE_TYPEID = 4;
    @Bind(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @Bind(R.id.unit_radioButton)
    RadioButton mUnitRB;
    @Bind(R.id.barrack_radioButton)
    RadioButton mBarrackRB;
    @Bind(R.id.toolbar)
    Toolbar mImprovementToolBar;
    @Bind(R.id.scrollview_addplan)
    ScrollView mscrollView;
    @Bind(R.id.unit_layout)
    View unitLayout;
    @Bind(R.id.category_layout)
    View categoryLayout;
    @Bind(R.id.improvement_layout)
    View improvementLayout;
    @Bind(R.id.target_action_date_view)
    View mcalenderView;
    @Bind(R.id.remarks_view)
    View remarksView;
    /* @Bind(R.id.budget_EdtTxt)
     EditText budgetEdtTxt;*/
    ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
    @Bind(R.id.spinner_title)
    CustomFontTextview spinnerTitle;
    @Bind(R.id.budgetLabel)
    CustomFontTextview budgetLabel;
    @Bind(R.id.budgetEdtTxt)
    CustomFontEditText budgetEdtTxt;
    private String type;
    private TextView mTargetDate;
    private LinearLayout mCalenderLayout;
    private TextView unitName;
    private EditText remarksTxt;
    private TextView categoryName, improvementName;
    private Resources resources;
    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private LookUpSpinnerAdapter mLookUpSpinnerAdapter;
    private Spinner categorgySpinner;
    private Spinner improvementSpinner;
    private Spinner unitBarrackSpinner;
    private UnitBarrackSpinnerAdapter unitBarrackAdadpter;
    private ActionPlanAdapter actionPlanAdapter;
    private int mUnitBarrackId = -1;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String targetdateStr;
    private ImprovementPlanIR improvementPlanIRModel;
    private ArrayList<Object> objectlist = new ArrayList<>();
    private boolean isbarrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.improvement_add_plan);
        ButterKnife.bind(this);
        Util.hideKeyboard(this);
        resources = getResources();
        isbarrack = false;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
        improvementPlanIRModel = new ImprovementPlanIR();
        setUpViews();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_complaint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_complaint_done:
                validation();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpViews() {
        setSupportActionBar(mImprovementToolBar);
        mscrollView.smoothScrollTo(0, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_improvement_plans);
        unitName = (TextView) unitLayout.findViewById(R.id.spinner_title);
        categoryName = (TextView) categoryLayout.findViewById(R.id.spinner_title);
        improvementName = (TextView) improvementLayout.findViewById(R.id.spinner_title);
        unitBarrackSpinner = (Spinner) unitLayout.findViewById(R.id.issue_spinner);
        categorgySpinner = (Spinner) categoryLayout.findViewById(R.id.issue_spinner);
        improvementSpinner = (Spinner) improvementLayout.findViewById(R.id.issue_spinner);
        mTargetDate = (TextView) mcalenderView.findViewById(R.id.edittext_target_date);
        mCalenderLayout = (LinearLayout) mcalenderView.findViewById(R.id.calenderLayout);
        remarksTxt = (EditText) remarksView.findViewById(R.id.editTextTypeRemark);
        unitName.setText(resources.getString(R.string.unit));
        categoryName.setText(resources.getString(R.string.category_str));
        improvementName.setText(TableNameAndColumnStatement.IMPROVEMENT_PLAN);
        mRadioGroup.setOnCheckedChangeListener(this);
        mRadioGroup.check(mUnitRB.getId());
        if (util != null) {
            unitName.setText(util.createSpannableStringBuilder(getResources().getString(R.string.unit)));
            categoryName.setText(util.createSpannableStringBuilder(getResources().getString(R.string.category_str)));
            improvementName.setText(util.createSpannableStringBuilder(TableNameAndColumnStatement.IMPROVEMENT_PLAN));
        } else {
            Timber.d("Util :" + "Util object is null");
        }


        // default values for the input request model
        improvementPlanIRModel.setActionPlanStatusId(1); // pending staus
        improvementPlanIRModel.setActionPlanId(0);

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.unit_radioButton:
                type = resources.getString(R.string.unit);
                clearData();
                // unit lookup code is 12 for improvement plan
                unitName.setText(resources.getString(R.string.unit));
                populateUnitBarrackSpinnerData();
                break;
            case R.id.barrack_radioButton:
                type = resources.getString(R.string.barrack);
                clearData();
                // barrack lookup code is 11 for improvement plan
                unitName.setText(resources.getString(R.string.barrack));
                populateUnitBarrackSpinnerData();
                break;
        }
    }

    private void populateLookUpSpinner(final int lookupCode) {
        final ArrayList<LookupModel> lookupModelArrayList = new ArrayList<>();
        LookupModel lookupModel = new LookupModel();
        lookupModel.setName(resources.getString(R.string.select_category));
        lookupModelArrayList.add(lookupModel);
        lookupModelArrayList.addAll(issueLevelSelectionStatementDB.getImprovementLookupData(TableNameAndColumnStatement.lookup_improvementPlanCategory, lookupCode));
        if (mLookUpSpinnerAdapter == null) {
            mLookUpSpinnerAdapter = new LookUpSpinnerAdapter(this);
            mLookUpSpinnerAdapter.setSpinnerData(lookupModelArrayList);
            categorgySpinner.setAdapter(mLookUpSpinnerAdapter);
        } else {
            mLookUpSpinnerAdapter.setSpinnerData(lookupModelArrayList);
            categorgySpinner.setAdapter(mLookUpSpinnerAdapter);
        }

        categorgySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    int sub_category_id = lookupModelArrayList.get(position).getLookupIdentifier();
                    improvementPlanIRModel.setImprovementPlanCategoryId(sub_category_id);
                    populateImprovementPlanSpinner(lookupCode, sub_category_id);
                } else {
                    populateImprovementPlanSpinner(-1, -1); /// invalidate
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void populateUnitBarrackSpinnerData() {
        objectlist.clear();
        if (type.equalsIgnoreCase(resources.getString(R.string.unit))) {
            ArrayList<MyUnitHomeMO> unitList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitDetails(0);
            objectlist.addAll(unitList);
            objectlist.add(resources.getString(R.string.select_unit));
            unitList.clear();
        } else {

            List<Barrack> barrackList = IOPSApplication.getInstance().
                    getMyBarrackStatementDB().getBarrackDetails();
            objectlist.addAll(barrackList);
            objectlist.add(resources.getString(R.string.select_barrack));

            barrackList.clear();
        }


        if (unitBarrackAdadpter == null) {
            unitBarrackAdadpter = new UnitBarrackSpinnerAdapter(this, 0);
            unitBarrackAdadpter.setSpinnerData(objectlist);
            unitBarrackSpinner.setAdapter(unitBarrackAdadpter);
            unitBarrackSpinner.setSelection(unitBarrackAdadpter.getCount());

        } else {
            unitBarrackAdadpter.setSpinnerData(objectlist);
            unitBarrackSpinner.setAdapter(unitBarrackAdadpter);
            unitBarrackSpinner.setSelection(unitBarrackAdadpter.getCount());
        }

        if (null != objectlist && 0 != objectlist.size() - 1) {  // check the size and then make the category and improvement layout visibility
            categoryLayout.setVisibility(View.VISIBLE);
            improvementLayout.setVisibility(View.VISIBLE);
            remarksView.setVisibility(View.VISIBLE);
            isbarrack = true;

            unitBarrackSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Object obj = objectlist.get(position);
                    if (obj instanceof MyUnitHomeMO) {
                        MyUnitHomeMO unitHomeMO = (MyUnitHomeMO) obj;
                        mUnitBarrackId = unitHomeMO.getUnitId();
                        populateLookUpSpinner(UNIT_LOOKUP); // 12 is for unit in lookuptable
                        improvementPlanIRModel.setUnitId(mUnitBarrackId);

                    } else if (obj instanceof Barrack) {
                        Barrack barrackMo = (Barrack) obj;
                        mUnitBarrackId = barrackMo.getId();
                        populateLookUpSpinner(BARRACK_LOOKUP); // 11 is for unit in lookuptable
                        improvementPlanIRModel.setBarrackId(mUnitBarrackId);
                    } else {
                        mUnitBarrackId = -1;
                        populateLookUpSpinner(-1);//invalidate
                        mcalenderView.setVisibility(View.GONE); // calender view gone
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            categoryLayout.setVisibility(View.GONE);
            improvementLayout.setVisibility(View.GONE);
            remarksView.setVisibility(View.GONE);
            isbarrack = false;
        }
    }

    private void populateImprovementPlanSpinner(int issuecategoryId, int categorySubId) {
        improvementActionPlanList.clear();
        if (!((-1 == issuecategoryId) && (-1 == categorySubId))) {
            if (type.equalsIgnoreCase(resources.getString(R.string.unit))) {
                improvementActionPlanList.addAll(issueLevelSelectionStatementDB.getActionPlanList(mUnitBarrackId, IMPROVEMENTPLAN_ISSUE_TYPEID, issuecategoryId, categorySubId));
            } else {
                improvementActionPlanList.addAll(issueLevelSelectionStatementDB.getBarrackImprovementActionPlan(IMPROVEMENTPLAN_ISSUE_TYPEID, issuecategoryId, categorySubId));

            }
        } else {
            // display only select improvement plan in the list until the category the selected
        }

        if (actionPlanAdapter == null) {
            actionPlanAdapter = new ActionPlanAdapter(this);
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            improvementSpinner.setAdapter(actionPlanAdapter);
        } else {
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            improvementSpinner.setAdapter(actionPlanAdapter);
        }

        improvementSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (0 != position) {
                    mcalenderView.setVisibility(View.VISIBLE);
                    improvementPlanIRModel.setMasterActionPlanId(improvementActionPlanList.get(position).getId());
                    improvementPlanIRModel.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                    displayActionDate(improvementActionPlanList.get(position).getTurnAroundTime());
                } else {
                    mcalenderView.setVisibility(View.GONE);
                    improvementPlanIRModel.setMasterActionPlanId(null);
                    improvementPlanIRModel.setIssueMatrixId(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void displayActionDate(int days) {

        targetdateStr = dateTimeFormatConversionUtil.addDaysToCurrentDate(days);
        mTargetDate.setText(targetdateStr);
        mCalenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });

    }

    public void showCalendar() {
        Date mindate = dateTimeFormatConversionUtil.convertStringToDateFormat(dateTimeFormatConversionUtil.getCurrentDate());
        Date maxDate = dateTimeFormatConversionUtil.convertStringToDateFormat(targetdateStr);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        DateRangeCalender selectDateFragment = DateRangeCalender.newInstance(mindate, maxDate);
        selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                String updateDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                mTargetDate.setText(updateDate);

                //issuesMO.setTargetActionEndDate(updateDate);
            }
        });
    }


    private void validation() {
        if (budgetEdtTxt.getText().toString().trim().length() > 0) {
            improvementPlanIRModel.setBudget(budgetEdtTxt.getText().toString().trim());
        } else {
            improvementPlanIRModel.setBudget(null);
        }
        if (!isbarrack) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.unit_barrack_notavailable));
            return;
        }
        if (mUnitBarrackId == -1) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.client_handshake_validation));
            return;
        } else if (null == mTargetDate || 0 == mTargetDate.getText().toString().trim().length()) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.client_handshake_validation));
            return;
        }
        else if (improvementPlanIRModel.getMasterActionPlanId() == null) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.action_plan_error_msg));
            return;
        } else {
            improvementPlanIRModel.setExpectedClosureDate(mTargetDate.getText().toString().trim() + " 00:00:00");
            improvementPlanIRModel.setRaisedDate(dateTimeFormatConversionUtil.getCurrentDateTime());
            improvementPlanIRModel.setRemarks(remarksTxt.getText().toString().trim());
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addImprovementPlanDetails(improvementPlanIRModel);
            Bundle bundel = new Bundle();
            IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
            appPreferences.setIssueType(Constants.NavigationFlags.TYPE_IMPROVEMENT_PLAN);
            IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_improvement_plans));
            finish();
        }
    }

    /**
     * clear the fileds on change of selection
     */
    private void clearData(){
        budgetEdtTxt.setText("");
        remarksTxt.setText("");
    }

}
