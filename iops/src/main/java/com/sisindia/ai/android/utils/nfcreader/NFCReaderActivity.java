package com.sisindia.ai.android.utils.nfcreader;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.myunits.UnitPostAddEditActivity;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.NfcCheckResponse;
import com.sisindia.ai.android.rota.TaskDetailsActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

import static com.sisindia.ai.android.R.string.nfc_configured_successfully;
import static com.sisindia.ai.android.R.string.position;

/**
 * Created by Hannan Shaik on 7/18/16.
 * Reference : http://code.tutsplus.com/tutorials/reading-nfc-tags-with-android--mobile-17278
 */
public class NFCReaderActivity extends BaseActivity {

    private static final String TAG = NFCReaderActivity.class.getSimpleName();

    private NfcAdapter mNfcAdapter;
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Timber.d(TAG, "Wrong mime type: %s", type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];
            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                return null;
            }

            String nDefUid = byteArrayToString(tag.getId());
            Timber.d("NFC nDefUid : %s", nDefUid);
           /* RotaTaskModel rotaTaskModel = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getTaskUsingNfcTag(nDefUid,
                    dateTimeFormatConversionUtil.getWeekOfTheYear(dateTimeFormatConversionUtil.getCurrentDate()),
                    dateTimeFormatConversionUtil.getCurrentDate());
            if(rotaTaskModel != null){
            startTask(IOPSApplication.getInstance().getRotaLevelSelectionInstance().getTaskUsingNfcTag(nDefUid,
                    dateTimeFormatConversionUtil.getWeekOfTheYear(dateTimeFormatConversionUtil.getCurrentDate()),
                    dateTimeFormatConversionUtil.getCurrentDate()));
            }*/

            /*NdefMessage ndefMessage = ndef.getCachedNdefMessage();
            Log.d(TAG, "NFC NdefMessage : " + ndefMessage);

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding", e);
                    }
                }
            }
*/
            return nDefUid;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         * http://www.nfc-forum.org/specs/
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */
            byte[] payload = record.getPayload();
            String record1 = byteArrayToString(record.getId());
            Timber.d(TAG, "NFC record1 : %s", record1);
            String payload1 = byteArrayToString(payload);
            Timber.d(TAG, "NFC payload1 : %s", payload1);
            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;
            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
               /* try {
                    UnitPost postInformation = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getPostInformationUsingNfc(result, -5);
                    if (postInformation != null && postInformation.getId() != null) {
                        RotaTaskModel rotaTaskModel = IOPSApplication.getInstance()
                                .getRotaLevelSelectionInstance()
                                .getTaskUsingNfcTag(result, dateTimeFormatConversionUtil.getWeekOfTheYear(dateTimeFormatConversionUtil
                                        .getCurrentDate()), dateTimeFormatConversionUtil.getCurrentDate());
                        if (rotaTaskModel != null) {
                            startTask(rotaTaskModel);
                        } else {
                            *//*Toast.makeText(NFCReaderActivity.this, getString(R.string.no_tasks_configure_for_nfc_tag),
                                    Toast.LENGTH_SHORT).show();*//*
                            Intent intent = new Intent(Constants.Nfc.NFC_SCAN);
                            intent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, result);
                            sendBroadcast(intent);
                        }
                    }
                    else {
                        *//*RotaTaskModel rotaTaskModel = IOPSApplication.getInstance()
                                .getRotaLevelSelectionInstance()
                                .getTaskUsingNfcTag(result, dateTimeFormatConversionUtil.getWeekOfTheYear(dateTimeFormatConversionUtil
                                        .getCurrentDate()), dateTimeFormatConversionUtil.getCurrentDate());
                        if (rotaTaskModel != null) {
                            if(rotaTaskModel.getTaskStatusId()==2)
                            startTask(rotaTaskModel);
                        } else {
                            Intent intent = new Intent(Constants.Nfc.NFC_SCAN);
                            intent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, result);
                            sendBroadcast(intent);
                        }*//*

                        Intent intent = new Intent(Constants.Nfc.NFC_SCAN);
                        intent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, result);
                        sendBroadcast(intent);

                    }


                } catch (NumberFormatException ex) {
                    Timber.e(ex, TAG);
                }*/
                Intent intent = new Intent(Constants.Nfc.NFC_SCAN);
                intent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, result);
                sendBroadcast(intent);
                finish();
            }
        }

        private void startTask(RotaTaskModel rotaTaskModel) {
            SingletonUnitDetails singletonUnitDetails = SingletonUnitDetails.getInstance();
            singletonUnitDetails.setUnitDetails(rotaTaskModel, 0);
            appPreferences.setRotaTaskJsonData(rotaTaskModel);
           /* updateRotaTaskModelListener.updateRotaTask(rotaTaskList.get(position));
            IOPSApplication.getInstance().getUpdateStatementDBInstance().
                    updatePostBasedOnUnit(((RotaTaskModel) rotaTaskList.get(position)).getUnitId());*/
            Intent taskDetailsIntent = new Intent(NFCReaderActivity.this, TaskDetailsActivity.class);
            taskDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(taskDetailsIntent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        setupForegroundDispatch(this, mNfcAdapter);
    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        stopForegroundDispatch(this, mNfcAdapter);

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    /**
     * @param activity The corresponding {@link Activity} requesting the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    /**
     * @param activity The corresponding {@link BaseActivity} requesting to stop the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    private String byteArrayToString(byte[] byteArray) {
        return String.format("%0" + (byteArray.length * 2) + "X", new BigInteger(1, byteArray));
    }

    /*private void checkNfcAddStatusOnline(int unitId, final String uniqueId) {
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.Nfc.UNIT_ID, unitId);
        map.put(Constants.Nfc.DEVICE_NO, uniqueId);
        checkNfcAddStatusViaApi(map);
    }

    private void checkNfcAddStatusViaApi(Map<String, Object> map) {
        new SISClient(this).getApi().checkNfcStatus(map, new Callback<NfcCheckResponse>() {
            @Override
            public void success(NfcCheckResponse nfcCheckResponse, Response response) {
                Timber.d("NFC add status check : %s", nfcCheckResponse);
                nfcAddAllowed = nfcCheckResponse.nfcData.allowToAdd;
                if(!nfcAddAllowed){
                    Toast.makeText(UnitPostAddEditActivity.this, R.string.nfc_already_added,
                            Toast.LENGTH_SHORT).show();
                    nfcUniqueId = null;
                }
                else{
                    Toast.makeText(UnitPostAddEditActivity.this, getString(R.string.nfc_configured_successfully),
                            Toast.LENGTH_SHORT).show();
                    IOPSApplication.getInstance().getUnitPostUpdateStatementDB().updateNfcDeviceNo(nfcUniqueId,
                            -1,unitId);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error, "Error while getting add nfc status");
            }
        });
    }*/

}