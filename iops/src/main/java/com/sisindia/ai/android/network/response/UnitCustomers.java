package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class UnitCustomers {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("CustomerName")

    public String CustomerName;
    @SerializedName("CustomerDescription")

    public String CustomerDescription;
    @SerializedName("IsActive")

    public Boolean IsActive;
    @SerializedName("CustomerHoAddressId")

    public Integer CustomerHoAddressId;
    @SerializedName("CustomerPrimaryContactId")

    public Integer CustomerPrimaryContactId;
    @SerializedName("CustomerSecondaryContactId")

    public Integer CustomerSecondaryContactId;
    @SerializedName("IsSmsToBeSent")

    public Boolean IsSmsToBeSent;
    @SerializedName("IsEmailToBeSent")

    public Boolean IsEmailToBeSent;
    @SerializedName("CustomerHoAddress")

    public String CustomerHoAddress;
    @SerializedName("CustomerPrimaryContactName")

    public String CustomerPrimaryContactName;
    @SerializedName("CustomerSecondaryContactName")

    public String CustomerSecondaryContactName;
    @SerializedName("PrimaryContactEmail")

    public String PrimaryContactEmail;
    @SerializedName("PrimaryContactPhone")

    public String PrimaryContactPhone;
    @SerializedName("SecondaryContactEmail")

    public String SecondaryContactEmail;
    @SerializedName("SecondaryContactPhone")

    public String SecondaryContactPhone;


    public String getCustomerDescription() {
        return CustomerDescription;
    }

    public void setCustomerDescription(String customerDescription) {
        CustomerDescription = customerDescription;
    }

    public String getCustomerHoAddress() {
        return CustomerHoAddress;
    }

    public void setCustomerHoAddress(String customerHoAddress) {
        CustomerHoAddress = customerHoAddress;
    }

    public Integer getCustomerHoAddressId() {
        return CustomerHoAddressId;
    }

    public void setCustomerHoAddressId(Integer customerHoAddressId) {
        CustomerHoAddressId = customerHoAddressId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public Integer getCustomerPrimaryContactId() {
        return CustomerPrimaryContactId;
    }

    public void setCustomerPrimaryContactId(Integer customerPrimaryContactId) {
        CustomerPrimaryContactId = customerPrimaryContactId;
    }

    public String getCustomerPrimaryContactName() {
        return CustomerPrimaryContactName;
    }

    public void setCustomerPrimaryContactName(String customerPrimaryContactName) {
        CustomerPrimaryContactName = customerPrimaryContactName;
    }

    public Integer getCustomerSecondaryContactId() {
        return CustomerSecondaryContactId;
    }

    public void setCustomerSecondaryContactId(Integer customerSecondaryContactId) {
        CustomerSecondaryContactId = customerSecondaryContactId;
    }

    public String getCustomerSecondaryContactName() {
        return CustomerSecondaryContactName;
    }

    public void setCustomerSecondaryContactName(String customerSecondaryContactName) {
        CustomerSecondaryContactName = customerSecondaryContactName;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Boolean getEmailToBeSent() {
        return IsEmailToBeSent;
    }

    public void setEmailToBeSent(Boolean emailToBeSent) {
        IsEmailToBeSent = emailToBeSent;
    }

    public Boolean getSmsToBeSent() {
        return IsSmsToBeSent;
    }

    public void setSmsToBeSent(Boolean smsToBeSent) {
        IsSmsToBeSent = smsToBeSent;
    }

    public String getPrimaryContactEmail() {
        return PrimaryContactEmail;
    }

    public void setPrimaryContactEmail(String primaryContactEmail) {
        PrimaryContactEmail = primaryContactEmail;
    }

    public String getPrimaryContactPhone() {
        return PrimaryContactPhone;
    }

    public void setPrimaryContactPhone(String primaryContactPhone) {
        PrimaryContactPhone = primaryContactPhone;
    }

    public String getSecondaryContactEmail() {
        return SecondaryContactEmail;
    }

    public void setSecondaryContactEmail(String secondaryContactEmail) {
        SecondaryContactEmail = secondaryContactEmail;
    }

    public String getSecondaryContactPhone() {
        return SecondaryContactPhone;
    }

    public void setSecondaryContactPhone(String secondaryContactPhone) {
        SecondaryContactPhone = secondaryContactPhone;
    }
}
