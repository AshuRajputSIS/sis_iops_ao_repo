package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitPost implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("UnitPostName")
    @Expose
    private String unitPostName;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("MainGateDistance")
    @Expose
    private double mainGateDistance;
    @SerializedName("BarrackDistance")
    @Expose
    private double barrackDistance;
    @SerializedName("UnitOfficeDistance")
    @Expose
    private double unitOfficeDistance;
    @SerializedName("ArmedStrength")
    @Expose
    private Integer armedStrength;
    @SerializedName("UnarmedStrength")
    @Expose
    private Integer unarmedStrength;
    @SerializedName("GeoPoint")
    @Expose
    private String geoPoint;
    @SerializedName("IsMainGate")
    @Expose
    private Boolean isMainGate;

    @SerializedName("DeviceNo")
    public String deviceNo;

    @SerializedName("UnitPostCoordinates")
    @Expose
    private List<UnitPostCoordinate> unitPostCoordinates = new ArrayList<UnitPostCoordinate>();
    @SerializedName("UnitPostGeoPoint")
    @Expose
    private List<UnitPostGeoPoint> unitPostGeoPoint = new ArrayList<UnitPostGeoPoint>();
    @SerializedName("UnitEquipments")
    @Expose
    private List<UnitEquipments> unitEquipments = new ArrayList<UnitEquipments>();

    private Boolean isNew=false;
    private Boolean isSynced=true;

    public Boolean isSynced() {
        return isSynced;
    }

    public void setSynced(Boolean synced) {
        isSynced = synced;
    }

    public Boolean isNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }



    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The unitPostName
     */
    public String getUnitPostName() {
        return unitPostName;
    }

    /**
     * @param unitPostName The UnitPostName
     */
    public void setUnitPostName(String unitPostName) {
        this.unitPostName = unitPostName;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The mainGateDistance
     */
    public double getMainGateDistance() {
        return mainGateDistance;
    }

    /**
     * @param mainGateDistance The MainGateDistance
     */
    public void setMainGateDistance(double mainGateDistance) {
        this.mainGateDistance = mainGateDistance;
    }

    /**
     * @return The barrackDistance
     */
    public double getBarrackDistance() {
        return barrackDistance;
    }

    /**
     * @param barrackDistance The BarrackDistance
     */
    public void setBarrackDistance(double barrackDistance) {
        this.barrackDistance = barrackDistance;
    }

    /**
     * @return The unitOfficeDistance
     */
    public double getUnitOfficeDistance() {
        return unitOfficeDistance;
    }

    /**
     * @param unitOfficeDistance The UnitOfficeDistance
     */
    public void setUnitOfficeDistance(double unitOfficeDistance) {
        this.unitOfficeDistance = unitOfficeDistance;
    }

    /**
     * @return The armedStrength
     */
    public Integer getArmedStrength() {
        return armedStrength;
    }

    /**
     * @param armedStrength The ArmedStrength
     */
    public void setArmedStrength(Integer armedStrength) {
        this.armedStrength = armedStrength;
    }

    /**
     * @return The unarmedStrength
     */
    public Integer getUnarmedStrength() {
        return unarmedStrength;
    }

    /**
     * @param unarmedStrength The UnarmedStrength
     */
    public void setUnarmedStrength(Integer unarmedStrength) {
        this.unarmedStrength = unarmedStrength;
    }

    /**
     * @return The geoPoint
     */
    public String getGeoPoint() {
        return geoPoint;
    }

    /**
     * @param geoPoint The GeoPoint
     */
    public void setGeoPoint(String geoPoint) {
        this.geoPoint = geoPoint;
    }

    /**
     * @return The isMainGate
     */
    public Boolean getIsMainGate() {
        return isMainGate;
    }

    /**
     * @param isMainGate The IsMainGate
     */
    public void setIsMainGate(Boolean isMainGate) {
        this.isMainGate = isMainGate;
    }

    /**
     * @return The unitPostCoordinates
     */
    public List<UnitPostCoordinate> getUnitPostCoordinates() {
        return unitPostCoordinates;
    }

    /**
     * @param unitPostCoordinates The UnitPostCoordinates
     */
    public void setUnitPostCoordinates(List<UnitPostCoordinate> unitPostCoordinates) {
        this.unitPostCoordinates = unitPostCoordinates;
    }

    /**
     * @return The unitPostGeoPoint
     */
    public List<UnitPostGeoPoint> getUnitPostGeoPoint() {
        return unitPostGeoPoint;
    }

    /**
     * @param unitPostGeoPoint The UnitPostGeoPoint
     */
    public void setUnitPostGeoPoint(List<UnitPostGeoPoint> unitPostGeoPoint) {
        this.unitPostGeoPoint = unitPostGeoPoint;
    }

    /**
     * @return The unitEquipments
     */
    public List<UnitEquipments> getUnitEquipments() {
        return unitEquipments;
    }

    /**
     * @param unitEquipments The UnitEquipments
     */
    public void setUnitEquipments(List<UnitEquipments> unitEquipments) {
        this.unitEquipments = unitEquipments;
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(((UnitPost) obj).unitId, this.unitId);
    }
}
