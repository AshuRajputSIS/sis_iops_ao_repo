package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

public class MetaDataIMO {

    @SerializedName("AttachmentTypeId")
    private Integer attachmentTypeId;
    @SerializedName("AttachmentTitle")
    private String attachmentTitle;
    @SerializedName("AttachmentExtension")
    private String attachmentExtension;
    @SerializedName("SizeInKB")
    private Integer sizeInKB;
    @SerializedName("AttachmentSourceTypeId")
    private Integer attachmentSourceTypeId;
    @SerializedName("AttachmentSourceId")
    private Integer attachmentSourceId;
    @SerializedName("SequenceNo")
    private Integer sequenceNo;
    @SerializedName("UnitTaskId")
    private Integer unitTaskId;
    @SerializedName("UnitId")
    private Integer unitId;
    @SerializedName("AttachmentSourceCode")
    private String attachmentSourceCode;

    public String getAttachmentSourceCode() {
        return attachmentSourceCode;
    }

    public void setAttachmentSourceCode(String attachmentSourceCode) {
        this.attachmentSourceCode = attachmentSourceCode;
    }

    /**
     * @return The attachmentTypeId
     */
    public Integer getAttachmentTypeId() {
        return attachmentTypeId;
    }

    /**
     * @param attachmentTypeId The AttachmentTypeId
     */
    public void setAttachmentTypeId(Integer attachmentTypeId) {
        this.attachmentTypeId = attachmentTypeId;
    }

    /**
     * @return The attachmentTitle
     */
    public String getAttachmentTitle() {
        return attachmentTitle;
    }

    /**
     * @param attachmentTitle The AttachmentTitle
     */
    public void setAttachmentTitle(String attachmentTitle) {
        this.attachmentTitle = attachmentTitle;
    }

    /**
     * @return The attachmentExtension
     */
    public String getAttachmentExtension() {
        return attachmentExtension;
    }

    /**
     * @param attachmentExtension The AttachmentExtension
     */
    public void setAttachmentExtension(String attachmentExtension) {
        this.attachmentExtension = attachmentExtension;
    }

    /**
     * @return The sizeInKB
     */
    public Integer getSizeInKB() {
        return sizeInKB;
    }

    /**
     * @param sizeInKB The SizeInKB
     */
    public void setSizeInKB(Integer sizeInKB) {
        this.sizeInKB = sizeInKB;
    }

    /**
     * @return The attachmentSourceTypeId
     */
    public Integer getAttachmentSourceTypeId() {
        return attachmentSourceTypeId;
    }

    /**
     * @param attachmentSourceTypeId The AttachmentSourceTypeId
     */
    public void setAttachmentSourceTypeId(Integer attachmentSourceTypeId) {
        this.attachmentSourceTypeId = attachmentSourceTypeId;
    }

    /**
     * @return The attachmentSourceId
     */
    public Integer getAttachmentSourceId() {
        return attachmentSourceId;
    }

    /**
     * @param attachmentSourceId The AttachmentSourceId
     */
    public void setAttachmentSourceId(Integer attachmentSourceId) {
        this.attachmentSourceId = attachmentSourceId;
    }

    /**
     * @return The sequenceNo
     */
    public Integer getSequenceNo() {
        return sequenceNo;
    }

    /**
     * @param sequenceNo The SequenceNo
     */
    public void setSequenceNo(Integer sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * @return The unitTaskId
     */
    public Integer getUnitTaskId() {
        return unitTaskId;
    }

    /**
     * @param unitTaskId The UnitTaskId
     */
    public void setUnitTaskId(Integer unitTaskId) {
        this.unitTaskId = unitTaskId;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    @Override
    public String toString() {
        return "MetaDataIMO{" +
                "attachmentTypeId=" + attachmentTypeId +
                ", attachmentTitle='" + attachmentTitle + '\'' +
                ", attachmentExtension='" + attachmentExtension + '\'' +
                ", sizeInKB=" + sizeInKB +
                ", attachmentSourceTypeId=" + attachmentSourceTypeId +
                ", attachmentSourceId=" + attachmentSourceId +
                ", sequenceNo=" + sequenceNo +
                ", unitTaskId=" + unitTaskId +
                ", unitId=" + unitId +
                ", attachmentSourceCode='" + attachmentSourceCode + '\'' +
                '}';
    }
}

