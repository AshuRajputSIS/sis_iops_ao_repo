package com.sisindia.ai.android.mybarrack;

/**
 * Created by compass on 6/12/2017.
 */

public interface BarrackSyncListener {
    void barrackSyncListener(boolean isSynced);
}
