package com.sisindia.ai.android.fcmnotification.fcmIssues;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.FcmIssuesData;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 02-12-2016.
 */

public class FCMIssueStausUpdate extends SISAITrackingDB {
    private boolean isUpdatedSuccessfully;
    private SQLiteDatabase sqlite=null;

    public FCMIssueStausUpdate(Context context) {
        super(context);
    }

    public synchronized boolean insertIssuesModel(FcmIssuesData fcmIssuesData,boolean isIssueStatus) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if(fcmIssuesData != null) {
                    sqlite.beginTransaction();
                    if(isIssueStatus) {
                        updateFCMIssuesStatus(fcmIssuesData.getId(),fcmIssuesData.getStatus(),sqlite);
                        if(fcmIssuesData.getStatus() == 3) {
                            IOPSApplication.getInstance().getIssueInsertionDBInstance()
                                    .updateImprovementPlanStatus(fcmIssuesData.getId(), 2,
                                             fcmIssuesData.getClosureDateTime(),sqlite);
                        }
                    }
                    else {
                        updateFCMImprovementPlanStatus(fcmIssuesData.getId(), fcmIssuesData.getStatus(),sqlite);
                    }
                    sqlite.setTransactionSuccessful();
                    isUpdatedSuccessfully = true;
                }else {
                    isUpdatedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isUpdatedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isUpdatedSuccessfully;
        }
    }

   /* public  void updateIssuesStatus(int id,IssueClosureMo issueClosureMo){



    }*/

    private void updateFCMIssuesStatus(int issueid, int statusId, SQLiteDatabase sqlite) {

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ISSUES_TABLE+
                " SET " + TableNameAndColumnStatement.ISSUE_STATUS_ID+ " = " + statusId +
                " WHERE " + TableNameAndColumnStatement.ISSUE_ID+ " = " + issueid;

        Timber.d("FCM-UpdateIssueTable  %s", updateQuery);
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("FCM-updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        }
    }

    private void updateFCMImprovementPlanStatus(int improvemntPlanId, int statusId, SQLiteDatabase sqlite) {
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE+
                " SET " + TableNameAndColumnStatement.STATUS_ID+ " = " + statusId +
                " WHERE " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID+ " = " + improvemntPlanId;

        Timber.d("FCM-UpdateImprovementTable  %s", updateQuery);
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("FCM-UpdateImprovementQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        }
    }
}
