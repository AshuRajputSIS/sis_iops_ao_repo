package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NotificationCompat;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.DependentSyncDb;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */

public  class CommonSyncDbOperation   {
    private Context mContext;
    private NotificationCompat.InboxStyle inboxStyle;
    private AppPreferences appPreferences;
    public DependentSyncDb dependentSyncDb;

    public   static final int UNIT_STRENGTH_SYNCED = 1;
    public   static final int ISSUES_SYNCED = 2;
    public   static final int KIT_REQUEST_SYNCED = 3;
    public   static final int IMPROVEMNT_PLAN_SYNCED = 4;
    public   static final  int MEAT_DATA_SYNCED = 5;
    public   static final  int IS_FILE_SYNCED = 6;

    public CommonSyncDbOperation(Context mcontext) {
        this.mContext = mcontext;
        appPreferences = new AppPreferences(mcontext);
        dependentSyncDb = new DependentSyncDb(mcontext);

    }

    /**
     *
     * @param Tablename
     * @param taskId
     * @param attachmentReferenceId
     * @param sequenceId
     * @param attachmentRefreenceType
     */


    public  void updatePostidMetadataTable(String Tablename, int taskId, int attachmentReferenceId
            , int sequenceId, int attachmentRefreenceType) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = '" + attachmentReferenceId + "' , " +
                TableNameAndColumnStatement.TASK_ID + " = '" + taskId + "'" +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + sequenceId + "' AND "+
                TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE + "= '" + attachmentRefreenceType +"'"
                ;
        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }


    public  void updateUnitRaisingMetaData(String Tablename, int attachmentReferenceId,int localAttachmentReferenceId
            , int attachmentRefreenceType) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + attachmentReferenceId +
                " WHERE "+ TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE + " = " + attachmentRefreenceType
                +" and "+TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = "+localAttachmentReferenceId;

        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }
    /**
     *
     * @param Tablename
     * @param attach_id
     * @param id
     */

    protected void UpdateAttachmentIdMetadataTable(String Tablename, int attach_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_ID + "= '" + attach_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }

    /**
     * onDutyChange Issueid in Grievance Table and Complaint Table
     * @param Tablename
     * @param issueid
     * @param id
     */
    protected void updateIssueIdTable(String Tablename, int issueid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ISSUE_ID + "= '" + issueid +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }



    public  void updateidMetadataTable(String Tablename, int attachmentReferenceId, int sequenceId, int attachmentRefreenceType) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + attachmentReferenceId  +"' "+

                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + sequenceId + "' "
                ;
        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }


    /*@Override
    public void successfulSyncUpdate(int staskId, int whichSync, int syncCount, String col) {
        updateSyncStatus(staskId,whichSync,syncCount,col);
    }



    public void  updateSyncStatus(int taskId, int whichSync, int syncCount, String columnName){
                        switch (whichSync){
                            case CommonSyncDbOperation.UNIT_STRENGTH_SYNCED :
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED,taskId,syncCount,columnName);
                                break;
                            case CommonSyncDbOperation.ISSUES_SYNCED:
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_ISSUES_SYNCED,taskId,syncCount,columnName);
                                break;
                            case CommonSyncDbOperation.KIT_REQUEST_SYNCED :
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED,taskId,syncCount,columnName);
                                break;
                            case CommonSyncDbOperation.IMPROVEMNT_PLAN_SYNCED:
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED,taskId,syncCount,columnName);
                                break;
                            case CommonSyncDbOperation.MEAT_DATA_SYNCED :
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_METADATA_SYNCED,taskId,syncCount,columnName);
                                break;
                            case CommonSyncDbOperation.IS_FILE_SYNCED :
                                dependentSyncDb.updateDependentTaskSyncDetails(
                                        TableNameAndColumnStatement.IS_IMAGES_SYNCED,taskId,syncCount,columnName);
                                break;
                        }

    }*/


}
