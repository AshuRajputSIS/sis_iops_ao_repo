package com.sisindia.ai.android.rota.models;

import java.io.Serializable;

/**
 * Created by nischala on 20/5/16.
 */
public class RotaTaskListMO implements Serializable {

    private String RotaTaskName;
    private int RotaTaskActivityId;
    private int RotaTaskBlackIcon;
    private int RotaTaskWhiteIcon;
    private String unitName;
    private Integer unitId;



    public int getRotaTaskId() {
        return rotaTaskId;
    }

    public void setRotaTaskId(int rotaTaskId) {
        this.rotaTaskId = rotaTaskId;
    }

    private int rotaTaskId;

    public int getRotaTaskActivityId() {
        return RotaTaskActivityId;
    }

    public void setRotaTaskActivityId(int rotaTaskActivityId) {
        RotaTaskActivityId = rotaTaskActivityId;
    }


    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }


    public int getRotaTaskWhiteIcon() {
        return RotaTaskWhiteIcon;
    }

    public void setRotaTaskWhiteIcon(int rotaTaskWhiteIcon) {
        RotaTaskWhiteIcon = rotaTaskWhiteIcon;
    }

    public String getRotaTaskName() {
        return RotaTaskName;
    }

    public void setRotaTaskName(String rotaTaskName) {
        RotaTaskName = rotaTaskName;
    }

    public int getRotaTaskBlackIcon() {
        return RotaTaskBlackIcon;
    }

    public void setRotaTaskBlackIcon(int rotaTaskBlackIcon) {
        RotaTaskBlackIcon = rotaTaskBlackIcon;
    }
}
