package com.sisindia.ai.android.database.unitatriskpoaDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.IssueMatrixModel;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 19/7/16.
 */

public class UARUpdateStatementDB extends SISAITrackingDB {
    public UARUpdateStatementDB(Context context) {
        super(context);
    }
    public void updateUnitRiskPOA(int unit_risk_id, int unit_risk_action_plan_id,
                                  int id) {


        // Select All Query
        String updateQuery = "update " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE+" set " +
                TableNameAndColumnStatement.IS_SYNCED+"='1'," +
                TableNameAndColumnStatement.IS_COMPLETED+"='1' " +
                " where " +
                TableNameAndColumnStatement.UNIT_RISK_ID+"='" + unit_risk_id + "' " +
                " and " +
                TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID+"='" + unit_risk_action_plan_id + "' " +
                " and " +
                TableNameAndColumnStatement.ID+"='" + id + "'";

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }


    }


    public void updateUnitRiskCompletionDate(String closedDate,int unit_risk_id
    ) {


        // Select All Query
        String updateQuery = "update " + TableNameAndColumnStatement.UNIT_RISK_TABLE+" set " +
                TableNameAndColumnStatement.CLOSED_AT+"='"+closedDate+"' ," +
                TableNameAndColumnStatement.UNIT_RISK_STATUS_ID+ " = "+ 2 +
                " where " +
                TableNameAndColumnStatement.UNIT_RISK_ID+"='" + unit_risk_id + "'";


        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }


    }
    public void updateUnitRiskPOAStatus(String remarks, String closeDate,
                                        int unit_risk_id, int unit_risk_action_plan_id,
                                        int id) {
        // Select All Query
        String updateQuery = "update " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE+" set " +
                TableNameAndColumnStatement.REMARKS+"='" + remarks + "', " +
                TableNameAndColumnStatement.CLOSED_DATE+"='" + closeDate + "'," +
                TableNameAndColumnStatement.COMPLETION_DATE+"='" + closeDate + "'," +
                TableNameAndColumnStatement.IS_COMPLETED+"='" + 1 + "' " +
                " where " +
                TableNameAndColumnStatement.UNIT_RISK_ID+"='" + unit_risk_id + "' " +
                " and " +
                TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID+"='" + unit_risk_action_plan_id + "' " +
                " and " +
                TableNameAndColumnStatement.ID+"='" + id + "'";

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updateQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }


    }



}
