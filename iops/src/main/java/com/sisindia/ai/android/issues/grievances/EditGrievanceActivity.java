package com.sisindia.ai.android.issues.grievances;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.IssueClosureMo;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditGrievanceActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.lastInspectionUnitName)
    CustomFontTextview lastInspectionUnitName;
    @Bind(R.id.guradName_or_clientName)
    CustomFontTextview guradNameOrClientName;
    @Bind(R.id.guard_Id)
    CustomFontTextview guardId;
    @Bind(R.id.nature_of_grievance)
    CustomFontTextview natureOfGrievance;
    @Bind(R.id.issueTargetClosureDate)
    CustomFontTextview issueTargetClosureDate;
    @Bind(R.id.timeDurationLeft)
    RelativeTimeTextView timeDurationLeft;
    @Bind(R.id.assigendLabel)
    CustomFontTextview assigendLabel;
    @Bind(R.id.edit_grievance_parent_layout)
    RelativeLayout editGrievanceParentLayout;
   /* @Bind(R.id.spinner_assignedTo)
    Spinner spinnerAssignedTo;*/

    @Bind(R.id.statusLabel)
    CustomFontTextview statusLabel;
    @Bind(R.id.spinner_status)
    Spinner spinnerStatus;
    @Bind(R.id.button_save)
    CustomFontButton buttonSave;
    @Bind(R.id.button_cancel)
    CustomFontButton buttonCancel;
    private LookUpSpinnerAdapter statusAdapter,assignedToAdapter;
    @Bind(R.id.add_grievance_remarks)
    CustomFontEditText addGrievanceRemarks;
    @Bind(R.id.assignedToTv)
    CustomFontTextview assignedToTv;
    @Bind(R.id.buttons_layout)
    LinearLayout buttonsLayout;
    private ArrayList<LookupModel> issueStatus;
    private IssuesMO grievanceData;
    private IssueClosureMo issueClosureMo;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private Resources resources;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_grievances);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.EDIT_GRIEVANCE));
        resources= getResources();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        setUpViews();


    }

    private void setUpViews() {

        statusAdapter = new LookUpSpinnerAdapter(this);
        assignedToAdapter = new LookUpSpinnerAdapter(this);

        issueStatus = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.IssueStatus), null);
        statusAdapter.setSpinnerData(issueStatus);
        spinnerStatus.setAdapter(statusAdapter);
        issueClosureMo = new IssueClosureMo();
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra(getResources().getString(R.string.grievanceDataObj))) {
            grievanceData = intent.getParcelableExtra(getResources().getString(R.string.grievanceDataObj));
        }
        spinnerStatus.setSelection(Util.getIndex(grievanceData.getStatus(),spinnerStatus,issueStatus));
        setUpViewVisibility();
        if (grievanceData != null) {
            lastInspectionUnitName.setText(grievanceData.getUnitName());
            guradNameOrClientName.setText(grievanceData.getGuardName());
            issueTargetClosureDate.setText(grievanceData.getTargetActionEndDate());
            long millis = dateTimeFormatConversionUtil.convertStringToDate(grievanceData.getCreatedDateTime()).getTime();
            timeDurationLeft.setReferenceTime(millis);
            guardId.setText(grievanceData.getGuardCode());
            natureOfGrievance.setText(String.valueOf(grievanceData.getNatureOfGrievance()));
            assignedToTv.setText(String.valueOf(grievanceData.getAssignedToName()));

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.add_post_save){
            validateFields();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void updateIssueTable() {
        issueClosureMo.setClosureEndDate(dateTimeFormatConversionUtil.getCurrentDateTime());
        IOPSApplication.getInstance().getIssueUpdateStatementDB()
                .updateIssuesStatus(grievanceData.getpId(), issueClosureMo);
        if(issueClosureMo.getStatusId() == 3) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance()
                    .updateImprovementPlanStatus(grievanceData.getIssueId(), 2,null, null);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String issueStatusString = issueStatus.get(position).getName();
        issueClosureMo.setStatusName(issueStatusString);
        issueClosureMo.setStatusId(issueStatus.get(position).getLookupIdentifier());
        if (issueStatus.get(position).getLookupIdentifier() == 3 ) {
            addGrievanceRemarks.setVisibility(View.VISIBLE);
            buttonsLayout.setVisibility(View.VISIBLE);
            addGrievanceRemarks.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    issueClosureMo.setClosureRemarks(s.toString());
                }
            });
        } else {
            addGrievanceRemarks.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        MenuItem saveMenu = menu.findItem(R.id.add_post_save);
        if (appPreferences.getAppUserId() == grievanceData.getAssignedToId()) {
            if (resources.getString(R.string.Resolved).equalsIgnoreCase(grievanceData.getStatus())){
                saveMenu.setVisible(false);
            }
            else {
                saveMenu.setVisible(true);
            }
        } else {
            saveMenu.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.button_save:
                validateFields();
                break;
            case R.id.button_cancel:
                finish();
                break;
        }
    }

    private void setUpViewVisibility(){
        if(grievanceData.getAssignedToId()!=0) {
            if (appPreferences.getAppUserId() == grievanceData.getAssignedToId()) {
                if (resources.getString(R.string.Resolved).equalsIgnoreCase(grievanceData.getStatus())) {
                    spinnerStatus.setEnabled(false);
                    spinnerStatus.setBackgroundColor(getResources().getColor(R.color.white));
                    addGrievanceRemarks.setVisibility(View.GONE);
                    buttonsLayout.setVisibility(View.GONE);
                } else {
                    spinnerStatus.setEnabled(true);
                    spinnerStatus.setOnItemSelectedListener(this);
                }
            } else {
                spinnerStatus.setEnabled(false);
                spinnerStatus.setBackgroundColor(getResources().getColor(R.color.white));
                buttonsLayout.setVisibility(View.GONE);
                addGrievanceRemarks.setVisibility(View.GONE);
            }
        }
        else{
            spinnerStatus.setEnabled(false);
            spinnerStatus.setBackgroundColor(getResources().getColor(R.color.white));
            addGrievanceRemarks.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.GONE);
            snackBarWithMesg(buttonsLayout,getResources().getString(R.string.data_not_synced));
        }
    }

    private void validateFields(){
        if (issueClosureMo != null && issueClosureMo.getStatusId()!=0){
            if (resources.getString(R.string.Resolved).equalsIgnoreCase(issueClosureMo.getStatusName())) {
                    updateIssueTable();
                    triggerSyncAdapter();
                    appPreferences.setIssueType(Constants.NavigationFlags.TYPE_GRIEVANCE);
                    IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_greivance));
                    finish();
            }
            else{
                updateIssueTable();
                appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
                IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_complaints));
                finish();
            }
        }
    }
    private void triggerSyncAdapter(){
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }
}
