package com.sisindia.ai.android.rota;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myperformance.TimeLineViewAdapter;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 16-09-2016.
 */
public class ConveyanceReportActivity extends BaseActivity {

    @Bind(R.id.conveyance_recyclerview)
    RecyclerView conveyanceRecyclerview;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
//    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conveyance_report);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.conveyanceReport));
//        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        //insertActivityLog();
        generateTimeLineView();
    }

    /*private void insertActivityLog() {
        TimeLineModel timeLineModel = new TimeLineModel();
        timeLineModel.setActivity("Duty Off");
        timeLineModel.setLocation("");
        timeLineModel.setTaskId("");
        timeLineModel.setStatus("1");
        timeLineModel.setTaskName("");
        timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
        timeLineModel.setEndTime("");
        timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());
        IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
    }*/


    private void generateTimeLineView() {

        conveyanceRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getActivityLogDB().getActivityLogs();

//        List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getMyPerformanceStatementsDB().getActivityTimelineByDuty("","");
        getBackStatck();

        if (timeLineModel != null) {
            conveyanceRecyclerview.setAdapter(new TimeLineViewAdapter(timeLineModel, this));
        }
    }

    private void getBackStatck() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getRunningTasks(10);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (IOPSApplication.getInstance().getActivityLogDB().getActivityLogsCount() > 0) {
            IOPSApplication.getInstance().getActivityLogDB().deleteActivityLogs();
        }
        super.onBackPressed();
    }
}
