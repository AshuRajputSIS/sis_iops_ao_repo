package com.sisindia.ai.android.myunits;

/**
 * Created by Saruchi on 21-06-2016.
 * Client handshake slect contact MO
 */

public class ContactMO {
    private String name;
    private String designation;
    private String contactNo;
    private int unitcontactID;

    public int getUnitcontactID() {
        return unitcontactID;
    }

    public void setUnitcontactID(int unitcontactID) {
        this.unitcontactID = unitcontactID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Override
    public String toString() {
        return "ContactMO{" +
                "name='" + name + '\'' +
                ", designation='" + designation + '\'' +
                ", contactNo='" + contactNo + '\'' +
                ", unitcontactID='" + unitcontactID + '\'' +
                '}';
    }
}
