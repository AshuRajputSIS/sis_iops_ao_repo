package com.sisindia.ai.android.annualkitreplacement;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.issues.improvements.ImprovementPlanName;
import com.sisindia.ai.android.views.CustomFontTextview;

/**
 * Created by Shushrut on 20-04-2016.
 */
public class KitReplacementAdapter extends RecyclerView.Adapter<KitReplacementAdapter.ImprovementDetailsHolder> implements View.OnClickListener {
    Context mContext;
    String[] mDataItems;

    public KitReplacementAdapter(Context mContext, String[] mDataItems) {
        this.mDataItems = mDataItems;
        this.mContext = mContext;

    }


    @Override
    public ImprovementDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.kit_row_item, parent, false);
        ImprovementDetailsHolder mHolder = new ImprovementDetailsHolder(view);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(ImprovementDetailsHolder holder, int position) {

        holder.getMcheckType().setText(mDataItems[position]);
        holder.getMcheckType().setOnClickListener(this);

    }


    @Override
    public int getItemCount() {
        return mDataItems.length;
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(mContext, ImprovementPlanName.class);
        i.putExtra(mContext.getResources().getString(R.string.isfromunitRisk), false);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }

    public class ImprovementDetailsHolder extends RecyclerView.ViewHolder {
        ImageView mImageview;
        CustomFontTextview mcheckType;


        public ImprovementDetailsHolder(View itemView) {
            super(itemView);

            mcheckType = (CustomFontTextview) itemView.findViewById(R.id.kit_item_name);
        }


        public CustomFontTextview getMcheckType() {
            return mcheckType;
        }

        public void setMcheckType(CustomFontTextview mcheckType) {
            this.mcheckType = mcheckType;
        }
    }
}
