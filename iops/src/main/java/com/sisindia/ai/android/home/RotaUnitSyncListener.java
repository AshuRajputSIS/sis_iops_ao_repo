package com.sisindia.ai.android.home;

/**
 * Created by Saruchi on 08-06-2016.
 */
public interface RotaUnitSyncListener {
    void rotaUnitDbSyncing(boolean isSyned);
}
