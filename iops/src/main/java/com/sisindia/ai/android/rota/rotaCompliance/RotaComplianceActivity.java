package com.sisindia.ai.android.rota.rotaCompliance;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.net.HttpURLConnection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class RotaComplianceActivity extends BaseActivity {

    @Bind(R.id.deviationsTv)
    CustomFontTextview deviationsTv;

    @Bind(R.id.requestedCountTv)
    CustomFontTextview requestedCountTv;

    @Bind(R.id.approvedCountTv)
    CustomFontTextview approvedCountTv;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.rotaComplianceRecyclerview)
    RecyclerView rotaComplianceRecyclerview;

    private List<RotaComplianceTaskDetails> rotaComplianceTaskDetailsList;
    private Resources resources;
    private RotaComplianceOR rotaCompliance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rota_compliance_activity);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.rota_compliance));
        resources = getResources();
        Intent intent = getIntent();
        if (intent != null) {
            String selectedRotaDate = intent.getStringExtra(getResources().getString(R.string.rota_selected_date));
            if (!TextUtils.isEmpty(selectedRotaDate)) {
                getRotaCompliance(selectedRotaDate);
            }
        }
    }

    void setRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rotaComplianceRecyclerview.setLayoutManager(linearLayoutManager);
        rotaComplianceRecyclerview.setItemAnimator(new DefaultItemAnimator());
        RotaComplianceAdapter rotaComplianceAdapter = new RotaComplianceAdapter(this);


        if (rotaComplianceTaskDetailsList != null && rotaComplianceTaskDetailsList.size() > 0) {

            int requestedAdhocCount = rotaCompliance.rotaComplianceData.AdhocTaskCount;
            int appRoveAdhocCount = rotaCompliance.rotaComplianceData.AdhocTaskApprovedCount;

            requestedCountTv.setText(String.valueOf(requestedAdhocCount));
            approvedCountTv.setText(String.valueOf(appRoveAdhocCount));

            rotaComplianceAdapter.setRotaComplianceList(rotaComplianceTaskDetailsList);
            rotaComplianceRecyclerview.setAdapter(rotaComplianceAdapter);
        }
    }


    void getRotaCompliance(String selectedDate) {
        Util.showProgressBar(RotaComplianceActivity.this, R.string.loading_please_wait);
        sisClient.getApi().getRotaCompliance(selectedDate, new Callback<RotaComplianceOR>() {
            @Override
            public void success(RotaComplianceOR rotaComplianceOR, Response response) {
                switch (rotaComplianceOR.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        Util.dismissProgressBar(isDestroyed());
                        rotaCompliance = rotaComplianceOR;
                        if (rotaCompliance != null && rotaCompliance.rotaComplianceData != null) {
                            rotaComplianceTaskDetailsList = rotaCompliance.rotaComplianceData.rotaComplianceModelItemList;
                        }
                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        Toast.makeText(RotaComplianceActivity.this, resources.getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(RotaComplianceActivity.this, resources.getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                        break;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRecyclerView();
                    }
                });
                Util.dismissProgressBar(isDestroyed());
            }

            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);
                Util.dismissProgressBar(isDestroyed());
                setRecyclerView();
            }
        });
    }

    public void ReportView(View view) {
        Intent intent = new Intent(this, RoccReportViewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rota_compliance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
