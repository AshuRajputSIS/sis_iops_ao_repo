package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/10/16.
 */

public class RecruitmentOR {
    @SerializedName("RecruitmentId")
    @Expose
    private Integer recruitmentId;

    /**
     *
     * @return
     * The recruitmentId
     */
    public Integer getRecruitmentId() {
        return recruitmentId;
    }

    /**
     *
     * @param recruitmentId
     * The RecruitmentId
     */
    public void setRecruitmentId(Integer recruitmentId) {
        this.recruitmentId = recruitmentId;
    }

    @Override
    public String toString() {
        return "RecruitmentOR{" +
                "recruitmentId=" + recruitmentId +
                '}';
    }
}
