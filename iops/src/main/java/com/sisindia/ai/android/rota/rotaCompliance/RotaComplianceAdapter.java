package com.sisindia.ai.android.rota.rotaCompliance;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 05-07-2016.
 */
public class RotaComplianceAdapter extends RecyclerView.Adapter<RotaComplianceAdapter.RotaComplianceViewHolder> {

    private Context mContext;
    private List<RotaComplianceTaskDetails> rotaComplianceList = new ArrayList<>();


    public RotaComplianceAdapter(Context dayCheckNavigationActivity) {
        mContext = dayCheckNavigationActivity;


    }

    public void setRotaComplianceList(List<RotaComplianceTaskDetails> rotaComplianceList) {
        this.rotaComplianceList = rotaComplianceList;
    }

    @Override
    public RotaComplianceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rota_compliance_row, parent, false);
        RotaComplianceViewHolder mDayCheckViewholder = new RotaComplianceViewHolder(view);
        return mDayCheckViewholder;
    }

    @Override
    public void onBindViewHolder(RotaComplianceViewHolder holder, int position) {

        RotaComplianceViewHolder rotaComplianceViewHolder = holder;
        float percentage = 0.00f;


        if (rotaComplianceList.get(position).MonthlyRotaTarget != 0) {
            percentage = (Float.valueOf(rotaComplianceList.get(position)
                    .currentMonthCompletedTasksCount) /
                    Float.valueOf(rotaComplianceList.get(position).MonthlyRotaTarget));

            rotaComplianceViewHolder.progressPercent.setText(rotaComplianceList.get(position).currentMonthCompletedTasksCount.toString());
            rotaComplianceViewHolder.totalPercentage.setText(rotaComplianceList.get(position).MonthlyRotaTarget.toString());
            rotaComplianceViewHolder.progressPercentText.setText(String.valueOf((int) (percentage * 100)) + "%");
            rotaComplianceViewHolder.checkingType.setText(rotaComplianceList.get(position).taskType);


            ViewGroup.LayoutParams targetProgressPercentage = rotaComplianceViewHolder
                    .targetProgressPercentage.getLayoutParams();
            int width = targetProgressPercentage.width;
            int actualPercentage = (int) (percentage * 100);
            //int actualPercentageLength = (Util.pxToDp(width)*actualPercentage)/100;
            int actualPercentageLength = (width * actualPercentage) / 100;

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT));

            layoutParams.width = actualPercentageLength;
            layoutParams.height = Util.dpToPx(20);

            rotaComplianceViewHolder.actualProgressPercentage.setLayoutParams(layoutParams);
            if (actualPercentage >= 90) {
                rotaComplianceViewHolder.totalPercentage.setVisibility(View.GONE);
                rotaComplianceViewHolder.progressPercent.setTextColor(Color.parseColor("#000000"));
            }
            if(actualPercentage == 0){
                rotaComplianceViewHolder.progressPercentageCount.setVisibility(View.VISIBLE);
                rotaComplianceViewHolder.progressPercentageCount.setText(
                        rotaComplianceList.get(position).currentMonthCompletedTasksCount.toString());
            }
            else {
                rotaComplianceViewHolder.totalPercentage.setVisibility(View.VISIBLE);
            }
        } else {
            rotaComplianceViewHolder.rowLinearLayout.setVisibility(ViewPager.GONE);
            //rotaComplianceViewHolder.progressPercentText.setText(String.valueOf((int)(percentage*100)) + "%");
            //rotaComplianceViewHolder.checkingType.setText(rotaComplianceList.get(position).getTaskType());
        }


    }


    @Override
    public int getItemCount() {
        int count = rotaComplianceList == null ? 0 : rotaComplianceList.size();
        return count;
    }


    public class RotaComplianceViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progress_percent_text)
        CustomFontTextview progressPercentText;
        @Bind(R.id.target_progress_percentage)
        View targetProgressPercentage;
        @Bind(R.id.actual_progress_percentage)
        View actualProgressPercentage;
        @Bind(R.id.progress_percent)
        TextView progressPercent;
        @Bind(R.id.totalPercentage)
        TextView totalPercentage;
        @Bind(R.id.checking_type)
        CustomFontTextview checkingType;
        @Bind(R.id.mainLayout)
        LinearLayout rowLinearLayout;
        @Bind(R.id.progressPercentageCount)
        TextView progressPercentageCount;



        public RotaComplianceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
