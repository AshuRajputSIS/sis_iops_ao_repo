package com.sisindia.ai.android.issues;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.DeletionStatementDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.IssueIMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;


public class IssuesUpdateAsyncTask extends AsyncTask<String, String, String> {

    private SISClient sisClient;
    private Context mContext;
    private IssuesSyncListener issuesSyncListener;
    private IssuesAndImprovementPlan issuesAndImprovementPlan;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    public IssuesUpdateAsyncTask(Context context, IssuesAndImprovementPlan issuesAndImprovementPlan, IssuesSyncListener issuesSyncListener) {

        this.mContext = context;
        this.issuesSyncListener = issuesSyncListener;
        sisClient = new SISClient(mContext);
        this.issuesAndImprovementPlan = issuesAndImprovementPlan;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        issuesSyncListener.issuesSyncing(true);
        // showprogressbar(R.string.loadConfigureText);

    }

    @Override
    protected String doInBackground(String... params) {
        // insertLoadConfigurationData(loadConfigurationBaseMO);
        if (issuesAndImprovementPlan != null) {
            SQLiteDatabase sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            sqlite.beginTransaction();
            boolean isDeletionSucess = issuesTableDeletion(mContext, sqlite);
            if (isDeletionSucess) {
                boolean isInsertSuccess = insertUpdateIssuesAndImprovementTable(sqlite);

                if (isInsertSuccess) {
                    sqlite.setTransactionSuccessful();
                    sqlite.endTransaction();
                    closeDb(sqlite);
                } else {
                    sqlite.endTransaction();
                    closeDb(sqlite);
                    Timber.e("Error While Inserting Issues and improvement tables");
                }
            } else {
                sqlite.endTransaction();
                closeDb(sqlite);
                Timber.e("Error While Deleting Issues and Improvement plan tables");
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        hideprogressbar(true);
    }


    private void hideprogressbar(boolean isCompleted) {
        Util.dismissProgressBar(isCompleted);
    }


    private boolean insertUpdateIssuesAndImprovementTable(SQLiteDatabase sqlite) {
        boolean isSuccessful = true;
        try {
                addIssueDetails(issuesAndImprovementPlan.issueList, sqlite);
                addImprovementPlanDetails(issuesAndImprovementPlan.improvementList, sqlite);
        } catch (Exception exception) {
            isSuccessful = false;
            Crashlytics.logException(exception);
        }
        return isSuccessful;
    }

    public void addIssueDetails(List<IssueIMO> issuesMOList, SQLiteDatabase sqlite) {

        synchronized (sqlite) {
            if (issuesMOList != null && issuesMOList.size() != 0) {

                for (IssueIMO issuesMO : issuesMOList) {


                    try {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.ISSUE_ID, issuesMO.getIssueId());
                        values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, issuesMO.getIssueMatrixId());
                        values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issuesMO.getIssueTypeId());
                        values.put(TableNameAndColumnStatement.SOURCE_TASK_ID, issuesMO.getSourceTaskId());
                        values.put(TableNameAndColumnStatement.UNIT_ID, issuesMO.getUnitId());
                        values.put(TableNameAndColumnStatement.BARRACK_ID, issuesMO.getBarrackId());
                        values.put(TableNameAndColumnStatement.GUARD_ID, issuesMO.getGuardId());
                        if(issuesMO.getIssueCategoryId() == null){
                            values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, 0);
                        }
                        else{
                            values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issuesMO.getIssueCategoryId());
                        }

                        values.put(TableNameAndColumnStatement.ISSUE_MODE_ID, 1);
                        values.put(TableNameAndColumnStatement.ISSUE_NATURE_ID, issuesMO.getIssueNatureId());
                        values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issuesMO.getIssueCauseId());
                        values.put(TableNameAndColumnStatement.ISSUE_SERVERITY_ID, issuesMO.getIssueSeverityId());
                        values.put(TableNameAndColumnStatement.ISSUE_STATUS_ID, issuesMO.getIssueStatus());
                        values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, issuesMO.getRaisedDateTime());
                        values.put(TableNameAndColumnStatement.CREATED_BY_ID, issuesMO.getRaisedById());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, issuesMO.getAssignedToId());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME, issuesMO.getAssigneeName());
                        String date = dateTimeFormatConversionUtil.convertDateTimeToDate(issuesMO.getTargetActionDateTime());
                        values.put(TableNameAndColumnStatement.TARGET_ACTION_END_DATE, date);
                        values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, issuesMO.getIssueActionId());
                        values.put(TableNameAndColumnStatement.CLOSURE_END_DATE, issuesMO.getClosureDateTime());
                        values.put(TableNameAndColumnStatement.REMARKS, issuesMO.getClientRemarks());
                        values.put(TableNameAndColumnStatement.CLOSURE_REMARKS, issuesMO.getClientRemarks());
                        //values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,issuesMO.ge);
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME, issuesMO.getAssigneeName());
                        values.put(TableNameAndColumnStatement.UNIT_CONTACT_NAME, issuesMO.getClientContactName());
                        values.put(TableNameAndColumnStatement.GUARD_CODE, issuesMO.getGuardCode());
                        values.put(TableNameAndColumnStatement.PHONE_NO, issuesMO.getClientPhoneNumber());
                        values.put(TableNameAndColumnStatement.IS_NEW, 0);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                        sqlite.insertOrThrow(TableNameAndColumnStatement.ISSUES_TABLE, null, values);
                    } catch (Exception exception) {
                        Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                        Crashlytics.logException(exception);
                    }


                }
            }
        }
    }

    public void addImprovementPlanDetails(List<ActionPlanMO> improvementPlanList, SQLiteDatabase sqlite) {


        synchronized (sqlite) {
            if (improvementPlanList != null && improvementPlanList.size() > 0) {

                for (ActionPlanMO improvementPlanIRModel : improvementPlanList) {
                    if (improvementPlanIRModel != null) {
                        try {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, improvementPlanIRModel.getIssueMatrixId());
                            values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID, improvementPlanIRModel.getActionPlanId());
                            values.put(TableNameAndColumnStatement.UNIT_ID, improvementPlanIRModel.getUnitId());
                            values.put(TableNameAndColumnStatement.BARRACK_ID, improvementPlanIRModel.getBarrackId());
                            values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, improvementPlanIRModel.getRaisedDate());
                            values.put(TableNameAndColumnStatement.ISSUE_ID, improvementPlanIRModel.getIssueId());
                            values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanIRModel.getMasterActionPlanId());
                            values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE, improvementPlanIRModel.getExpectedClosureDate());
                            values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID, improvementPlanIRModel.getTargetEmployeeId());
                            values.put(TableNameAndColumnStatement.STATUS_ID, improvementPlanIRModel.getActionPlanStatusId());
                            values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID, improvementPlanIRModel.getImprovementPlanCategoryId());
                            values.put(TableNameAndColumnStatement.REMARKS, improvementPlanIRModel.getRemarks());
                            values.put(TableNameAndColumnStatement.BUDGET, improvementPlanIRModel.getBudget());
                            values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, improvementPlanIRModel.getClosureDate());
                            // values.put(TableNameAndColumnStatement.CLOSED_BY_ID,improvementPlanIRModel.get);
                            values.put(TableNameAndColumnStatement.CLOSURE_COMMENT, improvementPlanIRModel.getClosureComment());
                            values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME, improvementPlanIRModel.getAssigneeName());
                            values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, improvementPlanIRModel.getAssignedToId());
                            values.put(TableNameAndColumnStatement.TASK_ID, improvementPlanIRModel.getSourceTaskId());
                            values.put(TableNameAndColumnStatement.IS_NEW, 0);
                            values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                            sqlite.insertOrThrow(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, null, values);

                        } catch (Exception exception) {
                            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                            Crashlytics.logException(exception);

                        }
                    }
                }

            }
        }
    }


    private static synchronized boolean issuesTableDeletion(Context mContext, SQLiteDatabase sqlite) {
        ArrayList<String> issuesAndImprovementPlans = new ArrayList<>();
        issuesAndImprovementPlans.add(TableNameAndColumnStatement.ISSUES_TABLE);
        issuesAndImprovementPlans.add(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE);


        DeletionStatementDB deletionStatementDB = new DeletionStatementDB(mContext);
        boolean isDeletionSucess = true;

        for (String tableName : issuesAndImprovementPlans) {
            if (!deletionStatementDB.IsTableDeleted(tableName, sqlite)) {
                isDeletionSucess = false;
                break;
            }
        }

        return isDeletionSucess;
    }

    private static void closeDb(SQLiteDatabase sqlite) {
        if (null != sqlite && sqlite.isOpen()) {
            sqlite.close();
        }
    }
}
