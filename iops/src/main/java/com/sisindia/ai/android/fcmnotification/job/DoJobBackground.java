package com.sisindia.ai.android.fcmnotification.job;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.annualkitreplacement.BaseAkrSync;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.EventBusIntentReceiver;
import com.sisindia.ai.android.commons.GenericAPICall;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.network.MasterActionPlanBaseOR;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.debugToolDTO.QuerySelectStatementDB;
import com.sisindia.ai.android.debugingtool.QueryResultDb;
import com.sisindia.ai.android.fcmcollection.FCMBillCollectionInsertion;
import com.sisindia.ai.android.fcmnotification.Config;
import com.sisindia.ai.android.fcmnotification.FcmGuardModelInsertion;
import com.sisindia.ai.android.fcmnotification.GuardForBranchBase;
import com.sisindia.ai.android.fcmnotification.IssueMatrixBaseMO;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.fcmnotification.NotificationUtils;
import com.sisindia.ai.android.fcmnotification.fcmBillCheckList.FcmBaseBillCheckListMO;
import com.sisindia.ai.android.fcmnotification.fcmBillCheckList.FcmBillingModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave.FCMBaseHolidayMO;
import com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave.FCMBaseLeaveMO;
import com.sisindia.ai.android.fcmnotification.fcmLookup.FcmBaseLookupData;
import com.sisindia.ai.android.fcmnotification.fcmLookup.FcmLookUpModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmRecuirtment.FCMRecuirtmentInsertion;
import com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk.FCMBaseUnitAtRiskActionPlanMO;
import com.sisindia.ai.android.fcmnotification.fcmbarrack.FCMBarrackDeletion;
import com.sisindia.ai.android.fcmnotification.fcmbarrack.FCMBaseBarrackMO;
import com.sisindia.ai.android.fcmnotification.fcmbarrack.FCMBaseBarrackStrengthMO;
import com.sisindia.ai.android.fcmnotification.fcmrota.FCMRotaModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMBaseUnitBarrackMO;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMBaseUnitContactMO;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMBaseUnitStrengthMO;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitBarrackDeletion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitBarrackModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitDeletion;
import com.sisindia.ai.android.issues.BaseIssuesActionplanSync;
import com.sisindia.ai.android.issues.BaseIssuesStatusSync;
import com.sisindia.ai.android.network.UnitTypeRequestMO;
import com.sisindia.ai.android.network.response.BaseRotaSync;
import com.sisindia.ai.android.network.response.BaseUnitSync;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.BaseRecruitmentOR;
import com.sisindia.ai.android.syncadpter.CommonSyncDbOperation;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitAtRiskBaseMO;
import com.sisindia.ai.android.utils.Util;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLDecoder;

/**
 * Created by shankar on 11/11/16.
 */

public class DoJobBackground extends CommonSyncDbOperation {

    //    private HttpURLConnection connection;
    private AppPreferences appPreferences;
    private final Context context;
    private NotificationReceivingMO mBackGroundSyncMO = new NotificationReceivingMO();
    //    private String line;
//    private BaseRotaSync rotaMO;
//    private BaseUnitSync unitEntity;
    private GenericAPICall genericAPICall;
    private String responseData = "";
    private boolean isInsertedSuccessfully;
    private NotificationUtils mNotificationUtils;

    public DoJobBackground(Context mContext, NotificationReceivingMO backGroundSyncMO) {
        super(mContext);
        this.context = mContext;
        this.mBackGroundSyncMO = backGroundSyncMO;
        appPreferences = new AppPreferences(context);
        mNotificationUtils = new NotificationUtils(mContext);
        doBackGroundJob(mBackGroundSyncMO);
    }

    /**
     * @param mBackGroundSyncMO If CallBack URL is empty, we have to do delete operation based on the Id which ever,
     *                          receiving in the "deleteId" and table name will be based on the "Name" tag.
     */
    private synchronized void doBackGroundJob(NotificationReceivingMO mBackGroundSyncMO) {

        switch (mBackGroundSyncMO.getName()) {
            case Config.ROTA:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseRotaSync baseRotaSync = IOPSApplication.getGsonInstance().fromJson(responseData, BaseRotaSync.class);
                        if (null != baseRotaSync && baseRotaSync.getStatusCode() == HttpURLConnection.HTTP_OK && null != baseRotaSync.getData()) {

                            isInsertedSuccessfully = IOPSApplication.getInstance().getFcmRotaModelInsertionDBInstance().insertRotaModel(baseRotaSync.getData());
                            mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);

//                            EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.ROTA));
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }
                break;

            case Config.UNIT:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseUnitSync baseUnitSync = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseUnitSync.class);
                        if (null != baseUnitSync && baseUnitSync.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != baseUnitSync.getData() && baseUnitSync.getData().size() != 0) {
                            isInsertedSuccessfully = IOPSApplication.getInstance().getFcmUnitModelInsertion().insertionUnitModel(baseUnitSync.getData());
                            mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    // DELETE OPERATION
                    FCMUnitDeletion fcmUnitDeletion = new FCMUnitDeletion(IOPSApplication.getInstance());
                    boolean isRecordDeleted = fcmUnitDeletion.deleteUnitRecord(mBackGroundSyncMO.getDeleteId());
                    mNotificationUtils.notificationUpdate(isRecordDeleted, mBackGroundSyncMO);

                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.UNIT));
                break;

            case Config.BARRACK:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseBarrackMO fcmBaseBarrackMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FCMBaseBarrackMO.class);
                        if (null != fcmBaseBarrackMO && fcmBaseBarrackMO.getStatusCode() == 200) {
                            if (null != fcmBaseBarrackMO.getData() && fcmBaseBarrackMO.getData().size() != 0) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmBarrackModelInsertion().insertBarrack(fcmBaseBarrackMO.getData());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    // DELETE OPERATION
                    FCMBarrackDeletion fcmBarrackDeletion = new FCMBarrackDeletion(IOPSApplication.getInstance());
                    boolean isRecordDeleted = fcmBarrackDeletion.deleteBarrackRecord(mBackGroundSyncMO.getDeleteId());
                    mNotificationUtils.notificationUpdate(isRecordDeleted, mBackGroundSyncMO);
                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.BARRACK));
                break;

            case Config.UNIT_STRENGTH:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseUnitStrengthMO fcmBaseUnitStrengthMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FCMBaseUnitStrengthMO.class);
                        if (null != fcmBaseUnitStrengthMO && fcmBaseUnitStrengthMO.getStatusCode() == 200) {
                            if (null != fcmBaseUnitStrengthMO.getData()) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmUnitStrengthModelInsertion().
                                        insertUnitStrength(fcmBaseUnitStrengthMO.getData());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }
                break;

            case Config.BARRACK_STRENGTH:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseBarrackStrengthMO fcmBaseBarrackStrengthMO = IOPSApplication.getGsonInstance().
                                fromJson(responseData.toString(), FCMBaseBarrackStrengthMO.class);
                        if (null != fcmBaseBarrackStrengthMO && fcmBaseBarrackStrengthMO.getStatusCode() == 200) {
                            if (null != fcmBaseBarrackStrengthMO.getData()) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmBarrackStrengthInsertion().
                                        insertBarrackStrength(fcmBaseBarrackStrengthMO.getData());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                break;

            case Config.UNIT_AT_RISK:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        UnitAtRiskBaseMO unitAtRiskBaseMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), UnitAtRiskBaseMO.class);
                        if (null != unitAtRiskBaseMO && unitAtRiskBaseMO.getStatusCode() == 200) {
                            if (null != unitAtRiskBaseMO.getData() && null != unitAtRiskBaseMO.getData() && null != unitAtRiskBaseMO.getData().getUnitRiskCount()) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmUnitAtRiskInsertion().
                                        insertUnitAtRisk(unitAtRiskBaseMO.getData().getRiskDetailModels(), unitAtRiskBaseMO.getData().getUnitRiskCount());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.UNIT_AT_RISK));
                break;

            case Config.UNIT_RISK_ACTION_PLAN:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseUnitAtRiskActionPlanMO fcmBaseUnitAtRiskActionPlanMO = IOPSApplication.getGsonInstance().
                                fromJson(responseData.toString(), FCMBaseUnitAtRiskActionPlanMO.class);
                        if (null != fcmBaseUnitAtRiskActionPlanMO && fcmBaseUnitAtRiskActionPlanMO.getStatusCode() == 200) {
                            if (null != fcmBaseUnitAtRiskActionPlanMO.getData()) {
                                /**
                                 * Write update query for action plan. Currently created for insertion only if in future they want we can use.
                                 */
                                /*isInsertedSuccessfully = IOPSApplication.getInstance().getFcmUnitAtRiskActionPlan().
                                        UnitAtRiskActionPlan(fcmBaseUnitAtRiskActionPlanMO.getData());*/
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }

                break;

            case Config.HOLIDAY:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseHolidayMO fcmBaseHolidayMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FCMBaseHolidayMO.class);
                        if (null != fcmBaseHolidayMO && fcmBaseHolidayMO.getStatusCode() == 200) {
                            if (null != fcmBaseHolidayMO.getData() && fcmBaseHolidayMO.getData().size() != 0) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmHolidayModelInsertion().insertHoliday(fcmBaseHolidayMO.getData());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                break;

            case Config.LEAVE:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseLeaveMO fcmBaseLeaveMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FCMBaseLeaveMO.class);
                        if (null != fcmBaseLeaveMO && fcmBaseLeaveMO.getStatusCode() == 200) {
                            if (null != fcmBaseLeaveMO.getEmployeeLeaveCalendarList() && fcmBaseLeaveMO.getEmployeeLeaveCalendarList().size() != 0) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmEmployeeLeaveInsertion().
                                        insertEmployeeLeave(fcmBaseLeaveMO.getEmployeeLeaveCalendarList());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                break;

            case Config.UNIT_CONTACT:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseUnitContactMO fcmBaseUnitContactMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FCMBaseUnitContactMO.class);
                        if (null != fcmBaseUnitContactMO && fcmBaseUnitContactMO.getStatusCode() == 200) {
                            if (null != fcmBaseUnitContactMO.getData()) {
                                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmUnitContactModelInsertion().
                                        insertUnitContact(fcmBaseUnitContactMO.getData());
                                mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                            }
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    //DELETE OPERATION
                    FCMUnitDeletion fcmUnitDeletion = new FCMUnitDeletion(IOPSApplication.getInstance());
                    boolean isRecordDeleted = fcmUnitDeletion.deleteUnitContactRecord(mBackGroundSyncMO.getDeleteId());
                    mNotificationUtils.notificationUpdate(isRecordDeleted, mBackGroundSyncMO);
                }
                break;

            case Config.AKR:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseAkrSync baseAkrSync = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseAkrSync.class);
                        if (null != baseAkrSync && baseAkrSync.getStatusCode() == HttpURLConnection.HTTP_OK && null != baseAkrSync.getFcmAkrDistributionData()) {
                            isInsertedSuccessfully = IOPSApplication.getInstance().getFcmAkrModelInsertionDBInstance()
                                    .insertAkrDistributionModel(baseAkrSync.getFcmAkrDistributionData());
                            mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.AKR));
                break;

            case Config.ISSUE:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    updateIssueStatus(true);
                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.ISSUE));
                break;

            case Config.IMPROVEMENT_PLAN:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    updateIssueStatus(false);
                }
                // Trigger EventBus
                EventBus.getDefault().post(new EventBusIntentReceiver(Activity.RESULT_OK, Config.ISSUE));
                break;
            case Config.ADD_COMPLAINT:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseIssuesActionplanSync baseIssuesActionplanSync = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseIssuesActionplanSync.class);
                        if (null != baseIssuesActionplanSync && baseIssuesActionplanSync.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != baseIssuesActionplanSync.getFcmIssuesActionPlanData()) {
                            isInsertedSuccessfully = IOPSApplication.getInstance().getFcmIssuesModelInsertionDBInstance().insertIssuesModel(baseIssuesActionplanSync.getFcmIssuesActionPlanData());
                            mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                        }

                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                break;
            case Config.BILL_SUBMISSION_CHECKLIST:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FcmBaseBillCheckListMO fcmBaseBillCheckListMO =
                                IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FcmBaseBillCheckListMO.class);
                        if (null != fcmBaseBillCheckListMO && fcmBaseBillCheckListMO.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != fcmBaseBillCheckListMO.getData()) {
                            FcmBillingModelInsertion fcmBillingModelInsertion = new FcmBillingModelInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmBillingModelInsertion.
                                    insertBillCheckListModel(fcmBaseBillCheckListMO.getData());
                            mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }
                break;

            case Config.LOOKUP:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FcmBaseLookupData fcmBaseLookupData = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), FcmBaseLookupData.class);
                        if (null != fcmBaseLookupData && fcmBaseLookupData.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != fcmBaseLookupData.getLookupModels()) {
                            FcmLookUpModelInsertion fcmLookUpModelInsertion = new FcmLookUpModelInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmLookUpModelInsertion.insertLookupModel(fcmBaseLookupData.getLookupModels());
                            mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }

                break;
            case Config.UNIT_BARRACK:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        FCMBaseUnitBarrackMO unitBarrackBaseMO = IOPSApplication.getGsonInstance().fromJson(responseData.toString(),
                                FCMBaseUnitBarrackMO.class);
                        if (null != unitBarrackBaseMO && unitBarrackBaseMO.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != unitBarrackBaseMO.getUnitBarrackList()) {
                            FCMUnitBarrackModelInsertion fcmUnitBarrackModelInsertion = new FCMUnitBarrackModelInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmUnitBarrackModelInsertion.
                                    insertUnitBarrack(unitBarrackBaseMO.getUnitBarrackList());
                            mNotificationUtils.notificationUpdate(isInsertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    //DELETE OPERATION
                    FCMUnitBarrackDeletion fcmUnitBarrackDeletion = new FCMUnitBarrackDeletion(IOPSApplication.getInstance());
                    boolean isRecordDeleted = fcmUnitBarrackDeletion.deleteUnitBarrackRecord(mBackGroundSyncMO.getDeleteId());
                    mNotificationUtils.notificationUpdate(isRecordDeleted, mBackGroundSyncMO);
                }

                break;
            case Config.RECRUITMENT:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseRecruitmentOR baseRecruitmentOR = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseRecruitmentOR.class);
                        if (null != baseRecruitmentOR && baseRecruitmentOR.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != baseRecruitmentOR.getData()) {
                            FCMRecuirtmentInsertion fcmRecuirtmentInsertion = new FCMRecuirtmentInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmRecuirtmentInsertion.insertRecuirtmentModel(baseRecruitmentOR);
                            mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                        }

                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }

                break;
                   /* case Config.BILL_SUBMISSION:
                        if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                            responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                            if (!TextUtils.isEmpty(responseData)) {
                                BaseRecruitmentOR baseRecruitmentOR = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseRecruitmentOR.class);
                                if (null != baseRecruitmentOR && baseRecruitmentOR.getStatusCode() == HttpURLConnection.HTTP_OK
                                        && null != baseRecruitmentOR.getData()) {
                                    FCMRecuirtmentInsertion fcmRecuirtmentInsertion = new FCMRecuirtmentInsertion(IOPSApplication.getInstance());
                                    isInsertedSuccessfully = fcmRecuirtmentInsertion.insertRecuirtmentModel(baseRecruitmentOR);
                                    mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                                }

                            }
                            else{
                                mNotificationUtils.notificationUpdate(false,mBackGroundSyncMO);
                            }
                        } else {
                            *//**
             * Do delete operation if want
             *//*
                        }

                        break;*/
            case Config.UNIT_MONTHLY_BILLDETAIL:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseBillCollectionOR baseBillCollectionOR = IOPSApplication.getGsonInstance().fromJson(responseData.toString(),
                                BaseBillCollectionOR.class);
                        if (null != baseBillCollectionOR && baseBillCollectionOR.statusCode == HttpURLConnection.HTTP_OK
                                && null != baseBillCollectionOR.unitMonthlyBillDetail) {
                            FCMBillCollectionInsertion fcmBillCollectionInsertion = new FCMBillCollectionInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmBillCollectionInsertion.insertBillCollectionApiTable(baseBillCollectionOR.unitMonthlyBillDetail);
                            mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                        }

                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }

                break;
            case Config.ADD_TASK:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        BaseAddTaskOR baseAddTaskOR = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseAddTaskOR.class);
                        if (null != baseAddTaskOR && baseAddTaskOR.getStatusCode() == HttpURLConnection.HTTP_OK
                                && null != baseAddTaskOR.getData()) {
                            FCMRotaModelInsertion fcmRecuirtmentInsertion = new FCMRotaModelInsertion(IOPSApplication.getInstance());
                            isInsertedSuccessfully = fcmRecuirtmentInsertion.insertFCMAddTask(baseAddTaskOR.getData());
                            mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);
                        }

                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                } else {
                    /**
                     * Do delete operation if want
                     */
                }

                break;
            case Config.ADHOC:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.BILL_SUBMISSION:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.BILL_COLLECTION:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.BILL_COLLECTION_DUE:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.BILL_COLLECTION_OVERDUE:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.AKR_DUE:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.CUSTOM:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.POA_COUNT:
                mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                break;
            case Config.LOGOUT:
                if (appPreferences != null) {
                    mNotificationUtils.notificationUpdate(true, mBackGroundSyncMO);
                    Util.setAlarmManager(context, Constants.SYNC_TRIGGER_TIME_AFTER_BLOCKING_AI);
                }
                break;
            case Config.GUARD_REFRESH:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        GuardForBranchBase guardForBranchBase = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), GuardForBranchBase.class);
                        if (null != guardForBranchBase && guardForBranchBase.statusCode == HttpURLConnection.HTTP_OK && null != guardForBranchBase.data) {
                            FcmGuardModelInsertion fcmGuardModelInsertion = new FcmGuardModelInsertion(context);
                            boolean insertedSuccessfully = fcmGuardModelInsertion.insertGuardInfo(guardForBranchBase.data.guardsForBranch);
                            mNotificationUtils.notificationUpdate(insertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }

                break;
            case Config.DIAGNOSTICS:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    QuerySelectStatementDB querySelectStatementDB = new QuerySelectStatementDB(context);
                    String result = null;
                    try {
                        result = querySelectStatementDB.getQueryExecutorResult(URLDecoder.decode(mBackGroundSyncMO.getCallbackUrl().trim(), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    QueryResultDb queryResultDb = new QueryResultDb(context);
                    boolean insertedSuccessfully = queryResultDb.insertQueryResult(mBackGroundSyncMO.getDeleteId(), result);
                    mNotificationUtils.notificationUpdate(insertedSuccessfully, mBackGroundSyncMO);
                }
                break;

            case Config.ISSUE_MATRIX:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        IssueMatrixBaseMO issueMatrixModel = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), IssueMatrixBaseMO.class);
                        if (null != issueMatrixModel && issueMatrixModel.statusCode == HttpURLConnection.HTTP_OK && null != issueMatrixModel.data) {
                            boolean insertedSuccessfully = IOPSApplication.getInstance().getInsertionInstance().insertOrUpdateIssueMatrixTable(issueMatrixModel.data);
                            mNotificationUtils.notificationUpdate(insertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }
                break;

            case Config.MASTER_ACTION_PLAN:
                if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                    responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                    if (!TextUtils.isEmpty(responseData)) {
                        MasterActionPlanBaseOR masterActionPlanBaseOR = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), MasterActionPlanBaseOR.class);
                        if (null != masterActionPlanBaseOR && masterActionPlanBaseOR.statusCode == HttpURLConnection.HTTP_OK &&
                                null != masterActionPlanBaseOR.masterActionPlanModelList) {

                            boolean insertedSuccessfully = IOPSApplication.getInstance().getInsertionInstance()
                                    .insertOrUpdateActionPlanToTable(masterActionPlanBaseOR.masterActionPlanModelList);
                            mNotificationUtils.notificationUpdate(insertedSuccessfully, mBackGroundSyncMO);
                        }
                    } else {
                        mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                    }
                }
                break;

            case Config.REMOVE_SHARED_PREF_KEY:
                try {
//                    Note: Need to send "Shared Pref KEY" name in callBackUrl Tag which needed to removed from the Shared Pref
                    if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl()))
                        appPreferences.removeKeyFromSharedPref(mBackGroundSyncMO.getCallbackUrl().trim());

                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                break;

            case Config.UNIT_TYPE:
                try {
                    if (!TextUtils.isEmpty(mBackGroundSyncMO.getCallbackUrl())) {
                        responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
                        if (!TextUtils.isEmpty(responseData)) {
                            UnitTypeRequestMO unitTypeRequest = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), UnitTypeRequestMO.class);
                            if (null != unitTypeRequest && unitTypeRequest.statusCode == HttpURLConnection.HTTP_OK
                                    && null != unitTypeRequest.unitTypeDataMO) {
                                boolean insertedSuccessfully = IOPSApplication.getInstance().getInsertionInstance()
                                        .insertUnitTypeDetails(unitTypeRequest.unitTypeDataMO);

                                mNotificationUtils.notificationUpdate(insertedSuccessfully, mBackGroundSyncMO);
                            }
                        } else {
                            mNotificationUtils.notificationUpdate(false, mBackGroundSyncMO);
                        }
                    }
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                break;
        }
    }

    public String genericAPICall(String callBackAPI) {
        try {
            callBackAPI = URLDecoder.decode(callBackAPI.trim(), "UTF-8");
            genericAPICall = new GenericAPICall(callBackAPI, appPreferences.getAccessToken());
            responseData = genericAPICall.doInBackground();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return responseData;
    }

    private void updateIssueStatus(boolean statusUpdate) {
        responseData = genericAPICall(mBackGroundSyncMO.getCallbackUrl());
        if (!TextUtils.isEmpty(responseData)) {
            BaseIssuesStatusSync baseIssuesStatusSync = IOPSApplication.getGsonInstance().fromJson(responseData.toString(), BaseIssuesStatusSync.class);
            if (null != baseIssuesStatusSync && baseIssuesStatusSync.getStatusCode() == HttpURLConnection.HTTP_OK && null != baseIssuesStatusSync.getFcmIssuesData()) {
                isInsertedSuccessfully = IOPSApplication.getInstance().getFcmIssueStausUpdateDBInstance()
                        .insertIssuesModel(baseIssuesStatusSync.getFcmIssuesData(), statusUpdate);
                mNotificationUtils.insertNotificationReceipt(isInsertedSuccessfully, mBackGroundSyncMO);

            } else {
                /**
                 * Do delete operation if want
                 */
            }
        }
    }

}
