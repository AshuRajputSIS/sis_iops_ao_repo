package com.sisindia.ai.android.rota.billsubmission.modelObjects;

/**
 * Created by shankar on 23/9/16.
 */

public class LookUpCheckListItem {

    private String checkListItem;
    private int checkListItemId;

    public String getCheckListItem() {
        return checkListItem;
    }

    public void setCheckListItem(String checkListItem) {
        this.checkListItem = checkListItem;
    }

    public int getCheckListItemId() {
        return checkListItemId;
    }

    public void setCheckListItemId(int checkListItemId) {
        this.checkListItemId = checkListItemId;
    }



}
