package com.sisindia.ai.android.fcmnotification.fcmbarrack;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.BarrackStrength;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMBarrackStrengthInsertion extends SISAITrackingDB {

    private SQLiteDatabase sqlite=null;
    private boolean isInsertedSuccessfully;

    public FCMBarrackStrengthInsertion(Context context) {
        super(context);
    }
    public synchronized boolean insertBarrackStrength(BarrackStrength barrackStrengthList) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                if(barrackStrengthList != null) {
                    sqlite.beginTransaction();
                    insertBarrackStrengthModel(barrackStrengthList);

                    sqlite.setTransactionSuccessful();

                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertBarrackStrengthModel(BarrackStrength barrackStrength) {

                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, barrackStrength.getId());
                values.put(TableNameAndColumnStatement.SHIFT_ID, barrackStrength.getShiftId());
                values.put(TableNameAndColumnStatement.RANK_ID, barrackStrength.getRankId());
                values.put(TableNameAndColumnStatement.RANK_COUNT, barrackStrength.getRankCount());
                values.put(TableNameAndColumnStatement.RANK_ABBREVATION, barrackStrength.getRankAbbrevation());
                values.put(TableNameAndColumnStatement.BARRACK_ID, barrackStrength.getBarrackId());
                values.put(TableNameAndColumnStatement.UNIT_NAME, barrackStrength.getUnitName());
                values.put(TableNameAndColumnStatement.SHIFT_NAME, barrackStrength.getShiftName());
                values.put(TableNameAndColumnStatement.RANK_NAME, barrackStrength.getRankName());
                values.put(TableNameAndColumnStatement.BARRACK_NAME, barrackStrength.getBarrackName());
                values.put(TableNameAndColumnStatement.STRENGTH, barrackStrength.getStrength());
                values.put(TableNameAndColumnStatement.ACTUAL, 0);
                values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                values.put(TableNameAndColumnStatement.IS_ARMED, barrackStrength.getIsArmed());
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, barrackStrength.getId(),
                        TableNameAndColumnStatement.ID
                        , TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, TableNameAndColumnStatement.ID, sqlite);
                if (genericUpdateTableMO.getIsAvailable() != 0) {
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().
                            deleteTableById(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE,
                                    genericUpdateTableMO.getId(),sqlite);
                    sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
                } else {
                    sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);

        }
    }
}
