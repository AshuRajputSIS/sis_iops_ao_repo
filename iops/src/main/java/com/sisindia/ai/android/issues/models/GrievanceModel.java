package com.sisindia.ai.android.issues.models;

import java.io.Serializable;

/**
 * Created by Shushrut on 15-04-2016.
 */
public class GrievanceModel implements Serializable {
    public String guardName;
    public String grevianceNature;
    public String getGrevianceDescription;
    public String unitName;
    public String mStartDate;
    public String mEndDate;
    public int mEmployeeID;
    public String date;
    private String grievanceNatureId;

    public String getGrievanceNatureId() {
        return grievanceNatureId;
    }

    public void setGrievanceNatureId(String grievanceNatureId) {
        this.grievanceNatureId = grievanceNatureId;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public String getGrevianceNature() {
        return grevianceNature;
    }

    public void setGrevianceNature(String grevianceNature) {
        this.grevianceNature = grevianceNature;
    }

    public String getGetGrevianceDescription() {
        return getGrevianceDescription;
    }

    public void setGetGrevianceDescription(String getGrevianceDescription) {
        this.getGrevianceDescription = getGrevianceDescription;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getmEmployeeID() {
        return mEmployeeID;
    }

    public void setmEmployeeID(int mEmployeeID) {
        this.mEmployeeID = mEmployeeID;
    }
}
