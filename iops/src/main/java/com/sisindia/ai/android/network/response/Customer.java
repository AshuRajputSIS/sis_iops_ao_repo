package com.sisindia.ai.android.network.response;

public class Customer {

    private Integer Id;
    private String CustomerName;
    private String CustomerDescription;
    private Boolean IsActive;
    private Integer CustomerHoAddressId;
    private Integer CustomerPrimaryContactId;
    private Integer CustomerSecondaryContactId;
    private Boolean IsSmsToBeSent;
    private Boolean IsEmailToBeSent;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The CustomerName
     */
    public String getCustomerName() {
        return CustomerName;
    }

    /**
     * @param CustomerName The CustomerName
     */
    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    /**
     * @return The CustomerDescription
     */
    public String getCustomerDescription() {
        return CustomerDescription;
    }

    /**
     * @param CustomerDescription The CustomerDescription
     */
    public void setCustomerDescription(String CustomerDescription) {
        this.CustomerDescription = CustomerDescription;
    }

    /**
     * @return The IsActive
     */
    public Boolean getIsActive() {
        return IsActive;
    }

    /**
     * @param IsActive The IsActive
     */
    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    /**
     * @return The CustomerHoAddressId
     */
    public Integer getCustomerHoAddressId() {
        return CustomerHoAddressId;
    }

    /**
     * @param CustomerHoAddressId The CustomerHoAddressId
     */
    public void setCustomerHoAddressId(Integer CustomerHoAddressId) {
        this.CustomerHoAddressId = CustomerHoAddressId;
    }

    /**
     * @return The CustomerPrimaryContactId
     */
    public Integer getCustomerPrimaryContactId() {
        return CustomerPrimaryContactId;
    }

    /**
     * @param CustomerPrimaryContactId The CustomerPrimaryContactId
     */
    public void setCustomerPrimaryContactId(Integer CustomerPrimaryContactId) {
        this.CustomerPrimaryContactId = CustomerPrimaryContactId;
    }

    /**
     * @return The CustomerSecondaryContactId
     */
    public Integer getCustomerSecondaryContactId() {
        return CustomerSecondaryContactId;
    }

    /**
     * @param CustomerSecondaryContactId The CustomerSecondaryContactId
     */
    public void setCustomerSecondaryContactId(Integer CustomerSecondaryContactId) {
        this.CustomerSecondaryContactId = CustomerSecondaryContactId;
    }

    /**
     * @return The IsSmsToBeSent
     */
    public Boolean getIsSmsToBeSent() {
        return IsSmsToBeSent;
    }

    /**
     * @param IsSmsToBeSent The IsSmsToBeSent
     */
    public void setIsSmsToBeSent(Boolean IsSmsToBeSent) {
        this.IsSmsToBeSent = IsSmsToBeSent;
    }

    /**
     * @return The IsEmailToBeSent
     */
    public Boolean getIsEmailToBeSent() {
        return IsEmailToBeSent;
    }

    /**
     * @param IsEmailToBeSent The IsEmailToBeSent
     */
    public void setIsEmailToBeSent(Boolean IsEmailToBeSent) {
        this.IsEmailToBeSent = IsEmailToBeSent;
    }

}