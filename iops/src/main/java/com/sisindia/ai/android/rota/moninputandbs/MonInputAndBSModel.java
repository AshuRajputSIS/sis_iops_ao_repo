package com.sisindia.ai.android.rota.moninputandbs;

/**
 * Created by Ashu Rajput on 3/20/2018.
 */

public class MonInputAndBSModel {

    private int totalCountOfMiOrBS;
    private int pendingCountOfMiOrBS;
    private int typeMIorBS; //[1: MonInput and 2: Bill Submission]

    public int getTypeMIorBS() {
        return typeMIorBS;
    }

    public void setTypeMIorBS(int typeMIorBS) {
        this.typeMIorBS = typeMIorBS;
    }

    public int getTotalCountOfMiOrBS() {
        return totalCountOfMiOrBS;
    }

    public void setTotalCountOfMiOrBS(int totalCountOfMiOrBS) {
        this.totalCountOfMiOrBS = totalCountOfMiOrBS;
    }

    public int getPendingCountOfMiOrBS() {
        return pendingCountOfMiOrBS;
    }

    public void setPendingCountOfMiOrBS(int pendingCountOfMiOrBS) {
        this.pendingCountOfMiOrBS = pendingCountOfMiOrBS;
    }
}
