package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.mybarrack.BarrackInspectionTabActivity;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.clientcoordination.ClientCoordinationActivity;
import com.sisindia.ai.android.rota.daycheck.taskExecution.DayNightChecking;
import com.sisindia.ai.android.rota.daycheck.taskExecution.DayNightCheckingBaseMO;
import com.sisindia.ai.android.rota.daycheck.taskExecution.SingletonDayNightChecking;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.nightcheck.NightCheckNavigationActivity;
import com.sisindia.ai.android.utils.Util;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LastInspectionReviewActivity extends DayCheckBaseActivity implements ShowLastInspectionDialogListener {

    private Context mContext;
    @Bind(R.id.generic_checking_recyclerview)
    RecyclerView checkingRecyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private LastInspectionReviewAdapter lastInspectionReviewAdapter;
    private RotaTaskModel rotaTaskModel;
    public int unitId;
//    private ArrayList<Object> lastInspectionDataList;
//    private SISAITrackingDB sisaiTrackingDBInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_checking);
        mContext = LastInspectionReviewActivity.this;
        ButterKnife.bind(this);
//        sisaiTrackingDBInstance = IOPSApplication.getInstance().getSISAITrackingDBInstance();
        rotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if (rotaTaskModel == null) {
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
        }

        if (null != rotaTaskModel) {
            unitId = rotaTaskModel.getUnitId();
        }
        LastCheckInspectionBaseMO lastCheckInspectionBaseMO = IOPSApplication.getInstance().
                getSelectionStatementDBInstance().fetchLastCheckInspectionData(rotaTaskModel.getUnitId(),
                rotaTaskModel.getTaskId(), RotaTaskStatusEnum.Completed.getValue(), rotaTaskModel.getTaskTypeId(),
                rotaTaskModel.getUnitName(), rotaTaskModel.getBarrackId(), rotaTaskModel.getBarrackName());

//        String fromScreen = null;

        if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Night_Checking.getValue())
            setBaseToolbar(toolbar, getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING));
        else if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Barrack_Inspection.getValue())
            setBaseToolbar(toolbar, getResources().getString(R.string.barrack_inspection_string));
        else if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Client_Coordination.getValue())
            setBaseToolbar(toolbar, getResources().getString(R.string.client_coordination));
        else
            setBaseToolbar(toolbar, getResources().getString(R.string.dayCheckingTitle));

        //lastCheckInspectionBaseMO.getLastCheckInspectionBaseMOList()
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        checkingRecyclerView.setLayoutManager(linearLayoutManager);
        checkingRecyclerView.setItemAnimator(new DefaultItemAnimator());
        lastInspectionReviewAdapter = new LastInspectionReviewAdapter(this, rotaTaskModel.getTaskTypeId());
        lastInspectionReviewAdapter.setCheckingData(lastCheckInspectionBaseMO.getLastCheckInspectionBaseMOList());
        checkingRecyclerView.setAdapter(lastInspectionReviewAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_day_checking, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.next) {
            StepperDataMo.clearStepper();
            if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Barrack_Inspection.getValue()) {
                Intent intent = new Intent(mContext, BarrackInspectionTabActivity.class);
                intent.putExtra("RotaModel", rotaTaskModel);
                startActivity(intent);
            } else if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Night_Checking.getValue()) {
                // Util.setReferenceTypeId(5);
                createTaskExecutionResult();
                Intent nightcheck = new Intent(mContext, NightCheckNavigationActivity.class);
                startActivity(nightcheck);

            } else if (rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Client_Coordination.getValue()) {
                Intent clientCoordinationIntent = new Intent(mContext, ClientCoordinationActivity.class);
                //clientCoordinationIntent.putExtra(SingleUnitActivity.CHECKING_TYPE, checkingType);
                startActivity(clientCoordinationIntent);
            } else {
                createTaskExecutionResult();
                Intent dayCheckNavigationActivity = new Intent(mContext, DayCheckNavigationActivity.class);
                startActivity(dayCheckNavigationActivity);
                Util.setReferenceTypeId(5);
            }

            /*Intent sitePostsIntent = new  Intent(mContext,SitePostsCheckingActivity.class);
            sitePostsIntent.putExtra(SingleUnitActivity.CHECKING_TYPE,checkingType);
            startActivity(sitePostsIntent);*/
            return true;
        } else if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createTaskExecutionResult() {
        DayNightCheckingBaseMO dayNightCheckingBaseMO = new DayNightCheckingBaseMO();
        DayNightChecking dayNightChecking = new DayNightChecking();
        dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
        SingletonDayNightChecking.getInstance().setDayNightCheckData(dayNightCheckingBaseMO);
    }

    @Override
    public void showLastInspectionDialog(int layout, Object object) {
        LastInspectionDialogFragment lastInspectionDialogFragment = LastInspectionDialogFragment.newInstance();
        lastInspectionDialogFragment.setContent(object);
        lastInspectionDialogFragment.setLayout(layout);
        lastInspectionDialogFragment.show(getSupportFragmentManager(), "");
    }
}
