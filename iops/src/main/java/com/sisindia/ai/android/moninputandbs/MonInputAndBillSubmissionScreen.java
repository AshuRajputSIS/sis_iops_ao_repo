package com.sisindia.ai.android.moninputandbs;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.rota.TaskDetailsActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 3/21/2018.
 */

public class MonInputAndBillSubmissionScreen extends AppCompatActivity {

    private int callTypeMIorBS;
    private AppPreferences appPreferences;
//    private SISClient sisClient;
//    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    @Bind(R.id.monInputAndBillSubmissionRV)
    RecyclerView monInputAndBillSubmissionRV;

    @Bind(R.id.moninputBSSyncButton)
    ImageView moninputBSSyncButton;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private ArrayList<Object> rotaTaskList;
    private String selectedDate = "";

    /**
     * callTypeMIorBS = 1 -> MonInput
     * callTypeMIorBS = 2 -> Bill Submission
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moninput_billsubmission);
        ButterKnife.bind(this);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("CALL_TYPE_MI_BS")) {
                callTypeMIorBS = receivedBundle.getInt("CALL_TYPE_MI_BS");
            }
            if (receivedBundle.containsKey("CALENDAR_SELECTED_DATE")) {
                selectedDate = receivedBundle.getString("CALENDAR_SELECTED_DATE");
            }
        }

        setUpToolBar();
        initializingElements();
        initializeRecyclerView();
    }

    private void initializingElements() {
        appPreferences = new AppPreferences(this);
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (callTypeMIorBS == 1)
            getSupportActionBar().setTitle("Mon Input");
        else if (callTypeMIorBS == 2)
            getSupportActionBar().setTitle("Bill Submission");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initializeRecyclerView() {

        rotaTaskList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getMIAndBSMonthlyRota(callTypeMIorBS,selectedDate);

        monInputAndBillSubmissionRV.setLayoutManager(new LinearLayoutManager(this));
        monInputAndBillSubmissionRV.setItemAnimator(new DefaultItemAnimator());

        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        monInputAndBillSubmissionRV.addItemDecoration(dividerItemDecoration);

        monInputAndBillSubmissionRV.setAdapter(new MonInputBSAdapter(this, rotaTaskList, new OnRecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClick(View v, int position) {
                SingletonUnitDetails singletonUnitDetails = SingletonUnitDetails.getInstance();
                singletonUnitDetails.setUnitDetails(rotaTaskList.get(position), position);
                appPreferences.setRotaTaskJsonData(rotaTaskList.get(position));
//                updateRotaTaskModelListener.updateRotaTask(rotaTaskList.get(position));
                IOPSApplication.getInstance().getUpdateStatementDBInstance().updatePostBasedOnUnit(((RotaTaskModel) rotaTaskList.get(position)).getUnitId());
                Intent taskDetailsIntent = new Intent(MonInputAndBillSubmissionScreen.this, TaskDetailsActivity.class);
                startActivity(taskDetailsIntent);
            }
        }));
    }
}