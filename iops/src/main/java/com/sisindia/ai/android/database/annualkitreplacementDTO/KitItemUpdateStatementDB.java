package com.sisindia.ai.android.database.annualkitreplacementDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 02-09-2016.
 */
public class KitItemUpdateStatementDB extends SISAITrackingDB {


    public KitItemUpdateStatementDB(Context context) {
        super(context);
    }

    public  synchronized void updateKitItemRequestedId(int id,int kitRequestedId){

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE +
                " SET " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + "='" + kitRequestedId + "'" +
                " ," + TableNameAndColumnStatement.IS_NEW+ "=0" +
                " ," + TableNameAndColumnStatement.IS_SYNCED+ "=1"+
                " WHERE "+
                TableNameAndColumnStatement.ID+ "='"+id+"'";


        Timber.d("UpdateKitItemRequestTable  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateKitItemRequestCount  %s", cursor.getCount());

        }catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public synchronized void updateAnnualKitDistributionId(int id){

        String updateQuery = " UPDATE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+
                " SET " + TableNameAndColumnStatement.IS_SYNCED+ "=1" +
                " WHERE "+  TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID+ "='"+id+"'"
                + " and "+ TableNameAndColumnStatement.ISSUED_DATE  + " != '' ";
        //|| "+TableNameAndColumnStatement.IS_UNPAID + " != 0 )"


        Timber.d("UpdateAnnualKitDistributedID  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateAnnualKitDistributedCount  %s", cursor.getCount());


        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }



    public synchronized void updateAnnualKitDistributionIdWithNonIssue(int id,int kitDistributedId){

        String updateQuery = " UPDATE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+
                " SET " + TableNameAndColumnStatement.IS_SYNCED+ "=1" +
                " WHERE "+  TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID+ "='"+id+"'"
                + " and "+ TableNameAndColumnStatement.IS_UNPAID  + " != 0 ";
        //|| "+TableNameAndColumnStatement.IS_UNPAID + " != 0 )"


        Timber.d("UpdateAnnualKitDistributedID  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateAnnualKitDistributedCount  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }


    public  synchronized void updateKitDistributionItemId(int kitDistributionItemId, KitDeliveredItemsIR kitDeliveredItemsIR){

        String updateQuery = null;


            updateQuery = "UPDATE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+
                    " SET " + TableNameAndColumnStatement.IS_ISSUED+ " = " + 1 +
                    " ," + TableNameAndColumnStatement.IS_SYNCED+ " = " + 0 +
                    " ," + TableNameAndColumnStatement.IS_UNPAID+ " = " + 0 +
                    " ," + TableNameAndColumnStatement.NON_ISSUE_REASON_ID+ " = " + 0 +
                    " ," + TableNameAndColumnStatement.ISSUED_DATE+ " = '"+ kitDeliveredItemsIR.getIssuedDate()+ "'"+
                    " WHERE "+  TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID + "='"+kitDistributionItemId+"'";
        

        Timber.d("UpdateAnnualKitDistributedID  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateAnnualKitDistributedCount  %s", cursor.getCount());
        }catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }


    public  synchronized void updateKitDistributionStatus(int kitDistributionId,
                                                          int kitDistributionStatus, String currentDateTime){

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE +
                " SET " + TableNameAndColumnStatement.DISTRIBUTION_STATUS + "='" +  kitDistributionStatus + "'" +
                ","+TableNameAndColumnStatement.DISTRIBUTION_DATE + "='" +  currentDateTime + "'" +
                " WHERE "+  TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + "='"+kitDistributionId+"'";


        Timber.d("UpdateKitDistributedID  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateKitDistributedCount  %s", cursor.getCount());
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }


    public  synchronized void updateStatusOnAllKitDistributionItemsIssued(int kitDistributionId, int kitDistributionStatus, String currentDateTime){

        String selectQuery = " SELECT TotalItems, ifnull(IssuedItems,0) IssuedItems from "+
                             " (SELECT count(*) as TotalItems, kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+" from "+TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE +" kd " +
                             " inner join "+TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+" kti on kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+" = kd."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID +
                             " where kd."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+"= "+kitDistributionId+ " group by kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+")t " +
                             " left join (SELECT count(*) as IssuedItems, kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+"  from "+TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE +" kd " +
                             " inner join "+TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+" kti on kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+" = kd."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+
                             " where kd."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+" = "+kitDistributionId+ " and kti."+TableNameAndColumnStatement.IS_ISSUED+" = 1 group by kti."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+")i on t."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID+" = i."+TableNameAndColumnStatement.KIT_DISTRIBUTION_ID;


        Timber.d("UpdateKitDistributedID  %s", selectQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if(cursor != null && cursor.moveToNext()){
                if(cursor.getInt(cursor.getColumnIndex("TotalItems")) == cursor.getInt(cursor.getColumnIndex("IssuedItems"))){
                    updateKitDistributionStatus(kitDistributionId,kitDistributionStatus,currentDateTime);
                }
            }
            Timber.d("UpdateKitDistributedCount  %s", cursor.getCount());
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void updateKitDistributionItemStatus(int nonIssueReasonId,int kitDistributionId) {

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE +
                " SET " + TableNameAndColumnStatement.IS_UNPAID + "='" +  1 + "'" +
                " ," + TableNameAndColumnStatement.IS_SYNCED+ " = " + 0 +
                " ," + TableNameAndColumnStatement.IS_ISSUED+ " = " + 0 +
                " ," + TableNameAndColumnStatement.ISSUE_DATE+ " = '' " +
                ","+TableNameAndColumnStatement.NON_ISSUE_REASON_ID + "='" +  nonIssueReasonId + "'" +
                " WHERE "+  TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + "='"+kitDistributionId+"'";


                // " and "+TableNameAndColumnStatement.IS_ISSUED+"=0" ; (commented bcz some problem with data from backend)


        Timber.d("UpdateKitDistributedID  %s", updateQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateKitDistributedCount  %s", cursor.getCount());
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        }finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }
}
