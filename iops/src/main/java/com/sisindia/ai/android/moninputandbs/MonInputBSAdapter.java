package com.sisindia.ai.android.moninputandbs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.daycheck.NoTaskMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 3/21/2018.
 */

public class MonInputBSAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater layoutInflater = null;
    private ArrayList<Object> rotaModelList;
    private Context context = null;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    public static final int TYPE_NO_TASK = 3;
    private OnRecyclerViewItemClickListener itemListener;

    public MonInputBSAdapter(Context context, ArrayList<Object> rotaModelList, OnRecyclerViewItemClickListener itemListener) {
        layoutInflater = LayoutInflater.from(context);
        this.rotaModelList = rotaModelList;
        this.context = context;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        this.itemListener = itemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView;
        switch (viewType) {

            case TYPE_HEADER:
                itemView = layoutInflater.inflate(R.layout.header_row, parent, false);
                recycleViewHolder = new HeaderViewHolder(itemView);
                break;

            case TYPE_ROW:
                itemView = layoutInflater.inflate(R.layout.recycler_rota_row, parent, false);
                recycleViewHolder = new MIBSViewHolder(itemView);
                break;
            case TYPE_NO_TASK:
                itemView = layoutInflater.inflate(R.layout.notask_for_day, parent, false);
                recycleViewHolder = new NoTaskViewHolder(itemView);
                break;
        }
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {

            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.headerTextview.setText((String) rotaModelList.get(position));
                break;

            case TYPE_ROW:
                MIBSViewHolder rowDataHolder = (MIBSViewHolder) holder;

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskTypeId() == 3) {
                    rowDataHolder.addressTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getBarrackName());
                } else {
                    rowDataHolder.addressTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getUnitName());
                }

                rowDataHolder.checkingTextview.setText(((RotaTaskModel) rotaModelList.get(position)).getTypeChecking());
                String startTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(((RotaTaskModel) rotaModelList.get(position)).getStartTime());
                String endTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(((RotaTaskModel) rotaModelList.get(position)).getEndTime());

                rowDataHolder.timeTextview.setText(startTime + "-" + endTime);
                rowDataHolder.typeIcon.setImageResource(((RotaTaskModel) rotaModelList.get(position)).getIcon());

                if (TextUtils.isEmpty(((RotaTaskModel) rotaModelList.get(position)).getUnitCode()))
                    rowDataHolder.unitCode.setVisibility(View.GONE);
                else
                    rowDataHolder.unitCode.setText(((RotaTaskModel) rotaModelList.get(position)).getUnitCode());

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatusId() == 3)
                    rowDataHolder.nextIcon.setImageResource(R.drawable.greentick);
                else if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatusId() == 4)
                    rowDataHolder.nextIcon.setImageResource(R.drawable.red_tick);
                else
                    rowDataHolder.nextIcon.setImageResource(R.drawable.arrow_right);

                if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("InActive") ||
                        ((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("In Active")) {
                    rowDataHolder.approvedStatus.setText(context.getResources().getString(R.string.COMPLETED_AND_SYNCED));
                } else if (((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("InProgress") ||
                        ((RotaTaskModel) rotaModelList.get(position)).getTaskStatus().equalsIgnoreCase("In Progress")) {
                    rowDataHolder.approvedStatus.setText(context.getResources().getString(R.string.INPROGRESS));
                } else
                    rowDataHolder.approvedStatus.setText(((RotaTaskModel) rotaModelList.get(position)).getTaskStatus());

                break;
            case TYPE_NO_TASK:
                NoTaskViewHolder noTaskViewHolder = (NoTaskViewHolder) holder;
                noTaskViewHolder.noTaskTextview.setText(((NoTaskMO) rotaModelList.get(position)).getMessage());
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (rotaModelList.get(position) instanceof String)
            return TYPE_HEADER;
        else if (rotaModelList.get(position) instanceof RotaTaskModel)
            return TYPE_ROW;
        else if (rotaModelList.get(position) instanceof NoTaskMO)
            return TYPE_NO_TASK;

        return -1;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (rotaModelList != null) {
            count = rotaModelList.size() == 0 ? 0 : rotaModelList.size();
        }
        return count;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        //        TextView headerTextview;
        @Bind(R.id.recycle_header)
        CustomFontTextview headerTextview;

        public HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
//            this.headerTextview = (CustomFontTextview) view.findViewById(R.id.recycle_header);
        }
    }

    public class MIBSViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_time)
        TextView timeTextview;
        @Bind(R.id.address)
        TextView addressTextview;
        @Bind(R.id.checkType)
        TextView checkingTextview;
        @Bind(R.id.approved_status)
        TextView approvedStatus;
        @Bind(R.id.unit_code_tv)
        TextView unitCode;
        @Bind(R.id.dayCheck)
        ImageView typeIcon;
        @Bind(R.id.nextIcon)
        ImageView nextIcon;

        public MIBSViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.onRecyclerViewItemClick(v, getLayoutPosition());
                }
            });
        }
    }

    public class NoTaskViewHolder extends RecyclerView.ViewHolder {
        //        TextView noTaskTextview;
        @Bind(R.id.noTaskTextview)
        CustomFontTextview noTaskTextview;

        public NoTaskViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
//            this.noTaskTextview = (CustomFontTextview) view.findViewById(R.id.noTaskTextview);
        }
    }

}
