package com.sisindia.ai.android.unitatriskpoa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 15/7/16.
 */

public class UARActionPlanFrgamentAdapter extends FragmentPagerAdapter {






    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();


    public UARActionPlanFrgamentAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void addFragment(Fragment grievanceFragment, String fragmentTitle) {
        mFragmentList.add(grievanceFragment);
        mFragmentTitleList.add(fragmentTitle);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}