package com.sisindia.ai.android.home;

import android.content.Context;
import android.os.AsyncTask;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.RotaUnitSynchronization;
import com.sisindia.ai.android.rota.models.RotaMO;
import com.sisindia.ai.android.utils.Util;

/**
 * Created by Saruchi on 06-06-2016.
 */
public class RotaUpdateAsyncTask extends AsyncTask<String, String, String> {


    private RotaMO rotaData = new RotaMO();
    private Context context;
    private RotaUnitSynchronization rotaUnitSynchronization;
    private RotaUnitSyncListener rotaUnitSyncListener;

    public RotaUpdateAsyncTask(Context context, RotaMO data, RotaUnitSyncListener rotaUnitSyncListener) {
        rotaData = data;
        this.context = context;
        this.rotaUnitSyncListener = rotaUnitSyncListener;
        rotaUnitSynchronization = new RotaUnitSynchronization(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {
        if (rotaData != null) {

            if (null != rotaData.getRotaModelMO()) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertRotaTable(rotaData.getRotaModelMO(), true);
            }
            if (null != rotaData.getUnitTasks() && rotaData.getUnitTasks().size() != 0) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertTaskTable(rotaData.getUnitTasks(), 0, 1, true);
            }
            if (null != rotaData.getRotaTasks() && rotaData.getRotaTasks().size() != 0) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertRotaTaskTable(rotaData.getRotaTasks(), true);
            }
            if (null != rotaData.getUnitMonthlyBillDetail() && rotaData.getUnitMonthlyBillDetail().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertBillCollactionApiTable(rotaData.getUnitMonthlyBillDetail());
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        hideprogressbar(true);
        rotaUnitSyncListener.rotaUnitDbSyncing(true);
        super.onPostExecute(s);
    }

    private void hideprogressbar(boolean isCompleted) {
        Util.dismissProgressBar(isCompleted);
    }

}