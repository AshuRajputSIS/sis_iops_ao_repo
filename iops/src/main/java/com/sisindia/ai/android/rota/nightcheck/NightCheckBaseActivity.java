/*
package com.sisindia.ai.android.rota.nightcheck;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.rotaDTO.RotaTempTaskIdUpdateStatementDB;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.DayCheckListAdapter;
import com.sisindia.ai.android.rota.daycheck.StepperDataMo;
import com.sisindia.ai.android.rota.daycheck.taskExecution.SingletonDayNightChecking;
import com.sisindia.ai.android.rota.models.RotaTaskModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

*/
/**
 * Created by Shushrut on 24-06-2016.
 *//*

public class NightCheckBaseActivity extends BaseActivity {
    protected static ArrayList<AttachmentMetaDataModel> attachmentMetaArrayList =new ArrayList<>();
    protected RotaTaskModel rotaTaskModel;
    protected  LinearLayout completedTaskCountLayout;
    ArrayList<View> mProgressConnect;
    private ArrayList<View> viewList;
    private  AttachmentMetaDataModel attachmentMetaDataModel;
    private TextView unitNameTextview;
    private String unitName;
    private TextView completedTasksTextView;
    private TextView totalTasksTextView;

    public NightCheckBaseActivity(){
        rotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if(rotaTaskModel == null)
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
    }

    public AttachmentMetaDataModel getAttachmentMetaDataModel() {
        return attachmentMetaDataModel;
    }

    public void setAttachmentMetaDataModel(AttachmentMetaDataModel attachmentMetaDataModel) {
        this.attachmentMetaDataModel = attachmentMetaDataModel;
        attachmentMetaArrayList .add(attachmentMetaDataModel);
        Timber.d("attachmentMetaArrayList  %s", attachmentMetaArrayList.size());

    }

    public void setHeaderUnitName(String unitName){
        this.unitName = unitName;

    }

    public  void updateDayCheckUIStepper(int pos, DayCheckListAdapter mAdapter) {
        if(mAdapter != null) {
            mAdapter.notifyItemChanged(pos - 1);
            mAdapter.updateWithTickMark(pos - 1, true);
        }
        switch (pos){
            case 1:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 2:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 3:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 4:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 5:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 6:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 7:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
        }

    }
    public  void updateNightCheckUIStepper(int pos, NightCheckListAdapter mAdapter) {
        if(mAdapter != null) {
            mAdapter.notifyItemChanged(pos - 1);
            mAdapter.updateWithTickMark(pos - 1, true);
        }
        switch (pos){
            case 1:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 2:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 3:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 4:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 5:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
            case 6:
                viewList.get(pos-1).setBackgroundResource(R.drawable.green_circle);
                break;
        }

    }
  */
/*  public void checkStepperProgress(int pos,ArrayList<View> viewList){
        StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
        if( StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().contains(pos-1)){
            updateStepperConnector(pos-1,viewList);
        }if ( StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().contains(pos+1)){
            updateStepperConnector(pos+1,viewList);
        }
    }*//*


    private void updateStepperConnector(int pos,ArrayList<View> viewList) {
        switch (pos) {
            case 1:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
            case 2:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
            case 3:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
            case 4:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
            case 5:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
            case 6:
                mProgressConnect.get(pos - 1).setBackgroundResource(R.color.color_green_barchart);
                break;
        }
    }

     public void getStepperViews(View mStepperViewParent) {

        viewList = new ArrayList<>();
         unitNameTextview = (TextView) mStepperViewParent.findViewById(R.id.stepperTitle);
         if(unitName !=null)
           unitNameTextview.setText(unitName);
         completedTasksTextView = (TextView)mStepperViewParent.findViewById(R.id.completed_task_count);
         totalTasksTextView = (TextView)mStepperViewParent.findViewById(R.id.total_task_count);
         completedTasksTextView.setText(String.valueOf(StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().size()));
         completedTaskCountLayout = (LinearLayout) mStepperViewParent.findViewById(R.id.completed_task_count_layout);
         totalTasksTextView.setText(String.valueOf(NightCheckEnum.values().length));
        viewList.add(mStepperViewParent.findViewById(R.id.task_strenght));
        viewList.add(mStepperViewParent.findViewById(R.id.task_guard_check));
        viewList.add(mStepperViewParent.findViewById(R.id.task_duty_check));
        viewList.add(mStepperViewParent.findViewById(R.id.task_client_check));
        viewList.add(mStepperViewParent.findViewById(R.id.task_security_check));
        viewList.add(mStepperViewParent.findViewById(R.id.client_handshake));
        viewList.add(mStepperViewParent.findViewById(R.id.equipment_check));
        mProgressConnect = new ArrayList<>();
        mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_one_completed));
        mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_two_completed));
       mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_three_completed));
        mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_four_completed));
        mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_five_completed));
        mProgressConnect.add(mStepperViewParent.findViewById(R.id.stepper_six_completed));

         HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
         if(null!=mStepperDataHolder && mStepperDataHolder.size()!=0){
             for (Map.Entry<String, Integer> entry : mStepperDataHolder.entrySet()) {
                 String key = entry.getKey();
                 Integer value = entry.getValue();
                 updateDayCheckUIStepper(value, null);
                 updateStepperConnector(value,mProgressConnect);
             }
         }
    }


    protected  void getTaskExecutionResult(){
        dayNightCheckingBaseMO = SingletonDayNightChecking.getInstance().getDayNightCheckData();
        if(dayNightCheckingBaseMO != null) {
            dayNightChecking = dayNightCheckingBaseMO.getDayNightChecking();
        }
    }

    protected  void setTaskExecutionResult(){
        SingletonDayNightChecking.getInstance().setDayNightCheckData(dayNightCheckingBaseMO);
    }
}
*/
