package com.sisindia.ai.android.commons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 03-08-2016.
 */

public class ImprovementActionPlanMO {


    private int issueMatrixId;
    private String name ;
    private int id;
    private int turnAroundTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTurnAroundTime() {
        return turnAroundTime;
    }

    public void setTurnAroundTime(int turnAroundTime) {
        this.turnAroundTime = turnAroundTime;
    }
    public int getIssueMatrixId() {
        return issueMatrixId;
    }

    public void setIssueMatrixId(int issueMatrixId) {
        this.issueMatrixId = issueMatrixId;
    }



    @Override
    public String toString() {
        return "ImprovementActionPlanMO{" +
                "issueMatrixId=" + issueMatrixId +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", turnAroundTime=" + turnAroundTime +
                '}';
    }
}
