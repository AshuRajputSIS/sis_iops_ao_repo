package com.sisindia.ai.android.issues.complaints;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.rota.models.LookupModel;

import java.util.ArrayList;

/**
 * Created by Shushrut on 01-08-2016.
 */
public class ComplaintBySpinnerAdapter extends ArrayAdapter<LookupModel> {
    Context context;
    int resource;
    LayoutInflater inflator;
    ArrayList<ComplaintInfoMO> mData;
    public ComplaintBySpinnerAdapter(Context context, int resource, ArrayList<ComplaintInfoMO> mData) {
        super(context, resource);
        this.mData = mData;
        inflator = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position,convertView,parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/

        View row = inflator.inflate(R.layout.spinner_row, parent, false);
        TextView mTextView = (TextView)row.findViewById(R.id.spinner_row_text);
        mTextView.setText(mData.get(position).getmFirstName() +" "+mData.get(position).getmLastName());

        return row;
    }

    @Override
    public int getCount() {
        return mData.size();
    }
}
