package com.sisindia.ai.android.settings;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.ImageSyncingRecylerAdapter;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.syncadpter.UnSyncedRecords;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.GridSpacingItemDecoration;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ManualImageSyncing extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private Resources mResources;
    private ArrayList<String> pathList;
    private ImageSyncingRecylerAdapter mAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_image_syncing);
        ButterKnife.bind(this);
        mResources = getResources();
        context = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
    }

    private void initViews() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.gallery_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ImageSyncingRecylerAdapter(getApplicationContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(this, R.dimen.dimen_5));
        setAdapter();
    }

    private void setAdapter() {
        getImagesPath();
        if (mAdapter != null && pathList.size() != 0) {
            mAdapter.setData(pathList);
        } else if (pathList.size() == 0) {
            Util.showToast(this, mResources.getString(R.string.no_image_to_sync));
        }
    }

    private void getImagesPath() {

        pathList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.FILE_PATH + " from " +
                TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where " +
                TableNameAndColumnStatement.IS_FILE_Upload + " ='" + 0 + "'";
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        pathList.add(cursor.getString(cursor.getColumnIndex(
                                TableNameAndColumnStatement.FILE_PATH)));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync, menu);
        String syncRequestDateTime = IOPSApplication.getInstance().getDutyAttendanceDB().getLatestSyncRequestTime();
        if (!TextUtils.isEmpty(syncRequestDateTime)) {
            if (new DateTimeFormatConversionUtil().getTimeDifference(syncRequestDateTime)) {
                menu.getItem(0).setVisible(true);
            } else {
                menu.getItem(0).setVisible(false);
            }
        } else {
            menu.getItem(0).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.sync) {
            if (pathList.size() != 0) {
                UnSyncedRecords unSyncedRecords = new UnSyncedRecords(this);
                unSyncedRecords.uploadUnSyncedIssues();
                unSyncedRecords.uploadUnSyncedImages();
                unSyncedRecords.uploadUnsyncedStrength();
                unSyncedRecords.uploadUnsyncedImprovementPlans();
                unSyncedRecords.uploadUnsyncedKitRequests();
                Bundle bundle = new Bundle();
                IOPSApplication.getInstance().getDutyAttendanceDB().insertIntoSyncRequest(new DateTimeFormatConversionUtil().getCurrentDateTime());
                IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
                item.setVisible(false);
                finish();
            } else {
                Util.showToast(this, mResources.getString(R.string.no_image_to_sync));
            }
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
