package com.sisindia.ai.android.rota.nightcheck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.daycheck.DayCheckEnum;
import com.sisindia.ai.android.rota.daycheck.OnStepperUpdateListener;
import com.sisindia.ai.android.rota.daycheck.SecurityRiskAdapter;
import com.sisindia.ai.android.rota.daycheck.SecurityRiskHelper;
import com.sisindia.ai.android.rota.daycheck.taskExecution.SecurityRisk;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskNightCheckActivity extends DayCheckBaseActivity implements DialogClickListener {
    @Bind(R.id.toolbar)
    Toolbar mToolBar;
    @Bind(R.id.risktTitle)
    TextView mRisktTitle;
    @Bind(R.id.security_risk_recyclerview)
    RecyclerView mSecurityRiskRecyclerview;
    SecurityRiskAdapter mSecurityRiskAdapter;
    @Bind(R.id.risk_stepper)
    View mStepperViewParent;
    @Bind(R.id.task_client_check)
    View stepperFourView;
    @Bind(R.id.task_security_check)
    View stepperFiveView;
    @Bind(R.id.stepper_four_completed)
    View stepperFourViewCompleted;
    @Bind(R.id.stepper_three_completed)
    View stepperThreeViewCompleted;
    @Bind(R.id.framelayout_stepper_four)
    FrameLayout framelayout_stepper_four;
    @Bind(R.id.framelayout_stepper_five)
    FrameLayout framelayout_stepper_five;
    @Bind(R.id.stepper_frame_six)
    FrameLayout mstepper_frame_six;
    @Bind(R.id.stepper_five_completed)
    View mstepper_five_completed;
    String data;
    private ArrayList<SecurityRiskModelMO> modelObject;
    private ArrayList<SecurityRiskModelMO> mValidatonArrayList;
    private ArrayList<String> mDataTitle;
    private ArrayList<AttachmentMetaDataModel> attachmentMetaDataModelList = new ArrayList<>();
    private List<SecurityRisk> securityRiskList;
    private ArrayList<Integer> mMetaDataItemPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.security_activity);
        ButterKnife.bind(this);

        mRisktTitle.setText(getResources().getString(R.string.security_risk));
        setBaseToolbar(mToolBar, getResources().getString(R.string.nightCheckingTitle));

        modelObject = new ArrayList<>();
        mDataTitle = new ArrayList<>();
        mValidatonArrayList = new ArrayList<>();
        if(rotaTaskModel != null) {
            setHeaderUnitName(rotaTaskModel.getUnitName());
            //unitGuardCount = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getUnitGuardsCount(rotaTaskModel.getUnitId());
        }
        ArrayList<SecurityRiskModelMO> modelobj = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getSecurityQuestionOption();
        attachmentMetaDataModelList = new ArrayList<>(modelobj.size());
        SecurityRiskHelper.getInstance().setDefaultAnswers(modelobj);

        Util.setmSecurityRiskModelHolder(modelobj);
        modelObject = modelobj;
        initAdapterData();
        getStepperViews(mStepperViewParent);
        getTaskExecutionResult();
        data = getResources().getString(R.string.dayCheckingTitle);
        securityRiskList = new ArrayList<>();
        if (getIntent().hasExtra(getResources().getString(R.string.navigationcheckingTitle))) {
            data = getIntent().getExtras().getString(getResources().getString(R.string.navigationcheckingTitle), getResources().getString(R.string.dayCheckingTitle));

            if (data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                //setBaseToolbar(toolbar, data);
                stepperFourView.setVisibility(View.GONE);
                stepperFourViewCompleted.setVisibility(View.GONE);
                stepperThreeViewCompleted.setVisibility(View.GONE);
                framelayout_stepper_four.setVisibility(View.GONE);
                framelayout_stepper_five.setVisibility(View.GONE);
                mstepper_frame_six.setVisibility(View.GONE);
                stepperFiveView.setVisibility(View.GONE);
                mstepper_five_completed.setVisibility(View.GONE);
            }
        }
        mMetaDataItemPosition = new ArrayList<>();
    }

    /**
     * method used to initialize the risk obervation Adapter.
     */
    private void initAdapterData() {
        mSecurityRiskAdapter = new SecurityRiskAdapter(this, modelObject);
        mSecurityRiskRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mSecurityRiskRecyclerview.setAdapter(mSecurityRiskAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_day_check, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.daycheck_done:

                if (SecurityRiskHelper.getInstance().validateAnswers()) {
                    insertMetaData();
                    securityRiskList =  SecurityRiskHelper.getInstance().getSecurityRiskData();
                    dayNightChecking.setSecurityRisks(securityRiskList);
                    dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
                    setTaskExecutionResult();
                    OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();
                    if (data != null && data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                        mOnStepperUpdateListener.updateStepper(NightCheckEnum.valueOf(3).name(),NightCheckEnum.Security_Risk_Observations.getValue());
                    } else {
                        mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(5).name(),DayCheckEnum.Security_Risk_Observations.getValue());
                    }
                    attachmentMetaDataModelList.clear();
                    finish();
                } else {
                    snackBarWithMesg(mRisktTitle, "Some mandatory field is missing.");
                }
                break;
            case android.R.id.home:
                showConfirmationDialog(getResources().getInteger(R.integer.DAY_NIGHT_CHECK_CONFIRAMTION_DIALOG));
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        showConfirmationDialog(getResources().getInteger(R.integer.DAY_NIGHT_CHECK_CONFIRAMTION_DIALOG));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            AttachmentMetaDataModel model = data.getParcelableExtra(Constants.META_DATA);
            modelObject.get(requestCode).setPictureUri(model.getAttachmentPath());
            modelObject.get(requestCode).setDataFilled(true);
            modelObject.get(requestCode).setPictureCapturePos(requestCode);
            //attachmentMetaDataModelList.add(requestCode, model);
            if(mMetaDataItemPosition.contains(requestCode)){
                mMetaDataItemPosition.remove(requestCode);
            }
            if (!mMetaDataItemPosition.contains(requestCode)) {
                attachmentMetaDataModelList.add(model);
                mMetaDataItemPosition.add(requestCode);

            }
            mSecurityRiskAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertMetaData() {
        for (AttachmentMetaDataModel model : attachmentMetaDataModelList) {
            if (model != null) {
                setAttachmentMetaDataModel(model);
            }

        }
        finish();
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        if(clickType == 0){

            if (data != null && data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(3).name(),NightCheckEnum.Security_Risk_Observations.getValue());

            } else {
                mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(5).name(),DayCheckEnum.Security_Risk_Observations.getValue());
            }

            finish();
        }
        else{
            finish();
        }

    }
}
