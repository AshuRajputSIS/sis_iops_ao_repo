package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.annualkitreplacement.KitItemModel;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.issues.models.GrievanceMO;
import com.sisindia.ai.android.loadconfiguration.Areas;
import com.sisindia.ai.android.loadconfiguration.BarrackInspectionOptionMO;
import com.sisindia.ai.android.loadconfiguration.BarrackInspectionQuestionMO;
import com.sisindia.ai.android.loadconfiguration.Branch;
import com.sisindia.ai.android.loadconfiguration.EmployeeLeaveCalendar;
import com.sisindia.ai.android.loadconfiguration.Equipments;
import com.sisindia.ai.android.loadconfiguration.HolidayCalendar;
import com.sisindia.ai.android.loadconfiguration.IssueMatrixModel;
import com.sisindia.ai.android.loadconfiguration.KitApparelSizeOR;
import com.sisindia.ai.android.loadconfiguration.KitDistributionItemOR;
import com.sisindia.ai.android.loadconfiguration.KitDistributionOR;
import com.sisindia.ai.android.loadconfiguration.KitItemSizeOR;
import com.sisindia.ai.android.loadconfiguration.MasterActionPlanModel;
import com.sisindia.ai.android.loadconfiguration.RankMaster;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;
import com.sisindia.ai.android.loadconfiguration.UserProfile;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.request.UnitEquipmentIMO;
import com.sisindia.ai.android.network.request.UnitTypeDataMO;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecuirtementMO;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionsData;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.SecurityRiskMO;
import com.sisindia.ai.android.rota.models.SecurityRiskQuestionOptionMappingMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.Collection;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 18/5/16.
 */
public class InsertionStatementDB extends SISAITrackingDB {
    //    private Util mutil;
    private boolean appUserInserted = false;
    private Context context;

    public InsertionStatementDB(Context context) {
        super(context);
        this.context = context;
//        mutil = new Util();
    }

    /**
     * @param userProfile
     */

    public boolean insertAppUserTable(UserProfile userProfile) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                if (!TextUtils.isEmpty(userProfile.getProfilePhotoUrl())) {
                    values.put(TableNameAndColumnStatement.AI_PHOTO, userProfile.getProfilePhotoUrl().trim());
                } else {
                    values.put(TableNameAndColumnStatement.AI_PHOTO, "");
                }
                if (!TextUtils.isEmpty(userProfile.getGeoLatitude())) {
                    values.put(TableNameAndColumnStatement.GEO_LATITUDE, userProfile.getGeoLatitude().trim());
                } else {
                    values.put(TableNameAndColumnStatement.GEO_LATITUDE, "");
                }
                if (!TextUtils.isEmpty(userProfile.getGeoLongitude())) {
                    values.put(TableNameAndColumnStatement.GEO_LONGITUDE, userProfile.getGeoLongitude().trim());
                } else {
                    values.put(TableNameAndColumnStatement.GEO_LONGITUDE, "");
                }
                values.put(TableNameAndColumnStatement.EMPLOYEE_PROFILE_PIC_ID, userProfile.getEmployeeProfilePicId());
                values.put(TableNameAndColumnStatement.MINIATURE_PROFILE_PIC_ID, userProfile.getMiniatureProfilePicId());
                values.put(TableNameAndColumnStatement.EMPLOYEE_ID, userProfile.getId());
                values.put(TableNameAndColumnStatement.FIRST_NAME, userProfile.getFirstName());
                values.put(TableNameAndColumnStatement.LAST_NAME, userProfile.getLastName());
                values.put(TableNameAndColumnStatement.MIDDLE_NAME, userProfile.getMiddleName());
                values.put(TableNameAndColumnStatement.EMPLOYEE_NO, userProfile.getEmployeeNo());
                values.put(TableNameAndColumnStatement.ORGANIZATION_ID, userProfile.getOrganizationId());
                values.put(TableNameAndColumnStatement.PRIMARY_ADDRESS_ID, userProfile.getPrimaryAddressId());
                values.put(TableNameAndColumnStatement.RESIDENTIAL_ADDRESS_ID, userProfile.getResidentialAddressId());
                values.put(TableNameAndColumnStatement.IS_ACTIVE, userProfile.getIsActive());
                values.put(TableNameAndColumnStatement.ADMIN_CONTROLLER_ID, userProfile.getAdminControllerId());
                values.put(TableNameAndColumnStatement.IS_REFERRAL, userProfile.getIsReferral());
                values.put(TableNameAndColumnStatement.REFERRAL_ID, userProfile.getReferralId());
                values.put(TableNameAndColumnStatement.REFERRAL_SOURCE, userProfile.getReferralSource());
                values.put(TableNameAndColumnStatement.PREFERRED_LANGUAGE_ID, userProfile.getLanguagePreferenceId());
                values.put(TableNameAndColumnStatement.COMPANY_CONTACT_NUMBER, userProfile.getCompanyContactNumber());
                values.put(TableNameAndColumnStatement.DATE_OF_BIRTH, userProfile.getDateOfBirth());
                values.put(TableNameAndColumnStatement.DATE_OF_JOINING, userProfile.getDateOfJoining());
                values.put(TableNameAndColumnStatement.FUNCTIONAL_CONTROLLER_ID, userProfile.getFunctionalControllerId());
                values.put(TableNameAndColumnStatement.CONTACT_NO, userProfile.getContactNo());
                values.put(TableNameAndColumnStatement.EMERGENCY_CONTACT_NO, userProfile.getEmergencyContactNo());
                values.put(TableNameAndColumnStatement.ALTERNATE_CONTACT_NO, userProfile.getAlternateContactNo());
                values.put(TableNameAndColumnStatement.BRANCH_ID, userProfile.getBranchId());
                values.put(TableNameAndColumnStatement.EMPLOYEE_SIGNATURE_ID, userProfile.getEmployeeSignatureId());
                values.put(TableNameAndColumnStatement.RANK_ID, userProfile.getRankId());
                values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, userProfile.getEmailAddress());
                values.put(TableNameAndColumnStatement.FULL_ADDRESS, userProfile.getFullAddress());
                values.put(TableNameAndColumnStatement.REGION_ID, userProfile.getRegionId());
                values.put(TableNameAndColumnStatement.IS_DEFAULT_PASSWORD, userProfile.getIsDefaultPassword());
                sqlite.insert(TableNameAndColumnStatement.APP_USER_TABLE, null, values);
                sqlite.setTransactionSuccessful();
                appUserInserted = true;
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }

        return appUserInserted;
    }

    /**
     * RankMaster Model Inserted
     *
     * @param rankMasterList
     */
    public void insertRankMasterTable(List<RankMaster> rankMasterList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                for (RankMaster rankMaster : rankMasterList) {
                    if (null != rankMaster) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.RANK_ID, rankMaster.getId());
                        values.put(TableNameAndColumnStatement.RANK_NAME, rankMaster.getRankName());
                        values.put(TableNameAndColumnStatement.RANK_ABBREVATION, rankMaster.getRankAbbrevation());
                        values.put(TableNameAndColumnStatement.DESCRIPTIONS, rankMaster.getDescription());
                        values.put(TableNameAndColumnStatement.IS_ACTIVE, rankMaster.getIsActive());
                        values.put(TableNameAndColumnStatement.CAN_POSSESS_ARMS, rankMaster.getCanPossessArms());
                        values.put(TableNameAndColumnStatement.RANK_CODE, rankMaster.getRankCode());
                        sqlite.insert(TableNameAndColumnStatement.RANK_MASTER_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param holidaycalendarList
     */
    public void insertHolidayCalendar(List<HolidayCalendar> holidaycalendarList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                for (HolidayCalendar holidayCalendar : holidaycalendarList) {
                    if (null != holidayCalendar) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.HOLIDAY_ID, holidayCalendar.getId());
                        values.put(TableNameAndColumnStatement.HOLIDAY_DATE, holidayCalendar.getHolidayDate());
                        values.put(TableNameAndColumnStatement.HOLIDAY_DESCRIPTION, holidayCalendar.getHolidayDescription());
                        values.put(TableNameAndColumnStatement.IS_REGIONAL_HOLIDAY,
                                holidayCalendar.getIsRegionalHoliday());
                        values.put(TableNameAndColumnStatement.IS_ACTIVE, holidayCalendar.getIsActive());
                        String[] createdStringDate = holidayCalendar.getHolidayDate().split(" ");
                        values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                        sqlite.insert(TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param barrackList
     */
    public void insertBarrackTable(List<Barrack> barrackList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (Barrack barrack : barrackList) {
                    values.put(TableNameAndColumnStatement.BARRACK_ID, barrack.getId());
                    values.put(TableNameAndColumnStatement.BARRACK_NAME, barrack.getName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, barrack.getDescription());
                    values.put(TableNameAndColumnStatement.IS_MESS_AVAILABLE, barrack.getMessAvailable());
                    values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, barrack.getMessVendorId());
                    values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, barrack.getCustomerProvided());
                    values.put(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS, barrack.getBarrackAddress());
                    values.put(TableNameAndColumnStatement.BARRACK_OWNER_NAME, barrack.getOwnerContactName());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, barrack.getAreaInspectorName());
                    values.put(TableNameAndColumnStatement.BARRACK_INCHARGE_NAME, barrack.getInChargeName());
                    values.put(TableNameAndColumnStatement.BRANCH_ID, barrack.getBranchId());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    values.put(TableNameAndColumnStatement.GEO_LATITUDE, barrack.getGeoLatitude());
                    values.put(TableNameAndColumnStatement.GEO_LONGITUDE, barrack.getGeoLongitude());
                    sqlite.insert(TableNameAndColumnStatement.BARRACK_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param branch
     */

    public void insertBranchTable(Branch branch) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.BRANCH_NAME, branch.getName());
                values.put(TableNameAndColumnStatement.BRANCH_ID, branch.getId());
                values.put(TableNameAndColumnStatement.BRANCH_CODE, branch.getBranchCode());
                values.put(TableNameAndColumnStatement.BRANCH_FULL_ADDRESS, branch.getBranchAddress());
                values.put(TableNameAndColumnStatement.GEO_LATITUDE, branch.getGeoLatitude());
                values.put(TableNameAndColumnStatement.GEO_LONGITUDE, branch.getGeoLongitude());
                sqlite.insert(TableNameAndColumnStatement.BRANCH_TABLE, null, values);
                sqlite.setTransactionSuccessful();

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param areasList
     */
    public void insertInAreaTable(List<Areas> areasList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (Areas areas : areasList) {
                    values.put(TableNameAndColumnStatement.AREA_ID, areas.getId());
                    values.put(TableNameAndColumnStatement.AREA_CODE, areas.getAreaCode());
                    values.put(TableNameAndColumnStatement.BRANCH_ID, areas.getBranchId());
                    values.put(TableNameAndColumnStatement.AREA_NAME, areas.getName());
                    sqlite.insert(TableNameAndColumnStatement.AREA_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }


    /**
     * Insert GPS value from Service for Every 30secs.
     *
     * @param gps_latitude
     * @param gps_longitude
     * @param battery_percentage
     * @param logged_in_date_time
     * @param is_sync_end
     * @param is_new
     * @param is_sync_start
     */


    public void insertInGPSTable(double gps_latitude, double gps_longitude,
                                 String battery_percentage,
                                 String logged_in_date_time, boolean is_sync_end,
                                 boolean is_new, boolean is_sync_start) {

        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.GPS_LATITUDE, gps_latitude);
                values.put(TableNameAndColumnStatement.GPS_LONGITUDE, gps_longitude);
                values.put(TableNameAndColumnStatement.BATTERY_PERCENTAGE, Integer.valueOf(battery_percentage));
                values.put(TableNameAndColumnStatement.LOGGED_IN_DATETIME, logged_in_date_time);
                values.put(TableNameAndColumnStatement.DISTANCE_TRAVELLED, is_sync_end);
                values.put(TableNameAndColumnStatement.DISTANCE_TRAVELLED_IN_TIME, is_new);
                sqlite.insert(TableNameAndColumnStatement.GPS_BATTERY_HEALTH_TABLE, null, values);
                sqlite.setTransactionSuccessful();

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                try {
                    sqlite.endTransaction();
                    if (sqlite != null && sqlite.isOpen()) {
                        sqlite.close();
                    }
                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }
            }
        }
    }


    /**
     * @param grievanceMO
     * @param isNew
     * @param isSynced
     */

    public void insertGrievanceDetails(GrievanceMO grievanceMO, int isNew, int isSynced) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.ISSUE_ID, grievanceMO.getIssueId());
                values.put(TableNameAndColumnStatement.UNIT_ID, grievanceMO.getUnitId());
                values.put(TableNameAndColumnStatement.BARRACK_ID, grievanceMO.getBarrackId());
                values.put(TableNameAndColumnStatement.GRIEVANCE_NATURE_ID, grievanceMO.getGrievanceNatureId());
                values.put(TableNameAndColumnStatement.DESCRIPTIONS, grievanceMO.getAdditionalRemarks());
                values.put(TableNameAndColumnStatement.EMPLOYEE_ID, grievanceMO.getEmployeeId());
                values.put(TableNameAndColumnStatement.ASSIGNED_TO, grievanceMO.getAssignedTo()); // need to get employee ID of AI from last inspection screen.
                values.put(TableNameAndColumnStatement.GRIEVANCE_STATUS_ID, grievanceMO.getGrievanceStatusId());
                values.put(TableNameAndColumnStatement.RAISED_BY, grievanceMO.getAssignedTo());
                DateTimeFormatConversionUtil dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
                values.put(TableNameAndColumnStatement.CREATED_AT, dateTimeFormatConversionUtil.getCurrentDateTime());
                values.put(TableNameAndColumnStatement.CLOSED_AT, "");
                values.put(TableNameAndColumnStatement.IS_NEW, isNew);
                values.put(TableNameAndColumnStatement.IS_SYNCED, isSynced);
                values.put(TableNameAndColumnStatement.TASK_ID, grievanceMO.getTaskId());
                sqlite.insert(TableNameAndColumnStatement.GRIEVANCE_TABLE, null, values);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    /**
     * @param attachmentMetaDataModel
     */
    public void insertIntoAttachmentDetails(AttachmentMetaDataModel attachmentMetaDataModel) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.UNIT_ID, attachmentMetaDataModel.getUnitId());
                values.put(TableNameAndColumnStatement.TASK_ID, attachmentMetaDataModel.getTaskId());
                values.put(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID, attachmentMetaDataModel.getAttachmentSourceId());
                values.put(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE, attachmentMetaDataModel.getAttachmentSourceTypeId());
                values.put(TableNameAndColumnStatement.FILE_NAME, attachmentMetaDataModel.getAttachmentFileName());
                values.put(TableNameAndColumnStatement.FILE_EXTENSION, attachmentMetaDataModel.getAttachmentFileExtension());
                values.put(TableNameAndColumnStatement.FILE_SIZE, attachmentMetaDataModel.getAttachmentFileSize());
                values.put(TableNameAndColumnStatement.IS_NEW, true);
                values.put(TableNameAndColumnStatement.IS_SYNCED, false);
                values.put(TableNameAndColumnStatement.SEQUENCE_NO, attachmentMetaDataModel.getSequenceNo());
                values.put(TableNameAndColumnStatement.FILE_PATH, attachmentMetaDataModel.getAttachmentPath());
                values.put(TableNameAndColumnStatement.ATTACHMENT_TYPE_ID, attachmentMetaDataModel.getAttachmentTypeId());
                values.put(TableNameAndColumnStatement.IS_FILE_Upload, 0);
                values.put(TableNameAndColumnStatement.IS_SAVE, attachmentMetaDataModel.getIsSave());
                values.put(TableNameAndColumnStatement.ATTACHMENT_ID, attachmentMetaDataModel.getAttachmentId());
                sqlite.insert(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, null, values);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                Timber.d("ATTACHMENT_METADATA_TABLE %s", attachmentMetaDataModel);
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param unitEquipment
     * @param isNew
     * @param isSynced
     */
    public void addUnitEquipment(UnitEquipmentIMO unitEquipment, boolean isNew, boolean isSynced) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitEquipment != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID, 0);
                    values.put(TableNameAndColumnStatement.UNIQUE_NO, unitEquipment.getEquipmentUniqueNo());
                    values.put(TableNameAndColumnStatement.MANUFACTURER, unitEquipment.getEquipmentManufacturer());
                    values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, unitEquipment.getIsCustomerProvided());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS, unitEquipment.getStatus());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE, unitEquipment.getEquipmentId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitEquipment.getUnitId());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_COUNT, unitEquipment.getQuantity());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitEquipment.getPostId());
                    values.put(TableNameAndColumnStatement.IS_NEW, isNew);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, isSynced);
                    sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                } finally {
                    if (null != sqlite && sqlite.isOpen()) {
                        sqlite.close();
                    }
                }
            }
        }
    }

    public void insertMetaDataTable(List<AttachmentMetaDataModel> metaDataModelList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (AttachmentMetaDataModel attachmentMetaDataModel : metaDataModelList) {
                    if (attachmentMetaDataModel != null) {
                        values.put(TableNameAndColumnStatement.UNIT_ID, attachmentMetaDataModel.getUnitId());
                        values.put(TableNameAndColumnStatement.TASK_ID, attachmentMetaDataModel.getTaskId());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID, attachmentMetaDataModel.getAttachmentSourceId());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE, attachmentMetaDataModel.getAttachmentSourceTypeId());
                        values.put(TableNameAndColumnStatement.FILE_NAME, attachmentMetaDataModel.getAttachmentFileName());
                        values.put(TableNameAndColumnStatement.FILE_EXTENSION, attachmentMetaDataModel.getAttachmentFileExtension());
                        values.put(TableNameAndColumnStatement.FILE_SIZE, attachmentMetaDataModel.getAttachmentFileSize());
                        values.put(TableNameAndColumnStatement.IS_NEW, true);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, false);
                        values.put(TableNameAndColumnStatement.SEQUENCE_NO, attachmentMetaDataModel.getSequenceNo());
                        values.put(TableNameAndColumnStatement.FILE_PATH, attachmentMetaDataModel.getAttachmentPath());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_SOURCE_CODE, attachmentMetaDataModel.getAttachmentSourceCode());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_TYPE_ID, attachmentMetaDataModel.getAttachmentTypeId()); // picture
                        values.put(TableNameAndColumnStatement.IS_FILE_Upload, 0);
                        values.put(TableNameAndColumnStatement.ATTACHMENT_ID, attachmentMetaDataModel.getAttachmentId());
                        sqlite.insert(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, null, values);
                    }

                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {

                    sqlite.close();
                }
                Timber.d("metaDataModelList metaDataModelList  %s ", IOPSApplication.getGsonInstance().toJson(metaDataModelList));
            }
        }
    }


    public void insertMetaDataTableFromHashMap(Collection<AttachmentMetaDataModel> metaDataModelList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (AttachmentMetaDataModel attachmentMetaDataModel : metaDataModelList
                        ) {
                    if (attachmentMetaDataModel != null) {
                        values.put(TableNameAndColumnStatement.UNIT_ID, attachmentMetaDataModel.getUnitId());
                        values.put(TableNameAndColumnStatement.TASK_ID, attachmentMetaDataModel.getTaskId());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID, attachmentMetaDataModel.getAttachmentSourceId());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE, attachmentMetaDataModel.getAttachmentSourceTypeId());
                        values.put(TableNameAndColumnStatement.FILE_NAME, attachmentMetaDataModel.getAttachmentFileName());
                        values.put(TableNameAndColumnStatement.FILE_EXTENSION, attachmentMetaDataModel.getAttachmentFileExtension());
                        values.put(TableNameAndColumnStatement.FILE_SIZE, attachmentMetaDataModel.getAttachmentFileSize());
                        values.put(TableNameAndColumnStatement.IS_NEW, true);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, false);
                        values.put(TableNameAndColumnStatement.SEQUENCE_NO, attachmentMetaDataModel.getSequenceNo());
                        values.put(TableNameAndColumnStatement.FILE_PATH, attachmentMetaDataModel.getAttachmentPath());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_SOURCE_CODE, attachmentMetaDataModel.getAttachmentSourceCode());
                        values.put(TableNameAndColumnStatement.ATTACHMENT_TYPE_ID, attachmentMetaDataModel.getAttachmentTypeId()); // picture
                        values.put(TableNameAndColumnStatement.IS_FILE_Upload, 0);
                        values.put(TableNameAndColumnStatement.ATTACHMENT_ID, attachmentMetaDataModel.getAttachmentId());
                        sqlite.insert(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, null, values);
                    }

                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {

                    sqlite.close();
                }
                Timber.d("metaDataModelList metaDataModelList  %s ", IOPSApplication.getGsonInstance().toJson(metaDataModelList));
            }
        }
    }

    public void insertSecurityRiskQuestionToTable(List<SecurityRiskMO> mSecurityRiskQuestion) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (SecurityRiskMO attachmentMetaDataModel : mSecurityRiskQuestion) {
                    values.put(TableNameAndColumnStatement.SECURITY_QUESTION_ID, attachmentMetaDataModel.getId());
                    values.put(TableNameAndColumnStatement.SECURITY_QUESTIONS, attachmentMetaDataModel.getQuestion());
                    values.put(TableNameAndColumnStatement.SECURITY_QUESTION_DESCRIPTION, attachmentMetaDataModel.getDescription());
                    sqlite.insert(TableNameAndColumnStatement.SECURITY_RISK_QUESTION_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertSecurityRiskQuestionOptionsToTable(List<SecurityRiskQuestionOptionMappingMO> mSecurityRiskQuestionMappingModel) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (SecurityRiskQuestionOptionMappingMO attachmentMetaDataModel : mSecurityRiskQuestionMappingModel) {
                    values.put(TableNameAndColumnStatement.SECURITY_QUESTION_ID, attachmentMetaDataModel.getId());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID, attachmentMetaDataModel.getSecurityRiskQuestionId());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_OPTION_ID, attachmentMetaDataModel.getSecurityRiskOptionId());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_OPTION_NAME, attachmentMetaDataModel.getSecurityRiskOptionName());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_IS_MANDATORY, attachmentMetaDataModel.getIsMandatory());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_CONTROL_TYPE, attachmentMetaDataModel.getControlType());
                    values.put(TableNameAndColumnStatement.SECURITY_RISK_IS_ACTIVE, attachmentMetaDataModel.getIsActive());
                    sqlite.insert(TableNameAndColumnStatement.SECURITY_RISK_QUESTION_OPTIONS_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }


    public void insertBillCollactionApiTable(List<UnitMonthlyBillDetail> billCollactionApiMOList) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        try {
            IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTable(TableNameAndColumnStatement.UnitBillingStatusTable, sqlite);

            ContentValues values = new ContentValues();
            for (int i = 0; i < billCollactionApiMOList.size(); i++) {
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_ID, billCollactionApiMOList.get(i).getId());
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID, billCollactionApiMOList.get(i).getUnitId());
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_NAME, billCollactionApiMOList.get(i).getUnitName());
                values.put(TableNameAndColumnStatement.Bill_NUMBER, billCollactionApiMOList.get(i).getBillNo());
                values.put(TableNameAndColumnStatement.Bill_MONTH, billCollactionApiMOList.get(i).getBillMonth());
                values.put(TableNameAndColumnStatement.Bill_YEAR, billCollactionApiMOList.get(i).getBillYear());
                values.put(TableNameAndColumnStatement.OUTSTANDING_AMOUNT, billCollactionApiMOList.get(i).getOutstandingAmount());
                values.put(TableNameAndColumnStatement.OUTSTANDING_DAYS, billCollactionApiMOList.get(i).getOutstandingDays());
                values.put(TableNameAndColumnStatement.UPDATED_DATES, billCollactionApiMOList.get(i).getUpdatedDate());
                values.put(TableNameAndColumnStatement.IS_BILL_COLLECTED, false);
                sqlite.insertOrThrow(TableNameAndColumnStatement.UnitBillingStatusTable, null, values);
            }
            sqlite.setTransactionSuccessful();
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            try {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }

    public void insertLookUpDataToTable(List<LookupModel> mLookUpModelData) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (LookupModel lookupModel : mLookUpModelData) {
                    values.put(TableNameAndColumnStatement.ID, lookupModel.getId());
                    values.put(TableNameAndColumnStatement.LOOKUP_CODE, lookupModel.getLookupCode());
                    values.put(TableNameAndColumnStatement.LOOKUP_TYPE_ID, lookupModel.getLookupTypeId());
                    values.put(TableNameAndColumnStatement.LOOKUP_IDENTIFIER, lookupModel.getLookupIdentifier());
                    values.put(TableNameAndColumnStatement.LOOKUP_NAME, lookupModel.getName());
                    values.put(TableNameAndColumnStatement.LOOKUP_TYPE_NAME, lookupModel.getLookupTypeName());
                    sqlite.insert(TableNameAndColumnStatement.LOOKUP_MODEL_TABLE, null, values);

                    /*try {
                        if (lookupModel.getId() == 326) {
                            if (lookupModel.getActive() != null) {
                                appPreferences.saveForcedGrievanceFlag(lookupModel.getActive());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }*/

                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertActionPlanToTable(List<MasterActionPlanModel> mActionPlanModelList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (MasterActionPlanModel masterActionPlanModel : mActionPlanModelList) {
                    values.put(TableNameAndColumnStatement.ID, masterActionPlanModel.getId());
                    values.put(TableNameAndColumnStatement.NAME, masterActionPlanModel.getActionPlanName());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, masterActionPlanModel.getIsActive());
                    sqlite.insert(TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public boolean insertOrUpdateActionPlanToTable(List<MasterActionPlanModel> mActionPlanModelList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        boolean isInsertedSuccessfully = true;
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (MasterActionPlanModel masterActionPlanModel : mActionPlanModelList) {

                    if (!checkIfRecordExists(sqlite, masterActionPlanModel.getId(), TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE)) {
                        values.put(TableNameAndColumnStatement.ID, masterActionPlanModel.getId());
                        values.put(TableNameAndColumnStatement.NAME, masterActionPlanModel.getActionPlanName());
                        values.put(TableNameAndColumnStatement.IS_ACTIVE, masterActionPlanModel.getIsActive());
                        sqlite.insert(TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return isInsertedSuccessfully;
    }

    public void insertIssueMatrixTable(List<IssueMatrixModel> issueMatrixList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (IssueMatrixModel issueMatrixModel : issueMatrixList) {
                    values.put(TableNameAndColumnStatement.ID, issueMatrixModel.getId());
                    values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issueMatrixModel.getIssueTypeId());
                    values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issueMatrixModel.getIssueCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID, issueMatrixModel.getIssueSubCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issueMatrixModel.getIssueCauseId());
                    values.put(TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID, issueMatrixModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.DEFAULT_ASSIGNMENNT_ROLE_ID, issueMatrixModel.getDefaultAssignmentRoleId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, issueMatrixModel.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.TURN_AROUND_TIME, issueMatrixModel.getTurnAroundTime());
                    values.put(TableNameAndColumnStatement.PROOF_CONTROL_TYPE, issueMatrixModel.getProofControlType());
                    values.put(TableNameAndColumnStatement.MINIMUM_OPTIONAL_VALUE, issueMatrixModel.getMinimumOptionalValue());
                    sqlite.insert(TableNameAndColumnStatement.ISSUE_MATRIX_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public boolean insertOrUpdateIssueMatrixTable(List<IssueMatrixModel> issueMatrixList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        boolean insertStatus = true;
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (IssueMatrixModel issueMatrixModel : issueMatrixList) {

                    if (!checkIfRecordExists(sqlite, issueMatrixModel.getId(), TableNameAndColumnStatement.ISSUE_MATRIX_TABLE)) {

                        values.put(TableNameAndColumnStatement.ID, issueMatrixModel.getId());
                        values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issueMatrixModel.getIssueTypeId());
                        values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issueMatrixModel.getIssueCategoryId());
                        values.put(TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID, issueMatrixModel.getIssueSubCategoryId());
                        values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issueMatrixModel.getIssueCauseId());
                        values.put(TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID, issueMatrixModel.getMasterActionPlanId());
                        values.put(TableNameAndColumnStatement.DEFAULT_ASSIGNMENNT_ROLE_ID, issueMatrixModel.getDefaultAssignmentRoleId());
                        values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, issueMatrixModel.getUnitTypeId());
                        values.put(TableNameAndColumnStatement.TURN_AROUND_TIME, issueMatrixModel.getTurnAroundTime());
                        values.put(TableNameAndColumnStatement.PROOF_CONTROL_TYPE, issueMatrixModel.getProofControlType());
                        values.put(TableNameAndColumnStatement.MINIMUM_OPTIONAL_VALUE, issueMatrixModel.getMinimumOptionalValue());
                        sqlite.insert(TableNameAndColumnStatement.ISSUE_MATRIX_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                insertStatus = false;
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return insertStatus;
    }

    private boolean checkIfRecordExists(SQLiteDatabase sqlite, int id, String tableName) {

        try {
            synchronized (sqlite) {
                String selectQuery = "select count(1) as issueCount from " + tableName + " where id='" + id + "'";
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int count = cursor.getInt(cursor.getColumnIndex("issueCount"));
                    if (count > 0)
                        return true;
                    else
                        return false;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return false;
    }

    public boolean insertIntoClientCoordinationTable(List<QuestionsData> clientCoordinationQuestions) {
        boolean isCCRQuestionsUpdatedSuccessfully = true;
        SQLiteDatabase sqlite = getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            ContentValues cv = new ContentValues();
            try {
                for (QuestionsData data : clientCoordinationQuestions) {
                    cv.put(TableNameAndColumnStatement.question_id, data.questionId);
                    cv.put(TableNameAndColumnStatement.question, data.question);
                    cv.put(TableNameAndColumnStatement.is_active, data.isActive);
                    sqlite.insert(TableNameAndColumnStatement.client_coordination_table, null, cv);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                isCCRQuestionsUpdatedSuccessfully = false;
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return isCCRQuestionsUpdatedSuccessfully;
    }

    public void insertBarrackInspectionQuestionTable(List<BarrackInspectionQuestionMO> barrackInspectionQuestionMOList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (BarrackInspectionQuestionMO barrackInspectionQuestionMO : barrackInspectionQuestionMOList) {
                    values.put(TableNameAndColumnStatement.QUESTION_ID, barrackInspectionQuestionMO.getId());
                    values.put(TableNameAndColumnStatement.QUESTION, barrackInspectionQuestionMO.getQuestion());
                    values.put(TableNameAndColumnStatement.DESCRIPTION, barrackInspectionQuestionMO.getDescription());
                    values.put(TableNameAndColumnStatement.CONTROL_TYPE, barrackInspectionQuestionMO.getControlType());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, barrackInspectionQuestionMO.isActive());
                    sqlite.insert(TableNameAndColumnStatement.BARRACK_INSPECTION_QUESTION_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertBarrackInspectionAnswerTable(List<BarrackInspectionOptionMO> barrackInspectionOptionMOList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (BarrackInspectionOptionMO barrackInspectionOptionMO : barrackInspectionOptionMOList) {
                    values.put(TableNameAndColumnStatement.QUESTION_ID, barrackInspectionOptionMO.getBarrackInspectionQuestionId());
                    values.put(TableNameAndColumnStatement.OPTION_ID, barrackInspectionOptionMO.getBarrackInspectionOptionId());
                    values.put(TableNameAndColumnStatement.OPTION_NAME, barrackInspectionOptionMO.getBarrackInspectionOptionName());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, barrackInspectionOptionMO.isActive());
                    values.put(TableNameAndColumnStatement.IS_MANDATORY, barrackInspectionOptionMO.isMandatory());
                    values.put(TableNameAndColumnStatement.CONTROL_TYPE, barrackInspectionOptionMO.getControlType());
                    sqlite.insert(TableNameAndColumnStatement.BARRACK_INSPECTION_ANSWER_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }


    }

    public void insertKitItemTable(List<KitItemModel> kitItemModelList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (KitItemModel kitItemModel : kitItemModelList) {
                    values.put(TableNameAndColumnStatement.KIT_ITEM_ID, kitItemModel.getId());
                    values.put(TableNameAndColumnStatement.KIT_ITEM_CODE, kitItemModel.getKitItemCode());
                    values.put(TableNameAndColumnStatement.KIT_ITEM_NAME, kitItemModel.getName());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, kitItemModel.IsActive());
                    sqlite.insert(TableNameAndColumnStatement.KIT_ITEM_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertLeaveTable(List<EmployeeLeaveCalendar> employeeLeaveCalendar) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                for (EmployeeLeaveCalendar employeeLeaveCalendarMO : employeeLeaveCalendar) {
                    if (null != employeeLeaveCalendarMO) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.EMPLOYEE_ID, employeeLeaveCalendarMO.getEmployeeId());
                        values.put(TableNameAndColumnStatement.LEAVE_DATE, employeeLeaveCalendarMO.getLeaveDate());
                        String[] createdStringDate = employeeLeaveCalendarMO.getLeaveDate().split(" ");
                        values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                        sqlite.insert(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertKitSizeTable(List<KitApparelSizeOR> kitApparelSizes) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (kitApparelSizes != null && kitApparelSizes.size() > 0) {
                    for (KitApparelSizeOR kitApparelSizeOR : kitApparelSizes) {
                        if (kitApparelSizeOR != null) {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.KIT_SIZE_ID, kitApparelSizeOR.getId());
                            values.put(TableNameAndColumnStatement.KIT_SIZE_CODE, kitApparelSizeOR.getSizeCode());
                            values.put(TableNameAndColumnStatement.KIT_SIZE_NAME, kitApparelSizeOR.getSizeName());
                            values.put(TableNameAndColumnStatement.KIT_SIZE_VALUE, kitApparelSizeOR.getSizeValue());
                            sqlite.insert(TableNameAndColumnStatement.KIT_SIZE_TABLE, null, values);
                        }
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertKitDistributonTable(List<KitDistributionOR> kitDistributionORList) {


        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (kitDistributionORList != null && kitDistributionORList.size() > 0) {
                    for (KitDistributionOR kitDistributionOR : kitDistributionORList) {

                        if (kitDistributionOR != null) {

                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionOR.getId());
                            values.put(TableNameAndColumnStatement.ISSUE_OFFICER_ID, kitDistributionOR.getIssuingOfficerId());
                            values.put(TableNameAndColumnStatement.GUARD_CODE, kitDistributionOR.getGuardNo());
                            values.put(TableNameAndColumnStatement.GUARD_NAME, kitDistributionOR.getGuardName());
                            values.put(TableNameAndColumnStatement.UNIT_ID, kitDistributionOR.getUnitId());
                            values.put(TableNameAndColumnStatement.DISTRIBUTION_STATUS, kitDistributionOR.getDistributionStatus());
                            values.put(TableNameAndColumnStatement.RECIPIENT_ID, kitDistributionOR.getRecipientId());
                            values.put(TableNameAndColumnStatement.KIT_TYPE_ID, kitDistributionOR.getKitTypeId());
                            values.put(TableNameAndColumnStatement.BRANCH_ID, kitDistributionOR.getBranchId());
                            values.put(TableNameAndColumnStatement.DISTRIBUTION_DATE, kitDistributionOR.getDistributionDate());
                            sqlite.insert(TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, null, values);
                        }

                    }
                }

                sqlite.setTransactionSuccessful();

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }


            }
        }
    }


    public void insertKitItemSizesTable(List<KitItemSizeOR> kitItemSizes) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (kitItemSizes != null && kitItemSizes.size() > 0) {
                    for (KitItemSizeOR kitItemSize : kitItemSizes) {
                        if (kitItemSize != null) {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.KIT_ITEM_SIZE_ID, kitItemSize.getId());
                            values.put(TableNameAndColumnStatement.KIT_ITEM_ID, kitItemSize.getKitItemId());
                            values.put(TableNameAndColumnStatement.KIT_SIZE_ID, kitItemSize.getKitApparelSizeId());
                            sqlite.insert(TableNameAndColumnStatement.KIT_ITEM_SIZE_TABLE, null, values);
                        }
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertKitDistributonItemTable(List<KitDistributionItemOR> kitDistributionORList, SQLiteDatabase sqLiteDatabase) {
        if (sqLiteDatabase == null) {
            sqLiteDatabase = this.getWritableDatabase();
        }
        synchronized (sqLiteDatabase) {

            try {
                // Inserting Row
                if (kitDistributionORList != null && kitDistributionORList.size() > 0) {
                    for (KitDistributionItemOR kitDistributionItemOR : kitDistributionORList) {
                        if (kitDistributionItemOR != null) {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionItemOR.getKitDistributionId());
                            values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID, kitDistributionItemOR.getId());
                            values.put(TableNameAndColumnStatement.IS_ISSUED, kitDistributionItemOR.getIsIssued());
                            values.put(TableNameAndColumnStatement.IS_UNPAID, kitDistributionItemOR.getIsUnPaid());
                            if (kitDistributionItemOR.getNonIssueReasonId() == null) {
                                values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID, 0);
                            } else {
                                values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID, kitDistributionItemOR.getNonIssueReasonId());
                            }

                            values.put(TableNameAndColumnStatement.ISSUED_DATE, kitDistributionItemOR.getIssuedDate());
                            values.put(TableNameAndColumnStatement.KIT_ITEM_ID, kitDistributionItemOR.getKitItemId());
                            values.put(TableNameAndColumnStatement.KIT_SIZE_ID, kitDistributionItemOR.getKitApparelSizeId());
                            values.put(TableNameAndColumnStatement.QUANTITY, kitDistributionItemOR.getQuantity());
                            values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                            sqLiteDatabase.insert(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE, null, values);
                        }
                    }
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }

    public void insertEquipmentListTable(List<Equipments> equipmentsList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (equipmentsList != null && equipmentsList.size() > 0) {
                    for (Equipments equipment : equipmentsList) {
                        if (equipment != null) {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.ID, equipment.getId());
                            values.put(TableNameAndColumnStatement.EQUIPMENT_NAME, equipment.getEquipmentName());
                            values.put(TableNameAndColumnStatement.EQUIPMENT_CODE, equipment.getEquipmentCode());
                            values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE_ID, equipment.getEquipmentTypeId());
                            values.put(TableNameAndColumnStatement.IS_ACTIVE, equipment.getIsActive());
                            values.put(TableNameAndColumnStatement.DESCRIPTION, equipment.getDescription());
                            sqlite.insert(TableNameAndColumnStatement.EQUIPMENT_LIST_TABLE, null, values);
                        }
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            } catch (SQLiteException sqliteException) {
                Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            } catch (Exception exception) {
                Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param unitEquipment
     */
    public void updateInsertUnitEquipment(SyncingUnitEquipmentModel unitEquipment) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitEquipment != null) {
            synchronized (sqlite) {
                try {
                    if (unitEquipment.getId() != null && unitEquipment.getId() != 0) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.EQUIPMENT_COUNT, unitEquipment.getAddEquipmentLocalMO().getEquipmentCount());
                        values.put(TableNameAndColumnStatement.IS_SYNCED, unitEquipment.getAddEquipmentLocalMO().getIsSynced());
                        sqlite.update(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, values, TableNameAndColumnStatement.ID + "=" + unitEquipment.getId(), null);
                    } else {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID, 0);
                        values.put(TableNameAndColumnStatement.UNIQUE_NO, unitEquipment.getEquipmentUniqueNo());
                        values.put(TableNameAndColumnStatement.MANUFACTURER, unitEquipment.getEquipmentManufacturer());
                        values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, unitEquipment.getIsCustomerProvided());
                        values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS, unitEquipment.getStatus());
                        values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE, unitEquipment.getAddEquipmentLocalMO().getEquipmentTypId());
                        values.put(TableNameAndColumnStatement.UNIT_ID, unitEquipment.getUnitId());
                        values.put(TableNameAndColumnStatement.EQUIPMENT_COUNT, unitEquipment.getAddEquipmentLocalMO().getEquipmentCount());
                        values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitEquipment.getPostId());
                        values.put(TableNameAndColumnStatement.IS_NEW, unitEquipment.getAddEquipmentLocalMO().getIsNew());
                        values.put(TableNameAndColumnStatement.IS_SYNCED, unitEquipment.getAddEquipmentLocalMO().getIsSynced());
                        sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                } finally {
                    if (null != sqlite && sqlite.isOpen()) {
                        sqlite.close();
                    }
                }
            }
        }
    }

    public void insertRecuirtmentListTable(List<RecuirtementMO> recuirtementMOList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (recuirtementMOList != null && recuirtementMOList.size() > 0) {
                    for (RecuirtementMO recuirtementMO : recuirtementMOList) {
                        if (recuirtementMO != null) {
                            ContentValues values = new ContentValues();
                            values.put(TableNameAndColumnStatement.RECRUIT_ID, recuirtementMO.getRecruitmentId());
                            values.put(TableNameAndColumnStatement.FULL_NAME, recuirtementMO.getFullName());
                            values.put(TableNameAndColumnStatement.CONTACT_NO, recuirtementMO.getContactNo());
                            values.put(TableNameAndColumnStatement.DATE_OF_BIRTH, recuirtementMO.getDOB());
                            values.put(TableNameAndColumnStatement.IS_SELECTED, recuirtementMO.getIsSelected());
                            values.put(TableNameAndColumnStatement.IS_REJECTED, recuirtementMO.getIsRejected());
                            values.put(TableNameAndColumnStatement.ACTION_TAKEN_ON, recuirtementMO.getActionTakenOn());
                            values.put(TableNameAndColumnStatement.STATUS, recuirtementMO.getStatus());
                            values.put(TableNameAndColumnStatement.IS_SYNCED, Constants.IS_SYNCED);
                            sqlite.insert(TableNameAndColumnStatement.RECRUITMENT_TABLE, null, values);
                        }
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            } catch (SQLiteException sqliteException) {
                Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            } catch (Exception exception) {
                Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void updateClientCoordinationIssueMatrixTable(List<IssueMatrixModel> issueMatrixList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (IssueMatrixModel issueMatrixModel : issueMatrixList) {
                    values.put(TableNameAndColumnStatement.ID, issueMatrixModel.getId());
                    values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issueMatrixModel.getIssueTypeId());
                    values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issueMatrixModel.getIssueCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID, issueMatrixModel.getIssueSubCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issueMatrixModel.getIssueCauseId());
                    values.put(TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID, issueMatrixModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.DEFAULT_ASSIGNMENNT_ROLE_ID, issueMatrixModel.getDefaultAssignmentRoleId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, issueMatrixModel.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.TURN_AROUND_TIME, issueMatrixModel.getTurnAroundTime());
                    values.put(TableNameAndColumnStatement.PROOF_CONTROL_TYPE, issueMatrixModel.getProofControlType());
                    values.put(TableNameAndColumnStatement.MINIMUM_OPTIONAL_VALUE, issueMatrixModel.getMinimumOptionalValue());
                    sqlite.insert(TableNameAndColumnStatement.ISSUE_MATRIX_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public boolean insertUnitTypeDetails(List<UnitTypeDataMO> unitTypeList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        boolean isInsertedSuccessfully = true;
        synchronized (sqlite) {
            try {
                if (unitTypeList != null && unitTypeList.size() > 0) {
                    for (UnitTypeDataMO model : unitTypeList) {
                        if (model != null) {
                            ContentValues values = new ContentValues();
                            values.put("UnitTypeId", model.getId());
                            values.put("Name", model.getName());
                            values.put("Description", model.getDescription());
                            values.put("MinGuardSize", model.getMinGuardSize());
                            values.put("MaxGuardSize", model.getMaxGuardSize());
                            values.put("CheckLimit", model.getCheckLimit());
                            sqlite.insert(TableNameAndColumnStatement.UNIT_TYPE_TABLE, null, values);
                        }
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteException sqliteException) {
                Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                isInsertedSuccessfully = false;
            } catch (Exception exception) {
                Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return isInsertedSuccessfully;
    }


}
