package com.sisindia.ai.android.rota.daycheck.taskExecution;

import com.sisindia.ai.android.commons.IOPSApplication;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 17-05-2016.
 */


public class SingletonDayNightChecking {

    private static SingletonDayNightChecking singletonDayNightChecking;
    private static DayNightCheckingBaseMO dayNightCheckingBaseMO;


    private SingletonDayNightChecking(){}

    public static synchronized SingletonDayNightChecking getInstance(){
        if(singletonDayNightChecking == null){
            singletonDayNightChecking = new SingletonDayNightChecking();
        }
        return singletonDayNightChecking;
    }

    public void setDayNightCheckData(DayNightCheckingBaseMO dayNightCheckingBase){

        dayNightCheckingBaseMO = dayNightCheckingBase;
        Timber.d("set Task Execution result %s", IOPSApplication.getGsonInstance().toJson(dayNightCheckingBaseMO));

    }

    public DayNightCheckingBaseMO getDayNightCheckData(){
        Timber.d("get Task Execution result %s", IOPSApplication.getGsonInstance().toJson(dayNightCheckingBaseMO));
        return dayNightCheckingBaseMO;
    }
}

