package com.sisindia.ai.android.myunits.models;

/**
 * Created by Saruchi on 20-05-2016.
 */
public class CheckIsMainGateMO {
    private int postid;
    private int isMaingate;

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public int getIsMaingate() {
        return isMaingate;
    }

    public void setIsMaingate(int isMaingate) {
        this.isMaingate = isMaingate;
    }

    @Override
    public String toString() {
        return "CheckIsMainGateMO{" +
                "postid=" + postid +
                ", isMaingate=" + isMaingate +
                '}';
    }
}
