package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.network.request.IssueIMO;

import java.util.List;

/**
 * Created by compass on 3/5/2017.
 */
public class IssuesAndImprovementPlan {
    @SerializedName("Issues")
    List<IssueIMO> issueList;
    @SerializedName("ImprovementPlans")
    List<ActionPlanMO> improvementList;
}
