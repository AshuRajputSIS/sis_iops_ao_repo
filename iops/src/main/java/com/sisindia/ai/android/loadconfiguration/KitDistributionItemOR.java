package com.sisindia.ai.android.loadconfiguration;


import com.google.gson.annotations.SerializedName;

public class KitDistributionItemOR {

@SerializedName("Id")

private Integer id;
@SerializedName("KitDistributionId")

private Integer kitDistributionId;
@SerializedName("IsIssued")

private Boolean isIssued;
@SerializedName("NonIssueReasonId")

private Integer nonIssueReasonId;
@SerializedName("IssuedDate")
private String issuedDate;

@SerializedName("IsUnPaid")
private Boolean isUnPaid;
    @SerializedName("KitItemId")
private Integer kitItemId;

@SerializedName("KitApparelSizeId")
private Integer kitApparelSizeId;
@SerializedName("Quantity")

private Integer quantity;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The kitDistributionId
*/
public Integer getKitDistributionId() {
return kitDistributionId;
}

/**
* 
* @param kitDistributionId
* The KitDistributionId
*/
public void setKitDistributionId(Integer kitDistributionId) {
this.kitDistributionId = kitDistributionId;
}

/**
* 
* @return
* The isIssued
*/
public Boolean getIsIssued() {
return isIssued;
}

/**
* 
* @param isIssued
* The IsIssued
*/
public void setIsIssued(Boolean isIssued) {
this.isIssued = isIssued;
}

/**
* 
* @return
* The nonIssueReasonId
*/
public Integer getNonIssueReasonId() {
return nonIssueReasonId;
}

/**
* 
* @param nonIssueReasonId
* The NonIssueReasonId
*/
public void setNonIssueReasonId(Integer nonIssueReasonId) {
this.nonIssueReasonId = nonIssueReasonId;
}

/**
* 
* @return
* The issuedDate
*/
public String getIssuedDate() {
return issuedDate;
}

/**
* 
* @param issuedDate
* The IssuedDate
*/
public void setIssuedDate(String issuedDate) {
this.issuedDate = issuedDate;
}

/**
* 
* @return
* The isUnPaid
*/
public Boolean getIsUnPaid() {
return isUnPaid;
}

/**
* 
* @param isUnPaid
* The IsUnPaid
*/
public void setIsUnPaid(Boolean isUnPaid) {
this.isUnPaid = isUnPaid;
}

/**
* 
* @return
* The kitItemId
*/
public Integer getKitItemId() {
return kitItemId;
}

/**
* 
* @param kitItemId
* The KitItemId
*/
public void setKitItemId(Integer kitItemId) {
this.kitItemId = kitItemId;
}

/**
* 
* @return
* The kitApparelSizeId
*/
public Integer getKitApparelSizeId() {
return kitApparelSizeId;
}

/**
* 
* @param kitApparelSizeId
* The KitApparelSizeId
*/
public void setKitApparelSizeId(Integer kitApparelSizeId) {
this.kitApparelSizeId = kitApparelSizeId;
}

/**
* 
* @return
* The quantity
*/
public Integer getQuantity() {
return quantity;
}

/**
* 
* @param quantity
* The Quantity
*/
public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

}