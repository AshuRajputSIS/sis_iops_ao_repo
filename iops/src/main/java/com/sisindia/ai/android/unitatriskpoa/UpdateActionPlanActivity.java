package com.sisindia.ai.android.unitatriskpoa;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by shankar on 18/7/16.
 */

public class UpdateActionPlanActivity extends BaseActivity {
    @Bind(R.id.toolbar_post)
    Toolbar mToolBar;
    @Bind(R.id.closure_date)
    CustomFontTextview closureDate;
    @Bind(R.id.additionalRemarks)
    TextView additionalRemarks;
    @Bind(R.id.editTextTypeRemark)
    EditText editTextTypeRemark;
    @Bind(R.id.Parentlayout)
    CoordinatorLayout Parentlayout;
    @Bind(R.id.unit_name)
    CustomFontTextview unitName;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ActionPlanMO actionPlanMO;
    private String unitNameStr="";
    private List<ActionPlanMO> completedActionPlanMOList;
    private List<ActionPlanMO> pendingActionPlanMOList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_date);
        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);
       Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            actionPlanMO = (ActionPlanMO) bundle.getSerializable("ActionPlanMO");
            unitNameStr = bundle.getString("UnitName");
            getSupportActionBar().setTitle("POA : "+actionPlanMO.getActionPlan());
        }
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        closureDate.setText(dateTimeFormatConversionUtil.convertDateToDayMonthDateYearFormat(dateTimeFormatConversionUtil.getCurrentDate()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        unitName.setText(unitNameStr);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_post_save:

                updateUnitRiskStatus();


                break;
            case android.R.id.home:
                Intent intent = new Intent(this, UARActionPlanActivity.class);
                intent.putExtra("UnitRiskId", actionPlanMO.getUnitRiskId());
                intent.putExtra("UnitName", unitNameStr);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUnitRiskStatus() {

        if (null == editTextTypeRemark.getText().toString().trim() || editTextTypeRemark.getText().toString().trim().isEmpty()) {

            snackBarWithMesg(Parentlayout, mActivity.getResources().getString(R.string.additionalRemarksFill));


        } else {

            /*completedActionPlanMOList = IOPSApplication.getInstance().
                    getUARSelectionInstance().getUARCompletedList(actionPlanMO.getUnitRiskId(), 1);*/



            IOPSApplication.getInstance().getUARUpdateInstance().
                    updateUnitRiskPOAStatus(editTextTypeRemark.getText().toString().trim(),
                            dateTimeFormatConversionUtil.getCurrentDateTime(),
                             actionPlanMO.getUnitRiskId(),
                            actionPlanMO.getActionPlanId(),
                            actionPlanMO.getId());

            pendingActionPlanMOList = IOPSApplication.getInstance().
                    getUARSelectionInstance().getUARPendingList(actionPlanMO.getUnitRiskId(), 0);

            if(pendingActionPlanMOList != null && pendingActionPlanMOList.size() == 0){
                IOPSApplication.getInstance().getUARUpdateInstance()
                        .updateUnitRiskCompletionDate(dateTimeFormatConversionUtil
                                .getCurrentDateTime(), actionPlanMO.getUnitRiskId());
            }


            Bundle bundel = new Bundle();
            IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);

            Intent intent = new Intent(this, UARActionPlanActivity.class);
            intent.putExtra("UnitRiskId", actionPlanMO.getUnitRiskId());
            intent.putExtra("UnitName", unitNameStr);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }


}

