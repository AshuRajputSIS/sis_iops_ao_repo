package com.sisindia.ai.android.network.response;

public class CommonResponse implements java.io.Serializable {
    private static final long serialVersionUID = -5647066686699171851L;
    private int StatusCode;
    private String StatusMessage;


    public int getStatusCode() {
        return this.StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return this.StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "StatusCode=" + StatusCode +
                ", StatusMessage='" + StatusMessage + '\'' +
                '}';
    }
}
