package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 18-06-2016.
 */
public class LastCheckInspectionUnitAtRiskMO {

    private String planOfAction;
    private String plannedClosureDate;
    private String assignedTo;

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private int    statusId;
    private String status;

    public String getStatus() {
        return status;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getPlannedClosureDate() {
        return plannedClosureDate;
    }

    public void setPlannedClosureDate(String plannedClosureDate) {
        this.plannedClosureDate = plannedClosureDate;
    }

    public String getPlanOfAction() {
        return planOfAction;
    }

    public void setPlanOfAction(String planOfAction) {
        this.planOfAction = planOfAction;
    }


}
