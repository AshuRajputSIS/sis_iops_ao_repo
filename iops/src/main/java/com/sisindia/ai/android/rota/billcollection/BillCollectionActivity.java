package com.sisindia.ai.android.rota.billcollection;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import org.joda.time.DateTime;

import java.io.File;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by shruti on 05/6/16.
 */
public class BillCollectionActivity extends DayCheckBaseActivity implements
        View.OnClickListener, RadioGroup.OnCheckedChangeListener, DialogClickListener, TextWatcher {

    public LinearLayout linearLayout;
    @Bind(R.id.parent_relative_layout)
    RelativeLayout parent_relative_layout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.unit_name_text)
    CustomFontTextview unit_name_text;


    @Bind(R.id.tv_select_collection_date)
    CustomFontTextview tv_select_collection_date;

    @Bind(R.id.tv_collection_bill_amount_value)
    CustomFontTextview tv_collection_bill_Amount_value;

    @Bind(R.id.spinner_collection_month)
    Spinner spinner_collection_month;

    @Bind(R.id.edittext_collection_amount)
    CustomFontEditText edittext_collection_amount;
    @Bind(R.id.bill_collection_radio_group)
    RadioGroup bill_collection_radio_group;
    @Bind(R.id.cheque)
    RadioButton cheque;
    @Bind(R.id.rtgs)
    RadioButton rtgs;
    @Bind(R.id.collection_check_layout)
    RelativeLayout collection_check_layout;
    @Bind(R.id.collection_rgts_detail)
    RelativeLayout collection_rgts_detail;
    @Bind(R.id.edittext_check_number)
    CustomFontEditText edittext_check_number;
    @Bind(R.id.edittext_rgts_remarks)
    CustomFontEditText edittext_rgts_remarks;
    @Bind(R.id.collection_month_layout)
    LinearLayout collectionMonthLayout;
    Context mContext;
    List<UnitMonthlyBillDetail> collectionMonthMOList;
    @Bind(R.id.collection_amount_lable)
    CustomFontTextview collectionAmountLable;
    private LinearLayout linearLayout_photos;
    private ImageView takePhoto;
    private ArrayList<String> mArrayImageHolder = new ArrayList<>();
    private int BILL_COLLECTION_IMAGE_CODE = 102;
    private RotaTaskModel rotaTaskModel;
    private String billCollectionMode = "cheque";
    private String taskExecutionResult = "";
    private BillCollectionTaskExeMO billCollectionTaskExeMO;
    private BillCollectionMO billCollectionMO;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private CollectionMonthAdapter adapter;
    private int pos = 0;
    private int totalBillAmount = 0;
    private HashMap<Integer, UnitMonthlyBillDetail> billCollectionMonthMap = new HashMap<Integer, UnitMonthlyBillDetail>();
    private int billId = 0;
    private UnitMonthlyBillDetail unitMonthlyBillCollection;
    private boolean isMonthlyBillListEmpty;
    private String selectedMonthForCollection;
    private int selectedYearForCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_collection);

        mContext = BillCollectionActivity.this;
        ButterKnife.bind(this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        util = new Util();
        setUpToolBar();
        rotaTaskModel = (RotaTaskModel) getIntent().getSerializableExtra("RotaModel");
        unit_name_text.setText(rotaTaskModel.getUnitName());

        tv_select_collection_date.setText(dateTimeFormatConversionUtil.getCurrentDateTime_billcollaction());

        initCollectionMonthSpinner();
        //spinner_collection_month.setOnItemSelectedListener(this);
        bill_collection_radio_group.setOnCheckedChangeListener(this);

        edittext_collection_amount.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        edittext_collection_amount.addTextChangedListener(this);

        linearLayout_photos = (LinearLayout) findViewById(R.id.linearLayout_photos);
        takePhoto = (ImageView) findViewById(R.id.takePhoto);
        collectionAmountLable.setText(util.createSpannableStringBuilder(getResources().getString(R.string.collection_Amount)));
        takePhoto.setOnClickListener(this);
    }

    private void initCollectionMonthSpinner() {


        collectionMonthMOList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getListOfCollectionMonths(rotaTaskModel.getUnitId());
        if (collectionMonthMOList != null && collectionMonthMOList.size() > 0) {
            //     adapter = new CollectionMonthAdapter(mContext, collectionMonthMOList);
            //spinner_collection_month.setAdapter(adapter);
            //  adapter.notifyDataSetChanged();

            createDynamicView(collectionMonthMOList);


           /* String dueDate = getDuedate_billcollaction(tv_select_collection_date.getText().toString(),
                    collectionMonthMOList.get(0).getOutstandingDays());
            dueDateTxt.setText(dueDate + "");*/

            // int amount = collectionMonthMOList.get(0).getOutstandingAmount();
            String yourFormattedString = Util.getCommaSeparatedAmount(totalBillAmount);
            tv_collection_bill_Amount_value.setText(getResources().getString(R.string.Rs) + " " + yourFormattedString + "");
        } else {
            createSpinner();
        }

    }

    private void createSpinner() {
        DateTime dateTime = new DateTime();
        isMonthlyBillListEmpty = true;
        int currentMonth = dateTime.monthOfYear().get();
        int currentYear = dateTime.getYear();
        String[] month = getResources().getStringArray(R.array.month_name);
        final String[] monthYear = new String[12];
        for (int index = currentMonth - 1; index >= 0; index--) {
            monthYear[currentMonth - index - 1] = month[index] + " - " + currentYear;
        }

        for (int index = currentMonth; index < monthYear.length; index++) {
            monthYear[index] = month[index] + "-" + (currentYear - 1);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, monthYear);
        spinner_collection_month.setVisibility(View.VISIBLE);
        spinner_collection_month.setAdapter(arrayAdapter);
        spinner_collection_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedMonthForCollection = String.valueOf(position + 1);
                selectedYearForCollection = Integer.parseInt((monthYear[position].split("-"))[1].trim());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedMonthForCollection = String.valueOf(0);
                selectedYearForCollection = 0;
            }
        });
    }

    private void createDynamicView(final List<UnitMonthlyBillDetail> collectionMonthMOList) {

        linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.setPadding(10, 10, 10, 10);
        collectionMonthLayout.removeAllViews();
        for (int i = 0; i < collectionMonthMOList.size(); i++) {

            totalBillAmount = totalBillAmount + collectionMonthMOList.get(i).getOutstandingAmount();
            View view = new View(this);
            view.setPadding(10, 30, 10, 30);
            view.setBackgroundColor(getResources().getColor(R.color.color_grey_divider));
            view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 3));
            linearLayout.addView(view);

            LinearLayout verticalLayout = new LinearLayout(this);
            verticalLayout.setOrientation(LinearLayout.VERTICAL);
            verticalLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            verticalLayout.setPadding(20, 10, 20, 10);

            //BILL NUMBER TEXTVIEW Eg: Bill Number : BNUPY87878787
            TextView billNoTv = new TextView(this);
            billNoTv.setPadding(10, 10, 10, 10);
            billNoTv.setGravity(Gravity.LEFT);
            billNoTv.setTextSize(16);
            billNoTv.setTag(i);
            String billNumberLabel = " Bill Number : ";
            String billNo = collectionMonthMOList.get(i).getBillNo();
            billNoTv.setText(billNumberLabel + billNo);
            billNoTv.setTextColor(getResources().getColor(R.color.black));
            billNoTv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            verticalLayout.addView(billNoTv);
            //----------------------------------------------------------------//


            LinearLayout horizontalLayout = new LinearLayout(this);
            horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
            horizontalLayout.setGravity(Gravity.CENTER_VERTICAL);
            horizontalLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            horizontalLayout.setPadding(10, 10, 0, 15);

            billId = collectionMonthMOList.get(i).getId();
            unitMonthlyBillCollection = collectionMonthMOList.get(i);

            CheckBox checkBox = new CheckBox(this);
//            checkBox.setPadding(0, 10, 0, 0);
            checkBox.setTag(collectionMonthMOList.get(i));
//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            checkBox.setLayoutParams(lp);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    unitMonthlyBillCollection = (UnitMonthlyBillDetail) buttonView.getTag();
                    if (isChecked)
                        billCollectionMonthMap.put(unitMonthlyBillCollection.getId(), unitMonthlyBillCollection);
                    else
                        billCollectionMonthMap.remove(unitMonthlyBillCollection.getId());
                }
            });
            horizontalLayout.addView(checkBox);

            LinearLayout horizontalLayout1 = new LinearLayout(this);
            horizontalLayout1.setOrientation(LinearLayout.VERTICAL);
//            horizontalLayout1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            horizontalLayout1.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

            TextView billMonth = new TextView(this);
            billMonth.setPadding(5, 10, 10, 5);
            billMonth.setTextSize(16);
            billMonth.setTag(i);
            billMonth.setSingleLine(true);
            billMonth.setTextColor(getResources().getColor(R.color.BILL_COLLECTION_TEXT_COLOR));
            billMonth.setText(getMonth(collectionMonthMOList.get(i).getBillMonth()) + "-" + collectionMonthMOList.get(i).getBillYear());
            billMonth.setTextColor(getResources().getColor(R.color.BILL_COLLECTION_TEXT_COLOR));
//            billMonth.setLayoutParams(new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT));
            billMonth.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            horizontalLayout1.addView(billMonth);

            TextView billDueTxt = new TextView(this);
            billDueTxt.setPadding(5, 5, 10, 10);
            billDueTxt.setTextSize(16);
            billDueTxt.setTag(i);
            billDueTxt.setSingleLine(true);
            billDueTxt.setTextColor(getResources().getColor(R.color.BILL_COLLECTION_TEXT_COLOR));
            String dueDate = getDuedate_billcollaction(tv_select_collection_date.getText().toString(), collectionMonthMOList.get(i).getOutstandingDays());
            billDueTxt.setText(getResources().getString(R.string.DUE) + " " + dueDate);
            billDueTxt.setTextColor(getResources().getColor(R.color.color_grey));
//            billDueTxt.setLayoutParams(new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT));
            billDueTxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            horizontalLayout1.addView(billDueTxt);

            horizontalLayout.addView(horizontalLayout1);

            /*TextView dummy = new TextView(this);
            dummy.setPadding(10, 10, 10, 10);
            dummy.setVisibility(View.INVISIBLE);
            dummy.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 150));
            horizontalLayout.addView(dummy);*/

            TextView billAmount = new TextView(this);
            billAmount.setPadding(10, 10, 10, 10);
            billAmount.setGravity(Gravity.RIGHT);
            billAmount.setTextSize(16);
            billAmount.setTag(i);
            billAmount.setSingleLine(true);
            billAmount.setBackground(getResources().getDrawable(R.drawable.border_editext));

            int amount = collectionMonthMOList.get(i).getOutstandingAmount();
            String amountFormatedString = Util.getCommaSeparatedAmount(amount);
            billAmount.setText(getResources().getString(R.string.Rs) + " " + amountFormatedString);

            billAmount.setTextColor(getResources().getColor(R.color.BILL_COLLECTION_TEXT_COLOR));
//            billAmount.setLayoutParams(new LinearLayout.LayoutParams(300, LinearLayout.LayoutParams.WRAP_CONTENT));
            billAmount.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f));
            horizontalLayout.addView(billAmount);

            verticalLayout.addView(horizontalLayout);

            linearLayout.addView(verticalLayout);
        }
        collectionMonthLayout.addView(linearLayout);
    }

    private String getDuedate_billcollaction(String currentdate, int outstandingDays) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(currentdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, outstandingDays);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd yyyy");
        return sdf1.format(c.getTime());
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        billCollectionMonthMap.clear();
        clearSdCardImages();
        attachmentMetaArrayList.clear();
        finish();
    }


    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    //initiating toolbar
    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.BILL_COLLECTION));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_billsubmission_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.submit) {

            if (isMonthlyBillListEmpty) {
                if (TextUtils.isEmpty(selectedMonthForCollection)) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_Collection_Month));
                } else if (null == edittext_collection_amount || edittext_collection_amount.getText().toString().isEmpty()) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_Amount));
                } else if ((billCollectionMode.equalsIgnoreCase("cheque")) && (mArrayImageHolder != null && !(mArrayImageHolder.size() > 0))) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_ADD_IMAGES));
                } else {

                    storeBillDetails();
                }
            } else {
                if (billCollectionMonthMap == null || billCollectionMonthMap.size() == 0) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_Collection_Month));

                } else if (null == edittext_collection_amount || edittext_collection_amount.getText().toString().isEmpty()) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_Amount));
                } else if ((billCollectionMode.equalsIgnoreCase("cheque")) && (mArrayImageHolder != null && !(mArrayImageHolder.size() > 0))) {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.MSG_ADD_IMAGES));
                } else {

                    storeBillDetails();
                }
            }

        }
        if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.Bill_Collection_CONFIRAMTION_DIALOG)));
        }
        return super.onOptionsItemSelected(item);
    }

    private void storeBillDetails() {
        if (appPreferences.isEnableGps(BillCollectionActivity.this)) {
            updateUnitBillingStatusTable();

            taskExecutionResult = createTaskExecutionResult();

            if (!taskExecutionResult.isEmpty()) {

                dbUpdates();
                if (rotaTaskModel.getTaskId() < 0) {

                    if (IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                            .getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                        updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                                .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
                    }

                }
                attachmentMetaArrayList.clear();
                billCollectionMonthMap.clear();
                Intent homeActivityIntent = new Intent(BillCollectionActivity.this, HomeActivity.class);
                homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(homeActivityIntent);
                finish();
            } else {
                showGpsDialog(getString(R.string.gps_configure_msg));
            }

        }
    }

    private void dbUpdates() {
        updateRotaTaskTable();
        insertMetaData();
        updateMyPerformanceTable();
        triggerSyncAdapter();
    }

    private void triggerSyncAdapter() {
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }

    private synchronized void updateMyPerformanceTable() {
        IOPSApplication.getInstance().getMyPerformanceStatementsDB().
                UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(),
                        dateTimeFormatConversionUtil.getCurrentTime(), String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);
        attachmentMetaArrayList.clear();

    }

    private synchronized void updateRotaTaskTable() {
        String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateRotaTask(rotaTaskModel.getTaskId(),
                taskExecutionResult, RotaTaskStatusEnum.Completed.getValue(),
                dateTimeFormatConversionUtil.getCurrentDateTime(),
                rotaTaskModel.getRemarks(),
                geoLocation
        );
    }

    private void updateUnitBillingStatusTable() {
        //update table UnitBillingStatusTable


        for (Map.Entry<Integer, UnitMonthlyBillDetail> billCollection : billCollectionMonthMap.entrySet()) {

            UnitMonthlyBillDetail unitMonthlyBillDetail = billCollection.getValue();
            IOPSApplication.getInstance().getUpdateStatementDBInstance().
                    UpdateUnitBillingStatusTable(1, unitMonthlyBillDetail.getUnitId(), unitMonthlyBillDetail.getId());

        }


    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.Bill_Collection_CONFIRAMTION_DIALOG)));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.takePhoto:
                int viewId = v.getId();
                Intent capturePictureActivity = null;
                Util.mSequenceNumber++;
                if (mArrayImageHolder != null && mArrayImageHolder.size() < 3) {
                    capturePictureActivity = new Intent(BillCollectionActivity.this, CapturePictureActivity.class);
                    capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                    Util.setSendPhotoImageTo(Util.BILL_COLLECTION);
                    Util.initMetaDataArray(true);
                    startActivityForResult(capturePictureActivity, BILL_COLLECTION_IMAGE_CODE);
                } else {
                    snackBarWithMesg(parent_relative_layout, getResources().getString(R.string.Captured_Image_MSG));
                }
                break;


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BILL_COLLECTION_IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
            setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            mArrayImageHolder.add(imagePath);
            loadThumbNailImages(mArrayImageHolder);
        }
    }

    private void loadThumbNailImages(ArrayList<String> arrayImageHolder) {
        if (linearLayout_photos != null) {
            linearLayout_photos.removeAllViews();
            linearLayout_photos.addView(takePhoto);
        }

        if (arrayImageHolder != null && arrayImageHolder.size() > 0) {
            for (int index = arrayImageHolder.size() - 1; index >= 0; index--) {
                ImageView imageView = createImageView(Uri.parse(arrayImageHolder.get(index)));
                linearLayout_photos.addView(imageView);
            }

        }
    }

    public ImageView createImageView(Uri uri) {

        int marginBottom = Util.dpToPx(Constants.MARGIN_BOTTOM);
        int marginLeft = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginRight = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.HEIGT);
        params.width = Util.dpToPx(Constants.HEIGT);
        params.setMargins(marginLeft, Constants.ZERO, marginRight, marginBottom);

        RoundedCornerImageView takenImageView = new RoundedCornerImageView(this);
        takenImageView.setLayoutParams(params);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.camer_taken_pic_image_border);
        takenImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        takenImageView.setImageURI(uri);
        return takenImageView;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.bill_collection_radio_group:
                if (checkedId == R.id.cheque) {
                    billCollectionMode = String.valueOf(cheque.getText().toString());
                    edittext_collection_amount.setText("");
                    collection_check_layout.setVisibility(View.VISIBLE);
                    collection_rgts_detail.setVisibility(View.GONE);
                } else if (checkedId == R.id.rtgs) {
                    billCollectionMode = String.valueOf(rtgs.getText().toString());
                    edittext_collection_amount.setText("");
                    collection_check_layout.setVisibility(View.GONE);
                    collection_rgts_detail.setVisibility(View.VISIBLE);
                }
                break;
        }

    }

    private synchronized String createTaskExecutionResult() {


        try {

            billCollectionTaskExeMO = new BillCollectionTaskExeMO();

            List<BillCollectionMO> billCollectionMOList = new ArrayList<BillCollectionMO>();

            if (billCollectionMonthMap != null && billCollectionMonthMap.size() != 0) {
                for (Map.Entry<Integer, UnitMonthlyBillDetail> billCollection : billCollectionMonthMap.entrySet()) {

                    UnitMonthlyBillDetail unitMonthlyBillDetail = billCollection.getValue();
                    billCollectionMO = new BillCollectionMO();
                    billCollectionMO.setBillAmount(String.valueOf(unitMonthlyBillDetail.getOutstandingAmount()));
                    billCollectionMO.setChequeNo(edittext_check_number.getText().toString());
                    billCollectionMO.setCollectionMonth(String.valueOf(unitMonthlyBillDetail.getBillMonth()));
                    billCollectionMO.setCollectionYear(unitMonthlyBillDetail.getBillYear());
                    billCollectionMO.setCollectionAmount(edittext_collection_amount.getText().toString());
                    String dueDate = getDuedate_billcollaction(tv_select_collection_date.getText().toString(),
                            unitMonthlyBillDetail.getOutstandingDays());
                    billCollectionMO.setDueDate(dueDate);
                    billCollectionMO.setPaymentMode(billCollectionMode);
                    billCollectionMO.setRgtsDetails(edittext_rgts_remarks.getText().toString());
                    billCollectionMO.setCollectionDate(tv_select_collection_date.getText().toString());
                    billCollectionMOList.add(billCollectionMO);

                }
            } else {
                billCollectionMO = new BillCollectionMO();
                billCollectionMO.setChequeNo(edittext_check_number.getText().toString());
                billCollectionMO.setCollectionMonth(selectedMonthForCollection);
                billCollectionMO.setCollectionYear(selectedYearForCollection);
                billCollectionMO.setCollectionAmount(edittext_collection_amount.getText().toString());
                billCollectionMO.setPaymentMode(billCollectionMode);
                billCollectionMO.setRgtsDetails(edittext_rgts_remarks.getText().toString());
                billCollectionMO.setCollectionDate(tv_select_collection_date.getText().toString());
                billCollectionMOList.add(billCollectionMO);
            }


            billCollectionTaskExeMO.setBillCollection(billCollectionMOList);


            if (null != billCollectionTaskExeMO) {
                taskExecutionResult = IOPSApplication.getGsonInstance().toJson(billCollectionTaskExeMO);
            } else {

                taskExecutionResult = "";

            }

        } catch (Exception e) {
            taskExecutionResult = "";
        }
        return taskExecutionResult;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    private void myPerformanceActivityTrackingUpdation(RotaTaskModel rotaModel) {


        insertLogActivity();
        IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
        IOPSApplication.getInstance().getMyPerformanceStatementsDB().
                UpdateMYPerformanceTable(RotaTaskStatusEnum.valueOf(rotaModel.getTaskStatusId()).name(),
                        dateTimeFormatConversionUtil.getCurrentTime(), String.valueOf(rotaModel.getTaskId()), rotaModel.getId());

    }

    @Override
    public void finish() {
        super.finish();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearSdCardImages() {
        for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
            if (attachmentMetaDataModel != null) {
                File file = new File(attachmentMetaDataModel.getAttachmentPath());
                if (file != null && file.exists()) {
                    file.delete();
                }
            }
        }
    }

}
