package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 11-05-2016.
 */
public class SyncingMetaData {
    @SerializedName("Id")
    private Integer Id;

    @SerializedName("AttachmentTypeId")
    private Integer AttachmentTypeId;

    @SerializedName("AttachmentTitle")
    private String AttachmentTitle;

    @SerializedName("AttachmentExtension")
    private String AttachmentExtension;

    @SerializedName("SizeInKB")
    private Integer SizeInKB;

    @SerializedName("AttachmentSourceTypeId")
    private Integer AttachmentSourceTypeId;

    @SerializedName("AttachmentSourceId")
    private Integer AttachmentSourceId;

    @SerializedName("SequenceNo")
    private Integer SequenceNo;

    @SerializedName("TaskId")
    private Integer TaskId;

    @SerializedName("UnitId")
    private Integer UnitId;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The AttachmentTypeId
     */
    public Integer getAttachmentTypeId() {
        return AttachmentTypeId;
    }

    /**
     * @param AttachmentTypeId The AttachmentTypeId
     */
    public void setAttachmentTypeId(Integer AttachmentTypeId) {
        this.AttachmentTypeId = AttachmentTypeId;
    }

    /**
     * @return The AttachmentTitle
     */
    public String getAttachmentTitle() {
        return AttachmentTitle;
    }

    /**
     * @param AttachmentTitle The AttachmentTitle
     */
    public void setAttachmentTitle(String AttachmentTitle) {
        this.AttachmentTitle = AttachmentTitle;
    }

    /**
     * @return The AttachmentExtension
     */
    public String getAttachmentExtension() {
        return AttachmentExtension;
    }

    /**
     * @param AttachmentExtension The AttachmentExtension
     */
    public void setAttachmentExtension(String AttachmentExtension) {
        this.AttachmentExtension = AttachmentExtension;
    }

    /**
     * @return The SizeInKB
     */
    public Integer getSizeInKB() {
        return SizeInKB;
    }

    /**
     * @param SizeInKB The SizeInKB
     */
    public void setSizeInKB(Integer SizeInKB) {
        this.SizeInKB = SizeInKB;
    }

    /**
     * @return The AttachmentSourceTypeId
     */
    public Integer getAttachmentSourceTypeId() {
        return AttachmentSourceTypeId;
    }

    /**
     * @param AttachmentSourceTypeId The AttachmentSourceTypeId
     */
    public void setAttachmentSourceTypeId(Integer AttachmentSourceTypeId) {
        this.AttachmentSourceTypeId = AttachmentSourceTypeId;
    }

    /**
     * @return The AttachmentSourceId
     */
    public Integer getAttachmentSourceId() {
        return AttachmentSourceId;
    }

    /**
     * @param AttachmentSourceId The AttachmentSourceId
     */
    public void setAttachmentSourceId(Integer AttachmentSourceId) {
        this.AttachmentSourceId = AttachmentSourceId;
    }

    /**
     * @return The SequenceNo
     */
    public Integer getSequenceNo() {
        return SequenceNo;
    }

    /**
     * @param SequenceNo The SequenceNo
     */
    public void setSequenceNo(Integer SequenceNo) {
        this.SequenceNo = SequenceNo;
    }

    /**
     * @return The TaskId
     */
    public Integer getTaskId() {
        return TaskId;
    }

    /**
     * @param TaskId The TaskId
     */
    public void setTaskId(Integer TaskId) {
        this.TaskId = TaskId;
    }

    /**
     * @return The UnitId
     */
    public Integer getUnitId() {
        return UnitId;
    }

    /**
     * @param UnitId The UnitId
     */
    public void setUnitId(Integer UnitId) {
        this.UnitId = UnitId;
    }
}
