package com.sisindia.ai.android.rota.clientcoordination.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shivam on 26/7/16.
 */

public class QuestionsData {


    /*"ClientCoordainationQuestions": [
    {
        "Id": 1,
            "Question": "Maintenance of Authorized Strength",
            "IsActive": true
    },
    */

    @SerializedName("Id")
    public int questionId;

    @SerializedName("Question")
    public String question;

    @SerializedName("IsActive")
    public boolean isActive;
}
