package com.sisindia.ai.android.rota.daycheck;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;

public class StrengthCheckActivity extends DayCheckBaseActivity implements OnRecyclerViewItemClickListener, DialogClickListener {

    /*  @Bind(R.id.strengthCheckRecyclerview)
      RecyclerView strengthCheckRecyclerview;*/
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.strength_check_linear_layout)
    LinearLayout strengthCheckLinearLayout;
    @Bind(R.id.dayCheckHeader)
    View mStepperViewParent;
    @Bind(R.id.strength_check_base_layout)
    LinearLayout mParentLayout;
    @Bind(R.id.task_client_check)
    View stepperFourView;
    @Bind(R.id.task_security_check)
    View stepperFiveView;
    @Bind(R.id.stepper_four_completed)
    View stepperFourViewCompleted;
    @Bind(R.id.stepper_three_completed)
    View stepperThreeViewCompleted;
    @Bind(R.id.framelayout_stepper_four)
    FrameLayout framelayout_stepper_four;
    @Bind(R.id.framelayout_stepper_five)
    FrameLayout framelayout_stepper_five;
    @Bind(R.id.stepper_frame_six)
    FrameLayout mstepper_frame_six;
    @Bind(R.id.stepper_five_completed)
    View mstepper_five_completed;
    private ArrayList<Object> rankNameList;
    private LayoutInflater layoutInflater;
    private ArrayList<View> viewList;
    private ArrayList<Object> actualCountList;
    private List<EmployeeAuthorizedStrengthMO> employeeAuthorizedStrengthMOList;
    private ArrayList<EmployeeAuthorizedStrengthMO> updatedActualStrength;
    private String data;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strength_check);
        ButterKnife.bind(this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        mOnStepperUpdateListener = Util.getmHandlerInstance();
        //rotaTaskModel = (RotaTaskModel) SingletonUnitDetailsMO.getInstance().getUnitDetails();
        if (rotaTaskModel != null)
            setHeaderUnitName(rotaTaskModel.getUnitName());
        if (getIntent().hasExtra(getResources().getString(R.string.navigationcheckingTitle))) {
            data = getIntent().getExtras().getString(getResources().getString(R.string.navigationcheckingTitle), getResources().getString(R.string.dayCheckingTitle));

            if (data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                setBaseToolbar(toolbar, data);
                stepperFourView.setVisibility(View.GONE);
                stepperFourViewCompleted.setVisibility(View.GONE);
                stepperThreeViewCompleted.setVisibility(View.GONE);
                framelayout_stepper_four.setVisibility(View.GONE);
                framelayout_stepper_five.setVisibility(View.GONE);
                mstepper_five_completed.setVisibility(View.GONE);
                mstepper_frame_six.setVisibility(View.GONE);
                stepperFiveView.setVisibility(View.GONE);
            } else {
                setBaseToolbar(toolbar, data);
            }
        } else {
            setBaseToolbar(toolbar, getResources().getString(R.string.dayCheckingTitle));
        }
        // Util.showProgressBar(this,R.string.Loading_Data_Message);
        createScrollView();
        getStepperViews(mStepperViewParent);
        if (data != null && data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
            updateTotalTaskCount();
        }
        //createRecylerView();

    }

    private void createScrollView() {
        layoutInflater = getLayoutInflater();


        if (rotaTaskModel != null) {
            employeeAuthorizedStrengthMOList = IOPSApplication.getInstance().
                    getRotaLevelSelectionInstance().
                    getUnitEmpAuthorizedRank(rotaTaskModel.getUnitId(),false,rotaTaskModel.getBarrackId());
            if (employeeAuthorizedStrengthMOList != null && employeeAuthorizedStrengthMOList.size() > 0) {
                View headerView = layoutInflater.inflate(R.layout.strength_check_header, null, false);
                strengthCheckLinearLayout.addView(headerView);

                for (EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO : employeeAuthorizedStrengthMOList) {

                    View view = layoutInflater.inflate(R.layout.strength_check_row_item, null, false);
                    final StrengthCheckViewHolder strengthCheckViewHolder = new StrengthCheckViewHolder(view);
                    strengthCheckViewHolder.strengthCheckRankNameTv.
                            setText(util.createSpannableStringBuilder(employeeAuthorizedStrengthMO.getRankAbbrevation().trim()));
                    strengthCheckViewHolder.strengthCheckAuthorizedTv.setText(String.valueOf(employeeAuthorizedStrengthMO.getStrength()));
                    strengthCheckViewHolder.strengthCheckActualEt.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            strengthCheckViewHolder.errorMessage.setVisibility(View.GONE);
                            strengthCheckViewHolder.redCircle.setVisibility(View.GONE);
                        }
                    });
                    strengthCheckLinearLayout.addView(view);
                }
            } else {
                Toast.makeText(StrengthCheckActivity.this, "No Authorized rank available", Toast.LENGTH_SHORT).show();
                // snackBarWithDuration(mParentLayout, "No Authorized rank available",getResources().getInteger(R.integer.number_10));
            }
        }


    }


    private void validateFields() {
        int rowCount = strengthCheckLinearLayout.getChildCount();
        updatedActualStrength = new ArrayList<EmployeeAuthorizedStrengthMO>();
        for (int index = 1; index < rowCount; index++) {
            View childView = strengthCheckLinearLayout.getChildAt(index);
            EditText editText = (EditText) childView.findViewById(R.id.strengthCheckActual_et);
            TextView errorMessage = (TextView) childView.findViewById(R.id.errorMessage);
            View redDotView = childView.findViewById(R.id.redCircle);
            String actulaCountString = editText.getText().toString();

            if (actulaCountString != null && actulaCountString.isEmpty()) {
                errorMessage.setVisibility(View.VISIBLE);
                redDotView.setVisibility(View.VISIBLE);

            } else {
                EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO = new EmployeeAuthorizedStrengthMO();
                employeeAuthorizedStrengthMO.setUnitTakID(rotaTaskModel.getTaskId());
                employeeAuthorizedStrengthMO.setActual(Integer.parseInt(actulaCountString));
                employeeAuthorizedStrengthMO.setUnitId(employeeAuthorizedStrengthMOList.get(index - 1).getUnitId());
                employeeAuthorizedStrengthMO.setRankId(employeeAuthorizedStrengthMOList.get(index - 1).getRankId());
                employeeAuthorizedStrengthMO.setLeaveCount(0);
                employeeAuthorizedStrengthMO.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                updatedActualStrength.add(employeeAuthorizedStrengthMO);
                errorMessage.setVisibility(View.GONE);
                redDotView.setVisibility(View.GONE);
            }
        }


    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_duty_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done_menu) {
            validateFields();
            if (updatedActualStrength != null && employeeAuthorizedStrengthMOList != null &&
                    employeeAuthorizedStrengthMOList.size() == updatedActualStrength.size()) {
                setUnitStrength(updatedActualStrength,true);
                mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(1).name(),
                        DayCheckEnum.Strength_Check.getValue());
                finish();
            }
            return true;
        } else if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPositiveButtonClick(int clickType) {

        if(clickType == 0){
            mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(1).name(),DayCheckEnum.Strength_Check.getValue());
            finish();
        }
        else{
            finish();
        }
    }

    @Override
    public void onBackPressed() {
       /* if(employeeAuthorizedStrengthMOList != null && employeeAuthorizedStrengthMOList.size() != 0){
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
        }*/
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    class StrengthCheckViewHolder {
        @Bind(R.id.strengthCheckRankName_tv)
        CustomFontTextview strengthCheckRankNameTv;
        @Bind(R.id.strengthCheckAuthorized_tv)
        CustomFontTextview strengthCheckAuthorizedTv;
        @Bind(R.id.strengthCheckActual_et)
        CustomFontEditText strengthCheckActualEt;
        @Bind(R.id.redCircle)
        View redCircle;
        @Bind(R.id.errorMessage)
        CustomFontTextview errorMessage;

        StrengthCheckViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
