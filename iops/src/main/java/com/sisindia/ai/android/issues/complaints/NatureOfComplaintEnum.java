package com.sisindia.ai.android.issues.complaints;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum NatureOfComplaintEnum {

    Non_Compliance(1),
    Regulatory(2),
    Training_Quality(3),
    SG_Turnout(4),
    SG_Alertness(5),
    Duty_Awareness(6),
    Human_Relation(7),
    Shortage_Of_Manpower(8),
    Others(9);

    private static Map map = new HashMap<>();

    static {
        for (NatureOfComplaintEnum postStatusEM : NatureOfComplaintEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    NatureOfComplaintEnum(int value) {
        this.value = value;
    }

    public static NatureOfComplaintEnum valueOf(int postStatus) {
        return (NatureOfComplaintEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
