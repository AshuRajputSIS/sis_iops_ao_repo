package com.sisindia.ai.android.commons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.DutyAttendanceDB;
import com.sisindia.ai.android.database.DutyAttendanceMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

/**
 * Created by Ashu Rajput on 1/4/2018.
 */

public class NightDutyOnOffReceiver extends BroadcastReceiver {

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil = null;
    private DutyAttendanceDB dutyAttendanceDB = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        AppPreferences appPreferences = new AppPreferences(context);

        if (appPreferences.getDutyStatus()) {
            DutyAttendanceMo dutyOffAttendanceMo = new DutyAttendanceMo();
            dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
            dutyAttendanceDB = IOPSApplication.getInstance().getDutyAttendanceDB();

            dutyOffAttendanceMo.setDutyOffTime(dateTimeFormatConversionUtil.getCurrentDateTime());
            dutyAttendanceDB.updateDutyOffTime(dutyOffAttendanceMo.getDutyOffTime());
            dutyOffAttendanceMo = null;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        DutyAttendanceMo dutyAttendanceMo = new DutyAttendanceMo();
                        dutyAttendanceMo.setDutyOnTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                        dutyAttendanceDB.insertDutyAttendanceDetails(dutyAttendanceMo);

                        dateTimeFormatConversionUtil = null;
                        dutyAttendanceDB = null;

                        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(new Bundle());
                    } catch (Exception e) {
                    }

                }
            }, 5000);
        }
    }
}

