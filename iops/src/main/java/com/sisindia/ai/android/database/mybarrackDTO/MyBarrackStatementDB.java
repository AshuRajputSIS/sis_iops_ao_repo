package com.sisindia.ai.android.database.mybarrackDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 9/8/16.
 */

public class MyBarrackStatementDB extends SISAITrackingDB {
    private String geoLocation;
    private String lastInspectedDate;

    public MyBarrackStatementDB(Context context) {
        super(context);
    }

    public boolean checkBarrackAvailableStatus(int barrackId) {
        boolean isBarrackAvailable = false;
        String selectQuery;

        // Select All Query
        selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.BARRACK_TABLE
                + " where " + TableNameAndColumnStatement.BARRACK_ID + "='" + barrackId + "'";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        isBarrackAvailable = true;
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return isBarrackAvailable;
    }

    public List<Barrack> getBarrackDetailsByUnitId(int unitId) {
        List<Barrack> barrackList = new ArrayList<>();
        String selectQuery = " SELECT *" +
                " FROM " +
                TableNameAndColumnStatement.UNIT_BARRACK_TABLE + " ub " +
                "inner join " +
                TableNameAndColumnStatement.BARRACK_TABLE + " b " +
                "on b." + TableNameAndColumnStatement.BARRACK_ID +
                "=" + "ub." + TableNameAndColumnStatement.BARRACK_ID +
                " where " +
                TableNameAndColumnStatement.UNIT_ID + "='" + unitId + "' and " +
                TableNameAndColumnStatement.IS_ACTIVE + "='1'";


        Timber.d("getBarrackDetailsById selectQuery  %s", selectQuery);

        SQLiteDatabase db = null;
        Cursor barrackcursor = null;
        try {
            db = this.getReadableDatabase();
            barrackcursor = db.rawQuery(selectQuery, null);

            if (barrackcursor != null && !barrackcursor.isClosed()) {

                // looping through all rows and adding to list
                if (barrackcursor.moveToFirst()) {
                    do {
                        Barrack barrack = new Barrack();
                        barrack.setSequenceId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        barrack.setId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        barrack.setName(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));

                        if (barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.IS_MESS_AVAILABLE)) == 0) {
                            barrack.setMessAvailable(false);
                        } else {
                            barrack.setMessAvailable(true);
                        }

                        barrack.setBarrackAddress(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                        barrack.setBranchId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BRANCH_ID)));
                        barrackList.add(barrack);
                    } while (barrackcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {


            if (db != null && db.isOpen()) {
                db.close();
            }

            if (barrackcursor != null) {
                barrackcursor.close();
            }
            //Timber.d("barrackArrayList object  %s", IOPSApplication.getGsonInstance().toJson(barrackArrayList));

        }
        return barrackList;
    }

    public List<Barrack> getBarrackDetailsById(int barrackId) {
        List<Barrack> barrackList = new ArrayList<>();
        String selectQuery = " SELECT *" +
                " FROM " + TableNameAndColumnStatement.BARRACK_TABLE + " where " +
                TableNameAndColumnStatement.BARRACK_ID + "='" + barrackId + "'";

        Timber.d("getBarrackDetailsById selectQuery  %s", selectQuery);

        SQLiteDatabase db = null;
        Cursor barrackcursor = null;
        try {
            db = this.getReadableDatabase();
            barrackcursor = db.rawQuery(selectQuery, null);

            if (barrackcursor != null && !barrackcursor.isClosed()) {

                // looping through all rows and adding to list
                if (barrackcursor.moveToFirst()) {
                    do {
                        Barrack barrack = new Barrack();
                        barrack.setSequenceId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        barrack.setId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        barrack.setName(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));

                        if (barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.IS_MESS_AVAILABLE)) == 0) {
                            barrack.setMessAvailable(false);
                        } else {
                            barrack.setMessAvailable(true);
                        }

                        barrack.setBarrackAddress(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                        barrack.setBranchId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BRANCH_ID)));
                        barrackList.add(barrack);
                    } while (barrackcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {


            if (db != null && db.isOpen()) {
                db.close();
            }


        }
        return barrackList;
    }

    public ArrayList<Barrack> getBarrackDetails() {

        ArrayList<Barrack> barrackArrayList = new ArrayList<>();
      /*  String selectQuery = " SELECT *" +
                " FROM " + TableNameAndColumnStatement.BARRACK_TABLE;
        String selectQuery = " SELECT * " +
                " FROM " + TableNameAndColumnStatement.BARRACK_TABLE;*/
        //@Ashu : commenting previously defined Query:
        /*String selectQuery = " select b.id as id,b.barrack_id as barrack_id,b.barrack_name as barrack_name," +
                " b.is_mess_available as is_mess_available,b.geo_longitude as geo_longitude,b.geo_latitude as geo_latitude," +
                " b.barrack_full_address as barrack_full_address,b.branch_id as branch_id, 'self' as belongs_to" +
                " from unit_barrack ub " +
                " inner join barrack  b on  ub.barrack_id = b.barrack_id "+
                " where ub.barrack_id is not null "+
                " union "+
                " select b.id as id,b.barrack_id as barrack_id,b.barrack_name as barrack_name," +
                " b.is_mess_available as is_mess_available,b.geo_longitude as geo_longitude,b.geo_latitude as geo_latitude," +
                " b.barrack_full_address as barrack_full_address,b.branch_id as branch_id, 'others' as belongs_to  from barrack  b " +
                " left outer join  unit_barrack ub on ub.barrack_id = b.barrack_id "+
                " where ub.barrack_id is null ";*/

        String selectQuery = "select b.id as id,b.barrack_id as barrack_id,b.barrack_name as barrack_name, b.is_mess_available as is_mess_available," +
                "b.geo_longitude as geo_longitude,b.geo_latitude as geo_latitude, b.barrack_full_address as barrack_full_address," +
                "b.branch_id as branch_id, b.deviceno as nfcID,'self' as belongs_to from unit_barrack ub inner join barrack b on ub.barrack_id = b.barrack_id " +
                "where ub.barrack_id is not null union select b.id as id,b.barrack_id as barrack_id,b.barrack_name as barrack_name," +
                " b.is_mess_available as is_mess_available,b.geo_longitude as geo_longitude,b.geo_latitude as geo_latitude, " +
                "b.barrack_full_address as barrack_full_address,b.branch_id as branch_id, b.deviceno as nfcID," +
                "'others' as belongs_to from barrack b left outer join unit_barrack ub on ub.barrack_id = b.barrack_id where ub.barrack_id is null";

        Timber.d("getBarrackDetails selectQuery  %s", selectQuery);

        SQLiteDatabase db = null;
        Cursor barrackcursor = null;
        try {
            db = this.getReadableDatabase();
            barrackcursor = db.rawQuery(selectQuery, null);

            if (barrackcursor != null && !barrackcursor.isClosed()) {

                // looping through all rows and adding to list
                if (barrackcursor.moveToFirst()) {
                    do {
                        Barrack barrack = new Barrack();
                        barrack.setSequenceId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        barrack.setId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        barrack.setName(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));

                        if (barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.IS_MESS_AVAILABLE)) == 0) {
                            barrack.setMessAvailable(false);
                        } else {
                            barrack.setMessAvailable(true);
                        }
                        barrack.setGeoLatitude(barrackcursor.getDouble(barrackcursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        barrack.setGeoLatitude(barrackcursor.getDouble(barrackcursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        barrack.setBarrackAddress(barrackcursor.getString(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                        barrack.setBranchId(barrackcursor.getInt(barrackcursor.getColumnIndex(TableNameAndColumnStatement.BRANCH_ID)));
                        barrack.setBarrackNfcID(barrackcursor.getString(barrackcursor.getColumnIndex("nfcID")));
                        barrack.setBelongsTo(barrackcursor.getString(barrackcursor.getColumnIndex("belongs_to")));
                        barrackArrayList.add(barrack);
                    } while (barrackcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {


            if (db != null && db.isOpen()) {
                db.close();
            }

            if (barrackcursor != null) {
                barrackcursor.close();
            }
            //Timber.d("barrackArrayList object  %s", IOPSApplication.getGsonInstance().toJson(barrackArrayList));

        }
        return barrackArrayList;
    }

    public void updateBarrackGeoLocation(int barrackId, String latitude, String longitude, String nfcDeviceNo) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.BARRACK_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.GEO_LATITUDE, latitude);
            contentValues.put(TableNameAndColumnStatement.GEO_LONGITUDE, longitude);
            contentValues.put(TableNameAndColumnStatement.DEVICE_NO, nfcDeviceNo);
            contentValues.put(TableNameAndColumnStatement.IS_SYNCED, 0);
            contentValues.put(TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED, 0);
            sqlite.update(TableNameAndColumnStatement.BARRACK_TABLE, contentValues, whereClause,
                    new String[]{" " + barrackId});

        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {

            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }
    }

    public String getBarrackGeoLocation(Integer barrackId) {
        String selectQuery = " SELECT *" +
                " FROM " +
                TableNameAndColumnStatement.BARRACK_TABLE +
                " where " +
                TableNameAndColumnStatement.BARRACK_ID + "='" + barrackId + "'";
        Timber.d("getBarrackGeoLocation selectQuery  %s", selectQuery);
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) == null) {
                            geoLocation = "NA";
                        } else {
                            String latitude = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE));
                            String longitude = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE));
                            geoLocation = latitude + "," + longitude;
                        }
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return geoLocation;
    }

    public String getBarrackLastInspectedDate(Integer barrackId) {

        String selectQuery = " SELECT " + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME +
                " FROM " +
                TableNameAndColumnStatement.TASK_TABLE +
                " where " +
                TableNameAndColumnStatement.BARRACK_ID + "='" + barrackId + "' and "
                + TableNameAndColumnStatement.TASK_TYPE_ID + "='" + 3 + "' order by " +
                TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME + " desc";

        Timber.d("getBarrackLastInspectedDate selectQuery  %s", selectQuery);
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME)) == null) {
                            lastInspectedDate = TableNameAndColumnStatement.NOT_AVAILABLE;
                        } else {
                            lastInspectedDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME));
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            lastInspectedDate = "NA";
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lastInspectedDate;
    }

    //@Ashu: Adding methods for getting informaion w.r.t nfc installed on barracks
    public Barrack getBarrackNFCInformation(String uniqueId, int barrackID) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "";
                if (barrackID > 0) {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.BARRACK_TABLE + " WHERE " +
                            TableNameAndColumnStatement.BARRACK_ID + " = " + barrackID + " and " + TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                } else {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.BARRACK_TABLE + " WHERE " +
                            TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                }
                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {

                    Barrack barrackInfo = new Barrack();
                    barrackInfo.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                    barrackInfo.setBarrackID(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                    barrackInfo.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                    barrackInfo.setBarrackGeoPoints(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) + "," +
                            cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                    return barrackInfo;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return null;
    }

    public void updateNfcDeviceNoOfBarrack(String deviceNo, int barrackId) {

        SQLiteDatabase sqlite = null;
        String updateQuery = " UPDATE " + TableNameAndColumnStatement.BARRACK_TABLE +
                " SET " + TableNameAndColumnStatement.DEVICE_NO + " = " + " '' " +
                " WHERE " + TableNameAndColumnStatement.DEVICE_NO + " = '" + deviceNo + "'"
                + " AND " + TableNameAndColumnStatement.BARRACK_ID + " = " + barrackId;

        Timber.d("updateNfcDeviceNo  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            exception.printStackTrace();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    // USE BELOW QUERY TO CHECK WHETHER DEVICE NO EXISTS EITHER IN BARRACK TABLE OR UNIT POST TABLE
    public boolean isNfcAlreadyExists(String deviceNo) {

        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
//                String selectQuery = "select count(*) as nfcCount  from barrack where DeviceNo='" + deviceNo + "'";
                String selectQuery = "select CASE when (select count(1) from barrack where DeviceNo='" + deviceNo + "') = 1 then 1 " +
                        "else (select count(1) from unit_post where DeviceNo='" + deviceNo + "') end nfcCount";

                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int count = cursor.getInt(cursor.getColumnIndex("nfcCount"));
                    if (count > 0)
                        return true;
                    else
                        return false;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return false;
    }

    public ArrayList<Object> isNfcConfiguredAtBarrack(int barrackId) {

        SQLiteDatabase sqlite = null;
        ArrayList<Object> nfcDetails = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select DeviceNo,isNfcScanned from barrack where barrack_id='" + barrackId + "'";
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                nfcDetails = new ArrayList<>(cursor.getCount());
                if (cursor != null && cursor.moveToFirst()) {
//                    String deviceNo = cursor.getString(cursor.getColumnIndex("DeviceNo"));
//                    int isScannedDuringTask = cursor.getInt(cursor.getColumnIndex("isNfcScanned"));

                    nfcDetails.add(cursor.getString(cursor.getColumnIndex("DeviceNo")));
                    nfcDetails.add(cursor.getInt(cursor.getColumnIndex("isNfcScanned")));
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return nfcDetails;
    }

    /*public void updateNfcTouchedStatusDuringTask(int barrackId, String deviceNo, int touchedStatus) {

        SQLiteDatabase sqlite = null;
        String updateQuery = " UPDATE " + TableNameAndColumnStatement.BARRACK_TABLE +
                " SET " + TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED + " = '" + touchedStatus + "' " +
                " WHERE " + TableNameAndColumnStatement.DEVICE_NO + " = '" + deviceNo + "'"
                + " AND " + TableNameAndColumnStatement.BARRACK_ID + " = " + barrackId;

        Timber.d("updateNfcDeviceNo  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
//            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            exception.printStackTrace();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }*/

}
