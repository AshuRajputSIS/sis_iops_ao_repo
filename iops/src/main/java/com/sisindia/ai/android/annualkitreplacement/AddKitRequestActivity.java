package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.reflect.TypeToken;
import com.sisindia.ai.android.commons.CommonTags;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.EmployeeIdListAdapter;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.response.UnitEmployee;
import com.sisindia.ai.android.rota.daycheck.AddKitRequestMo;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.daycheck.SignatureActivity;
import com.sisindia.ai.android.rota.daycheck.TaskGuardMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity.BARCODE_CAPTURE_REQUEST_CODE;

public class AddKitRequestActivity extends DayCheckBaseActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener {


    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Nullable
    @Bind(R.id.tv_guard_name)
    CustomFontTextview tvGuardName;
    @Nullable
    @Bind(R.id.unit_name)
    CustomFontTextview unitNameTv;

    @Nullable
    @Bind(R.id.overAllTurnoutImage)
    RoundedCornerImageView kitReplaceImage;
    @Nullable
    @Bind(R.id.add_signature_guard_level)
    RoundedCornerImageView addSignatureGuardLevel;
    @Nullable
    @Bind(R.id.parent_layout)
    LinearLayout parentLayout;
    @Nullable
    @Bind(R.id.replaceItemLayout)
    LinearLayout replacItemLayout;

    @Nullable
    @Bind(R.id.employee_id_number)
    AutoCompleteTextView guardIdAutoCompleteTextview;

    @Nullable
    @Bind(R.id.guardNameDivider)
    View guardNameDivider;



    @Nullable
    @Bind(R.id.guard_parent_layout)
    LinearLayout guardParentLayout;
    @Nullable
    @Bind(R.id.scanBarCodeButton)
    CustomFontButton scanBarCodeButton;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private String unitId;
    private int employeeId;
    private String unitName;
    private int taskId;
    private String guardName;
    private String employeeNo;
    private ArrayList<KitItemModel> kitItemList;

    private Map<Integer,Integer>  kitItemSizeIds;
    private List<KitItemRequestItem> kitItemRequestItemList;

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<UnitEmployee> arrayListGuardIds;
    private boolean isSignatureMandatory;
    private android.view.inputmethod.InputMethodManager imm;
    private Resources resources;
    private AttachmentMetaDataModel attachmentMetaDataModel;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kit_request);
        ButterKnife.bind(this);
        resources = getResources();
        setBaseToolbar(toolbar, getResources().getString(R.string.KIT_ITEM_REQUEST));
        setupData();
        createCheckboxes();

    }

    private void setupData() {


        kitItemSizeIds = new HashMap<>();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();


        Intent intent = getIntent();
        if(intent != null){
            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey(TableNameAndColumnStatement.TASK_ID)) {
                employeeId = intent.getIntExtra(TableNameAndColumnStatement.employee_id, 0);
                taskId = intent.getIntExtra(TableNameAndColumnStatement.TASK_ID, 0);
                guardName = intent.getStringExtra(TableNameAndColumnStatement.guard_name);
                employeeNo = intent.getStringExtra(TableNameAndColumnStatement.EMPLOYEE_NO);
                unitName = intent.getStringExtra(TableNameAndColumnStatement.unitName);
                unitId = intent.getStringExtra(TableNameAndColumnStatement.UNIT_ID);
                isSignatureMandatory = true;
            }
        }
        kitReplaceImage.setOnClickListener(this);
        addSignatureGuardLevel.setOnClickListener(this);
        kitItemRequestMO.setGuardCode(employeeNo);
        kitItemRequestMO.setSourceTaskId(taskId);
        kitItemRequestMO.setGuardId(String.valueOf(employeeId));
        kitItemRequestMO.setRequestedStatus("1");
        unitNameTv.setText(unitName);
        tvGuardName.setText(guardName);
    }

    private void createCheckboxes() {
        kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
        kitItemList = kitItemSelectionStatementDB.getKitItemList();
        //String jsonData = appPreference.getAddKitRequestModel();
        TaskGuardMo taskGuradMo = new TaskGuardMo();
        taskGuradMo.setTaskId(taskId);
        taskGuradMo.setGuardCode(employeeNo);
        String jsonData = null;
        if(appPreferences.getAddKitRequest(employeeNo,taskId) != null) {
            jsonData = (appPreferences.getAddKitRequest(employeeNo,taskId)).getAddKitRequestJson();

            if (!TextUtils.isEmpty(jsonData)) {
                kitItemRequestMO = IOPSApplication.getGsonInstance().fromJson(jsonData, KitItemRequestMO.class);
                if (kitItemRequestMO != null) {
                    kitItemRequestItemList = IOPSApplication.getGsonInstance()
                            .fromJson(kitItemRequestMO.getRequestedItems(),
                            new TypeToken<ArrayList<KitItemRequestItem>>() {
                            }.getType());
                }
               /* if (appPreference.getAddKitRequest(employeeNo,taskId) != null) {
                    addSignatureGuardLevel.setImageBitmap(Util.getSignatureWithCode(employeeNo));
                }*/

                getImageFromAttachmentmodel();
                getSignatureFromAttachmentmodel();
            }
        }

        if (kitItemList != null && kitItemList.size() != 0) {
            for (int index = 0; index < kitItemList.size(); index++) {
                CheckBox customCheckbox = new CheckBox(this);
                customCheckbox.setText(kitItemList.get(index).getName());
                customCheckbox.setId(kitItemList.get(index).getId());

                int marginBottom = Util.dpToPx(getResources().getInteger(R.integer.number_4));
                int marginLeft = Util.dpToPx(CommonTags.number_16);
                int marginRight = Util.dpToPx(CommonTags.number_16);
                int marginTop = Util.dpToPx(getResources().getInteger(R.integer.number_4));
                Spinner spinner = new Spinner(this);
                spinner.setId(CommonTags.number_1000+kitItemList.get(index).getId()+kitItemList.get(index).getId());
                KitSizeSpinnerAdapter kitItemSizesAdapter = new KitSizeSpinnerAdapter(this);
                spinner.setAdapter(kitItemSizesAdapter);
                spinner.setSelected(true);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
                customCheckbox.setLayoutParams(params);
                LinearLayout.LayoutParams params1= new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                params1.setMargins(marginLeft, marginTop, marginRight, marginBottom);
                spinner.setLayoutParams(params1);
                replacItemLayout.addView(customCheckbox);
                replacItemLayout.addView(spinner);
                spinner.setVisibility(View.GONE);

                if(!TextUtils.isEmpty(jsonData)){
                    if(kitItemRequestItemList != null && kitItemRequestItemList.size() != 0){
                        for(int i=0;i < kitItemRequestItemList.size();i++){
                            if(kitItemRequestItemList.get(i).getKitItemId() == kitItemList.get(index).getId()){
                                customCheckbox.setChecked(true);
                                kitItemSizeIds.put(customCheckbox.getId(),null);
                                if(kitItemRequestItemList.get(i).getKitItemSizeId() != null) {
                                    setSpinner(customCheckbox, kitItemRequestItemList.get(i).getKitItemSizeId());
                                }
                            }
                        }
                    }
                }

                customCheckbox.setOnCheckedChangeListener(this);
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_kit_replace, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.kit_details_done) {
            validateFields();

        }

        return super.onOptionsItemSelected(item);
    }

    private void validateFields() {
        kitItemRequestItemList = new ArrayList<>();
        if(kitItemSizeIds != null && kitItemSizeIds.size() == 0){
            snackBarWithMesg(replacItemLayout,getResources().getString(R.string.kit_items_selection_msg));
        }
        else  if(!checkSignatureCaptured()){
            snackBarWithMesg(replacItemLayout,getResources().getString(R.string.Signature_Mandatory_Msg));
        }
        else
        {
            if(kitItemSizeIds != null ){
                kitItemSizeIds.values();
                Set<Integer> kitItemIds = kitItemSizeIds.keySet();
                for(Integer kitItemId : kitItemIds){
                    KitItemRequestItem kitItemRequestItem = new KitItemRequestItem();
                    kitItemRequestItem.setKitItemId(kitItemId);
                    kitItemRequestItem.setKitItemSizeId(kitItemSizeIds.get(kitItemId));
                    kitItemRequestItemList.add(kitItemRequestItem);
                }
            }
            kitItemRequestMO.setRequestedOn(dateTimeFormatConversionUtil.getCurrentDateTime());
            String json = IOPSApplication.getGsonInstance().toJson(kitItemRequestItemList);
            kitItemRequestMO.setRequestedItems(json);
            String jsonKitItemString = IOPSApplication.getGsonInstance().toJson(kitItemRequestMO);
            storeAddKitRequestJson(jsonKitItemString);
            //appPreference.setAddKitRequestModel(jsonKitItemString);
            finish();
        }
    }

    private void storeAddKitRequestJson(String jsonKitItemRequest) {
        AddKitRequestMo addKitRequestMo = new AddKitRequestMo();
        addKitRequestMo.setAddKitRequestJson(jsonKitItemRequest);
        addKitRequestMo.setTaskId(taskId);
        appPreferences.setAddKitRequest(employeeNo,addKitRequestMo);
    }

    @Override
    public void onCheckedChanged(@NonNull  final CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            setSpinner(buttonView,0);
        } else {
            kitItemSizeIds.remove(buttonView.getId());
            View view  = replacItemLayout.findViewById(CommonTags.number_1000+buttonView.getId() + buttonView.getId());
            if (view instanceof  Spinner) {
                Spinner spinner = (Spinner) view;
                spinner.setVisibility(View.GONE);
            }
        }

    }

    private void setSpinner(final View buttonView, int kitItemSizeId) {
        final ArrayList<KitSizeMO> kitSizeList = IOPSApplication.getInstance().getKitItemSelectionStatementDB().getKitSizeList(buttonView.getId());
        if(kitSizeList != null && kitSizeList.size() !=0){
            View view  =  replacItemLayout.findViewById(CommonTags.number_1000+buttonView.getId() + buttonView.getId());
            if (view instanceof  Spinner) {
                Spinner spinner = (Spinner) view;
                spinner.setVisibility(View.VISIBLE);
                SpinnerAdapter spinnerAdapter = spinner.getAdapter();
                KitSizeSpinnerAdapter kitSizeSpinnerAdapter = (KitSizeSpinnerAdapter) spinnerAdapter;
                kitSizeSpinnerAdapter.setSpinnerData(kitSizeList);
                kitSizeSpinnerAdapter.notifyDataSetChanged();
                if(kitItemSizeId != 0){
                    if(kitSizeList != null && kitSizeList.size() != 0) {
                        for (int index = 0; index < kitSizeList.size(); index++) {
                            if (kitSizeList.get(index).getKitItemSizeId() == kitItemSizeId) {
                                spinner.setSelection(index, false);
                                kitItemSizeIds.put(buttonView.getId(), kitSizeList.get(index).getKitItemSizeId());
                            }
                        }
                    }
                }
                else{
                    spinner.setSelection(0,false);
                    kitItemSizeIds.put(buttonView.getId(), kitSizeList.get(0).getKitItemSizeId());
                }


                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        kitItemSizeIds.put(buttonView.getId(), kitSizeList.get(position).getKitItemSizeId());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

        }
        else{
            kitItemSizeIds.put(buttonView.getId(),null);
        }

    }


    @Override
    public void onClick(@NonNull View v) {
        Util.mSequenceNumber++;
        switch (v.getId()) {
            case R.id.add_signature_guard_level:
                Intent signatureActivity = new Intent(this, SignatureActivity.class);
                signatureActivity.putExtra(TableNameAndColumnStatement.GUARD_CODE,employeeNo);
                startActivityForResult(signatureActivity, Util.CAPTURE_SIGNATURE);

                break;
            case R.id.overAllTurnoutImage:
                Intent capturePictureActivity = new Intent(AddKitRequestActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.KIT_ITEM_REQUEST);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.KIT_REPLACE_IMAGE_REQUEST_CODE);
                break;


        }
    }

    private synchronized void removeImageFromAttachmentModel() {

        List<AttachmentMetaDataModel> attachmentMetaArrayList = getAttachmentMetaArrayList();

        if(null!=attachmentMetaArrayList && attachmentMetaArrayList.size()!=0){

            for (AttachmentMetaDataModel modelData :attachmentMetaArrayList) {
                if(modelData.getAttachmentSourceCode().equalsIgnoreCase(employeeNo)  &&  modelData.getScreenName().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST) &&
                        modelData.getimageCode().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST)){
                    attachmentMetaArrayList.remove(modelData);
                }
            }


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (Util.CAPTURE_SIGNATURE == requestCode) {
                removeSignature();
                attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                addSignatureGuardLevel.setImageURI(Uri.parse(attachmentMetaDataModel.getAttachmentPath()));
                attachmentMetaDataModel.setAttachmentSourceCode(employeeNo);
                attachmentMetaDataModel.setimageCode(CommonTags.ADD_KIT_REQUEST);
                attachmentMetaDataModel.setScreenName(CommonTags.ADD_KIT_REQUEST);
                addSignatureGuardLevel.setTag(attachmentMetaDataModel);
                setAttachmentMetaDataModel(attachmentMetaDataModel);
                attachmentMetaDataModel=null;
            } else if (requestCode == Constants.KIT_REPLACE_IMAGE_REQUEST_CODE) {
                removeGuardImage();
                attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                String imagePath = attachmentMetaDataModel.getAttachmentPath();
                kitReplaceImage.setImageURI(Uri.parse(imagePath));
                attachmentMetaDataModel.setAttachmentSourceCode(employeeNo);
                attachmentMetaDataModel.setimageCode(CommonTags.ADD_KIT_REQUEST_IMAGE);
                attachmentMetaDataModel.setScreenName(CommonTags.ADD_KIT_REQUEST);
                kitReplaceImage.setTag(attachmentMetaDataModel);
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private boolean checkSignatureCaptured() {
        boolean isSignatureCaptured;
        if (addSignatureGuardLevel.getDrawable().getConstantState() ==
                getResources().getDrawable(R.mipmap.ic_addsign).getConstantState()) {
            isSignatureCaptured = false;
        } else {
            isSignatureCaptured = true;
        }
        return isSignatureCaptured;
    }





    private synchronized void getImageFromAttachmentmodel() {
        List<AttachmentMetaDataModel> attachmentMetaArrayList = getAttachmentMetaArrayList();

        if(null!=attachmentMetaArrayList && attachmentMetaArrayList.size()!=0){

            for(AttachmentMetaDataModel metaDataModel :attachmentMetaArrayList){
                if(metaDataModel.getAttachmentSourceCode() != null && metaDataModel.getAttachmentSourceCode().equalsIgnoreCase(employeeNo) &&  metaDataModel.getScreenName() != null &&
                        metaDataModel.getScreenName().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST) && metaDataModel.getimageCode() != null && metaDataModel.getimageCode().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST_IMAGE) )
                        {
                            kitReplaceImage.setImageURI(Uri.parse(metaDataModel.getAttachmentPath()));
                        }
                      }

                }
        else{
            kitReplaceImage.setImageResource(R.mipmap.ic_guardphoto);
        }
    }
    private synchronized void getSignatureFromAttachmentmodel() {
        List<AttachmentMetaDataModel> attachmentMetaArrayList = getAttachmentMetaArrayList();

        if(null!=attachmentMetaArrayList && attachmentMetaArrayList.size()!=0){

            for(AttachmentMetaDataModel metaDataModel :attachmentMetaArrayList){
                if(metaDataModel.getAttachmentSourceCode() != null && metaDataModel.getAttachmentSourceCode().equalsIgnoreCase(employeeNo) &&  metaDataModel.getScreenName() != null &&
                        metaDataModel.getScreenName().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST) && metaDataModel.getimageCode() != null && metaDataModel.getimageCode().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST))
                {
                    addSignatureGuardLevel.setImageURI(Uri.parse(metaDataModel.getAttachmentPath()));

                }

            }
        }
        else{
            addSignatureGuardLevel.setImageResource(R.mipmap.ic_addsign);
        }
    }


    private synchronized void removeImageAttachmentModel(AttachmentMetaDataModel metaDataModel) {
        List<AttachmentMetaDataModel> attachmentMetaArrayList = getAttachmentMetaArrayList();

        if (null != metaDataModel) {
            if (metaDataModel.getAttachmentSourceCode().equalsIgnoreCase(employeeNo) &&
                    metaDataModel.getimageCode().equalsIgnoreCase(CommonTags.ADD_KIT_REQUEST)) {
                attachmentMetaArrayList.remove(metaDataModel);
            } else {
                attachmentMetaArrayList.remove(metaDataModel);
            }
        }
    }

    private synchronized void removeSignature() {
        addSignatureGuardLevel.setImageResource(R.mipmap.ic_addsign);
        AttachmentMetaDataModel attachmentMetaDataModel = (AttachmentMetaDataModel) addSignatureGuardLevel.getTag();
        removeImageAttachmentModel(attachmentMetaDataModel);

    }

    private synchronized void removeGuardImage() {
        kitReplaceImage.setImageResource(R.mipmap.ic_guardphoto);
        AttachmentMetaDataModel attachmentMetaDataModel = (AttachmentMetaDataModel) kitReplaceImage.getTag();
        removeImageAttachmentModel(attachmentMetaDataModel);
    }
}