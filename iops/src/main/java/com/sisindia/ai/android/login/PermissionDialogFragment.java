package com.sisindia.ai.android.login;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.sisindia.ai.android.R;

/**
 * Created by Durga Prasad on 13-06-2016.
 */
public class PermissionDialogFragment extends DialogFragment {


    private android.widget.AdapterView.OnItemClickListener onItemClickListener;
    public static final String TAG = "PermissionDialogFragment";
    private View view;
    private LayoutInflater layoutInflater;
    private OnAcceptPermissionListener onAcceptPermissionListener;
    private  String message;
    private String  header;
    private boolean isGpsConfiguraion;


    public PermissionDialogFragment() {
        // Required empty public constructor
    }

    public  void setMessage(String message,boolean isGpsConfiguraion){
        this.message = message;
        this.isGpsConfiguraion = isGpsConfiguraion;
    }

    public void setHeaderTitle(String header) {
        this.header = header;
    }

    public static PermissionDialogFragment newInstance() {
        PermissionDialogFragment permissionDialogFragment = new PermissionDialogFragment();
        Bundle args = new Bundle();
        permissionDialogFragment.setArguments(args);
        return permissionDialogFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnAcceptPermissionListener){
            onAcceptPermissionListener = (OnAcceptPermissionListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutInflater = LayoutInflater.from(getActivity());

    }




    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        setCancelable(false);
        view = layoutInflater.inflate(R.layout.permission_dialog_content,null);
        TextView headerTextView =  (TextView) view.findViewById(R.id.navigationBackContentTv);
        TextView messageTextView = (TextView) view.findViewById(R.id.permissionsList);
        messageTextView.setText(message);
        headerTextView.setText(header);
        builder.setView(view);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            onAcceptPermissionListener.onPermissionsAccepted();
            }
        });



        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if(!isGpsConfiguraion) {
            onAcceptPermissionListener.onPermissionsAccepted();
        }
    }
}


