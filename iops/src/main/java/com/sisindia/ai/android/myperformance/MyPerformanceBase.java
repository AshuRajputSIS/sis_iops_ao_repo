
package com.sisindia.ai.android.myperformance;

import com.google.gson.annotations.SerializedName;

public class MyPerformanceBase {

    @SerializedName("StatusCode")
    private Integer statusCode;

    @SerializedName("StatusMessage")
    private String statusMessage;

    @SerializedName("Data")
    private Data data;

public Integer getStatusCode() {
return statusCode;
}

public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

public String getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

public Data getData() {
return data;
}

public void setData(Data data) {
this.data = data;
}

}