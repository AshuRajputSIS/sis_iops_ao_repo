package com.sisindia.ai.android.database.unitDTO;

import java.util.List;

/**
 * Created by compass on 5/31/2017.
 */

public class UnitPostGeoCoordinates {
    public double latitude;
    public double longitude;
    public boolean isMainGate;
}
