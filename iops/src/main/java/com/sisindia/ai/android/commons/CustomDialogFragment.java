package com.sisindia.ai.android.commons;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.sisindia.ai.android.R;

/**
 * Created by Durga Prasad on 09-06-2016.
 */
public class CustomDialogFragment extends DialogFragment {


    private AdapterView.OnItemClickListener onItemClickListener;
    public static final String TAG = "CustomDialogFragment";
    private View view;
    private View titleView;
    private String message = "";
    private int dialogType;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private DialogClickListener dialogClickListener;


    public CustomDialogFragment() {
        // Required empty public constructor
    }


    public static CustomDialogFragment newInstance() {
        CustomDialogFragment customDialogFragment = new CustomDialogFragment();
        Bundle args = new Bundle();
        customDialogFragment.setArguments(args);
        return customDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if (context instanceof DialogClickListener) {
            dialogClickListener = (DialogClickListener) context;
        }
    }

    public void setLayout(int whichPage) {
        this.dialogType = whichPage;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View titleView = LayoutInflater.from(mContext).inflate(R.layout.dialog_title, null);
        TextView dialogTitle = (TextView) titleView.findViewById(R.id.dialogTitle);


        view = LayoutInflater.from(mContext).inflate(R.layout.warning_msg, null);
        TextView content = (TextView) view.findViewById(R.id.warningContentTv);
        switch (dialogType) {
            case 1:
                if (message != null && message.length() != 0) {
                    content.setText(message);
                }
                dialogTitle.setText(getResources().getString(R.string.WARNING));
                dialogTitle.setTextColor(getResources().getColor(R.color.background_red));
                builder.setCustomTitle(titleView);
                builder = setPositiveNegativeButtons(builder, getResources().getString(R.string.ok));
                break;
        }
        builder.setView(view);
        return builder.create();
    }

    private AlertDialog.Builder setPositiveNegativeButtons(AlertDialog.Builder builder, String buttonCaption) {
        builder.setNegativeButton(buttonCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }
}

