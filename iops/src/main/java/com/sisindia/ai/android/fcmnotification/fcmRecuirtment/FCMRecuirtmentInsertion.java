package com.sisindia.ai.android.fcmnotification.fcmRecuirtment;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.BaseRecruitmentOR;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecuirtementMO;

public class FCMRecuirtmentInsertion extends SISAITrackingDB {
    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite=null;

    public FCMRecuirtmentInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertRecuirtmentModel(BaseRecruitmentOR baseRecruitmentOR) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if(baseRecruitmentOR != null) {
                    sqlite.beginTransaction();
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().
                            deleteRecordById(baseRecruitmentOR.getData().getRecruitmentId(),
                                    TableNameAndColumnStatement.RECRUIT_ID,
                                    TableNameAndColumnStatement.RECRUITMENT_TABLE,
                                    sqlite);
                    insertRecuirtmentTable(baseRecruitmentOR.getData());
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertRecuirtmentTable(RecuirtementMO recuirtementMO) {
        if (recuirtementMO != null) {
            ContentValues values = new ContentValues();
            values.put(TableNameAndColumnStatement.RECRUIT_ID, recuirtementMO.getRecruitmentId());
            values.put(TableNameAndColumnStatement.FULL_NAME, recuirtementMO.getFullName());
            values.put(TableNameAndColumnStatement.CONTACT_NO, recuirtementMO.getContactNo());
            values.put(TableNameAndColumnStatement.DATE_OF_BIRTH, recuirtementMO.getDOB());
            values.put(TableNameAndColumnStatement.IS_SELECTED, recuirtementMO.getIsSelected());
            values.put(TableNameAndColumnStatement.IS_REJECTED, recuirtementMO.getIsRejected());
            values.put(TableNameAndColumnStatement.ACTION_TAKEN_ON, recuirtementMO.getActionTakenOn());
            values.put(TableNameAndColumnStatement.STATUS ,recuirtementMO.getStatus());
            values.put(TableNameAndColumnStatement.IS_SYNCED , Constants.IS_SYNCED);
            sqlite.insertOrThrow(TableNameAndColumnStatement.RECRUITMENT_TABLE, null, values);
        }

    }
}