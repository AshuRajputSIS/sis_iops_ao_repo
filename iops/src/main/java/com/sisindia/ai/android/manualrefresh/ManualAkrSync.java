package com.sisindia.ai.android.manualrefresh;

import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.AkrSyncListener;
import com.sisindia.ai.android.annualkitreplacement.AkrUpdateAsyncTask;
import com.sisindia.ai.android.annualkitreplacement.BaseAkrSync;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitDistributionItemTableCountMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 6/1/2017.
 */

public class ManualAkrSync {

    public static void getAkrApiCall(final AkrSyncListener akrSyncListener, final Context mContext, final boolean isDestroyed) {
        if (tablesToSync()) {
            showprogressbar(mContext);
            new SISClient(mContext).getApi().getAllAkr(new Callback<BaseAkrSync>() {
                @Override
                public void success(BaseAkrSync akrSyncResponse, Response response) {
                    Timber.d("AkrSyncResponse response  %s", akrSyncResponse);
                    switch (akrSyncResponse.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            if (akrSyncResponse.fcmAkrDistributionData != null && akrSyncResponse.fcmAkrDistributionData.size() > 0)
                                new AkrUpdateAsyncTask(mContext, akrSyncResponse, akrSyncListener).execute();
                            else {
                                hideprogressbar(isDestroyed);
                                Util.showToast(mContext, "Nothing to update");
                            }
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(mContext, mContext.getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar(isDestroyed);
                    Timber.e(error, " AKR Error : ");
                    Crashlytics.logException(error);
                }
            });
        } else {
            Util.showToast(mContext, mContext.getString(R.string.please_sync_older_records));
        }
    }

    private static void showprogressbar(Context mContext) {
        Util.showProgressBar(mContext, R.string.loading_please_wait);
    }

    private static void hideprogressbar(boolean isDetroyed) {
        Util.dismissProgressBar(isDetroyed);
    }

    private synchronized static boolean tablesToSync() {
        KitDistributionItemTableCountMO kitDistributionItemTableCount = IOPSApplication.getInstance().getTableSyncCount().getKitDistributionItemTableCount();

        if (kitDistributionItemTableCount != null && kitDistributionItemTableCount.getSyncCount() != 0)
            return false;
        else
            return true;
    }
}
