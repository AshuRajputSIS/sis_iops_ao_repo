package com.sisindia.ai.android.unitatriskpoa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 15/7/16.
 */

public class CompletedCheckListAdapter extends  RecyclerView.Adapter<CompletedCheckListAdapter.CheckListViewHolder>  {
    private final DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private List<ActionPlanMO> completedCheckList;
    Context mcontext;

    private int pos;


    public CompletedCheckListAdapter(Context context, List<ActionPlanMO> completedCheckListMO) {
        this.mcontext = context;
        this.completedCheckList =completedCheckListMO;
        this.dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

    }




    @Override
    public int getItemCount() {

        return completedCheckList.size();

    }


    @Override
    public CheckListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.action_plan_checklist, parent, false);
        CheckListViewHolder checkListViewHolder = new CheckListViewHolder(view);

        return checkListViewHolder;
    }

    @Override
    public void onBindViewHolder(CheckListViewHolder holder, int position) {


        pos =position;
        CheckListViewHolder checkListViewHolder = holder;
        checkListViewHolder.itemName.setText("POA Name : " +completedCheckList.get(pos).getActionPlan());
        checkListViewHolder.assignName.setText("Assigned To : "+
                completedCheckList.get(pos).getAssignToName());
        checkListViewHolder.plannedDate.setText("Planned Date : "+
                dateTimeFormatConversionUtil.
                        convertDateTimeToDate(completedCheckList.get(pos).getPlanDate()));
        checkListViewHolder.completedDate.setVisibility(View.VISIBLE);
        checkListViewHolder.completedDate.setText("Completed Date : "+
                dateTimeFormatConversionUtil.
                        convertDateTimeToDate(completedCheckList.get(pos).getCompletedDate()));

        checkListViewHolder.reasonLable.setText("Reason :"+completedCheckList.get(pos).getReason());
        checkListViewHolder.checkBox.setClickable(false);
        checkListViewHolder.checkBox.setChecked(true);


       //checkListViewHolder.checkBox.setOnClickListener(this);





    }



    public class CheckListViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.checkBox_item)
        CheckBox checkBox;
        @Bind(R.id.planned_date)
        CustomFontTextview plannedDate;
        @Bind(R.id.item_name)
        CustomFontTextview itemName;
        @Bind(R.id.assigned_to)
        CustomFontTextview assignName;
        @Bind(R.id.complete_date)
        CustomFontTextview completedDate;
        @Bind(R.id.reasonLable)
        CustomFontTextview reasonLable;





        public CheckListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }




    }


}



