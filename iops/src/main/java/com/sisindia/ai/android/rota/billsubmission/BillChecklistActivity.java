package com.sisindia.ai.android.rota.billsubmission;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionCheckListBaseMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionCheckListItemMO;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BillChecklistActivity extends BaseActivity implements OnRecyclerViewItemClickListener {

    @Bind(R.id.bill_checklist_toolbar)
    Toolbar mToolBar;
    @Bind(R.id.bill_checklist_recyclerview)
    RecyclerView bill_checklist_recyclerview;

    private BillSubmissionCheckListBaseMO checkListBaseMO;
    private AddBillCheckListAdapter addBillCheckListAdapter;
    private Drawable dividerDrawable;
    ArrayList<BillSubmissionCheckListItemMO> checkListItemMOList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_checklist);
        ButterKnife.bind(this);

        setSupportActionBar(mToolBar);

        ActionBar actionar = getSupportActionBar();

        if (actionar != null) {
            actionar.setHomeButtonEnabled(true);
            actionar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.billing_checklist);
        }


        initViews();
    }

    private void initViews() {

        checkListBaseMO = (BillSubmissionCheckListBaseMO) getIntent().getSerializableExtra("checklistItem");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        bill_checklist_recyclerview.setLayoutManager(linearLayoutManager);
        bill_checklist_recyclerview.setItemAnimator(new DefaultItemAnimator());
        addBillCheckListAdapter = new AddBillCheckListAdapter(this);

        checkListItemMOList = (ArrayList) checkListBaseMO.getBillSubmissionCheckListMO();
        addBillCheckListAdapter.setCheckListData(checkListItemMOList);

        bill_checklist_recyclerview.setAdapter(addBillCheckListAdapter);

        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        bill_checklist_recyclerview.addItemDecoration(dividerItemDecoration);
        addBillCheckListAdapter.setOnRecyclerViewItemClickListener(this);
        addBillCheckListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ArrayList<BillSubmissionCheckListItemMO> billUpdatedcheckedList = null;
        Intent intent = new Intent();
        intent.putExtra("checkList", billUpdatedcheckedList);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_billsubmission_check_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done) {
            ArrayList<BillSubmissionCheckListItemMO> billUpdatedcheckedList= addBillCheckListAdapter.getUpdatedList();
            Intent intent =new Intent();
            intent.putExtra("checkList",billUpdatedcheckedList);
            setResult(RESULT_OK,intent);
            finish();
        }
        if (id == android.R.id.home) {
            ArrayList<BillSubmissionCheckListItemMO> billUpdatedcheckedList = null;
            Intent intent = new Intent();
            intent.putExtra("checkList", billUpdatedcheckedList);
            setResult(RESULT_CANCELED, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
