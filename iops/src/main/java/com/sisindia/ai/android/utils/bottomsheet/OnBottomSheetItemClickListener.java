package com.sisindia.ai.android.utils.bottomsheet;

import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitDetails;

import java.util.List;

/**
 * Created by Durga Prasad on 29-04-2016.
 */
public interface OnBottomSheetItemClickListener {

    void onSheetItemClick(String itemSelected, int viewId);

    void onUnitSheetItemClick(MyUnitHomeMO unitHomeMOList, int viewId);
}
