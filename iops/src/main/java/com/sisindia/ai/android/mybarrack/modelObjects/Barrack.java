package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.myunits.models.BarrackStrength;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Barrack implements Serializable {

    @SerializedName("Id")
    @Expose
    public Integer Id;
    @SerializedName("Name")
    @Expose
    public String Name;
    @SerializedName("Description")
    @Expose
    public String Description;
    @SerializedName("AddressId")
    @Expose
    public String AddressId;
    @SerializedName("OwnerContactId")
    @Expose
    public Integer OwnerContactId;
    @SerializedName("OwnerAddressId")
    @Expose
    public Integer OwnerAddressId;
    @SerializedName("InChargeId")
    @Expose
    public Integer InChargeId;
    @SerializedName("IsMessAvailable")
    @Expose
    public Boolean IsMessAvailable;
    @SerializedName("MessVendorId")
    @Expose
    public Integer MessVendorId;
    @SerializedName("AreaInspectorId")
    @Expose
    public Integer AreaInspectorId;
    @SerializedName("IsCustomerProvided")
    @Expose
    public Boolean IsCustomerProvided;
    @SerializedName("BarrackAddress")
    @Expose
    public String BarrackAddress;
    @SerializedName("OwnerContactName")
    @Expose
    public String OwnerContactName;
    @SerializedName("OwnerAddress")
    @Expose
    public String OwnerAddress;
    @SerializedName("InChargeName")
    @Expose
    public String InChargeName;
    @SerializedName("AreaInspectorName")
    @Expose
    public String AreaInspectorName;
    @SerializedName("GeoLatitude")
    @Expose
    public Double GeoLatitude=0.0;
    @SerializedName("GeoLongitude")
    @Expose
    public Double GeoLongitude=0.0;

    @SerializedName("BarrackStrength")
    @Expose
    private BarrackStrength barrackStrengthList = new BarrackStrength();

    @SerializedName("BranchId")
    @Expose
    public Integer BranchId;


    public Integer sequenceId;
    private String belongsTo;

    @SerializedName("DeviceNo")
    @Expose
    public String barrackNfcID;

    private int barrackID;
    private String barrackName;
    private String barrackGeoPoints;

    public String getBarrackGeoPoints() {
        return barrackGeoPoints;
    }

    public void setBarrackGeoPoints(String barrackGeoPoints) {
        this.barrackGeoPoints = barrackGeoPoints;
    }

    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }

    public String getBarrackNfcID() {
        return barrackNfcID;
    }

    public void setBarrackNfcID(String barrackNfcID) {
        this.barrackNfcID = barrackNfcID;
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }
    public BarrackStrength getBarrackStrengthList() {
        return barrackStrengthList;
    }

    public void setBarrackStrengthList(BarrackStrength barrackStrengthList) {
        this.barrackStrengthList = barrackStrengthList;
    }

    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        BranchId = branchId;
    }


    public String getAddressId() {
        return AddressId;
    }

    public void setAddressId(String addressId) {
        AddressId = addressId;
    }

    public Integer getAreaInspectorId() {
        return AreaInspectorId;
    }

    public void setAreaInspectorId(Integer areaInspectorId) {
        AreaInspectorId = areaInspectorId;
    }

    public String getAreaInspectorName() {
        return AreaInspectorName;
    }

    public void setAreaInspectorName(String areaInspectorName) {
        AreaInspectorName = areaInspectorName;
    }

    public String getBarrackAddress() {
        return BarrackAddress;
    }

    public void setBarrackAddress(String barrackAddress) {
        BarrackAddress = barrackAddress;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Double getGeoLatitude() {
        return GeoLatitude;
    }

    public void setGeoLatitude(Double geoLatitude) {
        GeoLatitude = geoLatitude;
    }

    public Double getGeoLongitude() {
        return GeoLongitude;
    }

    public void setGeoLongitude(Double geoLongitude) {
        GeoLongitude = geoLongitude;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getInChargeId() {
        return InChargeId;
    }

    public void setInChargeId(Integer inChargeId) {
        InChargeId = inChargeId;
    }

    public String getInChargeName() {
        return InChargeName;
    }

    public void setInChargeName(String inChargeName) {
        InChargeName = inChargeName;
    }

    public Boolean getCustomerProvided() {
        return IsCustomerProvided;
    }

    public void setCustomerProvided(Boolean customerProvided) {
        IsCustomerProvided = customerProvided;
    }

    public Boolean getMessAvailable() {
        return IsMessAvailable;
    }

    public void setMessAvailable(Boolean messAvailable) {
        IsMessAvailable = messAvailable;
    }

    public Integer getMessVendorId() {
        return MessVendorId;
    }

    public void setMessVendorId(Integer messVendorId) {
        MessVendorId = messVendorId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOwnerAddress() {
        return OwnerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        OwnerAddress = ownerAddress;
    }

    public Integer getOwnerAddressId() {
        return OwnerAddressId;
    }

    public void setOwnerAddressId(Integer ownerAddressId) {
        OwnerAddressId = ownerAddressId;
    }

    public Integer getOwnerContactId() {
        return OwnerContactId;
    }

    public void setOwnerContactId(Integer ownerContactId) {
        OwnerContactId = ownerContactId;
    }

    public String getOwnerContactName() {
        return OwnerContactName;
    }

    public void setOwnerContactName(String ownerContactName) {
        OwnerContactName = ownerContactName;
    }

    public void setBelongsTo(String belongsTo) {
        this.belongsTo = belongsTo;
    }

    public String getBelongsTo() {
        return belongsTo;
    }

    public int getBarrackID() {
        return barrackID;
    }

    public void setBarrackID(int barrackID) {
        this.barrackID = barrackID;
    }
}