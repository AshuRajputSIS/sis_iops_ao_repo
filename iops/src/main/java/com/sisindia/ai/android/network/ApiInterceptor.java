package com.sisindia.ai.android.network;

import com.sisindia.ai.android.utils.NetworkUtil;
import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Created by Durga Prasad on 26-10-2016.
 */
public class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        CacheControl cacheControl = new CacheControl.Builder()
                .maxAge(1, TimeUnit.DAYS)
                .build();

        return response.newBuilder()
                .header("CACHE_CONTROL", cacheControl.toString()) // 1 day
                .build();
    }
}