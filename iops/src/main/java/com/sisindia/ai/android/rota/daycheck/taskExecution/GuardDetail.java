package com.sisindia.ai.android.rota.daycheck.taskExecution;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardDetail implements Parcelable {

    @SerializedName("EmployeeId")
    @Expose
    private int employeeId;

    @SerializedName("EmployeeNo")
    @Expose
    private String employeeNo;

    @SerializedName("GuardName")
    @Expose
    private String guardName;

    @SerializedName("AvailableAtPost")
    @Expose
    private String availableAtPost;

    @SerializedName("ProperShave")
    @Expose
    private String properShave;

    @SerializedName("HairCut")
    @Expose
    private String hairCut;

    @SerializedName("ShoePolish")
    @Expose
    private String shoePolish;

    @SerializedName("FullUniform")
    @Expose
    private String fullUniform;

    @SerializedName("CleanUniform")
    @Expose
    private String cleanUniform;

    @SerializedName("ProperFittingUniform")
    @Expose
    private String properFittingUniform;

    @SerializedName("WellPressedUniform")
    @Expose
    private String wellPressedUniform;

    @SerializedName("IDCard")
    @Expose
    private String iDCard;

    @SerializedName("OverAllTurnout")
    @Expose
    private String overAllTurnout;

    @SerializedName("Amount")
    @Expose
    private String amount;

    @SerializedName("KnowledgeAboutDutyPost")
    @Expose
    private String knowledgeAboutDutyPost;

    @SerializedName("PerformingDutyAtPostSince")
    @Expose
    private String performingDutyAtPostSince;

    public String mGuardImageUri;

    @SerializedName("GEOLocation")
    private String geoLocation;

    //@Ashu Rajput adding extra entity w.r.t QR Scan Status
    @SerializedName("IsScannedFromQR")
    private int isScannedFromQR;

    @SerializedName("QRScanComments")
    private String QRScanComments;

    @SerializedName("IsQRScanWorking")
    private int isQRScanWorking;

    public String getQRScanComments() {
        return QRScanComments;
    }

    public void setQRScanComments(String QRScanComments) {
        this.QRScanComments = QRScanComments;
    }

    public int getIsScannedFromQR() {
        return isScannedFromQR;
    }

    public void setIsScannedFromQR(int isScannedFromQR) {
        this.isScannedFromQR = isScannedFromQR;
    }

    public int getIsQRScanWorking() {
        return isQRScanWorking;
    }

    public void setIsQRScanWorking(int isQRScanWorking) {
        this.isQRScanWorking = isQRScanWorking;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public String getAvailableAtPost() {
        return availableAtPost;
    }

    public void setAvailableAtPost(String availableAtPost) {
        this.availableAtPost = availableAtPost;
    }

    public String getProperShave() {
        return properShave;
    }

    public void setProperShave(String properShave) {
        this.properShave = properShave;
    }

    public String getHairCut() {
        return hairCut;
    }

    public void setHairCut(String hairCut) {
        this.hairCut = hairCut;
    }

    public String getShoePolish() {
        return shoePolish;
    }

    public void setShoePolish(String shoePolish) {
        this.shoePolish = shoePolish;
    }

    public String getFullUniform() {
        return fullUniform;
    }

    public void setFullUniform(String fullUniform) {
        this.fullUniform = fullUniform;
    }

    public String getCleanUniform() {
        return cleanUniform;
    }

    public void setCleanUniform(String cleanUniform) {
        this.cleanUniform = cleanUniform;
    }

    public String getProperFittingUniform() {
        return properFittingUniform;
    }

    public void setProperFittingUniform(String properFittingUniform) {
        this.properFittingUniform = properFittingUniform;
    }

    public String getWellPressedUniform() {
        return wellPressedUniform;
    }

    public void setWellPressedUniform(String wellPressedUniform) {
        this.wellPressedUniform = wellPressedUniform;
    }

    public String getIDCard() {
        return iDCard;
    }

    public void setIDCard(String iDCard) {
        this.iDCard = iDCard;
    }

    public String getOverAllTurnout() {
        return overAllTurnout;
    }

    public void setOverAllTurnout(String overAllTurnout) {
        this.overAllTurnout = overAllTurnout;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getKnowledgeAboutDutyPost() {
        return knowledgeAboutDutyPost;
    }

    public void setKnowledgeAboutDutyPost(String knowledgeAboutDutyPost) {
        this.knowledgeAboutDutyPost = knowledgeAboutDutyPost;
    }

    public String getPerformingDutyAtPostSince() {
        return performingDutyAtPostSince;
    }

    public void setPerformingDutyAtPostSince(String performingDutyAtPostSince) {
        this.performingDutyAtPostSince = performingDutyAtPostSince;
    }

    public String getmGuardImageUri() {
        return mGuardImageUri;
    }

    public void setmGuardImageUri(String mGuardImageUri) {
        this.mGuardImageUri = mGuardImageUri;
    }


    public GuardDetail() {
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.employeeId);
        dest.writeString(this.employeeNo);
        dest.writeString(this.guardName);
        dest.writeString(this.availableAtPost);
        dest.writeString(this.properShave);
        dest.writeString(this.hairCut);
        dest.writeString(this.shoePolish);
        dest.writeString(this.fullUniform);
        dest.writeString(this.cleanUniform);
        dest.writeString(this.properFittingUniform);
        dest.writeString(this.wellPressedUniform);
        dest.writeString(this.iDCard);
        dest.writeString(this.overAllTurnout);
        dest.writeString(this.amount);
        dest.writeString(this.knowledgeAboutDutyPost);
        dest.writeString(this.performingDutyAtPostSince);
        dest.writeString(this.mGuardImageUri);
        dest.writeString(this.geoLocation);
        dest.writeString(this.QRScanComments);
        dest.writeInt(this.isScannedFromQR);
        dest.writeInt(this.isQRScanWorking);
    }

    protected GuardDetail(Parcel in) {
        this.employeeId = in.readInt();
        this.employeeNo = in.readString();
        this.guardName = in.readString();
        this.availableAtPost = in.readString();
        this.properShave = in.readString();
        this.hairCut = in.readString();
        this.shoePolish = in.readString();
        this.fullUniform = in.readString();
        this.cleanUniform = in.readString();
        this.properFittingUniform = in.readString();
        this.wellPressedUniform = in.readString();
        this.iDCard = in.readString();
        this.overAllTurnout = in.readString();
        this.amount = in.readString();
        this.knowledgeAboutDutyPost = in.readString();
        this.performingDutyAtPostSince = in.readString();
        this.mGuardImageUri = in.readString();
        this.geoLocation = in.readString();
        this.QRScanComments = in.readString();
        this.isScannedFromQR = in.readInt();
        this.isQRScanWorking = in.readInt();
    }

    public static final Creator<GuardDetail> CREATOR = new Creator<GuardDetail>() {
        @Override
        public GuardDetail createFromParcel(Parcel source) {
            return new GuardDetail(source);
        }

        @Override
        public GuardDetail[] newArray(int size) {
            return new GuardDetail[size];
        }
    };
}