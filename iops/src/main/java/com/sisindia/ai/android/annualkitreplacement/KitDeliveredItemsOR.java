package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitDeliveredItemsOR {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private KitDeliveredItemsData kitDeliveredItemsData;

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public KitDeliveredItemsData getKitDeliveredItemsData() {
        return kitDeliveredItemsData;
    }

    public void setKitDeliveredItemsData(KitDeliveredItemsData kitDeliveredItemsData) {
        this.kitDeliveredItemsData = kitDeliveredItemsData;
    }
}