package com.sisindia.ai.android.rota.daycheck.taskExecution;


import com.google.gson.annotations.SerializedName;

public class SecurityRiskObservation {

@SerializedName("AuditBoundaryOrFencingofClientPremise")

private String auditBoundaryOrFencingofClientPremise;
@SerializedName("CheckLocksAtCriticalPoints")

private String checkLocksAtCriticalPoints;
@SerializedName("CCTVFunctional")

private String cCTVFunctional;
@SerializedName("LightingFuctional")

private String lightingFuctional;
@SerializedName("FireFightingEquipments")

private String fireFightingEquipments;

/**
* 
* @return
* The auditBoundaryOrFencingofClientPremise
*/
public String getAuditBoundaryOrFencingofClientPremise() {
return auditBoundaryOrFencingofClientPremise;
}

/**
* 
* @param auditBoundaryOrFencingofClientPremise
* The AuditBoundaryOrFencingofClientPremise
*/
public void setAuditBoundaryOrFencingofClientPremise(String auditBoundaryOrFencingofClientPremise) {
this.auditBoundaryOrFencingofClientPremise = auditBoundaryOrFencingofClientPremise;
}

/**
* 
* @return
* The checkLocksAtCriticalPoints
*/
public String getCheckLocksAtCriticalPoints() {
return checkLocksAtCriticalPoints;
}

/**
* 
* @param checkLocksAtCriticalPoints
* The CheckLocksAtCriticalPoints
*/
public void setCheckLocksAtCriticalPoints(String checkLocksAtCriticalPoints) {
this.checkLocksAtCriticalPoints = checkLocksAtCriticalPoints;
}

/**
* 
* @return
* The cCTVFunctional
*/
public String getCCTVFunctional() {
return cCTVFunctional;
}

/**
* 
* @param cCTVFunctional
* The CCTVFunctional
*/
public void setCCTVFunctional(String cCTVFunctional) {
this.cCTVFunctional = cCTVFunctional;
}

/**
* 
* @return
* The lightingFuctional
*/
public String getLightingFuctional() {
return lightingFuctional;
}

/**
* 
* @param lightingFuctional
* The LightingFuctional
*/
public void setLightingFuctional(String lightingFuctional) {
this.lightingFuctional = lightingFuctional;
}

/**
* 
* @return
* The fireFightingEquipments
*/
public String getFireFightingEquipments() {
return fireFightingEquipments;
}

/**
* 
* @param fireFightingEquipments
* The FireFightingEquipments
*/
public void setFireFightingEquipments(String fireFightingEquipments) {
this.fireFightingEquipments = fireFightingEquipments;
}

}