package com.sisindia.ai.android.commons;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.sisindia.ai.android.utils.Util;

/**
 * Created by shivam on 9/11/16.
 */

public class DeviceBootupReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Util.isServiceRunning(context, GpsTrackingService.class)) {
            Intent gpsIntent = new Intent(context, GpsTrackingService.class);
            startWakefulService(context, gpsIntent);
        }
    }
}