/*
package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.AddRotaTaskInput;
import com.sisindia.ai.android.network.request.RotaTaskInputRequestMO;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.network.response.TaskOutputMO;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

*/
/**
 * Created by Saruchi on 23-06-2016.
 * <p>
 * Syncing the Unit task table with server
 * <p>
 * all the DB related quires  i.e selection onDutyChange insertion are there in the particular file
 * you will find only syncing table db queries .
 *//*

public class RotaTaskSyncing extends CommonSyncDbOperation{
   private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, RotaTaskInputRequestMO> inputList;

    public RotaTaskSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        taskSyncing();
    }

    private void taskSyncing() {
         inputList = getAllTaskToSync();
        if (null != inputList && 0 != inputList.size()) {
            for (Integer key : inputList.keySet()) {
                RotaTaskInputRequestMO taskInputIMO = inputList.get(key);
                if (0 == taskInputIMO.getId()) {
                    final AddRotaTaskInput inputIMO = addTaskModel(taskInputIMO);
                    addTaskApiCall(inputIMO, key);
                } else {
                    if(taskInputIMO.getIsTaskStarted()==RotaStartTaskEnum.InProgress.getValue()){
                        //  updateIsTaskStarted(key, RotaStartTaskEnum.InActive.getValue());
                        startTaskApiCall(taskInputIMO,key);
                    }
                    else if(taskInputIMO.getIsTaskStarted()==RotaStartTaskEnum.InActive.getValue()){
                        updateTaskApicall(taskInputIMO, key);
                    }

                }


                Timber.d(Constants.TAG_SYNCADAPTER+"taskSyncing count   %s", "count: " + count);
                Timber.d(Constants.TAG_SYNCADAPTER+"taskSyncing   %s", "key: " + key + " value: " + inputList.get(key));
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing -->nothing to sync ");
            new IssuesSyncing(mContext);
            new UnitStrengthSyncing(mContext);
            new KitRequestSyncing(mContext);
            new NotificationSync(mContext);

        }
    }

    */
/**
     * fecthing the task details from the database
     *
     * @return
     *//*

    private HashMap<Integer, RotaTaskInputRequestMO>  getAllTaskToSync() {
        HashMap<Integer, RotaTaskInputRequestMO> rotaTaskList = new HashMap<>();
        String taskQuery = "SELECT  * FROM " + TableNameAndColumnStatement.TASK_TABLE + " where " +
                TableNameAndColumnStatement.TASK_STATUS_ID + " in ( " + 4 + "," + 5 + " ) or " +
                TableNameAndColumnStatement.TASK_ID + " = '" + 0 + "' or "+
                TableNameAndColumnStatement.IS_TASK_STARTED+ " = '" + RotaStartTaskEnum.InProgress.getValue() + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"taskQuery   %s", taskQuery);
        SQLiteDatabase sqlite=null;
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(taskQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        RotaTaskInputRequestMO rotaTaskMO = new RotaTaskInputRequestMO();
                        rotaTaskMO.setIsTaskStarted( cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_TASK_STARTED)));
                        rotaTaskMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        int unitId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        if(unitId==0){
                            rotaTaskMO.setUnitId(null);
                        }else {
                            rotaTaskMO.setUnitId(unitId);
                        }

                        rotaTaskMO.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                        rotaTaskMO.setAssigneeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setEstimatedExecutionTime(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME)));
                        rotaTaskMO.setActualExecutionTime(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME)));
                        rotaTaskMO.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                        rotaTaskMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTIONS)));
                        rotaTaskMO.setEstimatedTaskExecutionStartDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setEstimatedTaskExecutionEndDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionStartDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionEndDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME)));

                        int barrackId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID));
                        if (barrackId != 0) {
                            rotaTaskMO.setBarrackId(barrackId);

                        } else {
                            rotaTaskMO.setBarrackId(null);
                        }
                        rotaTaskMO.setIsAutoCreation(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)));
                        rotaTaskMO.setExecutorId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        rotaTaskMO.setCreatedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        String taskExecutionResult = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_EXECUTION_RESULT));
                        Timber.d(Constants.TAG_SYNCADAPTER+"taskExecutionResult jsonObj  %s", taskExecutionResult);
                        if (null != taskExecutionResult && taskExecutionResult.length() != 0) {
                            if (taskExecutionResult.equalsIgnoreCase("null")) {
                                Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing jsonObj  null %s", "null dont set");
                            } else {
                                JsonObject jsonObject = (new JsonParser()).parse(taskExecutionResult).getAsJsonObject();
                                rotaTaskMO.setTaskExecutionResult(jsonObject);
                                Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing jsonObj  %s", jsonObject.toString());
                            }
                        }
                        rotaTaskMO.setRelatedTaskId(0);
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)) == 0)
                        {
                            rotaTaskMO.setChangeRouteReasonId(null);
                        }
                        else{
                            rotaTaskMO.setChangeRouteReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)));
                        }

                        rotaTaskMO.setUnitPostId(null);
                        rotaTaskMO.setAdditionalComments("");
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)) == 0) {
                            rotaTaskMO.setApprovedById(null);
                        } else {
                            rotaTaskMO.setApprovedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)));

                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)) == 0) {
                            rotaTaskMO.setOtherReasonId(null);
                        } else {
                            rotaTaskMO.setOtherReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));

                        }
                        rotaTaskList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), rotaTaskMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing object  %s", rotaTaskList);
        }
        return rotaTaskList;
    }

    */
/**
     * if the task id =0 then we to set the input request without task execution paramater
     *
     * @param taskInputIMO
     * @return
     *//*

    private AddRotaTaskInput addTaskModel(RotaTaskInputRequestMO taskInputIMO) {
        AddRotaTaskInput addTaskInputIMO = new AddRotaTaskInput();
        addTaskInputIMO.setId(taskInputIMO.getId());
        if(taskInputIMO.getUnitId()==null){
            addTaskInputIMO.setUnitId(null);
        }else {
            addTaskInputIMO.setUnitId(taskInputIMO.getUnitId());
        }

        addTaskInputIMO.setTaskTypeId(taskInputIMO.getTaskTypeId());
        addTaskInputIMO.setAssigneeId(taskInputIMO.getAssigneeId());
        addTaskInputIMO.setEstimatedExecutionTime(taskInputIMO.getEstimatedExecutionTime());
        addTaskInputIMO.setActualExecutionTime(taskInputIMO.getActualExecutionTime());
        addTaskInputIMO.setTaskStatusId(taskInputIMO.getTaskStatusId());
        addTaskInputIMO.setDescription(taskInputIMO.getDescription());
        addTaskInputIMO.setEstimatedTaskExecutionStartDateTime(taskInputIMO.getEstimatedTaskExecutionStartDateTime());
        addTaskInputIMO.setEstimatedTaskExecutionEndDateTime(taskInputIMO.getEstimatedTaskExecutionEndDateTime());
        addTaskInputIMO.setBarrackId(taskInputIMO.getBarrackId());
        addTaskInputIMO.setIsAutoCreation(taskInputIMO.getIsAutoCreation());
        addTaskInputIMO.setExecutorId(taskInputIMO.getExecutorId());
        addTaskInputIMO.setCreatedDateTime(taskInputIMO.getCreatedDateTime());
        addTaskInputIMO.setCreatedById(taskInputIMO.getCreatedById());
        addTaskInputIMO.setRelatedTaskId(taskInputIMO.getRelatedTaskId());
        addTaskInputIMO.setUnitPostId(taskInputIMO.getUnitPostId());
        addTaskInputIMO.setAdditionalComments(taskInputIMO.getAdditionalComments());
        addTaskInputIMO.setApprovedById(taskInputIMO.getApprovedById());
        addTaskInputIMO.setAssignedById(taskInputIMO.getAssignedById());
        addTaskInputIMO.setChangeRouteReasonId(taskInputIMO.getChangeRouteReasonId());
        addTaskInputIMO.setOtherReasonId(taskInputIMO.getOtherReasonId());
        return addTaskInputIMO;
    }

    */
/**
     * Update the task id in the taskTable
     *
     * @param taskId
     * @param id
     *//*

    public void updateTaskid(int taskId, int id, int taskStatusD, int isnew, int isSync) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_ID + "= '" + taskId +
                "' ," + TableNameAndColumnStatement.TASK_STATUS_ID + "= " + taskStatusD +
                " ," + TableNameAndColumnStatement.IS_NEW + "= " + isnew +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + isSync +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    */
/**
     * Updating the taskID in Metadata Table.
     *
     * @param taskid
     * @param id
     *//*

    public void updateTaskidMetadataTable(int taskid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + taskid +
                "' ," + TableNameAndColumnStatement.TASK_ID + "= " + taskid +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    */
/**
     * syncing the  ADHOC ROTA task to server
     * @param inputIMO
     * @param key
     *//*

    private void addTaskApiCall(final AddRotaTaskInput inputIMO, final int key) {
        sisClient.getApi().syncAddUnitTask(inputIMO, new Callback<TaskOutputMO>() {
            @Override
            public void success(TaskOutputMO taskOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing response  %s", taskOutputMO);
                switch (taskOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        updateTaskid(taskOutputMO.getData().getTaskId(), key, 1, 1, 0);      // updating the taskid to TaskTable
                        updateTaskidMetadataTable(taskOutputMO.getData().getTaskId(), key); // updating the taskid to Metadatatable
                        updatePostidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,taskOutputMO.getData().getTaskId(),      // updating the PostID to Metadatatable
                                taskOutputMO.getData().getTaskId(), key, Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE, mContext));
                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);

                        }

                        break;
                    default:
                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);
                        }
                }

            }

            @Override
            public void failure(RetrofitError error) {
               count++;
                if (count == inputList.size()) {
                    count = 0;
                    new IssuesSyncing(mContext);
                    new UnitStrengthSyncing(mContext);
                    new KitRequestSyncing(mContext);
                    new NotificationSync(mContext);
                }
                Crashlytics.logException(error);
            }
        });
        Timber.d(Constants.TAG_SYNCADAPTER+"addTaskApiCall count   %s", "count: " + count);
    }

    private void updateTaskApicall(final RotaTaskInputRequestMO inputIMO, final int key) {
        Gson gson = new Gson();
        String request = gson.toJson(inputIMO);
        Timber.d(Constants.TAG_SYNCADAPTER+"RotaTaskSync :" + request.toString());

        sisClient.getApi().syncUnitTask(inputIMO, new Callback<TaskOutputMO>() {
            @Override
            public void success(TaskOutputMO taskOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"rotaTaskSyncing response  %s", taskOutputMO);
                switch (taskOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        //Rota Start Task api == upadteApi(two times)
                        // 1 -- > starttask 2 ---> submit TaskExecution Result.
//                        if(inputIMO.getIsTaskStarted()==RotaStartTaskEnum.InProgress.getValue()){
//                          //  updateIsTaskStarted(key, RotaStartTaskEnum.InActive.getValue());
//                        }
//                        else if(inputIMO.getIsTaskStarted()==RotaStartTaskEnum.InActive.getValue()){
//                            updateTaskid(taskOutputMO.getData().getTaskId(), key, RotaTaskStatusEnum.InActive.getValue(), 0, 1);      // updating the taskid to TaskTable
//                            updateTaskidMetadataTable(taskOutputMO.getData().getTaskId(), key); // updating the taskid to Metadatatable
//                            updatePostidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,taskOutputMO.getData().getTaskId(),      // updating the PostID to Metadatatable
//                                    taskOutputMO.getData().getTaskId(), key, Util.
//                                            getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE, mContext));
//                        }
                        updateTaskid(taskOutputMO.getData().getTaskId(), key, RotaTaskStatusEnum.InActive.getValue(), 0, 1);      // updating the taskid to TaskTable
                        updateTaskidMetadataTable(taskOutputMO.getData().getTaskId(), key); // updating the taskid to Metadatatable
                        updatePostidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,taskOutputMO.getData().getTaskId(),      // updating the PostID to Metadatatable
                                taskOutputMO.getData().getTaskId(), key, Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE, mContext));
                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);
                        }

                        break;
                    default:
                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);
                        }
                    }


            }

            @Override
            public void failure(RetrofitError error) {
                count++;
                if (count == inputList.size()) {
                    count = 0;
                    new IssuesSyncing(mContext);
                    new UnitStrengthSyncing(mContext);
                    new KitRequestSyncing(mContext);
                    new NotificationSync(mContext);
                }
               Crashlytics.logException(error);
            }
        });
        Timber.d(Constants.TAG_SYNCADAPTER+"updateTaskApicall count   %s", "count: " + count);

    }

    */
/**
     * Update the isTaskStarted in the taskTable
     *
     *
     * @param id
     *//*

    public void updateIsTaskStarted( int id, int isTaskStarted) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.IS_TASK_STARTED + "= '" + isTaskStarted + "' " +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    private void startTaskApiCall(final RotaTaskInputRequestMO inputIMO, final int key){
        sisClient.getApi().startTask(String.valueOf(inputIMO.getId()), new Callback<CommonResponse>() {
            @Override
            public void success(CommonResponse commonResponse, Response response) {
                switch (commonResponse.getStatusCode()) {
                    case 200:
                        updateIsTaskStarted(key, RotaStartTaskEnum.OnCompleted.getValue());
                        Timber.d(Constants.TAG_SYNCADAPTER+"startTaskApiCall updateQuery  %s", "200 ok");

                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);
                        }
                        break;
                    default:
                        count++;
                        if (count == inputList.size()) {
                            count = 0;
                            new IssuesSyncing(mContext);
                            new UnitStrengthSyncing(mContext);
                            new KitRequestSyncing(mContext);
                            new NotificationSync(mContext);
                        }
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e("Error %s", error.getMessage());
                count++;
                if (count == inputList.size()) {
                    count = 0;
                    new IssuesSyncing(mContext);
                    new UnitStrengthSyncing(mContext);
                    new KitRequestSyncing(mContext);
                    new NotificationSync(mContext);
                }
            }
        });
    }
}

*/
