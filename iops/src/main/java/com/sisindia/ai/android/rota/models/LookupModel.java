package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 18-07-2016.
 */
public class LookupModel {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("LookupTypeId")
    @Expose
    private Integer lookupTypeId;
    @SerializedName("LookupTypeName")
    @Expose
    private String lookupTypeName;
    @SerializedName("LookupIdentifier")
    @Expose
    private Integer lookupIdentifier;
    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("LookupCode")
    @Expose
    private String lookupCode;

    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The lookupTypeId
     */
    public Integer getLookupTypeId() {
        return lookupTypeId;
    }

    /**
     *
     * @param lookupTypeId
     * The LookupTypeId
     */
    public void setLookupTypeId(Integer lookupTypeId) {
        this.lookupTypeId = lookupTypeId;
    }

    /**
     *
     * @return
     * The lookupTypeName
     */
    public String getLookupTypeName() {
        return lookupTypeName;
    }

    /**
     *
     * @param lookupTypeName
     * The LookupTypeName
     */
    public void setLookupTypeName(String lookupTypeName) {
        this.lookupTypeName = lookupTypeName;
    }

    /**
     *
     * @return
     * The lookupIdentifier
     */
    public Integer getLookupIdentifier() {
        return lookupIdentifier;
    }

    /**
     *
     * @param lookupIdentifier
     * The LookupIdentifier
     */
    public void setLookupIdentifier(Integer lookupIdentifier) {
        this.lookupIdentifier = lookupIdentifier;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The lookupCode
     */
    public String getLookupCode() {
        return lookupCode;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    /**
     *
     * @param lookupCode
     * The LookupCode
     */
    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    @Override
    public String toString() {
        return "LookupModel{" +
                "id=" + id +
                ", lookupTypeId=" + lookupTypeId +
                ", lookupTypeName='" + lookupTypeName + '\'' +
                ", lookupIdentifier=" + lookupIdentifier +
                ", name='" + name + '\'' +
                ", lookupCode='" + lookupCode + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
