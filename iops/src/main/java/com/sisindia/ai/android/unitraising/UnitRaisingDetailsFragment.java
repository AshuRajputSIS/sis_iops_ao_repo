package com.sisindia.ai.android.unitraising;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.MapView;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ashu Rajput on 7/25/2017.
 */

public class UnitRaisingDetailsFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    @Bind(R.id.unit_name)
    CustomFontTextview unitName;
    @Bind(R.id.unit_name_text)
    CustomFontTextview unitNameText;
    @Bind(R.id.add_unit_image_tv)
    CustomFontTextview addUnitImageTv;
    @Bind(R.id.add_unit_image)
    RoundedCornerImageView addUnitImage;
    @Bind(R.id.raising_image_tv)
    CustomFontTextview raisingImageTv;
    @Bind(R.id.linearLayout_photos)
    LinearLayout linearLayoutPhotos;
    @Bind(R.id.horizontal_images_scroll)
    LinearLayout horizontalImagesScroll;
    @Bind(R.id.unitRaisingDetails)
    LinearLayout unitRaisingDetails;
    @Bind(R.id.mapview)
    MapView mapview;
    @Bind(R.id.remarks_edt)
    CustomFontEditText remarksEdt;
    private List<String> mArrayImageHolder;
    private RaisingDetailsMo raisingDetailsMo;
    private List<Integer> raisingIds;
    private ArrayList<LookupModel> lookupModelUnitRaisingDetails;

    public static UnitRaisingDetailsFragment newInstance() {
        UnitRaisingDetailsFragment fragment = new UnitRaisingDetailsFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        raisingDetailsMo = (RaisingDetailsMo) getArguments().getSerializable(TableNameAndColumnStatement.RAISING_DETAILS);
    }

    @Override
    protected int getLayout() {
        return R.layout.unit_raising_details_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.unit_raising_details_fragment, container, false);
        ButterKnife.bind(this, view);
        addUnitImage.setOnClickListener(this);
        unitNameText.setText(raisingDetailsMo.unitName);
        raisingIds = new ArrayList<>();
        mArrayImageHolder = new ArrayList<>();
        createEditTextDetails();
        createUnitRaisingDetails();
        return view;
    }

    private void createEditTextDetails() {
        remarksEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                raisingDetailsMo.remarks = s.toString();
            }
        });
    }

    private void createUnitRaisingDetails() {
        TextView unitRaisingTextview = new CustomFontTextview(getActivity());
        unitRaisingTextview.setPadding(10, 10, 10, 10);
        unitRaisingTextview.setTextSize(18);
        //unitRaisingTextview.setId(key);
        unitRaisingTextview.setTextColor(Color.BLACK);
        int marginBottom = Util.dpToPx(getResources().getInteger(R.integer.number_4));
        int marginLeft = Util.dpToPx(16);
        int marginRight = Util.dpToPx(16);
        int marginTop = Util.dpToPx(getResources().getInteger(R.integer.number_4));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
        unitRaisingTextview.setText(getActivity().getResources()
                .getString(R.string.unitRaisingDetail));
        params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        unitRaisingTextview.setLayoutParams(params);
        unitRaisingDetails.addView(unitRaisingTextview);
        lookupModelUnitRaisingDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().
                getLookupData(TableNameAndColumnStatement.UnitRaisingChecklist, null);
        if (lookupModelUnitRaisingDetails != null && lookupModelUnitRaisingDetails.size() != 0) {
            for (int index = 0; index < lookupModelUnitRaisingDetails.size(); index++) {
                CheckBox customCheckbox = new CheckBox(getActivity());
                customCheckbox.setText((lookupModelUnitRaisingDetails.get(index)).getName());
                customCheckbox.setId(lookupModelUnitRaisingDetails.get(index).getLookupIdentifier());
                customCheckbox.setChecked(false);
                LinearLayout.LayoutParams checkBoxParams = new LinearLayout.LayoutParams(
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                checkBoxParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
                customCheckbox.setLayoutParams(checkBoxParams);
                LinearLayout linearLayout = new LinearLayout(getActivity());
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(customCheckbox);
                unitRaisingDetails.addView(linearLayout);
                customCheckbox.setOnCheckedChangeListener(this);
            }
        }


    }

    public RaisingDetailsMo validateDetailsFragmentFields() {
        boolean isRaisingDetailsCaptured = false;
        boolean isRaisingLocationCaptured = false;
        raisingDetailsMo.raisingLocation = mapview.getLatitude() + "," + mapview.getLongitude();
        if (raisingIds.size() != 0) {

            String json = IOPSApplication.getGsonInstance().toJson(raisingIds);
            if (!TextUtils.isEmpty(json)) {
                raisingDetailsMo.raisingDetails = json;
                isRaisingDetailsCaptured = true;
            }
            if (TextUtils.isEmpty(raisingDetailsMo.raisingLocation)) {
                snackBarWithMesg(linearLayoutPhotos, getActivity().getString(R.string.MSG_RAISING_LOCATION));
            } else {
                isRaisingLocationCaptured = true;
            }
        } else {
            isRaisingDetailsCaptured = false;
            snackBarWithMesg(linearLayoutPhotos, getActivity().getString(R.string.MSG_RAISING_DETAILS));
        }
        raisingDetailsMo.validationStatus = isRaisingDetailsCaptured && isRaisingLocationCaptured;
        return raisingDetailsMo;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            if (!raisingIds.contains(buttonView.getId())) {
                raisingIds.add(buttonView.getId());
            }
        } else {
            if (raisingIds.contains(buttonView.getId())) {
                raisingIds.remove(raisingIds.indexOf(buttonView.getId()));
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent capturePictureIntent = new Intent(getActivity(), CapturePictureActivity.class);
        switch (v.getId()) {
            case R.id.add_unit_image:
                if (mArrayImageHolder.size() <= 2) {
                    Util.mSequenceNumber++;
                    //campturePictureIntent.putExtra(TableNameAndColumnStatement.UNIT_NAME, unitName);
                    //capturePictureIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, unitId);
                    Util.setSendPhotoImageTo(Util.UNIT_RAISING_DETAILS);
                    Util.initMetaDataArray(true);
                    capturePictureIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, raisingDetailsMo.unitId);
                    capturePictureIntent.putExtra(TableNameAndColumnStatement.UNIT_NAME, raisingDetailsMo.unitName);
                    startActivityForResult(capturePictureIntent, Util.CAPTURE_UNIT_RAISING_CODE);
                    break;
                } else {
                    snackBarWithMesg(linearLayoutPhotos, getActivity().getString(R.string.Captured_Image_MSG));
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Util.CAPTURE_UNIT_RAISING_CODE && resultCode == RESULT_OK && data != null) {
            AttachmentMetaDataModel attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
            String imagePath = attachmentMetaDataModel.getAttachmentPath();
            attachmentMetaArrayList.add(attachmentMetaDataModel);
            //setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            mArrayImageHolder.add(imagePath);
            loadThumbNailImages(mArrayImageHolder);
        } else {
            Util.mSequenceNumber--;
        }
    }


    public void setupMetaData(int raisingId) {
        if (attachmentMetaArrayList != null && attachmentMetaArrayList.size() > 0) {
            for (int i = 0; i < attachmentMetaArrayList.size(); i++) {
                if (attachmentMetaArrayList.get(i).getAttachmentSourceTypeId()
                        == Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING, getActivity())) {
                    attachmentMetaArrayList.get(i).setAttachmentSourceId(raisingId);
                }
            }

            IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTableFromHashMap(
                    attachmentMetaArrayList);
            attachmentMetaArrayList.clear();
        }
    }

    private void loadThumbNailImages(List<String> arrayImageHolder) {
        if (horizontalImagesScroll != null) {
            horizontalImagesScroll.removeAllViews();
            horizontalImagesScroll.addView(addUnitImage);
        }

        if (arrayImageHolder != null && arrayImageHolder.size() > 0) {
            for (int index = arrayImageHolder.size() - 1; index >= 0; index--) {
                ImageView imageView = createImageView(Uri.parse(arrayImageHolder.get(index)));
                horizontalImagesScroll.addView(imageView);
            }

        }
    }

    public ImageView createImageView(Uri uri) {

        int marginBottom = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginLeft = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginRight = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);

        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.HEIGT);
        params.width = Util.dpToPx(Constants.HEIGT);
        params.setMargins(marginLeft, marginBottom, marginRight, marginBottom);

        RoundedCornerImageView takenImageView = new RoundedCornerImageView(getActivity());
        takenImageView.setLayoutParams(params);
        Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.camer_taken_pic_image_border);
        takenImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        takenImageView.setImageURI(uri);
        return takenImageView;
    }
}