package com.sisindia.ai.android.rota.daycheck.taskExecution;

/**
 * Created by Shushrut on 14-07-2016.
 */
public class SecurityRiskAnswerMO {
    private Integer securityRiskQuestionId;
    private Integer securityRiskOptionId;
    private String securityRiskOptionName;
    private Integer isMandatory;
    private String controlType;
    private boolean isFilled;
    private String userComments;
    public Integer getSecurityRiskQuestionId() {
        return securityRiskQuestionId;
    }
    public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
        this.securityRiskQuestionId = securityRiskQuestionId;
    }
    public Integer getSecurityRiskOptionId() {
        return securityRiskOptionId;
    }
    public void setSecurityRiskOptionId(Integer securityRiskOptionId) {
        this.securityRiskOptionId = securityRiskOptionId;
    }
    public String getSecurityRiskOptionName() {
        return securityRiskOptionName;
    }
    public void setSecurityRiskOptionName(String securityRiskOptionName) {
        this.securityRiskOptionName = securityRiskOptionName;
    }
    public Integer getIsMandatory() {
        return isMandatory;
    }
    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }
    public String getControlType() {
        return controlType;
    }
    public void setControlType(String controlType) {
        this.controlType = controlType;
    }
    public boolean isFilled() {
        return isFilled;
    }
    public void setFilled(boolean filled) {
        isFilled = filled;
    }
    public String getUserComments() {
        return userComments;
    }
    public void setUserComments(String userComments) {
        this.userComments = userComments;
    }

    @Override
    public String toString() {
        return "SecurityRiskAnswerMO{" +
                "securityRiskQuestionId=" + securityRiskQuestionId +
                ", securityRiskOptionId=" + securityRiskOptionId +
                ", securityRiskOptionName='" + securityRiskOptionName + '\'' +
                ", isMandatory=" + isMandatory +
                ", controlType='" + controlType + '\'' +
                ", isFilled=" + isFilled +
                ", userComments='" + userComments + '\'' +
                '}';
    }
}
