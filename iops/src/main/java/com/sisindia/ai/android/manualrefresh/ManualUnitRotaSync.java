package com.sisindia.ai.android.manualrefresh;

import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitContactTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitEquipmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitPostTableCountMO;
import com.sisindia.ai.android.home.RotaUnitSyncListener;
import com.sisindia.ai.android.home.UnitUpdateAsyncTask;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.BaseUnitSync;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Saruchi on 02-02-2017.
 */

public class ManualUnitRotaSync {
    public static void getUnitApiCall(final RotaUnitSyncListener rotaUnitSyncListener, final Context mContext, final boolean isDetroyed) {
        if (tablesToSync()) {
            showprogressbar(mContext);
            new SISClient(mContext).getApi().getUnit(new Callback<BaseUnitSync>() {
                @Override
                public void success(BaseUnitSync baseUnitSync, Response response) {
                    switch (baseUnitSync.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            new UnitUpdateAsyncTask(mContext, baseUnitSync.getData(), rotaUnitSyncListener).execute();
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(mContext, mContext.getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar(isDetroyed);
                    Crashlytics.logException(error);
                }
            });
        } else {
            Util.showToast(mContext, mContext.getString(R.string.please_sync_older_records));
        }
    }

    private static void showprogressbar(Context mContext) {
        Util.showProgressBar(mContext, R.string.loading_please_wait);
    }

    private static void hideprogressbar(boolean isDetroyed) {
        Util.dismissProgressBar(isDetroyed);
    }

    private synchronized static boolean tablesToSync() {
        UnitContactTableCountMO unitContactTableCountMO = IOPSApplication.getInstance().getTableSyncCount().getUnitContactTableCount();
        UnitEquipmentTableCountMO unitEquipmentTableCountMO = IOPSApplication.getInstance().getTableSyncCount().getUnitEquipmentTableCount();
        UnitPostTableCountMO unitPostTableCountMO = IOPSApplication.getInstance().getTableSyncCount().getUnitPostTableCount();
        if (unitContactTableCountMO.getSyncCount() != 0 || unitEquipmentTableCountMO.getSyncCount() != 0 || unitPostTableCountMO.getSyncCount() != 0)
            return false;
        else
            return true;
    }
}
