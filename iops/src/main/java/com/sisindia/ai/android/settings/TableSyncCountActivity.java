package com.sisindia.ai.android.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.birbit.android.jobqueue.timer.Timer;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.tableSyncCountDTO.FileUploadCount;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.AttachmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.BarrackTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.ImprovementPlanTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.IssuesTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitDistributionItemTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitItemRequestTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.RecruitmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.TaskTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitContactTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitEquipmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitPostTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitStrengthTableCountMO;
import com.sisindia.ai.android.syncadpter.UnSyncedRecords;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TableSyncCountActivity extends BaseActivity {

    @Bind(R.id.Attachment_table)
    CustomFontTextview attachmentTable;
    @Bind(R.id.Attachment_table_count)
    CustomFontTextview attachmentTableCount;
    @Bind(R.id.barrack_table)
    CustomFontTextview barrackTable;
    @Bind(R.id.barrack_table_count)
    CustomFontTextview barrackTableCount;
    @Bind(R.id.improvement_plans_table)
    CustomFontTextview improvementPlansTable;
    @Bind(R.id.improvement_plans_table_count)
    CustomFontTextview improvementPlansTableCount;
    @Bind(R.id.kit_item_request_table)
    CustomFontTextview kitItemRequestTable;
    @Bind(R.id.kit_item_request_table_count)
    CustomFontTextview kitItemRequestTableCount;
    @Bind(R.id.kit_distribution_item_table)
    CustomFontTextview kitDistributionItemTable;
    @Bind(R.id.kit_distribution_item_table_count)
    CustomFontTextview kitDistributionItemTableCount;
    @Bind(R.id.recuirt_table)
    CustomFontTextview recuirtTable;
    @Bind(R.id.recuirt_table_count)
    CustomFontTextview recuirtTableCount;
    @Bind(R.id.task_table)
    CustomFontTextview taskTable;
    @Bind(R.id.task_table_count)
    CustomFontTextview taskTableCount;
    @Bind(R.id.unit_strength_table)
    CustomFontTextview unitStrengthTable;
    @Bind(R.id.unit_strength_table_count)
    CustomFontTextview unitStrengthTableCount;
    @Bind(R.id.unit_contact_table)
    CustomFontTextview unitContactTable;
    @Bind(R.id.unit_contact_table_count)
    CustomFontTextview unitContactTableCount;
    @Bind(R.id.unit_equip_table)
    CustomFontTextview unitEquipTable;
    @Bind(R.id.unit_equip_table_count)
    CustomFontTextview unitEquipTableCount;
    @Bind(R.id.unit_post_table)
    CustomFontTextview unitPostTable;
    @Bind(R.id.unit_post_table_count)
    CustomFontTextview unitPostTableCount;
    @Bind(R.id.unit_issue_table_count)
    CustomFontTextview unitIssueTableCount;
    @Bind(R.id.file_upload_count)
    CustomFontTextview fileUploadCount;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private TableSyncCountActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table_sync_count);

        ButterKnife.bind(this);
        setUpToolBar();
        getTableSyncCount();
    }

    private void getTableSyncCount() {

        TableSyncCountActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                 AttachmentTableCountMO attachmentTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getAttachmentTableCount();
                attachmentTableCount.setText(attachmentTableCountMO.getSyncCount()+" / "+attachmentTableCountMO.getTotalCount());

                BarrackTableCountMO barrackTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getBarrackTableCount();
                barrackTableCount.setText(barrackTableCountMO.getSyncCount()+" / "+barrackTableCountMO.getTotalCount());

                ImprovementPlanTableCountMO improvementPlanTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getImprovementPlanTableCount();
                improvementPlansTableCount.setText(improvementPlanTableCountMO.getSyncCount()+" / "+improvementPlanTableCountMO.getTotalCount());

                KitItemRequestTableCountMO kitItemRequestTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getKitItemRequestTableCount();
                kitItemRequestTableCount.setText(kitItemRequestTableCountMO.getSyncCount()+" / "+kitItemRequestTableCountMO.getTotalCount());


                KitDistributionItemTableCountMO kitDistributionItemTableCountMO =IOPSApplication.getInstance().getTableSyncCount().getKitDistributionItemTableCount();
                kitDistributionItemTableCount.setText(kitDistributionItemTableCountMO.getSyncCount()+" / "+ kitDistributionItemTableCountMO.getTotalCount());

                RecruitmentTableCountMO recruitmentTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getRecruitmentTableCount();
                recuirtTableCount.setText(recruitmentTableCountMO.getSyncCount()+" / "+recruitmentTableCountMO.getTotalCount());

                TaskTableCountMO taskTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getTaskTableCount();
                taskTableCount.setText(taskTableCountMO.getSyncCount()+" / "+taskTableCountMO.getTotalCount());

                UnitStrengthTableCountMO unitStrengthTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getUnitStrengthTableCount();
                unitStrengthTableCount.setText(unitStrengthTableCountMO.getSyncCount()+" / "+unitStrengthTableCountMO.getTotalCount());

                UnitContactTableCountMO unitContactTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getUnitContactTableCount();
                unitContactTableCount.setText(unitContactTableCountMO.getSyncCount()+" / "+unitContactTableCountMO.getTotalCount());


                UnitEquipmentTableCountMO unitEquipmentTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getUnitEquipmentTableCount();
                unitEquipTableCount.setText(unitEquipmentTableCountMO.getSyncCount()+" / "+unitEquipmentTableCountMO.getTotalCount());

                UnitPostTableCountMO unitPostTableCountMO=IOPSApplication.getInstance().getTableSyncCount().getUnitPostTableCount();
                unitPostTableCount.setText(unitPostTableCountMO.getSyncCount()+" / "+unitPostTableCountMO.getTotalCount());

                IssuesTableCountMO issuesTableCountMO = IOPSApplication.getInstance().getTableSyncCount().getIssueTableCount();
                unitIssueTableCount.setText(issuesTableCountMO.getSyncCount()+" / "+issuesTableCountMO.getTotalCount());

                FileUploadCount fileUploadCountMo = IOPSApplication.getInstance().getTableSyncCount().getFileUploadCount();
                fileUploadCount.setText(fileUploadCountMo.getSyncCount()+" / "+fileUploadCountMo.getTotalCount());


            }
        });

    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.TABLE_SYNC_COUNT));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync, menu);
        String syncRequestDateTime = IOPSApplication.getInstance().getDutyAttendanceDB().getLatestSyncRequestTime();
        menu.getItem(0).setVisible(true);
        if(!TextUtils.isEmpty(syncRequestDateTime)){
            if(new DateTimeFormatConversionUtil()
                    .getTimeDifference(syncRequestDateTime)){
                menu.getItem(0).setVisible(true);
            }
            else {
                menu.getItem(0).setVisible(false);
            }
        }
        else
        {
            menu.getItem(0).setVisible(true);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }else if(id == R.id.sync){
            UnSyncedRecords unSyncedRecords = new UnSyncedRecords(this);
            unSyncedRecords.uploadUnSyncedIssues();
            unSyncedRecords.uploadUnSyncedImages();
            unSyncedRecords.uploadUnsyncedStrength();
            unSyncedRecords.uploadUnsyncedImprovementPlans();
            unSyncedRecords.uploadUnsyncedKitRequests();
            Bundle bundle = new Bundle();
            IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
            IOPSApplication.getInstance().getDutyAttendanceDB()
                    .insertIntoSyncRequest(new DateTimeFormatConversionUtil()
                            .getCurrentDateTime());
            Util.showToast(TableSyncCountActivity.this,getResources()
                    .getString(R.string.sync_adapter_message));
            item.setVisible(false);

        }
        return super.onOptionsItemSelected(item);
    }
}
