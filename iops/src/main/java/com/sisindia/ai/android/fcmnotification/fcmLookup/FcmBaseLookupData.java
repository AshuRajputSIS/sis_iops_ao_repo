package com.sisindia.ai.android.fcmnotification.fcmLookup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.models.LookupModel;

import java.util.ArrayList;
import java.util.List;

public class FcmBaseLookupData {

@SerializedName("StatusCode")
@Expose
private Integer statusCode;
@SerializedName("StatusMessage")
@Expose
private String statusMessage;
@SerializedName("Data")
@Expose
private List<LookupModel> lookupModels;

public Integer getStatusCode() {
return statusCode;
}

public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

public String getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

    public List<LookupModel> getLookupModels() {
        return lookupModels;
    }

    public void setLookupModels(List<LookupModel> lookupModels) {
        this.lookupModels = lookupModels;
    }
}