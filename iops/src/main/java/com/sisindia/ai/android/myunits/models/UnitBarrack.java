package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitBarrack {

    @SerializedName("Id")
    @Expose
    public Integer Id;
    @SerializedName("BarrackId")
    @Expose
    public Integer BarrackId;
    @SerializedName("UnitId")
    @Expose
    public Integer UnitId;
    @SerializedName("IsActive")
    @Expose
    public Boolean IsActive;

    public Integer getBarrackId() {
        return BarrackId;
    }

    public void setBarrackId(Integer barrackId) {
        BarrackId = barrackId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Integer getUnitId() {
        return UnitId;
    }

    public void setUnitId(Integer unitId) {
        UnitId = unitId;
    }
}