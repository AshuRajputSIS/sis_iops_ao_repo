package com.sisindia.ai.android.rota.clientcoordination;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionResponse;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionResponse.ClientCoordination;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionResponse.ClientCoordination.QuestionsAndOptions;
import com.sisindia.ai.android.rota.daycheck.SignatureActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by shivam on 18/7/16.
 */

public class ClientCoordinationFragment extends BaseFragment implements CsiListener {

    public static final int ISSUE_TYPE_ID = 3;
    private static final int SCAN_FORM = 1001;
    private static final int SELFIE_BH = 5;
   /* private static final String TAG = ClientCoordinationFragment.class.getSimpleName();
    private static final String EXCELLENT = "excellent";
    private static final String GOOD = "good";
    private static final String SATISFACTORY = "satisfactory";
    private static final String POOR = "poor";
    private static final String VERY_POOR = "very_poor";*/

    @Bind(R.id.unit_name_text)
    TextView tvUnitName;

    @Bind(R.id.spinner_client_contact)
    Spinner spinnerClientContact;

    @Bind(R.id.rv_rating_questions)
    RecyclerView rvQuestions;

    @Bind(R.id.tv_csi)
    TextView tvCsiPercentage;

    @Bind(R.id.iv_client_signature)
    ImageView ivClientSignature;

    @Bind(R.id.iv_capture_image)
    ImageView ivScanForm;

    @Bind(R.id.selfieWithBH)
    ImageView selfieWithBH;

    @Bind(R.id.selfieCheckBox)
    CheckBox selfieCheckBox;

    private QuestionsAdapter adapter;
    private RotaTaskModel rotaTaskModel;

    private String clientContact;
    private int clientId;

    private Map<Integer, Integer> selectedAnswers;
    private Map<Integer, ImprovementPlanIR> improvementPlan;
    AttachmentMetaDataModel selfieAttachmentModel = null;

    public static ClientCoordinationFragment newInstance() {
        ClientCoordinationFragment fragment = new ClientCoordinationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.client_coordination_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        tvUnitName.setText(rotaTaskModel.getUnitName());
        inflateSpinnerList();

        rvQuestions.setNestedScrollingEnabled(false);
        rvQuestions.setAdapter(adapter);
        rvQuestions.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter.updateQuestions(rotaTaskModel.getUnitId());

        selfieCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selfieWithBH.setVisibility(View.VISIBLE);
                } else {
                    selfieWithBH.setVisibility(View.INVISIBLE);
//                    selfieWithBH.setImageResource(R.mipmap.ic_guardphoto);
//                    selfitAttachmentModel=null;
                }
            }
        });

        //getInspectorPerformance();
    }

    private void init() {
        selectedAnswers = new HashMap<>();
        improvementPlan = new HashMap<>();
        adapter = new QuestionsAdapter(mActivity, this);
        rotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if (rotaTaskModel == null) {
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
        }
    }

   /* private void getInspectorPerformance() {
        rgAreaPerformance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        inspectorFeedback = null;
                        break;
                    case R.id.rb_excellent:
                        inspectorFeedback = EXCELLENT;
                        responseChange(-1, 5);
                        break;
                    case R.id.rb_good:
                        inspectorFeedback = GOOD;
                        responseChange(-1, 4);
                        break;
                    case R.id.rb_satisfactory:
                        inspectorFeedback = SATISFACTORY;
                        responseChange(-1, 3);
                        break;
                    case R.id.rb_poor:
                        inspectorFeedback = POOR;
                        responseChange(-1, 2);
                        break;
                    case R.id.rb_very_poor:
                        inspectorFeedback = VERY_POOR;
                        responseChange(-1, 1);
                        break;
                }
            }
        });
    }*/

    private void inflateSpinnerList() {
        // Take client customers name from database
        final List<UnitContactsMO> unitContacts = IOPSApplication.getInstance()
                .getSelectionStatementDBInstance().fetchUnitContacts(rotaTaskModel.getUnitId());
        final String[] clientNames = new String[unitContacts.size()];
        for (int i = 0; i < unitContacts.size(); i++) {
            clientNames[i] = unitContacts.get(i).getClientDesignation() + " - " +
                    unitContacts.get(i).getContactFirstName() + " " +
                    unitContacts.get(i).getContactLastName();
        }
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_dropdown_item, clientNames);

        spinnerClientContact.setAdapter(spinnerAdapter);
        spinnerClientContact.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clientContact = unitContacts.get(position).getContactFirstName() + " " +
                        unitContacts.get(position).getContactLastName();
                clientId = unitContacts.get(position).getUnitContactId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // Customer signature required in case of digital form submission
    @OnClick(R.id.iv_client_signature)
    public void getCustomerSignature() {
        Intent signatureActivity = new Intent(mActivity, SignatureActivity.class);
        signatureActivity.putExtra("SignatureCapture", true);
        startActivityForResult(signatureActivity, Util.CAPTURE_SIGNATURE);
    }

    // If the customer is filling hard copy then take a snap of the form
    @OnClick(R.id.iv_capture_image)
    public void scanCustomerForm() {
        Intent scanFormIntent = new Intent(mActivity, CapturePictureActivity.class);
        scanFormIntent.putExtra(getResources().getString(R.string.toolbar_title), "");
        Util.setSendPhotoImageTo(Util.CLIENT_COORDINATION);
        Util.initMetaDataArray(true);
        startActivityForResult(scanFormIntent, SCAN_FORM);
    }

    // SELFIE WITH BH BUTTON
    @OnClick(R.id.selfieWithBH)
    public void selfieWithBH() {
        Intent scanFormIntent = new Intent(mActivity, CapturePictureActivity.class);
        scanFormIntent.putExtra(getResources().getString(R.string.toolbar_title), "");
        Util.setSendPhotoImageTo(Util.CLIENT_COORDINATION_SELFIE);
        Util.initMetaDataArray(true);
        startActivityForResult(scanFormIntent, SELFIE_BH);
    }

    @OnClick(R.id.b_add_complaints)
    public void addComplaint() {
        Intent addComplaint = new Intent(mActivity, AddComplaintActivity.class);
        Bundle complaintBundle = new Bundle();
        complaintBundle.putInt(TableNameAndColumnStatement.UNIT_ID, rotaTaskModel.getUnitId());
        complaintBundle.putString(TableNameAndColumnStatement.UNIT_NAME, rotaTaskModel.getUnitName());
        complaintBundle.putBoolean(TableNameAndColumnStatement.CHECKING_TYPE_DAY_CHECKING, true);
        addComplaint.putExtras(complaintBundle);
        startActivity(addComplaint);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            AttachmentMetaDataModel attachmentModel;
            switch (requestCode) {
                case Util.CAPTURE_SIGNATURE:
                    ivClientSignature.setImageBitmap(Util.getmSignature());
                    break;
                case SCAN_FORM:
                    String scanFormPath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
                    attachmentModel = data.getParcelableExtra(Constants.META_DATA);
                    ivScanForm.setImageURI(Uri.parse(scanFormPath));
                    ((ClientCoordinationActivity) getActivity()).metaDataSetUp(attachmentModel);
                    break;
                case SELFIE_BH:
                    String capturedPath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
//                    attachmentModel = data.getParcelableExtra(Constants.META_DATA);
                    selfieAttachmentModel = data.getParcelableExtra(Constants.META_DATA);
                    selfieWithBH.setImageURI(Uri.parse(capturedPath));
                    break;
            }
        }
    }

    @Override
    public void responseChange(int questionId, int response) {
        selectedAnswers.put(questionId, response);
        int rating = 0;
        for (Object obj : selectedAnswers.entrySet()) {
            Map.Entry pair = (Map.Entry) obj;
            rating += (int) pair.getValue();
        }
        float csi = (rating * 100) / 50;
        tvCsiPercentage.setText(csi + " %");
    }

    @Override
    public void lowFeedback(int planId, int questionId, String turnAroundTime, int issueMatrixId) {

        ImprovementPlanIR improvementModel = new ImprovementPlanIR();

        improvementModel.setExpectedClosureDate(turnAroundTime + " 00:00:00");
        improvementModel.setRaisedDate(new DateTimeFormatConversionUtil().getCurrentDateTime());
        //improvementModel.setImprovementPlanCategoryId(questionId);
        improvementModel.setImprovementPlanCategoryId(null);
        improvementModel.setClientCoordinationQuestionId(questionId);
        improvementModel.setUnitId(rotaTaskModel.getUnitId());
        improvementModel.setMasterActionPlanId(planId);
        improvementModel.setIssueMatrixId(issueMatrixId);
        improvementModel.setTaskId(rotaTaskModel.getTaskId());

        // default values for the input request model
        improvementModel.setActionPlanStatusId(1); // pending staus
        improvementModel.setActionPlanId(0);

        improvementPlan.put(questionId, improvementModel);
    }

    @Override
    public void lowFeedbackRemoved(int questionId) {
        improvementPlan.remove(questionId);
    }

    public QuestionResponse sendResponse() {

        int responseSize = selectedAnswers.size();
        int questionsSize = IOPSApplication.getInstance().getSelectionStatementDBInstance().getClientCoordinationQuestions().size();
        QuestionResponse response = new QuestionResponse();

        /**
         * Validations
         * 1. All the questions are marked
         * 2. If manual scan uploaded then client signature not required
         * 3. Client name selected from the spinner
         * 4. Area inspector feedback given
         * 5. Improvement plan in case of 1 or 2 rating
         */

        if (questionsSize != responseSize) {
            snackBarWithMessage(getString(R.string.answer_all_questions));
            return null;
        } else {
            if (checkImageCaptured() || checkSignatureCaptured()) {
                if (!validateSelfieWithBH()) {
                    snackBarWithMessage("Please take selfie with Branch Head");
                    return null;
                } else {
                    updateImprovementPlan();
                    List<QuestionsAndOptions> responseList = new ArrayList<>();
                    for (Object obj : selectedAnswers.entrySet()) {
                        Map.Entry pair = (Map.Entry) obj;
                        if ((int) pair.getKey() != -1) {
                            QuestionsAndOptions options = new QuestionsAndOptions();
                            options.optionId = (int) pair.getValue();
                            options.questionId = (int) pair.getKey();
                            responseList.add(options);
                        }
                    }
                    String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
                    ClientCoordination coordination = new ClientCoordination();
                    coordination.geoLocation = geoLocation;
                    coordination.clientName = clientContact;
                    coordination.clientId = clientId;
                    //coordination.areaInspectorRating = inspectorFeedback;
                    coordination.questionsAndOptions = responseList;

                    if (selfieCheckBox.isChecked())
                        coordination.isSelfieTaken = true;
                    else
                        coordination.isSelfieTaken = false;

                    response.clientCoordination = coordination;
                    return response;
                }
            } else {
                snackBarWithMessage(getString(R.string.scan_form_error));
                return null;
            }


            /*if (!checkImageCaptured() || !checkSignatureCaptured()) {
                snackBarWithMessage(getString(R.string.scan_form_error));
                return null;
            }  else {
                updateImprovementPlan();
                List<QuestionsAndOptions> responseList = new ArrayList<>();
                for (Object obj : selectedAnswers.entrySet()) {
                    Map.Entry pair = (Map.Entry) obj;
                    if ((int) pair.getKey() != -1) {
                        QuestionsAndOptions options = new QuestionsAndOptions();
                        options.optionId = (int) pair.getValue();
                        options.questionId = (int) pair.getKey();
                        responseList.add(options);
                    }
                }
                String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
                ClientCoordination coordination = new ClientCoordination();
                coordination.geoLocation = geoLocation;
                coordination.clientName = clientContact;
                coordination.clientId = clientId;
                //coordination.areaInspectorRating = inspectorFeedback;
                coordination.questionsAndOptions = responseList;
                response.clientCoordination = coordination;
                return response;
            }*/
        }
    }

    public boolean validateSelfieWithBH() {
        if (!selfieCheckBox.isChecked()) {
            return true;
        } else if (selfieCheckBox.isChecked() && selfieWithBH.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_guardphoto).getConstantState()) {
            return false;
        } else if (selfieCheckBox.isChecked() && selfieAttachmentModel == null) {
            return false;
        } else if (selfieCheckBox.isChecked() && selfieAttachmentModel != null) {
            ((ClientCoordinationActivity) getActivity()).metaDataSetUp(selfieAttachmentModel);
            return true;
        }
        return true;
    }

    private void updateImprovementPlan() {
        List<ImprovementPlanIR> improvementPlanList = new ArrayList<>();
        for (Object obj : improvementPlan.entrySet()) {
            Map.Entry pair = (Map.Entry) obj;
            improvementPlanList.add((ImprovementPlanIR) pair.getValue());
        }

        if (improvementPlan.size() > 0) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addImprovementPlanDetails(improvementPlanList);
        }
    }

    private boolean checkSignatureCaptured() {
        boolean isSignatureCaptured;
        if (ivClientSignature.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_addsign).getConstantState()) {
            isSignatureCaptured = false;
        } else {
            isSignatureCaptured = true;
        }
        return isSignatureCaptured;
    }

    private boolean checkImageCaptured() {

        boolean isPicTakenForTurnOut;
        if ((ivScanForm.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_guardphoto).getConstantState())) {
            isPicTakenForTurnOut = false;
        } else {
            isPicTakenForTurnOut = true;
        }
        return isPicTakenForTurnOut;
    }
}
