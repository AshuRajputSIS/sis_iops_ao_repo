package com.sisindia.ai.android.rota.barrackInspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.mybarrack.MetLandLordMo;

/**
 * Created by shankar on 3/8/16.
 */

public class BarrackInspectionTaskExeMO {

    @SerializedName("GEOLocation")
    private String geoLocation ;

    @SerializedName("BarrackInspection")
    @Expose
    private BarrackInspectionMO barrackInspection;

    @SerializedName("MetLandLord")
    @Expose
    private MetLandLordMo metLandLordMo;

    /**
     *
     * @return
     * The barrackInspection
     */
    public BarrackInspectionMO getBarrackInspection() {
        return barrackInspection;
    }

    /**
     *
     * @param barrackInspection
     * The BarrackInspection
     */
    public void setBarrackInspection(BarrackInspectionMO barrackInspection) {
        this.barrackInspection = barrackInspection;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    public MetLandLordMo getMetLandLordMo() {
        return metLandLordMo;
    }

    public void setMetLandLordMo(MetLandLordMo metLandLordMo) {
        this.metLandLordMo = metLandLordMo;
    }

    @Override
    public String toString() {
        return "BarrackInspectionTaskExeMO{" +
                "geoLocation='" + geoLocation + '\'' +
                ", barrackInspection=" + barrackInspection +
                ", metLandLordMo=" + metLandLordMo +
                '}';
    }
}

