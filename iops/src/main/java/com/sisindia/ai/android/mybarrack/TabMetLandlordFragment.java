package com.sisindia.ai.android.mybarrack;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackInspectionQuestionOptionMO;
import com.sisindia.ai.android.mybarrack.modelObjects.QuestionsAndOptionsMO;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by compass on 7/4/2017.
 */

public class TabMetLandlordFragment extends BaseFragment
        implements RadioGroup.OnCheckedChangeListener,View.OnClickListener {


    @Bind(R.id.baseLayout)
    LinearLayout baseLayout;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    @Bind(R.id.photo_with_landlord_tv)
    CustomFontTextview photoWithLandlordTv;
    @Bind(R.id.post_one_image)
    RoundedCornerImageView landlordImageView;
    @Bind(R.id.post_one_tv)
    CustomFontTextview postOneTv;
    @Bind(R.id.post_two_image)
    RoundedCornerImageView  custodianImageView;
    @Bind(R.id.post_two_tv)
    CustomFontTextview postTwoTv;
    @Bind(R.id.relativeLayout2)
    RelativeLayout photoRelativeLayout;

    private LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> questionOptionMap;
    private List<QuestionsAndOptionsMO> questionsAndOptionsMOList = new ArrayList<>();
    private LinearLayout nonMeetingLinearLayout;
    private LinearLayout meetingLinearLayout;
    LinearLayout linearLayout;

    @Override
    protected int getLayout() {
        return R.layout.metlandlord_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);

        questionOptionMap = IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                getBarrackInspectionQuestionOption(4);
        dynamicRadioGroup(questionOptionMap);
        custodianImageView.setOnClickListener(this);
        landlordImageView.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void dynamicRadioGroup(LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> questionOptionMap) {
        if (null != questionOptionMap) {
            linearLayout = new LinearLayout(this.getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayout.setPadding(10, 10, 10, 20);

            meetingLinearLayout = new LinearLayout(this.getActivity());
            meetingLinearLayout.setOrientation(LinearLayout.VERTICAL);
            meetingLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            meetingLinearLayout.setPadding(10, 10, 10, 20);

            nonMeetingLinearLayout = new LinearLayout(this.getActivity());
            nonMeetingLinearLayout.setOrientation(LinearLayout.VERTICAL);
            nonMeetingLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            nonMeetingLinearLayout.setPadding(10, 10, 10, 20);

            for (Map.Entry<Integer, List<BarrackInspectionQuestionOptionMO>> e : questionOptionMap.entrySet()) {
                Integer key = e.getKey();
                TextView questionTxt = new TextView(this.getActivity());
                questionTxt.setPadding(10, 10, 10, 10);
                questionTxt.setTextSize(18);
                questionTxt.setId(key);
                questionTxt.setTextColor(Color.BLACK);
                questionTxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                if (key == 10) {

                    nonMeetingLinearLayout.addView(questionTxt);
                } else if (key == 11 || key == 12 || key == 13 || key == 14) {
                    meetingLinearLayout.addView(questionTxt);

                } else {
                    linearLayout.addView(questionTxt);
                }

                // Initialize a new RadioGroup
                RadioGroup rg = new RadioGroup(this.getActivity());
                rg.setOrientation(RadioGroup.HORIZONTAL);
                rg.setPadding(10, 10, 10, 0);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                rg.setLayoutParams(lp);
                rg.setId(View.generateViewId());
                rg.setOnCheckedChangeListener(this);


                QuestionsAndOptionsMO questionsAndOptionsMO = new QuestionsAndOptionsMO();
                EditText editText = null;

                for (int i = 0; i < e.getValue().size(); i++) {
                    questionTxt.setText(e.getValue().get(i).getQuestion());

                    // Create a Radio Button for RadioGroup
                    if (!TextUtils.isEmpty(e.getValue().get(i).getControlType())&& e.getValue()
                            .get(i).getControlType().equalsIgnoreCase("MultipleChoice") ||
                            (!TextUtils.isEmpty(e.getValue().get(i).getOptions())&&
                                    !TextUtils.isEmpty(e.getValue().get(i).getControlType()) &&
                                    e.getValue().get(i).getControlType().equalsIgnoreCase("TextBox")
                            && e.getValue().get(i).getIsMandatory() == 1)) {
                        RadioButton option = new RadioButton(this.getActivity());
                        option.setText(e.getValue().get(i).getOptions());
                        option.setPadding(10, 10, 10, 10);
                        option.setTextColor(ContextCompat.getColor(getActivity(),
                                R.color.color_dark_grey));
                        option.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        option.setId(View.generateViewId());
                        // option.la
                        option.setTag(e.getValue().get(i));
                        option.setChecked(false);
                        if (key == 8) {
                            if (i == 1) {
                                option.setChecked(true);
                                questionsAndOptionsMO.setOptionId(e.getValue().get(i).getOptionId());
                                questionsAndOptionsMO.setQuestionId(e.getValue().get(i).getQuestionId());
                                questionsAndOptionsMO.setRemarks(null);
                                questionsAndOptionsMOList.add(questionsAndOptionsMO);
                            }
                        } else {
                            if (i == 0) {
                                option.setChecked(true);
                                questionsAndOptionsMO.setOptionId(e.getValue().get(i).getOptionId());
                                questionsAndOptionsMO.setQuestionId(e.getValue().get(i).getQuestionId());
                                questionsAndOptionsMO.setRemarks(null);
                                questionsAndOptionsMOList.add(questionsAndOptionsMO);
                            }
                        }

                        rg.addView(option);
                        rg.setOrientation(LinearLayout.VERTICAL);
                        rg.setWeightSum(2);

                    }


                    if ((e.getValue().get(i).getIsMandatory() == 1 &&
                            !TextUtils.isEmpty(e.getValue().get(i).getControlType()) &&
                                    e.getValue().get(i).getControlType().equalsIgnoreCase("TextBox"))
                            || ((TextUtils.isEmpty(e.getValue().get(i).getOptions())) &&
                            e.getValue().get(i).getControlType().equalsIgnoreCase("TextBox"))) {
                        editText = new EditText(this.getActivity());
                        editText.setPadding(10, 30, 10, 30);
                        editText.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_dark_grey));
                        editText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        editText.setId(View.generateViewId());
                        editText.setBackground(getResources().getDrawable(R.drawable.border_editext));
                        editText.setTag(e.getValue().get(i));
                        editText.setMaxLines(2);
                        editText.setFocusable(false);
                        editText.setFocusableInTouchMode(true);
                        if ((TextUtils.isEmpty(e.getValue().get(i).getOptions()))) {
                            if (key == 12) {
                                editText.setHint("Enter Name");
                            } else if (key == 13) {
                                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                                editText = setEditTextMaxLength(10, editText);
                                editText.setHint("Enter Mobile number");
                            }
                            editText.setVisibility(View.VISIBLE);
                            questionsAndOptionsMO.setOptionId(null);
                        } else {
                            questionsAndOptionsMO.setOptionId(e.getValue().get(i).getOptionId());
                            editText.setHint("Enter remarks");
                            editText.setVisibility(View.GONE);
                        }
                        questionsAndOptionsMO.setQuestionId(e.getValue().get(i).getQuestionId());
                        questionsAndOptionsMO.setRemarks(editText.getText().toString().toString());


                    }

                    questionsAndOptionsMOList.add(questionsAndOptionsMO);
                }



                // rg.getChildAt(0).setSelected(true);
                if (editText != null && rg != null) {
                    rg.setTag(editText);
                }

                if (key == 10) {

                    nonMeetingLinearLayout.addView(rg);
                } else if (key == 11 || key == 12 || key == 13 || key == 14) {
                    meetingLinearLayout.addView(rg);
                } else {
                    linearLayout.addView(rg);
                }
                if (editText != null) {

                    if (key == 10) {
                        nonMeetingLinearLayout.addView(editText);
                    } else if (key == 11 || key == 12 || key == 13 || key == 14) {
                        meetingLinearLayout.addView(editText);
                    } else {
                        linearLayout.addView(editText);
                    }

                }

                View view = new View(this.getActivity());
                view.setPadding(10, 30, 10, 30);
                view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_grey));
                LinearLayout.LayoutParams divideLayoutParams = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.MATCH_PARENT, 2);
                divideLayoutParams.setMargins(0, 24, 0, 24);
                view.setLayoutParams(divideLayoutParams);
                if (key == 10) {

                    nonMeetingLinearLayout.addView(view);
                } else if (key == 11 || key == 12 || key == 13 || key == 14) {
                    meetingLinearLayout.addView(view);
                } else {
                    linearLayout.addView(view);
                }
            }
            meetingLinearLayout.setVisibility(View.GONE);
            linearLayout.addView(nonMeetingLinearLayout);
            linearLayout.addView(meetingLinearLayout);

            baseLayout.addView(linearLayout);
            ((BarrackInspectionTabActivity) getActivity()).checkMetLandlordTabEntry(linearLayout);

        }


    }


    @Override
    public void onCheckedChanged(RadioGroup mGroup, int checkedRadioButtonId) {

        mGroup.check(checkedRadioButtonId);
        for (int m = 0; m < mGroup.getChildCount(); m++) {

            if (checkedRadioButtonId == mGroup.getChildAt(m).getId()) {

                BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO =
                        (BarrackInspectionQuestionOptionMO)
                                mGroup.getChildAt(m).getTag();


                if (barrackInspectionQuestionOptionMO.getQuestionId() == 8) {
                    if (barrackInspectionQuestionOptionMO.getOptionId() == 7) {

                        meetingLinearLayout.setVisibility(View.VISIBLE);
                        photoRelativeLayout.setVisibility(View.VISIBLE);
                        nonMeetingLinearLayout.setVisibility(View.GONE);
                        clearEditexts(meetingLinearLayout);
                        clearImageViews(photoRelativeLayout);
                    } else if (barrackInspectionQuestionOptionMO.getOptionId() == 8) {
                        meetingLinearLayout.setVisibility(View.GONE);
                        nonMeetingLinearLayout.setVisibility(View.VISIBLE);
                        photoRelativeLayout.setVisibility(View.GONE);
                        clearEditexts(nonMeetingLinearLayout);
                    }
                } else {
                    if (barrackInspectionQuestionOptionMO.getOptionId() == 14 ||
                            barrackInspectionQuestionOptionMO.getOptionId() == 13) {
                        if (mGroup.getTag() instanceof EditText) {
                            mGroup.setTag(R.integer.isRelatedToEditText, true);
                            EditText editText = (EditText) mGroup.getTag();
                            editText.setVisibility(View.VISIBLE);
                            editText.setText("");
                        }
                    } else if (barrackInspectionQuestionOptionMO.getOptionId() == 12 ||
                            barrackInspectionQuestionOptionMO.getOptionId() == 7) {
                        if (mGroup.getTag() instanceof EditText) {
                            EditText editText = (EditText) mGroup.getTag();
                            editText.setVisibility(View.GONE);
                        }

                    } else if (barrackInspectionQuestionOptionMO.getOptionId() == 10 ||
                            barrackInspectionQuestionOptionMO.getOptionId() == 11) {
                        if (linearLayout != null) {
                            TextView textView = (TextView) linearLayout.findViewById(13);
                            textView.setText("Mobile Number/" +
                                    barrackInspectionQuestionOptionMO.getOptions());
                        }
                        clearEditexts(meetingLinearLayout);
                    }
                }


                if (null != questionsAndOptionsMOList && questionsAndOptionsMOList.size() != 0) {
                    for (int i = 0; i < questionsAndOptionsMOList.size(); i++) {

                        QuestionsAndOptionsMO questionsAndOptionsMO = new QuestionsAndOptionsMO();

                        if (questionsAndOptionsMOList.get(i).getQuestionId() == barrackInspectionQuestionOptionMO.getQuestionId()) {

                            questionsAndOptionsMOList.remove(i);
                            questionsAndOptionsMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                            questionsAndOptionsMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                            questionsAndOptionsMO.setRemarks(null);
                            questionsAndOptionsMOList.add(i, questionsAndOptionsMO);
                        }

                    }
                }

            }
        }
    }

    void clearEditexts(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        if (childCount != 0) {
            for (int count = 0; count < childCount; count++) {
                View child = viewGroup.getChildAt(count);
                if (child instanceof EditText) {
                    ((EditText) child).setText("");
                } else if (child instanceof ViewGroup) {
                    clearEditexts((ViewGroup) child);
                }
            }
        }
    }

    void clearImageViews(ViewGroup viewGroup){
        int childCount = viewGroup.getChildCount();
        if (childCount != 0) {
            for (int count = 0; count < childCount; count++) {
                View child = viewGroup.getChildAt(count);
                if (child instanceof ImageView) {
                    ((ImageView) child).setImageResource(R.drawable.camera_pic);
                } else if (child instanceof ViewGroup) {
                    clearImageViews((ViewGroup) child);
                }
            }
        }
    }

    public EditText setEditTextMaxLength(int length, EditText editText) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
        return editText;
    }

    @Override
    public void onClick(View v) {
        Intent capturePictureActivity = null;

        switch (v.getId()){

            case R.id.post_one_image :
                Util.mSequenceNumber++;
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_LANDLORD);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_LANDLORD_IMAGE_CODE);
                break;
            case R.id.post_two_image :
                Util.mSequenceNumber++;
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_CUSTODIAN);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_CUSTODIAN_IMAGE_CODE);
                break;
        }



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra
                    (Constants.META_DATA)).getAttachmentPath();
            AttachmentMetaDataModel attachmentModel = null;
            attachmentModel = data.getParcelableExtra(Constants.META_DATA);
            ((BarrackInspectionTabActivity) getActivity()).metaDataSetUp(requestCode, attachmentModel);
            switch (requestCode){
                case Constants.BARRACK_LANDLORD_IMAGE_CODE :
                    landlordImageView.setImageURI(Uri.parse(imagePath));
                    break;
                case Constants.BARRACK_CUSTODIAN_IMAGE_CODE :
                    custodianImageView.setImageURI(Uri.parse(imagePath));
                    break;
            }

        }
    }


}
