package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.annualkitreplacement.KitItemModel;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.network.request.UnitTypeDataMO;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecuirtementMO;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionsData;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.RotaMO;
import com.sisindia.ai.android.rota.models.SecurityRiskMO;
import com.sisindia.ai.android.rota.models.SecurityRiskQuestionOptionMappingMO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 22/6/16.
 */

public class LoadConfigurationData implements Serializable {

    @SerializedName("UserProfile")
    @Expose
    private UserProfile userProfile;
    @SerializedName("Units")
    @Expose
    private List<Unit> units = new ArrayList<>();
    @SerializedName("Barrack")
    @Expose
    private List<Barrack> barrack = new ArrayList<>();
    @SerializedName("Branch")
    @Expose
    private Branch branch;
    @SerializedName("Areas")
    @Expose
    private List<Areas> areas = new ArrayList<>();
    @SerializedName("HolidayCalendar")
    @Expose
    private List<HolidayCalendar> holidayCalendar = new ArrayList<>();
    @SerializedName("EmployeeLeaveCalendar")
    @Expose
    private List<EmployeeLeaveCalendar> employeeLeaveCalendar = new ArrayList<>();
    @SerializedName("Rota")
    @Expose
    private RotaMO rotaMO;
    @SerializedName("RankMaster")
    @Expose
    private List<RankMaster> rankMaster = new ArrayList<>();

    @SerializedName("SecurityRiskQuestionModels")
    @Expose
    private List<SecurityRiskMO> mSecurityRiskMOs = new ArrayList<>();

    @SerializedName("SecurityRiskQuestionOptionMappingModels")
    @Expose
    private List<SecurityRiskQuestionOptionMappingMO> mSecurityRiskQuestionOptionMappingMOs = new ArrayList<>();

    @SerializedName("Equipments")
    @Expose
    private List<Equipments> equipmentsList = new ArrayList<>();

    @SerializedName("LookupModel")
    @Expose
    private List<LookupModel> lookupModel = new ArrayList<>();

    @SerializedName("ClientCoordinationQuestions")
    public List<QuestionsData> clientCoordinationQuestions = new ArrayList<>();

    @SerializedName("BarrackInspectionQuestions")
    @Expose
    private List<BarrackInspectionQuestionMO> barrackInspectionQuestionMO = new ArrayList<>();

    @SerializedName("BarrackInspectionQuestionOptionMappingModels")
    @Expose
    private List<BarrackInspectionOptionMO> barrackInspectionOptionMO = new ArrayList<>();

    @SerializedName("KitItemModel")
    @Expose
    private List<KitItemModel> kitItemModel = new ArrayList<>();

    @SerializedName("MasterActionPlanModel")
    @Expose
    private List<MasterActionPlanModel> masterActionPlanModel = new ArrayList<>();
    @SerializedName("IssueMatrixModel")
    @Expose
    private List<IssueMatrixModel> issueMatrixModel = new ArrayList<>();
    /**
     * @return The userProfile
     */

    @SerializedName("KitApparelSizes")
    private List<KitApparelSizeOR> kitApparelSizes = new ArrayList<>();


    @SerializedName("KitDistributions")
    private List<KitDistributionOR> kitDistributions;

    public List<KitDistributionOR> getKitDistributions() {
        return kitDistributions;
    }

    @SerializedName("Recruitments")
    @Expose
    private List<RecuirtementMO> recuirtementMOList = new ArrayList<>();

    @SerializedName("UnitType")
    @Expose
    private List<UnitTypeDataMO> unitTypeList = new ArrayList<>();

    public List<RecuirtementMO> getRecuirtementMOList() {
        return recuirtementMOList;
    }

    public void setRecuirtementMOList(List<RecuirtementMO> recuirtementMOList) {
        this.recuirtementMOList = recuirtementMOList;
    }

    public void setKitDistributions(List<KitDistributionOR> kitDistributions) {
        this.kitDistributions = kitDistributions;
    }

    /**
     * @return The kitApparelSizes
     */
    public List<KitApparelSizeOR> getKitApparelSizes() {
        return kitApparelSizes;
    }

    /**
     * @param kitApparelSizes The KitApparelSizes
     */
    public void setKitApparelSizes(List<KitApparelSizeOR> kitApparelSizes) {
        this.kitApparelSizes = kitApparelSizes;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     * @param userProfile The UserProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    /**
     * @return The units
     */
    public List<Unit> getUnits() {
        return units;
    }

    /**
     * @param units The Units
     */
    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    /**
     * @return The barrack
     */
    public List<Barrack> getBarrack() {
        return barrack;
    }

    /**
     * @param barrack The Barrack
     */
    public void setBarrack(List<Barrack> barrack) {
        this.barrack = barrack;
    }

    /**
     * @return The branch
     */
    public Branch getBranch() {
        return branch;
    }

    /**
     * @param branch The Branch
     */
    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    /**
     * @return The areas
     */
    public List<Areas> getAreas() {
        return areas;
    }

    /**
     * @param areas The Areas
     */
    public void setAreas(List<Areas> areas) {
        this.areas = areas;
    }


    /**
     * @return The rotaMO
     */
    public RotaMO getRotaMO() {
        return rotaMO;
    }

    /**
     * @param rotaMO The RotaMO
     */
    public void setRotaMO(RotaMO rotaMO) {
        this.rotaMO = rotaMO;
    }

    /**
     * @return The rankMaster
     */
    public List<RankMaster> getRankMaster() {
        return rankMaster;
    }

    /**
     * @param rankMaster The RankMaster
     */
    public void setRankMaster(List<RankMaster> rankMaster) {
        this.rankMaster = rankMaster;
    }

    public List<SecurityRiskMO> getmSecurityRiskMOs() {
        return mSecurityRiskMOs;
    }

    public void setmSecurityRiskMOs(List<SecurityRiskMO> mSecurityRiskMOs) {
        this.mSecurityRiskMOs = mSecurityRiskMOs;
    }

    public List<SecurityRiskQuestionOptionMappingMO> getmSecurityRiskQuestionOptionMappingMOs() {
        return mSecurityRiskQuestionOptionMappingMOs;
    }

    public void setmSecurityRiskQuestionOptionMappingMOs(List<SecurityRiskQuestionOptionMappingMO> mSecurityRiskQuestionOptionMappingMOs) {
        this.mSecurityRiskQuestionOptionMappingMOs = mSecurityRiskQuestionOptionMappingMOs;
    }

    /**
     * @return The lookupModel
     */
    public List<LookupModel> getLookupModel() {
        return lookupModel;
    }

    /**
     * @param lookupModel The LookupModel
     */
    public void setLookupModel(List<LookupModel> lookupModel) {
        this.lookupModel = lookupModel;
    }

    public List<BarrackInspectionQuestionMO> getBarrackInspectionQuestionMO() {
        return barrackInspectionQuestionMO;
    }

    public void setBarrackInspectionQuestionMO(List<BarrackInspectionQuestionMO> barrackInspectionQuestionMO) {
        this.barrackInspectionQuestionMO = barrackInspectionQuestionMO;
    }

    public List<BarrackInspectionOptionMO> getBarrackInspectionOptionMO() {
        return barrackInspectionOptionMO;
    }

    public void setBarrackInspectionOptionMO(List<BarrackInspectionOptionMO> barrackInspectionOptionMO) {
        this.barrackInspectionOptionMO = barrackInspectionOptionMO;
    }

    /**
     * @return The masterActionPlanModel
     */
    public List<MasterActionPlanModel> getMasterActionPlanModel() {
        return masterActionPlanModel;
    }

    /**
     * @param masterActionPlanModel The MasterActionPlanModel
     */
    public void setMasterActionPlanModel(List<MasterActionPlanModel> masterActionPlanModel) {
        this.masterActionPlanModel = masterActionPlanModel;
    }

    public List<IssueMatrixModel> getIssueMatrixModel() {
        return issueMatrixModel;
    }

    public void setIssueMatrixModel(List<IssueMatrixModel> issueMatrixModel) {
        this.issueMatrixModel = issueMatrixModel;
    }

    /**
     * @return The kitItemModel
     */
    public List<KitItemModel> getKitItemModel() {
        return kitItemModel;
    }

    /**
     * @param kitItemModel The KitItemModel
     */
    public void setKitItemModel(List<KitItemModel> kitItemModel) {
        this.kitItemModel = kitItemModel;
    }

    /**
     * @return The holidayCalendar
     */
    public List<HolidayCalendar> getHolidayCalendar() {
        return holidayCalendar;
    }

    /**
     * @param holidayCalendar The HolidayCalendar
     */
    public void setHolidayCalendar(List<HolidayCalendar> holidayCalendar) {
        this.holidayCalendar = holidayCalendar;
    }

    /**
     * @return The employeeLeaveCalendar
     */
    public List<EmployeeLeaveCalendar> getEmployeeLeaveCalendar() {
        return employeeLeaveCalendar;
    }

    /**
     * @param employeeLeaveCalendar The EmployeeLeaveCalendar
     */
    public void setEmployeeLeaveCalendar(List<EmployeeLeaveCalendar> employeeLeaveCalendar) {
        this.employeeLeaveCalendar = employeeLeaveCalendar;
    }

    @SerializedName("KitItemSizes")
    @Expose
    private List<KitItemSizeOR> kitItemSizes = new ArrayList<KitItemSizeOR>();

    /**
     * @return The kitItemSizes
     */
    public List<KitItemSizeOR> getKitItemSizes() {
        return kitItemSizes;
    }

    /**
     * @param kitItemSizes The KitItemSizes
     */
    public void setKitItemSizes(List<KitItemSizeOR> kitItemSizes) {
        this.kitItemSizes = kitItemSizes;
    }

    public List<Equipments> getEquipmentsList() {
        return equipmentsList;
    }

    public void setEquipmentsList(List<Equipments> equipmentsList) {
        this.equipmentsList = equipmentsList;
    }

    public List<UnitTypeDataMO> getUnitTypeList() {
        return unitTypeList;
    }

    @Override
    public String toString() {
        return "LoadConfigurationData{" +
                "userProfile=" + userProfile +
                ", units=" + units +
                ", barrack=" + barrack +
                ", branch=" + branch +
                ", areas=" + areas +
                ", holidayCalendar=" + holidayCalendar +
                ", employeeLeaveCalendar=" + employeeLeaveCalendar +
                ", rotaMO=" + rotaMO +
                ", rankMaster=" + rankMaster +
                ", mSecurityRiskMOs=" + mSecurityRiskMOs +
                ", mSecurityRiskQuestionOptionMappingMOs=" + mSecurityRiskQuestionOptionMappingMOs +
                ", equipmentsList=" + equipmentsList +
                ", lookupModel=" + lookupModel +
                ", clientCoordinationQuestions=" + clientCoordinationQuestions +
                ", barrackInspectionQuestionMO=" + barrackInspectionQuestionMO +
                ", barrackInspectionOptionMO=" + barrackInspectionOptionMO +
                ", kitItemModel=" + kitItemModel +
                ", masterActionPlanModel=" + masterActionPlanModel +
                ", issueMatrixModel=" + issueMatrixModel +
                ", kitApparelSizes=" + kitApparelSizes +
                ", kitDistributions=" + kitDistributions +
                ", kitItemSizes=" + kitItemSizes +
                '}';
    }
}
