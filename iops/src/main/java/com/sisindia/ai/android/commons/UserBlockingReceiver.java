package com.sisindia.ai.android.commons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.utils.SyncStartUpServiceAfterUserBlock;

/**
 * Created by compass on 9/12/2017.
 */

public class UserBlockingReceiver extends BroadcastReceiver {
    AppPreferences appPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Alarm triggered for sync startup service ", Toast.LENGTH_LONG).show();
        appPreferences = new AppPreferences(context);
        Intent serviceIntent = new Intent(context, SyncStartUpServiceAfterUserBlock.class);
        context.startService(serviceIntent);
    }
}
