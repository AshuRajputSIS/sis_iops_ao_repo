package com.sisindia.ai.android.database.recuirtmentDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.AddRecruitmentIR;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.TotalRecruite;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 04-08-2016.
 */
public class RecuritmentLevelStatementDB extends SISAITrackingDB {

    public RecuritmentLevelStatementDB(Context context) {
        super(context);
    }

    public void updateRecurimentId(int id, int recuritID) {

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.RECRUITMENT_TABLE +
                " SET " + TableNameAndColumnStatement.RECRUIT_ID + " = " + "'" + recuritID + "'" +
                " ," + TableNameAndColumnStatement.IS_NEW + " = " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + " =  " + 1 +
                " WHERE " +
                TableNameAndColumnStatement.ID + " = " + "'" + id + "'";

        Timber.d("UpdateIssueTable  %s", updateQuery);
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

    }

    public synchronized void deleteRecruitmentTable() {

        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                sqlite.delete(TableNameAndColumnStatement.RECRUITMENT_TABLE, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }


    }


    public synchronized void addNewRecruitment(AddRecruitmentIR recruitmentIR) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.FULL_NAME, recruitmentIR.getFullName());
                values.put(TableNameAndColumnStatement.CONTACT_NO, recruitmentIR.getContactNo());
                values.put(TableNameAndColumnStatement.DATE_OF_BIRTH, recruitmentIR.getDateOfBirth());
                values.put(TableNameAndColumnStatement.IS_SELECTED, 0);
                values.put(TableNameAndColumnStatement.IS_REJECTED, 0);
                values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                values.put(TableNameAndColumnStatement.ACTION_TAKEN_ON, recruitmentIR.getActionTakenOn());
                values.put(TableNameAndColumnStatement.STATUS, recruitmentIR.getStatus());
                sqlite.insert(TableNameAndColumnStatement.RECRUITMENT_TABLE, null, values);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {

                    sqlite.close();
                }
            }
        }
    }

    public synchronized void addRecurimentDetails(List<TotalRecruite> toalrecruit) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

            try {
                for (TotalRecruite recurimentHireMO : toalrecruit) {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.RECRUIT_ID, recurimentHireMO.getRecruitmentId());
                    values.put(TableNameAndColumnStatement.FULL_NAME, recurimentHireMO.getFullName());
                    values.put(TableNameAndColumnStatement.CONTACT_NO, recurimentHireMO.getContactNo());
                    values.put(TableNameAndColumnStatement.DATE_OF_BIRTH, recurimentHireMO.getDOB());
                    values.put(TableNameAndColumnStatement.IS_SELECTED, recurimentHireMO.getIsSelected());
                    values.put(TableNameAndColumnStatement.IS_REJECTED, recurimentHireMO.getIsRejected());
                    values.put(TableNameAndColumnStatement.ACTION_TAKEN_ON, recurimentHireMO.getActionTakenOn());
                    values.put(TableNameAndColumnStatement.STATUS, recurimentHireMO.getStatus());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    sqlite.insert(TableNameAndColumnStatement.RECRUITMENT_TABLE, null, values);

                }

                sqlite.setTransactionSuccessful();


            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {

                    sqlite.close();
                }
            }
        }

    }

    public List<TotalRecruite> getRecruitmentList() {
        List<TotalRecruite> recruitmentIRList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.RECRUITMENT_TABLE;
//        Timber.d("getRecruitmentList selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        TotalRecruite addRecruitmentIR = new TotalRecruite();
                        addRecruitmentIR.setActionTakenOn(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_TAKEN_ON)));
                        addRecruitmentIR.setFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FULL_NAME)));
                        addRecruitmentIR.setDOB(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DATE_OF_BIRTH)));
                        addRecruitmentIR.setContactNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CONTACT_NO)));
                        addRecruitmentIR.setStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.STATUS)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_SELECTED)) == 1) {
                            addRecruitmentIR.setIsSelected(true);
                        } else {
                            addRecruitmentIR.setIsSelected(false);
                        }
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_REJECTED)) == 1) {
                            addRecruitmentIR.setIsRejected(true);
                        } else {
                            addRecruitmentIR.setIsRejected(false);
                        }

                        recruitmentIRList.add(addRecruitmentIR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return recruitmentIRList;
    }
}

