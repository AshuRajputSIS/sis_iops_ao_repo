package com.sisindia.ai.android.rota;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;


/**
 * Created by Durga Prasad on 20-03-2016.
 */
public class AddTaskRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    ArrayList<RotaTaskListMO> rotaTaskList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public AddTaskRecyclerAdapter(Context context) {
        this.context = context;

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setRotaData(ArrayList<RotaTaskListMO> addTaskModelList) {
        this.rotaTaskList = addTaskModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;

        if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.header_row, parent, false);
            recycleViewHolder = new HeaderViewHolder(itemView);

        } else if (viewType == TYPE_ROW) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_addtask_row, parent, false);
            recycleViewHolder = new RotaViewHolder(itemView);

        }

        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RotaTaskListMO addTaskModel = (RotaTaskListMO) getObject(position);
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.headerTextview.setText(addTaskModel.getRotaTaskName());
                break;
            case TYPE_ROW:
                RotaViewHolder rowDataHolder = (RotaViewHolder) holder;
                rowDataHolder.checkingTextview.setText(addTaskModel.getRotaTaskName());
                if(addTaskModel.getRotaTaskBlackIcon() != 0) {
                    rowDataHolder.typeIcon.setImageResource(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(addTaskModel.getRotaTaskBlackIcon()).name()).getValue());
                }
                rowDataHolder.nextIcon.setImageResource(R.drawable.arrow_right);

                break;
        }


    }

    @Override
    public int getItemViewType(int position) {

        if (position != 0) {
            return TYPE_ROW;
        } else {
            return TYPE_HEADER;
        }
        /*if(rotaTaskList.get(position) instanceof  RotaTaskListMO){
            return  TYPE_HEADER;
        }
        else if(rotaTaskList.get(position) instanceof  RotaTaskListMO){
           return  TYPE_ROW;
        }

     return  -1;*/
    }

    @Override
    public int getItemCount() {
        int count = rotaTaskList == null ? 0 : rotaTaskList.size();
        return count;
    }

    private Object getObject(int position) {
        return rotaTaskList.get(position);
    }

    public class RotaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView checkingTextview;
        ImageView typeIcon, nextIcon;

        public RotaViewHolder(View view) {
            super(view);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkType);
            this.typeIcon = (ImageView) view.findViewById(R.id.checkTypeImage);
            this.nextIcon = (ImageView) view.findViewById(R.id.nextarrow);
            view.setOnClickListener(this
            );

        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView headerTextview;

        public HeaderViewHolder(View view) {
            super(view);
            this.headerTextview = (CustomFontTextview) view.findViewById(R.id.recycle_header);
        }

    }
}
