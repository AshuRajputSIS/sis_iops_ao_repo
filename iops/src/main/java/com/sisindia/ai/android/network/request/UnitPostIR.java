package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

public class UnitPostIR {


    @SerializedName("Id")
    private Integer id;
    @SerializedName("UnitId")
    private Integer unitId;
    @SerializedName("UnitPostName")
    private String unitPostName;
    @SerializedName("IsActive")
    private Integer isActive;
    @SerializedName("GeoPoint")
    private String geoPoint;
    @SerializedName("Description")
    private String description;
    @SerializedName("MainGateDistance")
    private Double mainGateDistance;
    @SerializedName("BarrackDistance")
    private Double barrackDistance;
    @SerializedName("UnitOfficeDistance")
    private Double unitOfficeDistance;
    @SerializedName("ArmedStrength")
    private Integer armedStrength;
    @SerializedName("UnarmedStrength")
    private Integer unarmedStrength;
    @SerializedName("IsMainGate")
    private Integer isMainGate;
    @SerializedName("DeviceNo")
    private String deviceNo;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The unitPostName
     */
    public String getUnitPostName() {
        return unitPostName;
    }

    /**
     * @param unitPostName The UnitPostName
     */
    public void setUnitPostName(String unitPostName) {
        this.unitPostName = unitPostName;
    }

    /**
     * @return The isActive
     */
    public Integer getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The geoPoint
     */
    public String getGeoPoint() {
        return geoPoint;
    }

    /**
     * @param geoPoint The GeoPoint
     */
    public void setGeoPoint(String geoPoint) {
        this.geoPoint = geoPoint;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The mainGateDistance
     */
    public Double getMainGateDistance() {
        return mainGateDistance;
    }

    /**
     * @param mainGateDistance The MainGateDistance
     */
    public void setMainGateDistance(Double mainGateDistance) {
        this.mainGateDistance = mainGateDistance;
    }

    /**
     * @return The barrackDistance
     */
    public Double getBarrackDistance() {
        return barrackDistance;
    }

    /**
     * @param barrackDistance The BarrackDistance
     */
    public void setBarrackDistance(Double barrackDistance) {
        this.barrackDistance = barrackDistance;
    }

    /**
     * @return The unitOfficeDistance
     */
    public Double getUnitOfficeDistance() {
        return unitOfficeDistance;
    }

    /**
     * @param unitOfficeDistance The UnitOfficeDistance
     */
    public void setUnitOfficeDistance(Double unitOfficeDistance) {
        this.unitOfficeDistance = unitOfficeDistance;
    }

    /**
     * @return The armedStrength
     */
    public Integer getArmedStrength() {
        return armedStrength;
    }

    /**
     * @param armedStrength The ArmedStrength
     */
    public void setArmedStrength(Integer armedStrength) {
        this.armedStrength = armedStrength;
    }

    /**
     * @return The unarmedStrength
     */
    public Integer getUnarmedStrength() {
        return unarmedStrength;
    }

    /**
     * @param unarmedStrength The UnarmedStrength
     */
    public void setUnarmedStrength(Integer unarmedStrength) {
        this.unarmedStrength = unarmedStrength;
    }

    /**
     * @return The isMainGate
     */
    public Integer getIsMainGate() {
        return isMainGate;
    }

    /**
     * @param isMainGate The IsMainGate
     */
    public void setIsMainGate(Integer isMainGate) {
        this.isMainGate = isMainGate;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    @Override
    public String toString() {
        return "UnitPostIMO{" +
                "id=" + id +
                ", unitId=" + unitId +
                ", unitPostName='" + unitPostName + '\'' +
                ", isActive=" + isActive +
                ", geoPoint='" + geoPoint + '\'' +
                ", description='" + description + '\'' +
                ", mainGateDistance=" + mainGateDistance +
                ", barrackDistance=" + barrackDistance +
                ", unitOfficeDistance=" + unitOfficeDistance +
                ", armedStrength=" + armedStrength +
                ", unarmedStrength=" + unarmedStrength +
                ", isMainGate=" + isMainGate +
                '}';
    }
}