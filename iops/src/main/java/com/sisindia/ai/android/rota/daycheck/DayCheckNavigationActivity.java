package com.sisindia.ai.android.rota.daycheck;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.UpdateStatementDB;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;
import com.sisindia.ai.android.rota.daycheck.taskExecution.NfcUnitPost;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity.BARCODE_CAPTURE_REQUEST_CODE;

/**
 * Created by Shushrut on 09-06-2016.
 */
public class DayCheckNavigationActivity extends DayCheckBaseActivity
        implements OnStepperUpdateListener, OnRecyclerViewItemClickListener, DialogClickListener {

    @Bind(R.id.stepper_home_page)
    View mStepperViewParent;

    @Bind(R.id.tv_scanned_posts)
    TextView tvScannedPosts;
    @Bind(R.id.tv_un_scanned_posts)
    TextView tvUnScannedPosts;
    @Bind(R.id.scanQRAtDcNcTask)
    TextView scanQRAtDcNcTask;

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private List<NfcUnitPost> nfcPostList;
    private String nfcUniqueId;
//    private boolean isQRScanned = false;

    /*BroadcastReceiver nfcCheckpointBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            nfcUniqueId = intent.getStringExtra(Constants.Nfc.NFC_UNIQUE_ID);
            broadcastNfcDataIfWithinThirtyMeters();
        }
    };*/

    private List<DayCheckActivityList> activityLists;
    private RecyclerView rvContentHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_task_progress);
        ButterKnife.bind(this);
        OnStepperUpdateListener mOnStepperUpdateListener = this;
        createDayCheckActivityList();
        if (rotaTaskModel != null)
            setHeaderUnitName(rotaTaskModel.getUnitName());
        getStepperViews(mStepperViewParent);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rvContentHolder = (RecyclerView) findViewById(R.id.contentholder_recyclerview);
        rvContentHolder.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new DayCheckListAdapter(this, activityLists);
        rvContentHolder.setAdapter(mAdapter);
        mAdapter.setOnRecyclerViewItemClickListener(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.dayCheckingTitle);
        }
        Util.setmHandlerInstance(mOnStepperUpdateListener);
        completedTaskCountLayout.setVisibility(View.GONE);
        resetCheckpoints();
    }

    private List<DayCheckActivityList> createDayCheckActivityList() {
        activityLists = new ArrayList<>();
        DayCheckActivityList dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Strength Check");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(1);
        activityLists.add(0, dayCheckActivityList);

        dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Guard Check");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(2);
        activityLists.add(1, dayCheckActivityList);

        dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Duty Register Check");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(3);
        activityLists.add(2, dayCheckActivityList);

        dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Client Register Check");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(4);
        activityLists.add(3, dayCheckActivityList);

        dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Security Risk Observations");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(5);
        activityLists.add(4, dayCheckActivityList);

        dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityName("Client Handshake");
        dayCheckActivityList.setActivityStatus(false);
        dayCheckActivityList.setActivityId(6);
        activityLists.add(5, dayCheckActivityList);
        return activityLists;
    }

    private void resetCheckpoints() {
        NFCCheckpoint.getInstance().clearCheckpoints();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_day_check, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder() != null &&
                StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().size() > 0) {
            HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
            if (null != mStepperDataHolder && mStepperDataHolder.size() != 0) {
                for (Map.Entry<String, Integer> entry : mStepperDataHolder.entrySet()) {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    updateDayCheckUIStepper(value, GREEN_CIRCLE);
                    updateStepperConnector(value, stepperLineConnectorList, GREEN_LINE);
                }
            }
        }
//        registerReceiver(nfcCheckpointBroadcastReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
        showScannedPosts();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.daycheck_done:
                if (appPreferences.isEnableGps(DayCheckNavigationActivity.this)) {
                    HashMap<String, Integer> stepperProgressList = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
                    getTaskExecutionResult();
                    dayNightChecking.nfcUnitPost = nfcPostList;
                    if (stepperProgressList != null && stepperProgressList.size() == DayCheckEnum.values().length) {

                        /*if (isQRScanned)
                            saveDayCheckTaskDetailsAfterValidation();
                        else
                            snackBarWithMesg(scanQRAtDcNcTask, "Please scan QR code first");*/

                        saveDayCheckTaskDetailsAfterValidation();

                    } else {
                        showFragmentDialog(getResources().getInteger(R.integer.NORMAL_DIALOG));
                    }
                } else {
                    showGpsDialog(getString(R.string.gps_configure_msg));
                }
                return true;

            case android.R.id.home:
                if (StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().size() == 0) {
                    showConfirmationDialog(getResources().getInteger(R.integer.DAY_NIGHT_CHECK_CONFIRAMTION_DIALOG));
                } else {
                    Util.reSetGrievanceModelHolder();
                    Util.reSetComplaintModelHolder();
                    if (attachmentMetaArrayList != null) {
                        attachmentMetaArrayList.clear();
                    }
                    if (Util.getmGuardDetailsHolder() != null && Util.mGuardDetailsHolder.size() > 0) {
                        appPreferences.setCheckedGuardDetails(null);
                        Util.refreshGuardDetailsHolder();
                    }
                    finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveDayCheckTaskDetailsAfterValidation() {

        Util.showProgressBar(DayCheckNavigationActivity.this, R.string.saveMessage);

        dbUpdates();
        clearAndCleanUtilsObjects();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Util.dismissProgressBar(true);
                Intent homeActivityIntent = new Intent(DayCheckNavigationActivity.this, HomeActivity.class);
                homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeActivityIntent);
                finish();
            }
        }, 3000);
    }

    private void dbUpdates() {
        updateRotaTaskTable();
        updateMyPerformanceTable();
        updateGuardDetails();
        insertIssueData();
        insertMetaData();
        insertKitItem();
        insertIntoUnitStrength();

        triggerSyncAdapter();

        insertLogActivity();

        if (rotaTaskModel.getTaskId() < 0) {
            if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                        .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
            }
        }
    }

    private void clearAndCleanUtilsObjects() {
        clearJsonKitRequest();
        clearCheckedGuards();
        Util.getmGuardDetailsHolder().clear();
        StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().clear();
        attachmentMetaArrayList.clear();
        Util.mAudioSequenceNumber = 0;
    }

    private void updateGuardDetails() {
        List<GuardDetail> guardDetails = dayNightChecking.getGuardDetails();
        UpdateStatementDB updateStatementDB = IOPSApplication.getInstance().getUpdateStatementDBInstance();
        if (guardDetails != null && guardDetails.size() != 0) {
            for (GuardDetail guardDetail : guardDetails) {
                updateStatementDB.updateGuardDetails(rotaTaskModel.getUnitId(),
                        guardDetail.getEmployeeNo(),
                        guardDetail.getOverAllTurnout(),
                        guardDetail.getAmount());
            }
        }
    }

    private void insertIntoUnitStrength() {
        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().
                updateUnitEmpActualStrength(updatedActualStrength, true);
    }

    private void clearCheckedGuards() {
        appPreferences.setCheckedGuardDetails(null);
    }

    private void clearJsonKitRequest() {
        if (Util.getmGuardDetailsHolder() != null) {
            for (int index = 0; index < Util.getmGuardDetailsHolder().size(); index++) {
                appPreferences.setAddKitRequest(Util.getmGuardDetailsHolder().get(index).getEmployeeNo(), null);
            }
        }
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }

    private synchronized void insertKitItem() {
        if (Util.getmGuardDetailsHolder() != null) {
            for (int index = 0; index < Util.getmGuardDetailsHolder().size(); index++) {
                AddKitRequestMo addKitRequestMo = appPreferences.getAddKitRequest
                        (Util.getmGuardDetailsHolder().get(index).getEmployeeNo(), rotaTaskModel.getTaskId());
                if (addKitRequestMo != null) {
                    kitItemRequestMO = IOPSApplication.getGsonInstance().fromJson(addKitRequestMo.getAddKitRequestJson(),
                            KitItemRequestMO.class);
                    if (kitItemRequestMO != null) {
                        IOPSApplication.getInstance().getKitItemInsertionStatementDB().insertKitItemRequest(kitItemRequestMO);
                    }
                }
            }
        }
    }

    private synchronized void insertIssueData() {
        Log.e("daycheck ", "day check ::" + Util.getmComplaintModelHolder().size());
        if (Util.getmGrievanceModelHolder() != null && 0 != Util.getmGrievanceModelHolder().size()) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(Util.getmGrievanceModelHolder());
            Util.reSetGrievanceModelHolder();
        }
        if (Util.getmComplaintModelHolder() != null && 0 != Util.getmComplaintModelHolder().size()) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(Util.getmComplaintModelHolder());
            Util.reSetComplaintModelHolder();
        }
        if (Util.getmGuardDetailsHolder() != null && Util.mGuardDetailsHolder.size() > 0) {
            Util.refreshGuardDetailsHolder();
        }
    }

    private synchronized void updateMyPerformanceTable() {
        IOPSApplication.getInstance().getMyPerformanceStatementsDB()
                .UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(),
                        dateTimeFormatConversionUtil.getCurrentTime(),
                        String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTableFromHashMap(attachmentMetaArrayList);
        attachmentMetaArrayList.clear();
    }

    private synchronized void updateRotaTaskTable() {
        String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
        dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
        IOPSApplication.getInstance().getUpdateStatementDBInstance().
                updateTaskResult(rotaTaskModel.getTaskId(),
                        IOPSApplication.getGsonInstance().toJson(dayNightCheckingBaseMO),
                        RotaTaskStatusEnum.Completed.getValue(),
                        dateTimeFormatConversionUtil.getCurrentDateTime().toString(), geoLocation);
    }

    @Override
    public void updateStepper(String activityName, int position) {
        StepperDataMo.getmStepperDataMoInstance().addStepperUpdatePosition(activityName, position);
        updateActivityList(position, activityName, true);
        updateDayCheckUIStepper(position, GREEN_CIRCLE);
        updateStepperConnector(position, stepperLineConnectorList, GREEN_LINE);
    }

    private void updateActivityList(int position, String activityName, boolean activityStatus) {
        activityName = activityName.replace("_", " ");
        DayCheckActivityList dayCheckActivityList = new DayCheckActivityList();
        dayCheckActivityList.setActivityStatus(activityStatus);
        dayCheckActivityList.setActivityName(activityName);
        dayCheckActivityList.setActivityId(position);
        activityLists.set(position - 1, dayCheckActivityList);
        mAdapter = new DayCheckListAdapter(this, activityLists);
        rvContentHolder.setAdapter(mAdapter);
        mAdapter.setOnRecyclerViewItemClickListener(this);
    }

    @Override
    public void downGradeStepper(String activityName, int position) {
        StepperDataMo.getmStepperDataMoInstance().removeItemFromStepper(activityName);
        updateDayCheckUIStepper(position, GREY_CIRCLE);
        updateStepperConnector(position, stepperLineConnectorList, GREY_LINE);
        updateActivityList(position, activityName, false);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent(DayCheckNavigationActivity.this, StrengthCheckActivity.class);
                break;
            case 1:
                intent = new Intent(DayCheckNavigationActivity.this, GuardCheckActivity.class);
                break;
            case 2:
                intent = new Intent(DayCheckNavigationActivity.this, DutyRegisterCheckActivity.class);
                break;
            case 3:
                intent = new Intent(DayCheckNavigationActivity.this, ClientRegisterCheckActivity.class);
                break;
            case 4:
                intent = new Intent(DayCheckNavigationActivity.this, SecurityRiskActivity.class);
                break;
            case 5:
                intent = new Intent(DayCheckNavigationActivity.this, ClientHandshakeActivity.class);
                break;
        }
        startActivity(intent);
    }

    private void broadcastNfcDataIfWithinThirtyMeters() {
        UnitPost postInformation = IOPSApplication.getInstance()
                .getUnitLevelSelectionQueryInstance().getPostInformation(nfcUniqueId, rotaTaskModel.getUnitId());
        if (postInformation != null) {
            try {
                String[] coOrdinates = postInformation.getGeoPoint().split(",");
                if (coOrdinates.length == 2) {
                    LatLng latLng = new LatLng(Double.valueOf(coOrdinates[0]), Double.valueOf(coOrdinates[1]));
                    Timber.d("QR Unit information Name: %s Post Id : %s LatLng : %s", postInformation.getUnitPostName(),
                            postInformation.getUnitId(), latLng);
                    if (isWithinThirtyMeters(latLng)) {
                        nfcLatLngWithinThirtyMeters(postInformation);
                        scanQRAtDcNcTask.setText("QR SCANNED");
                        scanQRAtDcNcTask.setTextColor(getResources().getColor(R.color.color_green));
                        snackBarWithMesg(scanQRAtDcNcTask, "QR scan has been matched");
//                        Toast.makeText(this, R.string.nfc_has_been_matched, Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(this, String.format(this.getString(R.string.nfc_not_matched),
                                postInformation.getUnitPostName()), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        } else {
            Toast.makeText(this, getString(R.string.nfc_not_configured), Toast.LENGTH_LONG).show();
        }
    }

    private void nfcLatLngWithinThirtyMeters(UnitPost postInformation) {
        UnitPost unitPost = new UnitPost();
        unitPost.setUnitPostName(postInformation.getUnitPostName());
        unitPost.setUnitId(postInformation.getUnitId());
        NFCCheckpoint.getInstance().addCheckpoint(unitPost);
        showScannedPosts();
    }

    private boolean isWithinThirtyMeters(LatLng latLng) {
        double distance = distanceCalculation(GpsTrackingService.latitude, GpsTrackingService.longitude,
                latLng.latitude, latLng.longitude);
        int distanceInMeters = (int) distance;
        return (distanceInMeters < 500);
    }

    private double distanceCalculation(double currentLatitude, double currentLongitude, double nfcLatitude, double nfcLongitude) {
        Location locationA = new Location("first");
        locationA.setLatitude(currentLatitude);
        locationA.setLongitude(currentLongitude);
        Location locationB = new Location("second");
        locationB.setLatitude(nfcLatitude);
        locationB.setLongitude(nfcLongitude);
        return locationA.distanceTo(locationB);
    }

    public void showScannedPosts() {
        List<UnitPost> checkPointList = NFCCheckpoint.getInstance().getCheckpoints();
        setScannedNfcPoints(getUnitPostsFromList(checkPointList, true));
        List<UnitPost> unitPostList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                .getNfcPostInformation(rotaTaskModel.getUnitId());
        setOtherCheckPoints(getUnitPostsFromList(unitPostList, false));
    }

    private void setScannedNfcPoints(String scannedNfc) {
        if (!TextUtils.isEmpty(scannedNfc)) {
            String nfcPostName = scannedNfc.substring(0, scannedNfc.length() - 2);
            tvScannedPosts.setText(nfcPostName);
        } else {
            tvScannedPosts.setText(R.string.no_scanned_posts);
        }
    }

    private void setOtherCheckPoints(String unitPostsWithNfc) {
        if (!TextUtils.isEmpty(unitPostsWithNfc)) {
            String nfcPostName = unitPostsWithNfc.substring(0, unitPostsWithNfc.length() - 2);
            tvUnScannedPosts.setText(nfcPostName);
        } else {
            tvUnScannedPosts.setText(R.string.no_nfc_for_this_unit);
        }
    }

    private String getUnitPostsFromList(List<UnitPost> unitPostList, boolean isFromScan) {
        if (unitPostList != null && unitPostList.size() > 0) {
            StringBuilder builder = new StringBuilder();
            if (isFromScan) {
                nfcPostList = new ArrayList<>();
            }

            for (UnitPost unitPost : unitPostList) {
                Timber.d("NFC Unit posts in day check : %s", unitPost.getUnitPostName());
                NfcUnitPost nfcUnitPost = new NfcUnitPost();
                nfcUnitPost.postId = unitPost.getUnitId();
                if (isFromScan) {
                    nfcPostList = new ArrayList<>();
                    nfcPostList.add(nfcUnitPost);
                }
                builder.append(unitPost.getUnitPostName()).append(", ");
            }
            return builder.toString();
        }
        return null;
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nfcCheckpointBroadcastReceiver);
    }*/

    @Override
    public void onBackPressed() {
        showConfirmationDialog(getResources().getInteger(R.integer.DAY_NIGHT_CHECK_CONFIRAMTION_DIALOG));
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        clearSdCardImages();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        clearJsonKitRequest();
        clearCheckedGuards();
        clearUnitStrength();
        StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().clear();
        Util.reSetGrievanceModelHolder();
        Util.reSetComplaintModelHolder();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearUnitStrength() {
        if (updatedActualStrength != null && updatedActualStrength.size() != 0) {
            updatedActualStrength.clear();
        }
    }

    private void clearSdCardImages() {
        for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
            if (attachmentMetaDataModel != null) {
                File file = new File(attachmentMetaDataModel.getAttachmentPath());
                if (file != null && file.exists()) {
                    file.delete();
                }
            }
        }
    }

    //Adding logic for QR scanning and respective result
    @OnClick(R.id.scanQRAtDcNcTask)
    public void scanQRAtDcNcTask() {
        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intent, BARCODE_CAPTURE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == BARCODE_CAPTURE_REQUEST_CODE && resultCode == RESULT_OK) {
                nfcUniqueId = data.getStringExtra(BarcodeCaptureActivity.BARCODE_VALUE);
                if (nfcUniqueId != null && !nfcUniqueId.trim().equals("")) {
                    broadcastNfcDataIfWithinThirtyMeters();
                } else {
                    snackBarWithMesg(scanQRAtDcNcTask, "QR Code is empty!!!");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }
}