package com.sisindia.ai.android.rota;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shankar on 20/4/16.
 */
public enum RotaTaskTypeEnum {


    // adding two enum for duty on and off
//    Duty_Off(-1),
//    Duty_On(0),
    Day_Checking(1),
    Night_Checking(2),
    Barrack_Inspection(3),
    Client_Coordination(4),
    Bill_Submission(5),
    Bill_Collection(6),
    Others(7);

    private static Map map = new HashMap<>();

    static {
        for (RotaTaskTypeEnum rotaTaskTypeEM : RotaTaskTypeEnum.values()) {
            map.put(rotaTaskTypeEM.value, rotaTaskTypeEM);
        }
    }

    private int value;

    RotaTaskTypeEnum(int value) {
        this.value = value;
    }

    public static RotaTaskTypeEnum valueOf(int taskType) {
        return (RotaTaskTypeEnum) map.get(taskType);
    }

    public int getValue() {
        return value;
    }
}
