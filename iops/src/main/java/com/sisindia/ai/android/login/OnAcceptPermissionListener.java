package com.sisindia.ai.android.login;

/**
 * Created by Durga Prasad on 13-06-2016.
 */
public interface OnAcceptPermissionListener {
    void onPermissionsAccepted();
}
