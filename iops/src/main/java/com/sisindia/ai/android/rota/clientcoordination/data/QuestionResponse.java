package com.sisindia.ai.android.rota.clientcoordination.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shivam on 27/7/16.
 */

public class QuestionResponse {

    /*"TaskExecutionResult": {
    "ClientCoordination": {
      "QuestionsAndOptions": [
        {
          "QuestionId": 1,
          "OptionValue": 3
        },
        {
          "QuestionId": 2,
          "OptionValue": 4
        }
      ],
      "ClientContactName": "test",
      "AreaInspectorRating": "Good"
    }
  },
  */

    @SerializedName("ClientCoordination")
    public ClientCoordination clientCoordination;

    public static class ClientCoordination {

        @SerializedName("GEOLocation")
        public String geoLocation ;

        @SerializedName("QuestionsAndOptions")
        public List<QuestionsAndOptions> questionsAndOptions;

        @SerializedName("ClientContactName")
        public String clientName;

        @SerializedName("ClientContactId")
        public int clientId;

        @SerializedName("AreaInspectorRating")
        public String areaInspectorRating;

        @SerializedName("IsSelfieTaken")
        public boolean isSelfieTaken;

        public static class QuestionsAndOptions {

            @SerializedName("QuestionId")
            public int questionId;

            @SerializedName("OptionId")
            public int optionId;

            @Override
            public String toString() {
                return "{QuestionId : " + questionId + ", OptionId : " + optionId + "}";
            }
        }
    }
}
