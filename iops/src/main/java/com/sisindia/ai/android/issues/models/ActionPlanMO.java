package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.SerializedName;

public class ActionPlanMO {

    @SerializedName("ActionPlanStatusId")

    private int actionPlanStatusId;
    @SerializedName("ApprovedById")
    private int approvedById;

    @SerializedName("ApprovedDate")
    private String approvedDate;

    @SerializedName("TargetEmployeeCode")
    private String targetEmployeeCode;

    @SerializedName("AssignedToDateTime")
    private String assignedToDateTime;

    @SerializedName("AssignedToId")
    private int assignedToId;

    @SerializedName("BarrackId")

    private int barrackId;
    @SerializedName("Budget")

    private int budget;
    @SerializedName("ClosureComment")

    private String closureComment;
    @SerializedName("ClosureDate")

    private String closureDate;
    @SerializedName("Description")

    private String description;
    @SerializedName("ExpectedClosureDate")

    private String expectedClosureDate;
    @SerializedName("Id")

    private int id;
    @SerializedName("ImprovementPlanCategoryId")

    private int improvementPlanCategoryId;
    @SerializedName("IssueId")

    private int issueId;
    @SerializedName("IssueMatrixId")

    private int issueMatrixId;
    @SerializedName("JsonActionPlan")

    private String jsonActionPlan;
    @SerializedName("KitRequirmentId")

    private int kitRequirmentId;
    @SerializedName("MasterActionPlanId")

    private int masterActionPlanId;
    @SerializedName("Name")

    private String name;
    @SerializedName("RaisedById")

    private int raisedById;
    @SerializedName("RaisedDate")

    private String raisedDate;
    @SerializedName("Remarks")

    private String remarks;
    @SerializedName("TargetEmployeeId")

    private int targetEmployeeId;
    @SerializedName("TargetEmployeeName")
    private String targetEmployeeName;

    @SerializedName("UnitId")
    private int unitId;

    @SerializedName("actionPlanId")
    private String actionPlanId;


    @SerializedName("ClientCoordinationQuestionId")
    private String clientCoordinationQuestionId;

    @SerializedName("SourceTaskId")
    private String sourceTaskId;


    @SerializedName("AssigneeName")
    private String assigneeName;

/**
* 
* @return
* The actionPlanStatusId
*/
public int getActionPlanStatusId() {
return actionPlanStatusId;
}

/**
* 
* @param actionPlanStatusId
* The ActionPlanStatusId
*/
public void setActionPlanStatusId(int actionPlanStatusId) {
this.actionPlanStatusId = actionPlanStatusId;
}

/**
* 
* @return
* The approvedById
*/
public int getApprovedById() {
return approvedById;
}

/**
* 
* @param approvedById
* The ApprovedById
*/
public void setApprovedById(int approvedById) {
this.approvedById = approvedById;
}

/**
* 
* @return
* The approvedDate
*/
public String getApprovedDate() {
return approvedDate;
}

/**
* 
* @param approvedDate
* The ApprovedDate
*/
public void setApprovedDate(String approvedDate) {
this.approvedDate = approvedDate;
}

/**
* 
* @return
* The assignedToId
*/
public int getAssignedToId() {
return assignedToId;
}

/**
* 
* @param assignedToId
* The AssignedToId
*/
public void setAssignedToId(int assignedToId) {
this.assignedToId = assignedToId;
}

/**
* 
* @return
* The barrackId
*/
public int getBarrackId() {
return barrackId;
}

/**
*
* @param barrackId
* The BarrackId
*/
public void setBarrackId(int barrackId) {
this.barrackId = barrackId;
}



/**
* 
* @return
* The closureComment
*/
public String getClosureComment() {
return closureComment;
}

/**
* 
* @param closureComment
* The ClosureComment
*/
public void setClosureComment(String closureComment) {
this.closureComment = closureComment;
}

/**
* 
* @return
* The closureDate
*/
public String getClosureDate() {
return closureDate;
}

/**
* 
* @param closureDate
* The ClosureDate
*/
public void setClosureDate(String closureDate) {
this.closureDate = closureDate;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The Description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The expectedClosureDate
*/
public String getExpectedClosureDate() {
return expectedClosureDate;
}

/**
* 
* @param expectedClosureDate
* The ExpectedClosureDate
*/
public void setExpectedClosureDate(String expectedClosureDate) {
this.expectedClosureDate = expectedClosureDate;
}

/**
* 
* @return
* The id
*/
public int getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(int id) {
this.id = id;
}

/**
* 
* @return
* The improvementPlanCategoryId
*/
public int getImprovementPlanCategoryId() {
return improvementPlanCategoryId;
}

/**
* 
* @param improvementPlanCategoryId
* The ImprovementPlanCategoryId
*/
public void setImprovementPlanCategoryId(int improvementPlanCategoryId) {
this.improvementPlanCategoryId = improvementPlanCategoryId;
}

/**
* 
* @return
* The issueId
*/
public int getIssueId() {
return issueId;
}

/**
* 
* @param issueId
* The IssueId
*/
public void setIssueId(int issueId) {
this.issueId = issueId;
}

/**
* 
* @return
* The issueMatrixId
*/
public int getIssueMatrixId() {
return issueMatrixId;
}

/**
* 
* @param issueMatrixId
* The IssueMatrixId
*/
public void setIssueMatrixId(int issueMatrixId) {
this.issueMatrixId = issueMatrixId;
}

/**
* 
* @return
* The jsonActionPlan
*/
public String getJsonActionPlan() {
return jsonActionPlan;
}

/**
* 
* @param jsonActionPlan
* The JsonActionPlan
*/
public void setJsonActionPlan(String jsonActionPlan) {
this.jsonActionPlan = jsonActionPlan;
}

/**
* 
* @return
* The kitRequirmentId
*/
public int getKitRequirmentId() {
return kitRequirmentId;
}

/**
* 
* @param kitRequirmentId
* The KitRequirmentId
*/
public void setKitRequirmentId(int kitRequirmentId) {
this.kitRequirmentId = kitRequirmentId;
}

/**
* 
* @return
* The masterActionPlanId
*/
public int getMasterActionPlanId() {
return masterActionPlanId;
}

/**
* 
* @param masterActionPlanId
* The MasterActionPlanId
*/
public void setMasterActionPlanId(int masterActionPlanId) {
this.masterActionPlanId = masterActionPlanId;
}

/**
* 
* @return
* The name
*/
public String getName() {
return name;
}

/**
* 
* @param name
* The Name
*/
public void setName(String name) {
this.name = name;
}

/**
* 
* @return
* The raisedById
*/
public int getRaisedById() {
return raisedById;
}

/**
* 
* @param raisedById
* The RaisedById
*/
public void setRaisedById(int raisedById) {
this.raisedById = raisedById;
}

/**
* 
* @return
* The raisedDate
*/
public String getRaisedDate() {
return raisedDate;
}

/**
* 
* @param raisedDate
* The RaisedDate
*/
public void setRaisedDate(String raisedDate) {
this.raisedDate = raisedDate;
}

/**
* 
* @return
* The remarks
*/
public String getRemarks() {
return remarks;
}

/**
* 
* @param remarks
* The Remarks
*/
public void setRemarks(String remarks) {
this.remarks = remarks;
}

/**
* 
* @return
* The targetEmployeeId
*/
public int getTargetEmployeeId() {
return targetEmployeeId;
}

/**
* 
* @param targetEmployeeId
* The TargetEmployeeId
*/
public void setTargetEmployeeId(int targetEmployeeId) {
this.targetEmployeeId = targetEmployeeId;
}

/**
* 
* @return
* The targetEmployeeName
*/
public String getTargetEmployeeName() {
return targetEmployeeName;
}

/**
* 
* @param targetEmployeeName
* The TargetEmployeeName
*/
public void setTargetEmployeeName(String targetEmployeeName) {
this.targetEmployeeName = targetEmployeeName;
}

/**
* 
* @return
* The unitId
*/
public int getUnitId() {
return unitId;
}

/**
* 
* @param unitId
* The UnitId
*/
public void setUnitId(int unitId) {
this.unitId = unitId;
}




    public String getActionPlanId() {
        return actionPlanId;
    }

    public void setActionPlanId(String actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    public String getAssignedToDateTime() {
        return assignedToDateTime;
    }

    public void setAssignedToDateTime(String assignedToDateTime) {
        this.assignedToDateTime = assignedToDateTime;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getClientCoordinationQuestionId() {
        return clientCoordinationQuestionId;
    }

    public void setClientCoordinationQuestionId(String clientCoordinationQuestionId) {
        this.clientCoordinationQuestionId = clientCoordinationQuestionId;
    }

    public String getSourceTaskId() {
        return sourceTaskId;
    }

    public void setSourceTaskId(String sourceTaskId) {
        this.sourceTaskId = sourceTaskId;
    }

    public String getTargetEmployeeCode() {
        return targetEmployeeCode;
    }

    public void setTargetEmployeeCode(String targetEmployeeCode) {
        this.targetEmployeeCode = targetEmployeeCode;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    @Override
    public String toString() {
        return "ActionPlanMO{" +
                "actionPlanId='" + actionPlanId + '\'' +
                ", actionPlanStatusId=" + actionPlanStatusId +
                ", approvedById=" + approvedById +
                ", approvedDate='" + approvedDate + '\'' +
                ", targetEmployeeCode='" + targetEmployeeCode + '\'' +
                ", assignedToDateTime='" + assignedToDateTime + '\'' +
                ", assignedToId=" + assignedToId +
                ", barrackId=" + barrackId +
                ", budget=" + budget +
                ", closureComment='" + closureComment + '\'' +
                ", closureDate='" + closureDate + '\'' +
                ", description='" + description + '\'' +
                ", expectedClosureDate='" + expectedClosureDate + '\'' +
                ", id=" + id +
                ", improvementPlanCategoryId=" + improvementPlanCategoryId +
                ", issueId=" + issueId +
                ", issueMatrixId=" + issueMatrixId +
                ", jsonActionPlan='" + jsonActionPlan + '\'' +
                ", kitRequirmentId=" + kitRequirmentId +
                ", masterActionPlanId=" + masterActionPlanId +
                ", name='" + name + '\'' +
                ", raisedById=" + raisedById +
                ", raisedDate='" + raisedDate + '\'' +
                ", remarks='" + remarks + '\'' +
                ", targetEmployeeId=" + targetEmployeeId +
                ", targetEmployeeName='" + targetEmployeeName + '\'' +
                ", unitId=" + unitId +
                ", clientCoordinationQuestionId='" + clientCoordinationQuestionId + '\'' +
                ", sourceTaskId='" + sourceTaskId + '\'' +
                ", assignedName='" + assigneeName + '\'' +
                '}';
    }
}

