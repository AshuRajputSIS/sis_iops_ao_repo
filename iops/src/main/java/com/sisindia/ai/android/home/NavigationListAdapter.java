package com.sisindia.ai.android.home;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

/**
 * Created by Saruchi on 19-03-2016.
 */
public class NavigationListAdapter extends BaseAdapter  {
    private static final String TAG = "NavigationListAdapter";
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_DIVIDER = 3;
    private final LayoutInflater layoutInflater;
    Context context;
    private ArrayList<NavigationModel> menuList = new ArrayList<NavigationModel>();
    private AppPreferences mAppPreferences;
    private GPSListener lister;
    private boolean isfailure;
    private AppUserData appUserData;
    // MenuItemClickListener menuItemClickListener;


    //    private String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
//    private int mIcons[];       // Int Array to store the passed icons resource value from MainActivity.java
    private UpdateDutyStatusListener updateDutyStatusUi;
    private HeaderViewHolder headerViewHolder;

    public NavigationListAdapter(Context context, ArrayList<NavigationModel> menuList, GPSListener lister,AppUserData appUserData) {
        this.context = context;
        this.menuList = menuList;
        mAppPreferences = new AppPreferences(context);
        this.lister = lister;
        this.appUserData = appUserData;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isfailure=false;
        setupLanguage();
    }

   /* public void setMenuItemClickListener(MenuItemClickListener menuItemClickListener){
        this.menuItemClickListener = menuItemClickListener;
    }*/

    @Override
    public int getCount() {
        return menuList.size() + 2;
    }

    @Override
    public Object getItem(int position) {
        if (position == getCount() - 1) {
            return position;
        } else {
            return menuList.get(position);

        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else if (position == 5) {
            return TYPE_DIVIDER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == menuList.size() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        int viewType = this.getItemViewType(position);
        switch (viewType) {
            case TYPE_HEADER:


                if (v == null) {
                    v = layoutInflater.inflate(R.layout.nav_header_main, parent, false);
                    headerViewHolder = new HeaderViewHolder(v);
                    v.setTag(headerViewHolder);
                } else {
                    headerViewHolder = (HeaderViewHolder) v.getTag();
                }
                headerViewHolder.userNameTxt.setText(mAppPreferences.getAppUser());
                headerViewHolder.mSwitch.setChecked(mAppPreferences.getDutyStatus());
                if (appUserData != null) {

                        if(!TextUtils.isEmpty(appUserData.getmImageUrl())){
                            loadAIImage(appUserData.getmImageUrl().trim());
                        }else{
                            headerViewHolder.mAIProfilePic.setImageResource(R.drawable.default_icon_ai);
                        }
                }
                        //check the current state before we display the screen
                        if (headerViewHolder.mSwitch.isChecked()) {
                            headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str) + " ");
                            headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.duty_started) + " ");
                            headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                            mAppPreferences.saveDutyStatus(true);
                            lister.enableGPSservice(true,false);

                        } else {
                            headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str) + " ");
                            headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.last_seen) + " ");
                            headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                            mAppPreferences.saveDutyStatus(false);
                            lister.enableGPSservice(false,false);
                        }
                headerViewHolder.mSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);

                return v;
            case TYPE_FOOTER:
                FooterViewHolder footerViewHolder;
                View footerview = convertView;
                if (footerview == null) {
                    footerview = layoutInflater.inflate(R.layout.menu_footer, parent, false);
                    footerViewHolder = new FooterViewHolder(footerview);
                    footerview.setTag(footerViewHolder);
                } else {
                }
                // return the created view
                return footerview;

            case TYPE_ITEM:
                GenericViewHolder genericViewHolder;
                View genericview = convertView;
                if (genericview == null) {
                    genericview = layoutInflater.inflate(R.layout.menu_row, parent, false);
                    genericViewHolder = new GenericViewHolder(genericview);
                    genericview.setTag(genericViewHolder);
                } else {
                    genericViewHolder = (GenericViewHolder) genericview.getTag();
                }
                genericViewHolder.txtName.setText(menuList.get(position - 1).getTitle());
                genericViewHolder.menuIcon.setBackgroundResource(menuList.get(position - 1).getIcon());
                if (menuList.get(position - 1).getisNotificationVisible() && menuList.get(position - 1).getNotificationCount() != 0) {
                    genericViewHolder.NotificationCount.setVisibility(View.VISIBLE);
                    genericViewHolder.NotificationCount.setText(menuList.get(position - 1).getNotificationCount() + "");
                } else {
                    genericViewHolder.NotificationCount.setVisibility(View.INVISIBLE);
                }
                // return the created view
                return genericview;

            case TYPE_DIVIDER:
                DividerViewHolder dividerViewHolder;

                View dividerview = convertView;
                if (dividerview == null) {
                    LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    dividerview = vi.inflate(R.layout.nav_horizontal_dividerline, parent, false);
                    dividerViewHolder = new DividerViewHolder(dividerview);
                    dividerview.setTag(dividerViewHolder);
                } else {
                    dividerViewHolder = (DividerViewHolder) dividerview.getTag();
                }
                // return the created view
                return dividerview;
            default:
                //Throw exception, unknown data type
        }

        return null;
    }

    class GenericViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        ImageView menuIcon;
        TextView NotificationCount;

        public GenericViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.rowText);
            this.menuIcon = (ImageView) itemView.findViewById(R.id.rowIcon);
            this.NotificationCount = (TextView) itemView.findViewById(R.id.noticount);
        }
    }

    class DividerViewHolder extends RecyclerView.ViewHolder {
        public DividerViewHolder(View itemView) {
            super(itemView);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView dutyoffTxt;
        RelativeTimeTextView lastSeenTxt;
        Switch mSwitch;
        CustomFontTextview userNameTxt;
        CircleImageView mAIProfilePic;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            this.dutyoffTxt = (TextView) itemView.findViewById(R.id.dutyoff_txt);
            this.lastSeenTxt = (RelativeTimeTextView) itemView.findViewById(R.id.lastseen_txt);
            this.mSwitch = (Switch) itemView.findViewById(R.id.switch1);
            this.userNameTxt = (CustomFontTextview) itemView.findViewById(R.id.userNameTxt);
            this.mAIProfilePic = (CircleImageView)itemView.findViewById(R.id.profileImage);
        }
    }

    public void failureResponse(boolean isfailure){
        if(headerViewHolder!=null) {
            if (headerViewHolder.mSwitch != null) {
                headerViewHolder.mSwitch.setOnCheckedChangeListener(null);
                headerViewHolder.mSwitch.setChecked(mAppPreferences.getDutyStatus());
                if (isfailure) {
                    if (headerViewHolder.mSwitch.isChecked()) {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                        headerViewHolder.lastSeenTxt.setPrefix("Started ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                        mAppPreferences.saveDutyStatus(true);
                    } else {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                        headerViewHolder.lastSeenTxt.setPrefix("Last Seen ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                        mAppPreferences.saveDutyStatus(false);
                    }
                } else {
                    if (headerViewHolder.mSwitch.isChecked()) {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                        mAppPreferences.setLastSeen(new Date().getTime());
                        headerViewHolder.lastSeenTxt.setPrefix("Started ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                    } else {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                        mAppPreferences.setLastSeen(new Date().getTime());
                        headerViewHolder.lastSeenTxt.setPrefix("Last Seen ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                    }
                }
                headerViewHolder.mSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);
            } else {
                // switch null
            }
        }
        else{
// holder null
        }

    }

    final CompoundButton.OnCheckedChangeListener toggleButtonChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            // The user changed the button, do something
            if (isChecked) {
                headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.duty_started) + " ");
                mAppPreferences.setLastSeen(new Date().getTime());
                headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                mAppPreferences.saveDutyStatus(true);
                lister.enableGPSservice(true,true);

            } else {
                headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.last_seen) + " ");
                mAppPreferences.setLastSeen(new Date().getTime());
                headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                mAppPreferences.saveDutyStatus(false);
                lister.enableGPSservice(false,true);
            }




           /* if (NetworkUtil.isConnected) {
                mAppPreferences.saveDutyStatusApiCall(true);
                lister.enableGPSservice(isChecked);


            } else {
                Util.showToast(context, context.getResources().getString(R.string.no_internet));
                mAppPreferences.saveDutyStatusApiCall(false);

            }*/


        }
    };

    /**
     * Loading  the AI image . If image is coming from LoadConfig then need to download and  display
     * else
     * Local uri and if local local uri is also not there then display the default image
     */
    private void loadAIImage( String mCapturedPicture) {
        if (!TextUtils.isEmpty(mCapturedPicture)) {
            if (!(mCapturedPicture.contains("http"))) {
                headerViewHolder. mAIProfilePic.setImageURI(Uri.parse(mCapturedPicture));
            } else {

                Glide.with(context)
                        .load(mCapturedPicture)
                        .placeholder(R.drawable.default_icon_ai)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into( headerViewHolder.mAIProfilePic);
            }
        } else {
            headerViewHolder.mAIProfilePic.setImageResource(R.drawable.default_icon_ai);
        }

    }


    protected void setLocale(String localeString, int languageId) {
        Locale locale = new Locale(localeString);
        if(languageId == Constants.HINDI_LANGUAGE_ID){
            locale.setDefault(locale);
            Configuration configuration = context.getApplicationContext().getResources().getConfiguration();
            configuration.locale = locale;
            context.getApplicationContext().getResources().updateConfiguration(configuration,
                  context.getApplicationContext().getResources().getDisplayMetrics());
        }

    }

    public void setupLanguage() {
        int  languageId = mAppPreferences.getLanguageId();
        String localeString = Locale.getDefault().getLanguage();

        if(languageId != 0){
            switch (languageId){
                case Constants.ENGLISH_LANGUAGE_ID :
                    break;
                case Constants.HINDI_LANGUAGE_ID :
                    if(!Constants.LOCALE_HINDI_INDENTIFIER.equalsIgnoreCase(localeString)) {
                        setLocale(Constants.LOCALE_HINDI_INDENTIFIER, Constants.HINDI_LANGUAGE_ID);
                    }
                    break;
            }
        }
        else{
            Timber.d(TAG + "%s", "onResume: Language is not selected yet");
        }
    }
}




