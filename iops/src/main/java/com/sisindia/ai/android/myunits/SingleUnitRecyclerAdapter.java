package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.SingleUnitMO;
import com.sisindia.ai.android.rota.RotaTaskBlackIconEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 29-03-2016.
 */
public class SingleUnitRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int IMPROVEMENT_ID = 10;
    public static final int ISSUES_ID = 11;
    public static final String VIEW_COMPLAINTS = "VIEW COMPLAINTS";
    public static final String VIEW_IMPROVEMENT_PLAN = "VIEW IMPROVEMENT PLAN";
    public static final String VIEW_HISTORY = "VIEW HISTORY";
    //List<SingleUnitCheckingTypesModel.CheckingType> singleUnitData;
    Context context;
    Resources resources;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private List<SingleUnitMO> singleUnitDataList;
    private OnViewHistoryClickListener onViewHistoryClickListener;


    public SingleUnitRecyclerAdapter(Context context) {
        this.context = context;
        resources = context.getResources();

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setOnViewHistoryClickListener(OnViewHistoryClickListener onViewHistoryClickListener){
        this.onViewHistoryClickListener = onViewHistoryClickListener;
    }

    public void setSingleUnitDataList(List<SingleUnitMO> singleUnitDataList) {
        this.singleUnitDataList = singleUnitDataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;

        View itemView = LayoutInflater.from(context).inflate(R.layout.single_unit_cardview, parent, false);

        recycleViewHolder = new SingleUnitViewHolder(itemView);

        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int taskTypeId = singleUnitDataList.get(position).getTaskTypeId();

        SingleUnitViewHolder singleUnitViewHolder = (SingleUnitViewHolder) holder;
        singleUnitViewHolder.singleunitChecktypeTxt
                .setText(singleUnitDataList.get(position).getTaskType());
        singleUnitViewHolder.singleunitChecktypeImage.
                setImageResource(RotaTaskBlackIconEnum.valueOf(
                        RotaTaskTypeEnum.valueOf(taskTypeId).name()).getValue());

        singleUnitViewHolder.singleunitCardviewNewCheck.setText(singleUnitDataList.get(position).getTaskType());
     if (IMPROVEMENT_ID == taskTypeId) {
            singleUnitViewHolder.singleunitCardviewMiddleLayout.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitCardviewMiddleAnnualkit.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitDividerTwo.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitCardviewViewHistory.setText(VIEW_IMPROVEMENT_PLAN);
            singleUnitViewHolder.singleunitCardviewNewListImage.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitCardviewNewCheck.setVisibility(View.GONE);
        } else if (ISSUES_ID == taskTypeId) {
            singleUnitViewHolder.singleunitCardviewMiddleLayout.setVisibility(View.VISIBLE);
            singleUnitViewHolder.singleunitCardviewMiddleAnnualkit.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitCardviewNewCheck.setText(VIEW_COMPLAINTS);
            singleUnitViewHolder.singleunitCardviewViewHistory.setText(VIEW_HISTORY);
            singleUnitViewHolder.singleunitCardviewNewListImage.setImageResource(R.drawable.list);
            singleUnitViewHolder.singleunitDividerTwo.setVisibility(View.VISIBLE);
        } else {
            singleUnitViewHolder.singleunitCardviewMiddleLayout.setVisibility(View.VISIBLE);
            singleUnitViewHolder.singleunitCardviewMiddleAnnualkit.setVisibility(View.GONE);
            singleUnitViewHolder.singleunitDividerTwo.setVisibility(View.VISIBLE);
            singleUnitViewHolder.singleunitCardviewViewHistory.setText(VIEW_HISTORY);
            singleUnitViewHolder.singleunitCardviewNewListImage.setVisibility(View.VISIBLE);
            singleUnitViewHolder.singleunitCardviewNewListImage.setImageResource(R.drawable.new_list);
            singleUnitViewHolder.singleunitCardviewNewCheck.setVisibility(View.VISIBLE);
            singleUnitViewHolder.singleunitCompletedCountTxt.setText(String.valueOf(singleUnitDataList.get(position).getCurrentMonthCompletedTasksCount()));
            int pendingCount = singleUnitDataList.get(position).getCurrentMonthAllTasksCount()-singleUnitDataList.get(position).getCurrentMonthCompletedTasksCount();
            singleUnitViewHolder.singleunitPendingCountTxt.setText(String.valueOf(pendingCount));
            String  lastCheckDate = singleUnitDataList.get(position).getLastActivityDate();

            if(  lastCheckDate != null  && !lastCheckDate.isEmpty()) {
                singleUnitViewHolder.singleunitLastCheckDateTv.setText(singleUnitDataList.get(position).getLastActivityDate());
            }
            else{
                singleUnitViewHolder.singleunitLastCheckDateTv.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
            }
        }


    }


    @Override
    public int getItemCount() {

        int count = singleUnitDataList == null ? 0 : singleUnitDataList.size();
        return count;
    }

    private Object getObject(int position) {
        return singleUnitDataList.get(position);
    }

    public class SingleUnitViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.singleunit_checktype_image)
        ImageView singleunitChecktypeImage;
        @Bind(R.id.singleunit_checktype_txt)
        TextView singleunitChecktypeTxt;
        @Bind(R.id.singleunit_divider_one)
        View singleunitDividerOne;
        @Bind(R.id.singleunit_cardview_middle_layout)
        LinearLayout singleunitCardviewMiddleLayout;
        @Bind(R.id.singleunit_completed_count_txt)
        CustomFontTextview singleunitCompletedCountTxt;
        @Bind(R.id.singleunit_cardview_completed_txt)
        CustomFontTextview singleunitCardviewCompletedTxt;
        @Bind(R.id.singleunit_issued_count_txt)
        CustomFontTextview singleunitIssuedCountTxt;
        @Bind(R.id.singleunit_cardview_issued_txt)
        CustomFontTextview singleunitCardviewIssuedTxt;
        @Bind(R.id.singleunit_delivered_count_txt)
        CustomFontTextview singleunitDeliveredCountTxt;
        @Bind(R.id.singleunit_cardview_delivered_txt)
        CustomFontTextview singleunitCardviewDeliveredTxt;
        @Bind(R.id.singleunit_pending_count_txt)
        CustomFontTextview singleunitPendingCountTxt;
        @Bind(R.id.singleunit_cardview_pending_txt)
        CustomFontTextview singleunitCardviewPendingTxt;
        @Bind(R.id.singleunit_cardview_middle_annualkit)
        LinearLayout singleunitCardviewMiddleAnnualkit;
        @Bind(R.id.relativeLayout)
        FrameLayout relativeLayout;
        @Bind(R.id.singleunit_divider_two)
        View singleunitDividerTwo;
        @Bind(R.id.singleunit_cardview_list_image)
        ImageView singleunitCardviewListImage;
        @Bind(R.id.singleunit_cardview_new_list_image)
        ImageView singleunitCardviewNewListImage;
        @Bind(R.id.singleunit_cardview_new_check)
        CustomFontTextview singleunitCardviewNewCheck;
        @Bind(R.id.singleunit_cardview_view_history)
        CustomFontTextview singleunitCardviewViewHistory;
        @Bind(R.id.singleunit_last_check_date_tv)
        CustomFontTextview singleunitLastCheckDateTv;
        @Bind(R.id.singleunit_last_check_label)
        CustomFontTextview singleunitLastCheckLabel;


        public SingleUnitViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            singleunitCardviewNewCheck.setOnClickListener(this);
            singleunitCardviewViewHistory.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            onViewHistoryClickListener.onViewHistoryClick(v, getLayoutPosition(),singleUnitDataList.get(getLayoutPosition()).getTaskTypeId());
        }
    }
}

