package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.SerializedName;

public class Areas {

    @SerializedName("Id")
    public Integer Id;

    @SerializedName("Name")
    public String Name;

    @SerializedName("Description")
    public String Description;

    @SerializedName("BranchId")
    public Integer BranchId;

    @SerializedName("AreaCode")
    public String AreaCode;


    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }



    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        BranchId = branchId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }



    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


}