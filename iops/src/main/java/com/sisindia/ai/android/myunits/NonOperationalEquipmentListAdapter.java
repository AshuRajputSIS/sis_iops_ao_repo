package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.EquipmentList;
import com.sisindia.ai.android.myunits.models.EquipmentListModel;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saruchi on 10-05-2016.
 */
public class NonOperationalEquipmentListAdapter extends BaseAdapter implements View.OnClickListener {
    CheckBox name;
    Context context;
    UnitPost mUnitPostDataHolder;
    private ArrayList<EquipmentListModel> equipList;
    private List<EquipmentList> selectedEquipment;
    private LayoutInflater inflater = null;
    private LinearLayout mlinear_row_view;
    private TextView mTextView_no_data;

    public NonOperationalEquipmentListAdapter(ArrayList<EquipmentListModel> equipList, Context context, UnitPost mUnitPostDataHolder) {

        selectedEquipment = new ArrayList<>();
        this.equipList = equipList;
        this.context = context;

        this.mUnitPostDataHolder = mUnitPostDataHolder;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {

        return equipList.size();
    }


    public Object getItem(int position) {
        return position;
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)

            vi = inflater.inflate(R.layout.post_details_equipment_row_item, null);
        mlinear_row_view = (LinearLayout) vi.findViewById(R.id.linear_row_view);
        mTextView_no_data = (TextView) vi.findViewById(R.id.no_data_exists);
        name = (CheckBox) vi.findViewById(R.id.equipment_name); // name
        name.setTag(String.valueOf(position));
        if (equipList.size() > 0) {
            name.setText(Util.getEquipmentBasedOnId(equipList.get(position).getEquipmentType(), context));
        } else {
            name.setVisibility(View.GONE);
        }
        name.setOnClickListener(this);
        if (equipList.size() == 0) {
            mTextView_no_data.setVisibility(View.VISIBLE);
            mlinear_row_view.setVisibility(View.GONE);
        } else {
            mTextView_no_data.setVisibility(View.GONE);
            mlinear_row_view.setVisibility(View.VISIBLE);
        }

        return vi;
    }

    @Override
    public void onClick(View v) {
        int pos = Integer.parseInt(v.getTag().toString());
        EquipmentList model = new EquipmentList();
        model.setIsNotFunctional(false);
        if (name.isChecked()) {
            model.setIsNotFunctional(true);
        }
        model.setEquipmentId(equipList.get(pos).getUnitEquipmentId());
        model.setIsCusomterProvided(equipList.get(pos).getIsCustomerProvided());
        model.setEquipmentName(equipList.get(pos).getEquipmentName());
        model.setEquipmentName(Util.getEquipmentBasedOnId(equipList.get(pos).getEquipmentType(), context));
        selectedEquipment.add(model);
       // mUnitPostDataHolder.setEquipmentList(selectedEquipment);
    }
}
