package com.sisindia.ai.android.myunits.models;


import com.google.gson.annotations.SerializedName;

public class UnitHistoryMO {

@SerializedName("UnitName")

private Object unitName;
@SerializedName("TaskType")

private Object taskType;
@SerializedName("TaskTypeId")

private Integer taskTypeId;
@SerializedName("CurrentMonthAllTasksCount")

private Integer currentMonthAllTasksCount;
@SerializedName("CurrentMonthCompletedTasksCount")

private Integer currentMonthCompletedTasksCount;
@SerializedName("Grievances")

private Integer grievances;
@SerializedName("Compliants")

private Integer compliants;
@SerializedName("LastActivityDate")

private String lastActivityDate;




    @SerializedName("LastMonthCompletedTasksCount")
    private int lastMonthCompletedTasksCount;



/**
* 
* @return
* The unitName
*/
public Object getUnitName() {
return unitName;
}

/**
* 
* @param unitName
* The UnitName
*/
public void setUnitName(Object unitName) {
this.unitName = unitName;
}

/**
* 
* @return
* The taskType
*/
public Object getTaskType() {
return taskType;
}

/**
* 
* @param taskType
* The TaskType
*/
public void setTaskType(Object taskType) {
this.taskType = taskType;
}

/**
* 
* @return
* The taskTypeId
*/
public Integer getTaskTypeId() {
return taskTypeId;
}

/**
* 
* @param taskTypeId
* The TaskTypeId
*/
public void setTaskTypeId(Integer taskTypeId) {
this.taskTypeId = taskTypeId;
}

/**
* 
* @return
* The currentMonthAllTasksCount
*/
public Integer getCurrentMonthAllTasksCount() {
return currentMonthAllTasksCount;
}

/**
* 
* @param currentMonthAllTasksCount
* The CurrentMonthAllTasksCount
*/
public void setCurrentMonthAllTasksCount(Integer currentMonthAllTasksCount) {
this.currentMonthAllTasksCount = currentMonthAllTasksCount;
}

/**
* 
* @return
* The currentMonthCompletedTasksCount
*/
public Integer getCurrentMonthCompletedTasksCount() {
return currentMonthCompletedTasksCount;
}

/**
* 
* @param currentMonthCompletedTasksCount
* The CurrentMonthCompletedTasksCount
*/
public void setCurrentMonthCompletedTasksCount(Integer currentMonthCompletedTasksCount) {
this.currentMonthCompletedTasksCount = currentMonthCompletedTasksCount;
}

/**
* 
* @return
* The grievances
*/
public Integer getGrievances() {
return grievances;
}

/**
* 
* @param grievances
* The Grievances
*/
public void setGrievances(Integer grievances) {
this.grievances = grievances;
}

/**
* 
* @return
* The compliants
*/
public Integer getCompliants() {
return compliants;
}

/**
* 
* @param compliants
* The Compliants
*/
public void setCompliants(Integer compliants) {
this.compliants = compliants;
}

/**
* 
* @return
* The lastActivityDate
*/
public String getLastActivityDate() {
return lastActivityDate;
}

/**
* 
* @param lastActivityDate
* The LastActivityDate
*/
public void setLastActivityDate(String lastActivityDate) {
this.lastActivityDate = lastActivityDate;
}

    public int getLastMonthCompletedTasksCount() {
        return lastMonthCompletedTasksCount;
    }

    public void setLastMonthCompletedTasksCount(int lastMonthCompletedTasksCount) {
        this.lastMonthCompletedTasksCount = lastMonthCompletedTasksCount;
    }

}

