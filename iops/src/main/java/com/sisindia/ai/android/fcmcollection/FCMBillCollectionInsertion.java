package com.sisindia.ai.android.fcmcollection;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;

import java.util.List;

/**
 * Created by compass on 8/10/2017.
 */

public class FCMBillCollectionInsertion extends SISAITrackingDB {

    public FCMBillCollectionInsertion(Context mcontext) {
        super(mcontext);
    }

    public boolean insertBillCollectionApiTable(List<UnitMonthlyBillDetail> billCollactionApiMOList) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        try {
            IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTable(TableNameAndColumnStatement.UnitBillingStatusTable, sqlite);

            ContentValues values = new ContentValues();
            for (int i = 0; i < billCollactionApiMOList.size(); i++) {
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_ID, billCollactionApiMOList.get(i).getId());
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID, billCollactionApiMOList.get(i).getUnitId());
                values.put(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_NAME, billCollactionApiMOList.get(i).getUnitName());
                values.put(TableNameAndColumnStatement.Bill_NUMBER, billCollactionApiMOList.get(i).getBillNo());
                values.put(TableNameAndColumnStatement.Bill_MONTH, billCollactionApiMOList.get(i).getBillMonth());
                values.put(TableNameAndColumnStatement.Bill_YEAR, billCollactionApiMOList.get(i).getBillYear());
                values.put(TableNameAndColumnStatement.OUTSTANDING_AMOUNT, billCollactionApiMOList.get(i).getOutstandingAmount());
                values.put(TableNameAndColumnStatement.OUTSTANDING_DAYS, billCollactionApiMOList.get(i).getOutstandingDays());
                values.put(TableNameAndColumnStatement.UPDATED_DATES, billCollactionApiMOList.get(i).getUpdatedDate());
                values.put(TableNameAndColumnStatement.IS_BILL_COLLECTED, false);
                sqlite.insertOrThrow(TableNameAndColumnStatement.UnitBillingStatusTable, null, values);
            }
            sqlite.setTransactionSuccessful();
            return true;
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        } finally {
            sqlite.endTransaction();
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }
}
