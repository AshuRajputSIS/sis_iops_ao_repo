package com.sisindia.ai.android.rota.daycheck.taskExecution;


import com.google.gson.annotations.SerializedName;

public class SecurityRisk {

@SerializedName("QuestionId")

private Integer questionId;
@SerializedName("OptionId")

private Integer optionId;
@SerializedName("Remarks")

private String remarks;

/**
* 
* @return
* The questionId
*/
public Integer getQuestionId() {
return questionId;
}

/**
* 
* @param questionId
* The QuestionId
*/
public void setQuestionId(Integer questionId) {
this.questionId = questionId;
}

/**
* 
* @return
* The optionId
*/
public Integer getOptionId() {
return optionId;
}

/**
* 
* @param optionId
* The OptionId
*/
public void setOptionId(Integer optionId) {
this.optionId = optionId;
}

/**
* 
* @return
* The remarks
*/
public String getRemarks() {
return remarks;
}

/**
* 
* @param remarks
* The Remarks
*/
public void setRemarks(String remarks) {
this.remarks = remarks;
}

}