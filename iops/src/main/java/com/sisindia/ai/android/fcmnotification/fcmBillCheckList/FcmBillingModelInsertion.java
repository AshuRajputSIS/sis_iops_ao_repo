package com.sisindia.ai.android.fcmnotification.fcmBillCheckList;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.UnitBillingCheckList;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Saruchi on 13-01-2017.
 */

public class FcmBillingModelInsertion extends SISAITrackingDB {
    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite=null;

    public FcmBillingModelInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertBillCheckListModel(BillingCheckListData billingCheckListData) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if(billingCheckListData != null) {
                    sqlite.beginTransaction();

                if(billingCheckListData.getUnitBillingCheckList()!=null && billingCheckListData.getUnitBillingCheckList().size()!=0){
                    for(UnitBillingCheckList unitBillinngModel :billingCheckListData.getUnitBillingCheckList()){
                        IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                deleteRecordById(unitBillinngModel.getUnitId(),
                                        TableNameAndColumnStatement.UNIT_ID ,
                                        TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE,sqlite);
                        break;
                    }

                    insertUnitBillingCheckList(billingCheckListData.getUnitBillingCheckList());
                }
                    else{
                    Timber.e("error : nulll");
                }

                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }
    public void insertUnitBillingCheckList(List<UnitBillingCheckList> unitBillingCheckList) {
            try {
                for (UnitBillingCheckList unitBilling : unitBillingCheckList) {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ID, unitBilling.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitBilling.getUnitId());
                    values.put(TableNameAndColumnStatement.BILL_CHECK_LIST_ID, unitBilling.getBillingCheckListId());
                    sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE, null, values);
                }
            }catch (Exception exception) {
                Crashlytics.logException(exception);
                exception.printStackTrace();

        }
    }

}
