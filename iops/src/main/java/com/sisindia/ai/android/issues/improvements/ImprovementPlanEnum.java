package com.sisindia.ai.android.issues.improvements;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum ImprovementPlanEnum {

    Client_Meeting_with_Seniors(1),
    Change_Uniform(2),
    Unit_Training(3),
    Manpower_Fullfillment(4),
    More_Checkings(5),
    Change_Manpower(6),
    Contract_Renewal(7),
    Barrack_Change(8),
    Barrack_Sanitation(9),
    Others(10);


    private static Map map = new HashMap<>();

    static {
        for (ImprovementPlanEnum postStatusEM : ImprovementPlanEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    ImprovementPlanEnum(int value) {
        this.value = value;
    }

    public static ImprovementPlanEnum valueOf(int postStatus) {
        return (ImprovementPlanEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
