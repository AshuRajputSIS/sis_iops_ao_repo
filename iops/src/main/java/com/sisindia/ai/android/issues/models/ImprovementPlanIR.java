package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImprovementPlanIR {




    @SerializedName("IssueMatrixId")
    @Expose
    private Integer issueMatrixId;
    @SerializedName("actionPlanId")
    @Expose
    private Integer actionPlanId;
    @SerializedName("MasterActionPlanId")
    @Expose
    private Integer masterActionPlanId;
    @SerializedName("IssueId")
    @Expose
    private Integer issueId;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("BarrackId")
    @Expose
    private Integer barrackId;
    @SerializedName("ActionPlanStatusId")
    @Expose
    private Integer actionPlanStatusId;
    @SerializedName("ImprovementPlanCategoryId")
    @Expose
    private Integer improvementPlanCategoryId;
    @SerializedName("RaisedDate")
    @Expose
    private String raisedDate;
    @SerializedName("ExpectedClosureDate")
    @Expose
    private String expectedClosureDate;
    @SerializedName("TargetEmployeeId")
    @Expose
    private Integer targetEmployeeId;
    @SerializedName("ClosureComment")
    @Expose
    private String closureComment;
    @SerializedName("AssignedToId")
    @Expose
    private Integer assignedToId;
    @SerializedName("KitRequirmentId")
    @Expose
    private Integer kitRequirmentId;

    @SerializedName("Remarks")
    @Expose
    private String remarks;

    @SerializedName("ClientCoordinationQuestionId")
    @Expose
    private Integer clientCoordinationQuestionId;

    @SerializedName("SourceTaskId")
    @Expose
    private Integer taskId;


    @SerializedName("Budget")
    @Expose
    private String budget;

    @SerializedName("ClosureDate")
    @Expose
    private String closureDate;

    public Integer getClientCoordinationQuestionId() {
        return clientCoordinationQuestionId;
    }

    public void setClientCoordinationQuestionId(Integer clientCoordinationQuestionId) {
        this.clientCoordinationQuestionId = clientCoordinationQuestionId;
    }

    public Integer getActionPlanId() {
        return actionPlanId;
    }

    public void setActionPlanId(Integer actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    /**
     * @return The masterActionPlanId
     */
    public Integer getMasterActionPlanId() {
        return masterActionPlanId;
    }

    /**
     * @param masterActionPlanId The MasterActionPlanId
     */
    public void setMasterActionPlanId(Integer masterActionPlanId) {
        this.masterActionPlanId = masterActionPlanId;
    }

    /**
     * @return The issueId
     */
    public Integer getIssueId() {
        return issueId;
    }

    /**
     * @param issueId The IssueId
     */
    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The barrackId
     */
    public Integer getBarrackId() {
        return barrackId;
    }

    /**
     * @param barrackId The BarrackId
     */
    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    /**
     * @return The actionPlanStatusId
     */
    public Integer getActionPlanStatusId() {
        return actionPlanStatusId;
    }

    /**
     * @param actionPlanStatusId The ActionPlanStatusId
     */
    public void setActionPlanStatusId(Integer actionPlanStatusId) {
        this.actionPlanStatusId = actionPlanStatusId;
    }

    /**
     * @return The improvementPlanCategoryId
     */
    public Integer getImprovementPlanCategoryId() {
        return improvementPlanCategoryId;
    }

    /**
     * @param improvementPlanCategoryId The ImprovementPlanCategoryId
     */
    public void setImprovementPlanCategoryId(Integer improvementPlanCategoryId) {
        this.improvementPlanCategoryId = improvementPlanCategoryId;
    }

    /**
     * @return The raisedDate
     */
    public String getRaisedDate() {
        return raisedDate;
    }

    /**
     * @param raisedDate The RaisedDate
     */
    public void setRaisedDate(String raisedDate) {
        this.raisedDate = raisedDate;
    }

    /**
     * @return The expectedClosureDate
     */
    public String getExpectedClosureDate() {
        return expectedClosureDate;
    }

    /**
     * @param expectedClosureDate The ExpectedClosureDate
     */
    public void setExpectedClosureDate(String expectedClosureDate) {
        this.expectedClosureDate = expectedClosureDate;
    }

    /**
     * @return The targetEmployeeId
     */
    public Integer getTargetEmployeeId() {
        return targetEmployeeId;
    }

    /**
     * @param targetEmployeeId The TargetEmployeeId
     */
    public void setTargetEmployeeId(Integer targetEmployeeId) {
        this.targetEmployeeId = targetEmployeeId;
    }

    /**
     * @return The closureComment
     */
    public String getClosureComment() {
        return closureComment;
    }

    /**
     * @param closureComment The ClosureComment
     */
    public void setClosureComment(String closureComment) {
        this.closureComment = closureComment;
    }

    /**
     * @return The assignedToId
     */
    public Integer getAssignedToId() {
        return assignedToId;
    }

    /**
     * @param assignedToId The AssignedToId
     */
    public void setAssignedToId(Integer assignedToId) {
        this.assignedToId = assignedToId;
    }

    /**
     * @return The kitRequirmentId
     */
    public Integer getKitRequirmentId() {
        return kitRequirmentId;
    }

    /**
     * @param kitRequirmentId The KitRequirmentId
     */
    public void setKitRequirmentId(Integer kitRequirmentId) {
        this.kitRequirmentId = kitRequirmentId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    public Integer getIssueMatrixId() {
        return issueMatrixId;
    }

    public void setIssueMatrixId(Integer issueMatrixId) {
        this.issueMatrixId = issueMatrixId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }
    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getClosureDate() {
        return closureDate;
    }

    public void setClosureDate(String closureDate) {
        this.closureDate = closureDate;
    }

    @Override
    public String toString() {
        return "ImprovementPlanIR{" +
                "issueMatrixId=" + issueMatrixId +
                ", actionPlanId=" + actionPlanId +
                ", masterActionPlanId=" + masterActionPlanId +
                ", issueId=" + issueId +
                ", unitId=" + unitId +
                ", barrackId=" + barrackId +
                ", actionPlanStatusId=" + actionPlanStatusId +
                ", improvementPlanCategoryId=" + improvementPlanCategoryId +
                ", raisedDate='" + raisedDate + '\'' +
                ", expectedClosureDate='" + expectedClosureDate + '\'' +
                ", targetEmployeeId=" + targetEmployeeId +
                ", closureComment='" + closureComment + '\'' +
                ", assignedToId=" + assignedToId +
                ", kitRequirmentId=" + kitRequirmentId +
                ", remarks='" + remarks + '\'' +
                ", clientCoordinationQuestionId=" + clientCoordinationQuestionId +
                ", taskId=" + taskId +
                ", budget='" + budget + '\'' +
                ", closureDate='" + closureDate + '\'' +
                '}';
    }
}