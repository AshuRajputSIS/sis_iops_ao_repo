package com.sisindia.ai.android.commons;

/**
 * Created by Saruchi on 30-06-2016.
 */

public class FileUploadMO {
    private String serverFilePath;

    public int getAttachmentID() {
        return attachmentID;
    }

    public void setAttachmentID(int attachmentID) {
        this.attachmentID = attachmentID;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private int attachmentID;
    private String filePath;

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    private int isSave;

    @Override
    public String toString() {
        return "FileUploadMO{" +
                "attachmentID=" + attachmentID +
                ", filePath='" + filePath + '\'' +
                ", isSave=" + isSave +
                '}';
    }


    public void setServerFilePath(String serverFilePath) {
        this.serverFilePath = serverFilePath;
    }

    public String getServerFilePath() {
        return serverFilePath;
    }
}
