package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.SerializedName;

public class KitItemSizeOR {

@SerializedName("Id")

private Integer id;
@SerializedName("KitItemId")

private Integer kitItemId;
@SerializedName("KitApparelSizeId")

private Integer kitApparelSizeId;
@SerializedName("IsActive")

private Boolean isActive;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The kitItemId
*/
public Integer getKitItemId() {
return kitItemId;
}

/**
* 
* @param kitItemId
* The KitItemId
*/
public void setKitItemId(Integer kitItemId) {
this.kitItemId = kitItemId;
}

/**
* 
* @return
* The kitApparelSizeId
*/
public Integer getKitApparelSizeId() {
return kitApparelSizeId;
}

/**
* 
* @param kitApparelSizeId
* The KitApparelSizeId
*/
public void setKitApparelSizeId(Integer kitApparelSizeId) {
this.kitApparelSizeId = kitApparelSizeId;
}

/**
* 
* @return
* The isActive
*/
public Boolean getIsActive() {
return isActive;
}

/**
* 
* @param isActive
* The IsActive
*/
public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

}