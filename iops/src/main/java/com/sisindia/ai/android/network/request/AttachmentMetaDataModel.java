package com.sisindia.ai.android.network.request;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Durga Prasad on 19-04-2016.
 */
public class AttachmentMetaDataModel implements Parcelable {




    @SerializedName("AttachmentTitle")
    private String attachmentTitle;
    @SerializedName("AttachmentFileName")
    private String attachmentFileName;
    @SerializedName("SizeInKB")
    private int attachmentFileSize;
    @SerializedName("AttachmentExtension")
    private String attachmentFileExtension;
    @SerializedName("AttachmentSourceTypeId")
    private int AttachmentSourceTypeId;
    @SerializedName("AttachmentSourceId")
    private int AttachmentSourceId;
    @SerializedName("SequenceNo")
    private int sequenceNo;
    @SerializedName("AttachmentPath")
    private String attachmentPath;
    @SerializedName("TaskId")
    private int taskId;
    private int attachmentId;
    @SerializedName("UnitId")
    private int unitId;
    @SerializedName("AttachmentSourceCode")
    private String attachmentSourceCode;
    private int isSave;
    private int AttachmentTypeId;
    private String imageCode;
    private String screenName;




    private int guradId; // used only while deleting the guard record

    public int getGuradId() {
        return guradId;
    }

    public void setGuradId(int guradId) {
        this.guradId = guradId;
    }

    public String getAttachmentSourceCode() {
        return attachmentSourceCode;
    }

    public void setAttachmentSourceCode(String attachmentSourceCode) {
        this.attachmentSourceCode = attachmentSourceCode;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

   public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public String getAttachmentTitle() {
        return attachmentTitle;
    }

    public void setAttachmentTitle(String attachmentTitle) {
        this.attachmentTitle = attachmentTitle;
    }

    public String getAttachmentFileExtension() {
        return attachmentFileExtension;
    }

    public void setAttachmentFileExtension(String attachmentFileExtension) {
        this.attachmentFileExtension = attachmentFileExtension;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public int getAttachmentFileSize() {
        return attachmentFileSize;
    }

    public void setAttachmentFileSize(int attachmentFileSize) {
        this.attachmentFileSize = attachmentFileSize;
    }

    public int getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(int sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public int getAttachmentSourceTypeId() {
        return AttachmentSourceTypeId;
    }

    public void setAttachmentSourceTypeId(int attachmentSourceTypeId) {
        AttachmentSourceTypeId = attachmentSourceTypeId;
    }

    public int getAttachmentSourceId() {
        return AttachmentSourceId;
    }

    public void setAttachmentSourceId(int attachmentSourceId) {
        AttachmentSourceId = attachmentSourceId;
    }

    public int getAttachmentTypeId() {
        return AttachmentTypeId;
    }

    public void setAttachmentTypeId(int attachmentTypeId) {
        AttachmentTypeId = attachmentTypeId;
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    @Override
    public String toString() {
        return "AttachmentMetaDataModel{" +
                "attachmentTitle='" + attachmentTitle + '\'' +
                ", attachmentFileName='" + attachmentFileName + '\'' +
                ", attachmentFileSize=" + attachmentFileSize +
                ", attachmentFileExtension='" + attachmentFileExtension + '\'' +
                ", AttachmentSourceTypeId=" + AttachmentSourceTypeId +
                ", AttachmentSourceId=" + AttachmentSourceId +
                ", sequenceNo=" + sequenceNo +
                ", attachmentPath='" + attachmentPath + '\'' +
                ", taskId=" + taskId +
                ", attachmentId=" + attachmentId +
                ", unitId=" + unitId +
                ", attachmentSourceCode='" + attachmentSourceCode + '\'' +
                ", isSave=" + isSave +
                ", AttachmentTypeId=" + AttachmentTypeId +
                ", imageCode='" + imageCode + '\'' +
                ", screenName='" + screenName + '\'' +
                '}';
    }

    public String getimageCode() {
        return imageCode;
    }

    public void setimageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.attachmentTitle);
        dest.writeString(this.attachmentFileName);
        dest.writeInt(this.attachmentFileSize);
        dest.writeString(this.attachmentFileExtension);
        dest.writeInt(this.AttachmentSourceTypeId);
        dest.writeInt(this.AttachmentSourceId);
        dest.writeInt(this.sequenceNo);
        dest.writeString(this.attachmentPath);
        dest.writeInt(this.taskId);
        dest.writeInt(this.attachmentId);
        dest.writeInt(this.unitId);
        dest.writeString(this.attachmentSourceCode);
        dest.writeInt(this.isSave);
        dest.writeInt(this.AttachmentTypeId);
        dest.writeString(this.imageCode);
        dest.writeString(this.screenName);
        dest.writeInt(this.guradId);
    }

    public AttachmentMetaDataModel() {
    }

    protected AttachmentMetaDataModel(Parcel in) {
        this.attachmentTitle = in.readString();
        this.attachmentFileName = in.readString();
        this.attachmentFileSize = in.readInt();
        this.attachmentFileExtension = in.readString();
        this.AttachmentSourceTypeId = in.readInt();
        this.AttachmentSourceId = in.readInt();
        this.sequenceNo = in.readInt();
        this.attachmentPath = in.readString();
        this.taskId = in.readInt();
        this.attachmentId = in.readInt();
        this.unitId = in.readInt();
        this.attachmentSourceCode = in.readString();
        this.isSave = in.readInt();
        this.AttachmentTypeId = in.readInt();
        this.imageCode = in.readString();
        this.screenName = in.readString();
        this.guradId = in.readInt();
    }

    public static final Parcelable.Creator<AttachmentMetaDataModel> CREATOR = new Parcelable.Creator<AttachmentMetaDataModel>() {
        @Override
        public AttachmentMetaDataModel createFromParcel(Parcel source) {
            return new AttachmentMetaDataModel(source);
        }

        @Override
        public AttachmentMetaDataModel[] newArray(int size) {
            return new AttachmentMetaDataModel[size];
        }
    };
}
