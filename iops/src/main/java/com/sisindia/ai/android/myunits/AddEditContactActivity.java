package com.sisindia.ai.android.myunits;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.utils.bottomsheet.CustomBottomSheetFragment;
import com.sisindia.ai.android.utils.bottomsheet.OnBottomSheetItemClickListener;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AddEditContactActivity extends AppCompatActivity implements ShowBottomSheetListener, OnBottomSheetItemClickListener,UpdatePostTabListener {

    @Bind(R.id.addpost_coordinator_layout)
    CoordinatorLayout addpostCoordinatorLayout;
    @Bind(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.addOrEditContactsContainer)
    FrameLayout addOrEditContactsContainer;
    AddEditClientContactFragment addEditClientContactFragment;
    private FragmentManager fragmentManager;
    private ActionBar actionBar;
    private String toolbarTitle;
    private int unitId;
    private Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_contact);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        resources= getResources();
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Intent addEditIntent = getIntent();
        if (addEditIntent != null) {
            toolbarTitle = addEditIntent.getStringExtra("toolbarTitle");
            unitId = addEditIntent.getIntExtra("unitId", 0);
        }
        addEditClientContactFragment = new AddEditClientContactFragment();

        if(toolbarTitle.equalsIgnoreCase(resources.getString(R.string.edit_contact_str))){
            if(null!=addEditIntent.getParcelableExtra("unitModel")){
              UnitContactsMO  unitContactsMO =addEditIntent.getParcelableExtra("unitModel");
                addEditClientContactFragment.setUnitContactMO(unitContactsMO,toolbarTitle);
            }
        }
        else {
            addEditClientContactFragment.setUnitContactMO(null,toolbarTitle);
        }

        addEditClientContactFragment.setUnitId(unitId);
        addEditClientContactFragment.setListner(this);
        replaceFragment(addEditClientContactFragment, toolbarTitle);
    }

    private void replaceFragment(Fragment fragment, String tagName) {
        fragmentManager = getSupportFragmentManager();
        getSupportActionBar().setTitle(tagName);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.addOrEditContactsContainer, fragment, tagName);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragmentManager != null) {
            if (fragmentManager.getBackStackEntryCount() > 0)
                fragmentManager.popBackStack();
            else
                finish();
        }
    }

    @Override
    public void showBottomSheet(ArrayList<String> elements, int viewId) {
        setBottomSheetFragment(elements, viewId);
    }

    @Override
    public void showUnitModelBottomSheet(List<MyUnitHomeMO> myUnitHomeMOs, int viewId) {

    }

    @Override
    public void onSheetItemClick(String itemSelected, int viewId) {
        addEditClientContactFragment = (AddEditClientContactFragment) fragmentManager.findFragmentByTag(toolbarTitle);
        addEditClientContactFragment.setValue(itemSelected, viewId);
    }

    @Override
    public void onUnitSheetItemClick(MyUnitHomeMO unitHomeMOList, int viewId) {

    }


    public void setBottomSheetFragment(ArrayList<String> elements, int viewId) {

        CustomBottomSheetFragment bottomSheetDialogFragment = new CustomBottomSheetFragment();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        bottomSheetDialogFragment.setSheetItemClickListener(this);
        bottomSheetDialogFragment.setBottomSheetData(elements, viewId);
    }

    @Override
    public void updatePostTab() {
        setResult(RESULT_OK,new Intent());
        finish();
    }
}
