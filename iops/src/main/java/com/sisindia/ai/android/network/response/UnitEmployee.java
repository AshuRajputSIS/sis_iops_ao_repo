package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UnitEmployee {

    @SerializedName("Id")
    public Integer Id;

    @SerializedName("UnitId")
    public Integer UnitId;

    @SerializedName("EmployeeId")
    public Integer EmployeeId;

    @SerializedName("IsActive")
    public Boolean IsActive;

    @SerializedName("DeploymentDate")
    public String DeploymentDate;

    @SerializedName("WeeklyOffDay")
    public Integer WeeklyOffDay;

    @SerializedName("EmployeeNo")
    public String EmployeeNo;

    @SerializedName("EmployeeFullName")
    public String EmployeeFullName;

    @SerializedName("EmployeContactNo")
    public String EmployeContactNo;

    @SerializedName("EmployeeRank")
    public List<com.sisindia.ai.android.network.response.EmployeeRank> EmployeeRank = new ArrayList<>();

    public String lastFine;

    public String fineAmount;

    public String rewardAmount;

    public Integer fineInstance;

    @SerializedName("UnitName")
    public String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getUnitId() {
        return UnitId;
    }

    public void setUnitId(Integer unitId) {
        UnitId = unitId;
    }

    public Integer getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        EmployeeId = employeeId;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean isActive) {
        IsActive = isActive;
    }

    public String getDeploymentDate() {
        return DeploymentDate;
    }

    public void setDeploymentDate(String deploymentDate) {
        DeploymentDate = deploymentDate;
    }

    public Integer getWeeklyOffDay() {
        return WeeklyOffDay;
    }

    public void setWeeklyOffDay(Integer weeklyOffDay) {
        WeeklyOffDay = weeklyOffDay;
    }

    public String getEmployeeNo() {
        return EmployeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        EmployeeNo = employeeNo;
    }

    public String getEmployeeFullName() {
        return EmployeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        EmployeeFullName = employeeFullName;
    }

    public String getEmployeContactNo() {
        return EmployeContactNo;
    }

    public void setEmployeContactNo(String employeContactNo) {
        EmployeContactNo = employeContactNo;
    }

    public List<com.sisindia.ai.android.network.response.EmployeeRank> getEmployeeRank() {
        return EmployeeRank;
    }

    public void setEmployeeRank(List<com.sisindia.ai.android.network.response.EmployeeRank> employeeRank) {
        EmployeeRank = employeeRank;
    }

    public String getLastFine() {
        return lastFine;
    }

    public void setLastFine(String lastFine) {
        this.lastFine = lastFine;
    }

    public String getFineAmount() {
        return fineAmount;
    }

    public void setFineAmount(String fineAmount) {
        this.fineAmount = fineAmount;
    }

    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public Integer getFineInstance() {
        return fineInstance;
    }

    public void setFineInstance(Integer fineInstance) {
        this.fineInstance = fineInstance;
    }
}