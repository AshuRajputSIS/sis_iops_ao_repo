package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.myunits.models.EmployeeRank;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.myunits.models.UnitBarrack;
import com.sisindia.ai.android.myunits.models.UnitContacts;
import com.sisindia.ai.android.myunits.models.UnitCustomers;
import com.sisindia.ai.android.myunits.models.UnitEmployees;
import com.sisindia.ai.android.myunits.models.UnitEquipments;
import com.sisindia.ai.android.myunits.models.UnitMessVendors;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.myunits.models.UnitPostGeoPoint;
import com.sisindia.ai.android.myunits.models.UnitShiftRank;

import timber.log.Timber;

/**
 * Created by Saruchi on 08-06-2016.
 */
public class UnitSychronizationDB extends SISAITrackingDB {


    public UnitSychronizationDB(Context context) {
        super(context);
    }

    /**
     * @return
     */
    public int getUnitTablesId(String tableName, int Id, String columnanme) {
        int id = 0;
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + columnanme + " from " +
                tableName +
                " where " + columnanme + " =" + Id;
        Timber.d("getUnitTablesId updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        id = cursor.getInt(cursor.getColumnIndex(columnanme));

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            cursor.close();
            sqlite.close();
        }
        return id;
    }


    public void insertInUnitTable(Unit unit) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unit != null) {
            synchronized (sqlite) {
                try {

                    Timber.d( "insertInUnitTable %s", unit);

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unit.getId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unit.getName());
                    values.put(TableNameAndColumnStatement.UNIT_CODE, unit.getUnitCode());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unit.getDescription());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, unit.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.AREA_ID, unit.getAreaId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unit.getCustomerId());
                    values.put(TableNameAndColumnStatement.DAY_CHECK_FREQUENCY, unit.getDayCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.NIGHT_CHECK_FREQUENCY, unit.getNightCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.CLIENT_COORDINATION_FREQUENCY, unit.getClientCoordinationFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.BILLING_DATE, unit.getBillDate());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_DATE, unit.getBillCollectionDate());
                    values.put(TableNameAndColumnStatement.WAGE_DATE, unit.getWageDate());
                    values.put(TableNameAndColumnStatement.BILLING_MODE, unit.getBillingMode());
                    values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, unit.getMessVendorId());
                    values.put(TableNameAndColumnStatement.IS_BARRACK_PROVIDED, unit.getIsBarrackProvided());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_ID, unit.getUnitCommanderId());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_ID, unit.getAreaInspectorId());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_ID, unit.getBillSubmissionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_ID, unit.getBillCollectionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID, unit.getBillAuthoritativeUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE, unit.getUnitType());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_NAME, unit.getUnitCommanderName());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, unit.getAreaInspectorName());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_NAME, unit.getBillSubmissionResponsibleName());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_NAME, unit.getBillCollectionResponsibleName());
                    values.put(TableNameAndColumnStatement.UNIT_FULL_ADDRESS, unit.getUnitFullAddress());
                    values.put(TableNameAndColumnStatement.UNIT_STATUS, unit.getUnitStatus());
                    values.put(TableNameAndColumnStatement.COLLECTION_STATUS, unit.getCollectionStatus());
                    values.put(TableNameAndColumnStatement.BILLING_TYPE_ID, unit.getBillingTypeId());
                    values.put(TableNameAndColumnStatement.BILL_GENERATION_DATE, unit.getBillGenerationDate());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unit.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unit.getUnarmedStrength());
                    String unitBoundary="";
                    if(null!=unit.getUnitBoundary() && unit.getUnitBoundary().size()!=0){
                        unitBoundary = IOPSApplication.getGsonInstance().toJson(unit.getUnitBoundary());
                    }

                    values.put(TableNameAndColumnStatement.UNIT_BOUNDARY, unitBoundary);
                    sqlite.insert(TableNameAndColumnStatement.UNIT_TABLE, null, values);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
               /* values.put(TableNameAndColumnStatement.UNIT_BILLING,unit.getBillDate());
                values.put(TableNameAndColumnStatement.UNIT_COLLECTION, unit.getBillCollectionDate());
                values.put(TableNameAndColumnStatement.UNIT_WAGES,unit.getWageDate());*/


            }
        }


    }


    public void updateInUnitTable(Unit unit, int id) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unit != null) {
            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unit.getId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unit.getName());
                    values.put(TableNameAndColumnStatement.UNIT_CODE, unit.getUnitCode());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unit.getDescription());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, unit.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.AREA_ID, unit.getAreaId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unit.getCustomerId());
                    values.put(TableNameAndColumnStatement.DAY_CHECK_FREQUENCY, unit.getDayCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.NIGHT_CHECK_FREQUENCY, unit.getNightCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.CLIENT_COORDINATION_FREQUENCY, unit.getClientCoordinationFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.BILLING_DATE, unit.getBillDate());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_DATE, unit.getBillCollectionDate());
                    values.put(TableNameAndColumnStatement.WAGE_DATE, unit.getWageDate());
                    values.put(TableNameAndColumnStatement.BILLING_MODE, unit.getBillingMode());
                    values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, unit.getMessVendorId());
                    values.put(TableNameAndColumnStatement.IS_BARRACK_PROVIDED, unit.getIsBarrackProvided());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_ID, unit.getUnitCommanderId());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_ID, unit.getAreaInspectorId());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_ID, unit.getBillSubmissionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_ID, unit.getBillCollectionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID, unit.getBillAuthoritativeUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE, unit.getUnitType());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_NAME, unit.getUnitCommanderName());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, unit.getAreaInspectorName());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_NAME, unit.getBillSubmissionResponsibleName());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_NAME, unit.getBillCollectionResponsibleName());
                    values.put(TableNameAndColumnStatement.UNIT_FULL_ADDRESS, unit.getUnitFullAddress());
                    values.put(TableNameAndColumnStatement.UNIT_STATUS, unit.getUnitStatus());
                    values.put(TableNameAndColumnStatement.COLLECTION_STATUS, unit.getCollectionStatus());
                    values.put(TableNameAndColumnStatement.BILLING_TYPE_ID, unit.getBillingTypeId());
                    values.put(TableNameAndColumnStatement.BILL_GENERATION_DATE, unit.getBillGenerationDate());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unit.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unit.getUnarmedStrength());
                    String unitBoundary="";
                    if(null!=unit.getUnitBoundary() && unit.getUnitBoundary().size()!=0){
                        unitBoundary = IOPSApplication.getGsonInstance().toJson(unit.getUnitBoundary());
                    }

                    values.put(TableNameAndColumnStatement.UNIT_BOUNDARY, unitBoundary);
                    sqlite.update(TableNameAndColumnStatement.UNIT_TABLE, values, TableNameAndColumnStatement.UNIT_ID + " = " + id, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
               /* values.put(TableNameAndColumnStatement.UNIT_BILLING,unit.getBillDate());
                values.put(TableNameAndColumnStatement.UNIT_COLLECTION, unit.getBillCollectionDate());
                values.put(TableNameAndColumnStatement.UNIT_WAGES,unit.getWageDate());*/


            }
        }


    }


    public void insertIntoUnitBarrack(UnitBarrack unitBarrack) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitBarrack != null) {

            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitBarrack.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitBarrack.getBarrackId());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, unitBarrack.getActive());
                    sqlite.insert(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, null, values);


                }catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }
        }


    }

    public void updateIntoUnitBarrack(UnitBarrack unitBarrack, int barrckid) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitBarrack != null) {

            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitBarrack.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitBarrack.getBarrackId());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE, unitBarrack.getActive());
                    sqlite.update(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, values, TableNameAndColumnStatement.BARRACK_ID + " = " + barrckid, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }
        }


    }


    public void insertIntoUnitEmployees(UnitEmployees unitEmployee) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitEmployee != null) {
            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitEmployee.getUnitId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID,
                            unitEmployee.getEmployeeId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_NO,
                            unitEmployee.getEmployeeNo());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME,
                            unitEmployee.getEmployeeFullName());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_CONTACT_NO,
                            unitEmployee.getEmployeContactNo());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE,
                            unitEmployee.isActive());

                    values.put(TableNameAndColumnStatement.DEPLOYMENT_DATE,
                            unitEmployee.getDeploymentDate());
                    values.put(TableNameAndColumnStatement.WEEKLY_OFF_DAY,
                            unitEmployee.getWeeklyOffDay());
                    if(null!=unitEmployee.getEmployeeRank()&&
                            unitEmployee.getEmployeeRank().size()!=0){
                        for (EmployeeRank employeeRank : unitEmployee.getEmployeeRank()) {
                            values.put(TableNameAndColumnStatement.EMPLOYEE_RANK_ID,
                                    employeeRank.getRankId());
                            values.put(TableNameAndColumnStatement.BRANCH_ID,
                                    employeeRank.getBranchId());
                        }
                    }
                    sqlite.insert(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, null, values);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }

            }
        }


    }

    public void updatetIntoUnitEmployees(UnitEmployees unitEmployee, int employeeid) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitEmployee != null) {
            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitEmployee.getUnitId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID,
                            unitEmployee.getEmployeeId());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_NO,
                            unitEmployee.getEmployeeNo());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME,
                            unitEmployee.getEmployeeFullName());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_CONTACT_NO,
                            unitEmployee.getEmployeContactNo());
                    values.put(TableNameAndColumnStatement.IS_ACTIVE,
                            unitEmployee.isActive());

                    values.put(TableNameAndColumnStatement.DEPLOYMENT_DATE,
                            unitEmployee.getDeploymentDate());
                    values.put(TableNameAndColumnStatement.WEEKLY_OFF_DAY,
                            unitEmployee.getWeeklyOffDay());
                    if(null!=unitEmployee.getEmployeeRank()&&
                            unitEmployee.getEmployeeRank().size()!=0){
                        for (EmployeeRank employeeRank : unitEmployee.getEmployeeRank()) {
                            values.put(TableNameAndColumnStatement.EMPLOYEE_RANK_ID,
                                    employeeRank.getRankId());
                            values.put(TableNameAndColumnStatement.BRANCH_ID,
                                    employeeRank.getBranchId());
                        }
                    }
                    sqlite.update(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, values, TableNameAndColumnStatement.EMPLOYEE_ID + " = " + employeeid, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }

            }
        }


    }

    public void insertIntoUnitPost(UnitPost unitPost) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitPost != null) {
            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitPost.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitPost.getId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_NAME, unitPost.getUnitPostName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitPost.getDescription());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unitPost.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unitPost.getUnarmedStrength());
                    values.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, unitPost.getMainGateDistance());
                    values.put(TableNameAndColumnStatement.BARRACK_DISTANCE, unitPost.getBarrackDistance());
                    values.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, unitPost.getUnitOfficeDistance());
                    values.put(TableNameAndColumnStatement.IS_MAIN_GATE, unitPost.getIsMainGate());
                    values.put(TableNameAndColumnStatement.DEVICE_NO, unitPost.deviceNo);
                    values.put(TableNameAndColumnStatement.IS_NEW, unitPost.isNew());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, unitPost.isSynced());

                    if (unitPost.getUnitPostGeoPoint() != null && unitPost.getUnitPostGeoPoint().size() != 0) {
                        for (UnitPostGeoPoint unitPostGeoPint : unitPost.getUnitPostGeoPoint()) {
                            values.put(TableNameAndColumnStatement.GEO_LATITUDE, unitPostGeoPint.getLatitude());
                            values.put(TableNameAndColumnStatement.GEO_LONGITUDE, unitPostGeoPint.getLongitude());
                        }
                    }
                    values.put(TableNameAndColumnStatement.POST_STATUS, unitPost.getIsActive());

                    sqlite.insert(TableNameAndColumnStatement.UNIT_POST_TABLE, null, values);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }
    }

    public void updateIntoUnitPost(UnitPost unitPost, int postid) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitPost != null) {
            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitPost.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitPost.getId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_NAME, unitPost.getUnitPostName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitPost.getDescription());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unitPost.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unitPost.getUnarmedStrength());
                    values.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, unitPost.getMainGateDistance());
                    values.put(TableNameAndColumnStatement.BARRACK_DISTANCE, unitPost.getBarrackDistance());
                    values.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, unitPost.getUnitOfficeDistance());
                    values.put(TableNameAndColumnStatement.IS_MAIN_GATE, unitPost.getIsMainGate());
                    values.put(TableNameAndColumnStatement.DEVICE_NO, unitPost.deviceNo);
                    values.put(TableNameAndColumnStatement.IS_NEW, unitPost.isNew());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, unitPost.isSynced());

                    if (unitPost.getUnitPostGeoPoint() != null && unitPost.getUnitPostGeoPoint().size() != 0) {
                        for (UnitPostGeoPoint unitPostGeoPint : unitPost.getUnitPostGeoPoint()) {
                            values.put(TableNameAndColumnStatement.GEO_LATITUDE, unitPostGeoPint.getLatitude());
                            values.put(TableNameAndColumnStatement.GEO_LONGITUDE, unitPostGeoPint.getLongitude());
                        }
                    }
                    values.put(TableNameAndColumnStatement.POST_STATUS, unitPost.getIsActive());
                    sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE, values, TableNameAndColumnStatement.UNIT_POST_ID + " = " + postid, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }
    }


    public void insertIntoUnitContacts(UnitContacts unitContact) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitContact != null) {

            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.IS_NEW, 0);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                            unitContact.getId());
                    values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
                    values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
                    values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
                    values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitContact.getUnitId());
                    values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
                    sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }
        }


    }

    public void updateIntoUnitContacts(UnitContacts unitContact, int unitContactId) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitContact != null) {

            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.IS_NEW, 0);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                            unitContact.getId());
                    values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
                    values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
                    values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
                    values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitContact.getUnitId());
                    values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
                    sqlite.update(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, values, TableNameAndColumnStatement.UNIT_CONTACT_ID + " = " + unitContactId, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }
        }
    }


    /**
     * @param unitMessVendor
     */
    public void updateIntoUnitMessVendor(UnitMessVendors unitMessVendor, int vendorId) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitMessVendor != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.VENDOR_ID, unitMessVendor.getId());
                    values.put(TableNameAndColumnStatement.VENDOR_NAME, unitMessVendor.getVendorName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitMessVendor.getDescription());
                    values.put(TableNameAndColumnStatement.VENDOR_CONTACT_NAME, unitMessVendor.getVendorContactName());
                    values.put(TableNameAndColumnStatement.VENDOR_FULL_ADDRESS, unitMessVendor.getVendorAddress());
                    sqlite.update(TableNameAndColumnStatement.MESS_VENDOR_TABLE, values, TableNameAndColumnStatement.VENDOR_ID + " = " + vendorId, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }

            }
        }

    }

    /**
     * @param unitCustomer
     */
    public void insertIntoCustomer(UnitCustomers unitCustomer) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitCustomer != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unitCustomer.getId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_NAME, unitCustomer.getCustomerName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitCustomer.getCustomerDescription());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_NAME, unitCustomer.getCustomerPrimaryContactName());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_EMAIL, unitCustomer.getPrimaryContactEmail());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_PHONE, unitCustomer.getPrimaryContactPhone());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_NAME, unitCustomer.getCustomerSecondaryContactName());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_EMAIL, unitCustomer.getSecondaryContactEmail());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_PHONE, unitCustomer.getSecondaryContactPhone());
                    values.put(TableNameAndColumnStatement.CAN_SEND_EMAIL, unitCustomer.isEmailToBeSent());
                    values.put(TableNameAndColumnStatement.CAN_SEND_SMS, unitCustomer.isSmsToBeSent());
                    values.put(TableNameAndColumnStatement.CUSTOMER_FULL_ADDRESS, unitCustomer.getCustomerHoAddress());

                    sqlite.insert(TableNameAndColumnStatement.CUSTOMER_TABLE, null, values);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }
    }

    /**
     * @param unitCustomer
     */
    public void updateIntoCustomer(UnitCustomers unitCustomer, int customerid) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitCustomer != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unitCustomer.getId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_NAME, unitCustomer.getCustomerName());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitCustomer.getCustomerDescription());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_NAME, unitCustomer.getCustomerPrimaryContactName());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_EMAIL, unitCustomer.getPrimaryContactEmail());
                    values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_PHONE, unitCustomer.getPrimaryContactPhone());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_NAME, unitCustomer.getCustomerSecondaryContactName());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_EMAIL, unitCustomer.getSecondaryContactEmail());
                    values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_PHONE, unitCustomer.getSecondaryContactPhone());
                    values.put(TableNameAndColumnStatement.CAN_SEND_EMAIL, unitCustomer.isEmailToBeSent());
                    values.put(TableNameAndColumnStatement.CAN_SEND_SMS, unitCustomer.isSmsToBeSent());
                    values.put(TableNameAndColumnStatement.CUSTOMER_FULL_ADDRESS, unitCustomer.getCustomerHoAddress());

                    sqlite.update(TableNameAndColumnStatement.CUSTOMER_TABLE, values, TableNameAndColumnStatement.CUSTOMER_ID + " = " + customerid, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }
    }


    /**
     * @param unitEquipment
     * @param postId
     */
    public void insertIntoUnitEquipments(UnitEquipments unitEquipment, int postId) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        if (unitEquipment != null) {
            synchronized (sqlite) {


                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID,
                            unitEquipment.getId());
                    values.put(TableNameAndColumnStatement.UNIQUE_NO,
                            unitEquipment.getEquipmentUniqueNo());
                    values.put(TableNameAndColumnStatement.MANUFACTURER,
                            unitEquipment.getEquipmentManufacturer());
                    values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED,
                            unitEquipment.isCustomerProvided());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS,
                            unitEquipment.getStatus());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE,
                            unitEquipment.getEquipmentId());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitEquipment.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID,
                            unitEquipment.getPostId());
                    sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }


    }

    /**
     * @param unitEquipment
     * @param postId
     */
    public void updateIntoUnitEquipments(UnitEquipments unitEquipment, int postId, int equuipmentId) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        if (unitEquipment != null) {
            synchronized (sqlite) {


                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID,
                            unitEquipment.getId());
                    values.put(TableNameAndColumnStatement.UNIQUE_NO,
                            unitEquipment.getEquipmentUniqueNo());
                    values.put(TableNameAndColumnStatement.MANUFACTURER,
                            unitEquipment.getEquipmentManufacturer());
                    values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED,
                            unitEquipment.isCustomerProvided());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS,
                            unitEquipment.getStatus());
                    values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE,
                            unitEquipment.getEquipmentId());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            unitEquipment.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID,
                            unitEquipment.getPostId());
                    sqlite.update(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, values, TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + " = " + equuipmentId, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }

    }

    public void updateUnitStrength(UnitShiftRank unitShiftRank,int rankID){
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        if (unitShiftRank != null) {
            synchronized (sqlite) {


                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, unitShiftRank.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitShiftRank.getUnitId());
                    values.put(TableNameAndColumnStatement.SHIFT_ID, unitShiftRank.getShiftId());
                    values.put(TableNameAndColumnStatement.RANK_ID, unitShiftRank.getRankId());
                    values.put(TableNameAndColumnStatement.RANK_COUNT, unitShiftRank.getRankCount());
                    values.put(TableNameAndColumnStatement.RANK_ABBREVATION, unitShiftRank.getRankAbbrevation());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitShiftRank.getBarrackId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unitShiftRank.getUnitName());
                    values.put(TableNameAndColumnStatement.SHIFT_NAME, unitShiftRank.getShiftName());
                    values.put(TableNameAndColumnStatement.RANK_NAME, unitShiftRank.getRankName());
                    values.put(TableNameAndColumnStatement.BARRACK_NAME, unitShiftRank.getBarrackName());
                    values.put(TableNameAndColumnStatement.STRENGTH, unitShiftRank.getStrength());
                    values.put(TableNameAndColumnStatement.ACTUAL, 0);
                    values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                    values.put(TableNameAndColumnStatement.IS_ARMED, unitShiftRank.getIsArmed());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    sqlite.update(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, values, TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID + " = " + rankID, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }
        }
    }

    public void insertUnitStrength(UnitShiftRank unitShiftRank){
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        if (unitShiftRank != null) {
            synchronized (sqlite) {


                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, unitShiftRank.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitShiftRank.getUnitId());
                    values.put(TableNameAndColumnStatement.SHIFT_ID, unitShiftRank.getShiftId());
                    values.put(TableNameAndColumnStatement.RANK_ID, unitShiftRank.getRankId());
                    values.put(TableNameAndColumnStatement.RANK_COUNT, unitShiftRank.getRankCount());
                    values.put(TableNameAndColumnStatement.RANK_ABBREVATION, unitShiftRank.getRankAbbrevation());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, unitShiftRank.getBarrackId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unitShiftRank.getUnitName());
                    values.put(TableNameAndColumnStatement.SHIFT_NAME, unitShiftRank.getShiftName());
                    values.put(TableNameAndColumnStatement.RANK_NAME, unitShiftRank.getRankName());
                    values.put(TableNameAndColumnStatement.BARRACK_NAME, unitShiftRank.getBarrackName());
                    values.put(TableNameAndColumnStatement.STRENGTH, unitShiftRank.getStrength());
                    values.put(TableNameAndColumnStatement.ACTUAL, 0);
                    values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                    values.put(TableNameAndColumnStatement.IS_ARMED, unitShiftRank.getIsArmed());
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                    sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                } finally {
                    sqlite.close();
                }
            }
        }
    }
}
