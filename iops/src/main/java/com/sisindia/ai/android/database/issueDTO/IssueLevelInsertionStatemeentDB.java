package com.sisindia.ai.android.database.issueDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 28-07-2016.
 */
public class IssueLevelInsertionStatemeentDB extends SISAITrackingDB {

    public IssueLevelInsertionStatemeentDB(Context context) {
        super(context);
    }

    public void addIssueDetails(ArrayList<IssuesMO> issuesMOList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {
            if (issuesMOList != null && issuesMOList.size() != 0) {
                try {
                    for (IssuesMO issuesMO : issuesMOList) {

                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.ISSUE_ID, issuesMO.getIssueId());
                        values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, issuesMO.getIssueMatrixId());
                        values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issuesMO.getIssueTypeId());
                        values.put(TableNameAndColumnStatement.SOURCE_TASK_ID, issuesMO.getSourceTaskId());
                        values.put(TableNameAndColumnStatement.UNIT_ID, issuesMO.getUnitId());
                        values.put(TableNameAndColumnStatement.BARRACK_ID, issuesMO.getBarrackId());
                        values.put(TableNameAndColumnStatement.GUARD_ID, issuesMO.getGuardId());
                        values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issuesMO.getIssueCategoryId());
                        values.put(TableNameAndColumnStatement.ISSUE_MODE_ID, issuesMO.getIssueModeId());
                        values.put(TableNameAndColumnStatement.ISSUE_NATURE_ID, issuesMO.getIssueNatureId());
                        values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issuesMO.getIssueCauseId());
                        values.put(TableNameAndColumnStatement.ISSUE_SERVERITY_ID, issuesMO.getIssueSeverityId());
                        values.put(TableNameAndColumnStatement.ISSUE_STATUS_ID, issuesMO.getIssueStatusId());
                        values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, issuesMO.getActionPlanId());
                        values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, issuesMO.getCreatedDateTime());
                        values.put(TableNameAndColumnStatement.CREATED_BY_ID, issuesMO.getCreatedById());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, issuesMO.getAssignedToId());
                        values.put(TableNameAndColumnStatement.TARGET_ACTION_END_DATE, issuesMO.getTargetActionEndDate());
                        values.put(TableNameAndColumnStatement.CLOSURE_END_DATE, issuesMO.getClosureEndDate());
                        values.put(TableNameAndColumnStatement.REMARKS, issuesMO.getRemarks());
                        values.put(TableNameAndColumnStatement.CLOSURE_REMARKS, issuesMO.getClosureRemarks());
                        values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID, issuesMO.getUnitContactId());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME, issuesMO.getAssignedToName());
                        values.put(TableNameAndColumnStatement.UNIT_CONTACT_NAME, issuesMO.getUnitContactName());
                        values.put(TableNameAndColumnStatement.GUARD_NAME, issuesMO.getGuardName());
                        values.put(TableNameAndColumnStatement.GUARD_CODE, issuesMO.getGuardCode());
                        values.put(TableNameAndColumnStatement.PHONE_NO, issuesMO.getPhoneNumber());
                        values.put(TableNameAndColumnStatement.IS_NEW, 1);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 0);


                        sqlite.insert(TableNameAndColumnStatement.ISSUES_TABLE, null, values);

                    }

                } catch (SQLiteConstraintException sqliteConstraintException) {
                    Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                    Crashlytics.logException(sqliteConstraintException);
                } catch (SQLiteException sqliteException) {
                    Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                    Crashlytics.logException(sqliteException);
                } catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                } finally {
                    if (null != sqlite && sqlite.isOpen()) {
                        sqlite.setTransactionSuccessful();
                        sqlite.endTransaction();

                        sqlite.close();
                    }
                }
            }
        }
    }

    public void addImprovementPlanDetails(ImprovementPlanIR improvementPlanIRModel) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {
            if (improvementPlanIRModel != null) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, improvementPlanIRModel.getIssueMatrixId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID, improvementPlanIRModel.getActionPlanId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, improvementPlanIRModel.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, improvementPlanIRModel.getBarrackId());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, improvementPlanIRModel.getRaisedDate());
                    values.put(TableNameAndColumnStatement.ISSUE_ID, improvementPlanIRModel.getIssueId());
                    values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanIRModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE, improvementPlanIRModel.getExpectedClosureDate());
                    values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID, improvementPlanIRModel.getTargetEmployeeId());
                    values.put(TableNameAndColumnStatement.STATUS_ID, improvementPlanIRModel.getActionPlanStatusId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID, improvementPlanIRModel.getImprovementPlanCategoryId());
                    values.put(TableNameAndColumnStatement.REMARKS, improvementPlanIRModel.getRemarks());
                    values.put(TableNameAndColumnStatement.BUDGET, improvementPlanIRModel.getBudget());
                    //values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, improvementPlanIRModel.getExpectedClosureDate());
                    // values.put(TableNameAndColumnStatement.CLOSED_BY_ID,improvementPlanIRModel.get);
                    values.put(TableNameAndColumnStatement.CLOSURE_COMMENT, improvementPlanIRModel.getClosureComment());
                    //  values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,improvementPlanIRModel.geta);
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, improvementPlanIRModel.getAssignedToId());
                    values.put(TableNameAndColumnStatement.TASK_ID, improvementPlanIRModel.getTaskId());
                    values.put(TableNameAndColumnStatement.IS_NEW, 1);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                    sqlite.insert(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, null, values);
                    sqlite.setTransactionSuccessful();

                } catch (SQLiteConstraintException sqliteConstraintException) {
                    Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                    Crashlytics.logException(sqliteConstraintException);
                } catch (SQLiteException sqliteException) {
                    Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                    Crashlytics.logException(sqliteException);
                } catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                } finally {
                    sqlite.endTransaction();
                }
            }

        }
    }

    public void addImprovementPlanDetails(List<ImprovementPlanIR> improvementPlanList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {
            if (improvementPlanList != null && improvementPlanList.size() > 0) {
                try {
                    for (ImprovementPlanIR improvementPlanIRModel : improvementPlanList) {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, improvementPlanIRModel.getIssueMatrixId());
                        values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID, improvementPlanIRModel.getActionPlanId());
                        values.put(TableNameAndColumnStatement.UNIT_ID, improvementPlanIRModel.getUnitId());
                        values.put(TableNameAndColumnStatement.BARRACK_ID, improvementPlanIRModel.getBarrackId());
                        values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, improvementPlanIRModel.getRaisedDate());
                        values.put(TableNameAndColumnStatement.ISSUE_ID, improvementPlanIRModel.getIssueId());
                        values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanIRModel.getMasterActionPlanId());
                        values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE, improvementPlanIRModel.getExpectedClosureDate());
                        values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID, improvementPlanIRModel.getTargetEmployeeId());
                        values.put(TableNameAndColumnStatement.STATUS_ID, improvementPlanIRModel.getActionPlanStatusId());
                        values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID, improvementPlanIRModel.getImprovementPlanCategoryId());
                        values.put(TableNameAndColumnStatement.REMARKS, improvementPlanIRModel.getRemarks());
                        values.put(TableNameAndColumnStatement.TASK_ID, improvementPlanIRModel.getTaskId());

                        //values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, improvementPlanIRModel.getExpectedClosureDate());
                        // values.put(TableNameAndColumnStatement.CLOSED_BY_ID,improvementPlanIRModel.get);
                        values.put(TableNameAndColumnStatement.CLOSURE_COMMENT, improvementPlanIRModel.getClosureComment());
                        //  values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,improvementPlanIRModel.geta);
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, improvementPlanIRModel.getAssignedToId());
                        values.put(TableNameAndColumnStatement.IS_NEW, 1);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                        values.put(TableNameAndColumnStatement.CLIENT_COORDINATION_QUESTION_ID, improvementPlanIRModel.getClientCoordinationQuestionId());
                        sqlite.insert(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, null, values);
                    }
                    sqlite.setTransactionSuccessful();

                } catch (SQLiteConstraintException sqliteConstraintException) {
                    Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                    Crashlytics.logException(sqliteConstraintException);
                } catch (SQLiteException sqliteException) {
                    Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                    Crashlytics.logException(sqliteException);
                } catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                } finally {
                    Timber.e("SQLITE transaction ended");
                    sqlite.endTransaction();
                }
            }
        }
    }

    public void updateImprovementPlanDetails(ImprovementPlanListMO improvementPlanIRModel) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {
            if (improvementPlanIRModel != null) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, improvementPlanIRModel.getIssueMatrixId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID, improvementPlanIRModel.getImprovementId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, improvementPlanIRModel.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, improvementPlanIRModel.getBarrackId());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, improvementPlanIRModel.getCreatedDateTime());
                    values.put(TableNameAndColumnStatement.ISSUE_ID, improvementPlanIRModel.getIssueId());
                    values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanIRModel.getActionPlanId());
                    values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE, improvementPlanIRModel.getExpectedClosureDate());
                    values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID, improvementPlanIRModel.getTargetEmployeeId());
                    values.put(TableNameAndColumnStatement.STATUS_ID, improvementPlanIRModel.getImprovementStatusId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID, improvementPlanIRModel.getImprovementPlanCategoryId());
                    values.put(TableNameAndColumnStatement.REMARKS, improvementPlanIRModel.getRemarks());
                    values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, improvementPlanIRModel.getClosureDateTime());
                    values.put(TableNameAndColumnStatement.CLOSED_BY_ID, improvementPlanIRModel.getClosedById());
                    values.put(TableNameAndColumnStatement.CLOSURE_COMMENT, improvementPlanIRModel.getClosureRemarks());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME, improvementPlanIRModel.getAssignedToName());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, improvementPlanIRModel.getAssignedToId());
                    values.put(TableNameAndColumnStatement.BUDGET, improvementPlanIRModel.getBudget());
                    values.put(TableNameAndColumnStatement.IS_NEW, 1);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                    sqlite.update(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, values, TableNameAndColumnStatement.ID + " = " + improvementPlanIRModel.getSeqId(), null);
                    sqlite.setTransactionSuccessful();

                } catch (SQLiteConstraintException sqliteConstraintException) {
                    Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                    Crashlytics.logException(sqliteConstraintException);
                } catch (SQLiteException sqliteException) {
                    Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                    Crashlytics.logException(sqliteException);
                } catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                } finally {
                    sqlite.endTransaction();
                    Timber.d("ImprovementPlanList upadte %s", IOPSApplication.getGsonInstance().toJson(improvementPlanIRModel));

                }
            }

        }
    }

    public void updateImprovementPlanStatus(int issueId, int statusId, String closureDateTime,
                                            SQLiteDatabase sqlite) {
        if (sqlite == null) {
            sqlite = this.getWritableDatabase();
        }
        sqlite.beginTransaction();
        DateTimeFormatConversionUtil dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();


        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();

                values.put(TableNameAndColumnStatement.IS_NEW, 0);
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                if (TextUtils.isEmpty(closureDateTime)) {
                    values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, dateTimeFormatConversionUtil.getCurrentDate());
                }
                values.put(TableNameAndColumnStatement.STATUS_ID, statusId);
                sqlite.update(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, values, TableNameAndColumnStatement.ISSUE_ID + " = " + issueId, null);
                sqlite.setTransactionSuccessful();

            } catch (SQLiteConstraintException sqliteConstraintException) {
                Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
                Crashlytics.logException(sqliteConstraintException);
            } catch (SQLiteException sqliteException) {
                Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
                Crashlytics.logException(sqliteException);
            } catch (Exception exception) {
                Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }


        }
    }
}
