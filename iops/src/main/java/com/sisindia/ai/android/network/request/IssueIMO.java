package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

public class IssueIMO  {

    @SerializedName("IssueTypeId")
    private String issueTypeId;
    @SerializedName("IssueMatrixId")
    private String issueMatrixId;
    @SerializedName("Description")
    private String description;
    @SerializedName("RaisedById")
    private String raisedById;
    @SerializedName("UnitId")
    private String unitId;
    @SerializedName("SourceTaskId")
    private String sourceTaskId;
    @SerializedName("IssueSourceId")
    private String issueSourceId;
    @SerializedName("IssueStatus")
    private String issueStatus;
    @SerializedName("AssignedToId")
    private String assignedToId;
    @SerializedName("RaisedDateTime")
    private String raisedDateTime;
    @SerializedName("AssignedToDateTime")
    private String assignedToDateTime;
    @SerializedName("ClosureDateTime")
    private String closureDateTime;
    @SerializedName("TargetActionDateTime")
    private String targetActionDateTime;
    @SerializedName("IssueCauseId")
    private String issueCauseId;
    @SerializedName("IssueSeverityId")
    private String issueSeverityId;
    @SerializedName("BarrackId")
    private String barrackId;
    @SerializedName("IssueNatureId")
    private String issueNatureId;
    @SerializedName("Remarks")
    private String clientRemarks;
    @SerializedName("IssueCheckList")
    private String issueCheckList;
    @SerializedName("GuardId")
    private String guardId;
    @SerializedName("IssueActionId")
    private String issueActionId;
    @SerializedName("GuardCode")
    private String guardCode;
    @SerializedName("IssueCategoryId")
    private String issueCategoryId;
    @SerializedName("UnitEmployeeId")
    private String unitEmployeeId;

    @SerializedName("Id")
    private Integer id;

    @SerializedName("IssueId")
    private Integer issueId;

    @SerializedName("PhoneNo")
    private String phoneNo;

    private Integer seqId;

    @SerializedName("IssueStatusValue")
    private String issueStatusValue;

    @SerializedName("AssigneeName")
    private String assigneeName;

    @SerializedName("GrievanceCause")
    private String grievanceCause;

    @SerializedName("NatureOfIssue")
    private String natureOfIssue;

    @SerializedName("IsFromROCC")
    private boolean isFromROCC;


    @SerializedName("ClientContactName")
    private String clientContactName;

    @SerializedName("ClientPhoneNumber")
    private String clientPhoneNumber;

    @SerializedName("StatusValue")
    private String statusValue;

    @SerializedName("RaisedByName")
    private String raisedByName;

    /**
     * @return The issueTypeId
     */
    public String getIssueTypeId() {
        return issueTypeId;
    }

    /**
     * @param issueTypeId The IssueTypeId
     */
    public void setIssueTypeId(String issueTypeId) {
        this.issueTypeId = issueTypeId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The raisedById
     */
    public String getRaisedById() {
        return raisedById;
    }

    /**
     * @param raisedById The RaisedById
     */
    public void setRaisedById(String raisedById) {
        this.raisedById = raisedById;
    }

    /**
     * @return The unitId
     */
    public String getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The sourceTaskId
     */
    public String getSourceTaskId() {
        return sourceTaskId;
    }

    /**
     * @param sourceTaskId The SourceTaskId
     */
    public void setSourceTaskId(String sourceTaskId) {
        this.sourceTaskId = sourceTaskId;
    }


    /**
     * @return The issueSourceId
     */
    public String getIssueSourceId() {
        return issueSourceId;
    }

    /**
     * @param issueSourceId The IssueSourceId
     */
    public void setIssueSourceId(String issueSourceId) {
        this.issueSourceId = issueSourceId;
    }

    /**
     * @return The issueStatus
     */
    public String getIssueStatus() {
        return issueStatus;
    }

    /**
     * @param issueStatus The IssueStatus
     */
    public void setIssueStatus(String issueStatus) {
        this.issueStatus = issueStatus;
    }

    /**
     * @return The assignedToId
     */
    public String getAssignedToId() {
        return assignedToId;
    }

    /**
     * @param assignedToId The AssignedToId
     */
    public void setAssignedToId(String assignedToId) {
        this.assignedToId = assignedToId;
    }

    /**
     * @return The raisedDateTime
     */
    public String getRaisedDateTime() {
        return raisedDateTime;
    }

    /**
     * @param raisedDateTime The RaisedDateTime
     */
    public void setRaisedDateTime(String raisedDateTime) {
        this.raisedDateTime = raisedDateTime;
    }

    /**
     * @return The assignedToDateTime
     */
    public String getAssignedToDateTime() {
        return assignedToDateTime;
    }

    /**
     * @param assignedToDateTime The AssignedToDateTime
     */
    public void setAssignedToDateTime(String assignedToDateTime) {
        this.assignedToDateTime = assignedToDateTime;
    }

    /**
     * @return The closureDateTime
     */
    public String getClosureDateTime() {
        return closureDateTime;
    }

    /**
     * @param closureDateTime The ClosureDateTime
     */
    public void setClosureDateTime(String closureDateTime) {
        this.closureDateTime = closureDateTime;
    }

    /**
     * @return The issueCauseId
     */
    public String getIssueCauseId() {
        return issueCauseId;
    }

    /**
     * @param issueCauseId The IssueCauseId
     */
    public void setIssueCauseId(String issueCauseId) {
        this.issueCauseId = issueCauseId;
    }

    /**
     * @return The barrackId
     */
    public String getBarrackId() {
        return barrackId;
    }

    /**
     * @param barrackId The BarrackId
     */
    public void setBarrackId(String barrackId) {
        this.barrackId = barrackId;
    }

    /**
     * @return The issueNatureId
     */
    public String getIssueNatureId() {
        return issueNatureId;
    }

    /**
     * @param issueNatureId The IssueNatureId
     */
    public void setIssueNatureId(String issueNatureId) {
        this.issueNatureId = issueNatureId;
    }

    /**
     * @return The clientRemarks
     */
    public String getClientRemarks() {
        return clientRemarks;
    }

    /**
     * @param clientRemarks The ClientRemarks
     */
    public void setClientRemarks(String clientRemarks) {
        this.clientRemarks = clientRemarks;
    }

    /**
     * @return The issueCheckList
     */
    public String getIssueCheckList() {
        return issueCheckList;
    }

    /**
     * @param issueCheckList The IssueCheckList
     */
    public void setIssueCheckList(String issueCheckList) {
        this.issueCheckList = issueCheckList;
    }

    /**
     * @return The guardId
     */
    public String getGuardId() {
        return guardId;
    }

    /**
     * @param guardId The GuardId
     */
    public void setGuardId(String guardId) {
        this.guardId = guardId;
    }

    /**
     * @return The issueActionId
     */
    public String getIssueActionId() {
        return issueActionId;
    }

    /**
     * @param issueActionId The IssueActionId
     */
    public void setIssueActionId(String issueActionId) {
        this.issueActionId = issueActionId;
    }

    public String getIssueMatrixId() {
        return issueMatrixId;
    }

    public void setIssueMatrixId(String issueMatrixId) {
        this.issueMatrixId = issueMatrixId;
    }

    public String getTargetActionDateTime() {
        return targetActionDateTime;
    }

    public void setTargetActionDateTime(String targetActionDateTime) {
        this.targetActionDateTime = targetActionDateTime;
    }

    @Override
    public String toString() {
        return "IssueIMO{" +
                "issueTypeId='" + issueTypeId + '\'' +
                ", issueMatrixId='" + issueMatrixId + '\'' +
                ", description='" + description + '\'' +
                ", raisedById='" + raisedById + '\'' +
                ", unitId='" + unitId + '\'' +
                ", sourceTaskId='" + sourceTaskId + '\'' +
                ", issueSourceId='" + issueSourceId + '\'' +
                ", issueStatus='" + issueStatus + '\'' +
                ", assignedToId='" + assignedToId + '\'' +
                ", raisedDateTime='" + raisedDateTime + '\'' +
                ", assignedToDateTime='" + assignedToDateTime + '\'' +
                ", closureDateTime='" + closureDateTime + '\'' +
                ", targetActionDateTime='" + targetActionDateTime + '\'' +
                ", issueCauseId='" + issueCauseId + '\'' +
                ", issueSeverityId='" + issueSeverityId + '\'' +
                ", barrackId='" + barrackId + '\'' +
                ", issueNatureId='" + issueNatureId + '\'' +
                ", clientRemarks='" + clientRemarks + '\'' +
                ", issueCheckList='" + issueCheckList + '\'' +
                ", guardId='" + guardId + '\'' +
                ", issueActionId='" + issueActionId + '\'' +
                ", guardCode='" + guardCode + '\'' +
                ", issueCategoryId='" + issueCategoryId + '\'' +
                ", unitEmployeeId='" + unitEmployeeId + '\'' +
                ", id=" + id +
                ", issueId=" + issueId +
                ", phoneNo='" + phoneNo + '\'' +
                ", seqId=" + seqId +
                ", issueStatusValue='" + issueStatusValue + '\'' +
                ", assigneeName='" + assigneeName + '\'' +
                ", grievanceCause='" + grievanceCause + '\'' +
                ", natureOfIssue='" + natureOfIssue + '\'' +
                ", isFromROCC=" + isFromROCC +
                ", clientContactName='" + clientContactName + '\'' +
                ", clientPhoneNumber='" + clientPhoneNumber + '\'' +
                ", statusValue='" + statusValue + '\'' +
                ", raisedByName='" + raisedByName + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuardCode() {
        return guardCode;
    }

    public void setGuardCode(String guardCode) {
        this.guardCode = guardCode;
    }

    public String getIssueCategoryId() {
        return issueCategoryId;
    }

    public void setIssueCategoryId(String issueCategoryId) {
        this.issueCategoryId = issueCategoryId;
    }

    public String getUnitEmployeeId() {
        return unitEmployeeId;
    }

    public void setUnitEmployeeId(String unitEmployeeId) {
        this.unitEmployeeId = unitEmployeeId;
    }

    public Integer getSeqId() {
        return seqId;
    }

    public void setSeqId(Integer seqId) {
        this.seqId = seqId;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getClientContactName() {
        return clientContactName;
    }

    public void setClientContactName(String clientContactName) {
        this.clientContactName = clientContactName;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public String getGrievanceCause() {
        return grievanceCause;
    }

    public void setGrievanceCause(String grievanceCause) {
        this.grievanceCause = grievanceCause;
    }

    public boolean isFromROCC() {
        return isFromROCC;
    }

    public void setFromROCC(boolean fromROCC) {
        isFromROCC = fromROCC;
    }

    public String getIssueSeverityId() {
        return issueSeverityId;
    }

    public void setIssueSeverityId(String issueSeverityId) {
        this.issueSeverityId = issueSeverityId;
    }

    public String getIssueStatusValue() {
        return issueStatusValue;
    }

    public void setIssueStatusValue(String issueStatusValue) {
        this.issueStatusValue = issueStatusValue;
    }

    public String getNatureOfIssue() {
        return natureOfIssue;
    }

    public void setNatureOfIssue(String natureOfIssue) {
        this.natureOfIssue = natureOfIssue;
    }

    public String getRaisedByName() {
        return raisedByName;
    }

    public void setRaisedByName(String raisedByName) {
        this.raisedByName = raisedByName;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }
}