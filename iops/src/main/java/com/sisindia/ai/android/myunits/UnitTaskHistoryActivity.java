package com.sisindia.ai.android.myunits;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitHistoryBaseOR;
import com.sisindia.ai.android.myunits.models.UnitHistoryMO;
import com.sisindia.ai.android.myunits.models.UnitTaskHistoryHeader;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UnitTaskHistoryActivity extends BaseActivity {

    private RecyclerView unitTaskHistoryRecycleview;
    private UnitTaskHistoryAdapter unitTaskHistoryAdapter;
    private Resources resources;
    private ArrayList unitTaskHistory;
    private MyUnitHomeMO myUnitHomeMO;
    private int taskTypeId;

    @Bind(R.id.unitHisoryParentLayout)
    RelativeLayout unitHisoryParentLayout;
    private  UnitHistoryBaseOR unitHistoryBase;
    private List<UnitHistoryMO> unitHistoryMOList;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.unittask_history_no_data_available)
    CustomFontTextview unittaskHistoryNoDataAvailable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_task_history);
        ButterKnife.bind(this);



        Object object = SingletonUnitDetails.getInstance().getUnitDetails();
        if (object instanceof MyUnitHomeMO) {
            myUnitHomeMO = (MyUnitHomeMO) object;
        }

        if(myUnitHomeMO == null){
            myUnitHomeMO = appPreferences.getUnitJsonData();
        }
        Intent intent = getIntent();
        if(intent != null){
            taskTypeId = intent.getIntExtra(TableNameAndColumnStatement.TASK_TYPE_ID,0);
            setBaseToolbar(toolbar, RotaTaskTypeEnum.valueOf(taskTypeId).name().replace("_"," "));
        }


        //dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        // RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        //unitTaskHistoryRecycleview.addItemDecoration(dividerItemDecoration);
        //unitTaskHistoryAdapter.setOnRecyclerViewItemClickListener(this);
        getUnitTaskHistoryApiCall();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {

            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private  void updateRecyclerView(){



        unitHistoryMOList = unitHistoryBase.getData();
        unitTaskHistory = new ArrayList();

        if(unitHistoryMOList != null && unitHistoryMOList.size() > 0){

            UnitTaskHistoryHeader unitTaskHistoryHeader = new UnitTaskHistoryHeader();
            unitTaskHistoryHeader.setLastMonthCount(String.valueOf(unitHistoryMOList.get(0).getLastMonthCompletedTasksCount()));
            unitTaskHistoryHeader.setThisMonthCount(String.valueOf(unitHistoryMOList.get(0).getCurrentMonthCompletedTasksCount()));
            unitTaskHistory.add(unitTaskHistoryHeader);
            unitTaskHistory.addAll(unitHistoryMOList);

            unitTaskHistoryRecycleview = (RecyclerView) findViewById(R.id.singleUnitViewHistory);
            if(unittaskHistoryNoDataAvailable.getVisibility() == View.GONE)
                unitTaskHistoryRecycleview.setVisibility(View.VISIBLE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            unitTaskHistoryRecycleview.setLayoutManager(linearLayoutManager);
            unitTaskHistoryRecycleview.setItemAnimator(new DefaultItemAnimator());
            unitTaskHistoryAdapter = new UnitTaskHistoryAdapter(this);
            //setUnitTaskHistoryData();

            unitTaskHistoryAdapter.setUnitTaskHistoryData(unitTaskHistory);
            unitTaskHistoryRecycleview.setAdapter(unitTaskHistoryAdapter);

        }
        else{
            unittaskHistoryNoDataAvailable.setVisibility(View.VISIBLE);
            unittaskHistoryNoDataAvailable.setText(getResources().getString(R.string.NO_DATA_AVAILABLE));
        }




    }

    private void getUnitTaskHistoryApiCall() {
        SISClient sisClient = new SISClient(this);
        showprogressbar();
        sisClient.getApi().getUnitTaskHistoryDetails(myUnitHomeMO.getUnitId(),taskTypeId,new Callback<UnitHistoryBaseOR>() {
            @Override
            public void success(final UnitHistoryBaseOR unitHistoryBaseOR, Response response) {

                switch (unitHistoryBaseOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        hideprogressbar();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                unitHistoryBase = unitHistoryBaseOR;
                                hideprogressbar();
                                updateRecyclerView();
                            }
                        });
                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR :
                        Toast.makeText(UnitTaskHistoryActivity.this, getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                        break;

                    default :
                        Toast.makeText(UnitTaskHistoryActivity.this, getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                        break;
                }

            }

            @Override
            public void failure(RetrofitError error) {
                hideprogressbar();
                Util.printRetorfitError("UnitTaskHistory", error);
                finish();
            }
        });

    }

    private void showprogressbar() {
        Util.showProgressBar(UnitTaskHistoryActivity.this, R.string.loading_please_wait);

    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }
}
