package com.sisindia.ai.android.rota;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.home.RotaUnitSyncListener;
import com.sisindia.ai.android.home.RotaUpdateAsyncTask;
import com.sisindia.ai.android.loadconfiguration.EmployeeLeaveCalendar;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.BaseRotaSync;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.rota.models.HolidayDateMO;
import com.sisindia.ai.android.rota.models.RotaCalendarMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.rotaCompliance.RotaComplianceActivity;
import com.sisindia.ai.android.rota.rotaCompliance.RotaCompliancePercentageOR;
import com.sisindia.ai.android.rota.rotacalendarview.RotaCalendarView;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RotaFragment extends Fragment implements RotaCalendarView.RobotoCalendarListener, OnRecyclerViewItemClickListener, View.OnClickListener, RotaUnitSyncListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public RotaCalendarView robotoCalendarView;
    public SISAITrackingDB sisaiTrackingDBInstance;

    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private RotaRecyclerViewAdapter rotaRecyclerViewAdapter;
    private RelativeLayout monthlyGraphPlayout;
    private FloatingActionButton rotaAddFlotingActionButton;
    private Drawable dividerDrawable;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private Calendar currentCalendar;

    private Date newDate;
    private UpdateRotaTaskModelListener updateRotaTaskModelListener;
    private ArrayList<Object> rotaTaskList;
    private RotaUnitSyncListener rotaUnitSyncListener;
    private List<RotaCalendarMO> rotaCalendarMOList;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private int hours, minutes, viewHeight;
    private RotaCompliancePercentageOR rotaCompliance;
    private AppPreferences appPreferences;
    private DateRotaModel dateRotaModel;
    private List<HolidayDateMO> holidayLeaveCalendarMOList;
    private List<EmployeeLeaveCalendar> leaveCalendarMOList;
    private View rotaFragmentView;
    private RotaCalendarView robotoCalendarPicker;
    private String todaysDate;
    //    private View monthlyEmptyProgressBar;
    //    private View dailyEmptyProgressBar;
    private RelativeLayout monthlyProgressLayout;
    private View monthlyProgressBar;
    private RelativeLayout dailyProgressLayout;
    private View dailyProgressBar;
    private LinearLayout rotaComplianceProgress;
    private CustomFontTextview rotaComplianceTextview;
    private CustomFontTextview rotaThismonthLabelTxt;
    private CustomFontTextview rotaPercentageThismonthTxt;
    private CustomFontTextview rotaPercentageTodayTxt;
    private CustomFontTextview rotaTodayLabelTxt;
    private View rootView;
    private SISClient sisClient;
    private String selectedRotaDate;

    public RotaFragment() {
        // Required empty public constructor
    }

    public static RotaFragment newInstance(String param1, String param2) {
        RotaFragment fragment = new RotaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void rotaUnitDbSyncing(boolean isSyned) {
        if (getActivity() != null) {
            if (isSyned) {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_str));
                /*rotaComplianceProgress.setVisibility(View.VISIBLE);
                rotaComplianceTextview.setVisibility(View.VISIBLE);*/
                setRotaTaskList();
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_error_str));
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);////for enable the menu in fragment
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        sisaiTrackingDBInstance = ((HomeActivity) getActivity()).sisaiTrackingDBInstance;
        sisClient = new SISClient(getActivity());
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        appPreferences = new AppPreferences(getActivity());
        rotaUnitSyncListener = this;
        dateTimeFormatConversionUtil.getFutureDateAfterSpecificMonths(3);
    }

    @Override
    public void onResume() {
        super.onResume();
        setRotaTaskList();
        if (NetworkUtil.isConnected) {
            if (appPreferences.getSaveAppVersion() < BuildConfig.VERSION_CODE) {
                updateAppVersion();
            }
        }
    }

    private void updateAppVersion() {
        sisClient.getApi().updateAppVersion(String.valueOf(BuildConfig.VERSION_NAME),
                new Callback<CommonResponse>() {
                    @Override
                    public void success(CommonResponse commonResponse, Response response) {
                        switch (commonResponse.getStatusCode()) {

                            case HttpURLConnection.HTTP_OK:
                                appPreferences.saveAppVersion(BuildConfig.VERSION_CODE);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Crashlytics.logException(error.getCause());
                    }
                });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_rota_homepage, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.calender:
                if (robotoCalendarView.getVisibility() != View.VISIBLE)
                    robotoCalendarView.setVisibility(View.VISIBLE);
                else
                    robotoCalendarView.setVisibility(View.GONE);
                break;
            case R.id.syncRota:

                if (robotoCalendarView.getVisibility() == View.VISIBLE)
                    robotoCalendarView.setVisibility(View.GONE);

                if (NetworkUtil.isConnected) {
                    getRotaApiCall();
                    //ManualUnitRotaSync.getUnitApiCall(rotaUnitSyncListener, getActivity(), isDetached());

                } else {
                    Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void getRotaApiCall() {

        /*if(tablesToSync()){}*/
        int taskCount = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getCompletedButNotSyncedTaskCount(dateTimeFormatConversionUtil.getCurrentDate());

        if (taskCount > 0) {
            Toast.makeText(getActivity(), "Please sync your completed task first", Toast.LENGTH_LONG).show();
        } else {
            sisClient.getApi().getRota(new Callback<BaseRotaSync>() {
                @Override
                public void success(BaseRotaSync baseRotaSync, Response response) {
                    switch (baseRotaSync.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            new RotaUpdateAsyncTask(getActivity(), baseRotaSync.getData(), rotaUnitSyncListener).execute();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar();
                    Crashlytics.logException(error);
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        if (context instanceof UpdateRotaTaskModelListener)
            updateRotaTaskModelListener = (UpdateRotaTaskModelListener) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void init(View view) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rota_recyclerview);
        monthlyGraphPlayout = (RelativeLayout) rootView.findViewById(R.id.mothly_grpah_layout);
        rotaAddFlotingActionButton = (FloatingActionButton) rootView.findViewById(R.id.rota_add_floatting_button);
        robotoCalendarPicker = (RotaCalendarView) view.findViewById(R.id.robotoCalendarPicker);
        rotaTodayLabelTxt = (CustomFontTextview) view.findViewById(R.id.rota_today_label_txt);
        rotaPercentageTodayTxt = (CustomFontTextview) view.findViewById(R.id.rota_percentage_today_txt);
        rotaPercentageThismonthTxt = (CustomFontTextview) view.findViewById(R.id.month_percentage_txt);
        rotaThismonthLabelTxt = (CustomFontTextview) view.findViewById(R.id.rota_thismonth_label_txt);
        rotaComplianceProgress = (LinearLayout) view.findViewById(R.id.rota_compliance_progress);
        dailyProgressBar = view.findViewById(R.id.daily_progress_bar);
        dailyProgressLayout = (RelativeLayout) view.findViewById(R.id.daily_progress_layout);
        monthlyProgressBar = view.findViewById(R.id.monthly_progress_bar);
        monthlyProgressLayout = (RelativeLayout) view.findViewById(R.id.monthly_progress_layout);
        rotaComplianceTextview = (CustomFontTextview) view.findViewById(R.id.rota_Compliance_Textview);

//        dailyEmptyProgressBar = view.findViewById(R.id.daily_empty_progress_bar);
//        monthlyEmptyProgressBar = view.findViewById(R.id.monthly_empty_progress_bar);

        rotaComplianceProgress.setVisibility(View.GONE);
        rotaComplianceTextview.setVisibility(View.GONE);
    }

    public void updateRotaComplianceUi(DateRotaModel dateRotaModel, int dateStatus, String selectedDate) {
        rotaComplianceTextview.setVisibility(View.VISIBLE);
        rotaComplianceProgress.setVisibility(View.VISIBLE);
        float completedDailyTaskPercentage = 0f;
        float completedMonthlyTaskPercentage = 0f;
        DecimalFormat df = new DecimalFormat("###.##");
        if (rotaCompliance != null && rotaCompliance.monthlyDailyRotaCompliance != null) {
            if (!TextUtils.isEmpty(rotaCompliance.monthlyDailyRotaCompliance.DailyCompliance)) {
                completedDailyTaskPercentage = Float.valueOf(rotaCompliance.monthlyDailyRotaCompliance.DailyCompliance);
            }
            if (!TextUtils.isEmpty(rotaCompliance.monthlyDailyRotaCompliance.monthlyCompliance)) {
                completedMonthlyTaskPercentage = Float.valueOf(rotaCompliance.monthlyDailyRotaCompliance.monthlyCompliance);
            }
        }
        View dailyEmptyProgressBar = dailyProgressLayout.findViewById(R.id.daily_empty_progress_bar);
        View dailyCompletedprogressBar = dailyProgressLayout.findViewById(R.id.daily_progress_bar);
        ViewGroup.LayoutParams dailyEmptyProgressBarLayoutParams = dailyEmptyProgressBar.getLayoutParams();
        ViewGroup.LayoutParams dailyCompletedprogressBarLayoutParams = dailyCompletedprogressBar.getLayoutParams();
        View monthlyEmptyProgressBar = monthlyProgressLayout.findViewById(R.id.monthly_empty_progress_bar);
        View monthlyCompletedProgressBar = monthlyProgressLayout.findViewById(R.id.monthly_progress_bar);
        ViewGroup.LayoutParams monthlyEmptyProgressBarLayoutParams = monthlyEmptyProgressBar.getLayoutParams();
        ViewGroup.LayoutParams monthlyCompletedprogressBarLayoutParams = monthlyCompletedProgressBar.getLayoutParams();
        if (dateStatus < 0) {
            rotaComplianceProgress.setVisibility(View.VISIBLE);
            rotaComplianceTextview.setVisibility(View.VISIBLE);
            if ("0.0".equals(String.valueOf(df.format(completedDailyTaskPercentage)))) {
                rotaPercentageTodayTxt.setText("0%");
            } else if ("100.0".equals(String.valueOf(df.format(completedDailyTaskPercentage)))) {
                rotaPercentageTodayTxt.setText("100%");
            } else {
                rotaPercentageTodayTxt.setText(String.valueOf(df.format(completedDailyTaskPercentage)) + "%");
            }
            if ("0.0".equals(String.valueOf(df.format(completedMonthlyTaskPercentage)))) {
                rotaPercentageThismonthTxt.setText("0%");
            } else if ("100.0".equals(String.valueOf(df.format(completedMonthlyTaskPercentage)))) {
                rotaPercentageTodayTxt.setText("100%");
            } else {
                rotaPercentageThismonthTxt.setText("" + String.valueOf(df.format(completedMonthlyTaskPercentage)) + "%");
            }
            dailyEmptyProgressBarLayoutParams.width = Util.dpToPx(400);
            dailyEmptyProgressBar.setLayoutParams(dailyEmptyProgressBarLayoutParams);
            monthlyEmptyProgressBarLayoutParams.width = Util.dpToPx(400);
            monthlyEmptyProgressBar.setLayoutParams(dailyEmptyProgressBarLayoutParams);
            dailyCompletedprogressBarLayoutParams.width = Util.dpToPx((int) (completedDailyTaskPercentage * 1.5f));
            dailyProgressBar.setLayoutParams(dailyCompletedprogressBarLayoutParams);
            monthlyCompletedprogressBarLayoutParams.width = Util.dpToPx((int) (completedMonthlyTaskPercentage * 1.5f));
            monthlyProgressBar.setLayoutParams(monthlyCompletedprogressBarLayoutParams);
            rotaTodayLabelTxt.setText(dateTimeFormatConversionUtil.convertDateToDayMonthDateFormat(selectedDate).split(",")[1]);
            rotaThismonthLabelTxt.setText(getResources().getString(R.string.THIS_MONTH));
        } else if (dateStatus == 0) {
            rotaComplianceProgress.setVisibility(View.VISIBLE);
            rotaComplianceTextview.setVisibility(View.VISIBLE);
            rotaThismonthLabelTxt.setText(getResources().getString(R.string.THIS_MONTH));
            if ("0.0".equals(String.valueOf(df.format(completedMonthlyTaskPercentage)))) {
                rotaPercentageThismonthTxt.setText("0%");
            } else if ("100.0".equals(String.valueOf(df.format(completedMonthlyTaskPercentage)))) {
                rotaPercentageTodayTxt.setText("100%");
            } else {
                rotaPercentageThismonthTxt.setText("" + String.valueOf(df.format(completedMonthlyTaskPercentage)) + "%");
            }
            if (null != dateRotaModel && dateRotaModel.getRotaTotalCount() != 0) {
                float dailyRotaCompliance = ((float) dateRotaModel.getRotaCompletedCount() / (float) dateRotaModel.getRotaTotalCount()) * 100;
                BigDecimal bigDecimal = round(dailyRotaCompliance, 1);
                if ("0.0".equals(bigDecimal.toString())) {
                    rotaPercentageTodayTxt.setText("0%");
                } else if ("100.0".equals(bigDecimal.toString())) {
                    rotaPercentageTodayTxt.setText("100%");
                } else {
                    rotaPercentageTodayTxt.setText(bigDecimal + "%");
                }
                rotaTodayLabelTxt.setText(getResources().getString(R.string.TODAY));
                dailyEmptyProgressBarLayoutParams.width = Util.dpToPx(400);
                dailyEmptyProgressBar.setLayoutParams(dailyEmptyProgressBarLayoutParams);
                monthlyEmptyProgressBarLayoutParams.width = Util.dpToPx(400);
                monthlyEmptyProgressBar.setLayoutParams(dailyEmptyProgressBarLayoutParams);
                dailyCompletedprogressBarLayoutParams.width = Util.dpToPx((int) (dailyRotaCompliance * 1.5f));
                dailyProgressBar.setLayoutParams(dailyCompletedprogressBarLayoutParams);
            }
            monthlyCompletedprogressBarLayoutParams.width = Util.dpToPx((int) (completedMonthlyTaskPercentage * 1.5f));
            monthlyProgressBar.setLayoutParams(monthlyCompletedprogressBarLayoutParams);
        } else {
            rotaComplianceProgress.setVisibility(View.GONE);
            rotaComplianceTextview.setVisibility(View.GONE);
        }
    }

    public BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    private void getRotaComplianceApiCall(final String selectedDate, final DateRotaModel dateRotaModel, final int dateStatus) {

        sisClient.getApi().getRotaCompliancePercentage(selectedDate, new Callback<RotaCompliancePercentageOR>() {
            @Override
            public void success(RotaCompliancePercentageOR rotaCompliancePercentageOR, Response response) {
                switch (rotaCompliancePercentageOR.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        hideprogressbar();
                        rotaCompliance = rotaCompliancePercentageOR;
                        if (isAdded() && getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // updateRotaComplianceUi(dateRotaModel, dateStatus,selectedDate);
                                }
                            });
                        }
                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        Toast.makeText(getActivity(), getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getActivity(), getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);
            }
        });
    }

    private void rotaCalenderView() {
        calendarViewWithCurrentDate();
        getAllCalendarViewMO(dateTimeFormatConversionUtil.getCurrentDate(),
                dateTimeFormatConversionUtil.getCalendarInstance().get(Calendar.MONTH) + 1, currentCalendar);
        setHolidayAndLeaveToCalendarView();
        setTaskIndicationToCalendarView();
    }

    private void calendarViewWithCurrentDate() {
        // Gets the calendar from the view
        robotoCalendarView = (RotaCalendarView) rotaFragmentView.findViewById(R.id.robotoCalendarPicker);
        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);
        // Initialize the RobotoCalendarPicker with the current index and date
        currentCalendar = Calendar.getInstance(Locale.getDefault());
        newDate = currentCalendar.getTime();
        // Mark current day
        robotoCalendarView.markDayAsCurrentDay(newDate);
        robotoCalendarView.markDayAsSelectedDay(newDate);
    }

    private void getAllCalendarViewMO(String currentDate, int month, Calendar currentCalendar) {
        dateRotaModel = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaAndTaskCount(currentDate);
        selectedRotaDate = currentDate;
        /*getRotaComplianceApiCall(currentDate,
                dateRotaModel, dateTimeFormatConversionUtil.dateComparison(currentDate,
                        currentDate));*/
        rotaCalendarMOList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaTaskTotalTime(currentCalendar);
        holidayLeaveCalendarMOList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getHolidayDate(month);
        leaveCalendarMOList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getLeaveDate(month);
    }

    private void setTaskIndicationToCalendarView() {
        if (rotaCalendarMOList != null) {
            for (RotaCalendarMO rotaCalendarMO : rotaCalendarMOList) {
                hours = (int) Math.floor(rotaCalendarMO.getTotalTime() / 60);
                minutes = rotaCalendarMO.getTotalTime() % 60;
                if (rotaCalendarMO.getCalendarDate().equalsIgnoreCase(dateTimeFormatConversionUtil.getCurrentDate())) {
                    if (hours == 1 || (hours == 0 && minutes != 0)) {
                        viewHeight = 20;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_CIRCLE,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else if ((hours == 2) || (hours == 3)) {
                        viewHeight = 30;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else if ((hours == 4) || (hours == 5)) {
                        viewHeight = 40;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else {
                        viewHeight = 50;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    }
                } else {
                    if (hours == 1 || (hours == 0 && minutes != 0)) {
                        viewHeight = 20;
                    } else if ((hours == 2) || (hours == 3)) {
                        viewHeight = 30;
                    } else if ((hours == 4) || (hours == 5)) {
                        viewHeight = 40;
                    } else {
                        viewHeight = 50;
                    }
                    robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.YELLOW_COLOR_VIEW,
                            dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                }
            }
        }
    }

    private void setHolidayAndLeaveToCalendarView() {
        if (holidayLeaveCalendarMOList != null) {
            for (HolidayDateMO holidayDateMO : holidayLeaveCalendarMOList) {
                robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.GREEN_CIRCLE,
                        dateTimeFormatConversionUtil.convertStringToDate(holidayDateMO.getCalendarDate()), 20);
            }
        }
        if (leaveCalendarMOList != null) {
            for (EmployeeLeaveCalendar leaveCalendar : leaveCalendarMOList) {
                robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.BLUE_CIRCLE,
                        dateTimeFormatConversionUtil.convertStringToDate(leaveCalendar.getLeaveDate()), 20);
            }
        }
    }

    protected void setRotaTaskList() {
        rotaTaskList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaTaskByDate(dateTimeFormatConversionUtil.getCurrentDate(), true, 0);
        todaysDate = Util.getDate(Calendar.getInstance().getTimeInMillis(), "rotaFragment");
        rotaRecyclerViewAdapter.setRotaData(rotaTaskList);
        if (rotaRecyclerViewAdapter != null) {
            rotaRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        SingletonUnitDetails singletonUnitDetails = SingletonUnitDetails.getInstance();
        singletonUnitDetails.setUnitDetails(rotaTaskList.get(position), position);
        appPreferences.setRotaTaskJsonData(rotaTaskList.get(position));
        updateRotaTaskModelListener.updateRotaTask(rotaTaskList.get(position));
        IOPSApplication.getInstance().getUpdateStatementDBInstance().updatePostBasedOnUnit(((RotaTaskModel) rotaTaskList.get(position)).getUnitId());
        Intent taskDetailsIntent = new Intent(getActivity(), TaskDetailsActivity.class);
        startActivity(taskDetailsIntent);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.rota_add_floatting_button) {
            if (appPreferences.getDutyStatus()) {
                openOnBoardingActivity(OnBoardingFactory.ADD_TASK, 4);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE_FOR_CREATING_NEW_TASK), Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.mothly_grpah_layout) {
            Intent rotaComplianceIntent = new Intent(getActivity(), RotaComplianceActivity.class);
            rotaComplianceIntent.putExtra(getActivity().getResources().getString(R.string.rota_selected_date), selectedRotaDate);
            startActivity(rotaComplianceIntent);
        }
    }

    private void updateRotaListByDateSelected(Date date) {
        todaysDate = Util.getDate(Calendar.getInstance().getTimeInMillis(), "rotaFragment");
        rotaTaskList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaTaskByDate(dateTimeFormatConversionUtil.getDate(date), true, 0);
//        int dateValue = dateTimeFormatConversionUtil.dateComparison(dateTimeFormatConversionUtil.getCurrentDate(), dateTimeFormatConversionUtil.getDate(date));
      /*  if(dateValue == 0 || dateValue < 0 ){
            //
        }
        else {
           rotaTaskList = IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                   .getRotaTaskByPastDate(dateTimeFormatConversionUtil.getDate(date), true, 0);//
        }*/

        rotaRecyclerViewAdapter.setRotaData(rotaTaskList, dateTimeFormatConversionUtil.getDate(date));
        if (rotaRecyclerViewAdapter != null) {
            rotaRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDateSelected(Calendar calendar) {
        updateRotaListByDateSelected(calendar.getTime());
        robotoCalendarView.markDayAsSelectedDay(calendar.getTime());
        dateRotaModel = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRotaAndTaskCount(dateTimeFormatConversionUtil.getDate(calendar.getTime()));
        selectedRotaDate = dateTimeFormatConversionUtil.getDate(calendar.getTime());
        if (!(dateTimeFormatConversionUtil.dateComparison(dateTimeFormatConversionUtil.onCalendarDateSelected(calendar), dateTimeFormatConversionUtil.getCurrentDate()) > 0)) {
         /*   getRotaComplianceApiCall(dateTimeFormatConversionUtil.getDate(calendar.getTime()),
                    dateRotaModel, dateTimeFormatConversionUtil.dateComparison(
                            dateTimeFormatConversionUtil.onCalendarDateSelected(calendar),
                            dateTimeFormatConversionUtil.getCurrentDate()));*/
        } else {
           /* rotaComplianceProgress.setVisibility(View.GONE);
            rotaComplianceTextview.setVisibility(View.GONE);*/
        }

        if (rotaCalendarMOList != null) {
            for (RotaCalendarMO rotaCalendarMO : rotaCalendarMOList) {
                hours = (int) Math.floor(rotaCalendarMO.getTotalTime() / 60);
                minutes = rotaCalendarMO.getTotalTime() % 60;
                if (rotaCalendarMO.getCalendarDate().equalsIgnoreCase(dateTimeFormatConversionUtil.getDate(calendar.getTime()))) {
                    if (hours == 1 || (hours == 0 && minutes != 0)) {
                        viewHeight = 20;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_CIRCLE,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else if ((hours == 2) || (hours == 3)) {
                        viewHeight = 30;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else if ((hours == 4) || (hours == 5)) {
                        viewHeight = 40;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    } else {
                        viewHeight = 50;
                        robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.RED_COLOR_VIEW,
                                dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                    }
                } else {
                    if (hours == 1 || (hours == 0 && minutes != 0)) {
                        viewHeight = 20;
                    } else if ((hours == 2) || (hours == 3)) {
                        viewHeight = 30;
                    } else if ((hours == 4) || (hours == 5)) {
                        viewHeight = 40;
                    } else {
                        viewHeight = 50;
                    }
                    robotoCalendarView.markFirstUnderlineWithStyle(RotaCalendarView.YELLOW_COLOR_VIEW,
                            dateTimeFormatConversionUtil.convertStringToDate(rotaCalendarMO.getEstimatedStartDateTime()), viewHeight);
                }
            }
        }
    }

    @Override
    public void onRightButtonClick(Calendar currentCalendar) {
        newDate = currentCalendar.getTime();
        // Mark current day
        robotoCalendarView.markDayAsCurrentDay(newDate);
        robotoCalendarView.markDayAsSelectedDay(newDate);
        getAllCalendarViewMO(dateTimeFormatConversionUtil.getDateFromCalender(currentCalendar), currentCalendar.get(Calendar.MONTH) + 1, currentCalendar);
        setHolidayAndLeaveToCalendarView();
        setTaskIndicationToCalendarView();
    }

    @Override
    public void onLeftButtonClick(Calendar currentCalendar) {
        newDate = currentCalendar.getTime();
        // Mark current day
        robotoCalendarView.markDayAsCurrentDay(newDate);
        robotoCalendarView.markDayAsSelectedDay(newDate);
        getAllCalendarViewMO(dateTimeFormatConversionUtil.getDateFromCalender(currentCalendar), currentCalendar.get(Calendar.MONTH) + 1, currentCalendar);
        setHolidayAndLeaveToCalendarView();
        setTaskIndicationToCalendarView();
    }

    /*private void showprogressbar() {
        Util.showProgressBar(getActivity(), R.string.loading_please_wait);
    }*/

    private void hideprogressbar() {
        Util.dismissProgressBar(isVisible());
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(getActivity(), OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        rootView = inflater.inflate(R.layout.fragment_rota, container, false);
        rotaFragmentView = rootView;
        init(rootView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        rotaRecyclerViewAdapter = new RotaRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(rotaRecyclerViewAdapter);
        dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        recyclerView.addItemDecoration(dividerItemDecoration);
        rotaRecyclerViewAdapter.setOnRecyclerViewItemClickListener(this);
        monthlyGraphPlayout.setOnClickListener(this);
        rotaAddFlotingActionButton.setOnClickListener(this);

        rotaCalenderView();
//        setRotaTaskList();
        robotoCalendarView.setVisibility(View.GONE);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    robotoCalendarView.setVisibility(View.GONE);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        return rootView;
    }
}