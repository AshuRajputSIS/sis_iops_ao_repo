package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class EmployeeRank {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("EmployeeId")

    public Integer EmployeeId;
    @SerializedName("RankId")

    public Integer RankId;
    @SerializedName("BranchId")

    public Integer BranchId;
    @SerializedName("IsActive")

    public Boolean IsActive;
    @SerializedName("RankName")

    public String RankName;

    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        BranchId = branchId;
    }

    public Integer getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        EmployeeId = employeeId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Integer getRankId() {
        return RankId;
    }

    public void setRankId(Integer rankId) {
        RankId = rankId;
    }

    public String getRankName() {
        return RankName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }
}