package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class RotaModelMO implements Serializable {

    @SerializedName("EmployeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("AreaInspectorName")
    @Expose
    private String areaInspectorName;
    @SerializedName("RotaId")
    @Expose
    private Integer rotaId;
    @SerializedName("RotaWeek")
    @Expose
    private Integer rotaWeek;
    @SerializedName("RotaDate")
    @Expose
    private String rotaDate;
    @SerializedName("RotaStatusId")
    @Expose
    private Integer rotaStatusId;
    @SerializedName("RotaMonth")
    @Expose
    private Integer rotaMonth;
    @SerializedName("RotaPublishedDateTime")
    @Expose
    private String rotaPublishedDateTime;
    @SerializedName("RotaYear")
    @Expose
    private Integer rotaYear;

    /**
     * @return The employeeId
     */
    public Integer getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId The EmployeeId
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return The areaInspectorName
     */
    public String getAreaInspectorName() {
        return areaInspectorName;
    }

    /**
     * @param areaInspectorName The AreaInspectorName
     */
    public void setAreaInspectorName(String areaInspectorName) {
        this.areaInspectorName = areaInspectorName;
    }

    /**
     * @return The rotaId
     */
    public Integer getRotaId() {
        return rotaId;
    }

    /**
     * @param rotaId The RotaId
     */
    public void setRotaId(Integer rotaId) {
        this.rotaId = rotaId;
    }

    /**
     * @return The rotaWeek
     */
    public Integer getRotaWeek() {
        return rotaWeek;
    }

    /**
     * @param rotaWeek The RotaWeek
     */
    public void setRotaWeek(Integer rotaWeek) {
        this.rotaWeek = rotaWeek;
    }

    /**
     * @return The rotaDate
     */
    public String getRotaDate() {
        return rotaDate;
    }

    /**
     * @param rotaDate The RotaDate
     */
    public void setRotaDate(String rotaDate) {
        this.rotaDate = rotaDate;
    }

    /**
     * @return The rotaStatusId
     */
    public Integer getRotaStatusId() {
        return rotaStatusId;
    }

    /**
     * @param rotaStatusId The RotaStatusId
     */
    public void setRotaStatusId(Integer rotaStatusId) {
        this.rotaStatusId = rotaStatusId;
    }

    /**
     * @return The rotaMonth
     */
    public Integer getRotaMonth() {
        return rotaMonth;
    }

    /**
     * @param rotaMonth The RotaMonth
     */
    public void setRotaMonth(Integer rotaMonth) {
        this.rotaMonth = rotaMonth;
    }

    /**
     * @return The rotaPublishedDateTime
     */
    public String getRotaPublishedDateTime() {
        return rotaPublishedDateTime;
    }

    /**
     * @param rotaPublishedDateTime The RotaPublishedDateTime
     */
    public void setRotaPublishedDateTime(String rotaPublishedDateTime) {
        this.rotaPublishedDateTime = rotaPublishedDateTime;
    }

    /**
     * @return The rotaYear
     */
    public Integer getRotaYear() {
        return rotaYear;
    }

    /**
     * @param rotaYear The RotaYear
     */
    public void setRotaYear(Integer rotaYear) {
        this.rotaYear = rotaYear;
    }

}
