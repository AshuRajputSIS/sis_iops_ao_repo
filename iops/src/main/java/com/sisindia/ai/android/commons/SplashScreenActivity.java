package com.sisindia.ai.android.commons;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Window;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appupdate.AppUpdateListener;
import com.sisindia.ai.android.appupdate.AppUpdateTask;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.FcmGuardModelInsertion;
import com.sisindia.ai.android.fcmnotification.GuardForBranchBase;
import com.sisindia.ai.android.home.LanguageActivity;
import com.sisindia.ai.android.network.CCRQuestionRequestMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.UnitTypeRequestMO;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.network.response.AppInfo;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.io.File;
import java.net.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;

import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;


public class SplashScreenActivity extends BaseActivity implements AppUpdateListener {

    //@Bind(R.id.tv_version_name)
    CustomFontTextview tvVersionName;
    //@Bind(R.id.tv_version_code)
    CustomFontTextview tvVersionCode;

    private int STORAGE_PERMISSION_CODE = 23;
    private AppInfo appUpdateInfo;
    private static int SPLASH_TIME_OUT = 3000;
    private static String MARKET_PLACE_URL = "market://details?id=com.google.android.gms";
    private static String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.google.android.gms";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private UpdatedConfigurationApiCalls updatedConfigurationApiCalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        tvVersionName = (CustomFontTextview) findViewById(R.id.tv_version_name);
        tvVersionCode = (CustomFontTextview) findViewById(R.id.tv_version_code);

        tvVersionName.setText(String.valueOf(BuildConfig.VERSION_NAME));
        tvVersionCode.setText(String.valueOf(BuildConfig.VERSION_CODE));
        appPreferences.setSplashScreenFlag(true);
        util = new Util();
       /* if (!appPreferences.isFirstTimeAlarmConfigured()) {
            Util.setAlarmManager(this, Constants.DELETE_SCHEDULE);
            appPreferences.setFirstTimeAlarmConfigured(true);
        }*/

        if (!appPreferences.isFirstTimeAlarmConfiguredForAttendance()) {
            Util.setAlarmManager(this, Constants.ATTENDANCE_SCHEDULE);
            appPreferences.setFirstTimeAlarmConfiguredForAttendance(true);
        }

        // Getting status
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getBaseContext());
        if (!TextUtils.isEmpty(appPreferences.getAccessToken())) {
            updatedConfigureData();
        }
        if (status == ConnectionResult.SUCCESS) {
            new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                @Override
                public void run() {
                    SISClient sisClient = new SISClient(SplashScreenActivity.this);
                    sisClient.getApi().getLatestAppVersionInfo(new Callback<ApiResponse<AppInfo>>() {
                        @Override
                        public void success(ApiResponse<AppInfo> appInfoApiResponse, Response response) {

                            // CHECKING AND SETTING THE GRIEVANCE FLAG FROM API WHETHER FORCE GRIEVANCE IS REQUIRED OR NOT
                            try {
                                boolean isForcedGrievanceRequired = false;
                                if (appInfoApiResponse.data.isGrievance == 1)
                                    isForcedGrievanceRequired = true;

                                appPreferences.saveForcedGrievanceFlag(isForcedGrievanceRequired);

                            } catch (Exception e) {
                                Crashlytics.logException(e);
                                appPreferences.saveForcedGrievanceFlag(false);
                            }

                            int currentVersionNo = BuildConfig.VERSION_CODE;
                            if (appInfoApiResponse != null && appInfoApiResponse.data != null && appInfoApiResponse.data.versionNumber > currentVersionNo) {
                                appUpdateInfo = appInfoApiResponse.data;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                    requestStoragePermission();
                                else
                                    showDownloadPrompt(appUpdateInfo);
                            } else {
                                Crashlytics.log("No New App Update Available");
                                continueToNextPage();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Crashlytics.logException(error);
                            continueToNextPage();
                        }
                    });
                }
            }, SPLASH_TIME_OUT);
        } else {
            updateGooglePlayServices();
        }

        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // INITIALIZING USER NAME AND IDENTIFIER TO FIND CRASH AGAINST PARTICULAR USER W.R.T BELOW TAGS
        try {
            Crashlytics.setUserName(appPreferences.getAppUser());
            Crashlytics.setUserIdentifier(appPreferences.getMobileNumber());
        } catch (Exception e) {
        }
    }

    private void continueToNextPage() {
        Intent intent;
        if (appPreferences.getloginstatus()) {
            openOnBoardingActivity(OnBoardingFactory.POST_LOGIN, 4);
        } else {
            if (appPreferences.getLanguageId() == Constants.ENGLISH_LANGUAGE_ID || appPreferences.getLanguageId() == Constants.HINDI_LANGUAGE_ID) {
                openOnBoardingActivity(OnBoardingFactory.REGISTRATION, 2);
            } else {
                String commonImagePath = Constants.IOPS_IMAGES;
                File file = new File(commonImagePath);
                file.mkdirs();
                appPreferences.setCommonImagePath(commonImagePath);
                intent = new Intent(SplashScreenActivity.this, LanguageActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void updatedConfigureData() {
        updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
        if (updatedConfigurationApiCalls != null) {

//            if (!updatedConfigurationApiCalls.isUpdatedMasterActionPlan || !appPreferences.isExecutingSecondTimeActionPlanAPI())

            /*if (!updatedConfigurationApiCalls.isUpdatedMasterActionPlan)
                getMasterActionPlanApiCall();
            if (!updatedConfigurationApiCalls.isUpdatedClientCoordinationIssue)
                getClientCoordinationApiCall();
            if (!updatedConfigurationApiCalls.isUpdateSecurityRiskOptions)
                getSecurityRiskOptionsApiCall();
            if (!updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestions)
                getBarrackInspectionQuestions();
            if (!updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestionAndOptions)
                getBarrackInspectionQuestionsAndOptions();
            if (!updatedConfigurationApiCalls.isDeltedImprovementPlanForThisBuild)
                deleteImprovementPlanRecords();*/


            /*if (!appPreferences.isCCRQuestionsUpdated())
                getCCRQuestionsAndUpdate();
            if (!appPreferences.isMasterActionPlanUpdated())
                getMasterActionPlanApiCall();
            if (!appPreferences.isCCRIssueMatrixUpdated())
                getClientCoordinationApiCall();*/

            if (!appPreferences.isGuardDetailsInserted())
                guardInformationUpdate();
            if (!appPreferences.isUnitTypeTableSynced())
                getUnitTypeTableData();
        }
    }

    public void updateGooglePlayServices() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
        alertDialogBuilder.setTitle(R.string.update_google_play_title);
        alertDialogBuilder
                .setMessage(R.string.update_google_play_message)
                .setCancelable(false)
                .setPositiveButton(R.string.update_lbl,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                launchGooglePlay();
                                finish();
                            }
                        });
        alertDialogBuilder.show();
    }

    public void launchGooglePlay() {
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_PLACE_URL)), 1);
        } catch (ActivityNotFoundException anfe) {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_URL)), 1);
        }
    }

    //We are calling this method to check the permission status
    private boolean isReadWriteStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (isReadWriteStorageAllowed()) {
            showDownloadPrompt(appUpdateInfo);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            new AlertDialog.Builder(this)
                    .setMessage(R.string.request_permission_message)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(SplashScreenActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_CODE);
        }
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showDownloadPrompt(appUpdateInfo);
                Timber.d("Storage Permission Granted");
            } else {
                Toast.makeText(this, R.string.permission_denied_message, Toast.LENGTH_LONG).show();
                requestStoragePermission();
            }
        }
    }

    /**
     * APP UPDATE LOGIC
     */
    private void showDownloadPrompt(final AppInfo appInfo) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.new_version_title)
                .setMessage(R.string.new_version_message)
                .setCancelable(false)
                .setPositiveButton(R.string.update_lbl, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDownload(appInfo.downloadUrl);

                    }
                })
                .setNegativeButton(R.string.skip_lbl, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        continueToNextPage();
                    }
                })
                .show();
    }

    private void startDownload(String downloadUrl) {
        if (isInternetConnected()) {
            ProgressDialog barProgressDialog = new ProgressDialog(this);
            barProgressDialog.setTitle(getString(R.string.update_progress_title));
            barProgressDialog.setMessage(getString(R.string.update_progress_message));
            barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            barProgressDialog.setProgress(0);
            barProgressDialog.setMax(100);
            barProgressDialog.setCanceledOnTouchOutside(false);
            barProgressDialog.setCancelable(false);
            barProgressDialog.show();
            AppUpdateTask updateApp = new AppUpdateTask();
            updateApp.setup(this, barProgressDialog);
            updateApp.execute(downloadUrl);
        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDownloadSuccess() {
//        unitBarrackIdUpdate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            File apkFile = new File(Constants.NEW_APK_PATH);
            Intent i = new Intent(Intent.ACTION_VIEW, FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", apkFile));
            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(i);
        } else {
            File apkFile = new File(Constants.NEW_APK_PATH);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onDownloadFailure(String message) {
//        unitBarrackIdUpdate();
        Crashlytics.logException(new Exception(message));
        Toast.makeText(this, R.string.app_update_failure_msg, Toast.LENGTH_SHORT).show();
        continueToNextPage();
    }

    public Boolean isInternetConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(SplashScreenActivity.this, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (client != null)
                client.connect();
        } catch (Exception e) {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (client != null)
                client.disconnect();
        } catch (Exception e) {
        }
    }

    /*public void getMasterActionPlanApiCall() {
        if (isInternetConnected()) {
            sisClient.getApi().getMasterActionPlan(new Callback<MasterActionPlanBaseOR>() {
                @Override
                public void success(MasterActionPlanBaseOR masterActionPlanBaseOR, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != masterActionPlanBaseOR && null != masterActionPlanBaseOR.masterActionPlanModelList
                                && masterActionPlanBaseOR.masterActionPlanModelList.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                    deleteSpecifiedkTable(TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE, 0);

                            IOPSApplication.getInstance().getInsertionInstance().insertActionPlanToTable(masterActionPlanBaseOR.masterActionPlanModelList);
                            appPreferences.updateMasterActionPlanAPIFlag(true);

                            *//*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                            updatedConfigurationApiCalls.isUpdatedMasterActionPlan = true;
                            appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*//*
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    *//*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                    updatedConfigurationApiCalls.isUpdatedMasterActionPlan = false;
                    appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*//*

                    Crashlytics.logException(error);
                }
            });
        }
    }*/

   /* private void deleteImprovementPlanRecords() {
        updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
        updatedConfigurationApiCalls.isDeltedImprovementPlanForThisBuild =
                IOPSApplication.getInstance().getDeletionStatementDBInstance()
                        .deleteSpecifiedkTable(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, 0);
        appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
    }

    public void getSecurityRiskOptionsApiCall() {
        if (isInternetConnected()) {
            sisClient.getApi().getSecurityActionPlan(new Callback<SecurityRiskOptionsBaseOR>() {
                @Override
                public void success(SecurityRiskOptionsBaseOR securityRiskOptionsBaseOR, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (securityRiskOptionsBaseOR != null) {
                            if (null != securityRiskOptionsBaseOR.securityRiskQuestionOptionMappingMoList && securityRiskOptionsBaseOR.securityRiskQuestionOptionMappingMoList.size() != 0) {
                                IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                        .deleteSpecifiedkTable(TableNameAndColumnStatement.SECURITY_RISK_QUESTION_OPTIONS_TABLE, 0);
                                IOPSApplication.getInstance().getInsertionInstance().insertSecurityRiskQuestionOptionsToTable(securityRiskOptionsBaseOR.securityRiskQuestionOptionMappingMoList);
                                updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                                updatedConfigurationApiCalls.isUpdateSecurityRiskOptions = true;
                                appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                    updatedConfigurationApiCalls.isUpdateSecurityRiskOptions = false;
                    appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                    Crashlytics.logException(error);
                }
            });
        }
        return;
    }

    public void getClientCoordinationApiCall() {
        if (isInternetConnected()) {
            sisClient.getApi().getClientCoordinationIssueMatrix(new Callback<ClientCoordinationIssueMatrixBaseOR>() {
                @Override
                public void success(ClientCoordinationIssueMatrixBaseOR clientCoordinationIssueMatrixBaseOR, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != clientCoordinationIssueMatrixBaseOR && null != clientCoordinationIssueMatrixBaseOR.clientCoordinationIssueMatricData
                                && clientCoordinationIssueMatrixBaseOR.clientCoordinationIssueMatricData.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                    .deleteSpecifiedkTable(TableNameAndColumnStatement.ISSUE_MATRIX_TABLE, 3);
                            IOPSApplication.getInstance().getUpdateStatementDBInstance().insertClientCoordinationIssueMatrixTable(clientCoordinationIssueMatrixBaseOR.clientCoordinationIssueMatricData);

                            appPreferences.updateCCRIssueMatrixAPIFlag(true);

                            *//*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                            updatedConfigurationApiCalls.isUpdatedClientCoordinationIssue = true;
                            appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*//*
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    *//*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                    updatedConfigurationApiCalls.isUpdatedClientCoordinationIssue = false;
                    appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*//*
                    Crashlytics.logException(error);
                }
            });
        }
    }

    public void getBarrackInspectionQuestions() {
        if (isInternetConnected()) {
            sisClient.getApi().getBarrackQuestionList(new Callback<BarrackQuestionBaseOR>() {
                @Override
                public void success(BarrackQuestionBaseOR barrackQuestionBaseOR, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != barrackQuestionBaseOR && null != barrackQuestionBaseOR.barrackInspectionQuestions
                                && barrackQuestionBaseOR.barrackInspectionQuestions.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                    .deleteSpecifiedkTable(TableNameAndColumnStatement.BARRACK_INSPECTION_QUESTION_TABLE, 0);
                            IOPSApplication.getInstance().getInsertionInstance().insertBarrackInspectionQuestionTable(barrackQuestionBaseOR.barrackInspectionQuestions);
                            updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                            updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestions = true;
                            appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                    updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestions = false;
                    appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                    Crashlytics.logException(error);
                }
            });
        }
    }

    public void getBarrackInspectionQuestionsAndOptions() {
        if (isInternetConnected()) {
            sisClient.getApi().getBarrackQuestionOptionList(new Callback<BarrackQuestionsAndOptionsBaseOR>() {
                @Override
                public void success(BarrackQuestionsAndOptionsBaseOR barrackQuestionsAndOptionsBaseOR, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != barrackQuestionsAndOptionsBaseOR && null != barrackQuestionsAndOptionsBaseOR.barrackQuestioonOptionsList
                                && barrackQuestionsAndOptionsBaseOR.barrackQuestioonOptionsList.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                    .deleteSpecifiedkTable(TableNameAndColumnStatement.BARRACK_INSPECTION_ANSWER_TABLE, 0);
                            IOPSApplication.getInstance().getInsertionInstance()
                                    .insertBarrackInspectionAnswerTable(barrackQuestionsAndOptionsBaseOR.barrackQuestioonOptionsList);
                            updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                            updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestionAndOptions = true;
                            appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                        }

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                    updatedConfigurationApiCalls.isUpdatedBarrackInspectionQuestionAndOptions = false;
                    appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);
                    Crashlytics.logException(error);
                }
            });
        }
    }

    private void unitBarrackIdUpdate() {
        if (isInternetConnected()) {
            sisClient.getApi().getUnitBarracks(new Callback<UnitBarrackBaseMO>() {
                @Override
                public void success(UnitBarrackBaseMO unitBarrackBaseMO, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (unitBarrackBaseMO != null) {
                            if (null != unitBarrackBaseMO && null != unitBarrackBaseMO.getUnitBarrackList()) {
                                FCMUnitBarrackModelInsertion fcmUnitBarrackModelInsertion = new FCMUnitBarrackModelInsertion(IOPSApplication.getInstance());
                                fcmUnitBarrackModelInsertion.insertUnitBarrack(unitBarrackBaseMO.getUnitBarrackList());
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Util.printRetorfitError("getUnitBarracks", error);
                    Crashlytics.logException(error);
                }
            });
        } else {
            Util.showToast(this, getResources().getString(R.string.no_internet));
        }
    }*/


    //@Ashu : Writing method to update guard related information from API to Local DB
    private void guardInformationUpdate() {
        if (isInternetConnected()) {
            try {
                int branchId = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails().getBranchId();
                sisClient.getApi().getBranchGuardsInformation(branchId, new Callback<GuardForBranchBase>() {
                    @Override
                    public void success(GuardForBranchBase guardForBranchBase, Response response) {
                        if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                            if (null != guardForBranchBase && guardForBranchBase.statusCode == HttpURLConnection.HTTP_OK && null != guardForBranchBase.data) {
                                FcmGuardModelInsertion fcmGuardModelInsertion = new FcmGuardModelInsertion(SplashScreenActivity.this);
                                boolean insertedSuccessfully = fcmGuardModelInsertion.insertGuardInfo(guardForBranchBase.data.guardsForBranch);

                                if (insertedSuccessfully)
                                    appPreferences.updateGuardDetailsInsertedFlag(true);

                                /*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                                updatedConfigurationApiCalls.isGuardInformationUpdated = insertedSuccessfully;
                                appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*/
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        /*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                        updatedConfigurationApiCalls.isGuardInformationUpdated = false;
                        appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*/

                        appPreferences.updateGuardDetailsInsertedFlag(false);
                        Crashlytics.logException(error);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Util.showToast(this, getResources().getString(R.string.no_internet));
        }
    }

    public void getCCRQuestionsAndUpdate() {
        if (isInternetConnected()) {
            sisClient.getApi().getCCRQuestions(new Callback<CCRQuestionRequestMO>() {
                @Override
                public void success(CCRQuestionRequestMO ccrQuestionRequestMO, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != ccrQuestionRequestMO && null != ccrQuestionRequestMO.clientCoordinationQuestions &&
                                ccrQuestionRequestMO.clientCoordinationQuestions.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                    .deleteSpecifiedkTable(TableNameAndColumnStatement.client_coordination_table, 0);
                            if (IOPSApplication.getInstance().getInsertionInstance().insertIntoClientCoordinationTable(ccrQuestionRequestMO.clientCoordinationQuestions))
                                appPreferences.updateCCRQuestionAPIFlag(true);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Crashlytics.logException(error);
                }
            });
        }
    }

    public void getUnitTypeTableData() {
        if (isInternetConnected()) {
            sisClient.getApi().getUnitTypeList(new Callback<UnitTypeRequestMO>() {
                @Override
                public void success(UnitTypeRequestMO unitTypeReqMO, Response response) {
                    if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                        if (null != unitTypeReqMO && null != unitTypeReqMO.unitTypeDataMO && unitTypeReqMO.unitTypeDataMO.size() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                    deleteSpecifiedkTable(TableNameAndColumnStatement.UNIT_TYPE_TABLE, 0);

                            IOPSApplication.getInstance().getInsertionInstance().insertUnitTypeDetails(unitTypeReqMO.unitTypeDataMO);
                            appPreferences.updateUnitTypeTableSyncedFlag(true);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Crashlytics.logException(error);
                }
            });
        }
    }

}