package com.sisindia.ai.android.database;

/**
 * Created by shankar on 17/10/16.
 */

public class TableNameAndColumnStatement {


    public static final String IS_ALL_DEPENDENT_MODULES_SYNCED = "is_all_dependent_modules_synced";
    public static final String CREATED_OR_EDITED = "created_or_edited";
    public static final String IS_UNIT_EQUIPMENT_SYNCED = "is_unit_equipment_synced";
    public static final String TASK_SUBMITED_LOCATION = "task_submited_location";
    public static final String NOTIFICATION_LIST = "notification_list";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String TITLE = "title";
    public static final String RECEIVED_DATE_TIME = "received_date_time";
    public static final String UNIT_LIST = "unit_list";
    public static final String IS_DELETED = "is_deleted";
    public static final String PRIORITY = "priority";
    public static final String DELETED_ID = "delete_id";
    public static final String MESSAGE = "message";
    public static final String SYNC_START_DATE_TIME = "sync_start_date_time";
    public static final String SYNC_REQUEST = "sync_request";
    public static final String IS_MANDATORY = "is_mandatory";
    public static final String BARRACK_CUSTODIAN = "Barrack Custodian";
    public static final String BARRACK_LANDLORD = "Barrack Landlord";
    public static final String UNIT_RAISING = "unit_raising";
    public static final String UNIT_RAISING_DEFERRED = "unit_raising_deferred_detail";
    public static final String UNIT_RAISING_CANCELLED = "unit_raising_cancellation";
    public static final String UNIT_RAISING_STRENGTH_DETAIL = "unit_raising_strength_detail";
    public static final String UNIT_RAISING_ID = "unit_raising_id";
    public static final String RAISING_DETAILS = "raising_details";
    public static final String PLANED_RAISING_DATE = "planed_raising_date";
    public static final String UNIT_RAISING_DEFERRED_ID = "unit_raising_deferred_id";
    public static final String DEFERRED_REASON = "deferred_reason";
    public static final String NEW_RAISING_DATE_TIME = "new_raising_date_time";
    public static final String UNIT_RAISING_CANCELLATION_ID = "unit_raising_cancelled_id";
    public static final String CANCELLATION_REASON = "cancellation_reason";
    public static final String IS_SYNC_REQUESTS_STARTED = "is_sync_requests_started";
    public static final String EXPECTED_RAISING_DATE_TIME = "expected_raising_date_time";
    public static final String RAISING_STATUS = "raising_status";
    public static final String UNIT_RAISING_LOCATION = "unit_raising_location";
    public static final String ROW_ID = "row_id";
    public static final String IS_UNIT_RAISING_STRENGTH_SYNCED = "is_unit_raising_strength_synced";
    public static final String UNIT_RAISING_LOCAL_ID = "unit_raising_local_id";
    public static final String UNIT_RAISING_DEFERRED_LOCAL_ID = "unit_raising_deferred_local_id";
    public static final Object UNIT_RAISING_CANCELLATION_LOCAL_ID = "unit_raising_cancellation_local_id";
    public static final String UNIT_BARRACK_ID = "unit_barrack_id";
    public static final String SERVER_FILE_PATH = "server_file_path";
    public static final String GUARDS_TABLE = "guards";
    public static final String UNIT_TYPE_TABLE = "unit_type";
    public static final String FINE_INSTANCE = "fine_instances";
    public static final String QUERY_ID = "query_id";
    public static final String NFC_EXEMPTED = "nfc_exempted";
    public static final String FINE_AMOUNT = "fine_amount";
    public static final String REWARD_AMOUNT = "reward_amount";
    public static final String LAST_FINE = "last_fine";
    public static final String BARRACK = "barrack";
    public static String UnitRaisingChecklist = "UnitRaisingChecklist";
    public int taskTypeId;


    public static final String GUARD_ADD_KIT_REQUEST_JSON_DATA = "guard_add_kit_request";
    public static final String ADHOC_STATUS = "adhoc_status";
    public static final String IS_UNITSTENGTH_SYNCED = "is_unstrength_synced";
    public static final String IS_ISSUES_SYNCED = "is_issues_synced";
    public static final String IS_KITREQUESTED_SYNCED = "is_kitrequested_synced";
    public static final String IS_IMPROVEMNTPLANS_SYNCED = "is_improvementplan_synced";
    public static final String IS_METADATA_SYNCED = "is_metadata_synced";
    public static final String IS_IMAGES_SYNCED = "is_images_synced";


    public static String APP_USER_TABLE = "app_user";
    public static String UNIT_TABLE = "unit";
    public static String GRIEVANCE_TABLE = "grievance";
    public static String AREA_TABLE = "area";
    public static String UNIT_RISK_TABLE = "unit_risk";
    public static String TASK_TABLE = "task";
    public static String COMPLAINT_TABLE = "complaint";
    public static String BRANCH_TABLE = "branch";
    public static String ROTA_TABLE = "rota";
    public static String UNIT_BARRACK_TABLE = "unit_barrack";
    public static String APP_USER_ATTENDANCE_TABLE = "app_user_attendance";
    public static String BARRACK_TABLE = "barrack";
    public static String UNIT_POST_TABLE = "unit_post";
    public static String ROTA_TASK_TABLE = "rota_task";
    public static String IMPROVEMENT_PLAN_TABLE = "improvement_plan";
    public static String UNIT_EMPLOYEE_TABLE = "unit_employee";
    public static String MESS_VENDOR_TABLE = "mess_vendor";
    public static String RANK_TABLE = "rank";
    public static String RECRUITMENT_TABLE = "recruitment";
    public static String UNIT_BARRACK_AUTHORIZED_TABLE = "unit_barrack_authorized";
    public static String UNIT_BARRACK_AUTHORIZED_STRENGTH_TABLE = "unit_barrack_authorized_strength";
    public static String UNIT_EQUIPMENT_TABLE = "unit_equipment";
    public static String ATTACHMENT_METADATA_TABLE = "attachment_metadata";
    public static String LEAVE_CALENDAR_TABLE = "leave_calendar";
    public static String CUSTOMER_TABLE = "customer";
    public static String UNIT_CONTACT_TABLE = "unit_contact";
    public static String HOLIDAY_CALENDAR_TABLE = "holiday_calendar";
    public static String GPS_BATTERY_HEALTH_TABLE = "gps_battery_health";
    public static String GUARD_PERFORMANCE_TABLE = "guard_performance";
    public static String KIT_DISTRIBUTION_TABLE = "kit_distribution";
    public static String KIT_DISTRIBUTION_EMPLOYEE_TABLE = "kit_distribution_employee";
    public static String UNIT_BARRACK_ACTUAL_STATUS_TABLE = "unit_barrack_actual_status";
    public static String UNIT_BARRACK_ACTUAL_STRENGTH_TABLE = "unit_barrack_actual_strength";
    public static String APP_USER_ACTIVITY_LOG_TABLE = "app_user_activity_log";
    public static String UNIT_RISK_ACTION_PLAN_TABLE = "unit_risk_action_plan";
    public static String UNIT_BARRACK_STRENGTH_TABLE = "unit_barrack_strength";
    public static String ROTA_TASK_ACTIVITY_LIST = "rota_task_activity_list";
    public static String RANK_MASTER_TABLE = "rank_master";
    public static String UNIT_AUTHORIZED_STRENGHT_TABLE = "unit_authorized_strength";
    public static String SECURITY_RISK_QUESTION_TABLE = "security_risk_question";
    public static String SECURITY_RISK_QUESTION_OPTIONS_TABLE = "security_risk_question_option";
    public static String MY_PERFORMANCE_TABLE = "my_performance";
    public static String UnitBillingStatusTable = "bill_collection";
    public static String NOTIFICATION_RECEIPT = "notification_receipt";
    public static String DEPENDENT_SYNC_FLOW = "dependent_sync";


    //UnitBillingStatusTable -Columns names -->
    public static String Bill_COLLACTION_ID = "id";
    public static String Bill_COLLACTION_UNIT_ID = "unit_id";
    public static String Bill_COLLACTION_UNIT_NAME = "unit_name";
    public static String Bill_NUMBER = "bill_number";
    public static String Bill_MONTH = "bill_month";
    public static String Bill_YEAR = "bill_year";
    public static String OUTSTANDING_AMOUNT = "outstanding_amount";
    public static String OUTSTANDING_DAYS = "outstanding_days";
    public static String UPDATED_DATES = "updated_dates";
    public static String IS_BILL_COLLECTED = "is_bill_collected";

    public static String PERFORMANCE_ID = "id";
    public static String PERFORMANCE_ACTIVITY = "activity";
    public static String PERFORMANCE_LOCATION = "location";
    public static String PERFORMANCE_TASK_ID = "task_id";
    public static String PERFORMANCE_TASK_NAME = "task_name";
    public static String TASK_STATUS = "task_status";
    public static String IS_TASK_STARTED = "is_task_started";

    public static String DATE = "date";
    public static String START_TIME = "start_time";
    public static String END_TIME = "end_time";

    public static String ID = "id";
    public static String EMPLOYEE_ID = "employee_id";
    public static String EMPLOYEE_NO = "employee_no";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String MIDDLE_NAME = "middle_name";
    public static String ORGANIZATION_ID = "OrganizationId";
    public static String IS_ACTIVE = "IsActive";
    public static String PRIMARY_ADDRESS_ID = "PrimaryAddressId";
    public static String RESIDENTIAL_ADDRESS_ID = "ResidentialAddressId";
    public static String ADMIN_CONTROLLER_ID = "AdminControllerId";
    public static String IS_REFERRAL = "IsReferral";
    public static String REFERRAL_ID = "ReferralId";
    public static String REFERRAL_SOURCE = "ReferralSource";
    public static String EMPLOYEE_PROFILE_PIC_ID = "EmployeeProfilePicId";
    public static String MINIATURE_PROFILE_PIC_ID = "MiniatureProfilePicId";
    public static String COMPANY_CONTACT_NUMBER = "CompanyContactNumber";
    public static String FUNCTIONAL_CONTROLLER_ID = "FunctionalControllerId";
    public static String EMPLOYEE_SIGNATURE_ID = "EmployeeSignatureId";
    public static String REGION_ID = "RegionId";
    public static String IS_DEFAULT_PASSWORD = "IsDefaultPassword";
    public static String RANK = "rank";
    public static String DATE_OF_BIRTH = "date_of_birth";
    public static String DATE_OF_JOINING = "date_of_joining";
    public static String EMERGENCY_CONTACT_NO = "emergency_contact_no";
    public static String ALTERNATE_CONTACT_NO = "AlternateContactNo";
    public static String BRANCH_ID = "branch_id";
    public static String APP_ROLE = "app_role";
    public static String EMAIL_ADDRESS = "EmailAddress";
    public static String PREFERRED_LANGUAGE_ID = "preferred_language_id";
    public static String FULL_ADDRESS = "full_address";
    public static String GEO_LATITUDE = "geo_latitude";
    public static String GEO_LONGITUDE = "geo_longitude";
    public static String POST_STATUS = "post_status";

    public static String IMEI1 = "imei1";
    public static String IMEI2 = "imei2";
    public static String FK_APP_USER_BRANCH = "fk_app_user_branch";
    public static String UQ_EMPLOYEE_NO_APP_USER = "uq_employee_no_app_user";
    public static String UQ_EMPLOYEE_ID_APP_USER = "uq_employee_id_app_user";
    public static String action_filter = "FILTER";
    public static String SINGLE_UNIT_TITLE = "Unit Name";

  /*<!--APP_USER_ACTIVITY_LOG_TABLE -Columns names -->*/

    public static String APP_USER_ID = "app_user_id";
    public static String ACTIVITY_LOG_DATE = "activity_log_date";
    public static String CHECK_IN_DATETIME = "check_in_date_time";
    public static String CHECK_OUT_DATETIME = "check_out_date_time";
    public static String IS_NEW = "is_new";
    public static String IS_SYNCED = "is_synced";
    public static String FK_APP_USER_ACTIVITY_LOG_APP_USER = "fk_app_user_activity_log_app_user";

 /* <!--APP_USER_Attendance_TABLE -Columns names -->*/

    public static String DUTY_START_DATETIME = "duty_start_date_time";
    public static String DUTY_END_DATETIME = "duty_end_date_time";
    public static String IS_NIGHT_SHIFT = "is_night_shift";
    public static String CURRENT_WEEK = "current_week";
    public static String FK_APP_USER_ATTENDANCE = "fk_app_user_attendance";

/*
 <!--Area_TABLE -Columns names -->
*/


    public static String AREA_CODE = "area_code";
    public static String AREA_NAME = "area_name";
    public static String FK_AREA_BRANCH = "fk_area_branch";
    public static String UQ_AREA_ID_AREA = "uq_area_id_area";


  /*<!--Attachment_MetaDate_TABLE -Columns names -->*/

    public static String FILE_NAME = "file_name";
    public static String FILE_SIZE = "file_size";
    public static String FILE_EXTENSION = "file_extension";
    public static String IS_START_SYNC = "is_synced";
    public static String IS_END_SYNC = "is_end_sync";
    public static String ATTACHMENT_REFREENCE_TYPE = "attachment_reference_type";
    public static String ATTACHMENT_REFRENCE_ID = "attachment_reference_id";
    public static String ATTACHMENT_SOURCE_CODE = "attachment_source_code";
    public static String ATTACHMENT_ID = "attachment_id";
    public static String FILE_PATH = "file_path";
    public static String IS_FILE_Upload = "is_file_upload";
    public static String SEQUENCE_NO = "sequence_no";


 /* <!--Branch_TABLE -Columns names -->*/

    public static String BRANCH_NAME = "branch_name";
    public static String BRANCH_CODE = "branch_code";
    public static String BRANCH_FULL_ADDRESS = "branch_full_address";
    public static String UQ_BRANCH_ID_BRANCH = "uq_branch_id_branch";


  /*<!--complaint_TABLE -Columns names -->*/

    public static String ISSUE_ID = "issue_id";
    public static String UNIT_ID = "unit_id";
    public static String COMPLAINT_MODE_ID = "complaint_mode_id";
    public static String COMPLAINT_NATURE_ID = "complaint_nature_id";
    public static String COMPLAINT_CAUSE_ID = "complaint_cause_id";
    public static String ACTION_PLAN_ID = "action_plan_id";
    public static String REMARKS = "remarks";
    public static String ASSIGNED_TO = "assigned_to";
    public static String COMPLAINT_STATUS_ID = "complaint_status_id";
    public static String CREATED_AT = "created_at";
    public static String CLIENT_REMARKS = "client_remarks";
    public static String CLOSED_AT = "closed_at";
    public static String TARGET_EMPLOYEE_ID = "target_employee_id";


    public static String IMPROVEMENT_PLAN_ID = "improvement_plan_id";
    public static String FK_COMPLAINT_UNIT = "fk_complaint_unit";

  /*<!--GPS_BATTERY_HEALTH_TABLE -Columns names -->*/

    public static String GPS_LATITUDE = "gps_latitude";
    public static String GPS_LONGITUDE = "gps_longitude";
    public static String BATTERY_PERCENTAGE = "battery_percentage";
    public static String LOGGED_IN_DATETIME = "logged_in_date_time";
    public static String IS_SYNC_END = "is_sync_end";
    public static String IS_SYNC_START = "is_sync_start";


 /* <!--Grievance_TABLE -Columns names -->*/

    public static String BARRACK_ID = "barrack_id";
    public static String GRIEVANCE_NATURE_ID = "grievance_nature_id";
    public static String DESCRIPTIONS = "Description";
    public static String GRIEVANCE_STATUS_ID = "grievance_status_id";
    public static String TASK_ID = "task_id";
    public static String FK_GRIEVANCE_UNIT = "fk_grievance_unit";
    public static String FK_GRIEVANCE_BARRACK = "fk_grievance_barrack";
    public static String RAISED_BY = "raised_by";


  /*<!--Guard_Performance_TABLE -Columns names -->*/

    public static String GUARD_ID = "guard_id";
    public static String RECOMMEND_REWARD = "recommend_reward";
    public static String RECOMMEND_FINE = "recommend_fine";
    public static String AMOUNT = "amount";

    public static String REASON_ID = "reason_id";
    public static String PERFORMANCE_RATING = "performance_rating";
    public static String FK_GUARD_PERFORMANCE_TASK = "fk_guard_performance_task";
    public static String FK_GUARD_PERFORMANCE_EMPLOYEE = "fk_guard_performance_employee";


 /* <!--holiday_calendar_TABLE -Columns names -->*/

    public static String HOLIDAY_DATE = "holiday_date";
    public static String HOLIDAY_DESCRIPTION = "holiday_description";
    public static String HOLIDAY_ID = "holiday_id";
    public static String IS_REGIONAL_HOLIDAY = "IsRegionalHoliday";
    public static String FK_HOLIDAY_CALENDAR_BRANCH = "fk_holiday_calendar_branch";
    public static String FK_HOLIDAY_ID_HOLIDAY_CALENDAR = "fk_holiday_id_holiday_calendar";


  /*<!--Improvement_TABLE -Columns names -->*/

    public static String IMPROVEMENT_PLAN_TYPE_ID = "improvement_plan_type_id";
    public static String COMPLAINT_ID = "complaint_id";
    public static String BUDGET = "budget";
    public static String IMPROVEMENT_PLAN_STATUS_ID = "improvement_plan_status_id";
    public static String FK_IMPROVEMENT_PLAN_UNIT = "fk_improvement_plan_unit";
    public static String FK_IMPROVEMENT_PLAN_BARRACK = "fk_improvement_plan_barrack";


 /* <!--kit_TABLE -Columns names -->*/

    public static String KIT_ID = "kit_id";
    public static String DELIVERY_DATE = "delivery_date";
    public static String ISSUING_OFFICER_ID = "issuing_officer_id";
    public static String ISSUING_OFFICER_NAME = "issuing_officer_name";
    public static String KIT_DELIVERY_STATUS = "kit_delivery_status";
    public static String UQ_KIT_UNIT = "uq_kit_unit";

 /* <!--Kit_Distribution_Employee_Table -Columns names -->*/

    public static String KIT_ITEM_ID = "kit_item_id";
    public static String KIT_DISTRIBUTION_ID = "kit_distribution_id";
    public static String ORDER_NO = "OrderNo";
    public static String RECIPIENT_ID = "recipient_id";
    public static String KIT_ISSUE_TYPE_ID = "kit_issue_type_id";
    public static String IS_ISSUED = "is_issued";
    public static String NON_ISSUE_REASON_ID = "non_issue_reason_id";
    public static String ISSUE_DATE = "issued_date";
    public static String IS_UNPAID = "is_unpaid";
    public static String KIT_DISTRIBUTION_EMP_ID = "kit_distribution_employee_id";
    public static String FK_KIT_ITEM_KIT = "fk_kit_item_kit";


 /* <!--leave_calendar_TABLE -Columns names -->*/

    public static String LEAVE_DATE = "leave_date";
    public static String TO_DATE = "to_date";
    public static String LEAVE_REASON_ID = "leave_reason_id";
    public static String LEAVE_STATUS_ID = "leave_status_id";
    public static String IS_HALF_DAY = "is_half_day";
    public static String LEAVE_CALENDAR_ID = "leave_calendar_id";
    public static String IS_APPROVED = "is_approved";
    public static String IS_REJECTED = "is_rejected";
    public static String LEAVE_TYPE_ID = "leave_type_id";


 /* <!--mess_vendor_TABLE -Columns names -->*/

    public static String VENDOR_ID = "vendor_id";
    public static String VENDOR_NAME = "vendor_name";
    public static String VENDOR_CONTACT_NAME = "vendor_contact_name";
    public static String VENDOR_FULL_ADDRESS = "vendor_full_adress";
    public static String UQ_VENDOR_ID_MESS_VENDOR = "uq_vendor_id_mess_vendor";

 /* <!--barrack_TABLE -Columns names -->*/

    public static String BARRACK_NAME = "barrack_name";
    public static String IS_MESS_AVAILABLE = "is_mess_available";
    public static String MESS_VENDOR_ID = "mess_vendor_id";
    public static String OWNER_ADDRESS = "barrack_owner_address";
    public static String IN_CHARGE_NAME = "in_charge_name";
    public static String BARRACK_OWNER_NAME = "barrack_owner_name";
    public static String BARRACK_OWNER_CONTACT_NUMBER = "barrack_owner_contact_number";
    public static String BARRACK_CODE = "Barrack_Code";
    public static String BARRACK_FULL_ADDRESS = "barrack_full_address";
    public static String BARRACK_INCHARGE_NAME = "barrack_incharge_name";
    public static String FK_BARRACK_MESS_VENDOR = "fk_barrack_mess_vendor";
    public static String UQ_BARRACK_ID_BARRACK = "uq_barrack_id_barrack";


  /*<!--rank_TABLE -Columns names -->*/

    public static String RANK_NAME = "rank_name";
    public static String RANK_ID = "rank_id";
    public static String CAN_POSSESS_ARMS = "can_possess_arms";
    public static String UQ_RANK_ID_RANK = "uq_rank_id_rank";


 /* <!--recruitment_TABLE -Columns names -->*/

    public static String RECRUIT_ID = "recruit_id";
    public static String FULL_NAME = "full_name";
    public static String CONTACT_NO = "contact_no";
    public static String REFFERAL_ID = "refferal_id";
    public static String IS_SELECTED = "is_selected";
    public static String APPLICATION_FORM_NO = "application_form_no";
    public static String ACTION_TAKEN_ON = "action_taken_on";
    public static String FK_RECRUITMENT_APP_USER = "fk_recruitment_app_user";


  /*<!--rota_TABLE -Columns names -->*/

    public static String ROTA_ID = "rota_id";
    public static String ROTA_WEEK = "rota_week";
    public static String ROTA_DATE = "rota_date";
    public static String ROTA_STATUS_ID = "rota_status_id";
    public static String ROTA_MONTH = "rota_month";
    public static String ROTA_PUBLISHED_DATE_TIME = "rota_published_date_time";
    public static String ROTA_YEAR = "rota_year";
    public static String FK_ROTA_APP_USER = "fk_rota_app_user";
    public static String UQ_ROTA_ID_ROTA = "uq_rota_id_rota";


 /* <!--rota_task_TABLE -Columns names -->*/

    public static String ROTA_TASK_ID = "rota_task_id";
    public static String ASSIGNED_ID = "Assigned_id";
    public static String SOURCE_GEO_LATITUDE = "source_geo_latitude";
    public static String SOURCE_GEO_LONGITUDE = "source_geo_longitude";
    public static String DESTINATION_GEO_LATITUDE = "destination_geo_latitude";
    public static String DESTINATION_GEO_LONGITUDE = "destination_geo_longitude";
    public static String ESTIMATED_TRAVEL_TIME = "estimated_travel_time";
    public static String ESTIMATED_DISTANCE = "estimated_distance";
    public static String TOTAL_ESTIMATED_TIME = "total_estimated_time";
    public static String ACTUAL_TRAVEL_TIME = "actual_travel_time";
    public static String ACTUAL_DISTANCE = "actual_distance";
    public static String ESTIMATED_TASK_EXECUTION_START_TIME = "estimated_task_execution_start_time";
    public static String ESTIMATED_TASK_EXECUTION_END_TIME = "estimated_task_execution_end_time";
    public static String ACTUAL_TASK_EXECUTION_START_TIME = "actual_task_execution_start_time";
    public static String ACTUAL_TASK_EXECUTION_END_TIME = "actual_task_execution_end_time";

    public static String TASK_SEQUENCE_NO = "task_sequence_no";
    public static String FK_ROTA_TASK_ROTA = "fk_rota_task_rota";
    public static String FK_ROTA_TASK_TASK = "fk_rota_task_task";
    public static String UQ_ROTA_TASK_ID = "uq_rota_task_id";


  /*<!--task_TABLE -Columns names -->*/

    public static String TASK_TYPE_ID = "task_type_id";
    public static String TASK_STATUS_ID = "task_status_id";
    public static String ESTIMATED_EXECUTION_TIME = "estimated_execution_time";
    public static String ACTUAL_EXECUTION_TIME = "actual_execution_time";
    public static String ESTIMATED_EXECUTION_START_DATE_TIME = "estimated_execution_start_date_time";
    public static String ACTUAL_EXECUTION_START_DATE_TIME = "actual_execution_start_date_time";
    public static String POST_ID = "post_id";
    public static String IS_APPROVAL_REQUIRED = "is_approval_required";
    public static String COMMENTS = "comments";
    public static String CREATED_BY = "created_by";
    public static String IS_AUTO = "is_auto";
    public static String TASK_EXECUTION_RESULT = "task_execution_result";
    public static String ESTIMATED_EXECUTION_END_DATE_TIME = "estimated_execution_end_date_time";
    public static String ACTUAL_EXECUTION_END_DATE_TIME = "actual_execution_end_date_time";
    public static String CREATED_DATE = "created_date";

    public static String FK_TASK_UNIT = "fk_task_unit";
    public static String FK_TASK_BARRACK = "fk_task_barrack";
    public static String FK_TASK_UNIT_POST = "fk_task_unit_post";


  /*<!--unit_TABLE -Columns names -->*/

    public static String UNIT_CODE = "unit_code";
    public static String UNIT_NAME = "unit_name";
    public static String UNIT_TYPE = "unit_type";
    public static String BILLING_MODE = "billing_mode";
    public static String IS_BARRACK_PROVIDED = "Is_Barrack_Provided";
    public static String DAY_CHECK_FREQUENCY = "day_check_frequency";
    public static String NIGHT_CHECK_FREQUENCY = "night_check_frequency";
    public static String CLIENT_COORDINATION_FREQUENCY = "client_coordination_frequency";
    public static String UNIT_COMMANDER_NAME = "unit_commander_name";
    public static String AREA_INSPECTOR_NAME = "area_inspector_name";
    public static String BILL_SUBMISSION_RESPONSIBLE_ID = "bill_submission_responsible_Id";
    public static String BILL_COLLECTION_RESPONSIBLE_ID = "bill_collection_responsible_Id";
    public static String BILL_AUTHORITATIVE_UNIT_ID = "bill_authoritative_unit_id";
    public static String BILL_SUBMISSION_RESPONSIBLE_NAME = "bill_submission_responsible_NAME";
    public static String BILL_COLLECTION_RESPONSIBLE_NAME = "bill_collection_responsible_NAME";
    public static String UNIT_TYPE_ID = "unit_type_id";
    public static String UNIT_TYPE_NAME = "unit_type_name";
    public static String SURVEY_DATE = "Survey_Date";
    public static String RAISING_BUDGET = "Raising_Budget";
    public static String BILLING_DATE = "Bill_Date";
    public static String BILL_COLLECTION_DATE = "Bill_Collection_Date";
    public static String UNIT_COMMANDER_ID = "unit_commander_Id";
    public static String AREA_INSPECTOR_ID = "area_inspector_Id";
    public static String WAGE_DATE = "Wage_Date";
    public static String UNIT_STATUS = "unit_status";
    public static String COLLECTION_STATUS = "collection_status";
    public static String BILLING_TYPE_ID = "Billing_Type_Id";
    public static String BILL_GENERATION_DATE = "bill_generation_date";
    public static String AREA_ID = "area_id";
    public static String UNIT_FULL_ADDRESS = "unit_full_address";
    public static String ARMED_STRENGTH = "armed_strength";
    public static String UNARMED_STRENGTH = "unarmed_strength";

    public static String FK_UNIT_AREA = "fk_unit_area";
    public static String FK_UNIT_MESS_VENDOR = "fk_unit_mess_vendor";
    public static String FK_UNIT_CUSTOMER = "fk_unit_customer";
    public static String UQ_UNIT_ID_UNIT = "uq_unit_id_unit";
    public static String UQ_UNIT_CODE_UNIT = "uq_unit_code_unit";


 /* <!--UNIT_BARRACK_TABLE -Columns names -->*/

    public static String FK_UNIT_BARRACK_UNIT = "fk_unit_barrack_unit";
    public static String FK_UNIT_BARRACK_BARRACK = "fk_unit_barrack_barrack";
    public static String UQ_UNIT_BARRACK = "uq_unit_barrack";


 /* <!--- Unit Authorized Strength Table -->*/

    public static String UNIT_SHIFT_RANK_ID = "unit_Shift_Rank_Id";
    public static String RANK_COUNT = "Rank_Count";
    public static String SHIFT_RANK_DETAILS = "Shift_Rank_Details";
    public static String STRENGTH = "Strength";
    public static String ACTUAL = "actual";
    public static String IS_ARMED = "IsArmed";

 /* <!--unit_barrack_actual_strength_TABLE -Columns names -->*/


    public static String SHIFT_ID = "shift_id";
    public static String CHECKED_DATE = "checked_date";
    public static String ACTUAL_COUNT = "actual_count";
    public static String LEAVE = "leave";
    public static String FK_UNIT_BARRACK_ACTUAL_STRENGTH_UNIT = "fk_unit_barrack_actual_strength_unit";
    public static String FK_UNIT_BARRACK_ACTUAL_STRENGTH_RANK = "fk_unit_barrack_actual_strength_rank";

    public static String FK_UNIT_BARRACK_ACTUAL_STRENGTH_BARRACK = "fk_unit_barrack_actual_strength_barrack";
    public static String FK_UNIT_BARRACK_ACTUAL_STRENGTH_TASK = "fk_unit_barrack_actual_strength_task";


  /*<!--unit_barrack_authorized_strength_TABLE -Columns names -->*/

    public static String SHIFT_NAME = "shift_name";
    public static String PERSONAL_COUNT = "personal_count";
    public static String FK_UNIT_BARRACK_AUTHORIZED_STRENGTH_UNIT = "fk_unit_barrack_authorized_strength_unit";
    public static String FK_UNIT_BARRACK_AUTHORIZED_STRENGTH_RANK = "fk_unit_barrack_authorized_strength_rank";
    public static String FK_UNIT_BARRACK_AUTHORIZED_STRENGTH_BARRACK = "fk_unit_barrack_authorized_strength_barrack";


    /*<!--unit_contact_TABLE -Columns names -->*/
    public static String UNIT_CONTACT_ID = "unit_contact_id";

    public static String PHONE_NO = "phone_no";
    public static String DESIGNATION = "designation";
    public static String FK_UNIT_CONTACT_UNIT = "fk_unit_contact_unit";


    /* <!--unit_employee_TABLE -Columns names -->*/
    public static String EMPLOYEE_FULL_NAME = "employee_full_name";

    public static String EMPLOYEE_CONTACT_NO = "employee_contact_no";
    public static String EMPLOYEE_RANK_ID = "employee_rank_id";
    public static String DEPLOYMENT_DATE = "deployment_date";
    public static String WEEKLY_OFF_DAY = "weekly_off_day";

    public static String FK_UNIT_EMPLOYEE_UNIT = "fk_unit_employee_unit";
    public static String FK_UNIT_EMPLOYEE_RANK = "fk_unit_employee_rank";
    public static String UQ_EMPLOYEE_NO_UNIT_EMPLOYEE = "uq_employee_no_unit_employee";


    /*<!--unit_equipment_TABLE -Columns names -->*/
    public static String UNIT_EQUIPMENT_ID = "unit_equipment_id";

    public static String UNIQUE_NO = "unique_no";
    public static String MANUFACTURER = "manufacturer";
    public static String IS_CUSTOMER_PROVIDED = "is_customer_provided";
    public static String EQUIPMENT_STATUS = "equipment_status";

    public static String EQUIPMENT_TYPE = "equipment_type";
    public static String FK_UNIT_EQUIPMENT_UNIT = "fk_unit_equipment_unit";
    public static String UQ_UNIT_EQUIPMENT_ID = "uq_unit_equipment_id";


    /* <!--unit_post_TABLE -Columns names -->*/
    public static String UNIT_POST_ID = "unit_post_id";
    public static String IS_MAIN_GATE = "is_main_gate";
    public static String DEVICE_NO = "DeviceNo";

    public static String UNIT_POST_NAME = "unit_post_name";

    public static String MAIN_GATE_DISTANCE = "main_gate_distance";
    public static String UNIT_BOUNDARY = "unit_boundary";

    public static String BARRACK_DISTANCE = "barrack_distance";
    public static String UNIT_OFFICE_DISTANCE = "unit_office_distance";
    public static String FK_UNIT_POST_UNIT = "fk_unit_post_unit";


  /*<!--unit_risk_TABLE -Columns names -->*/


    public static String UNIT_RISK_ID = "unit_risk_id";
    public static String CURRENT_MONTH = "current_month";
    public static String CURRENT_YEAR = "current_year";
    public static String STATUS = "Status";
    public static String UNIT_RISK_STATUS_ID = "unit_risk_status_id";
    public static String FK_UNIT_RISK_UNIT = "fk_unit_risk_unit";
    public static String UQ_UNIT_MONTH_YEAR = "uq_unit_month_year";
    public static String RISK_COUNT = "riskCount";
    public static String RISK_COUNT_BY_AI = "riskCountByAreaInspector";
    public static String ACTION_COUNT = "actionCount";
    public static String ACTION_PENDING_COUNT = "actionPendingCount";

    /*<!--unit_risk_check_list_TABLE -Columns names -->*/
    public static String UNIT_RISK_ACTION_PLAN_ID = "unit_risk_action_plan_id";
    public static String CHECK_LIST_ITEM = "check_list_item";
    public static String UNIT_RISK_REASON = "unit_risk_reason";
    public static String UNIT_RISK_ACTION_PLAN = "unit_risk_action_plan";
    public static String ACTION_PLAN_DATE = "action_plan_date";
    public static String CAN_EDIT = "can_edit";
    public static String COMPLETION_DATE = "completion_date";
    public static String IS_COMPLETED = "is_completed";

    public static String CLOSED_DATE = "closed_date";


    /*<!--customer_TABLE -Columns names -->*/
    public static String CUSTOMER_ID = "customer_id";

    public static String CUSTOMER_NAME = "customer_name";
    public static String PRIMARY_CONTACT_NAME = "primary_contact_name";
    public static String PRIMARY_CONTACT_EMAIL = "primary_contact_email";
    public static String PRIMARY_CONTACT_PHONE = "primary_contact_phone";

    public static String SECONDARY_CONTACT_NAME = "secondary_contact_name";
    public static String SECONDARY_CONTACT_EMAIL = "secondary_contact_email";
    public static String SECONDARY_CONTACT_PHONE = "secondary_contact_phone";

    public static String CAN_SEND_SMS = "can_send_sms";

    public static String CAN_SEND_EMAIL = "can_send_email";
    public static String CUSTOMER_FULL_ADDRESS = "customer_full_address";
    public static String UQ_CUSTOMER_ID_CUSTOMER = "uq_customer_id_customer";


    /* <!-- new_barrack_strength TABLE - column names-->*/
    public static String GUARD_TYPE_ID = "guard_type_id";
    public static String ACTUAL_STRENGTH = "actual_strength";
    public static String CHECKED_DATE_TIME = "checked_date_time";
    public static String IS_FILLED = "is_filled";

    /* <!-- Rota Task Activity List Table -->*/
    public static String ROTA_TASK_NAME = "rota_task_name";
    public static String ROTA_TASK_ACTIVITY_ID = "rota_task_activity_id";
    public static String ROTA_TASK_BLACK_ICON = "rota_task_black_icon";
    public static String ROTA_TASK_WHITE_ICON = "rota_task_white_icon";

    /*<!-- Rank Master Table - Column name -->*/
    public static String RANK_ABBREVATION = "Rank_Abbrevation";
    public static String RANK_CODE = "Rank_Code";

    /*<!--Client Coordination Table = Column names-->*/
    public static String client_coordination_table = "client_coordination_question";
    public static String question_id = "question_id";
    public static String question = "question";
    public static String is_active = "is_active";

    /* <!--APP PREFRENCE KEYS-->*/
    public static String imei_one = "imei_one";
    public static String imei_two = "imei_two";
    public static String duty_status = "duty_status";
    public static String duty_status_API = "duty_status_API";
    public static String access_token = "access_token";
    public static String token_type = "Token_type";


 /* <!--ADD POST STRNGS-->*/

    public static String KIT_DISTRIBUTION_ITEM_ID = "kit_distribution_item_id";
    public static String KIT_ITEM_SIZE_ID = "kit_item_size_id";
    public static String KIT_ITEM_SIZE_TABLE = "kit_item_size";

    public static String ISSUED_KIT_COUNT = "issued_kit_count";
    public static String PENDING_KIT_COUNT = "pending_kit_count";
    public static String TOTAL_KIT_COUNT = "total_kit_count";

    public static String AnnualKit = "Annual Kit";
    public static String AdhocKit = "Adhoc Kit";
    public static String KIT_SIZE_TABLE = "kit_size_table";
    public static String KIT_SIZE_CODE = "kit_size_code";
    public static String KIT_SIZE_NAME = "kit_size_name";
    public static String KIT_SIZE_VALUE = "kit_size_value";
    public static String ISSUE_OFFICER_ID = "issue_officer_id";
    public static String DISTRIBUTION_STATUS = "distribution_status";
    public static String KIT_TYPE_ID = "kit_type_id";
    public static String DISTRIBUTION_DATE = "distribution_date";
    public static String ISSUED_DATE = "issued_date";
    public static String KIT_SIZE_ID = "kit_size_id";
    public static String QUANTITY = "quantity";

    public static String KIT_ITEM_SIGNATURE = "kit_item_signature";
    public static String VEHICLE_REGISTER_CHECK = "vehicle_register_check";
    public static String VISITOR_REGISTER_CHECK = "visitor_register_check";
    public static String MATERIAL_REGISTER_CHECK = "material_register_check";
    public static String MAINTENANCE_REGISTER_CHECK = "maintenance_register_check";

    /*<!--Security risk questions constants-->*/
    public static String SECURITY_QUESTION_ID = "id";
    public static String SECURITY_QUESTIONS = "question";
    public static String SECURITY_QUESTION_DESCRIPTION = "description";
    public static String NO_HISTORY_MSG = "No history available for this task";
    public static String captureImagePosition = "position";
    public static String security_option_id = "securityOptionId";
    public static String appUser = "AppUser";
    public static String NO_DATA_AVAILABLE = "No Data Available.";
    public static String NO_TASK_FOR_SPECIFIC_DAY = "No Task For";
    public static String NO_TASK_FOR_CURRENT_MONTH = "No Task For Current Month";
    public static String TODAY = "TODAY";
    public static String THIS_MONTH = "THIS MONTH";
    public static String ok = "OK";
    public static String guardRegNo = "Reg No";
    public static String dateOfBirth = "Date Of Birth";
    public static String educationLabel = "Education";
    public static String heightLabel = "Height (in cms)";
    public static String bloodGroup = "Blood Group";
    public static String joiningDateLabel = "Joining Date";
    public static String idCardExpiryDate = "Id Card Expiry Date";
    public static String Experience = "Experience (in months)";
    public static String height = "5.8\'";
    public static String weightLabel = "Weight (in kgs)";
    public static String weight = "65 kg";
    public static String joiningDate = "23/04/2014";
    public static String LOOKUP_MODEL_TABLE = "lookup_table";
    public static String nightCheckingTitle = "Night Checking";
    public static String navigationcheckingTitle = "title";


    public static String LOOKUP_MODEL_DATA = "lookup_model_data";
    public static String LOOKUP_TYPE_ID = "lookup_type_id";
    public static String LOOKUP_TYPE_NAME = "lookup_type_name";
    public static String LOOKUP_IDENTIFIER = "lookup_identifier";
    public static String LOOKUP_CODE = "lookup_code";

    public static String LOOKUP_NAME = "lookup_name";


    public static String SECURITY_RISK_QUESTIONS_ID = "security_question_id";
    public static String SECURITY_RISK_OPTION_ID = "security_option_id";
    public static String SECURITY_RISK_OPTION_NAME = "security_option_name";
    public static String SECURITY_RISK_IS_MANDATORY = "is_mandatory";
    public static String SECURITY_RISK_CONTROL_TYPE = "control_type";
    public static String SECURITY_RISK_IS_ACTIVE = "is_active";

    public static String security_risk = "Security Risk Observations";

    public static String ROTA_CALENDAR_DATE = "substr(estimated_execution_start_date_time,1,10)";

    public static String AI_PHOTO = "ai_photo";
    public static String ATTACHMENT_TYPE_ID = "attachment_type_id";

    public static String DISTANCE_TRAVELLED = "distance_travelled";
    public static String DISTANCE_TRAVELLED_IN_TIME = "distance_travelled_time";
    public static String CREATED_DATE_TIME = "CreatedDateTime";
    public static String EXPECTED_CLOSURE_DATE = "expected_closure_date";
    public static String STATUS_ID = "status_id";
    public static String IMPROVEMENT_PLAN_CATEGORY_ID = "improvement_plan_category_id";
    public static String CLOSURE_DATE_TIME = "closure_date_time";
    public static String CLOSED_BY_ID = "closed_by_id";
    public static String CLOSURE_COMMENT = "closure_comment";
    public static String ASSIGNED_TO_ID = "assigned_to_id";
    public static String ASSIGNED_TO_NAME = "assigned_to_name";
    public static String ISSUE_MATRIX_ID = "issue_matrix_id";
    public static String CLIENT_COORDINATION_QUESTION_ID = "client_coordination_question_id";
    public static String TOTAL_ITEM_COUNT = "total_item_count";
    public static final String UN_PAID_COUNT = "unpaid_kit_count";
    public static String ASSIGNEE_ID = "assignee_id";
    public static String ASSIGNEE_NAME = "Assignee_Name";
    public static String CREATED_BY_ID = "createdById";
    public static String CREATED_BY_NAME = "createdByName";
    public static String ASSIGNED_BY_ID = "assigned_by_id";
    public static String SELECT_REASON_ID = "select_reason_id";
    public static String OTHER_TASK_REASON_ID = "other_task_reason_id";
    public static String LEAVE_COUNT = "leave_count";
    public static String EQUIPMENT_COUNT = "count";

    public static String BARRACK_INSPECTION_QUESTION_TABLE = "barrack_inspection_question";
    public static String QUESTION_ID = "question_id";
    public static String QUESTION = "question";
    public static String DESCRIPTION = "description";
    public static String CONTROL_TYPE = "control_type";

    public static String BARRACK_INSPECTION_ANSWER_TABLE = "barrack_inspection_question_option";
    public static String OPTION_ID = "option_id";
    public static String OPTION_NAME = "option_name";

    public static String GUARD_CODE = "guard_code";
    public static String GUARD_NAME = "guard_name";
    public static String UNIT_CONTACT_NAME = "unit_contact_name";
    public static String CLOSURE_REMARKS = "closure_remarks";
    public static String ISSUES_TABLE = "issues";
    public static String ISSUE_TYPE_ID = "issue_type_id";
    public static String SOURCE_TASK_ID = "source_task_id";
    public static String ISSUE_CATEGORY_ID = "issue_category_id";
    public static String ISSUE_MODE_ID = "issue_mode_Id";
    public static String ISSUE_NATURE_ID = "issue_nature_id";
    public static String ISSUE_CAUSE_ID = "issue_cause_id";
    public static String ISSUE_SERVERITY_ID = "issue_severity_id";
    public static String ISSUE_STATUS_ID = "issue_status_id";
    public static String TARGET_ACTION_END_DATE = "target_action_end_date";
    public static String CLOSURE_END_DATE = "closure_end_date";

    public static String MASTER_ACTION_PLAN_TABLE = "master_action_plan";
    public static String ISSUE_MATRIX_TABLE = "issue_matrix";
    public static String NAME = "Name";

    public static String ISSUE_SUB_CATEGORY_ID = "issue_sub_category_id";
    public static String MASTER_ACTION_PLAN_ID = "master_action_plan_id";
    public static String DEFAULT_ASSIGNMENNT_ROLE_ID = "default_assignment_role_id";
    public static String TURN_AROUND_TIME = "turn_around_time";
    public static String PROOF_CONTROL_TYPE = "proof_control_type";
    public static String MINIMUM_OPTIONAL_VALUE = "minimum_optional_value";

    public static String KIT_DISTRIBUTION_ITEM_TABLE = "kit_distribution_item";
    public static String ACTIVITY_LOG_TABLE = "activity_log";
    public static String ACTIVITY = "activity";
    public static String TASK_NAME = "task_name";
    public static String TIME_STAMP = "time_stamp";
    public static String TIME = "time";

    public static String kit_item_table = "kit_item";
    public static String KIT_ITEM_CODE = "kit_item_code";
    public static String KIT_ITEM_NAME = "kit_item_name";
    public static String REQUESTED_ON = "requested_on";
    public static String REQUESTED_STATUS = "requested_status";
    public static String REQUESTED_ITEMS = "requested_items";
    public static String KIT_ITEM_REQUEST_ID = "kit_item_requested_id";
    public static String KIT_ITEM_REQUEST_TABLE = "kit_item_request";
    public static String KIT_ITEM_TABLE = "kit_item";
    public static String COUNT = "count";
    public static String IMPROVEMENT_STATUS = "improvementStatus";
    public static String IMPROVEMENT_PLAN_CATEGORY = "improvement_plan_category";
    public static String action_plan_id = "ACTION_PLAN_ID";

    /**
     * Notification Table
     */
    public static String CALLBACK_URL = "callback_url";
    public static String IS_SUCCESSFUL = "is_successful";
    public static String NOTIFICATION_NAME = "notification_name";
    public static String NOTIFICATION_ID = "notification_id";

    public static String Source_type = "source_type";
    public static String unit = "Unit";
    public static String barrack = "Barrack";
    public static String lookup_improvementPlanCategory = "ImprovementPlanCategory";
    public static String NOT_AVAILABLE = "NA";
    public static String Other_tasks = "Other Tasks";
    public static String NO_TASK_FOR_TODAY = "No Task For Today";
    public static String TASK_COUNT = "taskCount";
    public static String TOTAL_TIME = "TotalTime";
    public static String MYUNITS_HOMEPAGE_COMPLETED = "COMPLETED";
    public static String MYUNITS_HOMEPAGE_PENDING = "PENDING";

    public static String jsonKitRequest = "JsonKitRequest";
    public static String splashScreenFlag = "splashScreenFlag";
    public static String isFirstTime = "isFirstTime";
    public static String appUserID = "AppUserId";
    public static String commonImagePath = "CommonImagePath";
    public static String taskStatus = "TaskStatus";
    public static String equipmentList = "equipmentList";
    public static String loadConfigurationDone = "loadConfiguration";
    public static String otp = "otp";
    public static String Grievance = "Grievances";
    public static String no_grievance_data = "Grievance data not available";
    public static String complaints_label = "Complaints";
    public static String no_complaints_data = "Complaint data not available";
    public static String improvementplan = "Improvement Plan";
    public static String no_improvementplan_data = "Improvement Plan data not available";
    public static String unitatrisk_poa = "Unit At Risk(POA)";
    public static String no_unitatrisk_data = "Unit At Risk(POA) data not available";
    public static String kitItemRquest = "Kit Items";
    public static String no_kititem_request_data = "Kit Item data not available";

    public static String NATURE_OF_GRIEVANCE = "NatureOfGrievance";
    public static String IssueCategory = "IssueCategory";
    public static String GrievanceType = "GrievanceType";
    public static String select_greviance_category = "Select Grievance Category";
    public static String select_greviance_sub_category = "Select Grievance Sub Category";
    public static String CHECKED_POST_IN_UNIT = "checkedPostInUnit";
    public static String Pending = "PENDING";
    public static String COMPLETED = "COMPLETED";
    public static String UNIT_POST = "unitpost";
    public static String GRIEVENCE = "Grievence";
    public static String UNIT_TASK = "UnitTask";

    public static String GUARD = "guard";
    public static String COMPLAIN = "complain";
    public static String DUTY_REGISTER_CHECK = "DutyRegisterCheck";
    public static String CLIENT_REGISTER_CHECK = "ClientRegisterCheck";
    public static String BILL_SUBMISSION = "Bill Submission";

    public static String SIGNATURE = "signature";
    public static String SECURITY_RISK = "security_risk";
    public static String IMPROVEMENT_PLAN = "Improvement Plan";
    public static String BARRACK_BEDDING = "Barrack Bedding";
    public static String BARRACK_KIT = "Barrack Kit";
    public static String BARRACK_MESS = "Barrack Mess";
    public static String BARRACK_OTHERS = "Barrack Others";
    public static String client_coordination = "Client Coordination";
    public static String KIT_ITEM_REQUEST = "Kit Item Request";
    public static String KIT_DISTRIBUTION_ITEM_IMAGE = "kit_distribution_item_image";
    public static String DISTRIBUTION_SIGANTURE = "distribution_signature";
    public static String BILL_COLLECTION = "Bill Collection";
    /*<!--EQUIPMENT LIST-->*/
    public static String equipment_vehical = "VEHICLE (4 WHEELER)";
    public static String equipment_motorCycle = "MOTOR CYCLE";
    public static String equipment_metalDetector = "METAL DETECTOR";
    public static String equipment_cycle = "CYCLE";
    public static String equipment_wireless_handheld = "WIRELESS HANDHELD";
    public static String equipment_wireless_on_vehical = "WIRELESS ON VEHICLE";
    public static String equipment_wireless_base_station = "WIRELESS BASE STATION";
    public static String equipment_cleaning_equipment = "CLEANING EQUIPMENT";
    public static String selfie_bh = "SelfieWithBH";

    /* <!--Media type-->*/
    public static String picture = "Picture";
    public static String audio_str = "Audio";
    public static String video = "Video";

    public static String CHECKING_TYPE_DAY_CHECKING = "Day Checking";
    public static String employee_id = "Employee ID Number";
    public static String guard_name = "Guard Name";
    public static String unitName = "Unit Name";
    public static String select_non_issue_reason_id = "Select non issue reason";
    public static String KitNonIssueReason = "KitNonIssueReason";
    public static String registerNumber = "registerNumber";
    public static String ISSUES_FRAGMENT = "IssuesFragment";

    public static String AI = "AI";
    public static String MAIN_GATE_IMAGE_URL = "main_gate_imageUrl";
    public static String IS_SAVE = "is_save";
    public static String SETTINGS_AI_PIC = "settings_ai_pic";

    //Duty Attendance table
    public static final String DUTY_ATTENDANCE_TABLE = "duty_attendance";
    public static final String DUTY_ON_DATE_TIME = "duty_on_date_time";
    public static final String DUTY_OFF_DATE_TIME = "duty_off_date_time";
    public static final String DUTY_REPONSE_ID = "duty_response_id";

    // Equipment Table
    public static final String EQUIPMENT_LIST_TABLE = "equipment_list_table";
    public static final String EQUIPMENT_NAME = "equipment_name";
    public static final String EQUIPMENT_CODE = "equipment_code";
    public static final String EQUIPMENT_TYPE_ID = "equipment_type_id";
    public static String AppVersion = "AppVersion";

    // unit_billing_check_list
    public static final String UNIT_BILLING_CHECK_LIST_TABLE = "unit_billing_check_list";
    public static final String BILL_CHECK_LIST_ID = "billing_check_list_id";

    //    @Ashu
    public static final String QUERY_TABLE_NAME = "menu_query_executor";
    public static final String QUERY_NAME = "query_name";
    public static final String QUERY = "query";
    public static final String QUERY_STATUS = "query_status";
    public static final String QUERY_RESULT = "query_result";
    public static final String NOTIFICATION_STATUS = "notification_status";
    public static final String RESULT_STATUS = "result_status";

    public static final String BARRACK_IS_NFC_SCANNED = "isNfcScanned";
}



