package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class Equipment {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("EquipmentName")

    public String EquipmentName;
    @SerializedName("EquipmentCode")

    public String EquipmentCode;
    @SerializedName("EquipmentTypeId")

    public Integer EquipmentTypeId;
    @SerializedName("IsActive")

    public Boolean IsActive;
    @SerializedName("Description")

    public String Description;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEquipmentCode() {
        return EquipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        EquipmentCode = equipmentCode;
    }

    public String getEquipmentName() {
        return EquipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        EquipmentName = equipmentName;
    }

    public Integer getEquipmentTypeId() {
        return EquipmentTypeId;
    }

    public void setEquipmentTypeId(Integer equipmentTypeId) {
        EquipmentTypeId = equipmentTypeId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }
}