package com.sisindia.ai.android.fcmnotification.fcmLookup;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.rota.models.LookupModel;

import java.util.List;

/**
 * Created by Durga Prasad on 01-12-2016.
 */

public class FcmLookUpModelInsertion extends SISAITrackingDB {
    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite = null;
    private Context context;

    public FcmLookUpModelInsertion(Context context) {
        super(context);
        this.context = context;
    }

    public synchronized boolean insertLookupModel(List<LookupModel> fcmLookupModelList) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if (fcmLookupModelList != null && fcmLookupModelList.size() != 0) {
                    sqlite.beginTransaction();
                    LookupDeletionStatementDB lookupDeletionStatementDB = new LookupDeletionStatementDB(context);
                    lookupDeletionStatementDB.deleteLookupTable(context.getResources().getString(R.string.LOOKUP_MODEL_TABLE), sqlite);
                    insertLookUpDataToTable(fcmLookupModelList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                } else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    public void insertLookUpDataToTable(List<LookupModel> mLookUpModelData) {

        synchronized (sqlite) {
            try {

                ContentValues values = new ContentValues();
                for (LookupModel lookupModel : mLookUpModelData) {
                    values.put(TableNameAndColumnStatement.ID, lookupModel.getId());
                    values.put(TableNameAndColumnStatement.LOOKUP_CODE, lookupModel.getLookupCode());
                    values.put(TableNameAndColumnStatement.LOOKUP_TYPE_ID, lookupModel.getLookupTypeId());
                    values.put(TableNameAndColumnStatement.LOOKUP_IDENTIFIER, lookupModel.getLookupIdentifier());
                    values.put(TableNameAndColumnStatement.LOOKUP_NAME, lookupModel.getName());
                    values.put(TableNameAndColumnStatement.LOOKUP_TYPE_NAME, lookupModel.getLookupTypeName());
                    sqlite.insert(TableNameAndColumnStatement.LOOKUP_MODEL_TABLE, null, values);
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }

}
