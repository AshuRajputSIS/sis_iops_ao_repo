package com.sisindia.ai.android.commons;

/**
 * Created by Durga Prasad on 18-06-2016.
 */

/**
 * Do not use this.
 */
@Deprecated
public interface DialogClickListener {

    void onPositiveButtonClick(int clickType);
}
