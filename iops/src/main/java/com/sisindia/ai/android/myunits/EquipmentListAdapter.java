package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.AddEquipmentLocalMO;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;

import java.util.ArrayList;

/**
 * Created by Saruchi on 10-05-2016.
 */
public class EquipmentListAdapter extends BaseAdapter {
    private ArrayList<SyncingUnitEquipmentModel> equipList = new ArrayList<>();
    private LayoutInflater inflater = null;


    public EquipmentListAdapter(ArrayList<SyncingUnitEquipmentModel> equipList, Context context) {
        this.equipList = equipList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return equipList.size();
    }


    public Object getItem(int position) {
        return position;
    }


    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)

            view = inflater.inflate(R.layout.common_textview_layout, null);


        TextView name = (TextView) view.findViewById(R.id.recycle_header); // name

        name.setText(equipList.get(position).getAddEquipmentLocalMO().getEquipmentType());


        return view;
    }

    public void updateAdapter( ArrayList<SyncingUnitEquipmentModel> list){
        this.equipList = list;
        notifyDataSetChanged();
    }

}
