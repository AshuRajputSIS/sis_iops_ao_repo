package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PerformanceratingIMO {

    @SerializedName("Alertness")
    @Expose
    private Integer Alertness;
    @SerializedName("Turnout")
    @Expose
    private Integer Turnout;
    @SerializedName("CommunicationSkills")
    @Expose
    private Integer CommunicationSkills;
    @SerializedName("DutyPerformance")
    @Expose
    private Integer DutyPerformance;

    /**
     * @return The Alertness
     */
    public Integer getAlertness() {
        return Alertness;
    }

    /**
     * @param Alertness The Alertness
     */
    public void setAlertness(Integer Alertness) {
        this.Alertness = Alertness;
    }

    /**
     * @return The Turnout
     */
    public Integer getTurnout() {
        return Turnout;
    }

    /**
     * @param Turnout The Turnout
     */
    public void setTurnout(Integer Turnout) {
        this.Turnout = Turnout;
    }

    /**
     * @return The CommunicationSkills
     */
    public Integer getCommunicationSkills() {
        return CommunicationSkills;
    }

    /**
     * @param CommunicationSkills The CommunicationSkills
     */
    public void setCommunicationSkills(Integer CommunicationSkills) {
        this.CommunicationSkills = CommunicationSkills;
    }

    /**
     * @return The DutyPerformance
     */
    public Integer getDutyPerformance() {
        return DutyPerformance;
    }

    /**
     * @param DutyPerformance The DutyPerformance
     */
    public void setDutyPerformance(Integer DutyPerformance) {
        this.DutyPerformance = DutyPerformance;
    }

    @Override
    public String toString() {
        return "PerformanceratingIMO{" +
                "Alertness=" + Alertness +
                ", Turnout=" + Turnout +
                ", CommunicationSkills=" + CommunicationSkills +
                ", DutyPerformance=" + DutyPerformance +
                '}';
    }
}