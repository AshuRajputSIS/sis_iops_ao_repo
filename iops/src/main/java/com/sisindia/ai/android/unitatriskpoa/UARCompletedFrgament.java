package com.sisindia.ai.android.unitatriskpoa;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 15/7/16.
 */

public class UARCompletedFrgament extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CURRENT_TAB = "current_tab";
    private static final String UNIT_RISK_ID = "unitRiskId";
    public static UARCompletedFrgament fragment;
    private  String unitName;
    private CompletedCheckListAdapter completedCheckListAdapter;
    private int unitRiskId;
    private List<ActionPlanMO> completedActionPlanMOList;
    private int mCurrentPos;
    private RecyclerView completedRecyclerView;
    private Bundle bundle;

    CustomFontTextview dummyname;
    CustomFontTextview unitNameTxt;


    public UARCompletedFrgament (){

    }
    public UARCompletedFrgament(int riskId,String unitname) {
        this.unitRiskId =riskId;
        this.unitName =unitname;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UARCompletedFrgament newInstance(String param1, String param2, int unitRiskId, int currentTab,String unitName) {
       // if (fragment == null) {
            fragment = new UARCompletedFrgament(unitRiskId,unitName);
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            args.putInt(CURRENT_TAB, currentTab);
            args.putInt(UNIT_RISK_ID, unitRiskId);

            fragment.setArguments(args);
       // }
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* bundle = getArguments();
        if (bundle != null) {
         //   unitRiskId = bundle.getInt(UNIT_RISK_ID);
            mCurrentPos = bundle.getInt(CURRENT_TAB);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.action_plan_list, container, false);

        initialiseView(view);
        initialiseDate();

        return view;
    }

    private void initialiseDate() {
        completedActionPlanMOList = new ArrayList<>();
        completedActionPlanMOList = IOPSApplication.getInstance().
                getUARSelectionInstance().getUARCompletedList(unitRiskId, 1);

        if (null != completedActionPlanMOList) {
            dummyname.setVisibility(View.GONE);
            completedRecyclerView.setVisibility(View.VISIBLE);
            completedCheckListAdapter = new CompletedCheckListAdapter(getContext(), completedActionPlanMOList);
            completedRecyclerView.setAdapter(completedCheckListAdapter);
            completedCheckListAdapter.notifyDataSetChanged();
        }else{
            dummyname.setVisibility(View.VISIBLE);
            completedRecyclerView.setVisibility(View.GONE);
        }
        unitNameTxt.setText(unitName);
    }

    private void initialiseView(View view) {

        completedRecyclerView = (RecyclerView) view.findViewById(R.id.RecyclerView);
        completedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        unitNameTxt = (CustomFontTextview) view.findViewById(R.id.unit_name);
        dummyname = (CustomFontTextview) view.findViewById(R.id.dummyname);
        dummyname.setVisibility(View.VISIBLE);
        completedRecyclerView.setVisibility(View.GONE);

    }

}