package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsOR;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemUpdateStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 18-11-2016.
 */

public class KitNonIssueItemSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private ArrayList<KitDeliveredItemsIR> kitDeliveredItemsIRListWithNonIssues;
    private KitItemUpdateStatementDB kitItemUpdateStatementDB;
    private HashMap<Integer, Integer> kitDistributionIdList;
    private int nonIssueCount;

    public KitNonIssueItemSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        kitItemUpdateStatementDB = IOPSApplication.getInstance().getKitItemUpdateStatementDB();
        kitNonIssueItemSyncing();
    }

    private void kitNonIssueItemSyncing() {
        kitDeliveredItemsIRListWithNonIssues = getAnnualKitItemListWithNonIssuesReason();
        if (kitDeliveredItemsIRListWithNonIssues != null && kitDeliveredItemsIRListWithNonIssues.size() != 0) {
            for (KitDeliveredItemsIR kitDeliveredItemsIR : kitDeliveredItemsIRListWithNonIssues) {
                AnnualKitItemsRequestApiCall(kitDeliveredItemsIR);
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER + "AnnualkitItemsSynciing -->nothing to sync ");
            dependentFailureSync();
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(dependentSyncsStatus != null && dependentSyncsStatus.kitDistributionId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {
        if(dependentSyncsStatus != null){
            if(dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
                MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
                if(dependentSyncsStatus.kitDistributionId != 0) {
                    metaDataSyncing.setKitDistributionId(dependentSyncsStatus.kitDistributionId);
                }
            }
            else {
                if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                    ImageSyncing imageSyncing = new ImageSyncing(mContext);
                    if(dependentSyncsStatus.kitDistributionId != 0) {
                        imageSyncing.setKitDistributionId(dependentSyncsStatus.kitDistributionId);
                    }
                }
            }
        }
    }

    private ArrayList<KitDeliveredItemsIR> getAnnualKitItemListWithNonIssuesReason() {
        ArrayList<KitDeliveredItemsIR> kitDeliveredItemsIRList = new ArrayList<>();
        kitDistributionIdList = new HashMap<>();
        String kitItemQuery = "select * from " +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE + " where " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0 + " and " +
                TableNameAndColumnStatement.IS_ISSUED + " = " + 0 + " and " +
                TableNameAndColumnStatement.NON_ISSUE_REASON_ID + " != " + 0 + " and " +
                TableNameAndColumnStatement.IS_UNPAID + " != 0";
        Timber.d("AnnualkitItemQuery   %s", kitItemQuery);
        Cursor cursor;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(kitItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        KitDeliveredItemsIR kitDeliveredItemsIR = new KitDeliveredItemsIR();

                        kitDeliveredItemsIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID)));
                        int kitDistributionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID));
                        kitDeliveredItemsIR.setKitDistributionId(kitDistributionId);

                        if(kitDistributionIdList.get(kitDistributionId) == null){
                            kitDistributionIdList.put(kitDistributionId,1);
                        }
                        else{
                            int  kitDistributionCount = kitDistributionIdList.get(kitDistributionId);
                            kitDistributionCount = kitDistributionCount +1;
                            kitDistributionIdList.put(kitDistributionId,kitDistributionCount);
                        }
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUED)) == 1) {
                            kitDeliveredItemsIR.setIsIssued(true);
                        } else {
                            kitDeliveredItemsIR.setIsIssued(false);
                        }

                        kitDeliveredItemsIR.setNonIssueReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.NON_ISSUE_REASON_ID)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNPAID)) == 1) {
                            kitDeliveredItemsIR.setIsUnPaid(true);
                        } else {
                            kitDeliveredItemsIR.setIsUnPaid(false);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_ID)) == 0) {
                            kitDeliveredItemsIR.setKitApparelSizeId(null);
                        } else {
                            kitDeliveredItemsIR.setKitApparelSizeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_ID)));
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)) == 0) {
                            kitDeliveredItemsIR.setKitItemId(null);
                        } else {
                            kitDeliveredItemsIR.setKitItemId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)));
                        }

                        kitDeliveredItemsIR.setQuantity(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.QUANTITY)));
                        kitDeliveredItemsIRList.add(kitDeliveredItemsIR);

                        Timber.d(Constants.TAG_SYNCADAPTER + "Annual kitItemJosnData %s", new Gson().toJson(kitDeliveredItemsIR));

                    } while (cursor.moveToNext());
                }

            }
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", kitDeliveredItemsIRList);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return kitDeliveredItemsIRList;
    }

    private synchronized void AnnualKitItemsRequestApiCall(final KitDeliveredItemsIR kitDeliveredItemsIR) {
        sisClient.getApi().syncKitDeliveredItemsUpdate(kitDeliveredItemsIR, new Callback<KitDeliveredItemsOR>() {

            @Override
            public void success(KitDeliveredItemsOR kitDeliveredItemsOR, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "kitItemRequest response  %s", kitDeliveredItemsOR);

                if (kitDeliveredItemsOR != null) {
                    switch (kitDeliveredItemsOR.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            if (kitDeliveredItemsOR.getKitDeliveredItemsData() != null) {
                                kitItemUpdateStatementDB.updateAnnualKitDistributionIdWithNonIssue(
                                        kitDeliveredItemsIR.getId(),
                                        kitDeliveredItemsOR.getKitDeliveredItemsData().getKitDistributionId());
                                updateStatus(kitDeliveredItemsOR);

                            }


                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("unitAtRiskSync", error);
            }
        });
    }

    private void updateStatus(KitDeliveredItemsOR kitDeliveredItemsOR) {
        int count = kitDistributionIdList.
                get(kitDeliveredItemsOR.getKitDeliveredItemsData()
                        .getKitDistributionId());
        if(count != 0)
            count = count -1;
        kitDistributionIdList.put(kitDeliveredItemsOR.getKitDeliveredItemsData()
                .getKitDistributionId(),count);
        if(0 == kitDistributionIdList.get(kitDeliveredItemsOR
                .getKitDeliveredItemsData().getKitDistributionId())){

            DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
            dependentSyncsStatus.kitDistributionId = kitDeliveredItemsOR
                    .getKitDeliveredItemsData().getKitDistributionId();
            dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isUnitEquipmentSynced = Constants.NEG_DEFAULT_COUNT;
            insertDepedentSyncData(dependentSyncsStatus);
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            metaDataSyncing.setKitDistributionId( kitDeliveredItemsOR
                    .getKitDeliveredItemsData().getKitDistributionId());
        }
    }

    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.kitDistributionId,
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ID,true)){
            dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
        }
    }
}