package com.sisindia.ai.android.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.SelectReasonAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;



public class LocalVideoFragment extends BaseFragment implements OnRecyclerViewItemClickListener {
    @Bind(R.id.helpRecylerView)
    RecyclerView helpRecylerView;
    private static final String SELECT_VIDEO= "Select videos";
    private ArrayList<Object> videosUrlList;
    private SelectReasonAdapter selectReasonAdapter;
    private Context context;




    public static LocalVideoFragment newInstance() {
        LocalVideoFragment fragment = new LocalVideoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videosUrlList = new ArrayList<>();
    }
    @Override
    protected int getLayout() {
        return R.layout.fragment_local_video;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_local_video, container, false);

        ButterKnife.bind(this, view);
        getHelpVideos();
        setupRecylerView();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void getHelpVideos() {
        File mediaStorageDir = new File(Constants.VIDEOS_FILE_PATH);
        videosUrlList.add(SELECT_VIDEO);
        if (mediaStorageDir != null) {
            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdir();
            }
            File[] files = mediaStorageDir.getAbsoluteFile().listFiles();
            if (files != null && files.length != 0) {
                for (File file : files) {
                    try {
                        file.getName();
                        VideosMetadataMo videosMetadataMo = new VideosMetadataMo();
                        videosMetadataMo.setFilePath(file.getPath());
                        videosMetadataMo.setFileName(file.getName());
                        videosUrlList.add(videosMetadataMo);
                        file.getCanonicalFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    private void setupRecylerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        helpRecylerView.setLayoutManager(linearLayoutManager);
        helpRecylerView.setItemAnimator(new DefaultItemAnimator());
        if(selectReasonAdapter == null) {
            selectReasonAdapter = new SelectReasonAdapter(getActivity());
        }
        if(videosUrlList != null) {
            if(videosUrlList.size() != 0) {
                selectReasonAdapter.setReasonDataList(videosUrlList);
                selectReasonAdapter.notifyDataSetChanged();
            }
        }
        helpRecylerView.setAdapter(selectReasonAdapter);
        selectReasonAdapter.setOnRecyclerViewItemClickListener(this);
    }
    @Override
    public void onRecyclerViewItemClick(View v, int position) {

        Intent callVideo = new Intent(getActivity(),VideoViewActivity.class);
        callVideo.putExtra(Constants.VIDEO_LINK,((VideosMetadataMo) videosUrlList.get(position)).getFilePath());
        callVideo.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(callVideo);

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}

