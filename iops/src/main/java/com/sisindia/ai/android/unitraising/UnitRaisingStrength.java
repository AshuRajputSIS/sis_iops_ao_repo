package com.sisindia.ai.android.unitraising;

import com.sisindia.ai.android.database.unitRaisingDTO.UnitRaisingStrengthMo;

import java.util.ArrayList;

/**
 * Created by compass on 8/12/2017.
 */

class UnitRaisingStrength {

    public boolean validationStatus;
    public ArrayList<UnitRaisingStrengthMo> updatedActualStrength;
}
