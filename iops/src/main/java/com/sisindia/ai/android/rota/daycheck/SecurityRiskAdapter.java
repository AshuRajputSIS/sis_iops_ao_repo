package com.sisindia.ai.android.rota.daycheck;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.rota.daycheck.taskExecution.RiskSpinAdapter;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.rota.models.SecurityRiskOptionMO;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskAdapter extends RecyclerView.Adapter<SecurityRiskAdapter.SecurityViewHolder> implements View.OnClickListener {
    private Context mContext;
    private ArrayList<SecurityRiskModelMO> modelObject;
    private RiskSpinAdapter adapter;
    private int optionId = 0;
    private SecurityRiskOptionMO optionHolder;

    public SecurityRiskAdapter(Context mContext, ArrayList<SecurityRiskModelMO> modelObject) {
        this.mContext = mContext;
        this.modelObject = modelObject;
    }

    @Override
    public SecurityRiskAdapter.SecurityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.security_risk_row, parent, false);
        SecurityViewHolder mHolder = new SecurityViewHolder(itemView);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(final SecurityRiskAdapter.SecurityViewHolder holder, final int position) {
        holder.mEditTextView.setText(modelObject.get(position).getReasonTxt());
//        Timber.d("hello == %s", modelObject.get(position));
        adapter = new RiskSpinAdapter(mContext, android.R.layout.simple_spinner_item, modelObject.get(position).getmRiskTypeHolder(), modelObject);
        holder.mCustomSpinner.setAdapter(adapter);
        holder.mCustomSpinner.setSelection(modelObject.get(position).getSpinnerPosition(), true);

        modelObject.get(position).setPos(position);

        if (modelObject.get(position).getPictureUri() != null) {
            int optionSelected = modelObject.get(position).getSpinnerPosition();
            SecurityRiskOptionMO option = modelObject.get(position).getmRiskTypeHolder().get(optionSelected);

            holder.mCameraImage.setImageURI(Uri.parse(modelObject.get(position).getPictureUri()));
            SecurityRiskHelper.getInstance().completedAnswer(modelObject.get(position).getSecurityRiskQuestionId(), option);
        } else {
            holder.mCameraImage.setImageResource(R.drawable.camera_pic);
        }

        if (modelObject.get(position).getReasonTxt() != null) {
            int optionSelected = modelObject.get(position).getSpinnerPosition();
            holder.mEditTextView.setText(modelObject.get(position).getReasonTxt().toString());
            SecurityRiskOptionMO option = modelObject.get(position).getmRiskTypeHolder().get(optionSelected);
            option.setmUserComments(modelObject.get(position).getReasonTxt().toString());

        }


        holder.mCustomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                modelObject.get(position).setSpinnerPosition(pos);

                if(modelObject.get(position).getmDropDownItem()!= pos ){
                    modelObject.get(position).setmDropDownItem(pos);
                    holder.mCameraImage.setImageResource(R.drawable.camera_pic);
                }
                if( modelObject.get(position).getSpinnerPosition() != holder.mCustomSpinner.getSelectedItemPosition() ){
                    holder.mCameraImage.setImageResource(R.drawable.camera_pic);
                }

                SecurityRiskOptionMO option = modelObject.get(position).getmRiskTypeHolder().get(pos);
                optionHolder = option;
                optionId = pos;
                if (option.getControlType() != null) {
                    setControlTypeVisibility(option.getControlType(), holder, position, pos);
                } else {
                    holder.mEditTextView.setVisibility(View.GONE);
                    holder.mCameraImage.setVisibility(View.GONE);
                }
                SecurityRiskHelper.getInstance().updateAnswer(modelObject.get(position).getSecurityRiskQuestionId(), option);
                Util.setmSecurityRiskModelHolder(modelObject);
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.mEditTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                if (holder.mEditTextView.getText().length() > 0) {

                    int optionSelected = modelObject.get(position).getSpinnerPosition();
                    SecurityRiskOptionMO option = modelObject.get(position).getmRiskTypeHolder().get(optionSelected);
                    optionHolder = option;
                    modelObject.get(position).setDataFilled(true);
                    SecurityRiskHelper.getInstance().completedAnswer(modelObject.get(position).getSecurityRiskQuestionId(), option);
                    Util.setmSecurityRiskModelHolder(modelObject);
                    modelObject.get(holder.getAdapterPosition()).setReasonTxt(holder.mEditTextView.getText().toString().trim());
                }
            }
        });


        String text = modelObject.get(holder.getAdapterPosition()).getReasonTxt();
        if(text != null && !text.isEmpty()){
            holder.mEditTextView.setText(modelObject.get(position).getReasonTxt());
        }
        holder.mCameraImage.setOnClickListener(this);
        holder.mCameraImage.setTag(position);
        int value = position;
        holder.mCustomTitle.setText((value + 1) + ". " + modelObject.get(position).getmSecurityTypeTitle());
    }
    @Override
    public int getItemCount() {
        return modelObject.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (Integer) v.getTag();
        Util.mSequenceNumber++;
        switch (pos) {
            default:
                modelObject.get(pos).setCameraIconSelectionPosition(pos);
                Intent capturePictureActivity = new Intent(mContext, CapturePictureActivity.class);
                capturePictureActivity.putExtra(mContext.getResources().getString(R.string.toolbar_title), "security_risk");
                capturePictureActivity.putExtra(mContext.getResources().getString(R.string.captureImagePosition), pos);
                capturePictureActivity.putExtra(mContext.getResources().getString(R.string.security_option_id), optionId);
                Util.setSendPhotoImageTo(Util.SECURITY_RISK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, mContext.getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                Activity origin = (Activity) mContext;
                origin.startActivityForResult(capturePictureActivity, pos);
                break;
        }
    }

    public class SecurityViewHolder extends RecyclerView.ViewHolder {
        CustomFontTextview mCustomTitle;
        Spinner mCustomSpinner;
        RoundedCornerImageView mCameraImage;
        CustomFontEditText mEditTextView;

        public SecurityViewHolder(View itemView) {
            super(itemView);
            mCustomTitle = (CustomFontTextview) itemView.findViewById(R.id.risk_title);
            mCameraImage = (RoundedCornerImageView) itemView.findViewById(R.id.risk_captureimage);
            mCustomSpinner = (Spinner) itemView.findViewById(R.id.risk_dropdown);
            mEditTextView = (CustomFontEditText) itemView.findViewById(R.id.customedittext);
        }
    }

    private void setControlTypeVisibility(String controlType, final SecurityRiskAdapter.SecurityViewHolder holder, final int listPos, int selectedPosition) {
        modelObject.get(listPos).setDataFilled(false);
        if (controlType.equalsIgnoreCase("Textbox")) {
            holder.mEditTextView.setVisibility(View.VISIBLE);
            holder.mEditTextView.setText(modelObject.get(listPos).getReasonTxt());
            optionHolder.setmUserComments(modelObject.get(listPos).getReasonTxt());
            modelObject.get(listPos).setEditFieldVisible(true);
            holder.mCameraImage.setVisibility(View.GONE);
        } else {
            modelObject.get(listPos).setEditFieldVisible(false);
            holder.mEditTextView.setVisibility(View.GONE);
            holder.mCameraImage.setVisibility(View.VISIBLE);

        }
    }
}
