package com.sisindia.ai.android.commons;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.network.GPSEnableDisableMO;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by IOPS on 2/9/2018.
 */

public class GPSEnableDisableTracking extends Service {

    private static final long SERVER_POLLING_INTERVAL = 5 * 60 * 1000;
    //    private static final long SERVER_POLLING_INTERVAL = 15 * 1000;
    private AppPreferences mAppPreference;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private Handler handler = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        mAppPreference = new AppPreferences(this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (mAppPreference.getDutyStatus()) {
                        sendGPSEnabledDisabledStatus();
                        handler.postDelayed(this, SERVER_POLLING_INTERVAL);
                    } else {
                        stopSelf();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.post(runnable);

        return START_STICKY;
    }

    private void sendGPSEnabledDisabledStatus() {

        try {
            if (Util.isNetworkConnected()) {
                if (mAppPreference.getDutyStatus()) {

                    GPSEnableDisableMO model = new GPSEnableDisableMO();
                    model.setAreaInspectorId(mAppPreference.getAppUserId());
                    model.setCapturedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                    model.setGpsActive(isGPSEnabled());

                    new SISClient(this).getApi().postGpsStatus(model, new Callback<CommonResponse>() {
                        @Override
                        public void success(CommonResponse commonResponse, Response response) {
                            if (commonResponse.getStatusCode() == 200) {
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Crashlytics.logException(error);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    public boolean isGPSEnabled() {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}