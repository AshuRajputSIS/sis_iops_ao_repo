package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitBillingCheckList {

@SerializedName("Id")
@Expose
private Integer id;
@SerializedName("UnitId")
@Expose
private Integer unitId;
@SerializedName("BillChecklistId")
@Expose
private Integer billingCheckListId;
@SerializedName("IsActive")
@Expose
private Boolean isActive;
@SerializedName("CreatedDateTime")
@Expose
private String createdDateTime;
@SerializedName("UpdatedDateTime")
@Expose
private String updatedDateTime;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getUnitId() {
return unitId;
}

public void setUnitId(Integer unitId) {
this.unitId = unitId;
}

public Integer getBillingCheckListId() {
return billingCheckListId;
}

public void setBillingCheckListId(Integer billingCheckListId) {
this.billingCheckListId = billingCheckListId;
}

public Boolean getIsActive() {
return isActive;
}

public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

public String getCreatedDateTime() {
return createdDateTime;
}

public void setCreatedDateTime(String createdDateTime) {
this.createdDateTime = createdDateTime;
}

public String getUpdatedDateTime() {
return updatedDateTime;
}

public void setUpdatedDateTime(String updatedDateTime) {
this.updatedDateTime = updatedDateTime;
}

}

