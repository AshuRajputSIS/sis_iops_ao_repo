package com.sisindia.ai.android.fcmnotification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.fcmnotification.job.BackGroundSyncJob;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.logger.MyLogger;

import java.util.Random;

/**
 * Created by shankar on 10/11/16.
 */

public class IOPSMessageReceivingService extends FirebaseMessagingService {

    private static final String TAG = IOPSMessageReceivingService.class.getSimpleName();
    private JobManager jobManager;
    private NotificationUtils notificationUtils;
    private AppPreferences appPreferences;
    private Random random = new Random();
    private Gson gson;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.v(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage == null) {
            return;
        } else {
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.v(TAG, "Data Payload: " + remoteMessage.getData().toString());
                appPreferences = new AppPreferences(getApplicationContext());
                String fcmMessage = remoteMessage.getData().toString();
                if (fcmMessage.contains("notification=")) {
                    fcmMessage = fcmMessage.replace("notification=", "\"notification\" :");
                }
                Log.v(TAG, "fcmMessage Payload: " + fcmMessage);
                notificationUtils = new NotificationUtils(getApplicationContext());
                try {
                    BaseNotification baseNotification = IOPSApplication.getGsonInstance().fromJson(fcmMessage, BaseNotification.class);
                    handleDataMessage(baseNotification);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                    MyLogger.setup(this);
                    NotificationReceivingMO notificationReceivingMO = new NotificationReceivingMO();
                    notificationReceivingMO.setId(0); // if error
                    DateTimeFormatConversionUtil conversionUtil = new DateTimeFormatConversionUtil();
                    notificationReceivingMO.setAppUserId(appPreferences.getAppUserId());
                    notificationReceivingMO.setRecievedDateTime(conversionUtil.getCalendarInstance().getTimeInMillis());
                    notificationUtils.notificationUpdate(false, notificationReceivingMO);
                }
            }
        }
    }

    private void handleDataMessage(BaseNotification baseNotification) {
        NotificationReceivingMO notificationReceivingMO = baseNotification.getNotificationReceivingMO();
        try {
            DateTimeFormatConversionUtil conversionUtil = new DateTimeFormatConversionUtil();
            notificationReceivingMO.setAppUserId(appPreferences.getAppUserId());
            notificationReceivingMO.setRecievedDateTime(conversionUtil.getCalendarInstance().getTimeInMillis());

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", notificationReceivingMO.getName());
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
                //  silentNotification(notificationReceivingMO);
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);
                resultIntent.putExtra("message", notificationReceivingMO.getName());
                showNotificationMessage(getApplicationContext(), notificationReceivingMO.getName(), "", "", resultIntent, notificationReceivingMO);
            }
            IOPSApplication.getInstance().getNotificationStatementDB().newNotificationInsertion(notificationReceivingMO);
            silentNotification(notificationReceivingMO);
        } catch (Exception e) {
            MyLogger.setup(this);
            Crashlytics.logException(e);
            notificationUtils.notificationUpdate(false, notificationReceivingMO);
            e.printStackTrace();
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent,
                                         NotificationReceivingMO notificationReceivingMO) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    public void silentNotification(NotificationReceivingMO notificationReceivingMO) {

        if (null != notificationReceivingMO) {

            /**
             *
             * API Call is added to background job
             *
             */

            jobManager = IOPSApplication.getInstance().getJobManager();
            jobManager.addJobInBackground(new BackGroundSyncJob(notificationReceivingMO));


            Intent intent = new Intent();
            if (Config.AKR_DUE.equalsIgnoreCase(notificationReceivingMO.getName())) {
                if (!TextUtils.isEmpty(notificationReceivingMO.getUnitList())) {
                    intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(HomeActivity.OPEN_AKR_FROM_NOTIFICATION, true);
                    appPreferences.setFilterIds(notificationReceivingMO.getUnitList());
                    //openOnBardingActivity(OnBoardingFactory.MY_UNIT, 6);
                }
            } else if (Config.BILL_COLLECTION.equalsIgnoreCase(notificationReceivingMO.getName())
                    || Config.BILL_SUBMISSION.equalsIgnoreCase(notificationReceivingMO.getName())
                    || Config.BILL_COLLECTION_DUE.equalsIgnoreCase(notificationReceivingMO.getName())
                    || Config.BILL_COLLECTION_OVERDUE.equalsIgnoreCase(notificationReceivingMO.getName())) {
                if (!TextUtils.isEmpty(notificationReceivingMO.getUnitList())) {
                    intent = new Intent(this, HomeActivity.class);
                    intent.putExtra(HomeActivity.OPEN_MY_UNIT_FROM_NOTIFICATION, true);
                    appPreferences.setFilterIds(notificationReceivingMO.getUnitList());
                    //openOnBardingActivity(OnBoardingFactory.MY_UNIT, 6);
                }
            } else if (Config.CUSTOM.equalsIgnoreCase(notificationReceivingMO.getName())) {
                intent = new Intent(this, NotificationWebView.class);
                intent.putExtra(getResources().getString(R.string.notification_url), notificationReceivingMO.getCallbackUrl());
                intent.putExtra(getResources().getString(R.string.notification_msg), notificationReceivingMO.getMessage());
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, Config.NOTIFICATION_ID, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Bitmap logoIcon = BitmapFactory.decodeResource(getResources(), R.drawable.iops);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.iops)
                    .setContentTitle("iOPS Notification Message")
                    .setContentText(notificationReceivingMO.getMessage())
                    .setAutoCancel(true)
                    .setLargeIcon(Bitmap.createScaledBitmap(logoIcon, 128, 128, false))
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(random.nextInt(), notificationBuilder.build());

        }
    }

    private void openOnBardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(this, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }
}
