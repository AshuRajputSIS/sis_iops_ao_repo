package com.sisindia.ai.android.database.rotaDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;
import com.sisindia.ai.android.rota.models.RotaModelMO;
import com.sisindia.ai.android.rota.models.RotaTask;
import com.sisindia.ai.android.rota.models.RotaTaskActivityListMO;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.rota.models.UnitTask;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 24/6/16.
 */

public class RotaLevelInsertionStatementDB extends SISAITrackingDB {


    public RotaLevelInsertionStatementDB(Context context) {
        super(context);
    }

    /**
     * @param rotaModelMOList
     */
//    public void insertRotaTable(RotaModelMO rotaModelMO, boolean isManualSync) {
    public void insertRotaTable(List<RotaModelMO> rotaModelMOList, boolean isManualSync) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (RotaModelMO rotaModelMO : rotaModelMOList) {

                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID, rotaModelMO.getEmployeeId());
                    values.put(TableNameAndColumnStatement.ROTA_ID, rotaModelMO.getRotaId());
                    values.put(TableNameAndColumnStatement.ROTA_WEEK, rotaModelMO.getRotaWeek());
                    values.put(TableNameAndColumnStatement.ROTA_MONTH, rotaModelMO.getRotaMonth());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID, rotaModelMO.getEmployeeId());
                    values.put(TableNameAndColumnStatement.ROTA_PUBLISHED_DATE_TIME, rotaModelMO.getRotaPublishedDateTime());
                    values.put(TableNameAndColumnStatement.ROTA_STATUS_ID, rotaModelMO.getRotaStatusId());
                    values.put(TableNameAndColumnStatement.ROTA_YEAR, rotaModelMO.getRotaYear());
                    values.put(TableNameAndColumnStatement.ROTA_DATE, rotaModelMO.getRotaDate());
                    if (isManualSync) {
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.ROTA_ID,
                                rotaModelMO.getRotaId(), "id", "rota", "id", sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                            IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTableById("rota", genericUpdateTableMO.getId(), sqlite);
                            sqlite.insert(TableNameAndColumnStatement.ROTA_TABLE, null, values);
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.ROTA_TABLE, null, values);
                        }
                        IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteRotaTaskTableById(TableNameAndColumnStatement.ROTA_TASK_TABLE, rotaModelMO.getRotaId(), sqlite);
                    } else {
                        sqlite.insert(TableNameAndColumnStatement.ROTA_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Crashlytics.logException(sqliteConstraintException);
            } catch (SQLiteException sqliteException) {
                Crashlytics.logException(sqliteException);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertRotaTaskTable(List<RotaTask> rotaTaskList, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (RotaTask rotaTask : rotaTaskList) {
                    values.put(TableNameAndColumnStatement.ROTA_ID, rotaTask.getRotaId());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_ID, rotaTask.getRotaTaskId());
                    values.put(TableNameAndColumnStatement.TASK_ID, rotaTask.getTaskId());
                    values.put(TableNameAndColumnStatement.SOURCE_GEO_LATITUDE, rotaTask.getSourceGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LATITUDE, rotaTask.getDestinationGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LONGITUDE, rotaTask.getDestinationGeoLongitude());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME, rotaTask.getEstimatedTravelTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_DISTANCE, rotaTask.getEstimatedDistance());
                    values.put(TableNameAndColumnStatement.TOTAL_ESTIMATED_TIME, rotaTask.getTotalEstimatedTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TRAVEL_TIME, rotaTask.getActualTravelTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_DISTANCE, rotaTask.getActualDistance());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME, rotaTask.getEstimatedTaskExecutionStartTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_END_TIME, rotaTask.getEstimatedTaskExecutionEndTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_START_TIME, String.valueOf(rotaTask.getActualTaskExecutionStartTime()));
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_END_TIME, String.valueOf(rotaTask.getActualTaskExecutionEndTime()));
                    values.put(TableNameAndColumnStatement.TASK_SEQUENCE_NO, rotaTask.getTaskSequenceNo());
                    sqlite.insert(TableNameAndColumnStatement.ROTA_TASK_TABLE, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Crashlytics.logException(sqliteConstraintException);
            } catch (SQLiteException sqliteException) {
                Crashlytics.logException(sqliteException);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }

    }

    public void insertTaskTable(List<UnitTask> rotaTaskList, int is_new, int is_synced, boolean isManualSync) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (UnitTask rotaTask : rotaTaskList) {
                    values.put(TableNameAndColumnStatement.TASK_ID, rotaTask.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, rotaTask.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, rotaTask.getUnitName());
                    values.put(TableNameAndColumnStatement.TASK_TYPE_ID, rotaTask.getTaskTypeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_ID, rotaTask.getAssigneeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_NAME, rotaTask.getAssigneeName());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME, rotaTask.getEstimatedExecutionTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME, rotaTask.getActualExecutionTime());
                    if (rotaTask.getTaskStatusId() == 4) {
                        values.put(TableNameAndColumnStatement.TASK_STATUS_ID, 3);
                    } else {
                        values.put(TableNameAndColumnStatement.TASK_STATUS_ID, rotaTask.getTaskStatusId());

                    }
                    // for start task api now as update api and to be synced in adapter
                    if (rotaTask.getTaskStatusId() == 1 || rotaTask.getTaskStatusId() == 2) {
                        values.put(TableNameAndColumnStatement.IS_TASK_STARTED, rotaTask.getTaskStatusId());

                    } else {
                        values.put(TableNameAndColumnStatement.IS_TASK_STARTED, RotaStartTaskEnum.InActive.getValue());
                    }
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, rotaTask.getDescription());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME, rotaTask.getEstimatedTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME, rotaTask.getActualTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.IS_AUTO, rotaTask.getIsAutoCreation());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, rotaTask.getCreatedDateTime());
                    values.put(TableNameAndColumnStatement.CREATED_BY_ID, rotaTask.getCreatedById());
                    values.put(TableNameAndColumnStatement.CREATED_BY_NAME, rotaTask.getCreatedByName());
                    values.put(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED, rotaTask.getApprovedById());
                    values.put(TableNameAndColumnStatement.ASSIGNED_BY_ID, rotaTask.getAssignedById());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, rotaTask.getBarrackId());
                    values.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT, rotaTask.getTaskExecutionResult());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME, rotaTask.getEstimatedTaskExecutionEndDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME, rotaTask.getActualTaskExecutionEndDateTime());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, rotaTask.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.IS_NEW, is_new);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, is_synced);

                    String[] createdStringDate = rotaTask.getCreatedDateTime().split(" ");
                    values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                    values.put(TableNameAndColumnStatement.SELECT_REASON_ID, rotaTask.getSelectReasonId());
                    values.put(TableNameAndColumnStatement.OTHER_TASK_REASON_ID, rotaTask.getOtherReasonId());
                    if (isManualSync) {
                        //  String count_Id,int id,String seq_id,String table_Name,String column_Name, SQLiteDatabase sqlite
                        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance()
                                .getTableSeqId(TableNameAndColumnStatement.TASK_ID, rotaTask.getId(), TableNameAndColumnStatement.ID
                                        , TableNameAndColumnStatement.TASK_TABLE, TableNameAndColumnStatement.ID, sqlite);
                        if (genericUpdateTableMO.getIsAvailable() != 0) {
                           /* sqlite.update(TableNameAndColumnStatement.TASK_TABLE, values,
                                    TableNameAndColumnStatement.ID + " = " + genericUpdateTableMO.getId(), null);*/
                            int deleteStatus = IOPSApplication.getInstance()
                                    .getDeletionStatementDBInstance()
                                    .deleteTableByIdStatus(TableNameAndColumnStatement.TASK_TABLE,
                                            genericUpdateTableMO.getId(), sqlite);
                            if (deleteStatus > 0) {
                                sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
                            }
                        } else {
                            sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
                        }

                    } else {
                        sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);
                    }
                }
                sqlite.setTransactionSuccessful();
                updateTempTaskId(sqlite);
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Crashlytics.logException(sqliteConstraintException);
            } catch (SQLiteException sqliteException) {
                Crashlytics.logException(sqliteException);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    private void updateTempTaskId(SQLiteDatabase sqLiteDatabase) {

        String updateStmt = " update " + TableNameAndColumnStatement.TASK_TABLE + " set " +
                TableNameAndColumnStatement.TASK_ID + " = -(select " +
                TableNameAndColumnStatement.ID + " from " + TableNameAndColumnStatement.TASK_TABLE +
                " where " + TableNameAndColumnStatement.TASK_ID + " =  " + 0 + " ) " +
                " where " + TableNameAndColumnStatement.TASK_ID + " = " + 0;

        Timber.d(" Update Temp Task id  %s", updateStmt);
        Cursor cursor = null;
        try {
            cursor = sqLiteDatabase.rawQuery(updateStmt, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        }
    }

    /**
     * RotaMO task Activity List Inserted
     *
     * @param rotaTaskActivityListMO
     */
    public void insertRotaTaskActivityList(RotaTaskActivityListMO rotaTaskActivityListMO) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                for (RotaTaskListMO rotaTaskListMO : rotaTaskActivityListMO.getRotaTaskListMO()) {
                    // Inserting Row
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ROTA_TASK_NAME, rotaTaskListMO.getRotaTaskName());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_ID, rotaTaskListMO.getRotaTaskActivityId());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_BLACK_ICON, rotaTaskListMO.getRotaTaskBlackIcon());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_WHITE_ICON, rotaTaskListMO.getRotaTaskWhiteIcon());
                    sqlite.insert(TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_LIST, null, values);
                }
                sqlite.setTransactionSuccessful();
            } catch (SQLiteConstraintException sqliteConstraintException) {
                Crashlytics.logException(sqliteConstraintException);
            } catch (SQLiteException sqliteException) {
                Crashlytics.logException(sqliteException);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }
}