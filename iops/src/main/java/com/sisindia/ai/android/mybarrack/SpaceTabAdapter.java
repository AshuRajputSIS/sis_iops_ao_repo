package com.sisindia.ai.android.mybarrack;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitatriskpoa.UnitAtRiskListClickListener;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.DataMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 29/7/16.
 */

public class SpaceTabAdapter extends
        RecyclerView.Adapter<SpaceTabAdapter.SpaceTavViewHolder> {

    public void setUnitAtRiskListClickListener(UnitAtRiskListClickListener unitAtRiskListClickListener) {
        this.unitAtRiskListClickListener = unitAtRiskListClickListener;
    }

    private UnitAtRiskListClickListener unitAtRiskListClickListener;

    Context mcontext;
    DataMO mData;


    public SpaceTabAdapter(Context mcontext, DataMO mData) {
        this.mcontext = mcontext;
        this.mData = mData;
    }


    @Override
    public SpaceTavViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.barrackinspection_space_tab_item, parent, false);
        SpaceTavViewHolder unitAtRiskViewHolder = new SpaceTavViewHolder(view);

        return unitAtRiskViewHolder;
    }

    @Override
    public void onBindViewHolder(SpaceTavViewHolder holder, final int position) {
        SpaceTavViewHolder unitAtRiskViewHolder = holder;

        RiskDetailModelMO reRiskDetailModelMO = mData.getRiskDetailModels().get(position);




    }

    @Override
    public int getItemCount() {
        return mData.getRiskDetailModels().size();

    }


    public class SpaceTavViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @Bind(R.id.question_id)
        CustomFontTextview Question;
        @Bind(R.id.option)
        RadioGroup radioGroup;
        @Bind(R.id.option_1)
        RadioButton option1;
        @Bind(R.id.option_2)
        RadioButton option2;
        @Bind(R.id.option_txt)
        RadioButton optionTxt;




        public SpaceTavViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            radioGroup.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.option:

                    unitAtRiskListClickListener.onItemClick(mData.getRiskDetailModels().get(getLayoutPosition()));


            }
        }
    }
}
