package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.models.LookupModel;

import java.util.List;

/**
 * Created by Durga Prasad on 02-04-2016.
 */
public class FilterUnitsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<LookupModel> filterUnitsList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public FilterUnitsAdapter(Context context) {
        this.context = context;

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setFilterUnitsData(List<LookupModel> filterUnitsList) {
        this.filterUnitsList = filterUnitsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder filterUnitsViewHolder = null;
        View itemView = LayoutInflater.from(context).inflate(R.layout.myunits_filter_units_row, parent, false);
        filterUnitsViewHolder = new FilterUnitsViewHolder(itemView);
        return filterUnitsViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        FilterUnitsViewHolder filterUnitsViewHolder = (FilterUnitsViewHolder) holder;
        filterUnitsViewHolder.filterUnitsTxt.setText(filterUnitsList.get(position).getName());


    }


    @Override
    public int getItemCount() {
        int count = 0;
        if (filterUnitsList != null) {
            count = filterUnitsList.size() == 0 ? 0 : filterUnitsList.size();
        }
        return count;
    }

    private Object getObject(int position) {
        return filterUnitsList.get(position);
    }

    public class FilterUnitsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView filterUnitsTxt;

        public FilterUnitsViewHolder(View view) {
            super(view);
            this.filterUnitsTxt = (TextView) view.findViewById(R.id.myunits_filter_units_txt);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }
}

