package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("Name")

    public String Name;
    @SerializedName("AddressLine1")

    public String AddressLine1;
    @SerializedName("AddressLine2")

    public String AddressLine2;
    @SerializedName("Pincode")

    public String Pincode;
    @SerializedName("CityId")

    public Integer CityId;
    @SerializedName("GeoLocation")

    public String GeoLocation;
    @SerializedName("StrokeColor")

    public Object StrokeColor;
    @SerializedName("FillColor")

    public Object FillColor;
    @SerializedName("CityName")

    public String CityName;
    @SerializedName("StateName")

    public String StateName;
    @SerializedName("DistrictName")

    public String DistrictName;
    @SerializedName("CountryName")

    public String CountryName;
    @SerializedName("FullAddress")

    public String FullAddress;

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        AddressLine2 = addressLine2;
    }

    public Integer getCityId() {
        return CityId;
    }

    public void setCityId(Integer cityId) {
        CityId = cityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public Object getFillColor() {
        return FillColor;
    }

    public void setFillColor(Object fillColor) {
        FillColor = fillColor;
    }

    public String getFullAddress() {
        return FullAddress;
    }

    public void setFullAddress(String fullAddress) {
        FullAddress = fullAddress;
    }

    public String getGeoLocation() {
        return GeoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        GeoLocation = geoLocation;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public Object getStrokeColor() {
        return StrokeColor;
    }

    public void setStrokeColor(Object strokeColor) {
        StrokeColor = strokeColor;
    }
}