package com.sisindia.ai.android.fcmnotification.fcmunits;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

/**
 * Created by compass on 2/23/2017.
 */

public class FCMUnitBarrackDeletion extends SISAITrackingDB {

    public FCMUnitBarrackDeletion(Context context) {
        super(context);
    }

    public boolean deleteUnitBarrackRecord(int unitBarrackId) {

        boolean isDeletedSuccessfully;
        SQLiteDatabase sqlite = this.getWritableDatabase();

        synchronized (sqlite) {

            try {

                if (unitBarrackId != 0) {
                    sqlite.beginTransaction();

                    IOPSApplication.getInstance().getDeletionStatementDBInstance()
                            .deleteRecordById(unitBarrackId, TableNameAndColumnStatement.ID,
                                    TableNameAndColumnStatement.UNIT_BARRACK_TABLE, sqlite);

                    sqlite.setTransactionSuccessful();


                    isDeletedSuccessfully = true;
                } else {
                    isDeletedSuccessfully = false;
                }
            } catch (Exception exception) {
                //sqlite.endTransaction();
                Crashlytics.logException(exception);
                isDeletedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
            return isDeletedSuccessfully;
        }
    }


    public void deleteUnitBarrack(String tableName,SQLiteDatabase sqlite ){

        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

    }


    public void deleteUnitBarrackUsingUnitId(String tableName,SQLiteDatabase sqlite, int unitId ){

        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, TableNameAndColumnStatement.UNIT_ID + " = "+unitId, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

    }
}
