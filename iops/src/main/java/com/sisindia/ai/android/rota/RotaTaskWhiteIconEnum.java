package com.sisindia.ai.android.rota;

import com.sisindia.ai.android.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 10-05-2016.
 */
public enum RotaTaskWhiteIconEnum {


    Day_Checking(R.drawable.add_day_check),
    Night_Checking(R.drawable.add_night_check),
    Barrack_Inspection(R.drawable.add_barrack_check),
    Client_Coordination(R.drawable.add_client_coordination),
    Bill_Submission(R.drawable.add_bill_submission),
    Bill_Collection(R.drawable.add_collection),
    Onsite_Training(R.drawable.add_onsite_training),
    Raising(R.drawable.add_other),
    Recruitment(R.drawable.add_other),
    Kit_Replacement(R.drawable.add_kit_replacement),
    Others(R.drawable.add_other);


    private static Map map = new HashMap<>();

    static {
        for (RotaTaskWhiteIconEnum taskTypeIcon : RotaTaskWhiteIconEnum.values()) {
            map.put(taskTypeIcon.value, taskTypeIcon);
        }
    }

    private int value;

    RotaTaskWhiteIconEnum(int value) {
        this.value = value;
    }

    public static RotaTaskWhiteIconEnum valueOf(int taskType) {
        return (RotaTaskWhiteIconEnum) map.get(taskType);
    }

    public int getValue() {
        return value;
    }
}
