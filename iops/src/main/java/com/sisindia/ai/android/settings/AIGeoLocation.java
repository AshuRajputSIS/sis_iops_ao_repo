package com.sisindia.ai.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.appuser.AppUserLocationMO;
import com.sisindia.ai.android.appuser.AppuserResponseMO;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.net.HttpURLConnection;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by shushrut on 6/9/16.
 */
public class AIGeoLocation extends BaseActivity {
    private static final int AI_GEO_PIC_CODE = 100;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.altenate_number_edittext)
    CustomFontEditText mAlternateNumber;
    @Bind(R.id.edit_photo)
    CircleImageView mEditPhoto;
    @Bind(R.id.alternateNumberLabel)
    CustomFontTextview alternateNumberLabel;
    @Bind(R.id.alternateAddressLabel)
    CustomFontTextview alternateAddressLabel;
    @Bind(R.id.profilePictureLabel)
    CustomFontTextview profilePictureLabel;
    @Bind(R.id.ai_profile_pic)
    CircleImageView mAIProfilePic;
    @Bind(R.id.altenate_address_edittext)
    CustomFontEditText mAlternateAddress;
    @Bind(R.id.setting_parent)
    LinearLayout mParentView;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private AppUserLocationMO mAppUserLocation;
    private String mCapturedPicture = "";
    private boolean isImageEdit;
    private Resources mResources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.area_inspector_location);
        ButterKnife.bind(this);
        mResources = getResources();
        isImageEdit = false;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mAppUserLocation = new AppUserLocationMO();

        if (util != null) {
            alternateAddressLabel.setText(util.createSpannableStringBuilder(getResources().getString(R.string.alternate_address)));
        }
        setUpToolBar();
    }

    //initiating toolbar
    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.AI_Location));
        mAIProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.edit_profile_icon_new));
        AppUserData mData = IOPSApplication.getInstance().getSettingsLevelSelectionStatmentDB().
                getAIPhotoWithAddress(appPreferences.getAppUserId());
        if (mData != null) {
            if (mData.getmImageUrl() != null && !TextUtils.isEmpty(mData.getmImageUrl().trim()))
                mCapturedPicture = mData.getmImageUrl().trim();
            else
                mAIProfilePic.setImageResource(R.drawable.default_icon_ai);

            if (mData.getmUserAlternateNumber() != null && !TextUtils.isEmpty(mData.getmUserAlternateNumber().trim()))
                mAlternateNumber.setText(mData.getmUserAlternateNumber().trim());
            else
                mAlternateNumber.setText("");

            if (mData.getmUserAddress() != null && !TextUtils.isEmpty(mData.getmUserAddress().trim()))
                mAlternateAddress.setText(String.valueOf(mData.getmUserAddress().trim()));
            else
                mAlternateAddress.setText("");
        }
        loadAIImage();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_post_save) {
            insertDataToTable();
        }
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void insertDataToTable() {
        if (mAlternateAddress.getText().toString().trim().length() > 0) {
            String altermateMobileNumber = mAlternateNumber.getText().toString().trim();
            if (!TextUtils.isEmpty(altermateMobileNumber)) {
                if (!Util.isValidPhoneNumber(altermateMobileNumber)) {
                    snackBarWithMesg(mParentView, getResources().getString(R.string.mobile_validation));
                    return;
                }
                if (appPreferences.getMobileNumber().equalsIgnoreCase(altermateMobileNumber)) {
                    snackBarWithMesg(mParentView, mResources.getString(R.string.user_mobile_validation));
                    return;
                }

            } else {
                altermateMobileNumber = null;
            }
            String address = mAlternateAddress.getText().toString();
            IOPSApplication.getInstance().getSettingsLevelUpdateStatementDB().
                    updateAppUserTable(appPreferences.getAppUserId(),
                            Double.toString(GpsTrackingService.latitude),
                            Double.toString(GpsTrackingService.longitude),
                            altermateMobileNumber, mCapturedPicture, address);
            mAppUserLocation.setAddressLine1(address);
            mAppUserLocation.setLat(Double.toString(GpsTrackingService.latitude));
            mAppUserLocation.setLong(Double.toString(GpsTrackingService.longitude));
            mAppUserLocation.setAlternateContactNo(altermateMobileNumber);
            if (isImageEdit) {
                insertMetaDataToTable();
                triggerSyncAdapter();
            }
            triggerAPICall();
            finish();
        } else {
            snackBarWithMesg(mParentView, getResources().getString(R.string.alternate_address_mandatory));
        }
    }

    private void insertMetaDataToTable() {
        if (attachmentMetaDataModel != null) {
            IOPSApplication.getInstance().getInsertionInstance().insertIntoAttachmentDetails(attachmentMetaDataModel);
        }
    }

    private void triggerAPICall() {
        sisClient.getApi().syncAILocation(mAppUserLocation, new Callback<AppuserResponseMO>() {
            @Override
            public void success(AppuserResponseMO appuserResponseMO, Response response) {
                switch (appuserResponseMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        IOPSApplication.getInstance().getSettingsLevelUpdateStatementDB().updatePrimaryAddressId(1,
                                appuserResponseMO.getData().getAddressId());
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }

    @OnClick(R.id.edit_photo)
    public void captureImage() {
        Intent captureAIPhoto = new Intent(this, CapturePictureActivity.class);

        captureAIPhoto.putExtra(getResources().getString(R.string.toolbar_title), "");
        Util.setSendPhotoImageTo(Util.SETTINGS_AI_PIC);
        Util.initMetaDataArray(true);
        startActivityForResult(captureAIPhoto, AI_GEO_PIC_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == AI_GEO_PIC_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    isImageEdit = true;
                    attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                    attachmentMetaDataModel.setIsSave(1);
                    mCapturedPicture = attachmentMetaDataModel.getAttachmentPath();
                    mAIProfilePic.setImageURI(Uri.parse(attachmentMetaDataModel.getAttachmentPath()));
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    isImageEdit = false;
                }
            }
        }
    }

    /**
     * Loading  the AI image . If image is coming from LoadConfig then need to download and  display
     * else
     * Local uri and if local local uri is also not there then display the default image
     */

    private void loadAIImage() {
        if (null != mCapturedPicture && !TextUtils.isEmpty(mCapturedPicture)) {
            if (!(mCapturedPicture.contains("http"))) {
                mAIProfilePic.setImageURI(Uri.parse(mCapturedPicture));
            } else {
                Glide.with(AIGeoLocation.this)
                        .load(mCapturedPicture)
                        .placeholder(R.drawable.default_icon_ai)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mAIProfilePic);
            }
        } else {
            mAIProfilePic.setImageResource(R.drawable.default_icon_ai);
        }
    }
}
