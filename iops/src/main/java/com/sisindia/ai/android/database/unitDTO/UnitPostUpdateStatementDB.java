package com.sisindia.ai.android.database.unitDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import timber.log.Timber;

/**
 * Created by compass on 9/1/2017.
 */

public class UnitPostUpdateStatementDB extends SISAITrackingDB {

    public UnitPostUpdateStatementDB(Context context) {
        super(context);
    }

    public void updateNfcDeviceNo(String devcieNo, int unitPostId, int unitId){

        SQLiteDatabase sqlite = null;
        String updateQuery = "";
        if(unitPostId == -1) {
            updateQuery = " UPDATE " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                    " SET " + TableNameAndColumnStatement.DEVICE_NO + " = " + " '' " +
                    " WHERE " + TableNameAndColumnStatement.DEVICE_NO + " = '" + devcieNo +"'"
                    + " AND " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId;
        }
        else{
            updateQuery = " UPDATE " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                    " SET " + TableNameAndColumnStatement.DEVICE_NO + " = " + "'" +
                    devcieNo + "'" +
                    " WHERE " + TableNameAndColumnStatement.UNIT_POST_ID + " = " + unitPostId+
                    " AND " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId;
        }
        Timber.d("updateNfcDeviceNo  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            exception.printStackTrace();
        } finally {
            if(null!=sqlite && sqlite.isOpen()){

                sqlite.close();
            }
        }
    }
}
