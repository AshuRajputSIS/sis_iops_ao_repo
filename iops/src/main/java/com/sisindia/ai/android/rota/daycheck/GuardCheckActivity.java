package com.sisindia.ai.android.rota.daycheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;

public class GuardCheckActivity extends DayCheckBaseActivity
        implements View.OnClickListener, OnRecyclerViewItemClickListener, DialogClickListener {

    @Bind(R.id.guard_check_recyclerview)
    RecyclerView guardCheckRecyclerview;
    @Bind(R.id.start_check)
    TextView mStartCheck;
    GuardCheckAdapter mAdapter;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.minimum_guard_check_tv)
    TextView minimumGuardCheckMessage;
    @Bind(R.id.guard_check_header)
    View mStepperViewParent;
    @Bind(R.id.guardCheckParentLayout)
    LinearLayout guardCheckParentLayout;
    //public ArrayList<String> guardIdList;
    @Bind(R.id.task_client_check)
    View stepperFourView;
    @Bind(R.id.task_security_check)
    View stepperFiveView;
    @Bind(R.id.stepper_four_completed)
    View stepperFourViewCompleted;
    @Bind(R.id.stepper_three_completed)
    View stepperThreeViewCompleted;
    @Bind(R.id.framelayout_stepper_four)
    FrameLayout framelayout_stepper_four;
    @Bind(R.id.framelayout_stepper_five)
    FrameLayout framelayout_stepper_five;
    String data;
    @Bind(R.id.stepper_frame_six)
    FrameLayout mstepper_frame_six;
    @Bind(R.id.stepper_five_completed)
    View mstepper_five_completed;
    private int removedPosition;
    //    private ArrayList<View> viewList;
    // private ArrayList<GuardDetail> guardDetailsList;
    private int minimumGuardCheckcount;
    private int[] unitGuardCountAndCheckLimit;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_check);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.GUARD_CHECK));
        guardCheckRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        appPreferences = new AppPreferences(this);
        initRecyclerView();
        mStartCheck.setOnClickListener(this);

        //rotaTaskModel = (RotaTaskModel) SingletonUnitDetailsMO.getInstance().getUnitDetails();
        data = getResources().getString(R.string.dayCheckingTitle);
        if (rotaTaskModel != null) {
            setHeaderUnitName(rotaTaskModel.getUnitName());
            unitGuardCountAndCheckLimit = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getUnitGuardsCount(rotaTaskModel.getUnitId());
        }
        if (getIntent().hasExtra(getResources().getString(R.string.navigationcheckingTitle))) {
            data = getIntent().getExtras().getString(getResources().getString(R.string.navigationcheckingTitle), getResources().getString(R.string.dayCheckingTitle));

            if (data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                //setBaseToolbar(toolbar, data);
                stepperFourView.setVisibility(View.GONE);
                stepperFourViewCompleted.setVisibility(View.GONE);
                stepperThreeViewCompleted.setVisibility(View.GONE);
                framelayout_stepper_four.setVisibility(View.GONE);
                framelayout_stepper_five.setVisibility(View.GONE);
                mstepper_frame_six.setVisibility(View.GONE);
                stepperFiveView.setVisibility(View.GONE);
                mstepper_five_completed.setVisibility(View.GONE);
            }
        }
        minimumGuardCheckcount = validateCountOfGuardsChecked();
        getStepperViews(mStepperViewParent);
        getTaskExecutionResult();
        if (data != null && data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
            updateTotalTaskCount();
        }
        // guardDetailsList = new ArrayList<>();
        //  guardIdList = new ArrayList<>();

        switch (minimumGuardCheckcount) {
            case 0:
                minimumGuardCheckMessage.setText(getResources().getString(R.string.NO_GUARDS_AVAILABLE));
                break;
            case 1:
                minimumGuardCheckMessage.setText(minimumGuardCheckcount + " " + getResources().getString(R.string.one_guard_check));
                break;
            default:
                minimumGuardCheckMessage.setText(minimumGuardCheckcount + " " + getResources().getString(R.string.multiple_guards_check));
                break;
        }
    }

    private void initRecyclerView() {
        mAdapter = new GuardCheckAdapter(this);
        if (Util.getmGuardDetailsHolder() != null && Util.mGuardDetailsHolder.size() > 0) {
            mAdapter.setGuardDetailsList(Util.getmGuardDetailsHolder());
        }
        mAdapter.setOnRecyclerViewItemClickListener(this);
        guardCheckRecyclerview.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_day_check, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.daycheck_done:

                if (minimumGuardCheckcount <= Util.getmGuardDetailsHolder().size())
                    saveGuardCheckDetailsToDB();
                else
                    snackBarWithMesg(guardCheckParentLayout, getResources().getString(R.string.GUARD_CHECK_VALIDATION_MESSAGE));

                //Writing below code for few AIs facing strength issues
                /*if (Util.getmGuardDetailsHolder().size() >= 1) {
                    if (minimumGuardCheckcount <= Util.getmGuardDetailsHolder().size())
                        saveGuardCheckDetailsToDB();
                    else
                        showUserAlert();
                } else
                    snackBarWithMesg(guardCheckParentLayout, getResources().getString(R.string.GUARD_CHECK_VALIDATION_MESSAGE));*/

                break;

            case android.R.id.home:
                showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showUserAlert() {

        String styledText = "<font color='red'>Warning:</font><br>You are skipping minimum guard check <font color='red'><i>(10% of shift strength)</i>!</font><br>" +
                "This action <font color='red'>may be</font> reviewed/rejected.<br><br>Click Ok to save?";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml(styledText));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                saveGuardCheckDetailsToDB();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    private void saveGuardCheckDetailsToDB() {
        OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();

        if (mOnStepperUpdateListener != null)
            mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(2).name(), DayCheckEnum.Guard_Check.getValue());

        getTaskExecutionResult();

        if (dayNightChecking != null)
            dayNightChecking.setGuardDetails(Util.getmGuardDetailsHolder());

        dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
        setTaskExecutionResult();
        Util.refreshCheckedGuardIdHolder();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_check:
                startActivityForResult(new Intent(GuardCheckActivity.this, GuardDetailsActivity.class), Constants.GUARD_DETAILS_CODE);
                break;
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        removedPosition = position;
        showConfirmationDialog(getResources().getInteger(R.integer.GUARD_CHECK_DELETION_CONFIRAMTION_DIALOG));
    }

    private int validateCountOfGuardsChecked() {

        int totalGuardStrength = unitGuardCountAndCheckLimit[0];
        int checkLimitInPercentage = unitGuardCountAndCheckLimit[1];

        int needTOCheckCount = 0;
        /*int reqGuardCount = guardCount / 3;
        if (reqGuardCount <= 10 && reqGuardCount >= 0) {
            needTOCheckCount = 1;
        } else if (reqGuardCount != 0) {
            float guardCountFloat = ((float) reqGuardCount) / 10;
            needTOCheckCount = Math.round(guardCountFloat);
        }*/

        if (totalGuardStrength > 0 && checkLimitInPercentage > 0) {
            float guardCountFloat = ((float) totalGuardStrength) / checkLimitInPercentage;
            needTOCheckCount = Math.round(guardCountFloat);
        }

        return needTOCheckCount;
    }

    private synchronized void removeImageAttachmentModel(ArrayList<GuardDetail> guardDetails, int removedPosition) {
        if (null != guardDetails && guardDetails.size() != 0) {
            if (getAttachmentMetaArrayList() != null && getAttachmentMetaArrayList().size() != 0) {
                for (int i = getAttachmentMetaArrayList().size() - 1; i >= 0; i--) {
                    if (guardDetails.get(removedPosition).getEmployeeNo()
                            .equalsIgnoreCase(getAttachmentMetaArrayList()
                                    .get(i).getAttachmentSourceCode())
                            || guardDetails.get(removedPosition).getEmployeeId() == getAttachmentMetaArrayList()
                            .get(i).getGuradId()) {
                        getAttachmentMetaArrayList().remove(i);
                        Timber.d("attachmentMetaArrayList removed  == %s", i);
                    }
                }
            }
        }
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        if (clickType == 2) {
            if (null != Util.getmGuardDetailsHolder() && Util.getmGuardDetailsHolder().size() != 0) {
                removeImageAttachmentModel(Util.getmGuardDetailsHolder(), removedPosition);
                CheckedGuardDetails checkedGuardDetails = appPreferences.getCheckedGuardDetails();
                if (checkedGuardDetails != null && checkedGuardDetails.checkedGuardDetailsMap != null) {
                    if (checkedGuardDetails.checkedGuardDetailsMap.size() != 0) {
                        checkedGuardDetails.checkedGuardDetailsMap.remove(mAdapter.getGuardDetailsList().get(removedPosition).getEmployeeNo());
                    }
                }
                removeIssue(Util.getmGuardDetailsHolder(), removedPosition);
                Util.getmGuardDetailsHolder().remove(removedPosition);
                setTaskExecutionResult();

                appPreferences.setCheckedGuardDetails(checkedGuardDetails);
                mAdapter.notifyItemRemoved(removedPosition);
            } else {
                if (mOnStepperUpdateListener != null) {
                    mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(2).name(), DayCheckEnum.Guard_Check.getValue());
                }
            }
        } else if (clickType == 0) {
            if (null != Util.getmGuardDetailsHolder() && Util.getmGuardDetailsHolder().size() != 0) {
                appPreferences.setCheckedGuardDetails(null);
                Util.getmGuardDetailsHolder().clear();
            }
            if (mOnStepperUpdateListener != null) {
                mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(2).name(), DayCheckEnum.Guard_Check.getValue());
            }
            finish();
        } else {
            finish();
        }
    }

    private synchronized void removeIssue(ArrayList<GuardDetail> guardDetails, int removedPosition) {
        if (guardDetails != null & guardDetails.size() != 0) {
            ArrayList<IssuesMO> issuesMO = Util.getmGrievanceModelHolder();
            if (null != issuesMO && issuesMO.size() != 0) {
                for (int i = 0; i < issuesMO.size(); i++) {
                    if (issuesMO.get(i).getGuardCode().equalsIgnoreCase(String.valueOf(guardDetails.get(removedPosition).getEmployeeNo()))) {
                        issuesMO.remove(i);
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.GUARD_DETAILS_CODE) {
            if (data != null) {
                GuardDetail guardDetails = data.getParcelableExtra(Constants.GUARD_DETAILS);
                if (guardDetails != null) {
                    if (null != Util.getmGuardDetailsHolder() && Util.getmGuardDetailsHolder().size() != 0) {
                        for (int i = 0; i < Util.getmGuardDetailsHolder().size(); i++) {
                            if (Util.getmGuardDetailsHolder().get(i).getEmployeeNo().equalsIgnoreCase(guardDetails.getEmployeeNo())) {
                                Util.getmGuardDetailsHolder().remove(i);
                            }
                        }
                    }
                    Util.setmGuardDetailsHolder(guardDetails);
                    mAdapter.setGuardDetailsList(Util.getmGuardDetailsHolder());
                    mAdapter.setOnRecyclerViewItemClickListener(this);
                    guardCheckRecyclerview.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.notifyItemRemoved(removedPosition);
                }
            }
        }
    }
}
