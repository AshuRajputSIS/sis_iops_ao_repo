package com.sisindia.ai.android.rota;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum PostStatusEnum {

    CHECKED(1),
    UNCHECKED(0);
    private static Map map = new HashMap<>();

    static {
        for (PostStatusEnum postStatusEM : PostStatusEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    PostStatusEnum(int value) {
        this.value = value;
    }

    public static PostStatusEnum valueOf(int postStatus) {
        return (PostStatusEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
