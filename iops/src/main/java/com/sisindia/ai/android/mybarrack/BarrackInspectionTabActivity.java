package com.sisindia.ai.android.mybarrack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackInspectionQuestionOptionMO;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackStrengthIR;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackStrengthMO;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.barrackInspection.BarrackInspectionMO;
import com.sisindia.ai.android.rota.barrackInspection.BarrackInspectionTaskExeMO;
import com.sisindia.ai.android.rota.barrackInspection.QuestionsAndOptionMO;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.services.concurrency.PriorityRunnable;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.sisindia.ai.android.R.integer.isRelatedToEditText;

/**
 * Created by shruti on 27/7/16.
 */
public class BarrackInspectionTabActivity extends DayCheckBaseActivity implements DialogClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    Context mContext;
    PagerAdapter adapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    @Bind(R.id.baseBarrackLayout)
    RelativeLayout baseBarrackLayout;
    @Bind(R.id.barrackNfcInformationTV)
    TextView barrackNfcInformationTV;

    private BarrackStrengthMO barrackStrengthMO;
    private RotaTaskModel rotaTaskModel;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private List<EmployeeAuthorizedStrengthMO> updatedActualStrength;
    private LinearLayout layoutView;
    private RelativeLayout remarkLayout;
    private String taskExecutionResult;
    private BarrackInspectionMO barrackInspectionMO;
    private BarrackInspectionTaskExeMO barrackInspectionTaskExeMO;
    private String barrackName;
    public HashMap<Integer, AttachmentMetaDataModel> imageList = new HashMap<>();
    private ArrayList<IssuesMO> mGrievanceList;
    private LinearLayout metLandlordLayout;
    private boolean isCheckedMetLandLordAtleatsOnce;
    private ArrayList<QuestionsAndOptionMO> questionsAndOptionMetLandLordList;
    private MetLandLordMo metLandLordMo;
    private BarrackMetLandlord barrackMetLandlord;
    private int ncfConfigurationStatus = 0;
    private String nfcUniqueId = "", dbNfcUniqueId = "";
    private boolean isNFCWarningEnabledToSubmit = false;

    @Override
    public void onBackPressed() {
        isNFCWarningEnabledToSubmit = false;
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barrack_tab);

        mContext = this;
        ButterKnife.bind(this);

        setUpToolBar();
        rotaTaskModel = (RotaTaskModel) getIntent().getSerializableExtra("RotaModel");
        if (null != rotaTaskModel) {
            getBarrackStrengthApiCall(rotaTaskModel.getBarrackId());
            barrackName = rotaTaskModel.getBarrackName();
            //IOPSApplication.getInstance().getSelectionStatementDBInstance().getBarrackNameById(rotaTaskModel.getBarrackId());
        }
        Util.hideKeyboard(this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        barrackInspectionMO = new BarrackInspectionMO();
        metLandLordMo = new MetLandLordMo();
        mGrievanceList = new ArrayList<>();
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.barrack_tab_strength)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.barrack_tab_space)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.barrack_tab_others)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.barrack_tab_met_landlord)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), barrackName, rotaTaskModel.getUnitId(),
                rotaTaskModel.getBarrackId(), rotaTaskModel.getTaskId());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        //CHECKING NFC IS CONFIGURED ON BARRACK OR NOT
        checkingBarrackNfcConfiguration();

    }

    //initiating toolbar
    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.barrack_inspection_string));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_billsubmission_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit) {

            if (appPreferences.isEnableGps(BarrackInspectionTabActivity.this)) {
                Util.hideKeyboard(this);

                if (!strengthTabStatus()) {
                    if (spaceTabValidation(layoutView)) {
                        Toast.makeText(this, "Please Answer all the question", Toast.LENGTH_SHORT).show();
                        viewPager.setCurrentItem(1);
                    } else {
                        if (otherTabValidation(remarkLayout)) {
                            Toast.makeText(this, "Please enter remarks", Toast.LENGTH_SHORT).show();
                            viewPager.setCurrentItem(2);
                        } /*else if (appPreferences.getForcedGrievanceFlag() && mGrievanceList != null && mGrievanceList.size() == 0) {
                            Toast.makeText(this, "Its mandatory to add grievance", Toast.LENGTH_SHORT).show();
                            viewPager.setCurrentItem(2);
                        }*/ else {
                            if (metLandLordValidation(metLandlordLayout).metLandlordStatus) {
                                Toast.makeText(this, "Please provide all information needed", Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(3);
                            } else if (!metLandLordValidation(metLandlordLayout).isValidPhoneNumber) {
                                Toast.makeText(this, getResources().getString(R.string.mobile_validation), Toast.LENGTH_SHORT).show();
                            } else {
                                if (isCheckedMetLandLordAtleatsOnce) {
                                    //SUBMITTING BARRACK INSPECTION REPORT TO LOCAL DB AND FOR SYNC TO SERVER
                                    /*if (ncfConfigurationStatus == 0) {
                                        isNFCWarningEnabledToSubmit = true;
                                        showConfirmationDialog(7);
                                    } else if (ncfConfigurationStatus == 1) {
                                        isNFCWarningEnabledToSubmit = true;
                                        showConfirmationDialog(8);
                                    } else {
                                        isNFCWarningEnabledToSubmit = false;
                                        submitBarrackInspectionReportToDB();
                                    }*/

                                    submitBarrackInspectionReportToDB();

                                }
                                //metaDataSetUp(int requestCode,  attachmentModel);
                                if (viewPager.getCurrentItem() == 3 && !isCheckedMetLandLordAtleatsOnce) {
                                    isCheckedMetLandLordAtleatsOnce = true;
                                    Toast.makeText(this, getResources().getString(R.string.met_landlord_validation_msg), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
            } else {
                showGpsDialog(getString(R.string.gps_configure_msg));
            }
        }
        if (id == android.R.id.home) {
            isNFCWarningEnabledToSubmit = false;
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitBarrackInspectionReportToDB() {

        getMetaDataFromMap();
        taskExecutionResult = createTaskExecutionResult();
        if (!taskExecutionResult.isEmpty()) {

            Util.showProgressBar(this, R.string.saveMessage);

            dbUpdates();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Util.dismissProgressBar(true);
                    Intent homeActivityIntent = new Intent(BarrackInspectionTabActivity.this, HomeActivity.class);
                    homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeActivityIntent);
                    finish();
                }
            }, 2500);
        }
    }

    private void dbUpdates() {
        updateRotaTaskTable();
        updateUnitAuthorizedStrengthTable();
        insertGrievanceData();
        insertMetaData();
        updateMyPerformanceTable();
        insertLogActivity();

        //UPDATING NFC TOUCH STATUS IN BARRACK TABLE
        /*if (isNFCWarningEnabledToSubmit)
            IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcTouchedStatusDuringTask(rotaTaskModel.getBarrackId(), nfcUniqueId, 0);
        else
            IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcTouchedStatusDuringTask(rotaTaskModel.getBarrackId(), nfcUniqueId, 1);*/

        triggerSyncAdapter();

        if (rotaTaskModel.getTaskId() < 0) {
            if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                        .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
            }
        }
        Util.mAudioSequenceNumber = 0;

    }

    public synchronized void insertLogActivity() {
        if (rotaTaskModel != null) {
            timeLineModel = new TimeLineModel();
            timeLineModel.setActivity("Check Out");
            timeLineModel.setLocation(rotaTaskModel.getUnitName());
            timeLineModel.setTaskId(String.valueOf(rotaTaskModel.getTaskId()));
            timeLineModel.setStatus(RotaTaskStatusEnum.Completed.name());
            timeLineModel.setTaskName(RotaTaskTypeEnum.valueOf(rotaTaskModel.getTaskTypeId()).name().replace("_", " "));
            timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
            timeLineModel.setEndTime("");
            timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());
            IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
        }
    }

    private synchronized void insertGrievanceData() {
        if (null != mGrievanceList && mGrievanceList.size() != 0) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(mGrievanceList);
            Util.reSetGrievanceModelHolder();
        }
    }

    public synchronized void getMetaDataFromMap() {
        if (null != imageList && imageList.size() != 0) {
            Iterator<Map.Entry<Integer, AttachmentMetaDataModel>> iter = imageList.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Integer, AttachmentMetaDataModel> entry = iter.next();
                setAttachmentMetaDataModel(entry.getValue());
            }
        }
    }

    public void metaDataSetUp(int requestCode, AttachmentMetaDataModel attachmentModel) {
        imageList.put(requestCode, attachmentModel);
    }

    private void updateUnitAuthorizedStrengthTable() {
        if (updatedActualStrength != null && updatedActualStrength.size() != 0) {
            IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateUnitEmpActualStrength(updatedActualStrength, false);
        }
    }

    private synchronized void updateMyPerformanceTable() {
        IOPSApplication.getInstance().getMyPerformanceStatementsDB().
                UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(),
                        dateTimeFormatConversionUtil.getCurrentTime(), String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());
    }

    private void triggerSyncAdapter() {
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);
    }

    private synchronized void updateRotaTaskTable() {
        String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;

        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateRotaTask(rotaTaskModel.getTaskId(),
                taskExecutionResult, RotaTaskStatusEnum.Completed.getValue(),
                dateTimeFormatConversionUtil.getCurrentDateTime(), rotaTaskModel.getRemarks(),
                geoLocation);
    }

    private String createTaskExecutionResult() {
        barrackInspectionTaskExeMO = new BarrackInspectionTaskExeMO();
        barrackInspectionTaskExeMO.setBarrackInspection(barrackInspectionMO);
        barrackInspectionTaskExeMO.setMetLandLordMo(metLandLordMo);
        taskExecutionResult = IOPSApplication.getGsonInstance().toJson(barrackInspectionTaskExeMO);
        System.out.println("taskExecutionResult :" + taskExecutionResult);

        return taskExecutionResult;
    }

    private boolean strengthTabStatus() {

        boolean strengthTabStatus = false;
        if (updatedActualStrength != null && updatedActualStrength.size() != 0) {

            for (int i = 0; i < updatedActualStrength.size(); i++) {

                if (updatedActualStrength.get(i).getActual() == 0) {
                    viewPager.setCurrentItem(0);
                    strengthTabStatus = true;
                    Toast.makeText(this, "Please Enter the strength", Toast.LENGTH_SHORT).show();
                }

            }

        }
        return strengthTabStatus;
    }

    private boolean otherTabValidation(RelativeLayout layout) {
        boolean otherTabStatus = false;
        int rowCount = layout.getChildCount();
        for (int index = 0; index < rowCount; index++) {

            View childView = layout.getChildAt(index);
            if (childView instanceof EditText) {
                ((EditText) childView).getText().toString().trim();

                if (((EditText) childView).getText().toString().trim().length() == 0) {
                    otherTabStatus = true;

                } else {
                    rotaTaskModel.setRemarks(((EditText) childView).getText().toString().trim());
                }
            }
        }
        return otherTabStatus;
    }

    public void checkStrengthEntry(int actulaCount, int leaveCount) {
        updatedActualStrength = new ArrayList<>();
        updatedActualStrength.clear();
        EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO = new EmployeeAuthorizedStrengthMO();
        employeeAuthorizedStrengthMO.setUnitTakID(rotaTaskModel.getTaskId());
        employeeAuthorizedStrengthMO.setActual(actulaCount);
        employeeAuthorizedStrengthMO.setUnitId(rotaTaskModel.getUnitId());
        employeeAuthorizedStrengthMO.setRankId(null);
        employeeAuthorizedStrengthMO.setBarrackId(rotaTaskModel.getBarrackId());
        employeeAuthorizedStrengthMO.setLeaveCount(leaveCount);
        employeeAuthorizedStrengthMO.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
        updatedActualStrength.add(employeeAuthorizedStrengthMO);
    }

    public void checkSpaceTabEntry(LinearLayout baseLayout) {
        layoutView = baseLayout;
    }

    private boolean spaceTabValidation(LinearLayout baseLayout) {
        boolean spaceTabStatus = false;
        ArrayList<QuestionsAndOptionMO> questionsAndOptionMOList = new ArrayList<>();

        int rowCount = baseLayout.getChildCount();
        for (int index = 0; index < rowCount; index++) {

            View childView = baseLayout.getChildAt(index);
            QuestionsAndOptionMO questionsAndOptionMO = new QuestionsAndOptionMO();

            if (childView instanceof EditText) {
                if (((EditText) childView).getText().toString().trim().length() == 0) {
                    spaceTabStatus = true;
                } else {
                    BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO =
                            (BarrackInspectionQuestionOptionMO)
                                    (childView).getTag();
                    questionsAndOptionMO.setOptionId(null);
                    questionsAndOptionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                    questionsAndOptionMO.setRemarks(((EditText) childView).getText().toString().trim());
                    questionsAndOptionMOList.add(questionsAndOptionMO);

                }
            } else if (childView instanceof RadioGroup) {
                RadioGroup radioGroup = (RadioGroup) childView;


                for (int m = 0; m < radioGroup.getChildCount(); m++) {

                    if (radioGroup.getCheckedRadioButtonId() == radioGroup.getChildAt(m).getId()) {
                        BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO = (BarrackInspectionQuestionOptionMO)
                                radioGroup.getChildAt(m).getTag();
                        questionsAndOptionMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                        questionsAndOptionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                        questionsAndOptionMO.setRemarks(null);
                        questionsAndOptionMOList.add(questionsAndOptionMO);
                    }
                }


            }
        }
        barrackInspectionMO.setQuestionsAndOptions(questionsAndOptionMOList);
        return spaceTabStatus;
    }

    private BarrackMetLandlord metLandLordValidation(LinearLayout layout) {
        viewPager.setCurrentItem(3);
        questionsAndOptionMetLandLordList = new ArrayList<>();
        barrackMetLandlord = new BarrackMetLandlord();
        barrackMetLandlord = linearLayoutChildValidations(layout, questionsAndOptionMetLandLordList, barrackMetLandlord);
        metLandLordMo.setQuestionsAndOptions(questionsAndOptionMetLandLordList);
        return barrackMetLandlord;
    }

    private BarrackMetLandlord linearLayoutChildValidations(LinearLayout layout, ArrayList<QuestionsAndOptionMO>
            questionsAndOptionMOList, BarrackMetLandlord barrackMetLandlord) {
        int rowCount = layout.getChildCount();
        for (int index = 0; index < rowCount; index++) {

            View childView = layout.getChildAt(index);
            QuestionsAndOptionMO questionsAndOptionMO = new QuestionsAndOptionMO();

            if (childView instanceof EditText) {
                if (childView.getVisibility() == View.VISIBLE) {
                    if (((EditText) childView).getText().toString().trim().length() == 0) {
                        barrackMetLandlord.metLandlordStatus = true;
                    } else {
                        if (((EditText) childView).getInputType() == InputType.TYPE_CLASS_NUMBER) {
                            if (Util.isValidPhoneNumber(((EditText) childView).getText().toString().trim())) {
                                BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO =
                                        (BarrackInspectionQuestionOptionMO)
                                                (childView).getTag();
                                questionsAndOptionMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                                questionsAndOptionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                                questionsAndOptionMO.setRemarks(((EditText) childView).getText().toString().trim());
                                questionsAndOptionMOList.add(questionsAndOptionMO);
                                barrackMetLandlord.isValidPhoneNumber = true;
                            } else {
                                barrackMetLandlord.isValidPhoneNumber = false;
                                //snackBarWithMesg(baseBarrackLayout, getResources().getString(R.string.mobile_validation));
                            }

                        } else {
                            BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO =
                                    (BarrackInspectionQuestionOptionMO)
                                            (childView).getTag();
                            questionsAndOptionMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                            questionsAndOptionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                            questionsAndOptionMO.setRemarks(((EditText) childView).getText().toString().trim());
                            questionsAndOptionMOList.add(questionsAndOptionMO);
                        }
                    }
                } else {
                    if (((EditText) childView).getInputType() == InputType.TYPE_CLASS_NUMBER) {
                        barrackMetLandlord.isValidPhoneNumber = true;
                    }
                }

            } else if (childView instanceof RadioGroup) {
                RadioGroup radioGroup = (RadioGroup) childView;
                Object object = radioGroup.getTag(isRelatedToEditText);
                boolean isRelatedToEditText = false;
                if (object != null) {
                    isRelatedToEditText = (boolean) object;
                }
                if (!isRelatedToEditText) {
                    for (int m = 0; m < ((RadioGroup) childView).getChildCount(); m++) {
                        if (childView.getVisibility() == View.VISIBLE) {
                            if (((RadioGroup) childView).getCheckedRadioButtonId() == ((RadioGroup) childView).getChildAt(m).getId()) {
                                BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO = (BarrackInspectionQuestionOptionMO)
                                        ((RadioGroup) childView).getChildAt(m).getTag();
                                questionsAndOptionMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                                questionsAndOptionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                                questionsAndOptionMO.setRemarks(null);
                                questionsAndOptionMOList.add(questionsAndOptionMO);
                            }
                        }
                    }
                }

            } else if (childView instanceof LinearLayout) {
                if (childView.getVisibility() == View.VISIBLE) {
                    barrackMetLandlord = linearLayoutChildValidations((LinearLayout) childView,
                            questionsAndOptionMOList,
                            barrackMetLandlord);
                } else {
                    barrackMetLandlord.isValidPhoneNumber = true;
                }
            }
        }
        questionsAndOptionMetLandLordList = questionsAndOptionMOList;
        return barrackMetLandlord;
    }

    public void checkMetLandlordTabEntry(LinearLayout layout) {
        metLandlordLayout = layout;
    }

    public void checkOthersTabEntry(RelativeLayout layoutView) {
        remarkLayout = layoutView;
    }

    private void getBarrackStrengthApiCall(int barrackid) {
        SISClient sisClient = new SISClient(this);
        sisClient.getApi().getBarrackLastInspectStrength(barrackid, new Callback<BarrackStrengthIR>() {
            @Override
            public void success(BarrackStrengthIR barrackStrengthIR, Response response) {

                switch (barrackStrengthIR.getStatusCode()) {

                    case HttpURLConnection.HTTP_OK:

                        barrackStrengthMO = barrackStrengthIR.getData();

                        if (barrackStrengthMO != null) {

                            if (null != BarrackInspectionTabActivity.class) {
                                runOnUiThread(new PriorityRunnable() {
                                    @Override
                                    public void run() {
                                        adapter = new PagerAdapter
                                                (getSupportFragmentManager(), tabLayout.getTabCount(),
                                                        barrackStrengthMO, barrackName, rotaTaskModel.getUnitId(),
                                                        rotaTaskModel.getBarrackId(), rotaTaskModel.getTaskId());
                                        viewPager.setAdapter(adapter);
                                    }
                                });

                            }
                        }
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);

            }
        });
    }

    public void greivanceInsertion(ArrayList<IssuesMO> grievanceList) {
        mGrievanceList.clear();
        mGrievanceList.addAll(grievanceList);
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        /*if (isNFCWarningEnabledToSubmit)
            submitBarrackInspectionReportToDB();
        else
            clearSdCardImages();*/

        clearSdCardImages();
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void finish() {
        super.finish();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearSdCardImages() {
        for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
            if (attachmentMetaDataModel != null) {
                File file = new File(attachmentMetaDataModel.getAttachmentPath());
                if (file != null && file.exists()) {
                    file.delete();
                }
            }
        }
    }

    //@Ashu : 21-Nov-2017
    private void checkingBarrackNfcConfiguration() {

        ArrayList<Object> nfcDetailsList = IOPSApplication.getInstance().getMyBarrackStatementDB().isNfcConfiguredAtBarrack(rotaTaskModel.getBarrackId());
        if (nfcDetailsList != null && nfcDetailsList.size() > 0) {
            dbNfcUniqueId = (String) nfcDetailsList.get(0);
            int status = (int) nfcDetailsList.get(1);

            if (dbNfcUniqueId != null) {
                if (status == 0) {
                    ncfConfigurationStatus = 1;
                    registerReceiver(nfcUniqueIdReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
                    barrackNfcInformationTV.setText("PLEASE TOUCH NFC...");
                    barrackNfcInformationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.nfc_on, 0, R.drawable.cross_icon_white, 0);
                } else if (status == 1) {
                    ncfConfigurationStatus = 2;
                    barrackNfcInformationTV.setText("NFC ALREADY TOUCHED");
                    barrackNfcInformationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.nfc_on, 0, R.drawable.tick_icon, 0);
                }
            } else {
                ncfConfigurationStatus = 0;
                barrackNfcInformationTV.setText("NFC NOT YET CONFIGURED");
                barrackNfcInformationTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.warning_icon, 0, R.drawable.cross_icon_white, 0);
            }
        }

    }

    BroadcastReceiver nfcUniqueIdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            nfcUniqueId = intent.getStringExtra(Constants.Nfc.NFC_UNIQUE_ID);
            if (TextUtils.isEmpty(nfcUniqueId)) {
                Toast.makeText(BarrackInspectionTabActivity.this, "NFC Device Id is empty or tampered", Toast.LENGTH_SHORT).show();
            } else {
                if (nfcUniqueId.equals(dbNfcUniqueId)) {
                    try {
                        ncfConfigurationStatus = 2;
                        barrackNfcInformationTV.setText("NFC TOUCHED SUCCESSFULLY");
                        barrackNfcInformationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.nfc_on, 0, R.drawable.tick_icon, 0);
                    } catch (Exception e) {
                    }
                } else {
                    Toast.makeText(BarrackInspectionTabActivity.this, "Barrack NFC mis-matched, please touch valid NFC", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (nfcUniqueIdReceiver != null)
                unregisterReceiver(nfcUniqueIdReceiver);
        } catch (Exception e) {
        }
    }
}
