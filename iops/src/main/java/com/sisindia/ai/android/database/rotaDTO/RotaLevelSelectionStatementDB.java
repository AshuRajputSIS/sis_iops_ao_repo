package com.sisindia.ai.android.database.rotaDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.EmployeeLeaveCalendar;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackInspectionQuestionOptionMO;
import com.sisindia.ai.android.rota.DateRotaModel;
import com.sisindia.ai.android.rota.RotaTaskBlackIconEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.RotaTaskWhiteIconEnum;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;
import com.sisindia.ai.android.rota.daycheck.NoTaskMO;
import com.sisindia.ai.android.rota.models.HolidayDateMO;
import com.sisindia.ai.android.rota.models.KitDistributionStatusMO;
import com.sisindia.ai.android.rota.models.RotaCalendarMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.models.SecurityRiskMO;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.rota.models.SecurityRiskOptionMO;
import com.sisindia.ai.android.rota.moninputandbs.MonInputAndBSModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by shankar on 25/6/16.
 */

public class RotaLevelSelectionStatementDB extends SISAITrackingDB {

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public RotaLevelSelectionStatementDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public ArrayList<Object> getRotaTaskByDate(String date, boolean fromRotaFragment, int barrackId) {
        SQLiteDatabase sqlite = null;
        ArrayList<Object> rotaTaskList = new ArrayList<>();
        ArrayList<Object> otherItemsList = new ArrayList<>();
        String query;

//        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(null);
        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(date);

        if (fromRotaFragment) {
            DateRotaModel dateRotaModel = getRotaAndTaskCount(date);
            rotaTaskList.add(0, dateRotaModel);
            otherItemsList.add(0, TableNameAndColumnStatement.Other_tasks);

            String currentDate = dateTimeFormatConversionUtil.getCurrentDate();
            int dateResult = dateTimeFormatConversionUtil.dateComparison(currentDate, date);

            if (dateResult > 0) {

                query = " SELECT " + TableNameAndColumnStatement.UNIT_ID + "," + TableNameAndColumnStatement.UNIT_NAME + "," +
                        TableNameAndColumnStatement.UNIT_CODE + ", " +
                        "Rot." + TableNameAndColumnStatement.ROTA_WEEK + " as rota_week, task." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, task." + TableNameAndColumnStatement.TASK_ID + "" +
                        " as task_id,id,barrack_id,task_status,barrack_name,barrack_full_address, createddatetime,task_type_id,task_status_id, unit_full_address,estimated_execution_start_date_time, estimated_execution_end_date_time,other_task_reason_id,is_auto from " +
                        " (SELECT rota_id, rota_week from rota where rota_week = " + dateRotaWeek + " order by id desc limit 1)Rot  inner join (SELECT rota_id, task_id from rota_task)rot_task on rot.rota_id = rot_task.rota_id  inner join " +
                        " (select u.unit_id,u.unit_name,u.unit_code,t.created_date,t.task_id,t.id,t.barrack_id,l.lookup_name  as task_status,b.barrack_name,b.barrack_full_address,t.createddatetime,t.task_type_id," +
                        " t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time, t.estimated_execution_end_date_time,t.other_task_reason_id,t.is_auto from task " +
                        "t left join unit u on t.unit_id = u.unit_id  inner join lookup_table l  on t.task_status_id = l.lookup_identifier and" +
                        " l.lookup_type_name = 'TaskStatus'  left join barrack b on t.barrack_id = b.barrack_id where  t.is_auto = 1  and substr(t.estimated_execution_start_date_time, 0, 11) = '"
                        + date + "' )task on task.task_id = rot_task.task_id  union  select u." + TableNameAndColumnStatement.UNIT_ID + ", u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                        "u." + TableNameAndColumnStatement.UNIT_CODE + ", 'NULL' rota_week, t." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, " +
                        "t." + TableNameAndColumnStatement.TASK_ID + ",t." + TableNameAndColumnStatement.ID + ",t." + TableNameAndColumnStatement.BARRACK_ID + ",l."
                        + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                        TableNameAndColumnStatement.TASK_STATUS + "," + "b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                        "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + ",t." + TableNameAndColumnStatement.CREATED_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                        "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                        "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                        "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                        "t." + TableNameAndColumnStatement.IS_AUTO +
                        " from " + TableNameAndColumnStatement.TASK_TABLE +
                        " t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                        " u on t." + TableNameAndColumnStatement.UNIT_ID +
                        " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                        " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                        " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                        + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                        "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus' left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t."
                        + TableNameAndColumnStatement.BARRACK_ID +
                        " = " + "b." + TableNameAndColumnStatement.BARRACK_ID +
                        " where  t.is_auto = 0  and  substr(t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME +
                        ",0,11) = '" + date + "' order by " +
                        TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";
            } else {

                query = " SELECT " + TableNameAndColumnStatement.UNIT_ID + "," + TableNameAndColumnStatement.UNIT_NAME + "," +
                        TableNameAndColumnStatement.UNIT_CODE + ", " +
                        "Rot." + TableNameAndColumnStatement.ROTA_WEEK + " as rota_week, " +
                        "task." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, " +
                        "task." + TableNameAndColumnStatement.TASK_ID + "" +
                        " as task_id,id,barrack_id,task_status,barrack_name,barrack_full_address, createddatetime,task_type_id,task_status_id, unit_full_address,estimated_execution_start_date_time," +
                        "estimated_execution_end_date_time,other_task_reason_id,is_auto from  (SELECT rota_id, rota_week from rota where rota_week = " + dateRotaWeek +
                        " order by id desc limit 1)Rot inner join  (SELECT rota_id, task_id from rota_task)rot_task on rot.rota_id = rot_task.rota_id " +
                        " inner join  (select u.unit_id,u.unit_name,u.unit_code,t.created_date,t.task_id,t.id,t.barrack_id,l.lookup_name  as task_status,b.barrack_name,b.barrack_full_address,t.createddatetime,t.task_type_id," +
                        " t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time, t.estimated_execution_end_date_time,t.other_task_reason_id,t.is_auto from task " +
                        "t left join unit u on t.unit_id = u.unit_id   inner join lookup_table l  on t.task_status_id = l.lookup_identifier and  l.lookup_type_name = 'TaskStatus' " +
                        " left join barrack b on t.barrack_id = b.barrack_id where  t.is_auto = 1  and substr(t.estimated_execution_start_date_time, 0, 11) = '"
                        + date + "' )task on task.task_id = rot_task.task_id  union " +
                        " select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                        "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                        "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                        " 'NULL' rota_week ," +
                        "t." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, " +
                        "t." + TableNameAndColumnStatement.TASK_ID + "," +
                        "t." + TableNameAndColumnStatement.ID + "," +
                        "t." + TableNameAndColumnStatement.BARRACK_ID + "," +
                        "l." + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                        TableNameAndColumnStatement.TASK_STATUS + "," +
                        "b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                        "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + "," +
                        "t." + TableNameAndColumnStatement.CREATED_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                        "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                        "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                        "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "," +
                        "t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                        "t." + TableNameAndColumnStatement.IS_AUTO +
                        " from " + TableNameAndColumnStatement.TASK_TABLE +
                        " t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                        " u on t." + TableNameAndColumnStatement.UNIT_ID +
                        " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                        " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                        " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                        + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                        "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus'"
                        + " left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t."
                        + TableNameAndColumnStatement.BARRACK_ID +
                        " = " + "b." + TableNameAndColumnStatement.BARRACK_ID +
                        " where  t.is_auto = 0  and " +
                        " substr(t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME +
                        ",0,11)" +
                        " = " + "'" + date + "'" +
                        " order by " +
                        TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";
            }
        } else {
            query = "SELECT unit_id,unit_name,unit_code,task.task_id as task_id ,id , barrack_id,barrack_name,barrack_full_address,createddatetime," +
                    "task_type_id,task_status_id,other_task_reason_id,unit_full_address,task_status,estimated_execution_start_date_time,estimated_execution_end_date_time," +
                    "is_auto " + "from " + "(SELECT " + "rota_id from rota  where rota_week = " + dateRotaWeek + " order by id " + "desc limit 1" + ")Rot inner join " +
                    "(SELECT  rota_id, task_id  from  rota_task) rot_task on rot.rota_id = rot_task.rota_id inner join " +
                    "(select u.unit_id, u.unit_name, u.unit_code, t.task_id, t.id, t.barrack_id, l.lookup_name  as  task_status," +
                    "b.barrack_name, b.barrack_full_address, t.createddatetime," +
                    "t.task_type_id, t.task_status_id, u.unit_full_address, t.estimated_execution_start_date_time, t.estimated_execution_end_date_time," +
                    "t.other_task_reason_id, t.is_auto  from  task t  inner join  barrack b  on  t.barrack_id  = b.barrack_id  inner join lookup_table l " +
                    "on  t.task_status_id  = l.lookup_identifier  and  l.lookup_type_name =  'TaskStatus'  left join  unit u  on  t.unit_id  = " +
                    "u.unit_id  where substr(t.createddatetime,0,11) = '" + date + "'  and substr(t.estimated_execution_start_date_time, 0, 11) = '"
                    + date + "'  and  t.is_auto  = 1)  task  union  select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                    "t." + TableNameAndColumnStatement.TASK_ID + "," +
                    "t." + TableNameAndColumnStatement.ID + "," +
                    "t." + TableNameAndColumnStatement.BARRACK_ID + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.CREATED_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                    "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                    "t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                    "l." + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                    TableNameAndColumnStatement.TASK_STATUS + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.IS_AUTO +
                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    "  t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                    " u on t." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                    " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                    " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                    + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                    "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus'" +
                    " left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t." +
                    TableNameAndColumnStatement.BARRACK_ID +
                    " = " + "b." + TableNameAndColumnStatement.BARRACK_ID +
                    " where substr(t." + TableNameAndColumnStatement.CREATED_DATE_TIME +
                    ",0, 11)" +
                    " = " + "'" + date + "' and " +
                    "t." + TableNameAndColumnStatement.BARRACK_ID + " = " + "'" +
                    barrackId + "' and " + "t." + TableNameAndColumnStatement.TASK_STATUS_ID + " not in (4,3) " +
                    " and " +
                    " substr(t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME +
                    ",0,11)" +
                    " = " + "'" + date + "'"
                    + " order by " +
                    TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";
        }
//        Timber.i("RotaHomeScreen Query : %s", query);
        try {
            sqlite = this.getReadableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)) == 1) {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));
                            rotaTaskList.add(rotaTaskModel);
                        } else {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)) == 0)
                                rotaTaskModel.setRotaWeek(dateTimeFormatConversionUtil.getWeekOfTheYear(date));
                            else
                                rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));

                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            otherItemsList.add(otherItemsList.size(), rotaTaskModel);
                            if (!fromRotaFragment) {
                                rotaTaskList.addAll(otherItemsList);
                            }
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        if (fromRotaFragment) {
            if (rotaTaskList.size() == 1) {
                NoTaskMO noTaskMO = new NoTaskMO();
                int dateComparesion = dateTimeFormatConversionUtil.dateComparison(dateTimeFormatConversionUtil.getCurrentDate(), date);
                if (dateComparesion == 0) {
                    noTaskMO.setMessage(TableNameAndColumnStatement.NO_TASK_FOR_TODAY);
                } else {
                    String requiredDate = dateTimeFormatConversionUtil.convertDateToDayMonthDateFormat(date).split(",")[1];
                    noTaskMO.setMessage(TableNameAndColumnStatement.NO_TASK_FOR_SPECIFIC_DAY + " " + requiredDate);
                }
                rotaTaskList.add(rotaTaskList.size(), noTaskMO);
            }

            //CHECKING BELOW CONDITION WHETHER SELECTED DATE LIES BETWEEN PREVIOUS MONTH 25 AND 11th OF NEXT MONTH (25,11]
            /*if (dateTimeFormatConversionUtil.isDateRangeBetween25To11(date)) {
            }*/

            // Need to insert "Bill Submission" and "Mon Input" data over here
            //[Adding counts for MonInput task]
            MonInputAndBSModel miModel = getTotalAndPendingCountsOfMiAndBS(1, date);
            if (miModel.getTotalCountOfMiOrBS() != 0 && miModel.getPendingCountOfMiOrBS() != 0) {
                rotaTaskList.add(miModel);
            }

            //[Adding counts for Bill Submission task]
            MonInputAndBSModel bsModel = getTotalAndPendingCountsOfMiAndBS(2, date);
            if (bsModel.getTotalCountOfMiOrBS() != 0 && bsModel.getPendingCountOfMiOrBS() != 0) {
                rotaTaskList.add(bsModel);
            }

            //------------------------------------------------------------------//
            if (otherItemsList.size() > 1) {
                rotaTaskList.addAll(otherItemsList);
            }

            //@Ashu Rajput : Added Next Night Task
            ArrayList<Object> nextDayNightTasksList = getNextDayNightRota(date);
            if (nextDayNightTasksList.size() > 0) {
                rotaTaskList.add("Next Day Night Task");
                rotaTaskList.addAll(nextDayNightTasksList);
            } else {
                NoTaskMO noTaskMO = new NoTaskMO();
                noTaskMO.setMessage("No Tasks For Night");
                rotaTaskList.add(noTaskMO);
            }

            //@Ashu Rajput : Checking Total and Pending count of kit distribution by AI
            KitDistributionStatusMO kitDistributionCounts = getTotalAndPendingCountForAKR();
            if (kitDistributionCounts != null && (kitDistributionCounts.getTotalCountOfKitsForDistribution() != 0 &&
                    kitDistributionCounts.getPendingCountOfKitsForDistribution() != 0)) {
                rotaTaskList.add(kitDistributionCounts);
            }

        }
        return rotaTaskList;
    }

    //@Ashu Rajput : Created below method to get rotas or tasks of next day between (00:00:00 AM  to 5:00:00)
    //@Ashu : Adding synchronized keyword to avoid DB lock : 23-06-2018
    private synchronized ArrayList<Object> getNextDayNightRota(String date) {

        date = dateTimeFormatConversionUtil.getNextDayDate(date);

        SQLiteDatabase sqlite = null;
        ArrayList<Object> nextDayNightTasksLists = new ArrayList<>();
        String query;
//        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(null);
        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(date);
        query = " SELECT " + TableNameAndColumnStatement.UNIT_ID + ","
                + TableNameAndColumnStatement.UNIT_NAME + "," +
                TableNameAndColumnStatement.UNIT_CODE + ", Rot." + TableNameAndColumnStatement.ROTA_WEEK + " as rota_week, " +
                "task." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, task." + TableNameAndColumnStatement.TASK_ID + "" +
                " as task_id,id,barrack_id,task_status,barrack_name,barrack_full_address, createddatetime,task_type_id,task_status_id, unit_full_address,estimated_execution_start_date_time, estimated_execution_end_date_time,other_task_reason_id,is_auto from " +
                " (SELECT rota_id, rota_week from rota where rota_week = " + dateRotaWeek + " order by id desc limit 1)Rot  inner join (SELECT rota_id, task_id from rota_task)rot_task on rot.rota_id = rot_task.rota_id  inner join " +
                " (select u.unit_id,u.unit_name,u.unit_code,t.created_date,t.task_id,t.id,t.barrack_id,l.lookup_name  as task_status,b.barrack_name,b.barrack_full_address,t.createddatetime,t.task_type_id," +
                " t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time, t.estimated_execution_end_date_time,t.other_task_reason_id,t.is_auto from task " +
                "t left join unit u on t.unit_id = u.unit_id  inner join lookup_table l  on t.task_status_id = l.lookup_identifier and" +
                " l.lookup_type_name = 'TaskStatus'  left join barrack b on t.barrack_id = b.barrack_id where  t.is_auto = 1 and" +
                " t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME +
                " between " + "'" + date + " 00:00:00' and '" + date + " 05:00:00' )task on task.task_id = rot_task.task_id  union  select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                "u." + TableNameAndColumnStatement.UNIT_NAME + ", u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                " 'NULL' rota_week ,t." + TableNameAndColumnStatement.CREATED_DATE + " as created_date, t." + TableNameAndColumnStatement.TASK_ID + "," +
                "t." + TableNameAndColumnStatement.ID + ", t." + TableNameAndColumnStatement.BARRACK_ID + ", l." + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                TableNameAndColumnStatement.TASK_STATUS + ",b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + ",t." + TableNameAndColumnStatement.CREATED_DATE_TIME + "," +
                "t." + TableNameAndColumnStatement.TASK_TYPE_ID + ",t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + ",t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + ",t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                "t." + TableNameAndColumnStatement.IS_AUTO + " from " + TableNameAndColumnStatement.TASK_TABLE + " t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                " u on t." + TableNameAndColumnStatement.UNIT_ID + " = u." + TableNameAndColumnStatement.UNIT_ID +
                " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE + " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus'"
                + " left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t." + TableNameAndColumnStatement.BARRACK_ID +
                " = " + "b." + TableNameAndColumnStatement.BARRACK_ID + " where  t.is_auto = 0  and  t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME +
                " between " + "'" + date + " 00:00:00' and '" + date + " 05:00:00' order by " + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";

//        Timber.i("RotaMO NextDay Night Query : %s", query);
        try {
            sqlite = this.getReadableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)) == 1) {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));
                            nextDayNightTasksLists.add(rotaTaskModel);
//                            nextDayNightTasksLists.add(nextDayNightTasksLists.size(), rotaTaskModel);
                        } else {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));

                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)) == 0) {
                                rotaTaskModel.setRotaWeek(dateTimeFormatConversionUtil.getWeekOfTheYear(date));
                            } else {
                                rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));
                            }
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));

                            nextDayNightTasksLists.add(rotaTaskModel);
//                            nextDayNightTasksLists.add(nextDayNightTasksLists.size(), rotaTaskModel);
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return nextDayNightTasksLists;
    }

    //@Ashu : Adding synchronized keyword to avoid DB lock
    public synchronized DateRotaModel getRotaAndTaskCount(String date) {
        DateRotaModel dateRotaModel = new DateRotaModel();
        String currentDate = dateTimeFormatConversionUtil.getCurrentDate();
        int dayResult = dateTimeFormatConversionUtil.dateComparison(currentDate, date);
        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(date);
        String query = "";
        if (dayResult > 0) {
            query = " SELECT CompletedRTC + CompletedATC as CompletedTaskCount, TotalRTC + TotalATC as TotalTaskCount from (SELECT count(case when t.task_status_id in (3,4) " +
                    " then t.task_id end) as CompletedRTC, count(t.task_status_id) as TotalRTC from (SELECT rota_id from rota where rota_week = "
                    + dateRotaWeek + " order by 1 desc limit 1)r inner join (select rota_id, rota_task_id, task_id from rota_task)rt on rt.rota_id= r.rota_id inner join (select task_id, " +
                    " task_status_id from task where is_auto = 1 and substr(estimated_execution_start_date_time,0,11) = '" +
                    date + "' )t on t.task_id = rt.task_id)rota, (SELECT count(case when ad.task_status_id in (3,4) then ad.task_id end) as CompletedATC, " +
                    " count(ad.task_id) as TotalATC from (select task_id, task_status_id from task where is_auto = 0 and substr(estimated_execution_start_date_time,0,11) = '" +
                    date + "')ad)adhoc";

        } else {
            query = " SELECT CompletedRTC + CompletedATC as CompletedTaskCount, TotalRTC + TotalATC as TotalTaskCount from (SELECT count(case when t.task_status_id in (3,4) " +
                    " then t.task_id end) as CompletedRTC, count(t.task_status_id) as TotalRTC from (SELECT rota_id from rota where rota_week = "
                    + dateRotaWeek + " order by 1 desc limit 1)r inner join (select rota_id, rota_task_id, task_id from rota_task)rt on rt.rota_id= r.rota_id inner join (select task_id,  task_status_id from task where is_auto = 1 " +
                    " and substr(estimated_execution_start_date_time,0,11) = '" +
                    date + "' )t on t.task_id = rt.task_id)rota, (SELECT count(case when ad.task_status_id in (3,4) then ad.task_id end) as CompletedATC, count(ad.task_id) as TotalATC from " +
                    " (select task_id, task_status_id from task where is_auto = 0 and substr(estimated_execution_start_date_time,0,11) = '" +
                    date + "')ad)adhoc";
        }

        SQLiteDatabase sqlite = null;

//        Timber.d("RotaTaskCount %s", query);

        String todaysDate = dateTimeFormatConversionUtil.convertDateToDayMonthDateFormat(date);
        dateRotaModel.setCurrentDateStr(todaysDate);
        dateRotaModel.setRotaDateStr(getRotaDate());
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        String rotaTotalCount = cursor.getString(cursor.getColumnIndex("TotalTaskCount"));
                        String rotaCompletedCount = cursor.getString(cursor.getColumnIndex("CompletedTaskCount"));
                        if (rotaTotalCount.equalsIgnoreCase("0")) {
                            dateRotaModel.setTaskStatus("No Rota Task");
                            dateRotaModel.setRotaTotalCount(Integer.valueOf("0"));
                            dateRotaModel.setRotaCompletedCount(Integer.valueOf("0"));
                        } else {
                            dateRotaModel.setTaskStatus("Today's Tasks (" + rotaCompletedCount + " of " + rotaTotalCount + " Completed)");
                            dateRotaModel.setRotaTotalCount(Integer.valueOf(rotaTotalCount));
                            dateRotaModel.setRotaCompletedCount(Integer.valueOf(rotaCompletedCount));
                        }
                    } while (cursor.moveToNext());
                }
            } else {
                dateRotaModel.setTaskStatus("No Rota Task");
                dateRotaModel.setRotaTotalCount(Integer.valueOf("0"));
                dateRotaModel.setRotaCompletedCount(Integer.valueOf("0"));
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dateRotaModel;
    }

    public ArrayList<Object> getMIAndBSMonthlyRota(int rotaRequestType, String selectedDate) {
        SQLiteDatabase sqlite = null;
        ArrayList<Object> rotaTaskList = new ArrayList<>();
        ArrayList<Object> otherItemsList = new ArrayList<>();
        otherItemsList.add(0, TableNameAndColumnStatement.Other_tasks);
        if (selectedDate != null && selectedDate.equals(""))
            selectedDate = dateTimeFormatConversionUtil.getCurrentDate();
        String query = "";

        int dateComparisonResult = dateTimeFormatConversionUtil.currentDateVs25thOfCurrentMonthDate(selectedDate);
        // Means : current date is less than current month 25th date
        if (dateComparisonResult < 0) {
            if (rotaRequestType == 1) {

                /*query = "select u.unit_id,u.unit_name,u.unit_code, 'NULL' rota_week ,t.created_date as created_date, t.task_id,t.id,t.barrack_id,l.lookup_name as task_status," +
                        "t.CreatedDateTime,t.task_type_id,t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time,t.estimated_execution_end_date_time," +
                        "t.other_task_reason_id,t.is_auto from task t left join unit u on t.unit_id = u.unit_id inner join lookup_table l on t.task_status_id = l.lookup_identifier " +
                        "and l.lookup_type_name = 'TaskStatus' where t.is_auto in(0,1) and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(t.estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getPreviousAndNextMonth24Or25Date(-1, "25") +
                        "' and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' ||substr(t.estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,1,2))<= '" + dateTimeFormatConversionUtil.getCurrentMonthDateFromDay("24") + "' " +
                        "and  t.Task_Type_Id=7 and t.Other_Task_reason_Id=10 order by estimated_execution_start_date_time asc";*/

                query = "select u.unit_id,u.unit_name,u.unit_code, 'NULL' rota_week ,t.created_date as created_date, t.task_id,t.id,t.barrack_id,l.lookup_name as task_status," +
                        "t.CreatedDateTime,t.task_type_id,t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time,t.estimated_execution_end_date_time," +
                        "t.other_task_reason_id,t.is_auto from task t left join unit u on t.unit_id = u.unit_id inner join lookup_table l on t.task_status_id = l.lookup_identifier " +
                        "and l.lookup_type_name = 'TaskStatus' where t.is_auto in(0,1) and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,4,2) ||'-' || substr(t.estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(selectedDate, -1, "25") +
                        "' and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' ||substr(t.estimated_execution_start_date_time,4,2) ||'-' ||substr(t.estimated_execution_start_date_time,1,2))<= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(selectedDate, "24") + "' " +
                        "and  t.Task_Type_Id=7 and t.Other_Task_reason_Id=10 order by estimated_execution_start_date_time asc";

            } else if (rotaRequestType == 2) {

                query = "select u.unit_id,u.unit_name,u.unit_code, 'NULL' rota_week ,t.created_date as created_date, t.task_id,t.id,t.barrack_id,l.lookup_name as task_status," +
                        "t.CreatedDateTime,t.task_type_id,t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time,t.estimated_execution_end_date_time," +
                        "t.other_task_reason_id,t.is_auto from task t left join unit u on t.unit_id = u.unit_id inner join lookup_table l on t.task_status_id = l.lookup_identifier " +
                        "and l.lookup_type_name = 'TaskStatus' where t.is_auto in(0,1) and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,4,2) ||'-' || substr(t.estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(selectedDate, -1, "25") + "' and " +
                        "DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' ||substr(t.estimated_execution_start_date_time,4,2) ||'-' ||substr(t.estimated_execution_start_date_time,1,2))<= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(selectedDate, "24") + "' " +
                        "and  t.Task_Type_Id=5 order by estimated_execution_start_date_time asc";
            }
        } else {
            if (rotaRequestType == 1) {

                query = "select u.unit_id,u.unit_name,u.unit_code, 'NULL' rota_week ,t.created_date as created_date, t.task_id,t.id,t.barrack_id,l.lookup_name as task_status," +
                        "t.CreatedDateTime,t.task_type_id,t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time,t.estimated_execution_end_date_time," +
                        "t.other_task_reason_id,t.is_auto from task t left join unit u on t.unit_id = u.unit_id inner join lookup_table l on t.task_status_id = l.lookup_identifier " +
                        "and l.lookup_type_name = 'TaskStatus' where t.is_auto in(0,1) and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,4,2) ||'-' || substr(t.estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(selectedDate, "25") +
                        "' and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' ||substr(t.estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,1,2))<= '"
                        + dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(selectedDate, 1, "24") + "' " +
                        "and  t.Task_Type_Id=7 and t.Other_Task_reason_Id=10 order by estimated_execution_start_date_time asc";

            } else if (rotaRequestType == 2) {

                query = "select u.unit_id,u.unit_name,u.unit_code, 'NULL' rota_week ,t.created_date as created_date, t.task_id,t.id,t.barrack_id,l.lookup_name as task_status," +
                        "t.CreatedDateTime,t.task_type_id,t.task_status_id,u.unit_full_address,t.estimated_execution_start_date_time,t.estimated_execution_end_date_time," +
                        "t.other_task_reason_id,t.is_auto from task t left join unit u on t.unit_id = u.unit_id inner join lookup_table l on t.task_status_id = l.lookup_identifier " +
                        "and l.lookup_type_name = 'TaskStatus' where t.is_auto in(0,1)  and DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' " +
                        "||substr(t.estimated_execution_start_date_time,4,2) ||'-' || substr(t.estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(selectedDate, "25") + "' and " +
                        "DATE(substr(t.estimated_execution_start_date_time,7,4) ||'-' ||substr(t.estimated_execution_start_date_time,4,2) ||'-' ||substr(t.estimated_execution_start_date_time,1,2))<= '"
                        + dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(selectedDate, 1, "24") + "' " +
                        "and  t.Task_Type_Id=5 order by estimated_execution_start_date_time asc";
            }
        }

//        Timber.i("MonInput_BillSub RotaQuery : %s", query);
        try {
            sqlite = this.getReadableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)) == 1) {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));
                            rotaTaskList.add(rotaTaskModel);
                        } else {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)) == 0)
                                rotaTaskModel.setRotaWeek(dateTimeFormatConversionUtil.getWeekOfTheYear(selectedDate));
                            else
                                rotaTaskModel.setRotaWeek(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_WEEK)));

                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            otherItemsList.add(rotaTaskModel);
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        if (rotaTaskList.size() == 1) {
            NoTaskMO noTaskMO = new NoTaskMO();
            noTaskMO.setMessage(TableNameAndColumnStatement.NO_TASK_FOR_CURRENT_MONTH);
            rotaTaskList.add(rotaTaskList.size(), noTaskMO);
        }

        if (otherItemsList != null && otherItemsList.size() > 0)
            rotaTaskList.addAll(otherItemsList);

        return rotaTaskList;
    }

    public synchronized MonInputAndBSModel getTotalAndPendingCountsOfMiAndBS(int requestType, String currentSelectedDate) {
        MonInputAndBSModel monInputAndBSModel = new MonInputAndBSModel();
        String query = "";

        int dateComparisonResult = dateTimeFormatConversionUtil.currentDateVs25thOfCurrentMonthDate(currentSelectedDate);
        // Means : current date is less than current month 25th date
        if (dateComparisonResult < 0) {
            /*if (requestType == 1) {
                monInputAndBSModel.setTypeMIorBS(1);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" + dateTimeFormatConversionUtil.getPreviousAndNextMonth24Or25Date(-1, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" + dateTimeFormatConversionUtil.getCurrentMonthDateFromDay("24") + "'" +
                        " and other_task_reason_id=10 and Task_Type_Id=7 and Is_Auto in (0,1)";
            } else if (requestType == 2) {
                monInputAndBSModel.setTypeMIorBS(2);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" + dateTimeFormatConversionUtil.getPreviousAndNextMonth24Or25Date(-1, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" + dateTimeFormatConversionUtil.getCurrentMonthDateFromDay("24") + "'" +
                        " and Task_Type_Id=5 and Is_Auto in (0,1)";
            }*/

            if (requestType == 1) {
                monInputAndBSModel.setTypeMIorBS(1);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" +
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(currentSelectedDate, -1, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" +
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(currentSelectedDate, "24") + "'" +
                        " and other_task_reason_id=10 and Task_Type_Id=7 and Is_Auto in (0,1)";
            } else if (requestType == 2) {
                monInputAndBSModel.setTypeMIorBS(2);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" +
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(currentSelectedDate, -1, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" +
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(currentSelectedDate, "24") + "'" +
                        " and Task_Type_Id=5 and Is_Auto in (0,1)";
            }


        } else {
            /*if (requestType == 1) {
                monInputAndBSModel.setTypeMIorBS(1);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" + dateTimeFormatConversionUtil.getCurrentMonthDateFromDay("25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" + dateTimeFormatConversionUtil.getPreviousAndNextMonth24Or25Date(1, "24") + "'" +
                        " and other_task_reason_id=10 and Task_Type_Id=7 and Is_Auto in (0,1)";
            } else if (requestType == 2) {
                monInputAndBSModel.setTypeMIorBS(2);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '" + dateTimeFormatConversionUtil.getCurrentMonthDateFromDay("25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" + dateTimeFormatConversionUtil.getPreviousAndNextMonth24Or25Date(1, "24") + "'" +
                        " and Task_Type_Id=5 and Is_Auto in (0,1)";
            }*/

            if (requestType == 1) {
                monInputAndBSModel.setTypeMIorBS(1);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(currentSelectedDate, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" +
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(currentSelectedDate, 1, "24") + "'" +
                        " and other_task_reason_id=10 and Task_Type_Id=7 and Is_Auto in (0,1)";
            } else if (requestType == 2) {
                monInputAndBSModel.setTypeMIorBS(2);
                query = "select count(1) TotalTaskAssigned, Count(case when task_status_id not in (3,4) then 1 else null end) PendingTask from task where " +
                        "DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' ||" +
                        "substr(estimated_execution_start_date_time,1,2)) >= '"
                        + dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(currentSelectedDate, "25") +
                        "' and DATE(substr(estimated_execution_start_date_time,7,4) ||'-' ||substr(estimated_execution_start_date_time,4,2) ||'-' " +
                        "||substr(estimated_execution_start_date_time,1,2))<= '" +
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(currentSelectedDate, 1, "24") + "'" +
                        " and Task_Type_Id=5 and Is_Auto in (0,1)";
            }
        }

        SQLiteDatabase sqlite = null;

//        Timber.d("MonInput_BS Count query %s", query);

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        monInputAndBSModel.setTotalCountOfMiOrBS(cursor.getInt(cursor.getColumnIndex("TotalTaskAssigned")));
                        monInputAndBSModel.setPendingCountOfMiOrBS(cursor.getInt(cursor.getColumnIndex("PendingTask")));
                    } while (cursor.moveToNext());
                }
            } else {
                monInputAndBSModel.setTotalCountOfMiOrBS(0);
                monInputAndBSModel.setPendingCountOfMiOrBS(0);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return monInputAndBSModel;
    }

    public synchronized KitDistributionStatusMO getTotalAndPendingCountForAKR() {
        KitDistributionStatusMO kitDistributionStatusMO = new KitDistributionStatusMO();
        String query = "select count(1) as TotalKitDistribution, count(case when distribution_status=2 then 1 else null end) " +
                "as PendingKitDistribution from kit_distribution";
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        kitDistributionStatusMO.setTotalCountOfKitsForDistribution(cursor.getInt(cursor.getColumnIndex("TotalKitDistribution")));
                        kitDistributionStatusMO.setPendingCountOfKitsForDistribution(cursor.getInt(cursor.getColumnIndex("PendingKitDistribution")));
                    } while (cursor.moveToNext());
                }
            } else {
                kitDistributionStatusMO.setTotalCountOfKitsForDistribution(0);
                kitDistributionStatusMO.setPendingCountOfKitsForDistribution(0);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return kitDistributionStatusMO;
    }

    public String getRotaDate() {

        String query = "select rota_date from rota order by id desc limit 1";
        String rotaDate = "";
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        rotaDate = dateTimeFormatConversionUtil.convertDateToDayMonthDateFormat(cursor.getString(cursor.getColumnIndex("rota_date")));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return rotaDate;
    }

    public List<EmployeeAuthorizedStrengthMO> getUnitEmpAuthorizedRank(int UnitId, boolean isBarrackInspection, int barrackId) {
        List<EmployeeAuthorizedStrengthMO> employeeAuthorizedStrengthMOList = new ArrayList<>();
        String selectQuery;
        if (isBarrackInspection) {
            selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE
                    + " where " + TableNameAndColumnStatement.BARRACK_ID + "='" + barrackId + "' and " +
                    TableNameAndColumnStatement.RANK_ID + " is null ";
        } else {
            selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE
                    + " where " + TableNameAndColumnStatement.UNIT_ID + "='" + UnitId + "' and " +
                    TableNameAndColumnStatement.RANK_ID + " is not null";
        }
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO = new EmployeeAuthorizedStrengthMO();
                        employeeAuthorizedStrengthMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        employeeAuthorizedStrengthMO.setRankId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID)));
                        employeeAuthorizedStrengthMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        employeeAuthorizedStrengthMO.setRankAbbrevation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ABBREVATION)));
                        employeeAuthorizedStrengthMO.setRankCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_COUNT)));
                        employeeAuthorizedStrengthMO.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        employeeAuthorizedStrengthMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        employeeAuthorizedStrengthMO.setRankName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_NAME)));
                        employeeAuthorizedStrengthMO.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                        employeeAuthorizedStrengthMO.setStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.STRENGTH)));
                        employeeAuthorizedStrengthMO.setActual(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL)));
                        employeeAuthorizedStrengthMOList.add(employeeAuthorizedStrengthMO);
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return employeeAuthorizedStrengthMOList;
    }

    public int[] getUnitGuardsCount(int UnitId) {
     /*   String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE
                + " where " + TableNameAndColumnStatement.UNIT_ID + "='" + UnitId + "'";*/

//        String selectQuery = "select (armed_strength+unarmed_strength) as Strength from unit where unit_id=" + UnitId;

        String selectQuery = "select (u.armed_strength+u.unarmed_strength) as totalStrength,ut.CheckLimit from unit u inner join unit_type ut on u.unit_type=ut.name where unit_id=" + UnitId;

        Cursor cursor;
        int count[] = new int[2];
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    count[0] = cursor.getInt(cursor.getColumnIndex("totalStrength"));
                    count[1] = cursor.getInt(cursor.getColumnIndex("CheckLimit"));
                } while (cursor.moveToNext());
            }
            /*if (cursor != null && !cursor.isClosed()) {
                count = cursor.getInt(cursor.getColumnIndex("Strength"));
            }*/
           /* if (cursor != null && !cursor.isClosed()) {
                count = cursor.getCount();
            }*/
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return count;
    }

    public int getCompletedButNotSyncedTaskCount(String todayDate) {
        String selectQuery = "select count(1)  as taskCount  from task where task_status_id=4 and is_auto in (0,1) and substr(estimated_execution_start_date_time, 0, 11)='" + todayDate + "'";

        Cursor cursor;
        int count = 0;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                count = cursor.getInt(cursor.getColumnIndex("taskCount"));
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return count;
    }

    public List<RotaCalendarMO> getRotaTaskTotalTime(Calendar currentCalendar) {
        int firstDayOfMonth = dateTimeFormatConversionUtil.firstDayOfMonth(currentCalendar);
        int lastDayOfMonth = dateTimeFormatConversionUtil.lastDayOfMonth(currentCalendar);
        List<RotaCalendarMO> rotaCalendarMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT count(id) as taskCount,estimated_execution_start_date_time," +
                "substr(estimated_execution_start_date_time,1,10), " +
                "sum((strftime('%s',substr(estimated_execution_end_date_time,7,4)" +
                "||'-'||substr(estimated_execution_end_date_time,4,2)||'-'" +
                "||substr(estimated_execution_end_date_time,1,2)||' '||" +
                "substr(estimated_execution_end_date_time,12,2) || ':' || " +
                "substr(estimated_execution_end_date_time,15,2) || ':' ||" +
                "substr(estimated_execution_end_date_time,18,2)) - " +
                "strftime('%s',substr(estimated_execution_start_date_time,7,4)" +
                "||'-'||substr(estimated_execution_start_date_time,4,2)||'-'" +
                "||substr(estimated_execution_start_date_time,1,2)||' '||" +
                "substr(estimated_execution_start_date_time,12,2) || ':' || " +
                "substr(estimated_execution_start_date_time,15,2) || ':' ||" +
                "substr(estimated_execution_start_date_time,18,2)))/60) as 'TotalTime'" +
                "FROM task " +
                "WHERE DATE(substr(estimated_execution_start_date_time,7,4)" +
                "||substr(estimated_execution_start_date_time,4,2)" +
                "||substr(estimated_execution_start_date_time,1,2)) " +
                "BETWEEN DATE(" + firstDayOfMonth + ") AND DATE(" + lastDayOfMonth + ") " +
                "GROUP BY substr(estimated_execution_start_date_time,1,10)";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        RotaCalendarMO rotaCalendarMO = new RotaCalendarMO();
                        rotaCalendarMO.setTaskCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_COUNT)));
                        rotaCalendarMO.setCalendarDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_CALENDAR_DATE)));
                        rotaCalendarMO.setEstimatedStartDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        rotaCalendarMO.setTotalTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TOTAL_TIME)));
                        rotaCalendarMOList.add(rotaCalendarMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return rotaCalendarMOList;
    }

    public List<HolidayDateMO> getHolidayDate(int month) {
        String monthStr;
        if (String.valueOf(month).length() == 1) {
            monthStr = "0" + String.valueOf(month);
        } else {
            monthStr = String.valueOf(month);
        }
        List<HolidayDateMO> holidayDateMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * from " + TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE + " where " +
                TableNameAndColumnStatement.IS_ACTIVE + "=1 and substr(" +
                TableNameAndColumnStatement.HOLIDAY_DATE + ", 4, 2) = '" + monthStr + "'";
        //  select * from holiday_calendar where substr(holiday_date, 4, 2) = '11'
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        HolidayDateMO holidayDateMO = new HolidayDateMO();
                        holidayDateMO.setCalendarDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.HOLIDAY_DATE)));
                        holidayDateMOList.add(holidayDateMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return holidayDateMOList;
    }

    public List<EmployeeLeaveCalendar> getLeaveDate(int month) {
        String monthStr;
        if (String.valueOf(month).length() == 1) {
            monthStr = "0" + String.valueOf(month);
        } else {
            monthStr = String.valueOf(month);
        }
        List<EmployeeLeaveCalendar> leaveCalendarList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * from " + TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE + " where " +
                "substr(" + TableNameAndColumnStatement.LEAVE_DATE + ", 4, 2) = '" + monthStr + "'";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        EmployeeLeaveCalendar leaveCalendar = new EmployeeLeaveCalendar();
                        leaveCalendar.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID)));
                        leaveCalendar.setLeaveDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LEAVE_DATE)));
                        leaveCalendarList.add(leaveCalendar);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return leaveCalendarList;
    }

    public ArrayList<SecurityRiskModelMO> getSecurityQuestionOption() {
        int id = -1;
        ArrayList<SecurityRiskModelMO> mGobalDataHolder = new ArrayList<>();
        ArrayList<SecurityRiskOptionMO> mRiskTypeHolder;
        // Model object which holds the security objects.
        SecurityRiskModelMO mSecurityRiskModelMO = null;
        SQLiteDatabase sqlite = null;
        String selectQuestion = "select * from " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_TABLE;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(selectQuestion, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        mSecurityRiskModelMO = new SecurityRiskModelMO();
                        Cursor answerCursor = null;
                        id = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));
                        SecurityRiskMO mSecurityRiskQuestionModel = new SecurityRiskMO();
                        mSecurityRiskQuestionModel.setId(cursor.getColumnIndex(TableNameAndColumnStatement.ID));
                        mSecurityRiskQuestionModel.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTION_DESCRIPTION)));
                        mSecurityRiskQuestionModel.setQuestion(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTIONS)));
                        mSecurityRiskModelMO.setmSecurityTypeTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTIONS)));
                        mSecurityRiskModelMO.setSecurityRiskQuestionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTION_ID)));
                        String selectQuestionOption = "select * from " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_OPTIONS_TABLE + " where " + TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID + "='" + id + "'";
                        answerCursor = sqlite.rawQuery(selectQuestionOption, null);
                        if (answerCursor != null && !answerCursor.isClosed()) {
                            mRiskTypeHolder = new ArrayList<>();
                            if (answerCursor.moveToFirst()) {
                                do {
                                    id = answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID));
                                    SecurityRiskOptionMO model = new SecurityRiskOptionMO();
                                    model.setId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                                    model.setControlType(answerCursor.getString(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_CONTROL_TYPE)));
                                    model.setIsMandatory(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_IS_MANDATORY)));
                                    // model.setIsMandatory(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID))));
                                    model.setSecurityRiskOptionId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_OPTION_ID)));
                                    model.setSecurityRiskOptionName(answerCursor.getString(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_OPTION_NAME)));
                                    model.setSecurityRiskQuestionId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID)));
                                    mRiskTypeHolder.add(model);
                                    mSecurityRiskModelMO.setmRiskTypeHolder(mRiskTypeHolder);
                                } while (answerCursor.moveToNext());
                            }
                        }
                        mGobalDataHolder.add(mSecurityRiskModelMO);
                    } while (cursor.moveToNext());
                }
            }
//            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return mGobalDataHolder;
    }

    public LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> getBarrackInspectionQuestionOption(int fragmentType) {
        LinkedHashMap<Integer, List<BarrackInspectionQuestionOptionMO>> barrackInspectionMap = new LinkedHashMap<>();
        String selectQuery = "";
        // Select All Query
        if (Constants.SPACE_FRAGMENT == fragmentType) {
            selectQuery = "select bq.question_id as 'QuestionId', bo.option_id as 'OptionId', " +
                    "bq.question as 'Question',bo.option_name as 'Option', bq.control_type as 'ControlType' " +
                    "from barrack_inspection_question bq left outer join barrack_inspection_question_option " +
                    "bo on bq.question_id = bo.question_id" +
                    " where bq.question_id in(1,2,3,4,5,6,7) " +
                    " group by bq.question_id, bq.question, bo.option_id"
            ;

        } else if (Constants.MET_LANLORD_FRAGMENT == fragmentType) {
            selectQuery = "select bq.question_id as 'QuestionId', bo.option_id as 'OptionId', " +
                    "bq.question as 'Question',bo.option_name as 'Option', bo.control_type as 'ControlType', bo.is_mandatory as 'isMandatory' " +
                    "from barrack_inspection_question bq left outer join barrack_inspection_question_option " +
                    "bo on bq.question_id = bo.question_id " +
                    " where bq.question_id  not in(1,2,3,4,5,6,7) " +
                    "group by bq.question_id, bq.question, bo.option_id";
        }

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        int tempId = 0;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        List<BarrackInspectionQuestionOptionMO> barrackInspectionMOList = new ArrayList<>();
                        BarrackInspectionQuestionOptionMO questionOptionMO = new BarrackInspectionQuestionOptionMO();
                        if (tempId == cursor.getInt(cursor.
                                getColumnIndex("QuestionId"))) {
                            for (Map.Entry<Integer, List<BarrackInspectionQuestionOptionMO>> e : barrackInspectionMap.entrySet()) {
                                Integer key = e.getKey();
                                if (key == tempId && e.getValue().size() != 0) {
                                    BarrackInspectionQuestionOptionMO barrackInspectionMO = new BarrackInspectionQuestionOptionMO();
                                    for (BarrackInspectionQuestionOptionMO barrackInspectionQuestionOptionMO : e.getValue()) {
                                        barrackInspectionMO.setQuestionId(barrackInspectionQuestionOptionMO.getQuestionId());
                                        barrackInspectionMO.setOptionId(barrackInspectionQuestionOptionMO.getOptionId());
                                        barrackInspectionMO.setQuestion(barrackInspectionQuestionOptionMO.getQuestion());
                                        barrackInspectionMO.setOptions(barrackInspectionQuestionOptionMO.getOptions());
                                        barrackInspectionMO.setControlType(barrackInspectionQuestionOptionMO.getControlType());
                                        barrackInspectionMOList.add(0, barrackInspectionMO);
                                    }
                                }
                            }
                            barrackInspectionMOList.size();
                            questionOptionMO.setQuestionId(cursor.getInt(cursor.getColumnIndex("QuestionId")));
                            questionOptionMO.setOptionId(cursor.getInt(cursor.getColumnIndex("OptionId")));
                            questionOptionMO.setQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                            questionOptionMO.setOptions(cursor.getString(cursor.getColumnIndex("Option")));
                            questionOptionMO.setControlType(cursor.getString(cursor.getColumnIndex("ControlType")));
                            if (fragmentType == Constants.MET_LANLORD_FRAGMENT) {
                                questionOptionMO.isMandatory(cursor.getInt(cursor.getColumnIndex("isMandatory")));
                            }
                            barrackInspectionMOList.add(barrackInspectionMOList.size(), questionOptionMO);
                            barrackInspectionMOList.size();
                            barrackInspectionMap.remove(tempId);
                            barrackInspectionMap.put(tempId, barrackInspectionMOList);
                        } else {
                            tempId = cursor.getInt(cursor.getColumnIndex("QuestionId"));
                            questionOptionMO.setQuestionId(cursor.getInt(cursor.getColumnIndex("QuestionId")));
                            questionOptionMO.setOptionId(cursor.getInt(cursor.getColumnIndex("OptionId")));
                            questionOptionMO.setQuestion(cursor.getString(cursor.getColumnIndex("Question")));
                            questionOptionMO.setOptions(cursor.getString(cursor.getColumnIndex("Option")));
                            questionOptionMO.setControlType(cursor.getString(cursor.getColumnIndex("ControlType")));
                            if (fragmentType == Constants.MET_LANLORD_FRAGMENT) {
                                questionOptionMO.isMandatory(cursor.getInt(cursor.getColumnIndex("isMandatory")));
                            }

                            barrackInspectionMOList.add(questionOptionMO);
                            barrackInspectionMap.put(tempId, barrackInspectionMOList);
                        }
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return barrackInspectionMap;
    }

    public ArrayList<Integer> getRanksIds() {
        ArrayList<Integer> rankIdList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT " + TableNameAndColumnStatement.RANK_ID + " FROM " + TableNameAndColumnStatement.RANK_MASTER_TABLE
                + " where " + TableNameAndColumnStatement.RANK_ABBREVATION + " like  '%AIG_%'  or "
                + TableNameAndColumnStatement.RANK_ABBREVATION + " like  '%BH_%' ";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        rankIdList.add(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID)));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return rankIdList;
    }

    public boolean getTasksWithSameData(int unitId, int taskTypeId, String estimatedStartDateTime, String estimatedEndDateTime) {

        String selectQuery = "select * from " + TableNameAndColumnStatement.TASK_TABLE + " where " +
                TableNameAndColumnStatement.TASK_TYPE_ID + " = " + taskTypeId + " and " +
                TableNameAndColumnStatement.UNIT_ID + " =  " + unitId + " and " +
                TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " = '" + estimatedStartDateTime + "' and  " +
                TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "  = '" + estimatedEndDateTime + "';";

        Timber.d("SameTaskQueryCheck %s", selectQuery);

        Cursor cursor = null;
        boolean isTaskAlreadyCreated = false;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.getCount() != 0) {
                    isTaskAlreadyCreated = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isTaskAlreadyCreated;

    }

    public int getServerTaskId(int localTaskId) {

        String query = " select " + TableNameAndColumnStatement.TASK_ID + " from " + TableNameAndColumnStatement.TASK_TABLE +
                " where " + TableNameAndColumnStatement.ID + " = " + Math.abs(localTaskId);
        int serverTaskId = 0;
        String rotaDate = "";
        SQLiteDatabase sqlite = null;
        Timber.d("RotaTaskId %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        serverTaskId = cursor.getInt(cursor.getColumnIndex("task_id"));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return serverTaskId;
    }

    public StringBuilder getRotaIds(String date) {
        String sqlSupportedDateTimeFormat = Util.getSqlSupportedDateTimeFormat(TableNameAndColumnStatement.ROTA_DATE);
        String selectQuery = " select rota_id from " + TableNameAndColumnStatement.ROTA_TABLE +
                "  where datetime('" + sqlSupportedDateTimeFormat + "') < datetime('" + date + " 00:00:00')";
        Cursor cursor = null;
        StringBuilder rotaIds = new StringBuilder();
//        Timber.i("RotaUpdated checking query %s", selectQuery);
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        rotaIds.append(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ROTA_ID)) + ",");
                        if (cursor.isLast()) {
                            rotaIds.append(cursor.getString(cursor.
                                    getColumnIndex(TableNameAndColumnStatement.ROTA_ID)));
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return rotaIds;
    }

    public StringBuilder getTaskIds(String rotaIds) {
        String selectQuery = "select task_id from " + TableNameAndColumnStatement.ROTA_TASK_TABLE + " where " +
                TableNameAndColumnStatement.ROTA_ID + " in( " + rotaIds + ")";
        Cursor cursor = null;
//        Timber.i("RotaUpdated checking query %s", selectQuery);
        StringBuilder taskIds = new StringBuilder();
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        taskIds.append(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.TASK_ID)) + ",");
                        if (cursor.isLast()) {
                            taskIds.append(cursor.getString(cursor.
                                    getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return taskIds;
    }

    /*public ArrayList<Object> getRotaTaskByPastDate(String date, boolean fromRotaFragment, int barrackId) {
        SQLiteDatabase sqlite = null;
        boolean isAuto = false;
        ArrayList<Object> rotaTaskList = new ArrayList<Object>();
        ArrayList<Object> otherItemsList = new ArrayList<Object>();
        String query;
        if (fromRotaFragment) {
            DateRotaModel dateRotaModel = new DateRotaModel();
            dateRotaModel = getRotaAndTaskCount(date);
            rotaTaskList.add(0, dateRotaModel);
            otherItemsList.add(0, TableNameAndColumnStatement.Other_tasks);
            query = " select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                    "t." + TableNameAndColumnStatement.TASK_ID + "," +
                    "t." + TableNameAndColumnStatement.ID + "," +
                    "t." + TableNameAndColumnStatement.BARRACK_ID + "," +
                    "l." + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                    TableNameAndColumnStatement.TASK_STATUS + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.CREATED_DATE + "," +
                    *//*"t." + TableNameAndColumnStatement.ADHOC_STATUS+ "," +*//*
                    "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                    "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                    "t." + TableNameAndColumnStatement.IS_AUTO +
                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    " t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                    " u on t." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                    " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                    " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                    + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                    "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus'"
                    + " left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t."
                    + TableNameAndColumnStatement.BARRACK_ID +
                    " = " + "b." + TableNameAndColumnStatement.BARRACK_ID +
                    " where t." + TableNameAndColumnStatement.CREATED_DATE +
                    " = " + "'" + date + "' "
                    + " order by " +
                    TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";

        } else {
            query = "select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                    "t." + TableNameAndColumnStatement.TASK_ID + "," +
                    "t." + TableNameAndColumnStatement.ID + "," +
                    "t." + TableNameAndColumnStatement.BARRACK_ID + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                    "b." + TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.CREATED_DATE + "," +
                   *//* "t." + TableNameAndColumnStatement.ADHOC_STATUS+ "," +*//*
                    "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                    "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                    "t." + TableNameAndColumnStatement.OTHER_TASK_REASON_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                    "l." + TableNameAndColumnStatement.LOOKUP_NAME + " as " +
                    TableNameAndColumnStatement.TASK_STATUS + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.IS_AUTO +
                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    "  t left join " + TableNameAndColumnStatement.UNIT_TABLE +
                    " u on t." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                    " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                    " l  on t." + TableNameAndColumnStatement.TASK_STATUS_ID
                    + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                    "  and  l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'TaskStatus'" +
                    " left join " + TableNameAndColumnStatement.BARRACK_TABLE + " b on t." +
                    TableNameAndColumnStatement.BARRACK_ID +
                    " = " + "b." + TableNameAndColumnStatement.BARRACK_ID +
                    " where t." + TableNameAndColumnStatement.CREATED_DATE +
                    " = " + "'" + date + "' and " +
                    "t." + TableNameAndColumnStatement.BARRACK_ID + " = " + "'" +
                    barrackId
                    + " order by " +
                    TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " asc ";
            ;
        }
        try {
            sqlite = this.getReadableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
//            String billingStatus = "";
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)) == 1) {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            rotaTaskList.add(rotaTaskModel);
                       *//* } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO))) == 0) {*//*
                        } else {
                            RotaTaskModel rotaTaskModel = new RotaTaskModel();
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                            rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                            String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                            String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                            rotaTaskModel.setStartDate(startDate);
                            rotaTaskModel.setEndDate(endDate);
                            rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                            rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                            String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                            String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                            String time = startTime + " - " + endTime;
                            rotaTaskModel.setStartTime(startTime);
                            rotaTaskModel.setEndTime(endTime);
                            rotaTaskModel.setTime(time);
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                            rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                            rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                            *//* if(TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ADHOC_STATUS)))){
                                rotaTaskModel.setTaskStatus(resources.getString(R.string.pending));
                            }
                            else{
                                rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ADHOC_STATUS)));
                            }*//*
                            rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                            rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                            otherItemsList.add(otherItemsList.size(), rotaTaskModel);
                            if (!fromRotaFragment) {
                                rotaTaskList.addAll(otherItemsList);
                            }
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        if (fromRotaFragment) {
            if (rotaTaskList.size() == 1) {
                NoTaskMO noTaskMO = new NoTaskMO();
                int dateComparesion = dateTimeFormatConversionUtil.dateComparison(dateTimeFormatConversionUtil.getCurrentDate(), date);
                if (dateComparesion == 0) {
                    noTaskMO.setMessage(TableNameAndColumnStatement.NO_TASK_FOR_TODAY);
                } else {
                    String requiredDate = dateTimeFormatConversionUtil.convertDateToDayMonthDateFormat(date).split(",")[1];
                    noTaskMO.setMessage(TableNameAndColumnStatement.NO_TASK_FOR_SPECIFIC_DAY + " " + requiredDate);
                }
                rotaTaskList.add(rotaTaskList.size(), noTaskMO);
            }
            if (otherItemsList.size() > 1) {
                rotaTaskList.addAll(otherItemsList);
            }
        }
        return rotaTaskList;
    }*/

   /* public ArrayList<SecurityRiskModelMO> InsertSecurityQuestionOption() {
        int id = -1;
        // global array list to hold the iterated item.
        ArrayList<SecurityRiskModelMO> mGobalDataHolder = new ArrayList<>();
        //Array List to hold the list of questions for a given id.
        ArrayList<SecurityRiskOptionMO> mRiskTypeHolder;
        // Model object which holds the security objects.
        SecurityRiskModelMO mSecurityRiskModelMO = null;
        SQLiteDatabase sqlite = null;
        String selectQuestion = "select * from " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_TABLE;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(selectQuestion, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        mSecurityRiskModelMO = new SecurityRiskModelMO();
                        Cursor answerCursor = null;
                        id = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));
                        SecurityRiskMO mSecurityRiskQuestionModel = new SecurityRiskMO();
                        mSecurityRiskQuestionModel.setId(cursor.getColumnIndex(TableNameAndColumnStatement.ID));
                        mSecurityRiskQuestionModel.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTION_DESCRIPTION)));
                        mSecurityRiskQuestionModel.setQuestion(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTIONS)));
                        mSecurityRiskModelMO.setmSecurityTypeTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTIONS)));
                        mSecurityRiskModelMO.setSecurityRiskQuestionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_QUESTION_ID)));
                        String selectQuestionOption = "select * from " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_OPTIONS_TABLE + " where " +
                                TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID + "='" + id + "'";
                        answerCursor = sqlite.rawQuery(selectQuestionOption, null);
                        if (answerCursor != null && !answerCursor.isClosed()) {
                            mRiskTypeHolder = new ArrayList<>();
                            if (answerCursor.moveToFirst()) {
                                do {
                                    id = answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID));
                                    SecurityRiskOptionMO model = new SecurityRiskOptionMO();
                                    model.setId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                                    model.setControlType(answerCursor.getString(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_CONTROL_TYPE)));
                                    model.setIsMandatory(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_IS_MANDATORY)));
                                    // model.setIsMandatory(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.ID))));
                                    model.setSecurityRiskOptionId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_OPTION_ID)));
                                    model.setSecurityRiskOptionName(answerCursor.getString(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_OPTION_NAME)));
                                    model.setSecurityRiskQuestionId(answerCursor.getInt(answerCursor.getColumnIndex(TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID)));
                                    mRiskTypeHolder.add(model);
                                    mSecurityRiskModelMO.setmRiskTypeHolder(mRiskTypeHolder);
                                } while (answerCursor.moveToNext());
                            }
                        }
                        mGobalDataHolder.add(mSecurityRiskModelMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return mGobalDataHolder;
    }*/

   /* public List<BarrackInspectionQuestionMO> getBarrackInspectionQuestion() {
        List<BarrackInspectionQuestionMO> barrackInspectionQuestionMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.BARRACK_INSPECTION_QUESTION_TABLE
                + " where " + TableNameAndColumnStatement.IS_ACTIVE + "='" + true + "'";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        BarrackInspectionQuestionMO barrackInspectionQuestionMO = new BarrackInspectionQuestionMO();
                        barrackInspectionQuestionMO.setSequenceId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ID)));
                        barrackInspectionQuestionMO.setId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.QUESTION_ID)));
                        barrackInspectionQuestionMO.setQuestion(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.QUESTION)));
                        barrackInspectionQuestionMO.setDescription(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTION)));
                        barrackInspectionQuestionMO.setControlType(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CONTROL_TYPE)));
                      *//*  barrackInspectionQuestionMO.setBarrackId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.IS_ACTIVE))));*//*
                        barrackInspectionQuestionMOList.add(barrackInspectionQuestionMO);
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return barrackInspectionQuestionMOList;
    }*/

    /*public int isHolidayAndLeaveAvailable(int employee_id, String estimatedStartTime, String estimatedEndTime) {
        int isLeaveAvailable = 0;
        String[] startDate = estimatedStartTime.split(" ");
        String[] endDate = estimatedEndTime.split(" ");
        // Select All Query
        String selectQuery = "select count(*) as isAvailable from (select " + TableNameAndColumnStatement.CREATED_DATE
                + " from " + TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE + " union select "
                + TableNameAndColumnStatement.CREATED_DATE + " from "
                + TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE
                + " where " + TableNameAndColumnStatement.EMPLOYEE_ID + "='" + employee_id + "')"
                + " leavedate where " +
                "leavedate." + TableNameAndColumnStatement.CREATED_DATE + " in ('" + startDate[0] + "','" + endDate[0] + "')";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        isLeaveAvailable = (cursor.getInt(cursor.getColumnIndex("isAvailable")));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isLeaveAvailable;
    }*/

    /*public List<HolidayAndLeaveDateMO> getHolidayAndLeaveDate(int employee_id, String estimatedStartTime, String estimatedEndTime) {
        List<HolidayAndLeaveDateMO> leaveDateMOList = new ArrayList<>();
        String[] startDate = estimatedStartTime.split(" ");
        String[] endDate = estimatedEndTime.split(" ");
        // Select All Query
        String selectQuery = "select " +
                " leavedate." + TableNameAndColumnStatement.CREATED_DATE + " from (select " + TableNameAndColumnStatement.CREATED_DATE
                + " from " + TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE + " union select "
                + TableNameAndColumnStatement.CREATED_DATE + " from "
                + TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE
                + " where " + TableNameAndColumnStatement.EMPLOYEE_ID + "='" + employee_id + "')"
                + " leavedate where " +
                "leavedate." + TableNameAndColumnStatement.CREATED_DATE + " in ('" + startDate[0] + "','" + endDate[0] + "')";
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        HolidayAndLeaveDateMO holidayAndLeaveDateMO = new HolidayAndLeaveDateMO();
                        holidayAndLeaveDateMO.setCreate_date(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE)));
                        leaveDateMOList.add(holidayAndLeaveDateMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return leaveDateMOList;
    }*/

   /* public CompletedRotaTaskMo getAllTaskToSync() {
        HashMap<Integer, RotaTaskInputRequestMO> rotaTaskList = new HashMap<>();
        CompletedRotaTaskMo completedRotaTaskMo = new CompletedRotaTaskMo();
        int unsyncedCompletedCount = 0;
        String taskQuery = "";
        *//*String taskQuery = "SELECT  * FROM " + TableNameAndColumnStatement.TASK_TABLE + " where " +
                TableNameAndColumnStatement.TASK_STATUS_ID + " in ( " + 4 + "," + 5 + " ) or " +
                TableNameAndColumnStatement.TASK_ID + " = '" + 0 + "' or "+
                TableNameAndColumnStatement.IS_TASK_STARTED+ " = '" + RotaStartTaskEnum.InProgress.getValue() + "'";*//*
       *//* taskQuery = " select * from task where is_synced = 0 or (" +
                     TableNameAndColumnStatement.IS_TASK_STARTED + " = '" +
                RotaStartTaskEnum.InProgress.getValue()  + "' and "+
                TableNameAndColumnStatement.IS_SYNCED + " = "+ 1 + " )";*//*
        taskQuery = " select * from task where is_synced = 0";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(taskQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        RotaTaskInputRequestMO rotaTaskMO = new RotaTaskInputRequestMO();
                        rotaTaskMO.setIsTaskStarted(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_TASK_STARTED)));
                        rotaTaskMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        int unitId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        if (unitId == 0) {
                            rotaTaskMO.setUnitId(null);
                        } else {
                            rotaTaskMO.setUnitId(unitId);
                        }

                        rotaTaskMO.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                        rotaTaskMO.setAssigneeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setEstimatedExecutionTime(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME)));
                        rotaTaskMO.setActualExecutionTime(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME)));
                        rotaTaskMO.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                        rotaTaskMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTIONS)));
                        rotaTaskMO.setEstimatedTaskExecutionStartDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setEstimatedTaskExecutionEndDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionStartDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionEndDateTime
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME)));

                        int barrackId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID));
                        if (barrackId != 0) {
                            rotaTaskMO.setBarrackId(barrackId);

                        } else {
                            rotaTaskMO.setBarrackId(null);
                        }
                        rotaTaskMO.setIsAutoCreation(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)));
                        rotaTaskMO.setExecutorId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        rotaTaskMO.setCreatedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        String taskExecutionResult = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_EXECUTION_RESULT));
                        if (null != taskExecutionResult && taskExecutionResult.length() != 0) {
                            if (taskExecutionResult.equalsIgnoreCase("null")) {
                            } else {
                                JsonObject jsonObject = (new JsonParser()).parse(taskExecutionResult).getAsJsonObject();
                                rotaTaskMO.setTaskExecutionResult(jsonObject);
                            }
                        }
                        rotaTaskMO.setRelatedTaskId(0);
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)) == 0) {
                            rotaTaskMO.setChangeRouteReasonId(null);
                        } else {
                            rotaTaskMO.setChangeRouteReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)));
                        }

                        rotaTaskMO.setUnitPostId(null);
                        rotaTaskMO.setAdditionalComments("");
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)) == 0) {
                            rotaTaskMO.setApprovedById(null);
                        } else {
                            rotaTaskMO.setApprovedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)));

                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)) == 0) {
                            rotaTaskMO.setOtherReasonId(null);
                        } else {
                            rotaTaskMO.setOtherReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));

                        }
                        rotaTaskList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)), rotaTaskMO);
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)) == 4) {
                            unsyncedCompletedCount++;
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        completedRotaTaskMo.setUnSyncedCompletedTaks(unsyncedCompletedCount);
        completedRotaTaskMo.setUnSyncedCompletedTasksMap(rotaTaskList);
        return completedRotaTaskMo;
    }*/

    /*public boolean isRotaUpdated(String currentDate) {
        // Select All Query
        String selectQuery = "select * from " + TableNameAndColumnStatement.ROTA_TABLE + " where " +
                TableNameAndColumnStatement.ROTA_DATE + " = '" + currentDate + " 00:00:00'";
        Cursor cursor = null;
        boolean isRotaUpdated = false;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.getCount() != 0) {
                    isRotaUpdated = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isRotaUpdated;
    }*/

    /*public RotaTaskModel getTaskUsingNfcTag(String uniqueId, int rotaWeek, String date) {
        SQLiteDatabase sqlite = null;
        RotaTaskModel rotaTaskModel = null;
        String query = "";

        boolean isNfcConfiguredForUnit = false;
        String nfcConfiguredUnitQuery = " select unit_id from unit_post where deviceno = '" + uniqueId + "'";
        String nfcCOnfiguredBarrackQuery = " select barrack_id from barrack where deviceno = '" + uniqueId + "'";


        try {
            sqlite = this.getReadableDatabase();
            if (sqlite.rawQuery(nfcConfiguredUnitQuery, null).getCount() != 0) {
                isNfcConfiguredForUnit = true;
                query = " select t.id as id,t.task_type_id as task_type_id ,t.task_status_id as task_status_id," +
                        " t.task_id as task_id,t.estimated_execution_start_date_time as estimated_execution_start_date_time," +
                        " t.estimated_execution_end_date_time as estimated_execution_end_date_time,t.task_type_id as task_type_id," +
                        " t.other_task_reason_id as other_task_reason_id,t.is_auto as is_auto, " +
                        " u.unit_name as unit_name ,u.unit_id as unit_id ,u.unit_full_address as unit_full_address " +
                        " from task t " +
                        " join unit u on u.unit_id = t.unit_id and u.unit_id = (select unit_id from unit_post where deviceno = '" +
                        uniqueId + "') " +
                        " union " +
                        " select t.id as id,t.task_type_id as task_type_id ,t.task_status_id as task_status_id," +
                        " t.task_id as task_id,t.estimated_execution_start_date_time as estimated_execution_start_date_time," +
                        " t.estimated_execution_end_date_time as estimated_execution_end_date_time,t.task_type_id as task_type_id," +
                        " t.other_task_reason_id as other_task_reason_id,t.is_auto as is_auto, " +
                        " u.unit_name as unit_name ,u.unit_id as unit_id ,u.unit_full_address as unit_full_address " +
                        " from task t " +
                        " join unit u on u.unit_id = t.unit_id and u.unit_id = (select unit_id from unit_post where deviceno = '" +
                        uniqueId + "') " +
                        " where substr(estimated_execution_start_date_time, 0, 11) = '" + date + "' and t.task_status_id in (1,2) " +
                        " and t.task_type_id in (1,2)" +
                        " order by t.task_id limit 1";


            } else if (sqlite.rawQuery(nfcCOnfiguredBarrackQuery, null).getCount() != 0) {
                isNfcConfiguredForUnit = false;
                query = " select t.id as id,t.task_type_id as task_type_id ,t.task_status_id as task_status_id," +
                        " t.task_id as task_id,t.estimated_execution_start_date_time as estimated_execution_start_date_time," +
                        " t.estimated_execution_end_date_time as estimated_execution_end_date_time,t.task_type_id as task_type_id," +
                        " t.other_task_reason_id as other_task_reason_id,t.is_auto as is_auto, " +
                        " b.barrack_name as barrack_name, b.barrack_id as barrack_id,b.barrack_full_address as barrack_full_address " +
                        " from rota r join rota_task rt on r.rota_id = rt.rota_id and r.rota_week = " + rotaWeek +
                        " join task t on t.task_id = rt.task_id and t.task_status_id in (1,2) and t.task_type_id = 3 " +
                        " join barrack b on b.barrack_id = t.barrack_id and b.deviceno =  '" +
                        uniqueId + "' " +
                        " union " +
                        " select t.id as id,t.task_type_id as task_type_id ,t.task_status_id as task_status_id," +
                        " t.task_id as task_id,t.estimated_execution_start_date_time as estimated_execution_start_date_time," +
                        " t.estimated_execution_end_date_time as estimated_execution_end_date_time,t.task_type_id as task_type_id," +
                        " t.other_task_reason_id as other_task_reason_id,t.is_auto as is_auto, " +
                        " b.barrack_name as barrack_name, b.barrack_id as barrack_id,b.barrack_full_address as barrack_full_address " +
                        " from task t " +
                        " join  barrack b on b.barrack_id = t.barrack_id and  b.deviceno = '" +
                        uniqueId + "' " +
                        " where substr(estimated_execution_start_date_time, 0, 11) = '" + date + "' and t.task_status_id in (1,2) " +
                        " and t.task_type_id = 3 " +
                        " order by t.task_id limit 1";
            }
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        rotaTaskModel = new RotaTaskModel();
                        String typeChecking = RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name();
                        rotaTaskModel.setTypeChecking(typeChecking.replace("_", " "));
                        rotaTaskModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        rotaTaskModel.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                        String startTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        String endTime = dateTimeFormatConversionUtil.convertionDateTimeTo12HourTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                        String time = startTime + " - " + endTime;
                        rotaTaskModel.setStartTime(startTime);
                        rotaTaskModel.setEndTime(endTime);
                        rotaTaskModel.setTime(time);
                        String startDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)).split(" ")[0];
                        String endDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)).split(" ")[0];
                        rotaTaskModel.setStartDate(startDate);
                        rotaTaskModel.setEndDate(endDate);
                        if (isNfcConfiguredForUnit) {
                            rotaTaskModel.setAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                            rotaTaskModel.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            rotaTaskModel.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        } else {

                            rotaTaskModel.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            rotaTaskModel.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                            rotaTaskModel.setBarrackAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS)));
                        }

                        rotaTaskModel.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        rotaTaskModel.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));


                        rotaTaskModel.setIcon(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());
                        rotaTaskModel.setLightTypeCheckingIcon(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))).name()).getValue());

                        //rotaTaskModel.setTaskStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                        //rotaTaskModel.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)) == 0) {
                            rotaTaskModel.setRotaWeek(dateTimeFormatConversionUtil.getWeekOfTheYear(date));
                        } else {
                            rotaTaskModel.setRotaWeek(rotaWeek);
                        }
                        rotaTaskModel.setOtherTaskReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));
                       *//* } else if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO))) == 0) {*//*

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return rotaTaskModel;
    }*/
}
