package com.sisindia.ai.android.issues.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Saruchi on 08-08-2016.
 */

public class ImprovementPlanListMO implements Parcelable {


    private int seqId;  // sequence id
    private String barrackName;
    private String sourceType;
    private String improvementPlanCategoryName;
    private int improvementPlanCategoryId;
    private String improvementPlanId;
    private String unitName;
    private String improvementStatus;
    private int improvementStatusId;
    private String ClosureDateTime;
    private String expectedClosureDate; //target date
    private int improvementId;
    private String assignedToName;
    private int assignedToId;
    private String createdDateTime;
    private String actionPlanName;
    private String closureRemarks;
    private String remarks;
    private int actionPlanId;
    private Integer issueMatrixId;
    private Integer unitId;
    private Integer barrackId;
    private Integer targetEmployeeId;
    private Integer issueId;
    private Integer closedById;
    private String budget;

    public ImprovementPlanListMO() {
    }

    public String getImprovementPlanCategoryName() {
        return improvementPlanCategoryName;
    }

    public void setImprovementPlanCategoryName(String improvementPlanCategoryName) {
        this.improvementPlanCategoryName = improvementPlanCategoryName;
    }

    public int getImprovementPlanCategoryId() {
        return improvementPlanCategoryId;
    }

    public void setImprovementPlanCategoryId(int improvementPlanCategoryId) {
        this.improvementPlanCategoryId = improvementPlanCategoryId;
    }

    public String getImprovementPlanId() {
        return improvementPlanId;
    }

    public void setImprovementPlanId(String improvementPlanId) {
        this.improvementPlanId = improvementPlanId;
    }

    public String getClosureDateTime() {
        return ClosureDateTime;
    }

    public void setClosureDateTime(String closureDateTime) {
        ClosureDateTime = closureDateTime;
    }

    public String getUnitName() {

        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getImprovementStatus() {
        return improvementStatus;
    }

    public void setImprovementStatus(String improvementStatus) {
        this.improvementStatus = improvementStatus;
    }

    public int getImprovementStatusId() {
        return improvementStatusId;
    }

    public void setImprovementStatusId(int improvementStatusId) {
        this.improvementStatusId = improvementStatusId;
    }

    public String getExpectedClosureDate() {
        return expectedClosureDate;
    }

    public void setExpectedClosureDate(String expectedClosureDate) {
        this.expectedClosureDate = expectedClosureDate;
    }

    public int getImprovementId() {
        return improvementId;
    }

    public void setImprovementId(int improvementId) {
        this.improvementId = improvementId;
    }

    public String getAssignedToName() {
        return assignedToName;
    }

    public void setAssignedToName(String assignedToName) {
        this.assignedToName = assignedToName;
    }

    public int getAssignedToId() {
        return assignedToId;
    }

    public void setAssignedToId(int assignedToId) {
        this.assignedToId = assignedToId;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getActionPlanName() {
        return actionPlanName;
    }

    public void setActionPlanName(String actionPlanName) {
        this.actionPlanName = actionPlanName;
    }

    public int getSeqId() {
        return seqId;
    }

    public void setSeqId(int seqId) {
        this.seqId = seqId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getClosureRemarks() {
        return closureRemarks;
    }

    public void setClosureRemarks(String closureRemarks) {
        this.closureRemarks = closureRemarks;
    }

    public Integer getIssueMatrixId() {
        return issueMatrixId;
    }

    public void setIssueMatrixId(Integer issueMatrixId) {
        this.issueMatrixId = issueMatrixId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getBarrackId() {
        return barrackId;
    }

    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    public Integer getTargetEmployeeId() {
        return targetEmployeeId;
    }

    public void setTargetEmployeeId(Integer targetEmployeeId) {
        this.targetEmployeeId = targetEmployeeId;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    public Integer getClosedById() {
        return closedById;
    }

    public void setClosedById(Integer closedById) {
        this.closedById = closedById;
    }

    public Integer getActionPlanId() {
        return actionPlanId;
    }

    public void setActionPlanId(int actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    public void setActionPlanId(Integer actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    @Override
    public String toString() {
        return "ImprovementPlanListMO{" +
                "seqId=" + seqId +
                ", barrackName='" + barrackName + '\'' +
                ", sourceType='" + sourceType + '\'' +
                ", improvementPlanCategoryName='" + improvementPlanCategoryName + '\'' +
                ", improvementPlanCategoryId=" + improvementPlanCategoryId +
                ", improvementPlanId='" + improvementPlanId + '\'' +
                ", unitName='" + unitName + '\'' +
                ", improvementStatus='" + improvementStatus + '\'' +
                ", improvementStatusId=" + improvementStatusId +
                ", ClosureDateTime='" + ClosureDateTime + '\'' +
                ", expectedClosureDate='" + expectedClosureDate + '\'' +
                ", improvementId=" + improvementId +
                ", assignedToName='" + assignedToName + '\'' +
                ", assignedToId=" + assignedToId +
                ", createdDateTime='" + createdDateTime + '\'' +
                ", actionPlanName='" + actionPlanName + '\'' +
                ", closureRemarks='" + closureRemarks + '\'' +
                ", remarks='" + remarks + '\'' +
                ", actionPlanId=" + actionPlanId +
                ", issueMatrixId=" + issueMatrixId +
                ", unitId=" + unitId +
                ", barrackId=" + barrackId +
                ", targetEmployeeId=" + targetEmployeeId +
                ", issueId=" + issueId +
                ", closedById=" + closedById +
                ", budget='" + budget + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.seqId);
        dest.writeString(this.barrackName);
        dest.writeString(this.sourceType);
        dest.writeString(this.improvementPlanCategoryName);
        dest.writeInt(this.improvementPlanCategoryId);
        dest.writeString(this.improvementPlanId);
        dest.writeString(this.unitName);
        dest.writeString(this.improvementStatus);
        dest.writeInt(this.improvementStatusId);
        dest.writeString(this.ClosureDateTime);
        dest.writeString(this.expectedClosureDate);
        dest.writeInt(this.improvementId);
        dest.writeString(this.assignedToName);
        dest.writeInt(this.assignedToId);
        dest.writeString(this.createdDateTime);
        dest.writeString(this.actionPlanName);
        dest.writeString(this.closureRemarks);
        dest.writeString(this.remarks);
        dest.writeInt(this.actionPlanId);
        dest.writeValue(this.issueMatrixId);
        dest.writeValue(this.unitId);
        dest.writeValue(this.barrackId);
        dest.writeValue(this.targetEmployeeId);
        dest.writeValue(this.issueId);
        dest.writeValue(this.closedById);
        dest.writeString(this.budget);
    }

    protected ImprovementPlanListMO(Parcel in) {
        this.seqId = in.readInt();
        this.barrackName = in.readString();
        this.sourceType = in.readString();
        this.improvementPlanCategoryName = in.readString();
        this.improvementPlanCategoryId = in.readInt();
        this.improvementPlanId = in.readString();
        this.unitName = in.readString();
        this.improvementStatus = in.readString();
        this.improvementStatusId = in.readInt();
        this.ClosureDateTime = in.readString();
        this.expectedClosureDate = in.readString();
        this.improvementId = in.readInt();
        this.assignedToName = in.readString();
        this.assignedToId = in.readInt();
        this.createdDateTime = in.readString();
        this.actionPlanName = in.readString();
        this.closureRemarks = in.readString();
        this.remarks = in.readString();
        this.actionPlanId = in.readInt();
        this.issueMatrixId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.unitId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.barrackId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.targetEmployeeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.issueId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.closedById = (Integer) in.readValue(Integer.class.getClassLoader());
        this.budget = in.readString();
    }

    public static final Creator<ImprovementPlanListMO> CREATOR = new Creator<ImprovementPlanListMO>() {
        @Override
        public ImprovementPlanListMO createFromParcel(Parcel source) {
            return new ImprovementPlanListMO(source);
        }

        @Override
        public ImprovementPlanListMO[] newArray(int size) {
            return new ImprovementPlanListMO[size];
        }
    };
}
