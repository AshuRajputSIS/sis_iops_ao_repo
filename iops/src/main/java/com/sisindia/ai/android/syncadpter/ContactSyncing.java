package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.myunits.models.UnitContactIR;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.network.response.UnitContactOR;
import com.sisindia.ai.android.utils.Util;
import java.net.HttpURLConnection;
import java.util.HashMap;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 12-07-2016.
 */

public class ContactSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private HashMap<Integer, UnitContactIR> contactInputRequestList;

    public ContactSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        contactSyncing();
    }

    private void contactSyncing() {
        contactInputRequestList = getAllContactsToSync();
        if (contactInputRequestList != null && contactInputRequestList.size() != 0) {
            for (Integer key : contactInputRequestList.keySet()) {
                UnitContactIR unitContactIR = contactInputRequestList.get(key);
                contactApiCall(unitContactIR, key);

                Timber.d(Constants.TAG_SYNCADAPTER+"unitContactSyncing   %s", "key: " + key + " value: " + contactInputRequestList.get(key));
            }

        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"unitContactSyncing -->nothing to sync ");
        }

    }

    private void contactApiCall(final UnitContactIR unitContact, final int key) {
        sisClient.getApi().syncUnitContact(unitContact, new Callback<ApiResponse<UnitContactOR>>() {
            @Override
            public void success(ApiResponse<UnitContactOR>  unitContactORApiResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitContactSyncing response  %s", unitContactORApiResponse);
                switch (unitContactORApiResponse.statusCode)
                {
                    case HttpURLConnection.HTTP_OK:
                        updateContactIdTable(
                                unitContactORApiResponse.data.getUnitContactId(), key);

                        Timber.d(Constants.TAG_SYNCADAPTER+"unitContact count   %s", "count: " );
                        break;
                    default:

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitContactSyncing count   %s", "count: ");
                Util.printRetorfitError("unitContactSyncing", error);
            }
        });
    }


    private  HashMap<Integer, UnitContactIR> getAllContactsToSync(){
        HashMap<Integer, UnitContactIR> contactInputRequest = new HashMap<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE + " where " +
                TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' or " +
                TableNameAndColumnStatement.UNIT_CONTACT_ID + " = '" + 0 + "'";
        Cursor cursor = null;
        try {
            SQLiteDatabase db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitContactIR unitContactIR = new UnitContactIR();
                        unitContactIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_ID)));
                        unitContactIR.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        unitContactIR.setFirstName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)));
                        unitContactIR.setLastName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME)));
                        unitContactIR.setEmailId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMAIL_ADDRESS)));
                        unitContactIR.setContactNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        unitContactIR.setDesignation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESIGNATION)));


                        contactInputRequest.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), unitContactIR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"contactInputRequest object  %s", IOPSApplication.getGsonInstance().toJson(contactInputRequest));
        }
        return contactInputRequest;
    }

    /**
     *
     * @param contactId
     * @param id
     */
    protected void updateContactIdTable( int contactId, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE +
                " SET " + TableNameAndColumnStatement.UNIT_CONTACT_ID + "= '" + contactId +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }
}