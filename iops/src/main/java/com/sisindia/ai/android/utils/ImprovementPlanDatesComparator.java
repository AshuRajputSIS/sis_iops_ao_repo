package com.sisindia.ai.android.utils;

import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;

import java.util.Comparator;

/**
 * Created by compass on 3/16/2017.
 */

public class ImprovementPlanDatesComparator implements Comparator<Object> {
    DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

     public  ImprovementPlanDatesComparator(){
         dateTimeFormatConversionUtil  = new DateTimeFormatConversionUtil();

    }

    @Override
    public int compare(Object improvementPlan1, Object improvementPlan2) {
        return dateTimeFormatConversionUtil.convertStringToDate(((ImprovementPlanListMO)improvementPlan2).
                getCreatedDateTime()).compareTo( dateTimeFormatConversionUtil.
                convertStringToDate(((ImprovementPlanListMO)improvementPlan1).getCreatedDateTime()));
    }
}
