package com.sisindia.ai.android.rota;

import com.google.gson.annotations.SerializedName;

public class OtherActivityBase {

@SerializedName("OtherActivity")
private OtherActivityIR otherActivity;

/**
* 
* @return
* The otherActivity
*/
public OtherActivityIR getOtherActivity() {
return otherActivity;
}

/**
* 
* @param otherActivity
* The OtherActivity
*/
public void setOtherActivity(OtherActivityIR otherActivity) {
this.otherActivity = otherActivity;
}

}