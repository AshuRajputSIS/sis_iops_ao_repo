package com.sisindia.ai.android.unitraising;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by compass on 8/10/2017.
 */

public class RaisingDetailsMo implements Serializable {
    public int unitId;
    public String unitName;
    public String expectedRaisingDate;
    public String raisingDetails;
    public String raisingLocation;
    public String remarks;
    public int raisingStatus;
    public String unitCode;
    public int raisingId;
    public boolean validationStatus;
}
