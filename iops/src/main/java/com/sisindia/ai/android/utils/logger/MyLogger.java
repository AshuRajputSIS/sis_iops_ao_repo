package com.sisindia.ai.android.utils.logger;

import android.content.Context;
import android.os.Environment;

import com.sisindia.ai.android.BuildConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.brightinventions.slf4android.FileLogHandlerConfiguration;
import pl.brightinventions.slf4android.LoggerConfiguration;

/**
 * Created by Hannan Shaik on 13/07/16.
 */

public class MyLogger {

    private String className;
    private static Logger Log;
    public static String LOG_FOLDER = Environment.getExternalStorageDirectory().getPath() + "/"+ BuildConfig.APPLICATION_ID + "/logs/";
    private static int PURGE_DAYS = 7;

    public static void setup(Context context) {
        Date date = new Date();
        String today = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).format(date);

        File logDirectory = new File(LOG_FOLDER);
        if(!logDirectory.exists()){
            boolean isCreated = logDirectory.mkdirs();
            android.util.Log.e("Created",isCreated+"");
        }

        FileLogHandlerConfiguration fileHandler = LoggerConfiguration.fileLogHandler(context);
        fileHandler.setFullFilePathPattern(logDirectory.getPath() + "/"+today+".log");

        LoggerConfiguration.configuration()
                .removeRootLogcatHandler()
                .addHandlerToRootLogger(fileHandler);

        deleteOlderFiles();
    }

    private static void deleteOlderFiles(){
        final long purgeTime =
                System.currentTimeMillis() - (PURGE_DAYS * 24 * 60 * 60 * 1000);


        File logDirectory = new File(LOG_FOLDER);
        if(logDirectory.exists()) {
            File[] files = logDirectory.listFiles();
            if (files != null && files.length != 0)
            {
                for (File file : files) {
                    if (file.lastModified() < purgeTime) {
                        file.delete();
                    }
                }
        }
        }
    }

    public static MyLogger getInstance(String className){
        MyLogger instance = new MyLogger();
        instance.className = className;
        Log = LoggerFactory.getLogger(className);
        return instance;
    }

    public void d(String message){
        android.util.Log.d(className, message);
        Log.debug(message);
    }

    public void e(String message){
        android.util.Log.e(className, message);
        Log.error(message);
    }

    public void e(String message, Throwable t){
        android.util.Log.e(className, message, t);
        Log.error(message, t);
    }

    public void i(String message){
        android.util.Log.i(className, message);
        Log.info(message);
    }

}
