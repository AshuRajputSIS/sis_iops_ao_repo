package com.sisindia.ai.android.utils.nfcreader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;

import timber.log.Timber;

/**
 * Created by Hannan Shaik on 7/18/16.
 */
public class NFCReceiver extends BroadcastReceiver {

    private static final String TAG = NFCReceiver.class.getSimpleName();

    private boolean nfcAddAllowed;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(Constants.Nfc.NFC_UNIQUE_ID)) {

            String uniqueId = intent.getStringExtra(Constants.Nfc.NFC_UNIQUE_ID);

            if (!TextUtils.isEmpty(uniqueId)) {

                setupNfcDataAndSync(context, uniqueId);


                /*UnitPost postInformation = IOPSApplication.getInstance()
                        .getUnitLevelSelectionQueryInstance().getPostInformation(uniqueId);

                broadcastNfcDataIfWithinThirtyMeters(context, postInformation);

                if (Util.isNetworkConnected() && postInformation != null) {
                    if (!checkNfcAddStatusOnline(context, postInformation, uniqueId)) {
                        Toast.makeText(context, R.string.nfc_already_added, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else if (postInformation != null) {
                    if (postInformation.getId() != null) {
                        Toast.makeText(context, R.string.nfc_already_configured, Toast.LENGTH_SHORT).show();
                    }
                    return;
                }*/
            }
            //setupNfcDataAndSync(context, uniqueId);

        } else {
            Timber.d(TAG, context.getString(R.string.nfc_no_unique_id_error));
        }
    }

    /*private void broadcastNfcDataIfWithinThirtyMeters(Context context, UnitPost postInformation) {
        if (postInformation != null) {
            String[] coOrdinates = postInformation.getGeoPoint().split(",");
            if (coOrdinates.length == 2) {
                LatLng latLng = new LatLng(Double.valueOf(coOrdinates[0]),
                        Double.valueOf(coOrdinates[1]));
                Timber.d("NFC Unit information Name: %s Post Id : %s LatLng : %s",
                        postInformation.getUnitPostName(), postInformation.getUnitId(), latLng);
                if (isWithinThirtyMeters(latLng)) {
                    nfcLatLngWithinTenMeters(context, postInformation);
                } else {
                    Toast.makeText(context, String.format(context.getString(R.string.nfc_not_matched),
                            postInformation.getUnitPostName()), Toast.LENGTH_LONG).show();
                }
            }
        }
    }*/

    /*private void nfcLatLngWithinTenMeters(Context context, UnitPost postInformation) {
        Intent checkpointIntent = new Intent(Constants.Nfc.CHECKPOINT_SCAN);
        checkpointIntent.putExtra(TableNameAndColumnStatement.UNIT_POST_NAME,
                postInformation.getUnitPostName());
        checkpointIntent.putExtra(TableNameAndColumnStatement.UNIT_POST_ID,
                postInformation.getUnitId());
        context.sendBroadcast(checkpointIntent);
    }

    private void setupNfcDataAndSync(Context context, String uniqueId) {
        // Server call to save unique id and get post id in return
        Intent nfcIntent = new Intent(Constants.Nfc.NFC_UNIQUE_SCAN);
        nfcIntent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, uniqueId);
        Timber.d(TAG, "NFC Unique id : %s", uniqueId);
        context.sendBroadcast(nfcIntent);
    }

    private boolean checkNfcAddStatusOnline(final Context context, UnitPost postInformation, final String uniqueId) {
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.Nfc.UNIT_ID, postInformation.getId());
        map.put(Constants.Nfc.DEVICE_NO, uniqueId);

        checkNfcAddStatusViaApi(context, map);
        return nfcAddAllowed;
    }*/

    /*private void checkNfcAddStatusViaApi(Context context, Map<String, Object> map) {
        new SISClient(context).getApi().checkNfcStatus(map, new Callback<NfcCheckResponse>() {
            @Override
            public void success(NfcCheckResponse nfcCheckResponse, Response response) {
                Timber.d("NFC add status check : %s", nfcCheckResponse);
                nfcAddAllowed = nfcCheckResponse.nfcData.allowToAdd;
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error, "Error while getting add nfc status");
            }
        });
    }*/

    /*private boolean isWithinThirtyMeters(LatLng latLng) {
        float[] results = new float[1];
        Location.distanceBetween(GpsTrackingService.latitude, GpsTrackingService.longitude,
                latLng.latitude, latLng.longitude, results);
        float distanceInMeters = results[0];
        return (distanceInMeters < 10);
    }*/

    private void setupNfcDataAndSync(Context context, String uniqueId) {
        // Server call to save unique id and get post id in return
        Intent nfcIntent = new Intent(Constants.Nfc.NFC_UNIQUE_SCAN);
        nfcIntent.putExtra(Constants.Nfc.NFC_UNIQUE_ID, uniqueId);
        Timber.d(TAG, "NFC Unique id : %s", uniqueId);
        context.sendBroadcast(nfcIntent);
    }
}