package com.sisindia.ai.android.rota.daycheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.AddKitRequestActivity;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.CommonTags;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.IdCardMO;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.LastAkrDetails;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.grievances.AddIssuesGrievanceActivity;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.response.UnitEmployee;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;
import com.sisindia.ai.android.rota.models.GuardFineRewardMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity.BARCODE_CAPTURE_REQUEST_CODE;

public class GuardDetailsActivity extends DayCheckBaseActivity implements View.OnClickListener, DialogClickListener {

    private static int MAX_FINE = 200;
    private static int MAX_REWARD = 100;
    protected String SATISFACTORY = "Satisfactory";
    protected String IMPRESSIVE = "Impressive";
    protected String POORIMAGE = "Poor Image";
    private AppPreferences appPreference;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.employee_id_number)
    AutoCompleteTextView autoCompleteTextview;
    @Bind(R.id.editv_guard_id)
    CustomFontEditText guardName;
    @Bind(R.id.last_akr_issued)
    CustomFontTextview lastAkrIssued;
    @Bind(R.id.next_akr_date)
    CustomFontTextview nextAkrDate;
    @Bind(R.id.last_fine_amount)
    CustomFontTextview lastFine;
    @Bind(R.id.guard_check_qrScan)
    RadioGroup qrScanRadioGroup;
    @Bind(R.id.guard_at_post)
    RadioGroup guardAtPost;
    @Bind(R.id.guard_proper_shave)
    RadioGroup guardProperShave;
    @Bind(R.id.guard_haircut)
    RadioGroup guardHaircut;
    @Bind(R.id.guard_shoe_polish)
    RadioGroup guardShoePolish;
    @Bind(R.id.guard_uniform)
    RadioGroup guardUniform;
    @Bind(R.id.guard_clean_uniform)
    RadioGroup guardCleanUniform;
    @Bind(R.id.guard_fitting_uniform)
    RadioGroup guardFittingUniform;
    @Bind(R.id.guard_well_pressed_uniform)
    RadioGroup guardWellPressedUniform;
    @Bind(R.id.guard_ic_card)
    RadioGroup guardIcCard;
    @Bind(R.id.kit_request)
    CustomFontButton kitRequest;
    @Bind(R.id.over_all_turnout)
    Spinner mTurnOutSpinner;
    @Bind(R.id.knowledge_about_duty)
    Spinner mDutyKnowledge;
    @Bind(R.id.duty_performance)
    Spinner mduty_performance;
    @Bind(R.id.add_signature_guard_level)
    ImageView madd_signature_guard_level;
    @Bind(R.id.guard_add_grievance)
    CustomFontButton mguard_add_grievance;
    @Bind(R.id.guard_parent_layout)
    LinearLayout mguard_parent_layout;
    @Bind(R.id.guardFullLengthImage)
    ImageView guardFullLengthImage;
    @Bind(R.id.overAllTurnoutImage)
    ImageView overAllTurnoutImage;
    @Bind(R.id.impressive_title)
    CustomFontTextview impressiveTitle;
    @Bind(R.id.impressive_edittext)
    CustomFontEditText impressiveEdittext;
    @Bind(R.id.impressive_Layout)
    LinearLayout impressiveLayout;
    @Bind(R.id.poor_image_title)
    CustomFontTextview poorImageTitle;
    @Bind(R.id.poor_image_edittext)
    CustomFontEditText poorImageEdittext;
    @Bind(R.id.poor_image_layout)
    LinearLayout poorImageLayout;

    @Bind(R.id.qrScanCommentBox)
    CustomFontEditText qrScanCommentBox;

    /*@Bind(R.id.qrScanCommentBoxLabel)
    CustomFontTextview qrScanCommentBoxLabel;*/

    @Bind(R.id.qrScanStatusLayout)
    LinearLayout qrScanStatusLayout;

    @Bind(R.id.qrScanYesRadioButton)
    RadioButton qrScanYesRadioButton;
    @Bind(R.id.qrScanNoRadioButton)
    RadioButton qrScanNoRadioButton;

    private boolean isScannedFromQR = false;
    private boolean isQRScanWorking = false;

    ArrayList<RadioGroup> mRadioGroupList;
    GuardDetail mGuardDetail;
    private ArrayList<UnitEmployee> arrayListEmployeeIds;
    private UnitEmployee unitEmployeeDetails;
    private UnitEmployee updatedUnitEmployee;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private String employeeNo = "";
    private int totalFine = 0;
    private int totalReward = 0;
    private int acceptableFineAmount;
    private int acceptableRewardAmount;
    private String turnOutLabel = "";
    private Resources resources;
    private Map<String, Boolean> checkedGuardDetailsMap = null;
    private CheckedGuardDetails checkedGuardDetails;
    private String guardFullImagePath;
    private boolean isGrievanceAdded = false;

    /*private boolean hasFineNorReward;
    private int spinnerItemPos = -1;
    private int fineAmountFromServer = 0;
    private RelativeLayout relativeLayout;
    private TextView employeeIdTxt;
    private boolean toBeAllowedFineValidation = true;
    private boolean tobeAllowedRewardValidation = true;
    private String IMAGE_SCREEN_NAME = "Guard_Detail";*/

    /**
     * Radio Group checked Change listener
     */
    private RadioGroup.OnCheckedChangeListener customCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (group.getId()) {
                case R.id.guard_check_qrScan:
                    if (qrScanYesRadioButton.isChecked()) {
//                        qrScanCommentBox.setVisibility(View.GONE);
//                        qrScanCommentBoxLabel.setVisibility(View.GONE);
                        qrScanCommentBox.setText("");
                        isQRScanWorking = true;
                    } else if (qrScanNoRadioButton.isChecked()) {
//                        qrScanCommentBox.setVisibility(View.VISIBLE);
//                        qrScanCommentBoxLabel.setVisibility(View.VISIBLE);
                        isQRScanWorking = false;
                    }
                    break;
                case R.id.guard_at_post:
                    mGuardDetail.setAvailableAtPost(setSelectedValue(guardAtPost.getCheckedRadioButtonId(), guardAtPost));
                    break;
                case R.id.guard_proper_shave:
                    mGuardDetail.setProperShave(setSelectedValue(guardProperShave.getCheckedRadioButtonId(), guardProperShave));
                    break;
                case R.id.guard_haircut:
                    mGuardDetail.setHairCut(setSelectedValue(guardHaircut.getCheckedRadioButtonId(), guardHaircut));
                    break;
                case R.id.guard_shoe_polish:
                    mGuardDetail.setShoePolish(setSelectedValue(guardShoePolish.getCheckedRadioButtonId(), guardShoePolish));
                    break;
                case R.id.guard_uniform:
                    mGuardDetail.setFullUniform(setSelectedValue(guardUniform.getCheckedRadioButtonId(), guardUniform));
                    break;
                case R.id.guard_clean_uniform:
                    mGuardDetail.setCleanUniform(setSelectedValue(guardCleanUniform.getCheckedRadioButtonId(), guardCleanUniform));
                    break;
                case R.id.guard_fitting_uniform:
                    mGuardDetail.setProperFittingUniform(setSelectedValue(guardFittingUniform.getCheckedRadioButtonId(), guardFittingUniform));
                    break;
                case R.id.guard_well_pressed_uniform:
                    mGuardDetail.setWellPressedUniform(setSelectedValue(guardWellPressedUniform.getCheckedRadioButtonId(), guardWellPressedUniform));
                    break;
                case R.id.guard_ic_card:
                    mGuardDetail.setIDCard(setSelectedValue(guardIcCard.getCheckedRadioButtonId(), guardIcCard));
                    break;
            }
        }
    };
    private LastAkrDetails lastAkrDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guard_details);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mRadioGroupList = new ArrayList<>();
        mGuardDetail = new GuardDetail();
        resources = getResources();
        setBaseToolbar(toolbar, resources.getString(R.string.guard_check));
        appPreference = new AppPreferences(this);
        createRadioGroupArray();
        setTurnOutSpinnerData(mTurnOutSpinner);
        setDutyKnowledgeSpinnerData(mDutyKnowledge);
        setDutyPerformanceSpinnerData(mduty_performance);
        madd_signature_guard_level.setOnClickListener(this);
        mguard_add_grievance.setOnClickListener(this);
        guardFullLengthImage.setOnClickListener(this);
        overAllTurnoutImage.setOnClickListener(this);
        kitRequest.setOnClickListener(this);
        updatedUnitEmployee = new UnitEmployee();

//        autoCompleteEmployeeInfo();

        try {

            String[] receivedGuardCodes = IOPSApplication.getInstance().getSelectionStatementDBInstance().getGuardsCode();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, receivedGuardCodes);
            autoCompleteTextview.setAdapter(adapter);
            autoCompleteTextview.setThreshold(2);

            autoCompleteTextview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                    try {
                        String selectedGuardCode = autoCompleteTextview.getText().toString();
                        updateEmployeeInfo(selectedGuardCode.trim());
                    } catch (Exception e) {
                    }
                }
            });

            autoCompleteTextview.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    guardName.setText("");
                    qrScanStatusLayout.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() == 9) {
                        try {
                            arrayListEmployeeIds = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAllEmployeeDetails(s.toString().trim());
                            if (null != arrayListEmployeeIds && arrayListEmployeeIds.size() != 0) {
                                unitEmployeeDetails = arrayListEmployeeIds.get(0);
                                guardDetailsUIUpdate();
                                autoCompleteTextview.dismissDropDown();
                            } else {
                                //call API
                                guardRewardFineAndAKRDetails(s.toString().trim());
                            }
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }
                    }
                }
            });

        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void guardDetailsUIUpdate() {

        try {
            if (unitEmployeeDetails == null) {
                mGuardDetail.setGuardName(autoCompleteTextview.getText().toString().trim());
                mGuardDetail.setEmployeeId(0);
                mGuardDetail.setEmployeeNo(autoCompleteTextview.getText().toString().trim());
                employeeNo = autoCompleteTextview.getText().toString().trim();
                Util.setmGuard_Name(autoCompleteTextview.getText().toString().trim());
                Util.mGuard_Id = 0;
                Util.setGuardCode(autoCompleteTextview.getText().toString().trim());

                guardName.setText(resources.getString(R.string.NOT_AVAILABLE));
                lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + resources.getString(R.string.NOT_AVAILABLE));
                nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + resources.getString(R.string.NOT_AVAILABLE));
                lastFine.setText(resources.getString(R.string.last_fine) + ": " + resources.getString(R.string.NOT_AVAILABLE));
            } else {
                guardName.setText(unitEmployeeDetails.getEmployeeFullName());
                mGuardDetail.setGuardName(unitEmployeeDetails.getEmployeeFullName());
                mGuardDetail.setEmployeeId(unitEmployeeDetails.getEmployeeId());
                mGuardDetail.setEmployeeNo(unitEmployeeDetails.getEmployeeNo());
                employeeNo = autoCompleteTextview.getText().toString().trim();
                Util.setmGuard_Name(mGuardDetail.getGuardName());
                Util.mGuard_Id = mGuardDetail.getEmployeeId();
                Util.setGuardCode(mGuardDetail.getEmployeeNo());
            }

            showScanQRLayout();

        } catch (Exception e) {
        }
    }

    private void showScanQRLayout() {
        if (!isScannedFromQR) {
            isScannedFromQR = false;
            qrScanStatusLayout.setVisibility(View.VISIBLE);
            qrScanCommentBox.setText("");
            qrScanNoRadioButton.setChecked(true);
        }
    }

    private void setEmployeeDetails(String fullName, int employeeId, String employeeNum) {
        guardName.setText(fullName);
        Util.setmGuard_Name(fullName);
        Util.mGuard_Id = employeeId;
        Util.setGuardCode(autoCompleteTextview.getText().toString().trim());
        mGuardDetail.setEmployeeId(employeeId);
        mGuardDetail.setEmployeeNo(employeeNum);
        employeeNo = employeeNum;
        if (checkGuardDetails()) {
            autoCompleteTextview.setText("");
            snackBarWithMesg(autoCompleteTextview, resources.getString(R.string.DUPLICATE_GUARD_CHECK_MSG));
        } else {
            saveCheckGuardDetails(false);
        }
    }

    private boolean checkGuardDetails() {
        boolean isChecked = false;
        checkedGuardDetails = appPreference.getCheckedGuardDetails();
        if (checkedGuardDetails != null) {
            checkedGuardDetailsMap = checkedGuardDetails.checkedGuardDetailsMap;
        }

        if (checkedGuardDetailsMap != null && checkedGuardDetailsMap.size() != 0) {
            try {
                if (!TextUtils.isEmpty(employeeNo))
                    isChecked = checkedGuardDetailsMap.get(employeeNo.toUpperCase());
            } catch (NullPointerException npe) {
                isChecked = false;
            }
        }
        return isChecked;
    }

    private void saveCheckGuardDetails(boolean isChecked) {
        if (checkedGuardDetails == null) {
            checkedGuardDetails = new CheckedGuardDetails();
            checkedGuardDetailsMap = new HashMap<>();
        }
        if (!TextUtils.isEmpty(employeeNo)) {
            if (checkedGuardDetailsMap == null) {
                checkedGuardDetailsMap = new HashMap<>();
                checkedGuardDetailsMap.put(employeeNo.toUpperCase(), isChecked);
            } else {
                checkedGuardDetailsMap.put(employeeNo.toUpperCase(), isChecked);
            }
        }
        checkedGuardDetails.checkedGuardDetailsMap = checkedGuardDetailsMap;
        appPreference.setCheckedGuardDetails(checkedGuardDetails);
    }

    private void updateEmployeeInfo(String guardCode) {

        List<UnitEmployee> employees = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAllEmployeeDetails(guardCode);

        if (null != employees && employees.size() != 0) {
            unitEmployeeDetails = employees.get(0);
            setEmployeeDetails(unitEmployeeDetails.getEmployeeFullName(), unitEmployeeDetails.getEmployeeId(), unitEmployeeDetails.getEmployeeNo());
            lastAkrDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLastAkrDetails(rotaTaskModel.getUnitId(),
                    unitEmployeeDetails.getEmployeeId());
            setLastAkrDetails();
        } else {
            guardDetailsUIUpdate();
        }
    }

    public void setLastAkrDetails() {
        if (lastAkrDetails != null) {
            if (lastAkrDetails.getLastAkrIssueDate() != null)
                lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + lastAkrDetails.getLastAkrIssueDate());
            else
                lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + resources.getString(R.string.NOT_AVAILABLE));

            if (lastAkrDetails.getLastAkrDue() != null)
                nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + lastAkrDetails.getLastAkrDue());
            else
                nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + resources.getString(R.string.NOT_AVAILABLE));
        }

        if (unitEmployeeDetails != null)
            lastFine.setText(resources.getString(R.string.last_fine) + ": " + unitEmployeeDetails.getLastFine());
    }

    //@Ashu : Calling below API to get guard related information
    private void guardRewardFineAndAKRDetails(String emp_no) {
        SISClient sisClient = new SISClient(GuardDetailsActivity.this, "getGuardRewardFine", 10);
        sisClient.getApi().getGuardRewardFine(emp_no, new Callback<GuardFineRewardMO>() {
            @Override
            public void success(GuardFineRewardMO guardFineRewardMO, Response response) {

                if (null != guardFineRewardMO) {
//                    mGuardFineRewardMO = guardFineRewardMO;
                    switch (guardFineRewardMO.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            hideProgressbar();
                            if (null != guardFineRewardMO.getData()) {
                                try {
//                                fromAPICall = true;
                                    //updateEmployeeInfo(String.valueOf(guardFineRewardMO.getData().getEmployeeId()));
                                    if (guardFineRewardMO.getData().getLastAKRIssue() != null && !TextUtils.isEmpty(guardFineRewardMO.getData().getLastAKRIssue()))
                                        lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + guardFineRewardMO.getData().getLastAKRIssue());
                                    else
                                        lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + resources.getString(R.string.NOT_AVAILABLE));

                                    if (guardFineRewardMO.getData().getLastAKRDue() != null && !TextUtils.isEmpty(guardFineRewardMO.getData().getLastAKRDue()))
                                        nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + guardFineRewardMO.getData().getLastAKRDue());
                                    else
                                        nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + resources.getString(R.string.NOT_AVAILABLE));

                                    lastFine.setText(resources.getString(R.string.last_fine) + ": " + Integer.toString(guardFineRewardMO.getData().getLastFine()));

                                    UnitEmployee unitEmployee = new UnitEmployee();
                                    unitEmployee.setEmployeeId(guardFineRewardMO.getData().getEmployeeId());

                                    if (!guardFineRewardMO.getData().getEmployeeFullName().trim().isEmpty())
                                        unitEmployee.setEmployeeFullName(guardFineRewardMO.getData().getEmployeeFullName());
                                    else
                                        unitEmployee.setEmployeeFullName("NA");

                                    unitEmployee.setEmployeeNo(guardFineRewardMO.getData().getEmployeeCode());
                                    unitEmployee.setLastFine(Integer.toString(guardFineRewardMO.getData().getLastFine()));
                                    unitEmployee.setFineAmount(Integer.toString(guardFineRewardMO.getData().getLastFine()));
                                    unitEmployee.setRewardAmount(Integer.toString(guardFineRewardMO.getData().getReward()));

                                    unitEmployeeDetails = unitEmployee;
                                    guardDetailsUIUpdate();
                                } catch (Exception e) {
                                    Crashlytics.logException(e);
                                }
                            } else {
                                unitEmployeeDetails = null;
                                guardDetailsUIUpdate();
                            }
                            break;

                        default:
                            hideProgressbar();
//                            fromAPICall = false;
                            Util.showToast(GuardDetailsActivity.this, CommonTags.API_ERROR_MESG);
                            showScanQRLayout();
                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressbar();
//                fromAPICall = false;
                Crashlytics.logException(error);
                showScanQRLayout();
                Util.showToast(GuardDetailsActivity.this, CommonTags.API_ERROR_MESG);
            }
        });
    }

    /**
     * Method to set Performance spinner data
     *
     * @param mduty_performance
     */
    private void setDutyPerformanceSpinnerData(final Spinner mduty_performance) {
        ArrayList<LookupModel> lookupModelArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(resources.getString(R.string.PerformingDuty), null);
        String[] performance = new String[lookupModelArrayList.size()];
        for (int index = 0; index < lookupModelArrayList.size(); index++) {
            performance[index] = lookupModelArrayList.get(index).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, performance);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mduty_performance.setSelection(0);
        mduty_performance.setAdapter(adapter);
        mduty_performance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = mduty_performance.getSelectedItem().toString();
                mGuardDetail.setPerformingDutyAtPostSince(text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_day_check, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.daycheck_done:
                validateFields(mGuardDetail);
                break;
            case android.R.id.home:
                showConfirmationDialog((resources.getInteger(R.integer.CONFIRAMTION_DIALOG)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((resources.getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    /**
     * Method to check that all the mandatory fields are checked.
     *
     * @param mGuardDetail
     */
    private void validateFields(GuardDetail mGuardDetail) {

        if (autoCompleteTextview.getText().length() == 0) {
            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.guard_code_validation));
            return;
        } else if (!isScannedFromQR && !isQRScanWorking && (qrScanCommentBox.getText().toString().isEmpty() || qrScanCommentBox.getText().toString().trim().isEmpty())) {
            qrScanCommentBox.requestFocus();
            snackBarWithMesg(mguard_parent_layout, "Please enter valid remarks, if QR is not working");
            return;
        } else if (mGuardDetail.getAvailableAtPost().equalsIgnoreCase(resources.getString(R.string.yes)) && !checkForGuardImageAtPost()) {
            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.full_image_captured_validation));
            return;
        } else {
            if (mGuardDetail.getAvailableAtPost().equalsIgnoreCase(resources.getString(R.string.no))) {
               /* removeImageFromGuardFullImage();
                removeImageFromGuardTurnOut();
                removeSignature();*/
                // updateToModelData();
                switch (turnOutLabel) {
                    case "Impressive":
                        impressiveLayout.setVisibility(View.VISIBLE);
                        poorImageLayout.setVisibility(View.GONE);
                        poorImageTitle.setText("Reward Amount");
                        if (impressiveEdittext.getText().toString().trim().equalsIgnoreCase("0")) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MINIMUM_REWARD_AMOUNT_VALIDATION));
                            return;
                        } else {
                            setTurnOutValues(turnOutLabel, impressiveEdittext.getText().toString().trim());
                            if (unitEmployeeDetails != null) {
                                updatedUnitEmployee.setRewardAmount(unitEmployeeDetails.getRewardAmount() + mGuardDetail.getAmount());
                                validateRewardAmount(turnOutLabel, impressiveEdittext.getText().toString().trim(), false);
                            } else {
                                updateToModelData();
                            }
                        }
                        break;
                    case "Poor Image":
                        impressiveLayout.setVisibility(View.GONE);
                        poorImageLayout.setVisibility(View.VISIBLE);
                        poorImageTitle.setText("Fine Amount*");
                        if (TextUtils.isEmpty(poorImageEdittext.getText().toString().trim())) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MINIMUM_FINE_AMOUNT_VALIDATION));
                            return;
                        } else {
                            setTurnOutValues(turnOutLabel, poorImageEdittext.getText().toString().trim());
                            updatedUnitEmployee.setFineAmount(unitEmployeeDetails.getFineAmount() + mGuardDetail.getAmount());
                            if (unitEmployeeDetails != null) {
                                updatedUnitEmployee.setLastFine(mGuardDetail.getAmount());
                                validateFineAmount(turnOutLabel, poorImageEdittext.getText().toString().trim(), false);
                            } else {
                                updateToModelData();
                            }
                        }
                        break;
                    case "Satisfactory":
                        updateToModelData();
                        break;
                }
            } else {
                switch (turnOutLabel) {
                    case "Impressive":
                        impressiveLayout.setVisibility(View.VISIBLE);
                        poorImageLayout.setVisibility(View.GONE);
                        poorImageTitle.setText("Reward Amount");
                        if (impressiveEdittext.getText().toString().trim().equalsIgnoreCase("0")) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MINIMUM_REWARD_AMOUNT_VALIDATION));
                            return;
                        } else if (!checkImageCapturedForTurnOut()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_image_captured_validation));
                            return;
                        } else if (!checkSignatureCaptured()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_signature_validation));
                            return;
                        } else {
                            setTurnOutValues(turnOutLabel, impressiveEdittext.getText().toString().trim());
                            validateRewardAmount(turnOutLabel, impressiveEdittext.getText().toString().trim(), false);
                        }
                        break;
                    case "Poor Image":
                        impressiveLayout.setVisibility(View.GONE);
                        poorImageLayout.setVisibility(View.VISIBLE);
                        poorImageTitle.setText("Fine Amount*");
                        if (TextUtils.isEmpty(poorImageEdittext.getText().toString().trim())) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MINIMUM_FINE_AMOUNT_VALIDATION));
                            return;
                        } else if (!checkImageCapturedForTurnOut()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_image_captured_validation));
                            return;
                        } else if (!checkSignatureCaptured()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_signature_validation));
                            return;
                        } else {
                            setTurnOutValues(turnOutLabel, poorImageEdittext.getText().toString().trim());
                            validateFineAmount(turnOutLabel, poorImageEdittext.getText().toString().trim(), false);
                        }
                        break;
                    case "Satisfactory":
                        if (!checkImageCapturedForTurnOut()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_image_captured_validation));
                            return;
                        } else if (!checkSignatureCaptured()) {
                            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.turnout_signature_validation));
                            return;
                        } else {
                            updateToModelData();
                        }
                        break;
                }
            }
        }
    }

    private synchronized void removeImageAttachmentModel(AttachmentMetaDataModel metaDataModel) {
        List<AttachmentMetaDataModel> attachmentMetaArrayList = getAttachmentMetaArrayList();

        if (null != metaDataModel) {
            if (metaDataModel.getAttachmentSourceCode().equalsIgnoreCase(autoCompleteTextview.getText().toString().trim()) &&
                    metaDataModel.getimageCode().equalsIgnoreCase(CommonTags.GUARD_SIGNATURE)) {
                attachmentMetaArrayList.remove(metaDataModel);
            } else {
                attachmentMetaArrayList.remove(metaDataModel);
            }
        }
    }

    private boolean checkForGuardImageAtPost() {
        boolean isPicTaken;
        if ((guardFullLengthImage.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_guardphoto).getConstantState()))
            isPicTaken = false;
        else
            isPicTaken = true;
        return isPicTaken;
    }

    private void updateToModelData() {
        if (guardName.getText().length() != 0 && !guardName.getText().toString().trim().equalsIgnoreCase(resources.getString(R.string.NOT_AVAILABLE)))
            mGuardDetail.setGuardName(guardName.getText().toString());
        else
            mGuardDetail.setGuardName(autoCompleteTextview.getText().toString().trim());

        if (null != guardFullImagePath)
            mGuardDetail.setmGuardImageUri(guardFullImagePath);

        if (checkGuardDetails()) {
            autoCompleteTextview.setText("");
            snackBarWithMesg(autoCompleteTextview, resources.getString(R.string.DUPLICATE_GUARD_CHECK_MSG));
        } else {
            // CHECKING WHETHER AI HAVE RAISED OR ADD THE GRIEVANCE
            if (appPreference.getForcedGrievanceFlag() && (rotaTaskModel != null && rotaTaskModel.getTaskTypeId() != 2) && !isGrievanceAdded) {
                snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.guardGrievanceValidation));
            } else {
                
                /*@Ashu : Adding QR scanning result*/
                try {
                    mGuardDetail.setQRScanComments(qrScanCommentBox.getText().toString());
                    mGuardDetail.setIsQRScanWorking(isQRScanWorking ? 1 : 0);
                    mGuardDetail.setIsScannedFromQR(isScannedFromQR ? 1 : 0);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }

                mGuardDetail.setEmployeeNo(autoCompleteTextview.getText().toString());
                Intent guardDetailsIntent = new Intent();
                String geoLocation = GpsTrackingService.latitude + " ," + GpsTrackingService.longitude;
                mGuardDetail.setGeoLocation(geoLocation);
                saveCheckGuardDetails(true);
                guardDetailsIntent.putExtra(Constants.GUARD_DETAILS, mGuardDetail);
                setResult(Constants.NUMBER_ONE, guardDetailsIntent);
                finish();
            }
        }
    }

    /**
     * Method to set Knowledge spinner data
     *
     * @param mDutyKnowledge
     */
    private void setDutyKnowledgeSpinnerData(final Spinner mDutyKnowledge) {
        ArrayList<LookupModel> lookupModelArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(resources.getString(R.string.KnowledgeAboutDuty), null);
        String[] mdutyKnowledge = new String[lookupModelArrayList.size()];
        for (int index = 0; index < lookupModelArrayList.size(); index++) {
            mdutyKnowledge[index] = lookupModelArrayList.get(index).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mdutyKnowledge);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDutyKnowledge.setSelection(0);
        mDutyKnowledge.setAdapter(adapter);
        mDutyKnowledge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = mDutyKnowledge.getSelectedItem().toString();
                mGuardDetail.setKnowledgeAboutDutyPost(text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * @param mTurnOutSpinner
     */
    private void setTurnOutSpinnerData(final Spinner mTurnOutSpinner) {
        ArrayList<LookupModel> lookupModelArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(resources.getString(R.string.OverallTurnout), null);
        String[] planets = new String[lookupModelArrayList.size()];
        for (int index = 0; index < lookupModelArrayList.size(); index++) {
            planets[index] = lookupModelArrayList.get(index).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, planets);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTurnOutSpinner.setSelection(0);
        mTurnOutSpinner.setAdapter(adapter);
        mTurnOutSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                spinnerItemPos = position;
                showTourOutViews(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                turnOutLabel = IMPRESSIVE;
            }
        });
    }

    /**
     * Method which will return the selected value from the drop down in the turnout spinner.
     */
    private void showTourOutViews(int pos) {

        impressiveLayout.setVisibility(View.GONE);
        poorImageLayout.setVisibility(View.GONE);
        switch (pos) {
            case 0:
                turnOutLabel = IMPRESSIVE;
//                hasFineNorReward = false;
                impressiveLayout.setVisibility(View.VISIBLE);
                impressiveTitle.setText(resources.getString(R.string.reward_amount_value));
                impressiveEdittext.setHint(resources.getString(R.string.ENTER_AMOUNT));
                autoCompleteTextForAmount(turnOutLabel, impressiveEdittext);
                break;
            case 2:
                turnOutLabel = POORIMAGE;
//                hasFineNorReward = true;
                poorImageLayout.setVisibility(View.VISIBLE);
                poorImageTitle.setText(resources.getString(R.string.fine_amount));
                poorImageEdittext.setHint(resources.getString(R.string.ENTER_AMOUNT));
                autoCompleteTextForAmount(turnOutLabel, poorImageEdittext);
                break;
            case 1:
                poorImageLayout.setGravity(View.GONE);
                impressiveLayout.setGravity(View.GONE);
                turnOutLabel = SATISFACTORY;
//                hasFineNorReward = false;
                setTurnOutValues(turnOutLabel, " ");
                break;
        }
    }

    private void autoCompleteTextForAmount(final String turnOutLabel, final CustomFontEditText mturnout_edittext) {
        mturnout_edittext.setVisibility(View.VISIBLE);
       /* if (mGuardDetail.getAvailableAtPost().equalsIgnoreCase(resources.getString(R.string.no))) {
            if (turnOutLabel.equalsIgnoreCase(IMPRESSIVE)) {
                disableAutoEditText(mturnout_edittext);
            } else if (turnOutLabel.equalsIgnoreCase(POORIMAGE)) {
                disableAutoEditText(mturnout_edittext);
            } else {
                mturnout_edittext.setVisibility(View.GONE);
            }
        } else {*/
        enableAutoEditText(mturnout_edittext);
        mturnout_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String Amount = s.toString().trim();
                if (turnOutLabel.equalsIgnoreCase(POORIMAGE)) {
                    if (Amount.equalsIgnoreCase("0")) {
                        snackBarWithMesg(mguard_parent_layout, getString(R.string.MINIMUM_FINE_AMOUNT_VALIDATION));
                        mturnout_edittext.setText("");
                    } else if (!TextUtils.isEmpty(Amount) && !checkFineAmountValidation(Amount)) {
                        snackBarWithMesg(mguard_parent_layout, getString(R.string.MAXIMUM_FINE_AMOUNT_VALIDATION));
                    }
                } else {
                    if (Amount.equalsIgnoreCase("0")) {
                        snackBarWithMesg(mguard_parent_layout, getString(R.string.MINIMUM_REWARD_AMOUNT_VALIDATION));
                        mturnout_edittext.setText("");
                    } else if (!TextUtils.isEmpty(Amount) && !checkRewardAmountValidation(Amount)) {
                        snackBarWithMesg(mguard_parent_layout, getString(R.string.MAXIMUM_REWARD_AMOUNT_VALIDATION));
                    }
                }
            }
        });
        // }
    }

    private boolean checkRewardAmountValidation(String amount) {
        return Integer.valueOf(amount) <= MAX_REWARD;
    }

    private boolean checkFineAmountValidation(String amount) {
        return Integer.valueOf(amount) <= MAX_FINE;
    }

    private void enableAutoEditText(CustomFontEditText editText) {
        editText.setHint(getResources().getString(R.string.ENTER_AMOUNT));
        editText.setText("");
        editText.setClickable(true);
        editText.setFocusableInTouchMode(true);
        editText.setFocusable(true);
        editText.setEnabled(true);
        editText.setCursorVisible(true);
        editText.setKeyListener(DigitsKeyListener.getInstance());
    }

    private void disableAutoEditText(CustomFontEditText editText) {
        editText.setHint(getResources().getString(R.string.ENTER_AMOUNT));
        editText.setText("");
        editText.setClickable(false);
        editText.setFocusableInTouchMode(false);
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
    }

    private void validateFineAmount(String turnOutValue, String fineAmount, boolean isFromSpinnerSelection) {
        int turnOutFineValue = 0;
        int punishmentValue = 0;
        if (!TextUtils.isEmpty(fineAmount)) {
            turnOutFineValue = Integer.parseInt(fineAmount);
        }
        if (null != unitEmployeeDetails) {
            punishmentValue = Integer.parseInt(unitEmployeeDetails.getFineAmount());
            totalFine = turnOutFineValue + punishmentValue;
        } else {
            totalFine = turnOutFineValue;
        }
        if (totalFine == 0 || totalFine <= 0) {
            snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MINIMUM_FINE_AMOUNT_VALIDATION));
//            toBeAllowedFineValidation = false;
        } else if (totalFine <= MAX_FINE && totalFine >= 1) {
            setTurnOutValues(turnOutValue, String.valueOf(turnOutFineValue));
//            toBeAllowedFineValidation = true;
            updateToModelData();
        } else {
            acceptableFineAmount = MAX_FINE - punishmentValue;
            if (acceptableFineAmount == 0 || acceptableFineAmount < 0) {
                showFineDialog("Fine Amount Validation", turnOutValue);
            } else {
                snackBarWithMesg(mguard_parent_layout, "Fine amount should be " + acceptableFineAmount + " rupees or less");
//                toBeAllowedFineValidation = false;
            }
        }
    }

    private void showFineDialog(String mesg, final String turnOutValue) {
        new AlertDialog.Builder(this)
                .setTitle(mesg)
                .setMessage(resources.getString(R.string.MAXIMUM_FINE_EXIT_AMOUNT_VALIDATION))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                        toBeAllowedFineValidation = true;
                        poorImageEdittext.setText("0");
                        setTurnOutValues(turnOutValue, poorImageEdittext.getText().toString().trim());
                        updateToModelData();
                        // snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.MAXIMUM_FINE_EXIT_AMOUNT_VALIDATION)); System.exit(0);
                    }
                })
                .show();
    }

    private void validateRewardAmount(String turnOutValue, String rewardAmount, boolean isFromSpinnerSelection) {
        int turnOutRewardValue = 0;
        int rewardValue = 0;
        if (!TextUtils.isEmpty(rewardAmount)) {
            turnOutRewardValue = Integer.parseInt(rewardAmount);
        }
        if (null != unitEmployeeDetails) {
            rewardValue = Integer.parseInt(unitEmployeeDetails.getRewardAmount());
            totalReward = turnOutRewardValue + rewardValue;
        } else {
            totalReward = turnOutRewardValue;
        }
        if (totalReward == 0 || totalReward <= 0) {
//            tobeAllowedRewardValidation = true;
            updateToModelData();
        } else if (totalReward <= MAX_REWARD && totalReward >= 1) {
            setTurnOutValues(turnOutValue, String.valueOf(turnOutRewardValue));
//            tobeAllowedRewardValidation = true;
            updateToModelData();
        } else {
            acceptableRewardAmount = MAX_REWARD - rewardValue;
            if (acceptableRewardAmount == 0) {
                showRewardDialog("Reward Amount Validation", turnOutValue);
            } else {
//                tobeAllowedRewardValidation = false;
                snackBarWithMesg(mguard_parent_layout, "Reward amount should be " + acceptableRewardAmount + " rupees or less");
            }
        }
    }

    private void showRewardDialog(String mesg, final String turnOutValue) {

        new AlertDialog.Builder(this)
                .setTitle(mesg)
                .setMessage(resources.getString(R.string.MAXIMUM_REWARD_EXIT_AMOUNT_VALIDATION))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
//                        tobeAllowedRewardValidation = true;
                        impressiveEdittext.setText("0");
                        setTurnOutValues(turnOutValue, impressiveEdittext.getText().toString().trim());
                        updateToModelData();

                    }
                })
                .show();
    }

    private void setTurnOutValues(String title, String editTextValue) {
        mGuardDetail.setOverAllTurnout(title);
        mGuardDetail.setAmount(editTextValue);
    }

    /**
     * Method which creates the radioGroup with radioButton dynamically.
     */
    private void createRadioGroupArray() {

        mRadioGroupList.add(guardAtPost);
        mRadioGroupList.add(guardProperShave);
        mRadioGroupList.add(guardHaircut);
        mRadioGroupList.add(guardShoePolish);
        mRadioGroupList.add(guardUniform);
        mRadioGroupList.add(guardCleanUniform);
        mRadioGroupList.add(guardFittingUniform);
        mRadioGroupList.add(guardWellPressedUniform);
        mRadioGroupList.add(guardIcCard);

        qrScanRadioGroup.setOnCheckedChangeListener(customCheckedListener);
        guardAtPost.setOnCheckedChangeListener(customCheckedListener);
        guardProperShave.setOnCheckedChangeListener(customCheckedListener);
        guardHaircut.setOnCheckedChangeListener(customCheckedListener);
        guardShoePolish.setOnCheckedChangeListener(customCheckedListener);
        guardUniform.setOnCheckedChangeListener(customCheckedListener);
        guardCleanUniform.setOnCheckedChangeListener(customCheckedListener);
        guardFittingUniform.setOnCheckedChangeListener(customCheckedListener);
        guardWellPressedUniform.setOnCheckedChangeListener(customCheckedListener);
        guardIcCard.setOnCheckedChangeListener(customCheckedListener);
        for (RadioGroup mGroup : mRadioGroupList) {
            setRadioButtonChecked(mGroup);
        }
    }

    /**
     * Method to check the radio button in a given radioGroup.
     *
     * @param mGroup
     */
    private void setRadioButtonChecked(RadioGroup mGroup) {
//        LinearLayout.LayoutParams mLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        RadioGroup.LayoutParams params_grp = new RadioGroup.LayoutParams(getBaseContext(), null);
        params_grp.setMargins(0, 0, 70, 0);

        RadioButton radioBtn_no = new RadioButton(this);
        radioBtn_no.setLayoutParams(params_grp);
        // radioBtn_no.setLayoutParams(mLayoutParams);
        radioBtn_no.setText(R.string.no);
        radioBtn_no.setTag(0);
        RadioButton radioBtn_yes = new RadioButton(this);
        //radioBtn_yes.setLayoutParams(mLayoutParams);
        radioBtn_yes.setText(R.string.yes);
        radioBtn_yes.setLayoutParams(params_grp);
        radioBtn_yes.setTag(1);
        mGroup.addView(radioBtn_yes);
        mGroup.addView(radioBtn_no);
        mGroup.check(radioBtn_yes.getId());
    }

    /**
     * Method which returns the check radioButton value
     *
     * @param checkedRadioButtonId
     * @return
     */
    private String setSelectedValue(int checkedRadioButtonId, RadioGroup mGroup) {
        String valueSelected = "";
        RadioButton no = (RadioButton) mGroup.getChildAt(1);
        RadioButton yes = (RadioButton) mGroup.getChildAt(0);
        if (R.id.guard_at_post == mGroup.getId()) {
           /* removeImageFromGuardFullImage();
            removeImageFromGuardTurnOut();
            removeSignature();*/
            if (turnOutLabel.equalsIgnoreCase(IMPRESSIVE)) {
                impressiveEdittext.setText("");
            } else if (turnOutLabel.equalsIgnoreCase(POORIMAGE)) {
                poorImageEdittext.setText("");
            }
        }
        if (checkedRadioButtonId == no.getId()) {
            valueSelected = resources.getString(R.string.no);
            /*if (R.id.guard_at_post == mGroup.getId()) {
                if (turnOutLabel.equalsIgnoreCase(IMPRESSIVE)) {
                    disableAutoEditText(impressiveEdittext);
                } else if (turnOutLabel.equalsIgnoreCase(POORIMAGE)) {
                    disableAutoEditText(poorImageEdittext);
                } else {
                    poorImageEdittext.setVisibility(View.GONE);
                    impressiveEdittext.setVisibility(View.GONE);
                }
            }*/
        } else if (checkedRadioButtonId == yes.getId()) {
            valueSelected = resources.getString(R.string.yes);

            if (turnOutLabel.equalsIgnoreCase(IMPRESSIVE)) {
                impressiveEdittext.setText("");
                impressiveEdittext.setVisibility(View.VISIBLE);
                enableAutoEditText(impressiveEdittext);
            } else if (turnOutLabel.equalsIgnoreCase(POORIMAGE)) {
                poorImageEdittext.setText("");
                poorImageEdittext.setVisibility(View.VISIBLE);
                enableAutoEditText(poorImageEdittext);
            }
        }
        return valueSelected;
    }

    private synchronized void removeSignature() {
        madd_signature_guard_level.setImageResource(R.mipmap.ic_addsign);
        AttachmentMetaDataModel attachmentMetaDataModel = (AttachmentMetaDataModel) madd_signature_guard_level.getTag();
        removeImageAttachmentModel(attachmentMetaDataModel);

    }

    private synchronized void removeImageFromGuardTurnOut() {
        overAllTurnoutImage.setImageResource(R.mipmap.ic_guardphoto);
        AttachmentMetaDataModel attachmentMetaDataModel = (AttachmentMetaDataModel) overAllTurnoutImage.getTag();
        removeImageAttachmentModel(attachmentMetaDataModel);
    }

    private synchronized void removeImageFromGuardFullImage() {
        guardFullLengthImage.setImageResource(R.mipmap.ic_guardphoto);
        AttachmentMetaDataModel attachmentMetaDataModel = (AttachmentMetaDataModel) guardFullLengthImage.getTag();
        removeImageAttachmentModel(attachmentMetaDataModel);
    }

    @Override
    public void onClick(View v) {
        Intent capturePictureActivity = null;
        Util.mSequenceNumber++;
        switch (v.getId()) {
            case R.id.add_signature_guard_level:
                if (!TextUtils.isEmpty(autoCompleteTextview.getText().toString().trim())) {
                    Intent signatureActivity = new Intent(this, SignatureActivity.class);
                    signatureActivity.putExtra("SignatureCapture", false);
                    startActivityForResult(signatureActivity, Util.CAPTURE_SIGNATURE);
                } else {
                    snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.select_guard_id_first_msg));
                }
                break;
            case R.id.guard_add_grievance:
                if (!TextUtils.isEmpty(guardName.getText().toString().trim())) {
                    Intent startGreviance = new Intent(this, AddIssuesGrievanceActivity.class);
                    startGreviance.putExtra(resources.getString(R.string.employee_id), mGuardDetail.getEmployeeId());
                    startGreviance.putExtra(TableNameAndColumnStatement.TASK_ID, rotaTaskModel.getTaskId());
                    startGreviance.putExtra(resources.getString(R.string.guard_name), guardName.getText().toString());
                    startGreviance.putExtra(TableNameAndColumnStatement.EMPLOYEE_NO, employeeNo);
                    startGreviance.putExtra(resources.getString(R.string.unitName), rotaTaskModel.getUnitName());
                    startGreviance.putExtra(TableNameAndColumnStatement.UNIT_ID, rotaTaskModel.getUnitId());
                    startGreviance.putExtra(resources.getString(R.string.IS_FROM_DAY_NIGHT_CHECK), true);
                    startActivityForResult(startGreviance, 105);
//                    startActivity(startGreviance);
                } else {
                    snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.client_handshake_validation));
                }
                break;
            case R.id.guardFullLengthImage:
                /*if (!TextUtils.isEmpty(autoCompleteTextview.getText().toString().trim())) {
                    capturePictureActivity = new Intent(GuardDetailsActivity.this, CapturePictureActivity.class);
                    capturePictureActivity.putExtra(resources.getString(R.string.toolbar_title), "");
                    Util.setSendPhotoImageTo(Util.GUARD_DETAIL_FULL_IMAGE);
                    Util.initMetaDataArray(true);
                    capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY,
                            Constants.PIC_TAKEN_FROM_SITEPOST);
                    capturePictureActivity.putExtra(Constants.CHECKING_TYPE, resources.getString(R.string.GUARD_CHECK));
                    startActivityForResult(capturePictureActivity, resources.getInteger(R.integer.GUARD_DETAIL_CHECK_CODE));
                } else {
                    snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.select_guard_id_first_msg));
                }*/
                capturePictureActivity = new Intent(GuardDetailsActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(resources.getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.GUARD_DETAIL_FULL_IMAGE);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY,
                        Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, resources.getString(R.string.GUARD_CHECK));
                startActivityForResult(capturePictureActivity, resources.getInteger(R.integer.GUARD_DETAIL_CHECK_CODE));
                break;
            case R.id.overAllTurnoutImage:
                if (!TextUtils.isEmpty(autoCompleteTextview.getText().toString().trim())) {
                    capturePictureActivity = new Intent(GuardDetailsActivity.this, CapturePictureActivity.class);
                    capturePictureActivity.putExtra(resources.getString(R.string.toolbar_title), "");
                    Util.setSendPhotoImageTo(Util.GUARD_DETAIL_OVER_TURN_OUT);
                    Util.initMetaDataArray(true);
                    capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY,
                            Constants.PIC_TAKEN_FROM_SITEPOST);
                    capturePictureActivity.putExtra(Constants.CHECKING_TYPE, resources.getString(R.string.GUARD_CHECK));
                    startActivityForResult(capturePictureActivity, CommonTags.GUARD_DETAIL_CHECK_TURN_OUT_CODE);
                } else {
                    snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.select_guard_id_first_msg));
                }
                break;
            case R.id.kit_request:

                if (!TextUtils.isEmpty(autoCompleteTextview.getText().toString().trim())) {
                    Intent kitRequestIntent = new Intent(GuardDetailsActivity.this, AddKitRequestActivity.class);
                    kitRequestIntent.putExtra(resources.getString(R.string.employee_id), mGuardDetail.getEmployeeId());
                    kitRequestIntent.putExtra(TableNameAndColumnStatement.TASK_ID, rotaTaskModel.getTaskId());
                    kitRequestIntent.putExtra(resources.getString(R.string.guard_name), guardName.getText().toString());
                    kitRequestIntent.putExtra(TableNameAndColumnStatement.EMPLOYEE_NO, employeeNo);
                    kitRequestIntent.putExtra(resources.getString(R.string.unitName), rotaTaskModel.getUnitName());
                    kitRequestIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, rotaTaskModel.getUnitId());
                    startActivity(kitRequestIntent);
                } else {
                    snackBarWithMesg(mguard_parent_layout, resources.getString(R.string.select_guard_id_first_msg));
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Util.CAPTURE_SIGNATURE) {
                //  Bitmap bmp = data.getParcelableExtra(resources.getString(R.string.signature));
                attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                // madd_signature_guard_level.setImageBitmap(Util.getmSignature());
                madd_signature_guard_level.setImageURI(Uri.parse(attachmentMetaDataModel.getAttachmentPath()));
                attachmentMetaDataModel.setAttachmentSourceCode(autoCompleteTextview.getText().toString().trim());
                attachmentMetaDataModel.setimageCode(CommonTags.GUARD_SIGNATURE);
                attachmentMetaDataModel.setScreenName(CommonTags.GUARD_CHECK_SCREEN);
                madd_signature_guard_level.setTag(attachmentMetaDataModel);
                setAttachmentMetaDataModel(attachmentMetaDataModel);
                attachmentMetaDataModel = null;
                /*setAttachmentMetaDataModelInMap((AttachmentMetaDataKeys.GUARD_CHECK)[2],
                        (AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));*/

            } else if (requestCode == resources.getInteger(R.integer.GUARD_DETAIL_CHECK_CODE)) {
                attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                attachmentMetaDataModel.setAttachmentSourceCode(autoCompleteTextview.getText().toString().trim());
                attachmentMetaDataModel.setimageCode(CommonTags.GUARD_FULL_IMAGE_CODE);
                attachmentMetaDataModel.setScreenName(CommonTags.GUARD_CHECK_SCREEN);
                setAttachmentMetaDataModel(attachmentMetaDataModel);
                guardFullImagePath = attachmentMetaDataModel.getAttachmentPath();
                guardFullLengthImage.setImageURI(Uri.parse(attachmentMetaDataModel.getAttachmentPath()));
                guardFullLengthImage.setTag(attachmentMetaDataModel);
                /*setAttachmentMetaDataModelInMap((AttachmentMetaDataKeys.GUARD_CHECK)[0],
                        (AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));*/

            } else if (requestCode == CommonTags.GUARD_DETAIL_CHECK_TURN_OUT_CODE) {
                attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
                attachmentMetaDataModel.setimageCode(CommonTags.GUARD_TURN_OUT_IMAGE_CODE);
                attachmentMetaDataModel.setScreenName(CommonTags.GUARD_CHECK_SCREEN);
                attachmentMetaDataModel.setAttachmentSourceCode(autoCompleteTextview.getText().toString().trim());
                setAttachmentMetaDataModel(attachmentMetaDataModel);
                overAllTurnoutImage.setImageURI(Uri.parse(attachmentMetaDataModel.getAttachmentPath()));
                overAllTurnoutImage.setTag(attachmentMetaDataModel);
               /* setAttachmentMetaDataModelInMap((AttachmentMetaDataKeys.GUARD_CHECK)[1],
                        (AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));*/
            } else if (requestCode == BARCODE_CAPTURE_REQUEST_CODE && resultCode == RESULT_OK) {
                String barcodeScanValue = data.getStringExtra(BarcodeCaptureActivity.BARCODE_VALUE);
                IdCardMO idCard = new IdCardMO(barcodeScanValue);
                showIdCardInfo(idCard);
                autoCompleteTextview.setText(idCard.getRegNumber());
                autoCompleteTextview.dismissDropDown();
                updateEmployeeInfo(barcodeScanValue);

                lastAkrIssued.setText(resources.getString(R.string.last_akr) + ": " + resources.getString(R.string.NOT_AVAILABLE));
                nextAkrDate.setText(resources.getString(R.string.last_akr_due) + ": " + resources.getString(R.string.NOT_AVAILABLE));
                lastFine.setText(resources.getString(R.string.last_fine) + ": " + resources.getString(R.string.NOT_AVAILABLE));

                isScannedFromQR = true;
                isQRScanWorking = true;
                qrScanStatusLayout.setVisibility(View.GONE);
                qrScanCommentBox.setText("");

            } else if (requestCode == 105) {
                if (data != null) {
                    if (data.hasExtra("Grievance"))
                        issueModelList = data.getParcelableArrayListExtra("Grievance");
                    else if (data.hasExtra("IS_GRIEVANCE_ADDED")) {
                        isGrievanceAdded = data.getBooleanExtra("IS_GRIEVANCE_ADDED", false);
                        mguard_add_grievance.setText("GRIEVANCE ADDED");
                        mguard_add_grievance.setBackgroundResource(R.drawable.green_round_corner);
                    }
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        Util.refreshCheckedGuardIdHolder();
        finish();
    }

    public void doScanBarcode(View view) {
        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intent, BARCODE_CAPTURE_REQUEST_CODE);
    }

    private boolean checkImageCapturedForTurnOut() {

        boolean isPicTakenForTurnOut;
        if ((overAllTurnoutImage.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_guardphoto).getConstantState())) {
            isPicTakenForTurnOut = false;
        } else {
            isPicTakenForTurnOut = true;
        }
        return isPicTakenForTurnOut;
    }

    private boolean checkSignatureCaptured() {
        boolean isSignatureCaptured;
        if (madd_signature_guard_level.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_addsign).getConstantState()) {
            isSignatureCaptured = false;
        } else {
            isSignatureCaptured = true;
        }
        return isSignatureCaptured;
    }

}