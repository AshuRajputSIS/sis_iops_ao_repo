package com.sisindia.ai.android.debugingtool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashu Rajput on 8/31/2017.
 */

public class QueryExecutorBaseOR implements Serializable {

    @SerializedName("StatusCode")
    private Integer statusCode;
    @SerializedName("StatusMessage")
    private String statusMessage;

    private String data;

    @SerializedName("Data")
    @Expose
    public List<QueryExecutorMO> queryExecutorList;
//    private List<QueryExecutorMO> queriesList = new ArrayList<>();


    public void setData(String data) {
        this.data = data;
    }

    /*public String getData() {
        return data;
    }*/

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /*public List<QueryExecutorMO> getData() {
        return queriesList;
    }

    public void setData(List<QueryExecutorMO> data) {
        this.queriesList = data;
    }*/

}
