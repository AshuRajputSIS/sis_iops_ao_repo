package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 28/6/16.
 */

public class BillSubmissionTaskExeMO {


    @SerializedName("GEOLocation")
    private String geoLocation ;

    @SerializedName("BillSubmission")
    private BillSubmissionMO billSubmission;

    /**
     *
     * @return
     * The billSubmission
     */
    public BillSubmissionMO getBillSubmission() {
        return billSubmission;
    }

    /**
     *
     * @param billSubmission
     * The BillSubmission
     */
    public void setBillSubmission(BillSubmissionMO billSubmission) {
        this.billSubmission = billSubmission;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }
}
