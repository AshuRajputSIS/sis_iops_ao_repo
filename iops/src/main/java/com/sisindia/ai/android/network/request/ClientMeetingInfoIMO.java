package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientMeetingInfoIMO {

    @SerializedName("ClientMeet")
    @Expose
    private Boolean ClientMeet;
    @SerializedName("ClientName")
    @Expose
    private String ClientName;
    @SerializedName("AdditionalRemark")
    @Expose
    private String AdditionalRemark;

    /**
     * @return The ClientMeet
     */
    public Boolean getClientMeet() {
        return ClientMeet;
    }

    /**
     * @param ClientMeet The ClientMeet
     */
    public void setClientMeet(Boolean ClientMeet) {
        this.ClientMeet = ClientMeet;
    }

    /**
     * @return The ClientName
     */
    public String getClientName() {
        return ClientName;
    }

    /**
     * @param ClientName The ClientName
     */
    public void setClientName(String ClientName) {
        this.ClientName = ClientName;
    }

    /**
     * @return The AdditionalRemark
     */
    public String getAdditionalRemark() {
        return AdditionalRemark;
    }

    /**
     * @param AdditionalRemark The AdditionalRemark
     */
    public void setAdditionalRemark(String AdditionalRemark) {
        this.AdditionalRemark = AdditionalRemark;
    }

    @Override
    public String toString() {
        return "ClientMeetingInfoIMO{" +
                "ClientMeet=" + ClientMeet +
                ", ClientName='" + ClientName + '\'' +
                ", AdditionalRemark='" + AdditionalRemark + '\'' +
                '}';
    }
}