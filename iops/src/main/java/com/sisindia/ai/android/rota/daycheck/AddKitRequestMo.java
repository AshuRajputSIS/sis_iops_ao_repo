package com.sisindia.ai.android.rota.daycheck;

import android.graphics.Bitmap;

/**
 * Created by Durga Prasad on 25-11-2016.
 */
public class AddKitRequestMo {
    private  String addKitRequestJson;
    private  int taskId;

    public String getAddKitRequestJson() {
        return addKitRequestJson;
    }

    public void setAddKitRequestJson(String addKitRequestJson) {
        this.addKitRequestJson = addKitRequestJson;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
