package com.sisindia.ai.android.rota;

/**
 * Created by Durga Prasad on 16-09-2016.
 */
public class ActivityLogsMO {

    private  String activity;
    private String unitName;
    private  int taskId;
    private String taskName;
    private String taskStatus;
    private String timeStamp;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getActivity() {

        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
