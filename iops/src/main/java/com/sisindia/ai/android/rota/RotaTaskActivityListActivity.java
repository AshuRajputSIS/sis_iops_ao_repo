package com.sisindia.ai.android.rota;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.models.RotaTaskActivityListMO;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;


public class RotaTaskActivityListActivity extends BaseActivity implements OnRecyclerViewItemClickListener {

    final static String TAG = "RotaTaskActivityListActivity";
    RotaTaskActivityListMO rotaTaskActivityListMO;
    private RecyclerView addTaskRecylerView;
    private AddTaskRecyclerAdapter addTaskRecyclerAdapter;
    private Drawable dividerDrawable;
    private Toolbar toolbar;
    private Util util;
    private ArrayList<RotaTaskListMO> rotaTaskListMO;
    private int reasonId;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setBaseToolbar(toolbar,getResources().getString(R.string.ADD_TASK));



        rotaTaskActivityListMO = new RotaTaskActivityListMO();
        addTaskRecylerView = (RecyclerView) findViewById(R.id.add_task_recyclerview);


        Intent intent  = getIntent();
        if(intent != null){
            reasonId = intent.getExtras().getInt(getResources().getString(R.string.select_reason));

        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        addTaskRecylerView.setLayoutManager(linearLayoutManager);
        addTaskRecylerView.setItemAnimator(new DefaultItemAnimator());
        addTaskRecyclerAdapter = new AddTaskRecyclerAdapter(this);
        rotaTaskActivityListMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAllRotaTaskActivity(0);
        rotaTaskListMO = (ArrayList<RotaTaskListMO>) rotaTaskActivityListMO.getRotaTaskListMO();
        addTaskRecyclerAdapter.setRotaData(rotaTaskListMO);
        addTaskRecylerView.setAdapter(addTaskRecyclerAdapter);

        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        addTaskRecylerView.addItemDecoration(dividerItemDecoration);
        addTaskRecyclerAdapter.setOnRecyclerViewItemClickListener(this);
        addTaskRecyclerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        ArrayList<Integer> rankIds = IOPSApplication.getInstance().getRotaLevelSelectionInstance().getRanksIds();
        if(rotaTaskListMO != null && rotaTaskListMO.size() != 0){
            if(rotaTaskListMO.get(position).getRotaTaskActivityId() == RotaTaskTypeEnum.Bill_Submission.getValue()){
                if(rankIds != null && rankIds.size() != 0){
                     // int appUserRankId = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails().getRankId();
                    //     if(rankIds.contains(appUserRankId)){
                      RotaTaskListMO rotaTaskListMO = rotaTaskActivityListMO.getRotaTaskListMO().get(position);
                      Intent addTaskDetailIntent = new Intent(RotaTaskActivityListActivity.this, AddRotaTaskActivity.class);
                      addTaskDetailIntent.putExtra("FromMyBarrack", false);
                      addTaskDetailIntent.putExtra(Constants.CHECKING_TYPE, rotaTaskListMO);
                      addTaskDetailIntent.putExtra(getResources().getString(R.string.select_reason),reasonId);
                      addTaskDetailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                      startActivity(addTaskDetailIntent);
//                    }
//                    else{
                      //snackBarWithMesg(addTaskRecylerView,getResources().getString(R.string.BILLSUBMISSION_WARNING_MSG));
                 // }
              }
          }
            else {
              RotaTaskListMO rotaTaskListMO = rotaTaskActivityListMO.getRotaTaskListMO().get(position);
              Intent addTaskDetailIntent = new Intent(RotaTaskActivityListActivity.this, AddRotaTaskActivity.class);
              addTaskDetailIntent.putExtra("FromMyBarrack", false);
              addTaskDetailIntent.putExtra(Constants.CHECKING_TYPE, rotaTaskListMO);
              addTaskDetailIntent.putExtra(getResources().getString(R.string.select_reason),reasonId);
              addTaskDetailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(addTaskDetailIntent);
          }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            // NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }

}
