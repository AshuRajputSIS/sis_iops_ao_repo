package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

/**
 * Created by Shushrut on 09-06-2016.
 */
public class DayCheckListAdapter extends RecyclerView.Adapter<DayCheckListAdapter.DayCheckViewholder>  {

    int pos = 1;
    private Context mContext;
    private List<DayCheckActivityList> mActivityList;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private int positionToRefersh;
    private boolean isRefreshed;
    /*ImageView mnextarrow;*/
    public DayCheckListAdapter(Context dayCheckNavigationActivity, List<DayCheckActivityList> activityList) {
        mContext = dayCheckNavigationActivity;
        this.mActivityList = activityList;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener){
        this.onRecyclerViewItemClickListener = itemListener;
    }

    @Override
    public DayCheckViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_daycheck_row, parent, false);
        DayCheckViewholder mDayCheckViewholder = new DayCheckViewholder(view);
        return mDayCheckViewholder;
    }

    @Override
    public void onBindViewHolder(DayCheckViewholder holder, int position) {


       DayCheckActivityList dayCheckActivityList = mActivityList.get(position);
        if(null!=dayCheckActivityList){

                holder.mitemname.setText(dayCheckActivityList.getActivityName());
                holder.mitemname.setTag(position);

                if (dayCheckActivityList.isActivityStatus()) {
                    holder.mnextarrow.setImageResource(R.drawable.greentick);
                }else {
                    holder.mnextarrow.setImageResource(R.drawable.arrow_right);
                }

        }






                //holder.mitemname.setOnClickListener(this);
        // Log.e("ArrayList","ArrayList :::"+StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder().toString());
       /* if(isRefreshed){
            int pos = position;

            HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
            if(null!=mStepperDataHolder && mStepperDataHolder.size()!=0){
                for (Map.Entry<String, Integer> entry : mStepperDataHolder.entrySet()) {
                    String key = entry.getKey();
                    Integer value = entry.getValue();

                    key = key.replace("_"," ");
                    if (mDayCheckType.get(position).toString().trim().equalsIgnoreCase(key)) {
                        holder.mnextarrow.setImageResource(R.drawable.greentick);
                    }else {
                        holder.mnextarrow.setImageResource(R.drawable.arrow_right);
                    }
                }
            }


           *//* HashMap<String, Integer> mStepperDataHolder = StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder();
            if(null!=mStepperDataHolder && mStepperDataHolder.size()!=0){
                if(mStepperDataHolder.containsKey(mDayCheckType.get(position).toString().trim())){
                    holder.mnextarrow.setImageResource(R.drawable.greentick);
                }else {
                    holder.mnextarrow.setImageBitmap(null);
                }
            }*//*
           *//* if(StepperDataMo.getmStepperDataMoInstance().getmStepperDataHolder()) {
                holder.mnextarrow.setImageResource(R.drawable.greentick);
            }*//*

        }*/

        holder.mitem_count_value.setText(String.valueOf(position+1));
    }

    @Override
    public int getItemCount() {
        return mActivityList.size();
    }

    public DayCheckActivityList getItem(int position) {
        return mActivityList.get(position);
    }


   /* @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.itemname:
                Toast.makeText(mContext, "Pos :"+v.getTag(), Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    public void updateWithTickMark(int pos,boolean isRefreshed) {
        positionToRefersh = pos;
        this.isRefreshed = isRefreshed;

    }

    public class DayCheckViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomFontTextview mitemname;
        TextView mitem_count_value;
        ImageView mnextarrow;

        public DayCheckViewholder(View itemView) {
            super(itemView);
            mitemname = (CustomFontTextview)itemView.findViewById(R.id.itemname);
            mitem_count_value = (TextView)itemView.findViewById(R.id.item_count_value);
            mnextarrow = (ImageView)itemView.findViewById(R.id.nextarrow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v,getLayoutPosition());
        }
    }

}
