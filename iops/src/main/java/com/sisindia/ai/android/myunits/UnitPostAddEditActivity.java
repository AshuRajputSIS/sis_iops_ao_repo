package com.sisindia.ai.android.myunits;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.CheckIsMainGateMO;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.myunits.models.UnitPostGeoPoint;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.request.UnitEquipmentIMO;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.MapView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.sisindia.ai.android.myunits.AddEquipmentBaseActivity.ADD_EQUIPMENT_KEY;
import static com.sisindia.ai.android.myunits.AddEquipmentBaseActivity.ADD_POST_REQUEST_CODE;
import static com.sisindia.ai.android.myunits.EditEquipment.EDIT_EQUIPMENT_KEY;
import static com.sisindia.ai.android.myunits.EditEquipment.EDIT_EQUIPMENT_POS_KEY;
import static com.sisindia.ai.android.myunits.EditEquipment.EDIT_REQUEST_CODE;
import static com.sisindia.ai.android.utils.barcodereader.BarcodeCaptureActivity.BARCODE_CAPTURE_REQUEST_CODE;

public class UnitPostAddEditActivity extends BaseActivity implements OnItemClickListener, OnClickListener {

    //    private static final String TAG = UnitPostAddEditActivity.class.getSimpleName();
    private static final int REQUEST_TAKE_PHOTO = 1;
    @Bind(R.id.takePicture)
    CustomFontButton takePicture;
    @Bind(R.id.retakePictureButton)
    CustomFontButton retakePicture;
    @Bind(R.id.capturedImage)
    ImageView capturedImage;
    @Bind(R.id.pictureLayout)
    LinearLayout pictureLayout;
    @Bind(R.id.retakeViewlayout)
    RelativeLayout retakeViewlayout;
    @Bind(R.id.edit_postname)
    CustomFontEditText etPostName;
    @Bind(R.id.addpost_is_maingate_checckbox)
    CheckBox isMainGateCheckbox;
    @Bind(R.id.addpost_add_equipment_label_txt)
    TextView addEquipment;
    @Bind(R.id.no_equipments_label)
    TextView noEquipmentLabel;
    @Bind(R.id.tv_nfc_tag)
    TextView tvNfcTagAdded;
    @Bind(R.id.ll_nfc_layout)
    LinearLayout llNfcLayout;
    AttachmentMetaDataModel model;
    @Bind(R.id.equipmentList)
    LinearLayout equipmentList;
    @Bind(R.id.equipment_listview)
    ListView equipmentListview;
    LinearLayout mAddPostPic;
    @Bind(R.id.addpost_coordinator_layout)
    CoordinatorLayout addpost_coordinator_layout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.scanQRCodeButton)
    CustomFontButton scanQRCodeButton;

    private String addPostTitleBeforeUpdate, addPostTitleAfterUpdate;
    private boolean isImageCaptured = false;
    private ArrayList<SyncingUnitEquipmentModel> equipList = new ArrayList<>();
    private EquipmentListAdapter adapter;
    private MapView mapview;
    private int unitId;
    private String actionType;
    private PostData postdata;
    private int isMainGate = 0;
    private String qrUniqueId = "";
    private Resources mResources;
    //    private ArrayList<UnitPost> unitPostList;
//    private ArrayList<String> nfcList = new ArrayList<>();
//    private boolean nfcAddAllowed;
//    private boolean isNfcConfiguredForBarrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_units_add_post);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        mResources = getResources();

        Bundle bundel = getIntent().getExtras();
        if (bundel != null) {
            actionType = bundel.getString(mResources.getString(R.string.post_action_type));
            postdata = (PostData) getIntent().getSerializableExtra(mResources.getString(R.string.post_entity));
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(actionType);
        }
        setUpUI();

        addPostTitleBeforeUpdate = etPostName.getText().toString().trim();

        etPostName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                addPostTitleAfterUpdate = s.toString().trim();
                Util.setMpostName(addPostTitleAfterUpdate);
            }
        });

//        nfcList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getConfiguredNfclist();
    }

    /*BroadcastReceiver nfcUniqueIdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            nfcUniqueId = intent.getStringExtra(Constants.Nfc.NFC_UNIQUE_ID);
            Timber.d(TAG, "NFC onReceive: Unique Id : %s", nfcUniqueId);
            if (TextUtils.isEmpty(nfcUniqueId)) {
                llNfcLayout.setVisibility(View.GONE);
            } else {
                llNfcLayout.setVisibility(View.VISIBLE);
                tvNfcTagAdded.setText(R.string.nfc_added_to_this_post);
            }
            handleNfcUniqueId();
        }
    };*/

    /*private void handleNfcUniqueId() {
        if (Util.isNetworkConnected()) {
            checkNfcAddStatusOnline(unitId, nfcUniqueId);
        } else {
            UnitLevelSelectionQuery unitLevelSelectionQuery = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance();
            if (unitLevelSelectionQuery.isNfcConfiguredForUnitPostOffline(nfcUniqueId, unitId)) {
                Toast.makeText(this, R.string.nfc_already_configured, Toast.LENGTH_SHORT).show();
                nfcUniqueId = null;
            } else if (unitLevelSelectionQuery.isNfcConfiguredForBarrackOffline(nfcUniqueId)) {
                Toast.makeText(this, R.string.nfc_already_configured, Toast.LENGTH_SHORT).show();
                nfcUniqueId = null;
            } else {
                Toast.makeText(UnitPostAddEditActivity.this, getString(R.string.nfc_configured_successfully), Toast.LENGTH_SHORT).show();
                IOPSApplication.getInstance().getUnitPostUpdateStatementDB().updateNfcDeviceNo(nfcUniqueId, -1, unitId);
            }
        }

    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_post_save:
                if (TextUtils.isEmpty(etPostName.getText().toString().trim())) {
                    snackBarWithMesg(addpost_coordinator_layout, getString(R.string.post_name_cannot_be_empty));
                    return false;
                }
                Util.setMpostName(etPostName.getText().toString().trim());

                if (actionType.equalsIgnoreCase(getResources().getString(R.string.post_new))) {
                    Timber.d("isMainGate==   %s", isMainGate);
                    if (IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                            .duplicatePostExist(addPostTitleAfterUpdate.toLowerCase(), unitId)) {
                        snackBarWithMesg(addpost_coordinator_layout, getResources()
                                .getString(R.string.duplicate_post_name_validation));
                    } else if (TextUtils.isEmpty(qrUniqueId)) {
                        snackBarWithMesg(addpost_coordinator_layout, "Please scan QR code");
                        return false;
                    } else {
                        updateMainGateImageUrl(isMainGate);
                        postAddEdit();
                        insertInMetadataTable();
                        insertInUnitEquipment();
                    }
                } else if (actionType.equalsIgnoreCase(getResources().getString(R.string.post_edit))) {

                    if (TextUtils.isEmpty(qrUniqueId)) {
                        snackBarWithMesg(addpost_coordinator_layout, "Please scan QR code");
                        return false;
                    }

                    if (!addPostTitleAfterUpdate.trim().equalsIgnoreCase(addPostTitleBeforeUpdate) &&
                            IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                                    .duplicatePostExist(addPostTitleAfterUpdate.toLowerCase(), unitId)) {
                        snackBarWithMesg(addpost_coordinator_layout, getResources()
                                .getString(R.string.duplicate_post_name_validation));
                        return false;
                    }
                    if (isMainGate == 1) {
                        if (isMainGateCheckbox.isChecked())
                            isMainGate = 1;
                        else
                            isMainGate = 0;
                    }
                    Timber.d("isMainGate==   %s", isMainGate);
                    updateMainGateImageUrl(isMainGate);
                    postdata.setDeviceNo(qrUniqueId);
                    postdata.setLatitude(mapview.getLatitude());
                    postdata.setLongitude(mapview.getLongitude());
                    postdata.setIsMainGate(isMainGate);
                    postdata.setPostName(addPostTitleAfterUpdate.toString());

                    IOPSApplication.getInstance().getUpdateStatementDBInstance().UpdateUnitPostTable(postdata, 0);
                    UpdateEquipmentTable();
                    Bundle bundle = new Bundle();
                    IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
                    setResult(Constants.MYUNITS_ADD_POST_REQUEST_CODE, new Intent().putExtra("Upadte", addPostTitleAfterUpdate));
                    finish();
                }
                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void postAddEdit() {
        ArrayList<UnitPost> unitPostList = new ArrayList<>();
        unitPostList.add(setPostData());
        IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitPost(unitPostList, false);
    }

    private UnitPost setPostData() {
        UnitPost unitPost = new UnitPost();
        unitPost.setId(0);
        unitPost.setUnitId(unitId);
        unitPost.setUnitPostName(addPostTitleAfterUpdate);
        unitPost.setDescription("");
        unitPost.setArmedStrength(0);
        unitPost.setUnarmedStrength(0);
        unitPost.setMainGateDistance(0.0f);
        unitPost.setBarrackDistance(0.0f);
        unitPost.setUnitOfficeDistance(0.0f);
        unitPost.deviceNo = qrUniqueId;
        unitPost.setNew(true);
        unitPost.setSynced(false);

        if (isMainGate == 1)
            unitPost.setIsMainGate(true);
        else
            unitPost.setIsMainGate(false);

        List<UnitPostGeoPoint> unitPostGeoPointList = new ArrayList<>();
        UnitPostGeoPoint unitPostGeoPoint = new UnitPostGeoPoint();
        unitPostGeoPoint.setLatitude(mapview.getLatitude());
        unitPostGeoPoint.setLongitude(mapview.getLongitude());
        unitPostGeoPointList.add(unitPostGeoPoint);
        unitPost.setUnitPostGeoPoint(unitPostGeoPointList);
        return unitPost;
    }

    private void setUpUI() {
        mAddPostPic = (LinearLayout) findViewById(R.id.retakeView);
        MyUnitHomeMO unitDetails = (MyUnitHomeMO) SingletonUnitDetails.getInstance().getUnitDetails();
        if (unitDetails == null) {
            unitDetails = appPreferences.getUnitJsonData();
        }
        mapview = (MapView) findViewById(R.id.mapview);
        unitId = unitDetails.getUnitId();
        Util.initMetaDataArray(true);
        equipmentListview.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        if (actionType.equalsIgnoreCase(mResources.getString(R.string.post_edit))) {
            setEditPostUI();
            etPostName.setKeyListener(null); // non editable text box
        } else {
            isMainGateCheckbox.setOnClickListener(this);
            updateEquipmentAdapter(equipList);
        }
    }

    @OnClick(R.id.takePicture)
    public void onImageCapture() {
        if (!TextUtils.isEmpty(addPostTitleAfterUpdate))
            dispatchTakePictureIntent(true);
        else
            snackBarWithMesg(addpost_coordinator_layout, mResources.getString(R.string.post_name_validation));
    }

    @OnClick(R.id.retakePictureButton)
    public void onRetakePicture() {
        if (!TextUtils.isEmpty(addPostTitleAfterUpdate)) {
            dispatchTakePictureIntent(false);
        } else {
            snackBarWithMesg(addpost_coordinator_layout, mResources.getString(R.string.post_name_validation));
        }
    }

    @OnClick(R.id.addpost_add_equipment_label_txt)
    public void onAddEquipmentClick() {
        addPostTitleAfterUpdate = etPostName.getText().toString().trim();
        if (!TextUtils.isEmpty(addPostTitleAfterUpdate)) {
            Intent i = new Intent(this, AddEquipmentBaseActivity.class);
            i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivityForResult(i, ADD_POST_REQUEST_CODE);

        } else {
            snackBarWithMesg(addpost_coordinator_layout, mResources.getString(R.string.post_name_validation));
        }
    }

    @OnClick(R.id.scanQRCodeButton)
    public void scanQRCodeButton() {
        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intent, BARCODE_CAPTURE_REQUEST_CODE);
    }

    private void dispatchTakePictureIntent(boolean incrementSequenceNumber) {
        if (incrementSequenceNumber) {
            Util.mSequenceNumber++;
        }
        Util.setIncrementSequenceNumber(true);
        Intent capturePictureActivity = new Intent(this, CapturePictureActivity.class);
        capturePictureActivity.putExtra(mResources.getString(R.string.toolbar_title), mResources.getString(R.string.add_post));
        if (postdata != null) {
            capturePictureActivity.putExtra(TableNameAndColumnStatement.POST_ID, postdata.getUnitPostId());
        }
        Util.setSendPhotoImageTo(Util.POST_CHECK);
        startActivityForResult(capturePictureActivity, REQUEST_TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Constants.NUMBER_TWO) {
            model = data.getParcelableExtra(Constants.META_DATA);
            isImageCaptured = true;
            if (model != null) {
                capturedImage.setImageURI(Uri.parse(model.getAttachmentPath()));
            }
            pictureLayout.setVisibility(View.GONE);
            retakeViewlayout.setVisibility(View.VISIBLE);
        } else if (requestCode == EDIT_REQUEST_CODE && resultCode == RESULT_OK) {
            SyncingUnitEquipmentModel syncingUnitEquipmentModel = data.getParcelableExtra(EDIT_EQUIPMENT_KEY);
            int position = data.getIntExtra(EDIT_EQUIPMENT_POS_KEY, -1);
            editEquipmentUpdateList(syncingUnitEquipmentModel, position);
        } else if (requestCode == ADD_POST_REQUEST_CODE && resultCode == RESULT_OK) {
            // new Record to be inserted
            SyncingUnitEquipmentModel syncingUnitEquipmentModel = data.getParcelableExtra(ADD_EQUIPMENT_KEY);

            if (syncingUnitEquipmentModel != null)
                addEquipmentToList(syncingUnitEquipmentModel);

        } else if (requestCode == BARCODE_CAPTURE_REQUEST_CODE && resultCode == RESULT_OK) {
            qrUniqueId = data.getStringExtra(BarcodeCaptureActivity.BARCODE_VALUE);
            if (!qrUniqueId.trim().equals("")) {
                if (!IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().duplicateQRCodeExist(qrUniqueId)) {
                    scanQRCodeButton.setBackgroundResource(R.drawable.green_round_corner);
                    scanQRCodeButton.setText("QR SCANNED");
                    snackBarWithMesg(addpost_coordinator_layout, "QR Scanned successfully");
                } else {
                    snackBarWithMesg(addpost_coordinator_layout, "Scanned QR code already configured. Scan another one");
                }
            } else
                snackBarWithMesg(addpost_coordinator_layout, "Scanned QR code is empty...");
        }
    }

    /****
     * Method for Setting the Height of the ListView dynamically.
     * *** Hack to fix the issue of not showing all the items of the ListView
     * *** when placed inside a ScrollView
     ****/
    private void setListViewHeightBasedOnChildren() {
        ListAdapter listAdapter = equipmentListview.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(equipmentListview.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, equipmentListview);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = equipmentListview.getLayoutParams();
        params.height = totalHeight + (equipmentListview.getDividerHeight() * (listAdapter.getCount() - 1));
        equipmentListview.setLayoutParams(params);
    }

    private void insertInMetadataTable() {
        if (isImageCaptured && model != null) {
            Util.setmSequenceNumber(0);
            model.setAttachmentSourceTypeId(Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_POST, this)); // unit post or grivence via etc...
            model.setAttachmentSourceId(IOPSApplication.getInstance().getSelectionStatementDBInstance()
                    .getSequencePostId());
            model.setIsSave(isMainGate); //only store the IsMain=1 gate Image. Don't delete the file after it is uploaded to the server
            model.setAttachmentId(0);
            model.setUnitId(unitId);
            model.setTaskId(0);
            IOPSApplication.getInstance().getInsertionInstance().insertIntoAttachmentDetails(model);
        }
    }

    /**
     * update the image url to the unit table
     */
    private void updateMainGateImageUrl(int mainGate) {
        if (mainGate == 1) {
            if (model != null && !TextUtils.isEmpty(model.getAttachmentPath())) {
                IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                        .updateMainGateImageUrl(model.getAttachmentPath(), unitId);
            }
        } else {
            IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                    .updateMainGateImageUrl(null, unitId);
        }
    }

    private void setEditPostUI() {
        if (postdata != null) {
//            Timber.d("Post Dta object %s", postdata);
            etPostName.setText(postdata.getPostName());
            addPostTitleAfterUpdate = postdata.getPostName();
            mapview.setisRelocate(true, postdata.getLatitude(), postdata.getLongitude());
            qrUniqueId = postdata.getDeviceNo();
            Log.e("qrUniqueId", "qrUniqueId " + qrUniqueId);
            if (postdata.getIsMainGate() == 1) {
                isMainGate = 1;
                isMainGateCheckbox.setOnClickListener(null);
                isMainGateCheckbox.setChecked(true);
            } else {
                isMainGateCheckbox.setChecked(false);
                isMainGate = 0;
                isMainGateCheckbox.setOnClickListener(this);
            }

            equipList.clear();
            equipList = IOPSApplication.getInstance().getSelectionStatementDBInstance()
                    .getUnitEquipmentDetails(postdata.getUnitPostId());
            updateEquipmentAdapter(equipList);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addpost_is_maingate_checckbox:
                CheckIsMainGateMO model = IOPSApplication.getInstance().getSelectionStatementDBInstance().checkIsMainGate(unitId);
                if (isMainGateCheckbox.isChecked() && model.getIsMaingate() == 0)
                    isMainGate = 1;
                else if (!isMainGateCheckbox.isChecked() && model.getIsMaingate() == 0)
                    isMainGate = 0;
                else if (isMainGateCheckbox.isChecked() && model.getIsMaingate() == 1) {
                    isMainGate = 0;
                    isMainGateCheckbox.setChecked(false);
                    snackBarWithMesg(addpost_coordinator_layout, mResources.getString(R.string.post_main_gate_vaidation));
                }
                break;
        }
    }

    /*private void checkNfcAddStatusOnline(int unitId, final String uniqueId) {
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.Nfc.UNIT_ID, unitId);
        map.put(Constants.Nfc.DEVICE_NO, uniqueId);
        checkNfcAddStatusViaApi(map);
    }*/

    /*private void checkNfcAddStatusViaApi(final Map<String, Object> map) {
        new SISClient(this).getApi().checkNfcStatus(map, new Callback<NfcCheckResponse>() {
            @Override
            public void success(NfcCheckResponse nfcCheckResponse, Response response) {
                Timber.d("NFC add status check : %s", nfcCheckResponse);
                nfcAddAllowed = nfcCheckResponse.nfcData.allowToAdd;
                checkNfcAddStatusViaBarrackApi((String) map.get(Constants.Nfc.DEVICE_NO));
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error, "Error while getting add nfc status");
            }
        });
    }*/

    /*private void checkNfcAddStatusViaBarrackApi(String nfcDeviceID) {
        new SISClient(this).getApi().checkBarrackNfcStatus(nfcDeviceID, new Callback<NfcCheckResponse>() {
            @Override
            public void success(NfcCheckResponse nfcCheckResponse, Response response) {
                Timber.d("NFC add status check @Barrack: %s", nfcCheckResponse);
                isNfcConfiguredForBarrack = nfcCheckResponse.nfcData.allowToAdd;
                if (!nfcAddAllowed) {
                    Toast.makeText(UnitPostAddEditActivity.this, R.string.nfc_already_added, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = null;
                } else if (!isNfcConfiguredForBarrack) {
                    Toast.makeText(UnitPostAddEditActivity.this, R.string.nfc_already_added, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = null;
                } else {
                    Toast.makeText(UnitPostAddEditActivity.this, getString(R.string.nfc_configured_successfully), Toast.LENGTH_SHORT).show();
                    IOPSApplication.getInstance().getUnitPostUpdateStatementDB().updateNfcDeviceNo(nfcUniqueId, -1, unitId);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error, "Error while getting add nfc status");
            }
        });
    }*/

   /* @Override
    public void onResume() {
        super.onResume();
        registerReceiver(nfcUniqueIdReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nfcUniqueIdReceiver);
    }*/

    private void updateEquipmentAdapter(ArrayList<SyncingUnitEquipmentModel> equipList) {
        if (equipList != null && equipList.size() > 0) {
            noEquipmentLabel.setVisibility(View.GONE);
            equipmentListview.setVisibility(View.VISIBLE);
            if (null == adapter) {
                adapter = new EquipmentListAdapter(equipList, this);
                equipmentListview.setAdapter(adapter);
            } else {
                adapter.updateAdapter(equipList);
            }
            equipmentListview.setOnItemClickListener(this);
            setListViewHeightBasedOnChildren();

        } else {
            noEquipmentLabel.setVisibility(View.VISIBLE);
            equipmentListview.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Intent intent = new Intent(this, EditEquipment.class);
        intent.putExtra(EDIT_EQUIPMENT_KEY, equipList.get(pos));
        intent.putExtra(EDIT_EQUIPMENT_POS_KEY, pos);
        startActivityForResult(intent, EDIT_REQUEST_CODE);
    }

    private void UpdateEquipmentTable() {
        if (equipList != null && equipList.size() != 0) {
            for (SyncingUnitEquipmentModel model : equipList) {
                IOPSApplication.getInstance().getInsertionInstance().updateInsertUnitEquipment(model);
            }
        }
    }

    private void insertInUnitEquipment() {
        if (equipList != null && equipList.size() != 0) {
            for (SyncingUnitEquipmentModel model : equipList) {
                UnitEquipmentIMO unitEquipment = new UnitEquipmentIMO();
                unitEquipment.setPostId(IOPSApplication.getInstance().getSelectionStatementDBInstance().getSequencePostId());
                unitEquipment.setUnitId(unitId);
                unitEquipment.setStatus(1);
                unitEquipment.setQuantity(model.getAddEquipmentLocalMO().getEquipmentCount());
                unitEquipment.setEquipmentUniqueNo("");
                unitEquipment.setEquipmentManufacturer("");
                unitEquipment.setEquipmentId(model.getAddEquipmentLocalMO().getEquipmentTypId());
                unitEquipment.setIsCustomerProvided(model.getIsCustomerProvided());
                IOPSApplication.getInstance().getInsertionInstance().addUnitEquipment(unitEquipment, true, false);
            }
        }
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
        setResult(Constants.MYUNITS_ADD_POST_REQUEST_CODE, new Intent().putExtra("Upadte", addPostTitleAfterUpdate));
        finish();
    }

    private void editEquipmentUpdateList(SyncingUnitEquipmentModel syncingUnitEquipmentModel, int position) {
        if (-1 != position) {
            if (syncingUnitEquipmentModel.getAddEquipmentLocalMO().getIsSynced() == 0 &&
                    syncingUnitEquipmentModel.getAddEquipmentLocalMO().getIsNew() == 1) {
            } else if (syncingUnitEquipmentModel.getAddEquipmentLocalMO().getIsSynced() == 0 &&
                    syncingUnitEquipmentModel.getAddEquipmentLocalMO().getIsNew() == 0) {
            }
            syncingUnitEquipmentModel.getAddEquipmentLocalMO().setIsSynced(0);
            if (postdata != null) {
                syncingUnitEquipmentModel.setPostId(postdata.getUnitPostId());
            }
            equipList.set(position, syncingUnitEquipmentModel);
            updateEquipmentAdapter(equipList);
        }
    }

    private void addEquipmentToList(SyncingUnitEquipmentModel syncingUnitEquipmentModel) {
        if (actionType.equalsIgnoreCase(mResources.getString(R.string.post_edit))) {
            if (postdata.getUnitPostId() == 0) {
                syncingUnitEquipmentModel.setPostId(postdata.getSequenceId()); // postid if editing post
            } else {
                syncingUnitEquipmentModel.setPostId(postdata.getUnitPostId());
            }

        }
        syncingUnitEquipmentModel.getAddEquipmentLocalMO().setIsNew(1);
        syncingUnitEquipmentModel.getAddEquipmentLocalMO().setIsSynced(0);
        syncingUnitEquipmentModel.setUnitId(unitId);
        syncingUnitEquipmentModel.setStatus(1);
        syncingUnitEquipmentModel.setEquipmentUniqueNo("");
        syncingUnitEquipmentModel.setEquipmentManufacturer("");
        equipList.add(syncingUnitEquipmentModel);
        updateEquipmentAdapter(equipList);
    }
}