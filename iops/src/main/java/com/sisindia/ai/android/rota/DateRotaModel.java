package com.sisindia.ai.android.rota;

/**
 * Created by Saruchi on 16-06-2016.
 */

public class DateRotaModel {
    private String taskStatus;
    private String currentDateStr;
    private int rotaTotalCount;
    private int rotaCompletedCount;
    private String rotaDateStr;
    private int rotaSyncedCount;

    public int getRotaSyncedCount() {
        return rotaSyncedCount;
    }

    public void setRotaSyncedCount(int rotaSyncedCount) {
        this.rotaSyncedCount = rotaSyncedCount;
    }

    public int getRotaTotalCount() {
        return rotaTotalCount;
    }

    public void setRotaTotalCount(int rotaTotalCount) {
        this.rotaTotalCount = rotaTotalCount;
    }

    public int getRotaCompletedCount() {
        return rotaCompletedCount;
    }

    public void setRotaCompletedCount(int rotaCompletedCount) {
        this.rotaCompletedCount = rotaCompletedCount;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getCurrentDateStr() {
        return currentDateStr;
    }

    public void setCurrentDateStr(String currentDateStr) {
        this.currentDateStr = currentDateStr;
    }

    public String getRotaDateStr() {
        return rotaDateStr;
    }

    String k = new String();

    public void setRotaDateStr(String rotaDateStr) {
        this.rotaDateStr = rotaDateStr;
    }

    @Override
    public String toString() {
        return "DateRotaModel{" +
                "taskStatus='" + taskStatus + '\'' +
                ", currentDateStr='" + currentDateStr + '\'' +
                '}';
    }


}