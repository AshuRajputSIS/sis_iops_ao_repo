package com.sisindia.ai.android.issues.grievances;

/**
 * Created by Shushrut on 27-04-2016.
 */
public interface PhotoUpdateListener {
    void updatePhotoView();
}
