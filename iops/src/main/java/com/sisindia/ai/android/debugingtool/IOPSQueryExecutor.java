package com.sisindia.ai.android.debugingtool;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.debugToolDTO.QuerySelectStatementDB;
import com.sisindia.ai.android.debugingtool.adapter.QueryExecutorAdapter;
import com.sisindia.ai.android.debugingtool.adapter.QueryExecutorAdapter.ExecutableQueryListener;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorBaseOR;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ashu Rajput on 8/31/2017.
 */

public class IOPSQueryExecutor extends BaseActivity implements ExecutableQueryListener {
//    OnRefreshListener,

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.queryExecutorRV)
    RecyclerView queryExecutorRecyclerView;
    //    @Bind(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;
    private QueryExecutorAdapter queryExecutorAdapter;
    private QuerySelectStatementDB querySelectStatementDB = null;
    private List<QueryExecutorMO> receivedListOfQueries = Collections.emptyList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iops_query_executor);
        ButterKnife.bind(this);
        setUpToolBar();
//        swipeRefreshLayout.setOnRefreshListener(this);
        callListOfQueriesAPI();
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.setting));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_query_executor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            IOPSQueryExecutor.this.finish();
        } else if (id == R.id.refreshList) {
            Toast.makeText(IOPSQueryExecutor.this, "Refresh Clicked", Toast.LENGTH_LONG).show();
            callListOfQueriesAPI();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callListOfQueriesAPI() {

        SISClient sisClient = new SISClient(this);
        showProgressBar();

        sisClient.getApi().getQueryExecutorDetails(appPreferences.getAppUserId(), new Callback<QueryExecutorBaseOR>() {
            @Override
            public void success(final QueryExecutorBaseOR queryExecutorOR, Response response) {
                switch (queryExecutorOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        hideProgressBar();
                        receivedListOfQueries = queryExecutorOR.queryExecutorList;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showListOfQueries(receivedListOfQueries);
                            }
                        });

                        break;
                    default:
                        Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.API_FAIL_MSG));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressBar();
                Crashlytics.logException(error);
                Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.API_FAIL_MSG));
            }
        });
    }

    private void showListOfQueries(List<QueryExecutorMO> queryExecutorDetails) {
        if (queryExecutorDetails != null && queryExecutorDetails.size() > 0) {

            //Adding divider or separator line between each rows of recycler view
            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider));
            queryExecutorRecyclerView.addItemDecoration(dividerItemDecoration);

            queryExecutorAdapter = new QueryExecutorAdapter(this, queryExecutorDetails);
            queryExecutorRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            queryExecutorRecyclerView.setAdapter(queryExecutorAdapter);
        }
    }

    /*@Override
    public void onRefresh() {
        Toast.makeText(IOPSQueryExecutor.this, "Pull to Refresh executed...", Toast.LENGTH_LONG).show();
    }*/

    @Override
    public void executeQuery(int position, QueryExecutorMO queryExecutorMO) {
        if (queryExecutorMO != null) {
            Log.e("Query", "Query -> " + queryExecutorMO.getQuery() + " Id :" + queryExecutorMO.getId());
            queryExecutor(queryExecutorMO.getId(), queryExecutorMO.getQuery(), position);
        }
    }

    public void queryExecutor(String queryId, String query, int position) {
        if (querySelectStatementDB == null)
            querySelectStatementDB = new QuerySelectStatementDB(this);

//        if (queryToBeExecuted != null && !queryToBeExecuted.isEmpty()) {
        String returnValue = querySelectStatementDB.getQueryExecutorResult(query);
        if (returnValue != null && !returnValue.isEmpty()) {

            postQueryResult(queryId, returnValue, position);

        }
    }

    private void postQueryResult(String Id, String returnValue, final int position) {

        QueryExecutorMO queryExecutorMO = new QueryExecutorMO();
        try {
            queryExecutorMO.setId(Id);
            queryExecutorMO.setQueryResult(returnValue);
            //queryExecutorMO.setResultStatus(resultStatus);
            if (returnValue.equalsIgnoreCase("NULL_ARRAY") || returnValue.equalsIgnoreCase("NULL_CURSOR")) {
                queryExecutorMO.setQueryStatus(String.valueOf(0));
            } else if (returnValue.contains("EXCEPTION")) {
                queryExecutorMO.setQueryStatus(String.valueOf(-1));
            } else {
                queryExecutorMO.setQueryStatus(String.valueOf(1));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        SISClient sisClient = new SISClient(this);
//        sisClient.getApi().postQueryExecutorDetails("9700", mainJsonObject.toString(), new Callback<QueryExecutorBaseOR>() {
        sisClient.getApi().postQueryExecutorDetails(queryExecutorMO, new Callback<QueryExecutorBaseOR>() {
            @Override
            public void success(final QueryExecutorBaseOR queryExecutorOR, Response response) {
                switch (queryExecutorOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        hideProgressBar();
                        if (queryExecutorAdapter != null) {
                            Log.e("Refresh", "Coming to update the table");
                            if (receivedListOfQueries != null && receivedListOfQueries.size() != 0) {
                                receivedListOfQueries.get(position).setQueryResult("1");
                                queryExecutorAdapter = new QueryExecutorAdapter(IOPSQueryExecutor.this, receivedListOfQueries);
                                queryExecutorRecyclerView.setAdapter(queryExecutorAdapter);
                                queryExecutorAdapter.notifyDataSetChanged();
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(IOPSQueryExecutor.this, "Query executed successfully ", Toast.LENGTH_LONG).show();
                            }
                        });
                        break;
                    default:
                        Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.API_FAIL_MSG));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressBar();
                if (NetworkUtil.isConnected)
                    Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.API_FAIL_MSG));
            }
        });
    }

    private void showProgressBar() {
        Util.showProgressBar(IOPSQueryExecutor.this, R.string.loading_please_wait);
    }

    private void hideProgressBar() {
        Util.dismissProgressBar(isDestroyed());
    }


}
