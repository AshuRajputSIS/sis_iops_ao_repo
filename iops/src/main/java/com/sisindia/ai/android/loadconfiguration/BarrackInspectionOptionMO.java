package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/7/16.
 */
public class BarrackInspectionOptionMO {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("BarrackInspectionQuestionId")
    @Expose
    private Integer barrackInspectionQuestionId;
    @SerializedName("BarrackInspectionOptionId")
    @Expose
    private Integer barrackInspectionOptionId;

    @SerializedName("BarrackInspectionOptionName")
    @Expose
    private String barrackInspectionOptionName;
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;

    private Integer sequenceId;
    @SerializedName("IsMandatory")
    private boolean isMandatory;
    @SerializedName("ControlType")
    private String controlType;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getBarrackInspectionOptionName() {
        return barrackInspectionOptionName;
    }

    public void setBarrackInspectionOptionName(String barrackInspectionOptionName) {
        this.barrackInspectionOptionName = barrackInspectionOptionName;
    }

    public Integer getBarrackInspectionOptionId() {
        return barrackInspectionOptionId;
    }

    public void setBarrackInspectionOptionId(Integer barrackInspectionOptionId) {
        this.barrackInspectionOptionId = barrackInspectionOptionId;
    }

    public Integer getBarrackInspectionQuestionId() {
        return barrackInspectionQuestionId;
    }

    public void setBarrackInspectionQuestionId(Integer barrackInspectionQuestionId) {
        this.barrackInspectionQuestionId = barrackInspectionQuestionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }
}
