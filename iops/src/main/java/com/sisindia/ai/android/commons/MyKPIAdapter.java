package com.sisindia.ai.android.commons;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 5/31/2018.
 */

public class MyKPIAdapter extends RecyclerView.Adapter<MyKPIAdapter.MyKPIViewHolder> {

    private Context context = null;
    private LayoutInflater layoutInflater = null;

    String[] kpiID = {"KPI000011", "KPI000012", "KPI000013", "KPI000014", "KPI000015", "KPI000016", "KPI000017", "KPI000018"};
    String[] kpiName = {"Rota Compliance", "Short Billing", "Deduction control", "Collection vs target (Current Month Billing)",
            "Over time (OT Control)", "Disbandment Control", "Recruitment", "Annual KIT Replacement"};
    String[] kpiDesc = {"100%", "100%", "0.25% of billing under control", "100%", "2.50%", "0.25%", "10 Per Month", "100%"};
    String[] kpiWeight = {"20", "10", "15", "15", "10", "20", "5", "5"};
    String[] kpiCalcBasis = {"From iOPS", "Monthly actual billing/monthly contracted billing", "Actual deduction/ Billing",
            "Average Collection % as per ERP", "ERP Generated", "Calculated as 1-disbandment's percentage based on revenue",
            "As per registration form data", "From iOPS"};

    public MyKPIAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyKPIViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View kpiView = layoutInflater.inflate(R.layout.my_kpi_row, parent, false);
        return new MyKPIViewHolder(kpiView);
    }

    @Override
    public void onBindViewHolder(MyKPIViewHolder holder, int position) {

        holder.kpiID.setText(kpiID[position]);
        holder.kpiName.setText(kpiName[position]);
        holder.kpiDescription.setText(kpiDesc[position]);
        holder.kpiWeightage.setText(kpiWeight[position]);
        holder.kpiCalcBasis.setText(kpiCalcBasis[position]);
    }

    @Override
    public int getItemCount() {
        return kpiID.length;
    }

    public class MyKPIViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.kpiID)
        CustomFontTextview kpiID;
        @Bind(R.id.kpiName)
        CustomFontTextview kpiName;
        @Bind(R.id.kpiDescription)
        CustomFontTextview kpiDescription;
        @Bind(R.id.kpiWeightage)
        CustomFontTextview kpiWeightage;
        @Bind(R.id.kpiCalcBasis)
        CustomFontTextview kpiCalcBasis;

        public MyKPIViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
