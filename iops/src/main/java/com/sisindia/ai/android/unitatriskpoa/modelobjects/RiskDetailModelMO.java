package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 14/7/16.
 */

public class RiskDetailModelMO implements Serializable {

    @SerializedName("UnitRiskId")
    @Expose
    private Integer unitRiskId;
    @SerializedName("CurrentMonth")
    @Expose
    private Integer currentMonth;
    @SerializedName("CurrentYear")
    @Expose
    private Integer currentYear;
    @SerializedName("UnitName")
    @Expose
    private String unitName;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("CompletionDate")
    @Expose
    private String completionDate;
    @SerializedName("TotalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("UnitRiskStatusId")
    @Expose
    private Integer unitRiskStatusId;
    @SerializedName("ActionPlans")
    @Expose
    private List<ActionPlanMO> actionPlans = new ArrayList<ActionPlanMO>();


    private Integer id;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    /**
     *
     * @return
     * The unitRiskId
     */
    public Integer getUnitRiskId() {
        return unitRiskId;
    }

    /**
     *
     * @param unitRiskId
     * The UnitRiskId
     */
    public void setUnitRiskId(Integer unitRiskId) {
        this.unitRiskId = unitRiskId;
    }

    /**
     *
     * @return
     * The currentMonth
     */
    public Integer getCurrentMonth() {
        return currentMonth;
    }

    /**
     *
     * @param currentMonth
     * The CurrentMonth
     */
    public void setCurrentMonth(Integer currentMonth) {
        this.currentMonth = currentMonth;
    }

    /**
     *
     * @return
     * The currentYear
     */
    public Integer getCurrentYear() {
        return currentYear;
    }

    /**
     *
     * @param currentYear
     * The CurrentYear
     */
    public void setCurrentYear(Integer currentYear) {
        this.currentYear = currentYear;
    }

    /**
     *
     * @return
     * The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     *
     * @param unitName
     * The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     *
     * @return
     * The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     *
     * @param unitId
     * The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     *
     * @return
     * The completionDate
     */
    public String getCompletionDate() {
        return completionDate;
    }

    /**
     *
     * @param completionDate
     * The CompletionDate
     */
    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    /**
     *
     * @return
     * The totalItems
     */
    public Integer getTotalItems() {
        return totalItems;
    }

    /**
     *
     * @param totalItems
     * The TotalItems
     */
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    /**
     *
     * @return
     * The unitRiskStatusId
     */
    public Integer getUnitRiskStatusId() {
        return unitRiskStatusId;
    }

    /**
     *
     * @param unitRiskStatusId
     * The UnitRiskStatusId
     */
    public void setUnitRiskStatusId(Integer unitRiskStatusId) {
        this.unitRiskStatusId = unitRiskStatusId;
    }

    /**
     *
     * @return
     * The actionPlans
     */
    public List<ActionPlanMO> getActionPlans() {
        return actionPlans;
    }

    /**
     *
     * @param actionPlans
     * The ActionPlans
     */
    public void setActionPlans(List<ActionPlanMO> actionPlans) {
        this.actionPlans = actionPlans;
    }}
