package com.sisindia.ai.android.myperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ashu Rajput on 4/23/2018.
 */

public class TimeLineBaseMO {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;

    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;

    @SerializedName("Data")
    @Expose
    private List<TimeLineModel> timeLineModel;

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<TimeLineModel> getTimeLineModel() {
        return timeLineModel;
    }

    public void setTimeLineModel(List<TimeLineModel> timeLineModel) {
        this.timeLineModel = timeLineModel;
    }
}
