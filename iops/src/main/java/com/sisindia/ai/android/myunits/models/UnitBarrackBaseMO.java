package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.myunits.models.UnitBarrack;
import com.sisindia.ai.android.myunits.models.UnitShiftRank;

import java.util.List;

/**
 * Created by compass on 2/22/2017.
 */

public class UnitBarrackBaseMO {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private List<UnitBarrack> unitBarrackList;

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<UnitBarrack> getUnitBarrackList() {
        return unitBarrackList;
    }

    public void setUnitBarrackList(List<UnitBarrack> unitBarrackList) {
        this.unitBarrackList = unitBarrackList;
    }
}
