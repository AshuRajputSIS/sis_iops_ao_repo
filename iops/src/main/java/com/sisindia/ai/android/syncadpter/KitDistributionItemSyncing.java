package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsOR;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemUpdateStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 06-10-2016.
 */

public class KitDistributionItemSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<KitItemRequestMO> kitItemRequestMOList;
    private KitItemUpdateStatementDB kitItemUpdateStatementDB;
    private ArrayList<KitDeliveredItemsIR> kitDeliveredItemsIRList;
    private HashMap<Integer, Integer> kitDistributionIdList;
    private int kitDistributionCount;

    KitDistributionItemSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        kitItemUpdateStatementDB = IOPSApplication.getInstance().getKitItemUpdateStatementDB();
        this.kitItemRequestMOList = new ArrayList<>();
        annualKitItemsSyncing();
    }

    private void annualKitItemsSyncing() {
        kitDeliveredItemsIRList  = getAnnualKitItemList();
        if(kitDeliveredItemsIRList != null && kitDeliveredItemsIRList.size() != 0) {
            for (KitDeliveredItemsIR kitDeliveredItemsIR : kitDeliveredItemsIRList) {
                annualKitItemsRequestApiCall(kitDeliveredItemsIR,false);
            }
        } else {
            dependentFailureSync();
            Timber.d(Constants.TAG_SYNCADAPTER+"AnnualkitItemsSynciing -->nothing to sync ");

        }
    }


    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(dependentSyncsStatus != null && dependentSyncsStatus.kitDistributionId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {
        if(dependentSyncsStatus != null){
            if(dependentSyncsStatus.isMetaDataSynced !=  Constants.DEFAULT_SYNC_COUNT) {
                MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
                if(dependentSyncsStatus.kitDistributionId != 0) {
                    metaDataSyncing.setKitDistributionId(dependentSyncsStatus.kitDistributionId);
                }
            }
            else {
                if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                    ImageSyncing imageSyncing = new ImageSyncing(mContext);
                    if(dependentSyncsStatus.kitDistributionId != 0) {
                        imageSyncing.setKitDistributionId(dependentSyncsStatus.kitDistributionId);
                    }
                }
            }
        }
    }

    private ArrayList<KitDeliveredItemsIR> getAnnualKitItemList(){
        ArrayList<KitDeliveredItemsIR> kitDeliveredItemsIRList = new ArrayList<>();
        kitDistributionIdList = new HashMap<>();

        String kitItemQuery = "select * from " +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE + " where " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0 + " and " +
                TableNameAndColumnStatement.IS_UNPAID + " = " + 0 + " and " +
                TableNameAndColumnStatement.NON_ISSUE_REASON_ID + " = " + 0 + " and " +
                TableNameAndColumnStatement.ISSUED_DATE + " is not null ";
        Timber.d("AnnualkitItemQuery   %s", kitItemQuery);
        Cursor cursor;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(kitItemQuery, null);
            int kitDistributionId;

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        KitDeliveredItemsIR kitDeliveredItemsIR = new KitDeliveredItemsIR();

                        kitDeliveredItemsIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID)));
                        kitDeliveredItemsIR.setKitDistributionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID)));
                         kitDistributionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID));
                         if(kitDistributionIdList.get(kitDistributionId) == null){
                             kitDistributionIdList.put(kitDistributionId,1);
                         }
                         else{
                            int  kitDistributionCount = kitDistributionIdList.get(kitDistributionId);
                             kitDistributionCount = kitDistributionCount +1;
                             kitDistributionIdList.put(kitDistributionId,kitDistributionCount);
                         }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUED)) == 1){
                            kitDeliveredItemsIR.setIsIssued(true);
                        }
                        else{
                            kitDeliveredItemsIR.setIsIssued(false);
                        }

                        kitDeliveredItemsIR.setNonIssueReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.NON_ISSUE_REASON_ID)));
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNPAID)) == 1){
                            kitDeliveredItemsIR.setIsUnPaid(true);
                        }
                        else{
                            kitDeliveredItemsIR.setIsUnPaid(false);
                        }

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_ID)) == 0){
                            kitDeliveredItemsIR.setKitApparelSizeId(null);
                        }
                        else{
                            kitDeliveredItemsIR.setKitApparelSizeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_ID)));
                        }

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)) == 0){
                            kitDeliveredItemsIR.setKitItemId(null);
                        }
                        else{
                            kitDeliveredItemsIR.setKitItemId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)));
                        }

                        kitDeliveredItemsIR.setIssuedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_DATE)));
                        kitDeliveredItemsIR.setQuantity(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.QUANTITY)));
                        kitDeliveredItemsIRList.add(kitDeliveredItemsIR);

                        Timber.d(Constants.TAG_SYNCADAPTER+"Annual kitItemJosnData %s", new Gson().toJson(kitDeliveredItemsIR));
                    } while (cursor.moveToNext());
                }
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", kitItemRequestMOList);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return kitDeliveredItemsIRList;
    }

    private synchronized void annualKitItemsRequestApiCall(final KitDeliveredItemsIR kitDeliveredItemsIR,
                                                           final boolean isNonIssueUpdate) {
    
        sisClient.getApi().syncKitDeliveredItemsUpdate(kitDeliveredItemsIR, new Callback<KitDeliveredItemsOR>() {

            @Override
            public void success(KitDeliveredItemsOR kitDeliveredItemsOR, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"kitItemRequest response  %s", kitDeliveredItemsOR);

                if(kitDeliveredItemsOR != null ){
                    switch (kitDeliveredItemsOR.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            if(kitDeliveredItemsOR.getKitDeliveredItemsData() != null){
                                kitItemUpdateStatementDB.updateAnnualKitDistributionId(
                                        kitDeliveredItemsIR.getId());
                                updateStatus(kitDeliveredItemsOR);
                            }

                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER+"AnnualkitItemsSynciing count   %s", "count: ");
                Util.printRetorfitError("AnnualkitItemsSynciing", error);
            }
        });
    }

    private void updateStatus(KitDeliveredItemsOR kitDeliveredItemsOR) {
        kitDistributionCount = kitDistributionCount + 1;

        int count = kitDistributionIdList.
                get(kitDeliveredItemsOR.getKitDeliveredItemsData()
                        .getKitDistributionId());
        if(count != 0){
            count = count - 1;
        }

        kitDistributionIdList.put(kitDeliveredItemsOR.getKitDeliveredItemsData()
                .getKitDistributionId(),count);

        if(0 == kitDistributionIdList.
                get(kitDeliveredItemsOR.getKitDeliveredItemsData()
                        .getKitDistributionId())){
            DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
            dependentSyncsStatus.kitDistributionId = kitDeliveredItemsOR
                    .getKitDeliveredItemsData()
                    .getKitDistributionId();
            dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
            dependentSyncsStatus.isUnitEquipmentSynced = Constants.NEG_DEFAULT_COUNT;
            insertDepedentSyncData(dependentSyncsStatus);
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            metaDataSyncing.setKitDistributionId( kitDeliveredItemsOR
                    .getKitDeliveredItemsData().getKitDistributionId());
        }
    }

    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.kitDistributionId,
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ID,true)){
            dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
        }
    }
}