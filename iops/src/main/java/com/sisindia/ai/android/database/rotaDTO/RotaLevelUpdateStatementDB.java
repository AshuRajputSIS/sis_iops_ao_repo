package com.sisindia.ai.android.database.rotaDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;
import com.sisindia.ai.android.utils.logger.MyLogger;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 27/6/16.
 */

public class RotaLevelUpdateStatementDB extends SISAITrackingDB {

    public RotaLevelUpdateStatementDB(Context context) {
        super(context);
    }

    public void updateUnitEmpActualStrength(List<EmployeeAuthorizedStrengthMO> employeeAuthorizedStrengthMOList,
                                            boolean fromDayCheckFlow) {

        SQLiteDatabase sqlite = null;


        for (EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO :
                employeeAuthorizedStrengthMOList) {


            String updateQuery;
            if (fromDayCheckFlow) {
                updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                        " SET " + TableNameAndColumnStatement.ACTUAL + "='" + employeeAuthorizedStrengthMO.getActual() +
                        "' ," + TableNameAndColumnStatement.TASK_ID + "='" + employeeAuthorizedStrengthMO.getUnitTakID() +
                        "' ," + TableNameAndColumnStatement.CREATED_DATE_TIME + "='" + employeeAuthorizedStrengthMO.getCreatedDateTime() +
                        "' ," + TableNameAndColumnStatement.IS_SYNCED + "='" + 0 +
                        "' WHERE " +
                        TableNameAndColumnStatement.UNIT_ID + "='" + employeeAuthorizedStrengthMO.getUnitId() + "' and "
                        + TableNameAndColumnStatement.RANK_ID + "='" + employeeAuthorizedStrengthMO.getRankId() + "'";
            } else {
                updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                        " SET " + TableNameAndColumnStatement.ACTUAL + "='" + employeeAuthorizedStrengthMO.getActual() +
                        "' ," + TableNameAndColumnStatement.TASK_ID + "='" + employeeAuthorizedStrengthMO.getUnitTakID() +
                        "' ," + TableNameAndColumnStatement.LEAVE_COUNT + "='" + employeeAuthorizedStrengthMO.getLeaveCount() +
                        "' ," + TableNameAndColumnStatement.CREATED_DATE_TIME + "='" + employeeAuthorizedStrengthMO.getCreatedDateTime() +
                        "' ," + TableNameAndColumnStatement.IS_SYNCED + "='" + 0 +
                        "' WHERE " +
                        TableNameAndColumnStatement.BARRACK_ID + "='" + employeeAuthorizedStrengthMO.getBarrackId() + "'";
            }


            Timber.d("updateUnitEmpActualStrength  %s", updateQuery);
            Cursor cursor = null;
            try {
                sqlite = this.getWritableDatabase();
                cursor = sqlite.rawQuery(updateQuery, null);
                Timber.d("cursor detail object  %s", cursor.getCount());
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }

        }


    }


    /**
     * @param taskId
     * @param taskResult
     * @param
     */
    public void updateRotaTask(int taskId, String taskResult, int taskStatusId, String actualEndDateTime, String remarks, String geoLocation) {

        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.TASK_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT, taskResult);
            contentValues.put(TableNameAndColumnStatement.TASK_STATUS_ID, taskStatusId);
            contentValues.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME, actualEndDateTime);
            contentValues.put(TableNameAndColumnStatement.DESCRIPTIONS, remarks);
            contentValues.put(TableNameAndColumnStatement.IS_SYNCED, 0);
            contentValues.put(TableNameAndColumnStatement.IS_NEW, 1);
            contentValues.put(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION, geoLocation);
            contentValues.put(TableNameAndColumnStatement.IS_TASK_STARTED, RotaStartTaskEnum.InActive.getValue());
            sqlite.update(TableNameAndColumnStatement.TASK_TABLE, contentValues, whereClause, new String[]{" " + taskId});
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }


    /**
     * Update Actual Start date time column in task table
     *
     * @param taskId
     * @param currentDate
     */

    public void updateAcutualStartDateTime(int taskId, String currentDate, int taskStatusId) {

        SQLiteDatabase sqlite = null;
        String updateQuery = " UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME + " = " + "'" +
                currentDate + "'" + " , " +
                TableNameAndColumnStatement.TASK_STATUS_ID + " = " + "'" + taskStatusId + "' ," +
                TableNameAndColumnStatement.IS_TASK_STARTED + " = " + "'" + taskStatusId + "'" +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + " = " + taskId;
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            exception.printStackTrace();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {

                sqlite.close();
            }
        }
    }

    public void updateIsTaskStarted(int id, int isTaskStarted) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.IS_TASK_STARTED + "= '" + isTaskStarted + "' " +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER + "UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

}
