package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 12-05-2016.
 */
public class LastCheckInspectionComplaintsMO {

    private String natureOfComplaint;
    private String unitContactPerson;
    private String complaintClosedDate;
    private String assignedTo;
    private String complaintStatus;
    private String complaintMode;
    private String causeOfComplaint;
    private String createdDateTime;
    private String assignedName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    private  String unitName;

    public String getUnitContactPerson() {
        return unitContactPerson;
    }

    public void setUnitContactPerson(String unitContactPerson) {
        this.unitContactPerson = unitContactPerson;
    }


    public String getComplaintClosedDate() {
        return complaintClosedDate;
    }

    public void setComplaintClosedDate(String complaintClosedDate) {
        this.complaintClosedDate = complaintClosedDate;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }


    public String getNatureOfComplaint() {
        return natureOfComplaint;
    }

    public void setNatureOfComplaint(String natureOfComplaint) {
        this.natureOfComplaint = natureOfComplaint;
    }

    public String getComplaintMode() {
        return complaintMode;
    }

    public void setComplaintMode(String complaintMode) {
        this.complaintMode = complaintMode;
    }

    public String getCauseOfComplaint() {
        return causeOfComplaint;
    }

    public void setCauseOfComplaint(String causeOfComplaint) {
        this.causeOfComplaint = causeOfComplaint;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getAssignedName() {
        return assignedName;
    }

    public void setAssignedName(String assignedName) {
        this.assignedName = assignedName;
    }
}
