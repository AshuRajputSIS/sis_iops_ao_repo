package com.sisindia.ai.android.rota;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.help.VideosMetadataMo;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 18-07-2016.
 */
public class SelectReasonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ROW_REASON = 2;
    private static final int TYPE_ROW_VIDEO = 3;
    private ArrayList<Object> selectReasonList;
    private  Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;

    public SelectReasonAdapter(Context context) {
        this.context = context;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setReasonDataList(ArrayList<Object> selectReasonList) {
        this.selectReasonList = selectReasonList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;
        if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.header_row, parent, false);
            recycleViewHolder = new HeaderViewHolder(itemView);
        } else if (viewType == TYPE_ROW_REASON) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_addtask_row, parent, false);
            recycleViewHolder = new SelectReasonsViewHolder(itemView);
        } else if (viewType == TYPE_ROW_VIDEO) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_addtask_row, parent, false);
            recycleViewHolder = new SelectReasonsViewHolder(itemView);
        }
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = selectReasonList.get(position);
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                if (object instanceof String) {
                    headerViewHolder.headerTextview.setText((String) selectReasonList.get(position));
                }
                break;
            case TYPE_ROW_REASON:
                SelectReasonsViewHolder selectReasonsViewHolder = (SelectReasonsViewHolder) holder;
                if (object instanceof LookupModel) {
                    selectReasonsViewHolder.checkingTextview.setText(((LookupModel) selectReasonList.get(position)).getName());
                    selectReasonsViewHolder.typeIcon.setVisibility(View.GONE);
                    selectReasonsViewHolder.nextIcon.setVisibility(View.GONE);
                }
            case TYPE_ROW_VIDEO:
                SelectReasonsViewHolder videoViewHolder = (SelectReasonsViewHolder) holder;
                if (object instanceof VideosMetadataMo) {
                    videoViewHolder.checkingTextview.setText(((VideosMetadataMo) object).getFileName());
                    videoViewHolder.typeIcon.setVisibility(View.GONE);
                    videoViewHolder.nextIcon.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0) {
            if(getObject(position) instanceof  LookupModel)
                return TYPE_ROW_REASON;
            else
                return TYPE_ROW_VIDEO;
        } else {
            return TYPE_HEADER;
        }
    }

    @Override
    public int getItemCount() {
        int count = selectReasonList == null ? 0 : selectReasonList.size();
        return count;
    }

    private Object getObject(int position) {
        return selectReasonList.get(position);
    }

    public class SelectReasonsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView checkingTextview;
        ImageView typeIcon,nextIcon;
        public SelectReasonsViewHolder(View view) {
            super(view);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkType);
            this.nextIcon = (ImageView) view.findViewById(R.id.nextarrow);
            this.typeIcon =  (ImageView) view.findViewById(R.id.checkTypeImage);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v,getLayoutPosition());
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTextview;
        public HeaderViewHolder(View view) {
            super(view);
            this.headerTextview = (CustomFontTextview) view.findViewById(R.id.recycle_header);
        }
    }
}

