package com.sisindia.ai.android.database.scheduleDelete;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import timber.log.Timber;

/**
 * Created by compass on 6/25/2017.
 */

public class ScheduleDelete extends SISAITrackingDB {
    SQLiteDatabase sqLiteDatabase;
    private static final String TAG = ScheduleDelete.class.getSimpleName();

    public ScheduleDelete(Context context) {
        super(context);
    }

    public void setSqLiteDatabase() {
        sqLiteDatabase = getWritableDatabase();
    }

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }

    public boolean deleteRecordsByDateRange(String tableName, String columnName,
                                            String startDate) {
        String sqlSupportedDateFormat = "";
        if (TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE.equalsIgnoreCase(tableName)
                || TableNameAndColumnStatement.MY_PERFORMANCE_TABLE.equalsIgnoreCase(tableName)) {
            sqlSupportedDateFormat = Util.getSqlSupportedDateFormat(columnName);

        } else {
            sqlSupportedDateFormat = Util.getSqlSupportedDateTimeFormat(columnName);
        }

        String rawQuery = " delete  from " + tableName + " where datetime('" + sqlSupportedDateFormat +
                "') < datetime('" + startDate + " 00:00:00')";

        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }

    public boolean deleteIssueOrImprovementPlanRecords(String tableName, String columnName,
                                                       String col2,
                                                       String startDate,
                                                       int status) {

        String sqlSupportedDateTimeFormat = Util.getSqlSupportedDateTimeFormat(columnName);


        String rawQuery = " delete  from   " + tableName + "  where datetime('"
                + sqlSupportedDateTimeFormat
                + "') < datetime('" + startDate + " 00:00:00')"
                + " and " + TableNameAndColumnStatement.STATUS_ID + " = "
                + status;
        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }


    public boolean deleteRotaRelatedTables() {
        boolean isDeletedSuccessfully = false;
        synchronized (sqLiteDatabase) {
            try {

                sqLiteDatabase.beginTransaction();
                StringBuilder roatIds = IOPSApplication.getInstance()
                        .getRotaLevelSelectionInstance()
                        .getRotaIds(new DateTimeFormatConversionUtil()
                                .getDateBeforeSpecificMonths(3));
                StringBuilder taskIds = IOPSApplication.getInstance()
                        .getRotaLevelSelectionInstance()
                        .getTaskIds(roatIds.toString());
                deleteRotaTables(TableNameAndColumnStatement.ROTA_ID,
                        TableNameAndColumnStatement.ROTA_TABLE, roatIds);
                deleteRotaTables(TableNameAndColumnStatement.ROTA_ID,
                        TableNameAndColumnStatement.ROTA_TASK_TABLE, roatIds);
                deleteRotaTables(TableNameAndColumnStatement.TASK_ID,
                        TableNameAndColumnStatement.TASK_TABLE, taskIds);
                isDeletedSuccessfully = true;
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isDeletedSuccessfully = false;
            }
            return isDeletedSuccessfully;
        }
    }

    private boolean deleteRotaTables(String columnName, String tableName, StringBuilder ids) {
        String rawQuery = " delete from " + tableName + " where " + columnName + " in( "
                + ids + ")";
        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }

    public boolean deleteNotificationReceipt(String startDate, String columnName) {

        String sqlSupportedDateTimeFormat = Util.getSqlSupportedDateTimeFormat(columnName);
        String rawQuery = " delete from " + TableNameAndColumnStatement.NOTIFICATION_RECEIPT
                + " where  datetime('" + sqlSupportedDateTimeFormat
                + "') < datetime('" + startDate + " 00:00:00')"
                + " and " + TableNameAndColumnStatement.IS_SYNCED + " = 1 " +
                " and " + TableNameAndColumnStatement.IS_SUCCESSFUL + " = " + 1;
        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }

    public boolean deleteDependencyRecords(String startDate, String columnName) {
        String sqlSupportedDateTimeFormat = Util.getSqlSupportedDateTimeFormat(columnName);
        String rawQuery = " delete from " + TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW
                + " where  datetime('" + sqlSupportedDateTimeFormat
                + "') < datetime('" + startDate + " 00:00:00')"
                + " and " + TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED
                + " = 1";
        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }

    public boolean deleteRecruitmentRecords(String startDate, String columnName) {
        String sqlSupportedDateTimeFormat = Util.getSqlSupportedDateTimeFormat(columnName);


        String rawQuery = " delete from " + TableNameAndColumnStatement.RECRUITMENT_TABLE
                + " where  datetime('" + sqlSupportedDateTimeFormat
                + "') < datetime('" + startDate + " 00:00:00')"
                + " and " + TableNameAndColumnStatement.IS_SYNCED + " = 1";
        try {
            synchronized (sqLiteDatabase) {
                Cursor cursor = sqLiteDatabase.rawQuery(rawQuery, null);
                Timber.i(TAG, cursor.getCount());
                return true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        }
    }
}
