package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.KitDistributionItemOR;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga Prasad on 02-12-2016.
 */
public class FCMAkrDistributionData {

    @SerializedName("Id")
    private Integer id;

    @SerializedName("DistributionDate")
    private String distributionDate;

    @SerializedName("IssuingOfficerId")
    private Integer issuingOfficerId;

    @SerializedName("IssuingOfficerName")
    private String issuingOfficerName;

    @SerializedName("UnitId")
    private Integer unitId;

    @SerializedName("UnitName")
    private String unitName;

    @SerializedName("DistributionStatus")
    private Integer distributionStatus;

    @SerializedName("UnitTaskId")
    private Integer unitTaskId;

    @SerializedName("RecipientId")
    private Integer recipientId;

    @SerializedName("GuardNo")
    private String guardNo;

    @SerializedName("CreatedDateTime")
    private String createdDateTime;

    @SerializedName("UpdatedDateTime")
    private String updatedDateTime;

    @SerializedName("KitTypeId")
    private Integer kitTypeId;

    @SerializedName("BranchId")
    private Integer branchId;

    @SerializedName("GuardName")
    private String guardName;

    @SerializedName("KitDistributionItem")
    private List<KitDistributionItemOR> kitDistributionItem = new ArrayList<KitDistributionItemOR>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The distributionDate
     */
    public String getDistributionDate() {
        return distributionDate;
    }

    /**
     * @param distributionDate The DistributionDate
     */
    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }

    /**
     * @return The issuingOfficerId
     */
    public Integer getIssuingOfficerId() {
        return issuingOfficerId;
    }

    /**
     * @param issuingOfficerId The IssuingOfficerId
     */
    public void setIssuingOfficerId(Integer issuingOfficerId) {
        this.issuingOfficerId = issuingOfficerId;
    }

    /**
     * @return The issuingOfficerName
     */
    public String getIssuingOfficerName() {
        return issuingOfficerName;
    }

    /**
     * @param issuingOfficerName The IssuingOfficerName
     */
    public void setIssuingOfficerName(String issuingOfficerName) {
        this.issuingOfficerName = issuingOfficerName;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * @param unitName The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * @return The distributionStatus
     */
    public Integer getDistributionStatus() {
        return distributionStatus;
    }

    /**
     * @param distributionStatus The DistributionStatus
     */
    public void setDistributionStatus(Integer distributionStatus) {
        this.distributionStatus = distributionStatus;
    }

    /**
     * @return The unitTaskId
     */
    public Object getUnitTaskId() {
        return unitTaskId;
    }

    /**
     * @param unitTaskId The UnitTaskId
     */
    public void setUnitTaskId(Integer unitTaskId) {
        this.unitTaskId = unitTaskId;
    }

    /**
     * @return The recipientId
     */
    public Integer getRecipientId() {
        return recipientId;
    }

    /**
     * @param recipientId The RecipientId
     */
    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    /**
     * @return The guardNo
     */
    public String getGuardNo() {
        return guardNo;
    }

    /**
     * @param guardNo The GuardNo
     */
    public void setGuardNo(String guardNo) {
        this.guardNo = guardNo;
    }

    /**
     * @return The createdDateTime
     */
    public String getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * @param createdDateTime The CreatedDateTime
     */
    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * @return The updatedDateTime
     */
    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    /**
     * @param updatedDateTime The UpdatedDateTime
     */
    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    /**
     * @return The kitTypeId
     */
    public Integer getKitTypeId() {
        return kitTypeId;
    }

    /**
     * @param kitTypeId The KitTypeId
     */
    public void setKitTypeId(Integer kitTypeId) {
        this.kitTypeId = kitTypeId;
    }

    /**
     * @return The branchId
     */
    public Integer getBranchId() {
        return branchId;
    }

    /**
     * @param branchId The BranchId
     */
    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    /**
     * @return The kitDistributionItem
     */
    public List<KitDistributionItemOR> getKitDistributionItem() {
        return kitDistributionItem;
    }

    /**
     * @param kitDistributionItem The KitDistributionItem
     */
    public void setKitDistributionItem(List<KitDistributionItemOR> kitDistributionItem) {
        this.kitDistributionItem = kitDistributionItem;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }
}

