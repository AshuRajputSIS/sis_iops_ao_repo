package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BaseAkrSync {

    @SerializedName("StatusCode")
    private Integer statusCode;
    @SerializedName("StatusMessage")
    private String statusMessage;
    @SerializedName("Data")
    public List<FCMAkrDistributionData> fcmAkrDistributionData;

    public List<FCMAkrDistributionData> getFcmAkrDistributionData() {
        return fcmAkrDistributionData;
    }

    public void setFcmAkrDistributionData(List<FCMAkrDistributionData> fcmAkrDistributionData) {
        this.fcmAkrDistributionData = fcmAkrDistributionData;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "BaseAkrSync{" +
                "fcmAkrDistributionData=" + fcmAkrDistributionData +
                ", statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}