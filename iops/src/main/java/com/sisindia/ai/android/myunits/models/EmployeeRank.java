package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 23/6/16.
 */

public class EmployeeRank implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("EmployeeId")
    private int EmployeeId;
    @SerializedName("RankId")
    private int RankId;
    @SerializedName("BranchId")
    private int BranchId;
    @SerializedName("IsActive")
    private boolean IsActive;
    @SerializedName("RankName")
    private String RankName;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getRankName() {
        return RankName;
    }

    public void setRankName(String rankName) {
        RankName = rankName;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public int getRankId() {
        return RankId;
    }

    public void setRankId(int rankId) {
        RankId = rankId;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int branchId) {
        BranchId = branchId;
    }
}
