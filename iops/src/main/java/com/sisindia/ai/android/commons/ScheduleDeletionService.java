package com.sisindia.ai.android.commons;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.ScheduleDeleteTimes;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.scheduleDelete.ScheduleDelete;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

/**
 * Created by compass on 7/3/2017.
 */

public class ScheduleDeletionService extends IntentService {
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private static final String TAG = ScheduleSyncJobService.class.getSimpleName();

    public ScheduleDeletionService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        ScheduleDelete scheduleDelete = new ScheduleDelete(ScheduleDeletionService.this);
        scheduleDelete.setSqLiteDatabase();
        SQLiteDatabase sqLiteDatabase = scheduleDelete.getSqLiteDatabase();
        appPreferences = new AppPreferences(ScheduleDeletionService.this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        if (sqLiteDatabase != null) {
            try {
                sqLiteDatabase.beginTransaction();
                deleteRecordsFromTables(scheduleDelete);
                sqLiteDatabase.setTransactionSuccessful();
                ScheduleDeleteTimes scheduleDeleteTimes = new ScheduleDeleteTimes();
                scheduleDeleteTimes.deletedDateTime = dateTimeFormatConversionUtil
                        .getCurrentDateTime();
                scheduleDeleteTimes.nextDeletedDateTime = dateTimeFormatConversionUtil.
                        getFutureDateAfterSpecificMonths(3);
                appPreferences.setScheduleDeleteTimes(scheduleDeleteTimes);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sqLiteDatabase.endTransaction();
                if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
                    sqLiteDatabase.close();
                }
            }
        }
    }

    private void deleteRecordsFromTables(ScheduleDelete scheduleDelete) {
        scheduleDelete.deleteRecruitmentRecords(new DateTimeFormatConversionUtil()
                .getDateBeforeSpecificMonths(3), TableNameAndColumnStatement.CREATED_DATE_TIME);
        scheduleDelete.deleteDependencyRecords(new DateTimeFormatConversionUtil()
                .getDateBeforeSpecificMonths(3), TableNameAndColumnStatement.CREATED_DATE_TIME);
        scheduleDelete.deleteNotificationReceipt(new DateTimeFormatConversionUtil()
                .getDateBeforeSpecificMonths(3), TableNameAndColumnStatement.CREATED_DATE_TIME);
        scheduleDelete.deleteIssueOrImprovementPlanRecords(TableNameAndColumnStatement
                        .ISSUES_TABLE,
                TableNameAndColumnStatement.CREATED_DATE_TIME, TableNameAndColumnStatement
                        .ISSUE_STATUS_ID,
                new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3), 4);
        scheduleDelete.deleteIssueOrImprovementPlanRecords(TableNameAndColumnStatement.
                        IMPROVEMENT_PLAN_TABLE, TableNameAndColumnStatement.CREATED_DATE_TIME,
                TableNameAndColumnStatement.STATUS_ID,
                new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3), 3);
        scheduleDelete.deleteRecordsByDateRange(TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE,
                TableNameAndColumnStatement.DUTY_ON_DATE_TIME, new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3));
        scheduleDelete.deleteRecordsByDateRange(TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE,
                TableNameAndColumnStatement.REQUESTED_ON, new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3));
        scheduleDelete.deleteRecordsByDateRange(TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE,
                TableNameAndColumnStatement.CREATED_DATE, new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3));
        scheduleDelete.deleteRecordsByDateRange(TableNameAndColumnStatement.MY_PERFORMANCE_TABLE,
                TableNameAndColumnStatement.DATE, new DateTimeFormatConversionUtil()
                        .getDateBeforeSpecificMonths(3));
        scheduleDelete.deleteRotaRelatedTables();
    }
}
