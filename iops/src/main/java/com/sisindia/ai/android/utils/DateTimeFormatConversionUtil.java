package com.sisindia.ai.android.utils;

import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.rota.nightcheck.NighCheckValidation;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by shankar on 20/5/16.
 */
public class DateTimeFormatConversionUtil {

    private Calendar calendarInstance;
    private Date todaysDate;
    private SimpleDateFormat NORMAL_DATE_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private SimpleDateFormat NORMAL_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat YYYYMMDD_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat NORMAL_DATE_TIME_AMPM_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
    private SimpleDateFormat NORMAL_TIME_AMPM_FORMAT = new SimpleDateFormat("hh:mm aa");
    private SimpleDateFormat DAY_MONTH_DATE_YEAR_FORMAT = new SimpleDateFormat("EEE, MMM dd yyyy");
    private SimpleDateFormat DAY_MONTH_DATE_FORMAT = new SimpleDateFormat("EEEE, MMM dd");
    private SimpleDateFormat TWENTY_FOUR_HOUR_TIME_FORMATE = new SimpleDateFormat("HH:mm:ss");
    private SimpleDateFormat DATE_MONTH_NAME_YEAR_FORMAT = new SimpleDateFormat("dd MMM yyyy");
    private SimpleDateFormat SLASH_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat HOURS_MIN_FORMAT = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat DAYFORMAT = new SimpleDateFormat("yyyyMMdd");
    private static final long MILLI_SECONDS_PER_DAY = 86400000;
    private int timeDifferenceInSeconds = 1800;

    public String getFirstDay(Date d, Calendar calendarInstance) throws Exception {
        calendarInstance = getCalendarInstance();
        calendarInstance.setTime(d);
        calendarInstance.set(Calendar.DAY_OF_MONTH, 1);
        Date date = calendarInstance.getTime();
        return DAYFORMAT.format(date);
    }

    public String getLastDay(Date d, Calendar calendarInstance) throws Exception {
        calendarInstance = getCalendarInstance();
        calendarInstance.setTime(d);
        calendarInstance.set(Calendar.DAY_OF_MONTH, calendarInstance.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date date = calendarInstance.getTime();

        return DAYFORMAT.format(date);
    }

    public int lastDayOfMonth(Calendar currentCalendar) {
        int lastDate = 0;
        try {
            lastDate = Integer.parseInt(getLastDay(currentCalendar.getTime(), currentCalendar));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastDate;
    }

    public int firstDayOfMonth(Calendar currentCalendar) {

        int firstDate = 0;
        try {
            firstDate = Integer.parseInt(getFirstDay(currentCalendar.getTime(), currentCalendar));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return firstDate;

    }

    public Calendar getCalendarInstance() {
        return calendarInstance = Calendar.getInstance();
    }

    public String getDateFromCalender(Calendar calendarInstance) {
        //calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringDate = NORMAL_DATE_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    public String getCurrentDate() {
        calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringDate = NORMAL_DATE_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    public String onCalendarDateSelected(Calendar calendar) {
        todaysDate = calendar.getTime();
        String todayStringDate = NORMAL_DATE_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    public String getCurrentTime() {
        calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringTime = HOURS_MIN_FORMAT.format(todaysDate);
        return todayStringTime;
    }

    public String getDate(Date date) {
        String todayStringDate = NORMAL_DATE_FORMAT.format(date);
        return todayStringDate;
    }


    public String getCurrentDateTime() {
        calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringDate = NORMAL_DATE_TIME_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    public String getSpecificDateTimeInMillis() {
        calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringDate = NORMAL_DATE_TIME_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    public String getCurrentDateTime_billcollaction() {
        calendarInstance = Calendar.getInstance();
        todaysDate = calendarInstance.getTime();
        String todayStringDate = DAY_MONTH_DATE_YEAR_FORMAT.format(todaysDate);
        return todayStringDate;
    }

    /**
     * Date Convert from
     * Ex:  23-05-2016  to Monday,May 23
     *
     * @param date
     * @return String
     */

    public String convertDateToDayMonthDateFormat(String date) {
        String dayMonthDateFormat = "";
        try {
            dayMonthDateFormat = DAY_MONTH_DATE_FORMAT.format(NORMAL_DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayMonthDateFormat;
    }

    /**
     * Date Convert from
     * Ex:  23-05-2016  to Mon,May 23 2016
     *
     * @param date
     * @return String
     */

    public String convertDateToDayMonthDateYearFormat(String date) {
        String dayMonthDateYearFormat = "";
        try {
            dayMonthDateYearFormat = DAY_MONTH_DATE_YEAR_FORMAT.format(NORMAL_DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayMonthDateYearFormat;
    }

    /**
     * Date Convert from
     * Ex:  23-05-2016  to  23 May 2016
     *
     * @param time
     * @return String
     */
    public String convertionDateToDateMonthNameYearFormat(String time) {

        String dateMonthNameYearFormat = "";

        try {
            dateMonthNameYearFormat = DATE_MONTH_NAME_YEAR_FORMAT.format(NORMAL_DATE_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateMonthNameYearFormat;
    }


    /**
     * Date Convert from
     * Ex:  23-05-2016 22:11:36 to  23 May 2016
     *
     * @param time
     * @return String
     */
    public String convertionDateTimeToDateMonthNameYearFormat(String time) {

        String dateMonthNameYearFormat = "";

        try {
            dateMonthNameYearFormat = DATE_MONTH_NAME_YEAR_FORMAT.format(NORMAL_DATE_TIME_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateMonthNameYearFormat;
    }

    /**
     * Date Convert from
     * Ex:  23-05-2016 13:24:12  to  23-05-2016 1:24 PM / 1:24 PM
     *
     * @param time
     * @return String
     */


    public String convertionDateTimeTo12HourTime(String time) {

        String dateMonthNameYearFormat = "";
        String changedDateMonthNameYearFormat = "";

        try {
            dateMonthNameYearFormat = NORMAL_TIME_AMPM_FORMAT.format(NORMAL_DATE_TIME_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateMonthNameYearFormat;
    }

    /**
     * Date Convert from
     * Ex:  Mon,Feb 23 2016  to  23-05-2016
     *
     * @param date
     * @return String
     */

    public String convertDayMonthDateYearToDateFormat(String date) {
        String normalDateFormat = "";

        try {
            normalDateFormat = NORMAL_DATE_FORMAT.format(DAY_MONTH_DATE_YEAR_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return normalDateFormat;
    }

    /**
     * Date Convert from
     * Ex:  23-05-2016 00:00:00  to  23-05-2016
     *
     * @param date
     * @return String
     */

    public String convertDateTimeToDate(String date) {
        String normalDateFormat = "";
        try {
            if (!date.isEmpty())
                normalDateFormat = NORMAL_DATE_FORMAT.format(NORMAL_DATE_TIME_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return normalDateFormat;
    }

    /**
     * TIme conversion from 24 hour to 12 hours
     *
     * @param time
     * @return
     */
    public String timeConverstionFrom24To12(String time) {

        String convertedTime = "";

        try {
            convertedTime = NORMAL_TIME_AMPM_FORMAT.format(TWENTY_FOUR_HOUR_TIME_FORMATE.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return convertedTime;
    }

    /**
     * TIme conversion from 12 hour to 24 hours
     *
     * @param time
     * @return
     */
    public String timeConverstionFrom12To24(String time) {

        String convertedTime = "";
        String changedTime = "";

//        if (time.contains("AM")) {
//            changedTime = time.replace("AM", "a.m.");
//        } else if (time.contains("PM")) {
//            changedTime = time.replace("PM", "p.m.");
//        }
        changedTime = time;

        try {
            convertedTime = TWENTY_FOUR_HOUR_TIME_FORMATE.format(NORMAL_TIME_AMPM_FORMAT.parse(changedTime.trim()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedTime.trim();
    }


    public String changeFormatOfampmToAMPM(String time) {
        String formatAmPm = "";

        if (time.contains("a.m.")) {
            formatAmPm = time.replace("a.m.", "AM");
        } else if (time.contains("p.m.")) {
            formatAmPm = time.replace("p.m.", "PM");
        } else {
            formatAmPm = time;
        }
        return formatAmPm;
    }

    public String changeFormatOfAMPMToampm(String time) {
        String formatAmPm = "";

        if (time.contains("AM")) {
            formatAmPm = time.replace("AM", "a.m.");
        } else if (time.contains("PM")) {
            formatAmPm = time.replace("PM", "p.m.");
        } else {
            formatAmPm = time;
        }
        return formatAmPm;
    }

    public String timeConverstionToSlashDateFormat(String time) {

        String convertedTime = "";
        String changedTime = "";
        if (time.contains("AM")) {
            changedTime = time.replace("AM", "a.m.");
        } else if (time.contains("PM")) {
            changedTime = time.replace("PM", "p.m.");
        }


        try {
            convertedTime = SLASH_DATE_FORMAT.format(NORMAL_DATE_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedTime;
    }

    /**
     * Examples
     * Current Date      |     DateToCompare
     * 23-03-2018               25-03-2018     return -1
     * 23-03-2018               21-03-2018     return 1
     * 23-03-2018               23-03-2018     retrun 0
     */
    public int dateComparison(String todaysDateStr, String dateToCompareStr) {

        Date todaysDate = null;
        Date dateToCompare = null;
        try {
            todaysDate = NORMAL_DATE_FORMAT.parse(todaysDateStr);
            dateToCompare = NORMAL_DATE_FORMAT.parse(dateToCompareStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return todaysDate.compareTo(dateToCompare);
    }

    public int yymmddDateComparator(String selectedTaskStartDate, String dateToCompareStr) {

        Date todaysDate = null;
        Date dateToCompare = null;
        try {
            Date newDate = NORMAL_DATE_FORMAT.parse(selectedTaskStartDate);
            String formattedSelectedDate = YYYYMMDD_FORMAT.format(newDate);
            todaysDate = YYYYMMDD_FORMAT.parse(formattedSelectedDate);
            dateToCompare = YYYYMMDD_FORMAT.parse(dateToCompareStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return todaysDate.compareTo(dateToCompare);
    }

/*    public int dateComparisonWithWeek(String todaysDateStr, String minDate) {

        Date todaysDate = null;
        Date dateToCompare = null;
        try {
            todaysDate = NORMAL_DATE_FORMAT.parse(todaysDateStr);
            dateToCompare = NORMAL_DATE_FORMAT.parse(dateToCompareStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return todaysDate.compareTo(dateToCompare);
    }*/

    // 0  --> both times are equal
    // 1  --> timeToCompareStr  is past time
    // -1 --> timeToCompareStr is future time
    public long timeChecker(String currentTime, String timeToCompareStr) {


        long currentTimeInMills = Long.parseLong((currentTime.split(":")[0]).trim()) * 60 * 60 * 1000 + Long.parseLong((currentTime.split(":")[1]).trim()) * 60 * 1000;
        long timeToCompareStrMills = Long.parseLong((timeToCompareStr.split(":")[0]).trim()) * 60 * 60 * 1000 + Long.parseLong((timeToCompareStr.split(":")[1]).trim()) * 60 * 1000;

        return currentTimeInMills - timeToCompareStrMills;
    }

    public int dateTimeChecker(String stringStartTime, String stringEndTime) {

        Date startDateTime = null;
        Date endDateTime = null;


        try {
            startDateTime = NORMAL_DATE_TIME_FORMAT.parse(stringStartTime);
            endDateTime = NORMAL_DATE_TIME_FORMAT.parse(stringEndTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDateTime.compareTo(endDateTime);

    }

    public Date convertStringToDate(String date) {


        Date convertedDate = new Date();
        try {
            convertedDate = NORMAL_DATE_TIME_FORMAT.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedDate;
    }

    public String timeConverstionFrom24To12WithoutSeconds(String time) {

        String convertedTime = "";

        try {
            convertedTime = NORMAL_TIME_AMPM_FORMAT.format(HOURS_MIN_FORMAT.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return convertedTime;
    }

    /**
     * Convert the day to current month and current year date
     * used for displaying the wage date .
     *
     * @param dd
     * @return
     */
    public String dayToCompleteDate(int dd) {
        Calendar c = Calendar.getInstance();
        int day = dd;
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        return day + "-" + month + "-" + year;
    }

    public String addDaysToCurrentDate(int day) {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, day); // Adding number of days to calender
        String output = NORMAL_DATE_FORMAT.format(c.getTime());
        return output;


    }

    public long convertStringDateToMilliseconds(String dateTime) {

        long milliseconds = 0;
        Date convertedDate = new Date();
        try {
            convertedDate = NORMAL_DATE_TIME_FORMAT.parse(dateTime);
            milliseconds = convertedDate.getTime();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            milliseconds = 0;

        }

        return milliseconds;
    }

    public Date convertStringToDateFormat(String date) {


        Date convertedDate = new Date();
        try {
            convertedDate = NORMAL_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedDate;
    }

    public String convertDayMonthDateYearToDateTimeFormat(String date) {
        String normalDateFormat = "";

        try {
            normalDateFormat = NORMAL_DATE_TIME_FORMAT.format(DAY_MONTH_DATE_YEAR_FORMAT.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return normalDateFormat;
    }


    public String getPreviousDateFromCurrentDate(int noOfDays) {
        String normalDateFormat = "";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -noOfDays);

        try {
            normalDateFormat = NORMAL_DATE_TIME_FORMAT.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return normalDateFormat;
    }

    public boolean nightCheckValidation(String startDateTime, String endDateTime) throws ParseException {
        String minStartTime = " 23:00:00 ";
        String maxEndTime = " 05:30:00 ";
        String defaultEndTime = " 23:59:59 ";
        String defaultStartTime = " 00:00:00 ";
        boolean isValidDateTime = false;

        String startDate = startDateTime.split(" ")[0];
        String endDate = endDateTime.split(" ")[0];

        String startTime = startDateTime.split(" ")[1];
        String endTime = endDateTime.split(" ")[1];
        long startTimeInMills = Long.parseLong(startTime.split(":")[0].trim()) * 60 * 60 * 1000 + Long.parseLong(startTime.split(":")[1].trim()) * 60 * 1000;// TWENTY_FOUR_HOUR_TIME_FORMATE.parse(startTime).getTime();
        long endTimeInMills = Long.parseLong(endTime.split(":")[0].trim()) * 60 * 60 * 1000 + Long.parseLong(endTime.split(":")[1].trim()) * 60 * 1000;
        long defaultStartTimeInMills = Long.parseLong(defaultStartTime.split(":")[0].trim()) * 60 * 60 * 1000 + Long.parseLong(defaultStartTime.split(":")[1].trim()) * 60 * 1000;
        long defaultEndTimeInMills = Long.parseLong(defaultEndTime.split(":")[0].trim()) * 60 * 60 * 1000 + Long.parseLong(defaultEndTime.split(":")[1].trim()) * 60 * 1000 +
                Long.parseLong(defaultEndTime.split(":")[2].trim()) * 1000;
        long minStartTimeInMillis = Long.parseLong((minStartTime.split(":")[0]).trim()) * 60 * 60 * 1000 + Long.parseLong((minStartTime.split(":")[1]).trim()) * 60 * 1000;

        long startDateTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(startDateTime).getTime();
        long endDateTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(endDateTime).getTime();
        long startMinDateTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(startDate + minStartTime).getTime();
        long endMaxDateTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(endDate + maxEndTime).getTime();
        if (startDate.equals(endDate)) {
            if (defaultEndTimeInMills > startTimeInMills && defaultEndTimeInMills > endTimeInMills
                    && minStartTimeInMillis <= startTimeInMills && minStartTimeInMillis < endTimeInMills) {
                //previous day
                if (NORMAL_DATE_TIME_FORMAT.parse(startDate + defaultEndTime).getTime() >= endDateTimeInMills &&
                        startMinDateTimeInMills <= startDateTimeInMills) {
                    isValidDateTime = true;
                } else {
                    isValidDateTime = false;
                }
            } else {
                //next day
                if (NORMAL_DATE_TIME_FORMAT.parse(startDate + defaultStartTime).getTime() <= startDateTimeInMills &&
                        endMaxDateTimeInMills >= endDateTimeInMills) {
                    isValidDateTime = true;
                } else {
                    isValidDateTime = false;
                }
            }
        } else {
            if (startMinDateTimeInMills <= startDateTimeInMills &&
                    endMaxDateTimeInMills >= endDateTimeInMills) {
                isValidDateTime = true;
            } else {
                isValidDateTime = false;
            }
        }
        return isValidDateTime;
    }

    public NighCheckValidation nightCheckValidationBeforeStart(String startDate, String endDate, String startTime, String endTime) throws ParseException {

        String minStartTime = " 23:00:00 ";
        String defaultEndTime = " 23:59:59 ";
        String maxEndTime = " 05:30:00 ";
        NighCheckValidation nighCheckValidation = new NighCheckValidation();
        startTime = timeConverstionFrom12To24(startTime);
        endTime = timeConverstionFrom12To24(endTime);

        try {
            long defaultEndTimeInMills = getTimeInMillis(defaultEndTime);
            long startTimeInMills = getTimeInMillis(startTime);
            long endTimeInMills = getTimeInMillis(endTime);
            long minStartTimeInMillis = getTimeInMillis(minStartTime);
            if (startDate.equals(endDate)) {
                // previous day
                // pre date + 11:00 PM - (pre date+1) + 5:30 AM
                if (defaultEndTimeInMills > startTimeInMills && defaultEndTimeInMills > endTimeInMills
                        && minStartTimeInMillis <= startTimeInMills && minStartTimeInMillis < endTimeInMills) {
                    long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(NORMAL_DATE_FORMAT.parse(startDate));
                    calendar.add(Calendar.DATE, 1);

                    long validNightCheckStartTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(startDate + minStartTime).getTime();
                    long validNightCheckEndTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(NORMAL_DATE_FORMAT.format(calendar.getTime()) + maxEndTime).getTime();
                    if (validNightCheckStartTimeInMills <= currentDateTimeInMillis) {
                        nighCheckValidation.isBeforeMinTime = false;
                        if (currentDateTimeInMillis <= validNightCheckEndTimeInMills) {
                            nighCheckValidation.isAftermaxTime = false;
                        }
                    } else {
                        nighCheckValidation.isAftermaxTime = false;
                    }
                    return nighCheckValidation;
                } else {

                    //next day
                    // (next date - 1) + 11:00 PM - next date + 5:30 AM
                    long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(NORMAL_DATE_FORMAT.parse(startDate));
                    calendar.add(Calendar.DATE, -1);

                    long validNightCheckStartTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(NORMAL_DATE_FORMAT.format(calendar.getTime()) + minStartTime).getTime();
                    long validNightCheckEndTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(endDate + maxEndTime).getTime();

                    if (validNightCheckStartTimeInMills <= currentDateTimeInMillis) {
                        nighCheckValidation.isBeforeMinTime = false;
                        if (currentDateTimeInMillis <= validNightCheckEndTimeInMills) {
                            nighCheckValidation.isAftermaxTime = false;
                        }
                    } else {
                        nighCheckValidation.isAftermaxTime = false;
                    }
                    return nighCheckValidation;
                }

            } else {
                //In different dates
                // pre date + 11:00 PM - next date + 5:00 AM
                long currentDateTimeInMillis = Calendar.getInstance().getTimeInMillis();
                long validNightCheckStartTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(startDate + minStartTime).getTime();
                long validNightCheckEndTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(endDate + maxEndTime).getTime();

                if (validNightCheckStartTimeInMills <= currentDateTimeInMillis) {
                    nighCheckValidation.isBeforeMinTime = false;
                    if (currentDateTimeInMillis <= validNightCheckEndTimeInMills) {
                        nighCheckValidation.isAftermaxTime = false;
                    }
                } else {
                    nighCheckValidation.isAftermaxTime = false;
                }
                return nighCheckValidation;
            }
        } catch (NumberFormatException nfe) {
            Crashlytics.logException(nfe);
        } catch (ParseException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
        return nighCheckValidation;
    }

    public long getTimeInMillis(String inputTime) throws NumberFormatException {

        long millis = 0;

        if (!TextUtils.isEmpty(inputTime)) {
            String[] time = inputTime.split(":");
            if (time != null) {
                if (time.length == 2) {
                    millis = Long.parseLong(inputTime.split(":")[0].trim()) * 60 * 60 * 1000
                            + Long.parseLong(inputTime.split(":")[1].trim()) * 60 * 1000;

                } else if (time.length == 3) {
                    millis = Long.parseLong(inputTime.split(":")[0].trim()) * 60 * 60 * 1000
                            + Long.parseLong(inputTime.split(":")[1].trim()) * 60 * 1000
                            + Long.parseLong(inputTime.split(":")[2].trim()) * 1000;
                }
            }
        }
        return millis;
    }

    public boolean validateTimeDiffBetweenStartAndEndDateTime(String startDateTime, String endDateTime) {
        long timeDiff = 8 * 60 * 60 * 1000;
        try {
            long startTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(startDateTime).getTime();
            long endTimeInMills = NORMAL_DATE_TIME_FORMAT.parse(endDateTime).getTime();
            if (endTimeInMills - startTimeInMills > timeDiff) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public long getTimeInMillisForNumberOfDays(int count) {
        return count * MILLI_SECONDS_PER_DAY;
    }

    public boolean scheduleAlarmRangeChecking(String currentDate, long currentTimeInMills)
            throws ParseException {
        long morningMinTimeInMillis = NORMAL_DATE_TIME_FORMAT.parse(currentDate + Constants.MORNING_ALARM_SYNC_START_TIME).getTime();
        long morningMaxTimeInMillis = NORMAL_DATE_TIME_FORMAT.parse(currentDate + Constants.MORNING_ALARM_SYNC_END_TIME).getTime();
        long minTimeInMillis = NORMAL_DATE_TIME_FORMAT.parse(currentDate + Constants.ALARM_SYNC_START_TIME).getTime();
        long maxTimeInMillis = NORMAL_DATE_TIME_FORMAT.parse(currentDate + Constants.ALARM_SYNC_END_TIME).getTime();
        if ((morningMinTimeInMillis <= currentTimeInMills && morningMaxTimeInMillis >= currentTimeInMills) ||
                (minTimeInMillis <= currentTimeInMills && maxTimeInMillis >= currentTimeInMills)) {
            return true;
        }
        return false;
    }

    public long getSecondsForDateTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTimeInMillis() / 1000;
    }

    public long getSecondsForDateTime(String dateTime) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(NORMAL_DATE_TIME_FORMAT.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis() / 1000;
    }

    public boolean getTimeDifference(String startDateTime) {
        long currentTimeInSeconds = getSecondsForDateTime();
        long syncedDateTimeInSeconds = getSecondsForDateTime(startDateTime);
        return currentTimeInSeconds - syncedDateTimeInSeconds > timeDifferenceInSeconds;
    }

    public String getPreviousDate(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return NORMAL_DATE_FORMAT.format(calendar.getTime());
    }

    public String getPreviousDateYearFormat(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return YYYYMMDD_FORMAT.format(calendar.getTime());
    }

    public String getCurrentDateYearFormat() {
        Calendar calendar = Calendar.getInstance();
        return YYYYMMDD_FORMAT.format(calendar.getTime());
    }

    public String getYearMonthDateSqliteFormat(String date) {
        return YYYYMMDD_FORMAT.format(date);
    }

    public String getDateBeforeSpecificMonths(int noOfMonths) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -noOfMonths);
        return YYYYMMDD_FORMAT.format(calendar.getTime());
    }

    public int getWeekOfTheYear(String date) {
        DateTime dateTime = null;
        if (TextUtils.isEmpty(date)) {
            dateTime = new DateTime();
        } else {
            dateTime = DateTime.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy"));
        }

        return dateTime.getWeekOfWeekyear();
    }

    public String[] getMinMaxDateForLastAkr() {

        String[] minMaxDate = new String[2];
        DateTime requiredMonth;
        int month = DateTime.now().getMonthOfYear();
        int year = DateTime.now().getYear();
        int date = DateTime.now().getDayOfMonth();

        if (date < 12) {
            if (month == 1) {
                requiredMonth = new DateTime(year - 1, month + 11, 1, 0, 0);
            } else {
                requiredMonth = new DateTime(year, month - 1, 1, 0, 0);
            }
        } else {
            requiredMonth = new DateTime(year, month, 1, 0, 0);
        }
        minMaxDate[0] = requiredMonth.plusDays(11).toString("yyyy-MM-dd");
        minMaxDate[1] = requiredMonth.plusMonths(1).plusDays(10).toString("yyyy-MM-dd");
        return minMaxDate;
    }

    public String getNextDayDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(date));
            c.add(Calendar.DATE, 1);  // number of days to add
            date = sdf.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public boolean isTimeGreaterThan10() {
        try {
            int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (currentHour > 22) {
                return true;
            }

        } catch (Exception e) {
        }
        return true;
    }

    public String getFutureDateAfterSpecificMonths(int noOfMonths) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, noOfMonths);
        return YYYYMMDD_FORMAT.format(calendar.getTime());
    }

    private String convertStringDateToCalendar(String selectedDate, int noOfMonths) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(NORMAL_DATE_FORMAT.parse(selectedDate));
            cal.add(Calendar.MONTH, noOfMonths);
            return YYYYMMDD_FORMAT.format(cal.getTime());
        } catch (Exception e) {
        }
        return "";
    }

    private String convertStringNormalDateToCalendar(String selectedDate, int noOfMonths) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(NORMAL_DATE_FORMAT.parse(selectedDate));
            cal.add(Calendar.MONTH, noOfMonths);
            return NORMAL_DATE_FORMAT.format(cal.getTime());
        } catch (Exception e) {
        }
        return "";
    }

    public String getPreviousAndNextMonth24Or25Date(int month, String monthDay) {
        String nextMonth24Date = "";
        try {
            nextMonth24Date = getFutureDateAfterSpecificMonths(month).substring(0, 8) + monthDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nextMonth24Date;
    }

    public String getCurrentMonthDateFromDay(String monthDay) {
        String currentMonth24or25Date = "";
        try {
            currentMonth24or25Date = getFutureDateAfterSpecificMonths(0).substring(0, 8) + monthDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth24or25Date;
    }

    public String getPreOrNextMonth24Or25DateWRTSelectedDate(String selectedDate, int month, String monthDay) {
        String nextMonth24Date = "";
        try {
            nextMonth24Date = convertStringDateToCalendar(selectedDate, month).substring(0, 8) + monthDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nextMonth24Date;
    }

    public String getCurrentMonthDateFromDayWRTSelectedDate(String selectedDate, String monthDay) {
        String currentMonth24or25Date = "";
        try {
            currentMonth24or25Date = convertStringDateToCalendar(selectedDate, 0).substring(0, 8) + monthDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMonth24or25Date;
    }

    /**
     * Examples
     * Current Date      |     DateToCompare
     * 23-03-2018               25-03-2018     return -1
     * 23-03-2018               21-03-2018     return 1
     * 23-03-2018               23-03-2018     retrun 0
     */
    public int currentDateVs25thOfCurrentMonthDate(String todaysDateStr) {

        Date todaysDate = null;
        Date dateToCompare = null;
        try {
            todaysDate = NORMAL_DATE_FORMAT.parse(todaysDateStr);
            String currentMonth25Date = "25" + todaysDateStr.substring(2, todaysDateStr.length());
            dateToCompare = NORMAL_DATE_FORMAT.parse(currentMonth25Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return todaysDate.compareTo(dateToCompare);
    }

    public boolean isDateRangeBetween25To11(String selectedDate) {

        try {
            String selectedMonth25Date = "25" + selectedDate.substring(2, selectedDate.length());
//            String selectedMonth11Date = "11" + selectedDate.substring(2, selectedDate.length());
            String nextMonth11Date = "11" + convertStringNormalDateToCalendar(selectedDate, 1).substring(2, selectedDate.length());
            if (dateComparison(selectedDate, selectedMonth25Date) < 0) {
                Log.e("Date", "25Block");
                return false;
            } else if (dateComparison(selectedDate, selectedMonth25Date) >= 0) {
                Log.e("Date", "d");
                return true;
            } else if (dateComparison(selectedDate, nextMonth11Date) < 0) {
                Log.e("Date", "11Block");
                return true;
            } else {
                Log.e("Date", "Else false block");
                return false;
            }

        } catch (Exception e) {
        }
        return false;
    }

    public String convertHourMinToHourMinWithAMPM(String hourMin) {
        try {
            Date d = HOURS_MIN_FORMAT.parse(hourMin);
            return NORMAL_TIME_AMPM_FORMAT.format(d);
        } catch (Exception e) {
        }
        return "";
    }

}
