package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.UnitStrengthMO;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 03-04-2016.
 */
public class UnitDetailsStrengthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UnitStrengthMO> unitDetailsStrengthMOList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public UnitDetailsStrengthAdapter(Context context) {
        this.context = context;

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setStrengthDataList(List<UnitStrengthMO> strengthModelList) {
        this.unitDetailsStrengthMOList = strengthModelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.unitdetials_strengthtab_cardview, parent, false);
        RecyclerView.ViewHolder recycleViewHolder = new StrengthViewHolder(itemView);
        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        StrengthViewHolder strengthViewHolder = (StrengthViewHolder) holder;
        UnitStrengthMO unitStrengthMO = unitDetailsStrengthMOList.get(position);
        strengthViewHolder.unitdetailsStrengthtabSoTxt.setText(unitStrengthMO.getRankName());
        int deficiencyCount = unitStrengthMO.getAuthorised() - unitStrengthMO.getActual();

        if(deficiencyCount < 0 ){
            strengthViewHolder.unitdetailsStrengthtabDeficiencyCountTxt.setText(String.valueOf(0));
        }
        else{
            strengthViewHolder.unitdetailsStrengthtabDeficiencyCountTxt.setText(String.valueOf(deficiencyCount));
         }

        strengthViewHolder.unirsdetailsStrengthtabGreenbarCountTxt.setText(String.valueOf(unitStrengthMO.getActual()));
        strengthViewHolder.unirsdetailsStrengthtabBluebarCountTxt.setText(String.valueOf(unitStrengthMO.getAuthorised()));

        // setting blue graph height and width
        int actualStrengthHeight = unitStrengthMO.getActual();
        int authorizedStrengthHeight = unitStrengthMO.getAuthorised();

        if(authorizedStrengthHeight > actualStrengthHeight) {
            ViewGroup.LayoutParams greyBarLayoutParams = strengthViewHolder.unitdetailsStrengthtabBlueBargarphView.getLayoutParams();
            greyBarLayoutParams.width = Util.dpToPx(context.getResources().getInteger(R.integer.number_36));
            greyBarLayoutParams.height = Util.dpToPx(100);

            ViewGroup.LayoutParams greenBarLayoutParams = strengthViewHolder.unitdetailsStrengthtabGreenBargarphView.getLayoutParams();
            greenBarLayoutParams.width =  Util.dpToPx(context.getResources().getInteger(R.integer.number_36));
            if(authorizedStrengthHeight != 0) {
                int requiredHeight = (actualStrengthHeight * 100) / authorizedStrengthHeight;
                if(requiredHeight != 0 ){
                    greenBarLayoutParams.height = Util.dpToPx(requiredHeight);
                }
               else if(actualStrengthHeight!=0){
                    greenBarLayoutParams.height = Util.dpToPx(1);
                }

            }else {
                greenBarLayoutParams.height = Util.dpToPx(0);
            }
        }
        else {

            ViewGroup.LayoutParams greenBarLayoutParams = strengthViewHolder.unitdetailsStrengthtabGreenBargarphView.getLayoutParams();
            greenBarLayoutParams.width =  Util.dpToPx(context.getResources().getInteger(R.integer.number_36));
            greenBarLayoutParams.height = Util.dpToPx(100);

            ViewGroup.LayoutParams greyBarLayoutParams = strengthViewHolder.unitdetailsStrengthtabBlueBargarphView.getLayoutParams();
            greyBarLayoutParams.width = Util.dpToPx(context.getResources().getInteger(R.integer.number_36));
           if(actualStrengthHeight != 0) {
                int requiredHeight = (authorizedStrengthHeight * 100) / actualStrengthHeight;
                if(requiredHeight != 0 ){
                    greyBarLayoutParams.height = Util.dpToPx(requiredHeight);
                }
                else if(actualStrengthHeight!=0){
                    greyBarLayoutParams.height = Util.dpToPx(1);
                }

            }else {
                greyBarLayoutParams.height = Util.dpToPx(0);
            }


        }


    }


    @Override
    public int getItemCount() {

        int count = unitDetailsStrengthMOList == null ? 0 : unitDetailsStrengthMOList.size();

        return count;
    }

    private Object getObject(int position) {
        return unitDetailsStrengthMOList.get(position);
    }

    public class StrengthViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.unitdetials_strengthtab_cardview)
        CardView unitdetialsStrengthtabCardview;
        @Bind(R.id.unitdetails_strengthtab_so_txt)
        TextView unitdetailsStrengthtabSoTxt;
        @Bind(R.id.unitdetails_strengthtab_deficiency_count_txt)
        TextView unitdetailsStrengthtabDeficiencyCountTxt;
        @Bind(R.id.unitdetails_strengthtab_deficiency_label_txt)
        TextView unitdetailsStrengthtabDeficiencyLabelTxt;
        @Bind(R.id.unitdetails_strengthtab_green_circle_view)
        View unitdetailsStrengthtabGreenCircleView;
        @Bind(R.id.unitdetails_strengthtab_actual_label_txt)
        TextView unitdetailsStrengthtabActualLabelTxt;
        @Bind(R.id.unitdetails_strengthtab_blue_circle_view)
        View unitdetailsStrengthtabBlueCircleView;
        @Bind(R.id.unitdetails_generaltab_authorized_label_txt)
        TextView unitdetailsGeneraltabAuthorizedLabelTxt;

        @Bind(R.id.unitdetails_strengthtab_green_bargarph_view)
        View unitdetailsStrengthtabGreenBargarphView;
        @Bind(R.id.unitdetails_strengthtab_blue_bargarph_view)
        View unitdetailsStrengthtabBlueBargarphView;
        @Bind(R.id.unirsdetails_strengthtab_greenbar_count_txt)
        TextView unirsdetailsStrengthtabGreenbarCountTxt;
        @Bind(R.id.unirsdetails_strengthtab_bluebar_count_txt)
        TextView unirsdetailsStrengthtabBluebarCountTxt;


        public StrengthViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }


}

