package com.sisindia.ai.android.issues.grievances;

/**
 * Created by Durga Prasad on 13-05-2016.
 */
public class AdditonalRemarksMO {

    private String additionalRemarks;

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }
}
