package com.sisindia.ai.android.issues.models;

/**
 * Created by Shushrut on 19-05-2016.
 */
public class GrievanceMO {


    private int unitId;
    private int employeeId;
    private String additionalRemarks;
    private int issueId;
    private int barrackId;
    private int grievanceStatusId;
    private String createdAt;
    private int taskId;
    private String closedAt;
    private int grievanceNatureId;
    private String description;
    private int assignedTo;
    private int isNew;
    private int isSyned;
    private int employeeNumber;

    public int getIsSyned() {
        return isSyned;
    }

    public void setIsSyned(int syned) {
        isSyned = syned;
    }

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int aNew) {
        isNew = aNew;
    }


    public int getGrievanceNatureId() {
        return grievanceNatureId;
    }

    public void setGrievanceNatureId(int grievanceNatureId) {
        this.grievanceNatureId = grievanceNatureId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getBarrackId() {
        return barrackId;
    }

    public void setBarrackId(int barrackId) {
        this.barrackId = barrackId;
    }

    public int getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(int assignedTo) {
        this.assignedTo = assignedTo;
    }

    public int getGrievanceStatusId() {
        return grievanceStatusId;
    }

    public void setGrievanceStatusId(int grievanceStatusId) {
        this.grievanceStatusId = grievanceStatusId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }
}