package com.sisindia.ai.android.login;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.Config;
import com.sisindia.ai.android.fcmnotification.NotificationUtils;
import com.sisindia.ai.android.network.request.OtpInputRequestMO;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.TelephonyInfo;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.logger.MyLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import timber.log.Timber;


public class SendOTPActivity extends BaseActivity implements OnAcceptPermissionListener {


    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    @Bind(R.id.edit_registerNumber)
    EditText edittextRegisterNumber;
    @Bind(R.id.otpParentlayout)
    RelativeLayout otpParentlayout;
    private List<String> permissionsList = new ArrayList<String>();
    private String registerNumber;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Resources mResources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_send_otp);
        ButterKnife.bind(this);
        mResources = getResources();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
            grantPermission();
        }
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                }
                else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //  Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //txtMessage.setText(message);
                }
            }
        };

    }

    @Override
    protected void onStop() {
        super.onStop();
    }



    /**
     * api call to get the OTP
     *
     * @Query "phoneNumber" String phone,
     * GET METHOD
     */
    public void sendOtp(View view) {
        readingIMEINumber();
        String phoneNum = edittextRegisterNumber.getText().toString();
        if (Util.isValidPhoneNumber(phoneNum)) {
            Util.hideKeyboard(this);
            registerNumber = edittextRegisterNumber.getText().toString();
            appPreferences.saveMobileNumber(registerNumber);
            if (NetworkUtil.isConnected) {
                getOtpAPICall();
            } else {
                snackBarWithMesg(otpParentlayout,  mResources.getString(R.string.no_internet));
            }

        } else {
            Toast toast = Toast.makeText(this, mResources.getString(R.string.mobile_validation), Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private void getOtpAPICall() {
        showprogressbar();
        sisClient.getApi().sendOTP(createOTPRequest(),
                new Callback<CommonResponse>() {
                    @Override
                    public void success(CommonResponse commonResponse, Response response) {
                        hideprogressbar();
                        switch (commonResponse.getStatusCode()) {
                            case 200:
                                Intent loginIntent = new Intent(SendOTPActivity.this, LoginActivity.class);
                                loginIntent.putExtra(TableNameAndColumnStatement.registerNumber, registerNumber);
                                loginIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(loginIntent);
                                finish();
                                Toast.makeText(SendOTPActivity.this, commonResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
                                break;
                            case 412:
                                Toast.makeText(SendOTPActivity.this, commonResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
                                break;
                            case 500:
                                Toast.makeText(SendOTPActivity.this, commonResponse.getStatusMessage(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                    @Override
                    public void failure(RetrofitError error) {
                        hideprogressbar();
                        Crashlytics.logException(error);
                        try {
                            if (error != null) {
                                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                                Toast.makeText(SendOTPActivity.this, error.getResponse().getStatus(), Toast.LENGTH_SHORT).show();
                            } else {
                            }
                        } catch (Exception e) {

                        }
                    }
                });
    }

    private OtpInputRequestMO createOTPRequest() {
        OtpInputRequestMO otpRequest = new OtpInputRequestMO();
        otpRequest.setPhoneNumber(registerNumber);
        otpRequest.setIMEI1(appPreferences.getIMEIsim1().trim());
        otpRequest.setIMEI2(appPreferences.getIMEIsim2().trim());
        otpRequest.setAppVersion(String.valueOf(BuildConfig.VERSION_NAME));
        otpRequest.setDeviceToken(appPreferences.getFCMRegToken());
        otpRequest.setLanguageId(appPreferences.getLanguageId());
        otpRequest.setOptFlag(false);
        return otpRequest;
    }

    private void showprogressbar() {
        Util.showProgressBar(SendOTPActivity.this, R.string.loading_please_wait);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }

    private boolean checkPermission(String permision) {
        return ContextCompat.checkSelfPermission(SendOTPActivity.this,
                permision) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 124:
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.GET_ACCOUNTS,PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED)
                {
                    // All Permissions Granted
                    //Logger setup after obtaining the WRITE Permission
                    MyLogger.setup(this);
                } else {
                    // Permission Denied
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.READ_CONTACTS)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.WRITE_CONTACTS)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.READ_SMS)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.READ_PHONE_STATE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.RECORD_AUDIO)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, Manifest.permission.GET_ACCOUNTS))
                        {

                            PermissionDialogFragment permissionDialogFragment = new PermissionDialogFragment();
                            permissionDialogFragment.setMessage(mResources.getString(R.string.permission_str),false);
                            permissionDialogFragment.setHeaderTitle(getString(R.string.PermissionTitle));
                            FragmentManager fm = getSupportFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            ft.add(permissionDialogFragment, "permissionDialog").commitAllowingStateLoss();

                            return;
                        } else {

                        }
                    }
                }
                break;
            default: super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    public void grantPermission(){
        List<String> permissionsNeeded = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add(mResources.getString(R.string.permission_read_contact_str));
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
            permissionsNeeded.add(mResources.getString(R.string.permission_write_contact_str));
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add(mResources.getString(R.string.permission_write_external_str));
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add(mResources.getString(R.string.permission_read_external_str));
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add(mResources.getString(R.string.permission_read_sms_str));
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add(mResources.getString(R.string.permission_access_fine_location_str));
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add(mResources.getString(R.string.permission_access_coarse_location_str));
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add(mResources.getString(R.string.permission_access_camera_str));
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add(mResources.getString(R.string.permission_access_read_phone_str));
        if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add(mResources.getString(R.string.permission_record_audio_str));
        if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
            permissionsNeeded.add(mResources.getString(R.string.permission_get_accounts_str));
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = mResources.getString(R.string.permission_to_grant_access_str);
                for (int index = 0; index < permissionsNeeded.size(); index++) {
                    message = message + "\n" + permissionsNeeded.get(index);
                }
                PermissionDialogFragment permissionDialogFragment = new PermissionDialogFragment();
                permissionDialogFragment.setMessage(message,false);
                permissionDialogFragment.setHeaderTitle(getString(R.string.PermissionTitle));
                permissionDialogFragment.show(getSupportFragmentManager(),"");
                return;
            }
            ActivityCompat.requestPermissions(SendOTPActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(SendOTPActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(SendOTPActivity.this, permission))
                return false;
        }
        return true;
    }

    /**
     * Reading the IMEI number from the sim in Mashmallow
     * using the java relefection api oto read the IMEI number
     */
    private void readingIMEINumber() {
        if (TextUtils.isEmpty(appPreferences.getIMEIsim1()) || TextUtils.isEmpty(appPreferences.getIMEIsim2())) {
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
            appPreferences.saveIMEIsim1(telephonyInfo.getImsiSIM1());//"358953061524573"
            appPreferences.saveIMEIsim2(telephonyInfo.getImsiSIM2());//"358953061524581"
            Timber.d("ImsiSIM1   %s", telephonyInfo.getImsiSIM1());
            Timber.d("ImsiSIM2   %s", telephonyInfo.getImsiSIM2());
        }
    }

    @Override
    public void onPermissionsAccepted() {
        ActivityCompat.requestPermissions(SendOTPActivity.this,permissionsList.toArray(new String[permissionsList.size()]),
                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
    }
    @Override
    public void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
