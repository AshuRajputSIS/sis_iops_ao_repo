package com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.HolidayCalendar;

import java.util.List;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMHolidayModelInsertion extends SISAITrackingDB {

    private SQLiteDatabase sqlite=null;
    private boolean isInsertedSuccessfully;

    public FCMHolidayModelInsertion(Context context) {
        super(context);
    }
    public synchronized boolean insertHoliday(List<HolidayCalendar> holidaycalendarList) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                if(holidaycalendarList != null && holidaycalendarList.size()!=0) {
                    sqlite.beginTransaction();
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().
                            deleteTable(TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE,
                                    sqlite);
                    insertHolidayModel(holidaycalendarList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertHolidayModel(List<HolidayCalendar> holidayCalendarList) {

        for (HolidayCalendar holidayCalendar : holidayCalendarList) {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.HOLIDAY_ID, holidayCalendar.getId());
                values.put(TableNameAndColumnStatement.HOLIDAY_DATE, holidayCalendar.getHolidayDate());
                values.put(TableNameAndColumnStatement.HOLIDAY_DESCRIPTION, holidayCalendar.getHolidayDescription());
                values.put(TableNameAndColumnStatement.IS_REGIONAL_HOLIDAY,
                        holidayCalendar.getIsRegionalHoliday());
                values.put(TableNameAndColumnStatement.IS_ACTIVE, holidayCalendar.getIsActive());
                String[] createdStringDate = holidayCalendar.getHolidayDate().split(" ");
                values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                    sqlite.insertOrThrow(TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE, null, values);
                   }
    }
}
