package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 27/10/16.
 */

public class BaseRecruitmentMO {
    @SerializedName("CurrentMonth")
    @Expose
    private CurrentMonth currentMonth;
    @SerializedName("PreviousMonth")
    @Expose
    private PreviousMonth previousMonth;
    @SerializedName("CurrentYear")
    @Expose
    private CurrentYear currentYear;
    @SerializedName("TotalRecruites")
    @Expose
    private List<TotalRecruite> totalRecruites = new ArrayList<TotalRecruite>();

    /**
     *
     * @return
     * The currentMonth
     */
    public CurrentMonth getCurrentMonth() {
        return currentMonth;
    }

    /**
     *
     * @param currentMonth
     * The CurrentMonth
     */
    public void setCurrentMonth(CurrentMonth currentMonth) {
        this.currentMonth = currentMonth;
    }

    /**
     *
     * @return
     * The previousMonth
     */
    public PreviousMonth getPreviousMonth() {
        return previousMonth;
    }

    /**
     *
     * @param previousMonth
     * The PreviousMonth
     */
    public void setPreviousMonth(PreviousMonth previousMonth) {
        this.previousMonth = previousMonth;
    }

    /**
     *
     * @return
     * The currentYear
     */
    public CurrentYear getCurrentYear() {
        return currentYear;
    }

    /**
     *
     * @param currentYear
     * The CurrentYear
     */
    public void setCurrentYear(CurrentYear currentYear) {
        this.currentYear = currentYear;
    }

    /**
     *
     * @return
     * The totalRecruites
     */
    public List<TotalRecruite> getTotalRecruites() {
        return totalRecruites;
    }

    /**
     *
     * @param totalRecruites
     * The TotalRecruites
     */
    public void setTotalRecruites(List<TotalRecruite> totalRecruites) {
        this.totalRecruites = totalRecruites;
    }

    @Override
    public String toString() {
        return "BaseRecruitmentMO{" +
                "currentMonth=" + currentMonth +
                ", previousMonth=" + previousMonth +
                ", currentYear=" + currentYear +
                ", totalRecruites=" + totalRecruites +
                '}';
    }
}
