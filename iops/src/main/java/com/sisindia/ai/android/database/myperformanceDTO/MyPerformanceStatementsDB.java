package com.sisindia.ai.android.database.myperformanceDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.rota.DateRotaModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shruti on 19/7/16.
 */
public class MyPerformanceStatementsDB extends SISAITrackingDB {

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public MyPerformanceStatementsDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public void UpdateMYPerformanceTable(String status, String endTime, String taskId, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.MY_PERFORMANCE_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_STATUS + "='" + status + "'" +
                "," + TableNameAndColumnStatement.END_TIME + "='" + endTime + "'" +
                " WHERE " + TableNameAndColumnStatement.PERFORMANCE_TASK_ID + "='" + taskId +/*"' and " +
                TableNameAndColumnStatement.PERFORMANCE_ID) + "='" +id+*/"'";
        sqlite = this.getWritableDatabase();
        Cursor cursor = null;
        synchronized (sqlite) {
            try {
                cursor = sqlite.rawQuery(updateQuery, null);
                Timber.d("cursor detail object  %s", cursor.getCount());
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void insertIntoMyPerformanceTable(TimeLineModel timeLineModel) {
        if (timeLineModel.getTaskId().equalsIgnoreCase("null")) {
            if (timeLineModel.getActivity().equalsIgnoreCase("Duty on")) {
                if (checkDutyOnOffStatusByDate(dateTimeFormatConversionUtil.getCurrentDate(), timeLineModel.getActivity()) == 0) {
                    insertionIntoMyPerformanceTable(timeLineModel);
                }
            } else {
                if (checkDutyOnOffStatusByDate(dateTimeFormatConversionUtil.getCurrentDate(), timeLineModel.getActivity()) == 0) {
                    insertionIntoMyPerformanceTable(timeLineModel);
                } else {
                    updateDutyOffIntoMyPerformanceDB(timeLineModel);
                }
            }
        } else {
            int IsRecordExist = checkActivityAvailable(timeLineModel.getTaskId());

            if (IsRecordExist == 0) {
                insertionIntoMyPerformanceTable(timeLineModel);
            }
        }
    }

    private void updateDutyOffIntoMyPerformanceDB(TimeLineModel timeLineModel) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.MY_PERFORMANCE_TABLE +
                " SET " + TableNameAndColumnStatement.START_TIME + "='" + timeLineModel.getStartTime() + "'" +
                " WHERE " + TableNameAndColumnStatement.PERFORMANCE_ACTIVITY + "='" + timeLineModel.getActivity() + "'";
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
//            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

    }

    private void insertionIntoMyPerformanceTable(TimeLineModel timeLineModel) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();

                values.put(TableNameAndColumnStatement.PERFORMANCE_ACTIVITY, timeLineModel.getActivity());
                values.put(TableNameAndColumnStatement.PERFORMANCE_LOCATION, timeLineModel.getLocation());
                values.put(TableNameAndColumnStatement.PERFORMANCE_TASK_ID, timeLineModel.getTaskId());
                values.put(TableNameAndColumnStatement.PERFORMANCE_TASK_NAME, timeLineModel.getTaskName());
                values.put(TableNameAndColumnStatement.TASK_STATUS, timeLineModel.getStatus());
                values.put(TableNameAndColumnStatement.DATE, timeLineModel.getDate());
                values.put(TableNameAndColumnStatement.START_TIME, timeLineModel.getStartTime());
                values.put(TableNameAndColumnStatement.END_TIME, timeLineModel.getEndTime());

                sqlite.insert(TableNameAndColumnStatement.MY_PERFORMANCE_TABLE, null, values);
                sqlite.setTransactionSuccessful();

            } catch (Exception exception) {
//                Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    private int checkDutyOnOffStatusByDate(String currentDate, String activity) {

        String selectQuery = "select CASE WHEN count(*) >= 1 THEN 1 ELSE 0 END AS 'IsRecordExist' from " +
                TableNameAndColumnStatement.MY_PERFORMANCE_TABLE + " where " +
                TableNameAndColumnStatement.DATE + "='" + currentDate + "'" + " and " +
                TableNameAndColumnStatement.PERFORMANCE_ACTIVITY + "='" + activity + "'";

        Cursor totalCountCursor = null;
        int IsRecordExist = 0;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            totalCountCursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if (totalCountCursor != null && !totalCountCursor.isClosed()) {
                if (totalCountCursor.moveToFirst()) {
                    do {
                        IsRecordExist = totalCountCursor.getInt(totalCountCursor.getColumnIndex("IsRecordExist"));
                    } while (totalCountCursor.moveToNext());
                }
            }
        } catch (Exception exception) {
//            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return IsRecordExist;
    }

    private int checkActivityAvailable(String task_id) {
        String selectQuery = "select CASE WHEN count(*) >= 1 THEN 1 ELSE 0 END AS 'IsRecordExist' from " +
                TableNameAndColumnStatement.MY_PERFORMANCE_TABLE + " where " +
                TableNameAndColumnStatement.TASK_ID + "='" + task_id + "'";

        Cursor totalCountCursor = null;
        int IsRecordExist = 0;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            totalCountCursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if (totalCountCursor != null && !totalCountCursor.isClosed()) {
                if (totalCountCursor.moveToFirst()) {
                    do {
                        IsRecordExist = totalCountCursor.getInt(totalCountCursor.getColumnIndex("IsRecordExist"));
                    } while (totalCountCursor.moveToNext());
                }
            }
        } catch (Exception exception) {
//            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return IsRecordExist;
    }

    public List<TimeLineModel> getActivityTimeline(String date) {

        List<TimeLineModel> timeLineModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;

        String query = "select *" + " from " + TableNameAndColumnStatement.MY_PERFORMANCE_TABLE + " where " + TableNameAndColumnStatement.DATE + "=" + "'" +
                date + "' order by " + TableNameAndColumnStatement.START_TIME + " Asc";

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        TimeLineModel timeLineModel = new TimeLineModel();

                        timeLineModel.setActivity(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PERFORMANCE_ACTIVITY)));
                        timeLineModel.setLocation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PERFORMANCE_LOCATION)));
                        timeLineModel.setStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                        timeLineModel.setTaskId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PERFORMANCE_TASK_ID)));
                        timeLineModel.setTaskName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PERFORMANCE_TASK_NAME)));
                        timeLineModel.setStartTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.START_TIME)));
                        timeLineModel.setDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DATE)));
                        timeLineModel.setEndTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.END_TIME)));

                        timeLineModelList.add(timeLineModel);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
//            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return timeLineModelList;
    }

    public int getPerformanceWorkTime(String date) {

        SQLiteDatabase sqlite = null;
        int totalTimeMinutes = 0;

        String query = "select sum(cast(((case when (strftime('%s',substr(duty_off_date_time, 7, 4)||'-'||substr(duty_off_date_time, 4,2)||'-'||substr(duty_off_date_time, 1,2)" +
                "||' '||substr(duty_off_date_time, 12,100))) is null then strftime('%s',datetime('now', 'localtime')) else strftime('%s'," +
                "substr(duty_off_date_time, 7, 4)||'-'||substr(duty_off_date_time, 4,2)||'-'|| substr(duty_off_date_time, 1,2)" +
                "||' '||substr(duty_off_date_time, 12,100)) end - strftime('%s',substr(duty_on_date_time, 7, 4)||'-'||substr(duty_on_date_time, 4,2)||'-'||substr(duty_on_date_time, 1,2) " +
                "||' '|| substr(duty_on_date_time, 12,100)))) as int)/60) as Minutes  from duty_attendance where substr(duty_on_date_time, 0, 11)='" + date + "'";

//        Timber.d("getPerformanceWorkTime QUERY  %s", query);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        String totalMinutes = cursor.getString(cursor.getColumnIndex("Minutes"));
                        if (null != totalMinutes && !totalMinutes.isEmpty())
                            totalTimeMinutes = Integer.parseInt(totalMinutes);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return totalTimeMinutes;
    }

    public DateRotaModel getPerformanceTaskCount(String selectedDate) {

        SQLiteDatabase sqlite = null;
//        int[] taskCountDetails = new int[3];

        DateRotaModel dateRotaModel = new DateRotaModel();

        String query = "select count(1) TotalTaskAssigned ,Count(case when task_status_id in (3,4) then 1 else null end) TaskCompleted," +
                "Count(case when task_status_id in (3,4) and is_synced=1 then 1 else  null end) CompletedAndSynced from task where substr(estimated_execution_start_date_time, 0, 11)='" +
                 selectedDate + "' and Is_Auto in (0,1)";

        Timber.d("Total Task Counts query--   %s", query);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        String totalTaskAssigned = cursor.getString(cursor.getColumnIndex("TotalTaskAssigned"));
                        String taskCompleted = cursor.getString(cursor.getColumnIndex("TaskCompleted"));
                        String taskCompletedAndSynced = cursor.getString(cursor.getColumnIndex("CompletedAndSynced"));

                        if (null != totalTaskAssigned && !totalTaskAssigned.isEmpty())
                            dateRotaModel.setRotaTotalCount(Integer.parseInt(totalTaskAssigned));
                        else
                            dateRotaModel.setRotaTotalCount(0);

                        if (null != taskCompleted && !taskCompleted.isEmpty())
                            dateRotaModel.setRotaCompletedCount(Integer.parseInt(taskCompleted));
                        else
                            dateRotaModel.setRotaCompletedCount(0);

                        if (null != taskCompletedAndSynced && !taskCompletedAndSynced.isEmpty())
                            dateRotaModel.setRotaSyncedCount(Integer.parseInt(taskCompletedAndSynced));
                        else
                            dateRotaModel.setRotaSyncedCount(0);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dateRotaModel;
    }


   /* public DateRotaModel getPerformanceTaskAndComplianceCount(String date) {
        DateRotaModel dateRotaModel = new DateRotaModel();
        String currentDate = dateTimeFormatConversionUtil.getCurrentDate();
        int dayResult = dateTimeFormatConversionUtil.dateComparison(currentDate, date);
        int dateRotaWeek = dateTimeFormatConversionUtil.getWeekOfTheYear(date);
        String query = "";
        if (dayResult > 0) {
            query = " SELECT CompletedRTC + CompletedATC as CompletedTaskCount, TotalRTC + TotalATC as TotalTaskCount from (SELECT count(case when t.task_status_id in (3,4) " +
                    " then t.task_id end) as CompletedRTC, count(t.task_status_id) as TotalRTC from (SELECT rota_id from rota where rota_week = "
                    + dateRotaWeek + " order by 1 desc limit 1)r  inner join (select rota_id, rota_task_id, task_id from rota_task)rt on" +
                    " rt.rota_id= r.rota_id inner join (select task_id,  task_status_id from task where is_auto = 1  and substr(estimated_execution_start_date_time,0,11) = '" +
                    date + "' )t on t.task_id = rt.task_id)rota, (SELECT count(case when ad.task_status_id in (3,4) then ad.task_id end) as CompletedATC, " +
                    " count(ad.task_id) as TotalATC from (select task_id, task_status_id from task where is_auto = 0" +
                    " and substr(estimated_execution_start_date_time,0,11) = '" + date + "')ad)adhoc";

        } else {
            query = " SELECT CompletedRTC + CompletedATC as CompletedTaskCount, TotalRTC + TotalATC as TotalTaskCount from (SELECT count(case when t.task_status_id in (3,4) " +
                    "then t.task_id end) as CompletedRTC, count(t.task_status_id) as TotalRTC from (SELECT rota_id from rota where rota_week = "
                    + dateRotaWeek + " order by 1 desc limit 1)r inner join (select rota_id, rota_task_id, task_id from rota_task)rt on" +
                    " rt.rota_id= r.rota_id inner join (select task_id,  task_status_id from task where is_auto = 1 " +
                    " and substr(estimated_execution_start_date_time,0,11) = '" + date +
                    "' )t on t.task_id = rt.task_id)rota, (SELECT count(case when ad.task_status_id in (3,4) then ad.task_id end) as CompletedATC, " +
                    " count(ad.task_id) as TotalATC from (select task_id, task_status_id from task where is_auto = 0" +
                    " and substr(estimated_execution_start_date_time,0,11) = '" + date + "')ad)adhoc";
        }
        SQLiteDatabase sqlite = null;
        Timber.d("PerformanceTaskCompliance %s", query);

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        String rotaTotalCount = cursor.getString(cursor.getColumnIndex("TotalTaskCount"));
                        String rotaCompletedCount = cursor.getString(cursor.getColumnIndex("CompletedTaskCount"));
//                        String rotaSyncedCount = cursor.getString(cursor.getColumnIndex("SyncedTaskCount"));
                        String rotaSyncedCount = "0";
                        if (rotaTotalCount.equalsIgnoreCase("0")) {
                            dateRotaModel.setRotaTotalCount(0);
                            dateRotaModel.setRotaCompletedCount(0);
                            dateRotaModel.setRotaSyncedCount(0);
                        } else {
                            dateRotaModel.setRotaTotalCount(Integer.valueOf(rotaTotalCount));
                            dateRotaModel.setRotaCompletedCount(Integer.valueOf(rotaCompletedCount));
                            if (rotaSyncedCount.equals("0"))
                                dateRotaModel.setRotaSyncedCount(0);
                            else
                                dateRotaModel.setRotaSyncedCount(Integer.valueOf(rotaSyncedCount));
                        }
                    } while (cursor.moveToNext());
                }
            } else {
                dateRotaModel.setTaskStatus("No Rota Task");
                dateRotaModel.setRotaTotalCount(Integer.valueOf("0"));
                dateRotaModel.setRotaCompletedCount(Integer.valueOf("0"));
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dateRotaModel;
    }*/


}
