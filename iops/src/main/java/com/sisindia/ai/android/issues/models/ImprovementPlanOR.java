package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImprovementPlanOR {

    @SerializedName("ActionPlanId")
    @Expose
    private Integer actionPlanId;
    @SerializedName("AssignedToId")
    @Expose
    private Integer assignedToId;
    @SerializedName("AssigneeName")
    @Expose
    private String assigneeName;

    /**
     *
     * @return
     * The actionPlanId
     */
    public Integer getActionPlanId() {
        return actionPlanId;
    }

    /**
     *
     * @param actionPlanId
     * The ActionPlanId
     */
    public void setActionPlanId(Integer actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    /**
     *
     * @return
     * The assignedToId
     */
    public Integer getAssignedToId() {
        return assignedToId;
    }

    /**
     *
     * @param assignedToId
     * The AssignedToId
     */
    public void setAssignedToId(Integer assignedToId) {
        this.assignedToId = assignedToId;
    }

    /**
     *
     * @return
     * The assigneeName
     */
    public String getAssigneeName() {
        return assigneeName;
    }

    /**
     *
     * @param assigneeName
     * The AssigneeName
     */
    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    @Override
    public String toString() {
        return "ImprovementPlanOR{" +
                "actionPlanId=" + actionPlanId +
                ", assignedToId=" + assignedToId +
                ", assigneeName='" + assigneeName + '\'' +
                '}';
    }
}