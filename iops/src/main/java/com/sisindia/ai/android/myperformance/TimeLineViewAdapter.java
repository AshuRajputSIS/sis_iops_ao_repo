package com.sisindia.ai.android.myperformance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shaikatif on 5/7/16.
 */
public class TimeLineViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<TimeLineModel> timeLineModelList=new ArrayList<>();
    private Context context;

    public TimeLineViewAdapter(List<TimeLineModel> timeLineModelList,Context context){
        this.timeLineModelList=timeLineModelList;
        this.context=context;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.myperformance_timeline_view,parent,false);
        return new TimeLineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TimeLineViewHolder timeLineViewHolder=(TimeLineViewHolder)holder;
        if(position+1==timeLineModelList.size()){
            /**
             * This is to prevent the vertical Line for the last element
             */
            timeLineViewHolder.verticalLine.setVisibility(View.GONE);
        }else {
            /**
             * To increase vertical line length based on time difference.
             */
            checkDate(timeLineModelList.get(position).getStartTime(),timeLineModelList.get(position+1).getStartTime(),timeLineViewHolder.verticalLine);
        }
        timeLineViewHolder.timeTextView.setText(timeLineModelList.get(position).getStartTime());
        timeLineViewHolder.primaryLabel.setText(timeLineModelList.get(position).getActivity()+" "+timeLineModelList.get(position).getLocation());
        timeLineViewHolder.secondaryLabel.setText(timeLineModelList.get(position).getTaskName()+" "+timeLineModelList.get(position).getStatus());

    }

    private void checkDate(String currentTime, String nextTime, View verticalLine) {

        SimpleDateFormat currentTimeDateFormat=new SimpleDateFormat("h:mm");
        try {
            Date curDate=currentTimeDateFormat.parse(currentTime);
            Date nextDate=currentTimeDateFormat.parse(nextTime);
            /**
             * Subtract nextDate with current Date
             */
            long difference = nextDate.getTime() - curDate.getTime();
            int time= (int) (difference/1000);
            time=time/100;
            if(time>0) {
                verticalLine.requestLayout();
                int actualHeight=(int) context.getResources().getDimension(R.dimen.vertical_line_height);
                verticalLine.getLayoutParams().height =actualHeight+time;
            }
            } catch (ParseException e) {
            Toast.makeText(context,"Failed To convert dates",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return timeLineModelList.size();
    }


    public class TimeLineViewHolder extends RecyclerView.ViewHolder {
        TextView timeTextView;
        TextView primaryLabel;
        TextView secondaryLabel;
        View verticalLine;
        public TimeLineViewHolder(View itemView) {
            super(itemView);
            timeTextView= (TextView) itemView.findViewById(R.id.tv_time_label);
            primaryLabel= (TextView) itemView.findViewById(R.id.tv_primary_label);
            secondaryLabel= (TextView) itemView.findViewById(R.id.tv_secondary_label);
            verticalLine=itemView.findViewById(R.id.vertical_line);
        }
    }
}
