package com.sisindia.ai.android.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.GPSEnableDisableTracking;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

/**
 * Created by Saruchi on 12-11-2016.
 */

public class NavigationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "NavigationListAdapter";
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_DIVIDER = 3;
    private ArrayList<NavigationModel> menuList = new ArrayList<>();
    private AppPreferences mAppPreferences;
    private GPSListener lister;
    private boolean isfailure;
    private AppUserData appUserData;
    private Context context;
    private HeaderViewHolder headerViewHolder;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;

    final CompoundButton.OnCheckedChangeListener toggleButtonChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            // The user changed the button, do something
            if (isChecked) {
                headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.duty_started) + " ");
                mAppPreferences.setLastSeen(new Date().getTime());
                headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                mAppPreferences.saveDutyStatus(true);
                lister.enableGPSservice(true, true);
                startGPSEnableDisableService();

            } else {
                headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.last_seen) + " ");
                mAppPreferences.setLastSeen(new Date().getTime());
                headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                mAppPreferences.saveDutyStatus(false);
                lister.enableGPSservice(false, true);
                stopGPSEnableDisableService();
            }
        }
    };

    private void startGPSEnableDisableService() {
        if (BuildConfig.BUILD_VARIANT.equalsIgnoreCase("Prod")) {
            context.startService(new Intent(context, GPSEnableDisableTracking.class));
        }
    }

    private void stopGPSEnableDisableService() {
        if (BuildConfig.BUILD_VARIANT.equalsIgnoreCase("Prod")) {
            context.stopService(new Intent(context, GPSEnableDisableTracking.class));
        }
    }

    NavigationRecyclerAdapter(Context context, GPSListener lister, AppUserData appUserData) {
        this.context = context;
        mAppPreferences = new AppPreferences(context);
        this.lister = lister;
        this.appUserData = appUserData;
        isfailure = false;
        setupLanguage();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    void setMenuListData(ArrayList<NavigationModel> menuList) {
        this.menuList = menuList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header_main, parent, false);
                headerViewHolder = new HeaderViewHolder(headerView);
                return headerViewHolder;
            case TYPE_ITEM:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_row, parent, false);
                GenericViewHolder genericViewHolder = new GenericViewHolder(itemView);
                return genericViewHolder;

            case TYPE_FOOTER:
                View footerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_footer, parent, false);
                FooterViewHolder footerViewHolder = new FooterViewHolder(footerView);
                return footerViewHolder;

            case TYPE_DIVIDER:
                View dividerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_horizontal_dividerline, parent, false);
                DividerViewHolder dividerViewHolder = new DividerViewHolder(dividerView);
                return dividerViewHolder;
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            populateHeaderData((HeaderViewHolder) holder, position);
        } else if (holder instanceof GenericViewHolder) {
            populateMenuData((GenericViewHolder) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else if (position == 5) {
            return TYPE_DIVIDER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return menuList.size() + 2;
    }

    private void populateMenuData(final GenericViewHolder genericViewHolder, int position) {
        genericViewHolder.txtName.setText(menuList.get(position - 1).getTitle());
        genericViewHolder.menuIcon.setBackgroundResource(menuList.get(position - 1).getIcon());
        if (menuList.get(position - 1).getisNotificationVisible() && menuList.get(position - 1).getNotificationCount() != 0) {
            genericViewHolder.NotificationCount.setVisibility(View.VISIBLE);
            genericViewHolder.NotificationCount.setText(String.valueOf(menuList.get(position - 1).getNotificationCount()));
        } else {
            genericViewHolder.NotificationCount.setVisibility(View.INVISIBLE);
        }
    }

    private void populateHeaderData(final HeaderViewHolder headerViewHolder, int position) {
        headerViewHolder.userNameTxt.setText(mAppPreferences.getAppUser());
        headerViewHolder.mSwitch.setChecked(mAppPreferences.getDutyStatus());
        if (appUserData != null) {
            if (null != appUserData.getmImageUrl() && !TextUtils.isEmpty(appUserData.getmImageUrl())) {
                loadAiImage(appUserData.getmImageUrl().trim(), headerViewHolder);
            } else {
                headerViewHolder.mAIProfilePic.setImageResource(R.drawable.default_icon_ai);
            }
        }
        //check the current state before we display the screen
        if (headerViewHolder.mSwitch.isChecked()) {
            headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str) + " ");
            headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.duty_started) + " ");
            headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
            mAppPreferences.saveDutyStatus(true);
            lister.enableGPSservice(true, false);

        } else {
            headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str) + " ");
            headerViewHolder.lastSeenTxt.setPrefix(context.getResources().getString(R.string.last_seen) + " ");
            headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
            mAppPreferences.saveDutyStatus(false);
            lister.enableGPSservice(false, false);
        }
        headerViewHolder.mSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);
    }

    /**
     * Loading  the AI image . If image is coming from LoadConfig then need to download and  display
     * else
     * Local uri and if local local uri is also not there then display the default image
     */
    private void loadAiImage(String mCapturedPicture, HeaderViewHolder headerViewHolder) {
        if (null != mCapturedPicture && !TextUtils.isEmpty(mCapturedPicture)) {
            if (!(mCapturedPicture.contains("http"))) {
                headerViewHolder.mAIProfilePic.setImageURI(Uri.parse(mCapturedPicture));
            } else {
                Glide.with(context)
                        .load(mCapturedPicture)
                        .placeholder(R.drawable.default_icon_ai)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(headerViewHolder.mAIProfilePic);
            }
        } else {
            headerViewHolder.mAIProfilePic.setImageResource(R.drawable.default_icon_ai);
        }
    }

    private void setLocale(String localeString, int languageId) {
        Locale locale = new Locale(localeString);
        if (languageId == Constants.HINDI_LANGUAGE_ID) {
            locale.setDefault(locale);
            Configuration configuration = context.getApplicationContext().getResources().getConfiguration();
            configuration.locale = locale;
            context.getApplicationContext().getResources().updateConfiguration(configuration, context.getApplicationContext().getResources().getDisplayMetrics());
        }
    }

    void setupLanguage() {
        int languageId = mAppPreferences.getLanguageId();
        String localeString = Locale.getDefault().getLanguage();

        if (languageId != 0) {
            switch (languageId) {
                case Constants.ENGLISH_LANGUAGE_ID:
                    break;
                case Constants.HINDI_LANGUAGE_ID:
                    if (!Constants.LOCALE_HINDI_INDENTIFIER.equalsIgnoreCase(localeString)) {
                        setLocale(Constants.LOCALE_HINDI_INDENTIFIER, Constants.HINDI_LANGUAGE_ID);
                    }
                    break;
            }
        } else {
            Timber.d(TAG + "%s", "onResume: Language is not selected yet");
        }
    }

    /*void failureResponse(boolean isfailure) {
        if (headerViewHolder != null) {
            if (headerViewHolder.mSwitch != null) {
                headerViewHolder.mSwitch.setOnCheckedChangeListener(null);
                headerViewHolder.mSwitch.setChecked(mAppPreferences.getDutyStatus());
                if (isfailure) {
                    if (headerViewHolder.mSwitch.isChecked()) {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                        headerViewHolder.lastSeenTxt.setPrefix("Started ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                        mAppPreferences.saveDutyStatus(true);
                    } else {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                        headerViewHolder.lastSeenTxt.setPrefix("Last Seen ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                        mAppPreferences.saveDutyStatus(false);
                    }
                } else {
                    if (headerViewHolder.mSwitch.isChecked()) {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_on_str));
                        mAppPreferences.setLastSeen(new Date().getTime());
                        headerViewHolder.lastSeenTxt.setPrefix("Started ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                    } else {
                        headerViewHolder.dutyoffTxt.setText(context.getResources().getString(R.string.duty_of_str));
                        mAppPreferences.setLastSeen(new Date().getTime());
                        headerViewHolder.lastSeenTxt.setPrefix("Last Seen ");
                        headerViewHolder.lastSeenTxt.setReferenceTime(mAppPreferences.getLastSeen());
                    }
                }
                headerViewHolder.mSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);
            }
        }

    }*/

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == menuList.size() + 1;
    }

    void setAiImage() {
        appUserData = IOPSApplication.getInstance().getSettingsLevelSelectionStatmentDB().getAIPhotoWithAddress(mAppPreferences.getAppUserId());
        notifyDataSetChanged();
    }

    class GenericViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName;
        ImageView menuIcon;
        TextView NotificationCount;

        GenericViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.rowText);
            this.menuIcon = (ImageView) itemView.findViewById(R.id.rowIcon);
            this.NotificationCount = (TextView) itemView.findViewById(R.id.noticount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    class DividerViewHolder extends RecyclerView.ViewHolder {
        DividerViewHolder(View itemView) {
            super(itemView);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.powered)
        CustomFontTextview powered;

        FooterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView dutyoffTxt;
        RelativeTimeTextView lastSeenTxt;
        Switch mSwitch;
        CustomFontTextview userNameTxt;
        CircleImageView mAIProfilePic;

        HeaderViewHolder(View itemView) {
            super(itemView);
            this.dutyoffTxt = (TextView) itemView.findViewById(R.id.dutyoff_txt);
            this.lastSeenTxt = (RelativeTimeTextView) itemView.findViewById(R.id.lastseen_txt);
            this.mSwitch = (Switch) itemView.findViewById(R.id.switch1);
            this.userNameTxt = (CustomFontTextview) itemView.findViewById(R.id.userNameTxt);
            this.mAIProfilePic = (CircleImageView) itemView.findViewById(R.id.profileImage);
        }
    }
}
