package com.sisindia.ai.android.myunits;

/**
 * Created by Durga Prasad on 11-04-2016.
 */
public interface UnitDetailsFragmentListener {

    void updatePostData(String postName);
}
