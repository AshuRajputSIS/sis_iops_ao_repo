package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 19/7/16.
 */

public class UnitAtRiskIR {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("CompletedDate")
    @Expose
    private String completedDate;
    @SerializedName("IsCompleted")
    @Expose
    private Integer isCompleted;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     *
     * @param remarks
     * The Remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return
     * The completedDate
     */
    public String getCompletedDate() {
        return completedDate;
    }

    /**
     *
     * @param completedDate
     * The CompletedDate
     */
    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    /**
     *
     * @return
     * The isCompleted
     */
    public Integer getIsCompleted() {
        return isCompleted;
    }

    /**
     *
     * @param isCompleted
     * The IsCompleted
     */
    public void setIsCompleted(Integer isCompleted) {
        this.isCompleted = isCompleted;
    }
}
