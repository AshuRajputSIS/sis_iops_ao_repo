package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardDetailIMO {

    @SerializedName("EmployeeId")
    @Expose
    private String EmployeeId;
    @SerializedName("PostId")
    @Expose
    private String PostId;
    @SerializedName("GuardName")
    @Expose
    private String GuardName;
    @SerializedName("Performancerating")
    @Expose
    private PerformanceratingIMO Performancerating;
    @SerializedName("RewardFineIMO")
    @Expose
    private RewardFineIMO RewardFine;

    /**
     * @return The EmployeeId
     */
    public String getEmployeeId() {
        return EmployeeId;
    }

    /**
     * @param EmployeeId The EmployeeId
     */
    public void setEmployeeId(String EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    /**
     * @return The PostId
     */
    public String getPostId() {
        return PostId;
    }

    /**
     * @param PostId The PostId
     */
    public void setPostId(String PostId) {
        this.PostId = PostId;
    }

    /**
     * @return The GuardName
     */
    public String getGuardName() {
        return GuardName;
    }

    /**
     * @param GuardName The GuardName
     */
    public void setGuardName(String GuardName) {
        this.GuardName = GuardName;
    }

    /**
     * @return The Performancerating
     */
    public PerformanceratingIMO getPerformancerating() {
        return Performancerating;
    }

    /**
     * @param Performancerating The Performancerating
     */
    public void setPerformancerating(PerformanceratingIMO Performancerating) {
        this.Performancerating = Performancerating;
    }

    /**
     * @return The RewardFineIMO
     */
    public RewardFineIMO getRewardFine() {
        return RewardFine;
    }

    /**
     * @param RewardFine The RewardFineIMO
     */
    public void setRewardFine(RewardFineIMO RewardFine) {
        this.RewardFine = RewardFine;
    }

    @Override
    public String toString() {
        return "GuardDetailIMO{" +
                "EmployeeId='" + EmployeeId + '\'' +
                ", PostId='" + PostId + '\'' +
                ", GuardName='" + GuardName + '\'' +
                ", Performancerating=" + Performancerating +
                ", RewardFine=" + RewardFine +
                '}';
    }
}