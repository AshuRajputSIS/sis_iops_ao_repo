package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.UnitContactIR;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class AddEditClientContactFragment extends Fragment implements View.OnClickListener {

    @Bind(R.id.select_role)
    CustomFontEditText selectDesignation;
    @Bind(R.id.mobile_num_edt)
    CustomFontEditText mobileNumEdt;
    @Bind(R.id.name_edt)
    CustomFontEditText editName;
    @Bind(R.id.clear_btn)
    CustomFontButton clearBtn;
    @Bind(R.id.edit_contact_email)
    EditText contactEmailEdt;
    @Bind(R.id.lastname_edt)
    EditText lastNameEdit;
    @Bind(R.id.unitContacts_role)
    LinearLayout unitContactsRole;

    private FragmentManager fragmentManager;
    private String contactName, designation, mobileNumber, emailId;
    private ShowBottomSheetListener showBottomSheetListener;
    private int unitId;
    private UnitContactsMO unitContactsMO;
    private String title;
    private int sequenceID = -1;             // insertion new recored will be -1
    private IOPSApplication sisaiTrackingDBInstance;
    private ArrayList<UnitContactsMO> contactList = new ArrayList<>();
    private UpdatePostTabListener listner;
    private Resources resources;
    private ArrayList<String > designationList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//for enable the menu in fragment
        resources = getResources();
        sisaiTrackingDBInstance = IOPSApplication.getInstance();
        designationList = new ArrayList<>();
        fetchDesignationList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.add_edit_client_contact, null);
        ButterKnife.bind(this, view);

        selectDesignation.setOnClickListener(this);
        clearBtn.setOnClickListener(this);
        if (title.equalsIgnoreCase(resources.getString(R.string.edit_contact_str))) {
            setUpEditView();
        }

        return view;

    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public void setListner(UpdatePostTabListener listner) {
        this.listner = listner;
    }

    public void setUnitContactMO(UnitContactsMO unitContactMo, String title) {
        if (unitContactMo != null) {
            this.unitContactsMO = unitContactMo;
            sequenceID = unitContactMo.getId();
        }
        this.title = title;
    }

    private void clear() {
        mobileNumEdt.setText("");
        editName.setText("");
        contactEmailEdt.setText("");
        lastNameEdit.setText("");
        selectDesignation.setText("");
        editName.requestFocus();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_save_picture, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            validateFields();
            return true;
        }
        if (id == android.R.id.home) {
            getActivity().finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ShowBottomSheetListener)
            showBottomSheetListener = (ShowBottomSheetListener) context;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.clear_btn:
                clear();
                break;
            case R.id.select_role:
                if (designationList.size() > 0) {
                    showBottomSheetListener.showBottomSheet(designationList, view.getId());
                } else {
                    Util.customSnackBar(unitContactsRole, getActivity(), resources.getString(R.string.no_designation_list));
                }
        }
    }

    public void setValue(String value, int viewId) {
        switch (viewId) {
            case R.id.select_role:
                selectDesignation.setText(value);
                break;
        }
    }

    private void setUpEditView() {
        selectDesignation.setText(unitContactsMO.getClientDesignation());
        mobileNumEdt.setText(unitContactsMO.getContactNumber());
        editName.setText(unitContactsMO.getContactFirstName());
        lastNameEdit.setText(unitContactsMO.getContactLastName());
        contactEmailEdt.setText(unitContactsMO.getContactEmail());
        contactList = sisaiTrackingDBInstance.getUnitLevelSelectionQueryInstance().getListofContact(sequenceID, unitId);
    }

    private void validateFields() {
        UnitContactIR unitContact = new UnitContactIR();
        String contactLastName = lastNameEdit.getText().toString().trim();;
        String contactName = editName.getText().toString().trim();
        String designation = selectDesignation.getText().toString().trim();
        String mobileNumber = mobileNumEdt.getText().toString().trim();
        String emailId = contactEmailEdt.getText().toString().trim();

        if (contactName.length() == 0 || designation.length() == 0 || mobileNumber.length() == 0 ||contactLastName.length() == 0) {
           // Snackbar.make(unitContactsRole, getActivity().getResources().getString(R.string.client_handshake_validation), Snackbar.LENGTH_LONG).show();
            Util.customSnackBar(unitContactsRole,getActivity(),resources.getString(R.string.addEdit_task_validation));
            return;
        }
        if (!Util.isValidPhoneNumber(mobileNumber)) {
           // Snackbar.make(unitContactsRole, getActivity().getResources().getString(R.string.mobile_validation), Snackbar.LENGTH_LONG).show();
           Util.customSnackBar(unitContactsRole,getActivity(),resources.getString(R.string.mobile_validation));
            return;
        }

        if((emailId.length() != 0)){
            if (!Util.isValidMail(emailId)) {
                //Snackbar.make(unitContactsRole, getActivity().getResources().getString(R.string.email_validation), Snackbar.LENGTH_LONG).show();
                Util.customSnackBar(unitContactsRole,getActivity(),resources.getString(R.string.email_validation));

                return;
            }
        }
        else{
            // not mandatory
        }
        if (title.equalsIgnoreCase(resources.getString(R.string.edit_contact_str))) {

            if (checkDuplicateContact(mobileNumber, designation)) {
               // Snackbar.make(unitContactsRole, getActivity().getResources().getString(R.string.duplicate_contact_validation), Snackbar.LENGTH_LONG).show();
                Util.customSnackBar(unitContactsRole,getActivity(),resources.getString(R.string.duplicate_contact_validation));

                return;
            }

                unitContact.setId(unitContactsMO.getUnitContactId());

        } else {
            if (sisaiTrackingDBInstance.getUnitLevelSelectionQueryInstance().duplicateContactExist(mobileNumber, designation,unitId)) {
              //  Snackbar.make(unitContactsRole, getActivity().getResources().getString(R.string.duplicate_contact_validation), Snackbar.LENGTH_LONG).show();
                Util.customSnackBar(unitContactsRole,getActivity(),resources.getString(R.string.duplicate_contact_validation));

                return;
            }
            unitContact.setId(0);
        }
        unitContact.setDesignation(designation);
        unitContact.setContactNo(mobileNumber);
        unitContact.setEmailId(emailId);
        unitContact.setFirstName(contactName);
        unitContact.setLastName(contactLastName);
        unitContact.setUnitId(unitId);
        sisaiTrackingDBInstance.getUnitLevelInsertionDBInstance().insetrUpdateUnitContacts(unitContact, sequenceID);
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
        listner.updatePostTab();

    }

    /**
     * to check in case of edit contact
     *
     * @param mobile
     * @param desigation
     *
     * edit conatct user mobile no and designation should be ignoreds
     */
    private boolean checkDuplicateContact(String mobile, String desigation) {
        for (UnitContactsMO unitContact : contactList) {
            if (unitContact.getContactNumber().equalsIgnoreCase(mobile)
                    || unitContact.getClientDesignation().equalsIgnoreCase(desigation)) {
                return true;
            }
        }
        return false;
    }

    /**
     * look up for contact designation i.e lookup_type_id =43 for the contact designation
     * and lookup type name "ContactDesignation"
      */
    private void fetchDesignationList(){
        designationList.clear();
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + resources.getString(R.string.LOOKUP_NAME) + " from " +
                resources.getString(R.string.LOOKUP_MODEL_TABLE) + " where " +
                resources.getString(R.string.LOOKUP_TYPE_NAME) + " ='" + resources.getString(R.string.contact_designation_lookUp) + "'";
        Timber.d("selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        designationList.add(cursor.getString(cursor.getColumnIndex(resources.getString(R.string.LOOKUP_NAME))));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            exception.getCause();

        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();

            }


        }

    }
}
