package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitMessVendors {

    @SerializedName("Id")
    @Expose
    public Integer Id;
    @SerializedName("VendorName")
    @Expose
    public String VendorName;
    @SerializedName("Description")
    @Expose
    public String Description;
    @SerializedName("VendorContactId")
    @Expose
    public Integer VendorContactId;
    @SerializedName("VendorAddressId")
    @Expose
    public Integer VendorAddressId;
    @SerializedName("VendorContactName")
    @Expose
    public String VendorContactName;
    @SerializedName("VendorAddress")
    @Expose
    public String VendorAddress;
    @SerializedName("IsActive")
    @Expose
    public Boolean IsActive;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public String getVendorAddress() {
        return VendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        VendorAddress = vendorAddress;
    }

    public Integer getVendorAddressId() {
        return VendorAddressId;
    }

    public void setVendorAddressId(Integer vendorAddressId) {
        VendorAddressId = vendorAddressId;
    }

    public Integer getVendorContactId() {
        return VendorContactId;
    }

    public void setVendorContactId(Integer vendorContactId) {
        VendorContactId = vendorContactId;
    }

    public String getVendorContactName() {
        return VendorContactName;
    }

    public void setVendorContactName(String vendorContactName) {
        VendorContactName = vendorContactName;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }
}