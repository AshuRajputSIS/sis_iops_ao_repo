package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 12-05-2016.
 */
public class LastCheckInspectionHeaderMO {

    private String unitName;
    private String barrackName;
    private String lastInspectionDate;
    private String lastInspectionTime;
    private String inspectedBy;
    private int emPloyeeId;
    private String description;
    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }
    public String getInspectedBy() {
        return inspectedBy;
    }

    public void setInspectedBy(String inspectedBy) {
        this.inspectedBy = inspectedBy;
    }

    public String getLastInspectionDate() {
        return lastInspectionDate;
    }

    public void setLastInspectionDate(String lastInspectionDate) {
        this.lastInspectionDate = lastInspectionDate;
    }

    public int getEmPloyeeId() {
        return emPloyeeId;
    }

    public void setEmPloyeeId(int emPloyeeId) {
        this.emPloyeeId = emPloyeeId;
    }

    public String getLastInspectionTime() {
        return lastInspectionTime;
    }

    public void setLastInspectionTime(String lastInspectionTime) {
        this.lastInspectionTime = lastInspectionTime;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
