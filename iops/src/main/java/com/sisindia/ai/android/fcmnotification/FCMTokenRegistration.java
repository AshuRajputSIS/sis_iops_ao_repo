package com.sisindia.ai.android.fcmnotification;

/**
 * Created by shankar on 10/11/16.
 */

public class FCMTokenRegistration {

    public String getUniqueDeviceId() {
        return UniqueDeviceId;
    }

    public void setUniqueDeviceId(String uniqueDeviceId) {
        UniqueDeviceId = uniqueDeviceId;
    }

    private String UniqueDeviceId;
}
