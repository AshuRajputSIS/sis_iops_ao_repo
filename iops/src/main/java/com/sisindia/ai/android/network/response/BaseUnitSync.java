package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.myunits.models.Unit;

import java.util.ArrayList;
import java.util.List;

public class BaseUnitSync {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private List<Unit> data = new ArrayList<Unit>();

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * @return The data
     */
    public List<Unit> getData() {
        return data;
    }

    /**
     * @param data The LoadConfigurationData
     */
    public void setData(List<Unit> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseUnitSync{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", data=" + data +
                '}';
    }
}