package com.sisindia.ai.android.mybarrack;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackStrengthMO;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shruti on 27/7/16.
 */
public class TabStrengthFragment extends BaseFragment {

    @Bind(R.id.tv_barrack_capacity_value)
    CustomFontTextview tvBarrackCapacityValue;

    @Bind(R.id.et_barrack_strength_value)
    CustomFontEditText etBarrackStrengthValue;

    @Bind(R.id.et_barrack_leave_value)
    CustomFontEditText etBarrackLeaveValue;

    @Bind(R.id.tv_barrack_vacant_value)
    CustomFontTextview tvBarrackVacantValue;

    @Bind(R.id.tv_barrack_last_strength_value)
    CustomFontTextview tvBarrackLastStrengthValue;

    BarrackStrengthMO barrackStrengthMO;
    @Bind(R.id.tv_barrack_name)
    CustomFontTextview tvBarrackName;
    private ArrayList<EmployeeAuthorizedStrengthMO> updatedActualStrength = new ArrayList<>();
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String barrackName;
    private int unitId;
    private List<EmployeeAuthorizedStrengthMO> employeeAuthorizedStrengthMOList;
    private int barrackId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_strength_fragment, container, false);
        setUpUI(view);
        ButterKnife.bind(this, view);
        Util.hideKeyboard(getActivity());
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_strength_fragment;
    }

    private void setUpUI(View view) {

        ButterKnife.bind(this, view);
        tvBarrackVacantValue.setText("0");
        tvBarrackLastStrengthValue.setText("0");
        gettingApiValue();
        employeeAuthorizedStrengthMOList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                getUnitEmpAuthorizedRank(unitId, true, barrackId);
        if (employeeAuthorizedStrengthMOList != null && employeeAuthorizedStrengthMOList.size() > 0) {
            for (EmployeeAuthorizedStrengthMO employeeAuthorizedStrengthMO : employeeAuthorizedStrengthMOList) {
                tvBarrackCapacityValue.setText(String.valueOf(employeeAuthorizedStrengthMO.getStrength()));
            }
        } else {
            tvBarrackCapacityValue.setText("0");
        }

        etBarrackStrengthValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (null != getActivity()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateVacantValue(tvBarrackCapacityValue.getText().toString().trim(),
                                    etBarrackStrengthValue.getText().toString().trim(),
                                    etBarrackLeaveValue.getText().toString().trim());
                        }
                    });
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        etBarrackLeaveValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (null != getActivity()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateVacantValue(tvBarrackCapacityValue.getText().toString().trim(), etBarrackStrengthValue.getText().toString().trim(),
                                    etBarrackLeaveValue.getText().toString().trim());
                        }
                    });
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        if (null != getActivity()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateVacantValue(tvBarrackCapacityValue.getText().toString().trim(), etBarrackStrengthValue.getText().toString().trim(),
                            etBarrackLeaveValue.getText().toString().trim());
                }
            });
        }
    }

    private void gettingApiValue() {

        if (null != getArguments()) {
            barrackStrengthMO = (BarrackStrengthMO) getArguments().getSerializable("barrackStrengthMO");
            barrackName = getArguments().getString("barrackName");
            unitId = getArguments().getInt("unitId");
            barrackId = getArguments().getInt("barrackId");
            if (barrackStrengthMO != null) {
                // tvBarrackCapacityValue.setText(barrackStrengthMO.getCapacity() + "");
                tvBarrackLastStrengthValue.setText(barrackStrengthMO.getLastBarrackStrength() + "");
            }
        } else {
            tvBarrackCapacityValue.setText("0");
            tvBarrackLastStrengthValue.setText("0");
        }

        if (null != getActivity()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (null != barrackName && !barrackName.isEmpty()) {
                        tvBarrackName.setText(barrackName);
                    } else {
                        tvBarrackName.setText(getResources().getString(R.string.NOT_AVAILABLE));
                    }
                    updateVacantValue(tvBarrackCapacityValue.getText().toString().trim(), etBarrackStrengthValue.getText().toString().trim(),
                            etBarrackLeaveValue.getText().toString().trim());
                }
            });
        }
    }

    private void updateVacantValue(String capacity, String strengthPresent, String leaveSick) {

        int vacant = 0;
        int strengthPresentValue;
        int leaveSickValue;
        int capacityValue = 0;
        if (capacity.length() == 0) {
            capacityValue = 0;
        } else {
            capacityValue = Integer.parseInt(capacity);
        }
        if (strengthPresent.length() == 0) {
            strengthPresentValue = 0;
        } else {
            strengthPresentValue = Integer.parseInt(strengthPresent);
        }
        if (leaveSick.length() == 0) {
            leaveSickValue = 0;
        } else {
            leaveSickValue = Integer.parseInt(leaveSick);
        }

        vacant = capacityValue - (strengthPresentValue + leaveSickValue);
        if (vacant < 0) {
            tvBarrackVacantValue.setText("" + 0);
        } else {
            tvBarrackVacantValue.setText("" + vacant);
        }
        ((BarrackInspectionTabActivity) getActivity()).checkStrengthEntry(strengthPresentValue, leaveSickValue);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
