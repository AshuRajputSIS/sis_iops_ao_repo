package com.sisindia.ai.android.issues.models;

/**
 * Created by Shushrut on 19-05-2016.
 */
public class AddGrievanceHeaderMO {
    private String unitName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
