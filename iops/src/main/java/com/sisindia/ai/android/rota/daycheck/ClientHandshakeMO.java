package com.sisindia.ai.android.rota.daycheck;

import com.sisindia.ai.android.myunits.ContactMO;

/**
 * Created by Saruchi on 22-06-2016.
 */

public class ClientHandshakeMO {

    private String metClient;
    private ContactMO contactMO;
    private String serviceSatisfied;
    private String reason;

    public String getMetClient() {
        return metClient;
    }

    public void setMetClient(String metClient) {
        this.metClient = metClient;
    }

    public ContactMO getContactMO() {
        return contactMO;
    }

    public void setContactMO(ContactMO contactMO) {
        this.contactMO = contactMO;
    }

    public String getServiceSatisfied() {
        return serviceSatisfied;
    }

    public void setServiceSatisfied(String serviceSatisfied) {
        this.serviceSatisfied = serviceSatisfied;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ClientHandshakeMO{" +
                "metClient='" + metClient + '\'' +
                ", contactMO=" + contactMO +
                ", serviceSatisfied='" + serviceSatisfied + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
