package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitMessVendors implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("VendorName")
    private String VendorName;
    @SerializedName("Description")
    private String Description;
    @SerializedName("VendorContactId")
    private int VendorContactId;
    @SerializedName("VendorAddressId")
    private int VendorAddressId;
    @SerializedName("VendorContactName")
    private String VendorContactName;
    @SerializedName("VendorAddress")
    private String VendorAddress;
    @SerializedName("IsActive")
    private boolean IsActive;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getVendorContactId() {
        return VendorContactId;
    }

    public void setVendorContactId(int vendorContactId) {
        VendorContactId = vendorContactId;
    }

    public int getVendorAddressId() {
        return VendorAddressId;
    }

    public void setVendorAddressId(int vendorAddressId) {
        VendorAddressId = vendorAddressId;
    }

    public String getVendorContactName() {
        return VendorContactName;
    }

    public void setVendorContactName(String vendorContactName) {
        VendorContactName = vendorContactName;
    }

    public String getVendorAddress() {
        return VendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        VendorAddress = vendorAddress;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }
}
