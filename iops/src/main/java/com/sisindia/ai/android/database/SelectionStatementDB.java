package com.sisindia.ai.android.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.gson.reflect.TypeToken;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestItem;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.issues.complaints.ComplaintInfoMO;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.issues.models.NoDataAvailable;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;
import com.sisindia.ai.android.myunits.models.AddEquipmentLocalMO;
import com.sisindia.ai.android.myunits.models.CheckIsMainGateMO;
import com.sisindia.ai.android.myunits.models.EquipmentListModel;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.myunits.models.UnitDetails;
import com.sisindia.ai.android.network.request.GpsBatteryInputRequestBaseMO;
import com.sisindia.ai.android.network.request.GpsBatteryInputRequestMOLocation;
import com.sisindia.ai.android.network.response.AppRole;
import com.sisindia.ai.android.network.response.UnitEmployee;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.LookUpCheckListItem;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionsData;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionBaseMO;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionComplaintsMO;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionGrievancesMO;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionHeaderMO;
import com.sisindia.ai.android.rota.daycheck.LastCheckInspectionUnitAtRiskMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.RotaTaskActivityListMO;
import com.sisindia.ai.android.rota.models.RotaTaskActvityCheckMO;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 18/5/16.
 */
public class SelectionStatementDB extends SISAITrackingDB {

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
//    private Util util;

    public SelectionStatementDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
//        util = new Util();
    }

    /**
     * @param unitId
     * @param taskId
     * @param barrackName
     * @return
     */
    public LastCheckInspectionBaseMO fetchLastCheckInspectionData(int unitId, int taskId, int taskStatusId, int taskTypeId,
                                                                  String unitName, int barrackId, String barrackName) {
        LastCheckInspectionBaseMO lastCheckInspectionBaseMO = new LastCheckInspectionBaseMO();
        ArrayList<Object> lastCheckInspectionBaseMOList = new ArrayList<>();
        // Below query is used to get [UnitName,LastDayCheck,Done By, Additional Notes(removed as per req)]
        LastCheckInspectionHeaderMO lastCheckInspectionHeaderMO = fetchLastInspectionUnitDetails(unitId, taskId, taskStatusId, taskTypeId, barrackId);
        lastCheckInspectionHeaderMO.setUnitName(unitName);
        lastCheckInspectionHeaderMO.setBarrackName(barrackName);
        lastCheckInspectionBaseMOList.add(lastCheckInspectionHeaderMO);

        ArrayList<ImprovementPlanListMO> lastInspectionImprovementPlan = getLastImprovementPlanList(taskTypeId, unitId, 1, barrackId);

        if (taskTypeId == 4) {
            ArrayList<LastCheckInspectionComplaintsMO> lastCheckInspectionComplaintsMOArrayList = fetchLastInspectionComplaints(taskId, unitId, 1,
                    taskStatusId, taskTypeId);
            if (lastCheckInspectionComplaintsMOArrayList.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.complaints_label);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_complaints_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.complaints_label);
                for (int index = 0; index < lastCheckInspectionComplaintsMOArrayList.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastCheckInspectionComplaintsMOArrayList.get(index));
                }
            }
            if (lastInspectionImprovementPlan.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.improvementplan);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_improvementplan_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.improvementplan);
                for (int index = 0; index < lastInspectionImprovementPlan.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastInspectionImprovementPlan.get(index));
                }
            }
        } else if (taskTypeId == 3) {
            if (lastInspectionImprovementPlan.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.improvementplan);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_improvementplan_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.improvementplan);
                for (int index = 0; index < lastInspectionImprovementPlan.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastInspectionImprovementPlan.get(index));
                }
            }
        } else {
            //ArrayList<LastCheckInspectionImprovementpalnMO> lastCheckInspectionImprovementpalnMOArrayList = fetchLastInspectionImprovementplans(taskId, unitId, 1);

            //ADDING DETAILS OR INFORMATION IN THE LIST FOR "UAR/POA"
            ArrayList<Integer> unitRiskIds = fetchUnitRiskIds(unitId, 0);
            ArrayList<LastCheckInspectionUnitAtRiskMO> lastCheckInspectionUnitAtRiskMOArrayList = new ArrayList<>();
            for (Integer unitRiskId : unitRiskIds) {
                lastCheckInspectionUnitAtRiskMOArrayList.addAll(getUARPendingList(unitRiskId, 0));
            }

            if (lastCheckInspectionUnitAtRiskMOArrayList.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.unitatrisk_poa);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_unitatrisk_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.unitatrisk_poa);
                for (int index = 0; index < lastCheckInspectionUnitAtRiskMOArrayList.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastCheckInspectionUnitAtRiskMOArrayList.get(index));
                }
            }

            //ADDING INFORMATION OF DETAILS IN THE LIST AGAINST KIT DISTRIBUTION
            ArrayList<KitItemRequestMO> lasKitItemRequestMOList = fetchLastInspectionKitItems(taskId, unitId, 1, taskStatusId, taskTypeId);
            if (lasKitItemRequestMOList != null && lasKitItemRequestMOList.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.kitItemRquest);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_kititem_request_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.kitItemRquest);
                for (int index = 0; index < lasKitItemRequestMOList.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lasKitItemRequestMOList.get(index));
                }
            }

            //ADDING DETAILS IN LIST FOR "COMPLAINTS"
            ArrayList<LastCheckInspectionComplaintsMO> lastCheckInspectionComplaintsMOArrayList =
                    fetchLastInspectionComplaints(taskId, unitId, 1, taskStatusId, taskTypeId);
            if (lastCheckInspectionComplaintsMOArrayList.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.complaints_label);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_complaints_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.complaints_label);
                for (int index = 0; index < lastCheckInspectionComplaintsMOArrayList.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastCheckInspectionComplaintsMOArrayList.get(index));
                }
            }

            //ADDING GRIEVANCE DETAILS AT THE END OF THE LIST - AS PER REQ BY P.K TIWARI
            //            if (taskTypeId != 4) {
            ArrayList<LastCheckInspectionGrievancesMO> lastCheckInspectionGrievancesMOArrayList =
                    grievancesHistory(taskId, unitId, 1, taskStatusId, taskTypeId, barrackId);
            if (lastCheckInspectionGrievancesMOArrayList.size() == 0) {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.Grievance);
                NoDataAvailable noDataAvailable = new NoDataAvailable();
                noDataAvailable.setMessage(TableNameAndColumnStatement.no_grievance_data);
                lastCheckInspectionBaseMOList.add(noDataAvailable);
            } else {
                lastCheckInspectionBaseMOList.add(TableNameAndColumnStatement.Grievance);
                for (int index = 0; index < lastCheckInspectionGrievancesMOArrayList.size(); index++) {
                    lastCheckInspectionBaseMOList.add(lastCheckInspectionGrievancesMOArrayList.get(index));
                }
            }
//            }

        }
        lastCheckInspectionBaseMO.setLastCheckInspectionBaseMOList(lastCheckInspectionBaseMOList);
        return lastCheckInspectionBaseMO;
    }

    /**
     * @param unitId
     * @param taskId
     * @param taskStatusId
     * @param taskTypeId
     * @return
     */
    public LastCheckInspectionHeaderMO fetchLastInspectionUnitDetails(int unitId, int taskId, int taskStatusId, int taskTypeId, int barrackId) {

        String query;
        if (taskTypeId == 3) {
            query = "select task." + TableNameAndColumnStatement.TASK_ID + "," +
                    "task." + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME + "," +
                    "barrack." + TableNameAndColumnStatement.BARRACK_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.FIRST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.LAST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.EMPLOYEE_ID + "," +
                    "task." + TableNameAndColumnStatement.DESCRIPTION +
                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    " inner join " + TableNameAndColumnStatement.BARRACK_TABLE +
                    " on barrack." + TableNameAndColumnStatement.BARRACK_ID +
                    " = " + "task." + TableNameAndColumnStatement.BARRACK_ID +
                    " left join " + TableNameAndColumnStatement.APP_USER_TABLE +
                    " appUser on appUser." + TableNameAndColumnStatement.EMPLOYEE_ID +
                    " == " + "task." + TableNameAndColumnStatement.ASSIGNEE_ID +
                    " where barrack." + TableNameAndColumnStatement.BARRACK_ID +
                    " = " + barrackId + " and " + " task." + TableNameAndColumnStatement.TASK_STATUS_ID +
                    " in ( " + RotaTaskStatusEnum.Completed.getValue() + "," + RotaTaskStatusEnum.InActive.getValue() + ")" +
                    " and " + " task." + TableNameAndColumnStatement.TASK_TYPE_ID +
                    " = " + taskTypeId +
                    " group by task." + TableNameAndColumnStatement.BARRACK_ID +
                    " order by " + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME +
                    " desc limit 1 ";

        } else if (taskTypeId == 4) {
            query = "select task." + TableNameAndColumnStatement.TASK_ID + "," +
                    "task." + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME + "," +
                    "unit." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.FIRST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.LAST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.EMPLOYEE_ID + "," +
                    "task." + TableNameAndColumnStatement.DESCRIPTION +
                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    " inner join " + TableNameAndColumnStatement.UNIT_TABLE +
                    " on unit." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "task." + TableNameAndColumnStatement.UNIT_ID +
                    " left join " + TableNameAndColumnStatement.APP_USER_TABLE +
                    " appUser on appUser." + TableNameAndColumnStatement.EMPLOYEE_ID +
                    " == " + "task." + TableNameAndColumnStatement.ASSIGNEE_ID +
                    " where unit." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + unitId + " and " + " task." + TableNameAndColumnStatement.TASK_STATUS_ID +
                    " in ( " + RotaTaskStatusEnum.Completed.getValue() + "," + RotaTaskStatusEnum.InActive.getValue() + ")" +
                    " and " + " task." + TableNameAndColumnStatement.TASK_TYPE_ID +
                    " = " + taskTypeId +
                    " group by task." + TableNameAndColumnStatement.UNIT_ID +
                    " order by " + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME +
                    " desc limit 1 ";
        } else {
            query = "select task." + TableNameAndColumnStatement.TASK_ID + "," +
                    "task." + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME + "," +
                    "unit." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.FIRST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.LAST_NAME + "," +
                    "appUser." + TableNameAndColumnStatement.EMPLOYEE_ID + "," +
                    "task." + TableNameAndColumnStatement.DESCRIPTION +

                    " from " + TableNameAndColumnStatement.TASK_TABLE +
                    " inner join " + TableNameAndColumnStatement.UNIT_TABLE +
                    " on unit." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "task." + TableNameAndColumnStatement.UNIT_ID +
                    " left join " + TableNameAndColumnStatement.APP_USER_TABLE +
                    " appUser on appUser." + TableNameAndColumnStatement.EMPLOYEE_ID +
                    " == " + "task." + TableNameAndColumnStatement.ASSIGNEE_ID +
                    " where unit." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + unitId + " and " + " task." + TableNameAndColumnStatement.TASK_STATUS_ID +
                    " in ( " + RotaTaskStatusEnum.Completed.getValue() + "," + RotaTaskStatusEnum.InActive.getValue() + ")" +
                    " and " + " task." + TableNameAndColumnStatement.TASK_TYPE_ID +
                    " = " + taskTypeId +
                    " group by task." + TableNameAndColumnStatement.UNIT_ID +
                    " order by " + TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME +
                    " desc limit 1 ";
        }

        Timber.i("fetchLastInspectionUnitDetails %s", query);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        LastCheckInspectionHeaderMO lastCheckInspectionHeaderMO = new LastCheckInspectionHeaderMO();
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (taskTypeId == 3)
                            lastCheckInspectionHeaderMO.setBarrackName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                        else
                            lastCheckInspectionHeaderMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));

                        String timeStamp = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME));
                        lastCheckInspectionHeaderMO.setEmPloyeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID)));

                        if (timeStamp != null) {
                            String[] timeStampArray = timeStamp.split(" ");
                            String time = dateTimeFormatConversionUtil.timeConverstionFrom24To12(timeStampArray[1]);
                            lastCheckInspectionHeaderMO.setLastInspectionDate(timeStampArray[0]);
                            lastCheckInspectionHeaderMO.setLastInspectionTime(time);
                        } else {
                            lastCheckInspectionHeaderMO.setLastInspectionDate(TableNameAndColumnStatement.NOT_AVAILABLE);
                            lastCheckInspectionHeaderMO.setLastInspectionTime(TableNameAndColumnStatement.NOT_AVAILABLE);
                        }

                        String firstName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME));
                        String lastName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME));
                        /*String description = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTIONS));
                        if (!TextUtils.isEmpty(description)) {
                            lastCheckInspectionHeaderMO.setDescription(description);
                        }*/

                        if (!TextUtils.isEmpty(firstName) || !TextUtils.isEmpty(lastName)) {
                            StringBuilder inspectedBy = new StringBuilder();
                            if (!TextUtils.isEmpty(firstName))
                                inspectedBy.append(firstName);
                            if (!TextUtils.isEmpty(lastName))
                                inspectedBy.append(" " + lastName);
                            lastCheckInspectionHeaderMO.setInspectedBy(inspectedBy.toString());
                        } else {
                            lastCheckInspectionHeaderMO.setInspectedBy(TableNameAndColumnStatement.NOT_AVAILABLE);
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lastCheckInspectionHeaderMO;
    }

    /**
     * @param taskId
     * @param unitId
     * @param isSynced
     * @return
     */
    public ArrayList<LastCheckInspectionGrievancesMO> grievancesHistory(int taskId, int unitId,
                                                                        int isSynced, int taskStatusId,
                                                                        int taskTypeId, int barrackId) {

        ArrayList<LastCheckInspectionGrievancesMO> lastCheckInspectionGrievancesMOList = new ArrayList<>();
        String grievanceQuery;
        if (taskTypeId == 3) {
            grievanceQuery = " select barrack_name,NatureOfGrievance,GrievanceStatus, closure_end_date, id,target_action_end_date, Guard_Name as " + TableNameAndColumnStatement.GUARD_NAME + ", Guard_Code as " + TableNameAndColumnStatement.GUARD_CODE + ",assigned_to_name, CreatedDateTime as " + TableNameAndColumnStatement.CREATED_DATE_TIME + ", issue_nature_id, " +
                    " issue_status_id,topItem.source_task_id as " + TableNameAndColumnStatement.SOURCE_TASK_ID + " from (select source_task_id from issues where issue_type_id = 1 order by id desc limit 1)topItem inner join (select b.barrack_name, " +
                    " l.lookup_name AS NatureOfGrievance, l1.lookup_name AS GrievanceStatus, i.closure_end_date, i.id, i.target_action_end_date, i.Guard_Name, i.Guard_Code, i.assigned_to_name, i.CreatedDateTime, " +
                    " i.issue_nature_id, i.issue_status_id,i.source_task_id from issues i inner join barrack b on i.barrack_id = b.barrack_id inner join lookup_table l on i.issue_nature_id = l.lookup_identifier " +
                    " and l.lookup_type_id = 18 inner join lookup_table l1 on i.issue_status_id = l1.lookup_identifier and l1.lookup_type_id = 23 inner join task as task on task.task_id=i.source_task_id " +
                    " and task.barrack_id = b.barrack_id where task.task_type_id =" + taskTypeId + " and task.task_status_id in (3,4) and task.barrack_id =" + barrackId + " and i.issue_type_id = 1 order by source_task_id desc)allItems  " +
                    " on allItems.source_task_id = topItem.source_task_id";
        } else {
            grievanceQuery = " SELECT unit_name, NatureOfGrievance, GrievanceStatus, closure_end_date, id, target_action_end_date, Guard_Name as " + TableNameAndColumnStatement.GUARD_NAME + "," +
                    " Guard_Code as " + TableNameAndColumnStatement.GUARD_CODE + " ,assigned_to_name ,CreatedDateTime as " + TableNameAndColumnStatement.CREATED_DATE_TIME + "," +
                    " issue_nature_id, issue_status_id,topItem.source_task_id as " + TableNameAndColumnStatement.SOURCE_TASK_ID +
                    " from (SELECT source_task_id from issues where issue_type_id = 1 order by id desc limit 1)topItem " +
                    " inner join (select  u.unit_name, l.lookup_name AS NatureOfGrievance, l1.lookup_name AS GrievanceStatus, " +
                    " i.closure_end_date, i.id, i.target_action_end_date, i.Guard_Name, i.Guard_Code, i.assigned_to_name, i.CreatedDateTime, i.issue_nature_id, i.issue_status_id,i.source_task_id " +
                    " from issues i inner join   unit u on  u.unit_id = i.unit_id inner join lookup_table l on i.issue_nature_id = l.lookup_identifier and l.lookup_type_id = 18 " +
                    " inner join lookup_table l1 on i.issue_status_id = l1.lookup_identifier and l1.lookup_type_id = 23 " +
                    " inner join task as task on task.task_id=i.source_task_id where i.issue_type_id = 1  and task.task_type_id = " + taskTypeId + " and task.task_status_id in (3,4) and  task.unit_id = " + unitId + " order by  i.source_task_id desc)allItems  " +
                    " on allItems.source_task_id = topItem.source_task_id";
        }
        Timber.i("fetchLastInspectionGrievances %s", grievanceQuery);
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(grievanceQuery, null);
            String billingStatus = "";

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        LastCheckInspectionGrievancesMO lastCheckInspectionGrievancesMO = new LastCheckInspectionGrievancesMO();
                        lastCheckInspectionGrievancesMO.setGrievanceNature
                                (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NATURE_OF_GRIEVANCE)));
                        if (taskTypeId == 3) {
                            lastCheckInspectionGrievancesMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                        } else {
                            lastCheckInspectionGrievancesMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        }
                        lastCheckInspectionGrievancesMO.setGuardName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        lastCheckInspectionGrievancesMO.setGrievanceClosedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_END_DATE)));
                        lastCheckInspectionGrievancesMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        lastCheckInspectionGrievancesMO.setEmployeeId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        lastCheckInspectionGrievancesMO.setStatus(cursor.getString(cursor.getColumnIndex("GrievanceStatus")));
                        lastCheckInspectionGrievancesMO.setpId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        lastCheckInspectionGrievancesMO.setAssignedToName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        lastCheckInspectionGrievancesMO.setGrievanceClosedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_ACTION_END_DATE)));
                        lastCheckInspectionGrievancesMOList.add(lastCheckInspectionGrievancesMO);
                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lastCheckInspectionGrievancesMOList;
    }

    /**
     * @param taskId
     * @param unitId
     * @param isSynced
     * @return
     */
    public ArrayList<LastCheckInspectionComplaintsMO> fetchLastInspectionComplaints(int taskId, int unitId, int isSynced, int taskStatusId, int taskTypeId) {

        ArrayList<LastCheckInspectionComplaintsMO> lastCheckInspectionComplaintsMOList = new ArrayList<LastCheckInspectionComplaintsMO>();
        String complaintsQuery = " SELECT unit_name,NatureOfComplaint,ComplaintStatus,ModeOfComplaint,CauseOfComplaint,closure_end_date, " +
                " first_name,assigned_to_name,CreatedDateTime as " + TableNameAndColumnStatement.CREATED_DATE_TIME + ",issue_nature_id, issue_cause_id," +
                " remarks,target_action_end_date,topItem.source_task_id as " + TableNameAndColumnStatement.SOURCE_TASK_ID +
                " from (SELECT source_task_id from issues where issue_type_id = 2 order by id desc limit 1)topItem " +
                " inner join (select u.unit_name, l.lookup_name AS NatureOfComplaint, l1.lookup_name AS ComplaintStatus, l2.lookup_name AS ModeOfComplaint, " +
                " l3.lookup_name AS CauseOfComplaint, i.closure_end_date, uc.first_name, i.assigned_to_name, i.CreatedDateTime, i.issue_nature_id, " +
                " i.issue_cause_id, i.remarks, i.target_action_end_date, i.CreatedDateTime, i.assigned_to_name,i.source_task_id from unit u " +
                " inner join  issues i  on  u.unit_id = i.unit_id inner join lookup_table l  on i.issue_nature_id = l.lookup_identifier " +
                " and l.lookup_type_id = 16 inner join lookup_table l1 on i.issue_status_id = l1.lookup_identifier and l1.lookup_type_id = 23 " +
                " inner join lookup_table l2 on i.issue_mode_Id = l2.lookup_identifier and l2.lookup_type_id = 21 " +
                " inner join lookup_table l3 on i.issue_cause_id = l3.lookup_identifier and l3.lookup_type_id = 17 " +
                " left outer join unit_contact uc on i.unit_contact_id =  uc.unit_contact_id " +
                " inner join task t on t.task_id=i.source_task_id where i.issue_type_id = 2 and t.task_type_id = " + taskTypeId +
                " and t.task_status_id in (3,4)  and t.unit_id = " + unitId + " order by  i.source_task_id desc )allItems  on allItems.source_task_id = topItem.source_task_id";

        Timber.i("fetchLastInspectionComplaints %s", complaintsQuery);


        SQLiteDatabase sqlite = null;
        Cursor cursor = null;

        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(complaintsQuery, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        LastCheckInspectionComplaintsMO lastCheckInspectionComplaintsMO = new LastCheckInspectionComplaintsMO();
                        lastCheckInspectionComplaintsMO.setNatureOfComplaint(cursor.getString(cursor.getColumnIndex("NatureOfComplaint")));
                        lastCheckInspectionComplaintsMO.setComplaintMode(cursor.getString(cursor.getColumnIndex("ModeOfComplaint")));
                        lastCheckInspectionComplaintsMO.setCauseOfComplaint(cursor.getString(cursor.getColumnIndex("CauseOfComplaint")));
                        lastCheckInspectionComplaintsMO.setUnitContactPerson(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)));
                        lastCheckInspectionComplaintsMO.setAssignedTo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        lastCheckInspectionComplaintsMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        lastCheckInspectionComplaintsMO.setComplaintClosedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_ACTION_END_DATE)));
                        lastCheckInspectionComplaintsMO.setComplaintStatus(cursor.getString(cursor.getColumnIndex("ComplaintStatus")));
                        lastCheckInspectionComplaintsMO.setAssignedName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        lastCheckInspectionComplaintsMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        lastCheckInspectionComplaintsMOList.add(lastCheckInspectionComplaintsMO);
                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lastCheckInspectionComplaintsMOList;
    }

    public ArrayList<ImprovementPlanListMO> getLastImprovementPlanList(int taskTypeId, int unitId, int isSynced, int barrackId) {

        ArrayList<ImprovementPlanListMO> issuesMOList = new ArrayList<>();
        String issueQuery = null;
        if (taskTypeId == 3) {
            issueQuery = " select barrack_name,barrack_id, improvementStatus,Name,assigned_to_id, improvement_plan_id, remarks, action_plan_id, improvement_plan_category_id, id, closure_date_time, expected_closure_date, assigned_to_name, issue_matrix_id, unit_id, issue_id, target_employee_id, closed_by_id, closure_comment, " +
                    " assigned_to_name, CreatedDateTime, status_id,topItem.task_id from (select task_id from improvement_plan where task_id not null and barrack_id is not null order by task_id desc limit 1)topItem inner join (select b.barrack_name,b.barrack_id, l.lookup_name AS improvementStatus, i.assigned_to_id, " +
                    " i.improvement_plan_id, i.remarks, i.action_plan_id, i.improvement_plan_category_id, i.id, i.closure_date_time,i.task_id, i.expected_closure_date, i.assigned_to_name, i.issue_matrix_id, i.unit_id, i.issue_id, i.target_employee_id, i.closed_by_id, i.closure_comment, " +
                    " i.assigned_to_name, i.CreatedDateTime,map.Name, i.status_id,i.task_id from improvement_plan i left outer join  barrack b  on  b.barrack_id =  i.barrack_id " +
                    " inner join master_action_plan map on i.action_plan_id = map.id " +
                    " inner join lookup_table l  on i.status_id = l.lookup_identifier and l.lookup_type_name = 'ActionPlanStatus'  and b.barrack_id = " + barrackId + " and task_id is not null)allItems  on allItems.task_id = topItem.task_id";

        } else if (taskTypeId == 4) {

            issueQuery = " select unit_name, improvementStatus,Name, assigned_to_id, improvement_plan_id, remarks, action_plan_id, improvement_plan_category_id, id, closure_date_time, expected_closure_date, assigned_to_name, " +
                    " issue_matrix_id, unit_id, issue_id, target_employee_id, closed_by_id, closure_comment, assigned_to_name, CreatedDateTime, status_id,topItem.task_id from " +
                    " (select task_id from improvement_plan where task_id not null and barrack_id is  null order by task_id desc limit 1)topItem " +
                    " inner join (select u.unit_name, l.lookup_name AS improvementStatus, i.assigned_to_id, " +
                    " i.improvement_plan_id, i.remarks,map.Name,i.action_plan_id, i.improvement_plan_category_id, i.id, i.closure_date_time,i.task_id, i.expected_closure_date, i.assigned_to_name, i.issue_matrix_id, " +
                    " i.unit_id, i.issue_id, i.target_employee_id, i.closed_by_id, i.closure_comment, i.assigned_to_name, i.CreatedDateTime, i.status_id,i.task_id from improvement_plan i " +
                    " inner join master_action_plan map on i.action_plan_id = map.id " +
                    " left outer join  unit u  on  u.unit_id = i.unit_id inner join lookup_table l  on i.status_id = l.lookup_identifier and l.lookup_type_name = 'ActionPlanStatus'  and i.unit_id =" + unitId +
                    " and task_id is not null)allItems  on allItems.task_id = topItem.task_id";
        } else {
            issueQuery = "select u." + TableNameAndColumnStatement.UNIT_NAME + ","
                    + " l." + TableNameAndColumnStatement.LOOKUP_NAME + " AS "
                    + TableNameAndColumnStatement.IMPROVEMENT_STATUS + ","
                    + " lookup." + TableNameAndColumnStatement.LOOKUP_NAME + " AS "
                    + TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY + ","
                    + " i." + TableNameAndColumnStatement.ASSIGNED_TO_ID + ","
                    + " i." + TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + ","
                    + " i." + TableNameAndColumnStatement.REMARKS + ","
                    + " i." + TableNameAndColumnStatement.ACTION_PLAN_ID + ","
                    + " i." + TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID + ","
                    + " i." + TableNameAndColumnStatement.ID + ","
                    + " i." + TableNameAndColumnStatement.CLOSURE_DATE_TIME + ","
                    + " i." + TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE + ","
                    + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ","
                    + " i." + TableNameAndColumnStatement.ISSUE_MATRIX_ID + ","
                    + " i." + TableNameAndColumnStatement.UNIT_ID + ","
                    + " i." + TableNameAndColumnStatement.ISSUE_ID + ","
                    + " i." + TableNameAndColumnStatement.TARGET_EMPLOYEE_ID + ","
                    + " i." + TableNameAndColumnStatement.CLOSED_BY_ID + ","
                    + " i." + TableNameAndColumnStatement.CLOSURE_COMMENT + ","
                    + " i." + TableNameAndColumnStatement.ASSIGNED_TO_NAME + ","
                    + " i." + TableNameAndColumnStatement.CREATED_DATE_TIME + ","
                    + " map." + TableNameAndColumnStatement.NAME + ","
                    + " i." + TableNameAndColumnStatement.STATUS_ID
                    + " from " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE
                    + " i left outer join  " + TableNameAndColumnStatement.UNIT_TABLE
                    + " u  on  u." + TableNameAndColumnStatement.UNIT_ID
                    + " = i." + TableNameAndColumnStatement.UNIT_ID
                    + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                    + " l  on i." + TableNameAndColumnStatement.STATUS_ID
                    + " = l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER
                    + " and l." + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = 'ActionPlanStatus'"
                    + " inner join " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE
                    + " map on i." + TableNameAndColumnStatement.ACTION_PLAN_ID
                    + " = map." + TableNameAndColumnStatement.ID
                    + " inner join " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                    + " lookup on i." + TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID
                    + " = lookup." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + " where lookup." +
                    TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = '" +
                    TableNameAndColumnStatement.lookup_improvementPlanCategory + "'" +
                    " and i." + TableNameAndColumnStatement.IS_SYNCED + " = " + isSynced +
                    " and i." + TableNameAndColumnStatement.UNIT_ID + " = " + unitId +
                    " order by i." + TableNameAndColumnStatement.CREATED_DATE_TIME + " asc" +
                    ", i." + TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE + " asc" +
                    ", i." + TableNameAndColumnStatement.CLOSURE_DATE_TIME;
        }


        Timber.i("LastImprovementPlanList Query : %s " + issueQuery);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(issueQuery, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        ImprovementPlanListMO improvementMO = new ImprovementPlanListMO();
                        if (taskTypeId == 3) {
                            improvementMO.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                            improvementMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME)));
                        } else {
                            improvementMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                            improvementMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        }
                        improvementMO.setImprovementStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_STATUS)));
                        improvementMO.setRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        improvementMO.setSeqId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        improvementMO.setExpectedClosureDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE)));
                        improvementMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        improvementMO.setImprovementStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.STATUS_ID)));
                        improvementMO.setClosureDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_DATE_TIME)));
                        improvementMO.setImprovementId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID)));
                        improvementMO.setAssignedToId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)));
                        improvementMO.setAssignedToName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_NAME)));
                        improvementMO.setActionPlanName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NAME)));
                        improvementMO.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementMO.setTargetEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID)));
                        improvementMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));
                        improvementMO.setClosedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSED_BY_ID)));
                        improvementMO.setClosureRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_COMMENT)));
                        improvementMO.setActionPlanId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        issuesMOList.add(improvementMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
                Timber.d("LastImprovementPlanList %s", IOPSApplication.getGsonInstance().toJson(issuesMOList));
            }
        }
        return issuesMOList;
    }

    /**
     * @param taskId
     * @param unitId
     * @param isSynced
     * @return
     */
    public ArrayList<KitItemRequestMO> fetchLastInspectionKitItems(int taskId, int unitId, int isSynced, int taskStatusId, int taskTypeId) {

        ArrayList<KitItemRequestMO> lastCheckInspectionkitKitItemRequestMOList = new ArrayList<>();

        /*String kitItemRequestQuery = "select "
                + TableNameAndColumnStatement.GUARD_CODE + ","
                + TableNameAndColumnStatement.GUARD_ID + ","
                + TableNameAndColumnStatement.ID + ","
                + TableNameAndColumnStatement.REQUESTED_ITEMS + ","
                + TableNameAndColumnStatement.REQUESTED_STATUS + ","
                + TableNameAndColumnStatement.REQUESTED_ON + ","
                + TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + ","
                + TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + ","
                + " topItem." + TableNameAndColumnStatement.SOURCE_TASK_ID + " as " + TableNameAndColumnStatement.SOURCE_TASK_ID
                + " from "
                + " (SELECT " + TableNameAndColumnStatement.SOURCE_TASK_ID + " from " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE
                + " order by " + TableNameAndColumnStatement.ID + " desc limit 1)topItem  inner join "
                + "(select "
                + TableNameAndColumnStatement.GUARD_CODE + ","
                + TableNameAndColumnStatement.GUARD_ID + ","
                + "kir." + TableNameAndColumnStatement.ID + ","
                + TableNameAndColumnStatement.REQUESTED_ITEMS + ","
                + TableNameAndColumnStatement.REQUESTED_STATUS + ","
                + TableNameAndColumnStatement.REQUESTED_ON + ","
                + TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + ","
                + "ue." + TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + ","
                + TableNameAndColumnStatement.SOURCE_TASK_ID
                + " from " + TableNameAndColumnStatement.TASK_TABLE
                + " t inner join  " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE
                + " kir  on  kir." + TableNameAndColumnStatement.SOURCE_TASK_ID
                + " = t." + TableNameAndColumnStatement.TASK_ID
                + " inner join  " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE
                + " ue  on  ue." + TableNameAndColumnStatement.EMPLOYEE_ID
                + " = kir." + TableNameAndColumnStatement.GUARD_ID
                + " where  t." + TableNameAndColumnStatement.TASK_TYPE_ID + " = " + taskTypeId
                + " and t." + TableNameAndColumnStatement.TASK_STATUS_ID + " in (3,4) "
                + " and t." + TableNameAndColumnStatement.UNIT_ID + " = " + unitId + ") allItems"
                + " on allItems." + TableNameAndColumnStatement.SOURCE_TASK_ID + " = topItem." + TableNameAndColumnStatement.SOURCE_TASK_ID;*/

        //@AR: Changing query as employee table is NA
        String kitItemRequestQuery = "select guard_code, guard_id,id,requested_items,requested_status,requested_on,kit_item_requested_id,guard_name as employee_full_name, " +
                "topItem.source_task_id as source_task_id from (SELECT source_task_id from kit_item_request order by id desc limit 1) topItem inner join " +
                "(select g.guard_code,g.guard_id,kir.id,requested_items,requested_status,requested_on,kit_item_requested_id," +
                "g.guard_name,source_task_id from task t inner join kit_item_request kir on kir.source_task_id = t.task_id inner join " +
                "guards g on g.guard_id= kir.guard_id where t.task_type_id =" + taskTypeId + " and t.task_status_id in (3,4) and t.unit_id = " + unitId + ") " +
                "allItems on allItems.source_task_id = topItem.source_task_id";

        Timber.i("Kit Item Query %s", kitItemRequestQuery);
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(kitItemRequestQuery, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        KitItemRequestMO lastCheckInspectionKitItemRequestMO = new KitItemRequestMO();
                        lastCheckInspectionKitItemRequestMO.setGuardCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        lastCheckInspectionKitItemRequestMO.setGuardId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        lastCheckInspectionKitItemRequestMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        lastCheckInspectionKitItemRequestMO.setRequestedStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REQUESTED_STATUS)));
                        lastCheckInspectionKitItemRequestMO.setKitItemRequstedId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID)));
                        String requestedItems = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REQUESTED_ITEMS));
                        List<KitItemRequestItem> kitItemRequestList = IOPSApplication.getGsonInstance().fromJson(requestedItems, new TypeToken<ArrayList<KitItemRequestItem>>() {
                        }.getType());
                        lastCheckInspectionKitItemRequestMO.setKitItemRequestItems(kitItemRequestList);
                        if (kitItemRequestList != null && kitItemRequestList.size() != 0) {
                            ArrayList<String> kitItemNamesList = new ArrayList<>();

                            for (KitItemRequestItem kitItemRequestItem : kitItemRequestList) {
                                if (kitItemRequestItem != null) {
                                    kitItemNamesList.add(getKitItemList(kitItemRequestItem.getKitItemId(), kitItemRequestItem.getKitItemSizeId()));
                                }
                            }
                            lastCheckInspectionKitItemRequestMO.setKitItemRequestNames(kitItemNamesList);
                        }
                        lastCheckInspectionKitItemRequestMO.setRequestedItems(requestedItems);
                        lastCheckInspectionKitItemRequestMO.setGuardName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME)));
                        lastCheckInspectionKitItemRequestMO.setSourceTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)));
                        lastCheckInspectionKitItemRequestMO.setRequestedOn(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REQUESTED_ON)));
                        lastCheckInspectionkitKitItemRequestMOList.add(lastCheckInspectionKitItemRequestMO);
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lastCheckInspectionkitKitItemRequestMOList;
    }

    public String getKitItemList(Integer kitItemId, Integer kitItemSizeId) {
        String kitItemAndSizeQuery = "";
        String KitItemNameAndKitSizeName = "";
        if (kitItemSizeId != null) {
            if (kitItemId != null) {
                kitItemAndSizeQuery = "select ki.kit_item_name,kzt.kit_size_name from kit_item_size kiz " +
                        " join kit_item ki on ki.kit_item_id = kiz.kit_item_id " +
                        " join kit_size_table kzt on kzt.kit_size_id = kiz.kit_size_id " +
                        " where kiz.kit_item_id = " + kitItemId + " and kiz.kit_item_size_id  = " + kitItemSizeId;
            }
        } else {
            if (kitItemId != null) {
                kitItemAndSizeQuery = "select kit_item_name from kit_item where kit_item_id = " + kitItemId;
            }
        }
        Timber.i("kitItemQuery %s", kitItemAndSizeQuery);
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(kitItemAndSizeQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        if (kitItemSizeId != null) {
                            KitItemNameAndKitSizeName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_NAME))
                                    + " / " + cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_NAME));
                        } else {
                            KitItemNameAndKitSizeName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_NAME));
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return KitItemNameAndKitSizeName;
    }

    public ArrayList<Integer> fetchUnitRiskIds(int unitId, int riskPOAStatus) {
//        ArrayList<LastCheckInspectionUnitAtRiskMO> lastCheckInspectionUnitAtRiskMOList = new ArrayList<>();
        ArrayList<Integer> unitRiskIds = new ArrayList<>();
        String query = " select " + TableNameAndColumnStatement.UNIT_RISK_ID + " from " + TableNameAndColumnStatement.UNIT_RISK_TABLE +
                " where " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId;
        Timber.i("fetchLastInspectionUnitRiskIds %s", query);
        SQLiteDatabase sqlite = null;
        Cursor cursor;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        unitRiskIds.add(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ID)));
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitRiskIds;
    }

    public List<LastCheckInspectionUnitAtRiskMO> getUARPendingList(int unitRiskID, int isPending) {
        List<LastCheckInspectionUnitAtRiskMO> pendingList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE + " where " +
                TableNameAndColumnStatement.IS_COMPLETED + "='" + isPending + "' and " +
                TableNameAndColumnStatement.UNIT_RISK_ID + "='" + unitRiskID + "'";

        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        LastCheckInspectionUnitAtRiskMO lastCheckInspectionUnitAtRiskMO = new LastCheckInspectionUnitAtRiskMO();
                        lastCheckInspectionUnitAtRiskMO.setPlannedClosureDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_DATE)));
                        lastCheckInspectionUnitAtRiskMO.setPlanOfAction(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN)));
                        lastCheckInspectionUnitAtRiskMO.setAssignedTo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO)));
                        int iscompleted = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_COMPLETED));
                        if (iscompleted == 0) {
                            lastCheckInspectionUnitAtRiskMO.setStatus(TableNameAndColumnStatement.Pending);
                        } else {
                            lastCheckInspectionUnitAtRiskMO.setStatus(TableNameAndColumnStatement.COMPLETED);
                        }
                        pendingList.add(lastCheckInspectionUnitAtRiskMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return pendingList;
    }

    /**
     * @param unit_id
     * @return
     */
    public ArrayList<EquipmentListModel> fetchEquipmentList(int unit_id, int post_id) {
        SQLiteDatabase sqlite = null;
        ArrayList<EquipmentListModel> equipmentArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " where " + TableNameAndColumnStatement.UNIT_ID + "= " + unit_id + " and " + TableNameAndColumnStatement.UNIT_POST_ID + "= " + post_id;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        EquipmentListModel mEquipmentListModel = new EquipmentListModel();
                        mEquipmentListModel.setUnitEquipmentId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID)));
                        mEquipmentListModel.setUniqueNumber(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIQUE_NO)));
                        mEquipmentListModel.setManufacturer(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.MANUFACTURER)));
                        mEquipmentListModel.setIsCustomerProvided(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED)));
                        mEquipmentListModel.setEquipmentStatus(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_STATUS)));
                        mEquipmentListModel.setEquipmentType(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE)));
                        mEquipmentListModel.setUnit_id(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        equipmentArrayList.add(mEquipmentListModel);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return equipmentArrayList;
    }

    /**
     * @return
     */
    public int getSequencePostId() {
        int id = 0;
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + TableNameAndColumnStatement.ID + " from " +
                TableNameAndColumnStatement.UNIT_POST_TABLE +
                " where " + TableNameAndColumnStatement.UNIT_POST_ID + " = 0 order by "
                + TableNameAndColumnStatement.ID + " desc limit 1";
        Timber.d("SequencePostId updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        id = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return id;
    }

    public CheckIsMainGateMO checkIsMainGate(int unitId) {
        CheckIsMainGateMO model = new CheckIsMainGateMO();
        model.setIsMaingate(0);
        model.setPostid(0);

        SQLiteDatabase sqlite = null;
        String query = "SELECT " + TableNameAndColumnStatement.IS_MAIN_GATE + " , "
                + TableNameAndColumnStatement.UNIT_POST_ID + " FROM " +
                TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                TableNameAndColumnStatement.UNIT_ID + " =" + unitId + " AND " +
                TableNameAndColumnStatement.IS_MAIN_GATE + "=1";
//        Timber.d("query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        model.setIsMaingate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_MAIN_GATE)));

                        model.setPostid(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));

                    } while (cursor.moveToNext());
                }
                Timber.d("cursor detail isMainGate  %s", model);
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return model;
    }

    public ArrayList<UnitContactsMO> fetchUnitContacts(int unitId) {

        ArrayList<UnitContactsMO> unitContactsList = new ArrayList<>();
        Util util = new Util();
        Cursor cursor = null;

        String query = "select * from unit_contact where unit_id = " + unitId + " ORDER BY unit_id DESC LIMIT 4";
        Timber.i("fetchUnitContacts %s", query);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitContactsMO unitContactsMO = new UnitContactsMO();
                        unitContactsMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        unitContactsMO.setUnitContactId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_ID)));
                        unitContactsMO.setContactFirstName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)));
                        unitContactsMO.setContactLastName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME)));
                        unitContactsMO.setContactNumber(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        unitContactsMO.setContactEmail(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMAIL_ADDRESS)));
                        unitContactsMO.setClientDesignation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESIGNATION)));
                        unitContactsList.add(unitContactsMO);
                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return unitContactsList;
    }

    public ArrayList<UnitEmployee> getAllEmployeeDetails(String text) {

        String employeeSearchID = null;
        ArrayList<UnitEmployee> unitEmployeeIdList = new ArrayList<>();

        employeeSearchID = "select guard_code,guard_id,guard_name,last_fine,fine_amount,reward_amount from " +
                TableNameAndColumnStatement.GUARDS_TABLE + " where " + TableNameAndColumnStatement.GUARD_CODE + " like " + "'" + text + "%'";

//        Timber.d("getAllEmployeeDetails : %s", employeeSearchID);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(employeeSearchID, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        unitEmployee.setEmployeeFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        unitEmployee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));

                        /*if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME))))
                            unitEmployee.setUnitName("NA");
                        else
                            unitEmployee.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));*/

                        unitEmployee.setLastFine(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_FINE)));
                        unitEmployee.setFineAmount(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FINE_AMOUNT)));
                        unitEmployee.setRewardAmount(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REWARD_AMOUNT)));
                        unitEmployeeIdList.add(unitEmployee);

                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitEmployeeIdList;
    }

    public String[] getGuardsCode() {

        String employeeSearchID = null;
        String[] guardCode = null;
        employeeSearchID = "select guard_code from " + TableNameAndColumnStatement.GUARDS_TABLE;
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(employeeSearchID, null);

            if (cursor != null && cursor.getCount() != 0) {

                guardCode = new String[cursor.getCount()];
                int counter = 0;
                if (cursor.moveToFirst()) {
                    do {
                        guardCode[counter] = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE));
                        counter++;
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return guardCode;
    }

    public ArrayList<UnitEmployee> getUnitDetails() {

        ArrayList<UnitEmployee> unitDetailsList = new ArrayList<>();
        String employeeSearchID = "select unit_name,unit_id from unit where unit_id > 0";

//        Timber.d("getAllUNITDetails : %s", employeeSearchID);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(employeeSearchID, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setUnitId(cursor.getInt(cursor.getColumnIndex("unit_id")));
                        unitEmployee.setUnitName(cursor.getString(cursor.getColumnIndex("unit_name")));
                        unitDetailsList.add(unitEmployee);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitDetailsList;
    }

    public LastAkrDetails getLastAkrDetails(int unitId, int guardId) {

        String lastAkrQuery = null;
        String[] minMaxDates = dateTimeFormatConversionUtil.getMinMaxDateForLastAkr();
        String sqlSupportedDateFormat = Util.getSqlSupportedDateTimeFormat(TableNameAndColumnStatement.CREATED_DATE_TIME);
        if (unitId != 0) {
            lastAkrQuery = " select kd.CreatedDateTime as CreatedDateTime ,count(*) as akr_due from "
                    + TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE +
                    " kd join  " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE + " kdi  " +
                    " on kd.kit_distribution_id = kdi.kit_distribution_id and kd.recipient_id = " + guardId +
                    " and distribution_status = 2  and  datetime(" + sqlSupportedDateFormat + ") between  " +
                    " datetime('" + minMaxDates[0] + "') and datetime('" + minMaxDates[1] + "')";
        }

//        Timber.d("LastAkrDetails : %s", lastAkrQuery);
        LastAkrDetails lastAkrDetails = new LastAkrDetails();

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(lastAkrQuery, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        if (TextUtils.isEmpty(cursor.getString(
                                (cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME))))) {
                            lastAkrDetails.setLastAkrIssueDate("NA");
                        } else {
                            String date = dateTimeFormatConversionUtil.convertDateTimeToDate(cursor.getString(
                                    (cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME))));
                            lastAkrDetails.setLastAkrIssueDate(date);
                        }
                        lastAkrDetails.setLastAkrDue(cursor.getString(cursor.getColumnIndex("akr_due")));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        return lastAkrDetails;
    }

    public ArrayList<Object> fetchUnitPosts(int unitID) {
        ArrayList<Object> postList = new ArrayList<>();
        SQLiteDatabase sqlite = null;

        String query = "select * from " + TableNameAndColumnStatement.UNIT_POST_TABLE + " where " + TableNameAndColumnStatement.UNIT_ID + " = " + unitID;

        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                Cursor cursor = sqlite.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        PostData postData = new PostData();
                        postData.setSequenceId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        postData.setIsMainGate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_MAIN_GATE)));
                        postData.setPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                        postData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        postData.setUnitPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        postData.setArmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ARMED_STRENGTH)));
                        postData.setUnarmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNARMED_STRENGTH)));
                        postData.setMainGateDistance(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MAIN_GATE_DISTANCE)));
                        postData.setBarrackDistance(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_DISTANCE)));
                        postData.setUnitOfficeDistance(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE)));
                        postData.setDeviceNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DEVICE_NO)));
                        postData.setLatitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        postData.setLongitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        String geoLocation = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) + "," +
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE));
                        postData.setGeoPoint(geoLocation);
                        postData.setIcon(R.drawable.post);
                        postList.add(postData);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return postList;
    }

    public GpsBatteryInputRequestBaseMO fetchGpsBatteryData() {
        GpsBatteryInputRequestBaseMO gpsBatteryList = new GpsBatteryInputRequestBaseMO();
        List<GpsBatteryInputRequestMOLocation> locationList = new ArrayList<GpsBatteryInputRequestMOLocation>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.GPS_BATTERY_HEALTH_TABLE;

        Cursor cursor = null;


        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        GpsBatteryInputRequestMOLocation location = new GpsBatteryInputRequestMOLocation();

                        String geoLocation = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GPS_LATITUDE))
                                + "," +
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GPS_LONGITUDE));
                        location.setGEOLocation(geoLocation);
                        location.setBatteryPercentage(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.
                                BATTERY_PERCENTAGE)));
                        location.setCapturedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.
                                LOGGED_IN_DATETIME)));

                        locationList.add(location);
                        gpsBatteryList.setLocation(locationList);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return gpsBatteryList;
    }

    public RotaTaskActvityCheckMO checkActivityStatus(int Id) {

        int taskStatusId = 0;
        int taskId = 0;
        RotaTaskActvityCheckMO rotaTaskActvityCheck = new RotaTaskActvityCheckMO();
        String selectQuery = " SELECT " + TableNameAndColumnStatement.TASK_STATUS_ID
                + " , " + TableNameAndColumnStatement.TASK_ID +
                " FROM " + TableNameAndColumnStatement.TASK_TABLE + " WHERE " +
                TableNameAndColumnStatement.ID + " =" + Id;

        Cursor cursor = null;

        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                        taskId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID));
                        rotaTaskActvityCheck.setId(Id);
                        rotaTaskActvityCheck.setTaskId(taskId);
                        rotaTaskActvityCheck.setTaskStatusId(taskStatusId);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return rotaTaskActvityCheck;
    }

    public RotaTaskActivityListMO getAllRotaTaskActivity(int taskTypeId) {

        RotaTaskActivityListMO rotaTaskActivityListMO = new RotaTaskActivityListMO();

        List<RotaTaskListMO> rotaTaskList = new ArrayList<>();

        String selectQuery;
        if (taskTypeId == 0) {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_LIST;
        } else {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_LIST + " where " +
                    TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_ID + "='" + taskTypeId + "'";
        }

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        RotaTaskListMO rotaTaskListMO = new RotaTaskListMO();
                        rotaTaskListMO.setRotaTaskName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_TASK_NAME)));
                        rotaTaskListMO.setRotaTaskActivityId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_ID)));
                        rotaTaskListMO.setRotaTaskWhiteIcon(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_TASK_BLACK_ICON)));
                        rotaTaskListMO.setRotaTaskBlackIcon(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ROTA_TASK_WHITE_ICON)));
                        rotaTaskList.add(rotaTaskListMO);
                        rotaTaskActivityListMO.setRotaTaskListMO(rotaTaskList);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return rotaTaskActivityListMO;
    }

    public AppRole getAppUserDetails() {

        AppRole appRole = new AppRole();
        String selectQuery = "SELECT " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " , "
                + TableNameAndColumnStatement.RANK_ID + " , "
                + TableNameAndColumnStatement.BRANCH_ID +
                " FROM " + TableNameAndColumnStatement.APP_USER_TABLE;

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        appRole.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID)));
                        //appRole.setRoleId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.APP_ROLE))));
                        appRole.setRankId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID)));
                        appRole.setBranchId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BRANCH_ID)));
                        // appRole.setRoleName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.APP_ROLE))));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return appRole;
    }

    public ArrayList<SyncingUnitEquipmentModel> getUnitEquipmentDetails(int postId) {
        ArrayList<SyncingUnitEquipmentModel> equipmentModelArrayList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "SELECT * " + " FROM " +
                TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " where " + TableNameAndColumnStatement.UNIT_POST_ID + " = " + postId;

//        Timber.d("getUnitEquipment selectQuery  %s", selectQuery);

        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        SyncingUnitEquipmentModel model = new SyncingUnitEquipmentModel();
                        model.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        model.setEquipmentId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID)));
                        model.setEquipmentUniqueNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIQUE_NO)));
                        model.setEquipmentManufacturer(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MANUFACTURER)));
                        model.setIsCustomerProvided(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED)));
                        model.setStatus(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_STATUS)));
                        model.setEquipmentTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE)));
                        model.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        model.setPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        AddEquipmentLocalMO localMO = new AddEquipmentLocalMO();
                        localMO.setIsSynced(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_SYNCED)));
                        localMO.setIsNew(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_NEW)));
                        localMO.setEquipmentType(getEquipmentName(model.getEquipmentTypeId()));
                        localMO.setEquipmentTypId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE)));
                        localMO.setEquipmentCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.COUNT)));
                        model.setAddEquipmentLocalMO(localMO);
                        equipmentModelArrayList.add(model);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return equipmentModelArrayList;
    }

    public ArrayList<UnitDetails> getUnitDetailsFromUnitName(int unit_id) {
        SQLiteDatabase sqlite = null;
        String query = "select " + TableNameAndColumnStatement.UNIT_ID + ","
                + TableNameAndColumnStatement.UNIT_NAME + " , "
                + TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID + " , "
                + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + " from "
                + TableNameAndColumnStatement.UNIT_TABLE
                + " where " + TableNameAndColumnStatement.UNIT_ID + "='" + unit_id + "'";

        ArrayList<UnitDetails> unitDetailList = new ArrayList<>();
        List<Integer> barrackIdList;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {

                    UnitDetails unitDetails = new UnitDetails();
                    unitDetails.setUnitID(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                    unitDetails.setBillAuthoritativeUnit(cursor.getInt(cursor.getColumnIndex
                            (TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID)));
                    barrackIdList = checkBarrackAvailableForUnit(unitDetails.getUnitID());

                    unitDetails.setBarrackID(barrackIdList);
                    unitDetails.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
//                    unitDetails.setTaskTypeID(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID))));
                    unitDetails.setUnitAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                    unitDetailList.add(unitDetails);
                } while (cursor.moveToNext());
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
        return unitDetailList;
    }

    private synchronized List<Integer> checkBarrackAvailableForUnit(int unit_Id) {
        int isBarrackAvailable = 0;

        List<Integer> barrackIdList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query = "select " + TableNameAndColumnStatement.BARRACK_ID + " from "
                + TableNameAndColumnStatement.UNIT_BARRACK_TABLE
                + " where " + TableNameAndColumnStatement.UNIT_ID + "='" + unit_Id + "'";

        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {

                    isBarrackAvailable = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID));
                    barrackIdList.add(isBarrackAvailable);

                } while (cursor.moveToNext());
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return barrackIdList;
    }

    public List<UnitMonthlyBillDetail> getListOfCollectionMonths(int unitId) {

        SQLiteDatabase sqlite = null;
        String query = "select * from " + TableNameAndColumnStatement.UnitBillingStatusTable
                + " where " + TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID + "='" + unitId + "' and " +
                TableNameAndColumnStatement.IS_BILL_COLLECTED + "='" + 0 + "' order by "
                + TableNameAndColumnStatement.Bill_YEAR + " asc, "
                + TableNameAndColumnStatement.Bill_MONTH + " asc ";

//        System.out.println("Selection :" + query);
        ArrayList<UnitMonthlyBillDetail> billCollactionMonthMOList = new ArrayList<>();
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {

                    UnitMonthlyBillDetail billCollactionMonthMO = new UnitMonthlyBillDetail();

                    billCollactionMonthMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_COLLACTION_ID)));
                    billCollactionMonthMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID)));
                    billCollactionMonthMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_COLLACTION_UNIT_NAME)));
                    billCollactionMonthMO.setBillNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_NUMBER)));
                    billCollactionMonthMO.setBillMonth(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_MONTH)));
                    billCollactionMonthMO.setBillYear(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.Bill_YEAR)));
                    billCollactionMonthMO.setOutstandingAmount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OUTSTANDING_AMOUNT)));
                    billCollactionMonthMO.setOutstandingDays(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OUTSTANDING_DAYS)));
                    billCollactionMonthMO.setUpdatedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UPDATED_DATES)));
                    billCollactionMonthMO.setIsBillCollected(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_BILL_COLLECTED)));

                    billCollactionMonthMOList.add(billCollactionMonthMO);
                } while (cursor.moveToNext());
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (sqlite != null && !sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return billCollactionMonthMOList;
    }

    /**
     * @return
     */
    public String getAppUserName() {
        //my performance screen
        String username = "";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        String updateQuery = "select " + TableNameAndColumnStatement.FIRST_NAME + ","
                + TableNameAndColumnStatement.LAST_NAME +
                " from " +
                TableNameAndColumnStatement.APP_USER_TABLE;
        Timber.d("getAppUserName updateQuery  %s", updateQuery);

        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            String first_name, last_name;

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    first_name = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME));
                    last_name = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME));
                    if (last_name != null && !last_name.isEmpty()) {
                        username = first_name + " " + last_name;
                    } else {
                        username = first_name;
                    }
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return username;
    }

    public ArrayList<LookupModel> getLookupData(String lookupTypeName, String lookupCode) {

        ArrayList<LookupModel> lookupModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query = lookupCode == " " || lookupCode == null ?
                ("select " + TableNameAndColumnStatement.LOOKUP_TYPE_ID + ","
                        + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + ","
                        + TableNameAndColumnStatement.LOOKUP_NAME + ","
                        + TableNameAndColumnStatement.ID
                        + " from " +
                        TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                        + " where "
                        + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                        + " = '" + lookupTypeName + "'")
                :
                ("select " + TableNameAndColumnStatement.LOOKUP_TYPE_ID + ","
                        + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + ","
                        + TableNameAndColumnStatement.LOOKUP_NAME + ","
                        + TableNameAndColumnStatement.ID
                        + " from " +
                        TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                        + " where "
                        + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                        + " = '" + lookupTypeName + "'"
                        + " and lookup_code = '" + lookupCode + "'");

//        Timber.d("getLookupData Query  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        LookupModel lookupModel = new LookupModel();
                        lookupModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        lookupModel.setLookupIdentifier(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER)));
                        lookupModel.setLookupTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_ID)));
                        //lookupModel.setLookupTypeName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_NAME))));
                        lookupModel.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME)));
                        lookupModelList.add(lookupModel);
                    }
                    while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        LookupModel lookupModel = new LookupModel();
        if (lookupModelList.size() == 0) {
            if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.IssueCategory)) {
                lookupModel.setName("No Grievance Category Available");
                lookupModelList.add(0, lookupModel);
            } else if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.GrievanceType)) {
                lookupModel.setName("No Grievance Sub Category Available");
                lookupModelList.add(0, lookupModel);
            }

        } else {
            if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.IssueCategory)) {
                lookupModel.setName(TableNameAndColumnStatement.select_greviance_category);
                lookupModelList.add(0, lookupModel);
            } else if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.GrievanceType)) {
                lookupModel.setName(TableNameAndColumnStatement.select_greviance_sub_category);
                lookupModelList.add(0, lookupModel);
            }
        }
        return lookupModelList;
    }

    public ArrayList<LookupModel> getLookupDataWithNoGrievance(String lookupTypeName, String lookupCode) {

        ArrayList<LookupModel> lookupModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query = "select " + TableNameAndColumnStatement.LOOKUP_TYPE_ID + ","
                + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + "," + TableNameAndColumnStatement.LOOKUP_NAME + ","
                + TableNameAndColumnStatement.ID + " from " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE + " where "
                + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = '" + lookupTypeName + "'"
                + " and lookup_code = '" + lookupCode + "' and not lookup_identifier='13'";

//        Timber.d("getLookupData Query NoGrievance %s", query);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        LookupModel lookupModel = new LookupModel();
                        lookupModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        lookupModel.setLookupIdentifier(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER)));
                        lookupModel.setLookupTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_ID)));
                        lookupModel.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME)));
                        lookupModelList.add(lookupModel);
                    }
                    while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        LookupModel lookupModel = new LookupModel();
        if (lookupModelList.size() == 0) {
            if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.IssueCategory)) {
                lookupModel.setName("No Grievance Category Available");
                lookupModelList.add(0, lookupModel);
            } else if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.GrievanceType)) {
                lookupModel.setName("No Grievance Sub Category Available");
                lookupModelList.add(0, lookupModel);
            }

        } else {
            if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.IssueCategory)) {
                lookupModel.setName(TableNameAndColumnStatement.select_greviance_category);
                lookupModelList.add(0, lookupModel);
            } else if (lookupTypeName.equalsIgnoreCase(TableNameAndColumnStatement.GrievanceType)) {
                lookupModel.setName(TableNameAndColumnStatement.select_greviance_sub_category);
                lookupModelList.add(0, lookupModel);
            }
        }
        return lookupModelList;
    }

    public String getOtherReason(String lookupTypeName, String lookupCode, int lookUpIdentifier) {

        String lookupName = "";

        SQLiteDatabase sqlite = null;
        String query = lookupCode == " " || lookupCode == null ?
                ("select "
                        + TableNameAndColumnStatement.LOOKUP_NAME
                        + " from " +
                        TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                        + " where "
                        + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                        + " = '" + lookupTypeName + "'"
                        + " and "
                        + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + "=" + lookUpIdentifier) :
                ("select " + TableNameAndColumnStatement.LOOKUP_TYPE_ID
                        + " from " +
                        TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                        + " where "
                        + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                        + " = '" + lookupTypeName + "'"
                        + " and lookup_code = '" + lookupCode + " ' "
                        + " and "
                        + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + "=" + lookUpIdentifier);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        lookupName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME));
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lookupName;
    }

    public int getAreaInspectorId() {

        int areaInspectorId = 0;
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.EMPLOYEE_ID + " from " +
                TableNameAndColumnStatement.APP_USER_TABLE;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        areaInspectorId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return areaInspectorId;
    }

    public ArrayList<ComplaintInfoMO> getClientContactNames(int unitId) {
        ArrayList<ComplaintInfoMO> taskTypeIdList = new ArrayList<>();
        String query = " select first_name,last_name,unit_contact_id from unit_contact where unit_id = " + unitId;

        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        ComplaintInfoMO mComplaintInfoMO = new ComplaintInfoMO();
                        String firstName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME));
                        String secondName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME));
                        int unitContactId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_ID));
                        mComplaintInfoMO.setmFirstName(firstName);
                        mComplaintInfoMO.setmLastName(secondName);
                        mComplaintInfoMO.setmUnitContactId(unitContactId);
                        taskTypeIdList.add(mComplaintInfoMO);

                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return taskTypeIdList;
    }

    public ArrayList<QuestionsData> getClientCoordinationQuestions() {

        ArrayList<QuestionsData> questionsList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = null;
        String query = "select * from " +
                TableNameAndColumnStatement.client_coordination_table
                + " where "
                + TableNameAndColumnStatement.is_active
                + "='" + 1 + "'";

        Cursor cursor = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        QuestionsData questionsData = new QuestionsData();
                        questionsData.questionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.question_id));
                        questionsData.question = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.question));
                        //questionsData.isActive = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.is_active)));

                        questionsList.add(questionsData);
                    }
                    while (cursor.moveToNext());
                }

            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }

        }
        return questionsList;
    }

    public ArrayList<LookUpCheckListItem> getBillSubmissionCheckList(int unitId) {

        ArrayList<LookUpCheckListItem> checkListItemsList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = null;
        String query = "select * from " +
                TableNameAndColumnStatement.LOOKUP_MODEL_TABLE
                + " where "
                + TableNameAndColumnStatement.LOOKUP_TYPE_NAME
                + "= 'BillSubmissionCheckList' ";

        String query1 = "select " + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + " , " +
                TableNameAndColumnStatement.LOOKUP_NAME + " from (( select " + TableNameAndColumnStatement.UNIT_ID + " , " +
                TableNameAndColumnStatement.BILL_CHECK_LIST_ID + " from " + TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE +
                ") ucl inner join (select " + TableNameAndColumnStatement.LOOKUP_IDENTIFIER + " , " + TableNameAndColumnStatement.LOOKUP_NAME +
                " from " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE + " where " +
                TableNameAndColumnStatement.LOOKUP_TYPE_NAME + "= 'BillSubmissionCheckList' )l on l." + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                " = ucl." + TableNameAndColumnStatement.BILL_CHECK_LIST_ID + ") where " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId;
        Timber.d("getBillSubmissionCheckList Query  %s", query);
        Cursor cursor = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(query1, null);
            if (cursor != null) {


                if (cursor.getCount() != 0) {
                    getlookUpData(cursor, checkListItemsList);
                } else {
                    cursor = sqLiteDatabase.rawQuery(query, null);
                    getlookUpData(cursor, checkListItemsList);
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }

        }
        return checkListItemsList;
    }

    private void getlookUpData(Cursor cursor, ArrayList<LookUpCheckListItem> checkListItemsList) {
        if (!cursor.isClosed()) {
            if (cursor.moveToFirst()) {
                do {
                    LookUpCheckListItem lookUpCheckListItem = new LookUpCheckListItem();
                    lookUpCheckListItem.setCheckListItemId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER)));
                    lookUpCheckListItem.setCheckListItem(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME)));
                    //questionsData.isActive = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.is_active));

                    checkListItemsList.add(lookUpCheckListItem);
                }
                while (cursor.moveToNext());
            }

        }
    }

    public GenericUpdateTableMO getTableSeqId(String columnName1, int id, String columnName2, String tableName, String columName3, SQLiteDatabase sqlite) {
        GenericUpdateTableMO genericUpdateTableMO = new GenericUpdateTableMO();
        String Query = "select count(" + columnName1 + ") as isAvailable ," + columnName2 + "," + columnName1 + " from " +
                tableName + " where " + columnName1 + " = " + id;
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(Query, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        genericUpdateTableMO.setId(cursor.getInt(cursor.getColumnIndex(columnName2)));
                        genericUpdateTableMO.setIsAvailable(cursor.getInt(cursor.getColumnIndex("isAvailable")));
                    } while (cursor.moveToNext());
                }
            }
            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return genericUpdateTableMO;
    }

    public GenericUpdateTableMO getTableSeqId(int unitId, int rankId, String tableName, SQLiteDatabase sqlite) {
        GenericUpdateTableMO genericUpdateTableMO = new GenericUpdateTableMO();
//        String Query = "select count(1) as isAvailable, " + columnName2 + " from " + tableName + " where unit_id=" + unitId + " and rank_id=" + rankId;
        String Query = "select count(1) as isAvailable from " + tableName + " where unit_id=" + unitId + " and rank_id=" + rankId;
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(Query, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
//                        genericUpdateTableMO.setId(cursor.getInt(cursor.getColumnIndex(columnName2)));
                        genericUpdateTableMO.setIsAvailable(cursor.getInt(cursor.getColumnIndex("isAvailable")));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return genericUpdateTableMO;
    }

    private String getEquipmentName(int equipmentTypeId) {
        String name = "";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        String updateQuery = "select " + TableNameAndColumnStatement.EQUIPMENT_NAME +
                " from " +
                TableNameAndColumnStatement.EQUIPMENT_LIST_TABLE +
                " where " + TableNameAndColumnStatement.ID + " =  " + equipmentTypeId;
        Timber.d("getEquipmentName  %s", updateQuery);

        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_NAME));
                }

            }

            Timber.d("cursor detail object  %s", name);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return name;
    }

    //Adding below method to validate if AI is trying to do task on same day on same unit
    public boolean isSameUnitTaskAlreadyDone(int unitId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select count(1) as taskCount from task where unit_id=" + unitId + " and task_status_id in (3,4) " +
                        "and substr(actual_execution_end_date_time,0,11)='" + dateTimeFormatConversionUtil.getCurrentDate() + "'";

//                Timber.d("Query isSameUnitTaskAlreadyDone %s", selectQuery);

                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int count = cursor.getInt(cursor.getColumnIndex("taskCount"));
                    if (count > 0)
                        return true;
                    else
                        return false;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return false;
    }

    //USE LESS METHOD FOR THE MOMENT
    /*private int getAttachmentSourceTypeFromLookup(String lookupTypeName) {
        int sourceTypeId = 0;
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        String updateQuery = "select " + TableNameAndColumnStatement.LOOKUP_IDENTIFIER +
                " from " +
                TableNameAndColumnStatement.LOOKUP_MODEL_TABLE +
                " where " + TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " =  '" + lookupTypeName + "'";
        Timber.d("getEquipmentName  %s", updateQuery);

        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    sourceTypeId = cursor
                            .getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER));
                }

            }

            Timber.d("cursor detail object  %s", sourceTypeId);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return sourceTypeId;
    }

    public String getCollectionDays(int unitid) {
        String collectionDays = "";
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + "MAX(" + TableNameAndColumnStatement.OUTSTANDING_DAYS + ") " +
                "as " + TableNameAndColumnStatement.OUTSTANDING_DAYS + " from " +
                TableNameAndColumnStatement.UnitBillingStatusTable +
                " where " + TableNameAndColumnStatement.UNIT_ID + " =  " + unitid;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int cunt = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OUTSTANDING_DAYS));

                        collectionDays = String.valueOf(cunt);

                    } while (cursor.moveToNext());
                }
            }
            Timber.d("cursor detail object  %s", collectionDays);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                cursor.close();
                sqlite.close();
            }
        }
        return collectionDays;
    }

    public String getBarrackNameById(int barrackId) {

        String barrackName = "";
        SQLiteDatabase sqlite = null;
        String selectQuery = "select " + TableNameAndColumnStatement.BARRACK_NAME + " from " +
                TableNameAndColumnStatement.BARRACK_TABLE + " where " +
                TableNameAndColumnStatement.BARRACK_ID + " ='" + barrackId + "'";
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        barrackName = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_NAME));

                    } while (cursor.moveToNext());
                }

            }


        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        return barrackName;
    }

    public int getSequenceGrievanceId() {
        int id = 0;
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + TableNameAndColumnStatement.ID + " from " +
                TableNameAndColumnStatement.GRIEVANCE_TABLE +
                " where " + TableNameAndColumnStatement.ISSUE_ID + " = 0 order by "
                + TableNameAndColumnStatement.ID + " desc limit 1";
//        Timber.d("SequencePostId updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        id = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));

                    } while (cursor.moveToNext());
                }

            }

//            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            cursor.close();
            sqlite.close();
        }
        return id;
    }

    public int getTaskCount() {
        String countQuery = "SELECT  * FROM " + TableNameAndColumnStatement.TASK_TABLE
                + " WHERE " + (TableNameAndColumnStatement.TASK_STATUS_ID) + " = '2'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int checkActualValueIsFilled(double unitId, double taskId, double guardTypeId) {
        String query = "SELECT " + TableNameAndColumnStatement.IS_FILLED + " FROM "
                + TableNameAndColumnStatement.UNIT_BARRACK_STRENGTH_TABLE +
                " WHERE " + (TableNameAndColumnStatement.UNIT_ID) + " = " + unitId
                + " AND " + TableNameAndColumnStatement.TASK_ID + " = " + taskId
                + " AND " + TableNameAndColumnStatement.GUARD_TYPE_ID + " = " + guardTypeId;
        Cursor cursor = null;
//        Timber.d("query is filled:" + query);
        String value = "0";
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        value = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.IS_FILLED));
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return Integer.parseInt(value);
    }

    public ArrayList<String> getAttachmentPaths(int taskId, int referenceTypeId, int unitId) {

        ArrayList<String> imageFilePathList = new ArrayList<>();

        String selectQuery = " SELECT " + TableNameAndColumnStatement.FILE_NAME +
                " FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " WHERE " +
                TableNameAndColumnStatement.TASK_ID + " = " + taskId +
                " AND " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId +
                " AND " + TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE +
                " = " + referenceTypeId;

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        String path = AppPreferences.getCommonImagePath() + "/"
                                + cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_NAME));
                        if (util.isImagePathExist(path))
                            imageFilePathList.add(path);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return imageFilePathList;

    }

    public ArrayList<UnitEmployee> getAllEmployeesFromBranch(String text) {
        ArrayList<UnitEmployee> unitEmployeeIdList = new ArrayList<>();

        String query = "select " + TableNameAndColumnStatement.GUARD_ID + "," +
                TableNameAndColumnStatement.GUARD_CODE + "," +
                TableNameAndColumnStatement.GUARD_NAME + " where " +
                TableNameAndColumnStatement.EMPLOYEE_NO + " like " + "'" + text + "%'";

        Timber.d("empolyeeSearchId : %s", query);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        unitEmployee.setEmployeeFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        unitEmployee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        unitEmployeeIdList.add(unitEmployee);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        return unitEmployeeIdList;

    }

    public ArrayList<UnitEmployee> getAllEmployeeIdByEmployeeNo(double unitId, String text) {
        ArrayList<UnitEmployee> unitEmployeeIdList = new ArrayList<UnitEmployee>();
        String empolyeeSearchId = "select * from " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE + " where " +
                TableNameAndColumnStatement.UNIT_ID + "=" + unitId + " and (" + TableNameAndColumnStatement.EMPLOYEE_NO
                + " like " + "'" + text + "%'" + " or " + TableNameAndColumnStatement.EMPLOYEE_ID + "= '" + text + "' )";

        Timber.d("empolyeeSearchId : %s", empolyeeSearchId);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(empolyeeSearchId, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID)));
                        unitEmployee.setEmployeeFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME)));
                        unitEmployee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_NO)));
                        unitEmployeeIdList.add(unitEmployee);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        return unitEmployeeIdList;

    }

    public int getAttachmentsCount() {
        SQLiteDatabase sqlite = null;
        int count = 0;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                Cursor cursor = sqlite.query(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, null, null, null, null, null, null);
                count = cursor.getCount();
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }
        return count;
    }

    public ArrayList<Object> fetchSitePosts(int unitId) {
        ArrayList<Object> postCheckedList = new ArrayList<Object>();
        ArrayList<Object> postUnCheckedList = new ArrayList<Object>();
        SQLiteDatabase sqlite = null;

        String whereClause = TableNameAndColumnStatement.UNIT_ID +
                " = ? ";
        String[] placeHolderParameter = {String.valueOf(unitId)};


        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                Cursor cursor = sqlite.query(
                        TableNameAndColumnStatement.UNIT_POST_TABLE,
                        new String[]{
                                TableNameAndColumnStatement.UNIT_POST_NAME,
                                TableNameAndColumnStatement.UNIT_ID,
                                TableNameAndColumnStatement.UNIT_POST_ID,
                                TableNameAndColumnStatement.POST_STATUS},
                        whereClause, placeHolderParameter, null, null, null);
                if (cursor.moveToFirst()) {
                    do {

                        // orderDetail.setPrice(mCursor.getString(mCursor.getColumnIndex(KEY_PRICE)));
                        int postStatus = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.POST_STATUS));
                        if (postStatus == PostStatusEnum.CHECKED.getValue()) {
                            if (!(postCheckedList.contains(PostStatusEnum.valueOf(1).name())))
                                postCheckedList.add(PostStatusEnum.valueOf(1).name());
                            PostData postData = new PostData();
                            postData.setPostStatus(postStatus);
                            postData.setPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                            postData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            postData.setUnitPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                            postData.setIcon(R.drawable.post);
                            postCheckedList.add(postData);
                        } else {
                            if (!(postUnCheckedList.contains(PostStatusEnum.valueOf(0).name())))
                                postUnCheckedList.add(PostStatusEnum.valueOf(0).name());
                            PostData postData = new PostData();
                            postData.setPostStatus(postStatus);
                            postData.setPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                            postData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                            postData.setUnitPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                            postData.setIcon(R.drawable.post);
                            postUnCheckedList.add(postData);
                        }


                    } while (cursor.moveToNext());
                    postCheckedList.addAll(postUnCheckedList);
                }


            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return postCheckedList;
    }

    public ArrayList<UnitEmployee> getAllEmployeeId(int unitId, String text) {

        String empolyeeSearchId = null;
        ArrayList<UnitEmployee> unitEmployeeIdList = new ArrayList<>();
        if (unitId != 0) {
            empolyeeSearchId = "select * from " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE + " where " +
                    TableNameAndColumnStatement.UNIT_ID + "=" + unitId + " and " + TableNameAndColumnStatement.EMPLOYEE_NO + " like " + "'" + text + "%'";
        } else {

            empolyeeSearchId = "select u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    " ue." + TableNameAndColumnStatement.EMPLOYEE_NO + "," +
                    " ue." + TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + "," +
                    " ue." + TableNameAndColumnStatement.EMPLOYEE_ID +
                    " from " + TableNameAndColumnStatement.UNIT_TABLE +
                    "  u inner join " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE +
                    " ue  on u." + TableNameAndColumnStatement.UNIT_ID + "=" + "ue." +
                    TableNameAndColumnStatement.UNIT_ID + " where " +
                    TableNameAndColumnStatement.EMPLOYEE_NO + " like " + "'" + text + "%'";
        }

//        Timber.d("empolyeeSearchId : %s", empolyeeSearchId);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(empolyeeSearchId, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_ID)));
                        unitEmployee.setEmployeeFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME)));
                        unitEmployee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMPLOYEE_NO)));
                        if (unitId == 0) {
                            unitEmployee.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));

                        } else {
                            unitEmployee.setUnitName(null);

                        }
                        unitEmployee.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));

                        unitEmployeeIdList.add(unitEmployee);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        return unitEmployeeIdList;

    }

    public ArrayList<UnitEmployee> getAllEmployeeDetails(int unitId, String text) {

        String empolyeeSearchId = null;
        ArrayList<UnitEmployee> unitEmployeeIdList = new ArrayList<UnitEmployee>();
        if (unitId != 0) {
            empolyeeSearchId = "select u.unit_name as unit_name,g.guard_code as guard_code,g.guard_id as guard_id,g.guard_name as guard_name," +
                    " g.last_fine as last_fine,g.fine_amount as fine_amount,g.reward_amount as reward_amount  from " + TableNameAndColumnStatement.GUARDS_TABLE +
                    " g join unit u  on u.unit_id = g.unit_id and g." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + unitId + " where " + TableNameAndColumnStatement.GUARD_CODE + " like " + "'" + text + "%'";
        } else {
            empolyeeSearchId = "select u.unit_name as unit_name,u.unit_id as unit_id, g.guard_code as guard_code,g.guard_id as guard_id,g.guard_name as guard_name " +
                    " from " + TableNameAndColumnStatement.GUARDS_TABLE +
                    " g  left join unit u  on u.unit_id = g.unit_id " + " where " + TableNameAndColumnStatement.GUARD_CODE + " like " + "'" + text + "%'";
        }

//        Timber.d("getAllEmployeeDetails : %s", empolyeeSearchId);

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(empolyeeSearchId, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        UnitEmployee unitEmployee = new UnitEmployee();
                        unitEmployee.setEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        unitEmployee.setEmployeeFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        unitEmployee.setEmployeeNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));

                        if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)))) {
                            unitEmployee.setUnitName("NA");
                        } else {
                            unitEmployee.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        }

                        if (unitId != 0) {
                            unitEmployee.setUnitId(unitId);
                            unitEmployee.setLastFine(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_FINE)));
                            unitEmployee.setFineAmount(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FINE_AMOUNT)));
                            unitEmployee.setRewardAmount(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REWARD_AMOUNT)));
                        } else {
                            unitEmployee.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        }
                        unitEmployeeIdList.add(unitEmployee);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        return unitEmployeeIdList;

    }

  //being used in the postcheck fragment for syncing the add post data to server
    public ArrayList<SyncUnitAddPostModel> getPostDetails() {

        SQLiteDatabase sqlite = null;
        ArrayList<SyncUnitAddPostModel> addPostList = new ArrayList<SyncUnitAddPostModel>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                " where " + TableNameAndColumnStatement.IS_NEW + "=" + 1 + " OR " +
                TableNameAndColumnStatement.IS_SYNCED + "=" + 0;
        Timber.d("PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        SyncUnitAddPostModel postData = new SyncUnitAddPostModel();
                        postData.setIsMainGate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_MAIN_GATE)));
                        postData.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        postData.setUnitPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                        postData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        postData.setUnitPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        String geoLocation = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) + "," +
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE));
                        postData.setDescription("");
                        postData.setIsActive(1);
                        postData.setGeoPoint(geoLocation);
                        postData.setMainGateDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.MAIN_GATE_DISTANCE)));
                        postData.setBarrackDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_DISTANCE)));
                        postData.setUnitOfficeDistance(cursor.getDouble(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE)));
                        postData.setArmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ARMED_STRENGTH)));
                        postData.setUnarmedStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNARMED_STRENGTH)));

                        addPostList.add(postData);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }


            Timber.d("addPostList object  %s", IOPSApplication.getGsonInstance().toJson(addPostList));

        }
        return addPostList;
    }

    public ArrayList<Integer> getTaskTypeIds(int id, String currentDate, boolean FromMyBarrack) {
        ArrayList<Integer> taskTypeIdList = new ArrayList<>();

        String query;
        if (FromMyBarrack) {
            query = "select task_type_id from  task t  inner join rota_task rt on rt.task_id = t.task_id where barrack_id = " + id + " and "
                    + "created_date = " + "'" + currentDate + "'" + " and task_status_id in(1,2,4,5,6)";
        } else {
            query = "select task_type_id from task t  inner join rota_task rt on rt.task_id = t.task_id  where unit_id = " + id + " and "
                    + "created_date = " + "'" + currentDate + "'" + " and task_status_id in(1,2,4,5,6)";
        }
        Timber.i("UnitDetails %s", query);


        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);


            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        int taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        taskTypeIdList.add(taskTypeId);

                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return taskTypeIdList;
    }

    public ArrayList<Integer> getOtherTaskReasonIds(int unitId, int taskTypeId, String currentDate) {
        ArrayList<Integer> otherTaskReasonIds = new ArrayList<>();
        String query = "select other_task_reason_id from  task where unit_id = " + unitId + " and "
                + "created_date = " + "'" + currentDate + "' and task_type_id = " + taskTypeId;

        Timber.i("OtherTaskReason %s", query);
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        int otherReasonId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID));
                        otherTaskReasonIds.add(otherReasonId);
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return otherTaskReasonIds;
    }

    public ArrayList<SingleUnitTasksMO> fetchSingleUnitTasks(int unitId) {

        ArrayList<SingleUnitTasksMO> myUnitsList = new ArrayList<SingleUnitTasksMO>();
        Util util = new Util();
        String query = "select unit_id,task_type_id,estimated_execution_start_date_time,count(task_status_id) as count , task_status_id from task where unit_id = " + unitId + " group by  task_type_id,task_status_id";
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            String billingStatus = "";

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {

                        SingleUnitTasksMO singleUnitTaskMO = new SingleUnitTasksMO();
                        singleUnitTaskMO.setUnitTaskType(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                        //singleUnitTaskMO.setCompletedCount();
                        //singleUnitTaskMO.setPendingCount();
                        singleUnitTaskMO.setLastCheckDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME)));

                    } while (cursor.moveToNext());
                }

                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return myUnitsList;
    }

    //being used in the postcheck fragment for syncing the add post data to server
    public ArrayList<SyncingUnitEquipmentModel> getUnitEquipmensDetails() {

        SQLiteDatabase sqlite = null;
        ArrayList<SyncingUnitEquipmentModel> equipmentList = new ArrayList<SyncingUnitEquipmentModel>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " where " + TableNameAndColumnStatement.IS_NEW + "=1 OR " +
                TableNameAndColumnStatement.IS_SYNCED + "=0";
        Timber.d("PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        SyncingUnitEquipmentModel unitEquipment = new SyncingUnitEquipmentModel();
                        unitEquipment.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        unitEquipment.setEquipmentId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE)));
                        unitEquipment.setIsCustomerProvided(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED)));
                        unitEquipment.setStatus(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_STATUS)));
                        unitEquipment.setEquipmentUniqueNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIQUE_NO)));
                        unitEquipment.setEquipmentManufacturer(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MANUFACTURER)));
                        unitEquipment.setPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        unitEquipment.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        equipmentList.add(unitEquipment);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            Timber.d("equipmentList object  %s", IOPSApplication.getGsonInstance().toJson(equipmentList));

        }
        return equipmentList;
    }

    public ArrayList<SyncingMetaData> getMetaDataDetails() {

        SQLiteDatabase sqlite = null;
        ArrayList<SyncingMetaData> metaDataArrayList = new ArrayList<SyncingMetaData>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " where " + TableNameAndColumnStatement.IS_NEW + "=1 OR " +
                TableNameAndColumnStatement.IS_SYNCED + "=0";
        Timber.d("PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {


                        SyncingMetaData metaData = new SyncingMetaData();
                        metaData.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        metaData.setAttachmentTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_NAME)));
                        metaData.setAttachmentExtension(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_EXTENSION)));
                        metaData.setSizeInKB(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_SIZE)));
                        metaData.setAttachmentSourceTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)));
                        metaData.setAttachmentSourceId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                        metaData.setSequenceNo(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SEQUENCE_NO)));
                        metaData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        metaData.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        metaData.setAttachmentTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_ID)));

                        metaDataArrayList.add(metaData);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return metaDataArrayList;
    }

    public ArrayList<SyncingMetaData> getMetaDataToSync() {

        SQLiteDatabase sqlite = null;
        ArrayList<SyncingMetaData> metaDataArrayList = new ArrayList<SyncingMetaData>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " where " + TableNameAndColumnStatement.IS_NEW + "=1 OR " +
                TableNameAndColumnStatement.IS_SYNCED + "=0";
        Timber.d("PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d("cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {


                        SyncingMetaData metaData = new SyncingMetaData();
                        // metaData.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID))));
                        metaData.setAttachmentTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_NAME)));
                        metaData.setAttachmentExtension(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_EXTENSION)));
                        metaData.setSizeInKB(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_SIZE)));
                        metaData.setAttachmentSourceTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)));
                        metaData.setAttachmentSourceId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                        metaData.setSequenceNo(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SEQUENCE_NO)));
                        metaData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        metaData.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        metaData.setAttachmentSourceId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_ID)));

                        metaDataArrayList.add(metaData);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return metaDataArrayList;
    }
    */
}