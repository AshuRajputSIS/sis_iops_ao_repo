package com.sisindia.ai.android.issues.models;

/**
 * Created by Durga Prasad on 25-05-2016.
 */
public class NatureOfGrievanceMO {

    private String natureOfGrievance;

    public String getNatureOfGrievance() {
        return natureOfGrievance;
    }

    public void setNatureOfGrievance(String natureOfGrievance) {
        this.natureOfGrievance = natureOfGrievance;
    }
}
