package com.sisindia.ai.android.myperformance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shruti on 19-07-2016.
 */
public class TimeLineModel {

    @SerializedName("UnitName")
    @Expose
    private String location;

    @SerializedName("TaskType")
    @Expose
    private String taskName;

    @SerializedName("TaskStartTime")
    @Expose
    private String startTime;

    @SerializedName("TaskEndtime")
    @Expose
    private String endTime;

    @SerializedName("TaskTypeId")
    @Expose
    private int taskTypeId;

    @SerializedName("TimeLineDatetime")
    @Expose
    private String dutyOnOffDateTime;

    @SerializedName("Distance")
    @Expose
    private double distance;

    @SerializedName("Status")
    @Expose
    private String status;

    private int iconType;
    private String date;
    private String taskId;
    private String activity;
    private int taskStatusId;
    private int taskCounter;

    public int getTaskCounter() {
        return taskCounter;
    }

    public void setTaskCounter(int taskCounter) {
        this.taskCounter = taskCounter;
    }

    public String getDutyOnOffDateTime() {
        return dutyOnOffDateTime;
    }

    public void setDutyOnOffDateTime(String dutyOnOffDateTime) {
        this.dutyOnOffDateTime = dutyOnOffDateTime;
    }

    public int getIconType() {
        return iconType;
    }

    public void setIconType(int iconType) {
        this.iconType = iconType;
    }

    public int getTaskStatusId() {
        return taskStatusId;
    }

    public void setTaskStatusId(int taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
