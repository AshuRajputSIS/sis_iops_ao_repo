package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingStrengthOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;




public class UnitRaisingStrengthSync extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, UnitRaisingStrengthOR> unitRaisingStrengthList;
    private int unitRaisingId;

    public UnitRaisingStrengthSync(Context mcontext, int unitRaisingId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        this.unitRaisingId = unitRaisingId;
        unitRaisingStrengthSyncing();
    }

    private  void unitRaisingStrengthSyncing() {
        unitRaisingStrengthList = getUnitRaisingStrengthDetails();
        if (unitRaisingStrengthList != null && unitRaisingStrengthList.size() != 0) {
            updateUnitRaisingStrengthSyncCount(unitRaisingStrengthList.size());
            for (Integer key : unitRaisingStrengthList.keySet()) {
                UnitRaisingStrengthOR unitRaisingStrengthOR = unitRaisingStrengthList.get(key);
                unitRaisingStrengthApiCall(unitRaisingStrengthOR, key);
                Timber.d(Constants.TAG_SYNCADAPTER+"unitPostSyncing count   %s", "count: " + count);
                Timber.d("unitPostSyncing   %s", "key: " + key + " value: " + unitRaisingStrengthList.get(key));
            }

        } else {
            updateUnitRaisingStrengthSyncCount(0);
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitAddPostData -->nothing to sync ");
        }
    }







    private HashMap<Integer, UnitRaisingStrengthOR> getUnitRaisingStrengthDetails() {
        HashMap<Integer, UnitRaisingStrengthOR> unitRaisingStrengthList = new HashMap<>();
        SQLiteDatabase sqlite = null;

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_RAISING_STRENGTH_DETAIL +
                " where " + TableNameAndColumnStatement.IS_SYNCED + " = " + 0 + " and "+
                 TableNameAndColumnStatement.UNIT_RAISING_ID + " = "+unitRaisingId;
        Timber.d(Constants.TAG_SYNCADAPTER+" UnitRaisingStrengthDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        UnitRaisingStrengthOR unitRaisingStrengthOR = new UnitRaisingStrengthOR();
                        unitRaisingStrengthOR.rankId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID));
                        unitRaisingStrengthOR.actualStrength =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_STRENGTH));
                        unitRaisingStrengthOR.unitRaisingDetailId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RAISING_ID));
                        unitRaisingStrengthList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)),
                                unitRaisingStrengthOR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingStrengthDetails object  %s",
                    IOPSApplication.getGsonInstance().toJson(unitRaisingStrengthList));

        }
        return unitRaisingStrengthList;
    }



    private void updateUnitRaisingStrengthId(int unitRaisingId, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_RAISING_STRENGTH_DETAIL +
                " SET "+ TableNameAndColumnStatement.IS_SYNCED + " = " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= " + id;
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingStrength updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            updateSyncCountAndStatus();
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }


    private void unitRaisingStrengthApiCall(final UnitRaisingStrengthOR unitRaisingStrengthOR, final int key) {
        sisClient.getApi().saveUnitRaisingStrength(unitRaisingStrengthOR, new Callback<UnitRaisingStrengthResponse>() {
            @Override
            public void success(UnitRaisingStrengthResponse unitRaisingStrengthResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingStrengthApiCall response  %s", unitRaisingStrengthOR);
                switch (unitRaisingStrengthResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        updateUnitRaisingStrengthId(unitRaisingStrengthResponse.unitRaisingStrengthData.
                                unitRaisingStrengthId, key);
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UNIT_RISING_STRENGTH_SYNC", error);
            }
        });

    }

    private void updateSyncCountAndStatus() {
        if(unitRaisingId != 0) {
            int count = dependentSyncDb.getSyncCount(unitRaisingId,
                    TableNameAndColumnStatement.UNIT_RAISING_ID,
                    TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED);

            if (count > 0) {
                count = count - 1;
            }
            //Timber.i("NewSync %s"," Image Sync count update in -----"+taskId +"--------"+count);
            updateUnitRaisingStrengthSyncCount(count);
        }
    }

    public void updateUnitRaisingStrengthSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(unitRaisingId == dependentSyncsStatus.unitRaisingId){
                    dependentSyncsStatus.isUnitRaisingStrengthSynced = count;
                    updateUnitRaisingStrengthSyncCount(dependentSyncsStatus);
                }


            }
        }
    }

    private void updateUnitRaisingStrengthSyncCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(dependentSyncsStatus.unitRaisingId,
                Constants.UNIT_RAISING_SYNC,
                TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED,
                dependentSyncsStatus.isUnitRaisingStrengthSynced,dependentSyncsStatus.taskTypeId
        );
    }
}
