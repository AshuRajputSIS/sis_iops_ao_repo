package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.rota.models.RotaModelMO;
import com.sisindia.ai.android.rota.models.RotaTask;
import com.sisindia.ai.android.rota.models.UnitTask;

import timber.log.Timber;

/**
 * Created by Saruchi on 06-06-2016.
 */
public class RotaUnitSynchronization extends SISAITrackingDB {


    public RotaUnitSynchronization(Context context) {
        super(context);
    }

    /**
     * @return
     */
    public int getRotaTablesId(String tableName, int Id, String columnanme) {
        int id = 0;
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + columnanme + " from " +
                tableName +
                " where " + columnanme + " =" + Id;
        Timber.d("getRotaId updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        id = cursor.getInt(cursor.getColumnIndex(columnanme));

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", id);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            cursor.close();
            sqlite.close();
        }
        return id;
    }

    /**
     * @param monthlyRota
     */
    public void UpadteIntoRota(RotaModelMO monthlyRota, int rotaId) {
        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (monthlyRota != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID, monthlyRota.getEmployeeId());
                    values.put(TableNameAndColumnStatement.ROTA_ID, monthlyRota.getRotaId());
                    values.put(TableNameAndColumnStatement.ROTA_WEEK, monthlyRota.getRotaWeek());
                    values.put(TableNameAndColumnStatement.ROTA_MONTH, monthlyRota.getRotaMonth());
                    values.put(TableNameAndColumnStatement.EMPLOYEE_ID, monthlyRota.getEmployeeId());
                    values.put(TableNameAndColumnStatement.ROTA_PUBLISHED_DATE_TIME, monthlyRota.getRotaPublishedDateTime());
                    values.put(TableNameAndColumnStatement.ROTA_STATUS_ID, monthlyRota.getRotaStatusId());
                    values.put(TableNameAndColumnStatement.ROTA_YEAR, monthlyRota.getRotaYear());
                    values.put(TableNameAndColumnStatement.ROTA_DATE, monthlyRota.getRotaDate());
                    sqlite.update(TableNameAndColumnStatement.ROTA_TABLE, values, TableNameAndColumnStatement.ROTA_ID + " = " + rotaId, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }
        }


    }

    /**
     * @param montlyRotaTask
     */
    public void UpdateIntoRotaTask(RotaTask montlyRotaTask, int rotaTaskId) {
        SQLiteDatabase sqlite = this.getWritableDatabase();


        if (montlyRotaTask != null) {
            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ROTA_ID, montlyRotaTask.getRotaId());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_ID, montlyRotaTask.getRotaTaskId());
                    values.put(TableNameAndColumnStatement.TASK_ID, montlyRotaTask.getTaskId());
                    values.put(TableNameAndColumnStatement.SOURCE_GEO_LATITUDE, montlyRotaTask.getSourceGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LATITUDE, montlyRotaTask.getDestinationGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LONGITUDE, montlyRotaTask.getDestinationGeoLongitude());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME, montlyRotaTask.getEstimatedTravelTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_DISTANCE, montlyRotaTask.getEstimatedDistance());
                    values.put(TableNameAndColumnStatement.TOTAL_ESTIMATED_TIME, montlyRotaTask.getTotalEstimatedTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TRAVEL_TIME, montlyRotaTask.getActualTravelTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_DISTANCE, montlyRotaTask.getActualDistance());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME, montlyRotaTask.getEstimatedTaskExecutionStartTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_END_TIME, montlyRotaTask.getEstimatedTaskExecutionEndTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_START_TIME, String.valueOf(montlyRotaTask.getActualTaskExecutionStartTime()));
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_END_TIME, String.valueOf(montlyRotaTask.getActualTaskExecutionEndTime()));
                    values.put(TableNameAndColumnStatement.TASK_SEQUENCE_NO, montlyRotaTask.getTaskSequenceNo());
                    sqlite.update(TableNameAndColumnStatement.ROTA_TASK_TABLE, values, TableNameAndColumnStatement.ROTA_TASK_ID + " = " + rotaTaskId, null);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }

        }
    }

    /**
     * @param montlyRotaTask
     */
    public void insertIntoRotaTask(RotaTask montlyRotaTask) {
        SQLiteDatabase sqlite = this.getWritableDatabase();


        if (montlyRotaTask != null) {
            synchronized (sqlite) {
                try {


                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ROTA_ID, montlyRotaTask.getRotaId());
                    values.put(TableNameAndColumnStatement.ROTA_TASK_ID, montlyRotaTask.getRotaTaskId());
                    values.put(TableNameAndColumnStatement.TASK_ID, montlyRotaTask.getTaskId());
                    values.put(TableNameAndColumnStatement.SOURCE_GEO_LATITUDE, montlyRotaTask.getSourceGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LATITUDE, montlyRotaTask.getDestinationGeoLatitude());
                    values.put(TableNameAndColumnStatement.DESTINATION_GEO_LONGITUDE, montlyRotaTask.getDestinationGeoLongitude());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME, montlyRotaTask.getEstimatedTravelTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_DISTANCE, montlyRotaTask.getEstimatedDistance());
                    values.put(TableNameAndColumnStatement.TOTAL_ESTIMATED_TIME, montlyRotaTask.getTotalEstimatedTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TRAVEL_TIME, montlyRotaTask.getActualTravelTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_DISTANCE, montlyRotaTask.getActualDistance());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME, montlyRotaTask.getEstimatedTaskExecutionStartTime());
                    values.put(TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_END_TIME, montlyRotaTask.getEstimatedTaskExecutionEndTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_START_TIME, String.valueOf(montlyRotaTask.getActualTaskExecutionStartTime()));
                    values.put(TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_END_TIME, String.valueOf(montlyRotaTask.getActualTaskExecutionEndTime()));
                    values.put(TableNameAndColumnStatement.TASK_SEQUENCE_NO, montlyRotaTask.getTaskSequenceNo());
                    sqlite.insert(TableNameAndColumnStatement.ROTA_TASK_TABLE, null, values);

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }


            }

        }
    }


    /**
     * @param rotaTask
     */
    public void insertIntoUnitTask(UnitTask rotaTask, int is_new, int is_synced) {
        SQLiteDatabase sqlite = this.getWritableDatabase();

        if (rotaTask != null) {
            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.TASK_ID,
                            rotaTask.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            rotaTask.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME,
                            rotaTask.getUnitName());
                    values.put(TableNameAndColumnStatement.TASK_TYPE_ID,
                            rotaTask.getTaskTypeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_ID,
                            rotaTask.getAssigneeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_NAME,
                            rotaTask.getAssigneeName());

                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME,
                            rotaTask.getEstimatedExecutionTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME,
                            rotaTask.getActualExecutionTime());
                    values.put(TableNameAndColumnStatement.TASK_STATUS_ID,
                            rotaTask.getTaskStatusId());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS,
                            rotaTask.getDescription());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME,
                            rotaTask.getEstimatedTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME,
                            rotaTask.getActualTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.IS_AUTO, rotaTask.getIsAutoCreation());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME,
                            rotaTask.getCreatedDateTime());

                    values.put(TableNameAndColumnStatement.CREATED_BY_ID,
                            rotaTask.getCreatedById());
                    values.put(TableNameAndColumnStatement.CREATED_BY_NAME,
                            rotaTask.getCreatedByName());
                    values.put(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED, rotaTask.getApprovedById());

                    values.put(TableNameAndColumnStatement.ASSIGNED_BY_ID,
                            rotaTask.getAssignedById());

                    values.put(TableNameAndColumnStatement.BARRACK_ID,
                            rotaTask.getBarrackId());
                    values.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT,
                            rotaTask.getTaskExecutionResult());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME,
                            rotaTask.getEstimatedTaskExecutionEndDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME,
                            rotaTask.getActualTaskExecutionEndDateTime());

                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID,
                            rotaTask.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.IS_NEW, is_new);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, is_synced);
                    String[] createdStringDate = rotaTask.getCreatedDateTime().split(" ");
                    values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);

                  //  values.put(TableNameAndColumnStatement.CREATED_DATE, rotaTask.getCreatedDate());


                    sqlite.insert(TableNameAndColumnStatement.TASK_TABLE, null, values);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }

        }
    }


    /**
     * @param rotaTask
     */
    public void upadteIntoUnitTask(UnitTask rotaTask, int taskid, int is_new, int is_synced) {
        SQLiteDatabase sqlite = this.getWritableDatabase();

        if (rotaTask != null) {
            synchronized (sqlite) {
                try {

                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.TASK_ID,
                            rotaTask.getId());
                    values.put(TableNameAndColumnStatement.UNIT_ID,
                            rotaTask.getUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME,
                            rotaTask.getUnitName());
                    values.put(TableNameAndColumnStatement.TASK_TYPE_ID,
                            rotaTask.getTaskTypeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_ID,
                            rotaTask.getAssigneeId());
                    values.put(TableNameAndColumnStatement.ASSIGNEE_NAME,
                            rotaTask.getAssigneeName());

                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME,
                            rotaTask.getEstimatedExecutionTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME,
                            rotaTask.getActualExecutionTime());
                    values.put(TableNameAndColumnStatement.TASK_STATUS_ID,
                            rotaTask.getTaskStatusId());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS,
                            rotaTask.getDescription());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME,
                            rotaTask.getEstimatedTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME,
                            rotaTask.getActualTaskExecutionStartDateTime());
                    values.put(TableNameAndColumnStatement.IS_AUTO, rotaTask.getIsAutoCreation());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME,
                            rotaTask.getCreatedDateTime());

                    values.put(TableNameAndColumnStatement.CREATED_BY_ID,
                            rotaTask.getCreatedById());
                    values.put(TableNameAndColumnStatement.CREATED_BY_NAME,
                            rotaTask.getCreatedByName());
                    values.put(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED, rotaTask.getApprovedById());

                    values.put(TableNameAndColumnStatement.ASSIGNED_BY_ID,
                            rotaTask.getAssignedById());

                    values.put(TableNameAndColumnStatement.BARRACK_ID,
                            rotaTask.getBarrackId());
                    values.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT,
                            rotaTask.getTaskExecutionResult());
                    values.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME,
                            rotaTask.getEstimatedTaskExecutionEndDateTime());
                    values.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME,
                            rotaTask.getActualTaskExecutionEndDateTime());

                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID,
                            rotaTask.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.IS_NEW, is_new);
                    values.put(TableNameAndColumnStatement.IS_SYNCED, is_synced);
                    String[] createdStringDate = rotaTask.getEstimatedTaskExecutionStartDateTime().split(" ");
                    values.put(TableNameAndColumnStatement.CREATED_DATE, createdStringDate[0]);
                    sqlite.update(TableNameAndColumnStatement.TASK_TABLE, values,
                            TableNameAndColumnStatement.TASK_ID + " = " + taskid, null);


                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                }  finally {
                    sqlite.close();
                }
            }

        }
    }

}
