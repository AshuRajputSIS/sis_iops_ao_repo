package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.loadconfiguration.IssueMatrixModel;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 18/5/16.
 */
public class UpdateStatementDB extends SISAITrackingDB {
    public UpdateStatementDB(Context context) {
        super(context);
    }

    /**
     * @param unitPostId
     */
    public void updatePostStatus(int unitPostId) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.UNIT_POST_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.POST_STATUS, true);
            sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE,
                    contentValues, whereClause,
                    new String[]{" " + unitPostId});

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param unitId
     */
    public void updatePostBasedOnUnit(int unitId) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.UNIT_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.POST_STATUS, false);
            sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE, contentValues, whereClause, new String[]{" " + unitId});
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }

    }

    /**
     * @param
     * @param taskId
     * @param taskResult
     * @param taskSubmitedLocation
     */
    public void updateTaskResult(int taskId, String taskResult, int taskStatusId, String actualEndDateTime, String taskSubmitedLocation) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.TASK_ID + " = ?";
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {

                ContentValues contentValues = new ContentValues();
                contentValues.put(TableNameAndColumnStatement.TASK_EXECUTION_RESULT, taskResult);
                contentValues.put(TableNameAndColumnStatement.TASK_STATUS_ID, taskStatusId);
                contentValues.put(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME, actualEndDateTime);
                contentValues.put(TableNameAndColumnStatement.IS_TASK_STARTED, RotaStartTaskEnum.InActive.getValue());
                contentValues.put(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION, taskSubmitedLocation);
                contentValues.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                contentValues.put(TableNameAndColumnStatement.IS_NEW, 1);
                sqlite.update(TableNameAndColumnStatement.TASK_TABLE,
                        contentValues, whereClause,
                        new String[]{" " + taskId});

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public void UpdateUnitBillingStatusTable(int isBillCollected, int unit_id, int id) {
        SQLiteDatabase sqlite = null;
        String tableName = TableNameAndColumnStatement.UnitBillingStatusTable;
        String updateQuery = "UPDATE " + tableName +
                " SET " + TableNameAndColumnStatement.IS_BILL_COLLECTED + "= '" + isBillCollected + "'" +
                " WHERE " + TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID + "= '" + unit_id + "'" +
                "and " + TableNameAndColumnStatement.Bill_COLLACTION_ID + "= '" + id + "'";
        Timber.d("UpdateUnitBillingStatusTable  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("UpdateUnitBillingStatusTable  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void insertClientCoordinationIssueMatrixTable(List<IssueMatrixModel> issueMatrixList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                for (IssueMatrixModel issueMatrixModel : issueMatrixList) {
                    values.put(TableNameAndColumnStatement.ID, issueMatrixModel.getId());
                    values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID, issueMatrixModel.getIssueTypeId());
                    values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID, issueMatrixModel.getIssueCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID, issueMatrixModel.getIssueSubCategoryId());
                    values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID, issueMatrixModel.getIssueCauseId());
                    values.put(TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID, issueMatrixModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.DEFAULT_ASSIGNMENNT_ROLE_ID, issueMatrixModel.getDefaultAssignmentRoleId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, issueMatrixModel.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.TURN_AROUND_TIME, issueMatrixModel.getTurnAroundTime());
                    values.put(TableNameAndColumnStatement.PROOF_CONTROL_TYPE, issueMatrixModel.getProofControlType());
                    values.put(TableNameAndColumnStatement.MINIMUM_OPTIONAL_VALUE, issueMatrixModel.getMinimumOptionalValue());
                    sqlite.insert(TableNameAndColumnStatement.ISSUE_MATRIX_TABLE, null,
                            values);
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }

            }
        }
    }

    public boolean updateGuardDetails(int unit_id, String guardCode, String overAllTurnOut, String amount) {
        SQLiteDatabase sqlite = null;
        boolean isUpdatedSuccessfully = false;
        String updateStatement = null;
        String whereClause = " where guard_code like '" + guardCode + "'";
        if ("Impressive".equalsIgnoreCase(overAllTurnOut)) {
            updateStatement = " update guards set reward_amount = " + amount + "+ (select reward_amount from guards "
                    + whereClause + ")" + whereClause;
        } else if ("Poor Image".equalsIgnoreCase(overAllTurnOut)) {
            updateStatement = " update guards set fine_amount = " + amount + "+ (select fine_amount from guards "
                    + whereClause + "),last_fine = " + amount + " " + whereClause;

        }
        /*String whereClause = TableNameAndColumnStatement.UNIT_ID + " = "+ unit_id + " and "+
                TableNameAndColumnStatement.GUARD_CODE + " = "+ guardCode;*/
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                sqlite.beginTransaction();
                Cursor cursor = sqlite.rawQuery(updateStatement, null);
                Timber.d("cursor detail object  %s", cursor.getCount());
                sqlite.setTransactionSuccessful();
                isUpdatedSuccessfully = true;

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
        return isUpdatedSuccessfully;
    }

    public void updateEditTaskDetails(int taskId, String date, String estimatedTaskStartTime, String estimatedTaskEndTime, int taskStatusId) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.TASK_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME, estimatedTaskStartTime);
            contentValues.put(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME, estimatedTaskEndTime);
            if (estimatedTaskStartTime != null && !estimatedTaskStartTime.isEmpty()) {
                String[] dateAndTime = estimatedTaskStartTime.split(" ");
                if (dateAndTime != null && dateAndTime.length != 0)
                    contentValues.put(TableNameAndColumnStatement.CREATED_DATE, dateAndTime[0]);
            }

            contentValues.put(TableNameAndColumnStatement.IS_SYNCED, 1);
            contentValues.put(TableNameAndColumnStatement.IS_NEW, 0);
            contentValues.put(TableNameAndColumnStatement.TASK_STATUS_ID, taskStatusId);
            sqlite.update(TableNameAndColumnStatement.TASK_TABLE,
                    contentValues, whereClause,
                    new String[]{" " + taskId});

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    //@Ashu: Adding method to update rota_id of Rota_Task Table
    public void updateRotaIdOfRotaTaskTable(int rotaTaskId) {
        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.UNIT_POST_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.POST_STATUS, true);
            /*sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE,
                    contentValues, whereClause,
                    new String[]{" " + unitPostId});*/

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * if the user edit particular post which are already added.
     * we are setting the sync=0 so that updated values can sync with server
     * sequnace id being usd if the pposts have postid =0 and are ediited
     * takine the recylerview position and adding 1 in the postiion as we have list startng with zer element
     * Db starts with 1 positon
     */
    public void UpdateUnitPostTable(PostData postData, int isSynced) {

        SQLiteDatabase sqlite = null;
        String updateQuery;
        Timber.d("UpdateUnitPostTable sequenceid  %s", postData.getSequenceId());
        String unitPostName = postData.getPostName();
        if (unitPostName.contains("'")) {

            unitPostName = unitPostName.replace("'", "\'");
        }

        // Timber.d("UpdateUnitPostTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            if (postData.getUnitPostId() != 0) {

                String whereCondition = TableNameAndColumnStatement.UNIT_POST_ID + "= '" + postData.getUnitPostId() + "' AND " +
                        TableNameAndColumnStatement.UNIT_ID + "= '" + postData.getUnitId() + "'";

                ContentValues contentValues = new ContentValues();
                contentValues.put(TableNameAndColumnStatement.UNIT_POST_NAME, unitPostName);
                contentValues.put(TableNameAndColumnStatement.DESCRIPTIONS, postData.getDescription());
                contentValues.put(TableNameAndColumnStatement.ARMED_STRENGTH, postData.getArmedStrength());
                contentValues.put(TableNameAndColumnStatement.UNARMED_STRENGTH, postData.getUnarmedStrength());
                contentValues.put(TableNameAndColumnStatement.BARRACK_DISTANCE, postData.getBarrackDistance());
                contentValues.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, postData.getUnitOfficeDistance());
                contentValues.put(TableNameAndColumnStatement.GEO_LATITUDE, postData.getLatitude());
                contentValues.put(TableNameAndColumnStatement.GEO_LONGITUDE, postData.getLongitude());
                contentValues.put(TableNameAndColumnStatement.IS_MAIN_GATE, postData.getIsMainGate());
                contentValues.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, postData.getMainGateDistance());
                contentValues.put(TableNameAndColumnStatement.IS_SYNCED, isSynced);
                contentValues.put(TableNameAndColumnStatement.DEVICE_NO, postData.getDeviceNo());


                sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE, contentValues, whereCondition, null);

            } else {
                String whereCondition = TableNameAndColumnStatement.ID + "= '" + postData.getSequenceId() + "' AND " +
                        TableNameAndColumnStatement.UNIT_ID + "= '" + postData.getUnitId() + "'";

                ContentValues contentValues = new ContentValues();
                contentValues.put(TableNameAndColumnStatement.UNIT_POST_NAME, unitPostName);
                contentValues.put(TableNameAndColumnStatement.DESCRIPTIONS, postData.getDescription());
                contentValues.put(TableNameAndColumnStatement.ARMED_STRENGTH, postData.getArmedStrength());
                contentValues.put(TableNameAndColumnStatement.UNARMED_STRENGTH, postData.getUnarmedStrength());
                contentValues.put(TableNameAndColumnStatement.BARRACK_DISTANCE, postData.getBarrackDistance());
                contentValues.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, postData.getUnitOfficeDistance());
                contentValues.put(TableNameAndColumnStatement.GEO_LATITUDE, postData.getLatitude());
                contentValues.put(TableNameAndColumnStatement.GEO_LONGITUDE, postData.getLongitude());
                contentValues.put(TableNameAndColumnStatement.IS_MAIN_GATE, postData.getIsMainGate());
                contentValues.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, postData.getMainGateDistance());
                contentValues.put(TableNameAndColumnStatement.IS_SYNCED, isSynced);
                contentValues.put(TableNameAndColumnStatement.DEVICE_NO, postData.getDeviceNo());
                sqlite.update(TableNameAndColumnStatement.UNIT_POST_TABLE, contentValues, whereCondition, null);

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null) {
                sqlite.close();
            }
        }
    }

    /**
     * @param unitContactId
     * @param unitContact
     */
    public void updateUnitContacts(String unitContactId, UnitContactsMO unitContact) {

        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();
        if (unitContact != null) {
            synchronized (sqlite) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getContactFirstName());
                    values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getContactLastName());
                    values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getContactEmail());
                    values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNumber());
                    values.put(TableNameAndColumnStatement.UNIT_ID, unitContact.getUnitId());
                    values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getClientDesignation());
                    //sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
                    sqlite.update(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, values, "id = ?", new String[]{unitContactId});

                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                } finally {
                    sqlite.close();
                }
            }
        }

    }


    /**
     * @param Tablename
     * @param unit_post_id
     * @param id
     */
    public void UpdatePostidUnitPostTable(String Tablename, int unit_post_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + unit_post_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("UpdatePostidUnitPostTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param Tablename
     * @param unit_post_id
     * @param id
     */
    public void UpdatePostidUnitEquipmentTable(String Tablename, int unit_post_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + unit_post_id + "'" +
                " WHERE " + TableNameAndColumnStatement.UNIT_POST_ID + "= '" + id + "'";
        Timber.d("UpdatePostidUnitEquipmentTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param Tablename
     * @param equip_id
     * @param id
     */
    public void UpdateEquipIdUnitEquipmentTable(String Tablename, int equip_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + "= '" + equip_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param Tablename
     * @param taskId
     * @param attachmentReferenceId
     * @param sequenceId
     * @param attachmentRefreenceType
     */
    public void UpdatePostidMetadataTable(String Tablename, int taskId, int attachmentReferenceId, int sequenceId, int attachmentRefreenceType) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + attachmentReferenceId + "' , " +
                TableNameAndColumnStatement.TASK_ID + "= '" + taskId + "'" +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + sequenceId + "' AND " +
                TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE + "= '" + attachmentRefreenceType + "'";
        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param Tablename
     * @param attach_id
     * @param id
     */
    public void UpdateAttachmentIdMetadataTable(String Tablename, int attach_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_ID + "= '" + attach_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param unitId
     * @param ActualValue
     * @param guardTypeId
     * @param taskId
     */
    public void updateActualValue(double unitId, double ActualValue, double guardTypeId, double taskId) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_BARRACK_STRENGTH_TABLE +
                " SET " + TableNameAndColumnStatement.ACTUAL_STRENGTH + " = " + ActualValue +
                " WHERE " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId + " AND "
                + TableNameAndColumnStatement.TASK_ID + " = " + taskId + " AND "
                + TableNameAndColumnStatement.GUARD_TYPE_ID + " = " + guardTypeId;
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * Update the task id in the taskTable
     *
     * @param taskId
     * @param id
     */
    public void UpdateTaskidTaskTable(int taskId, int id, int taskStatusD, int isnew, int isSync) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_ID + "= '" + taskId +
                "' ," + TableNameAndColumnStatement.TASK_STATUS_ID + "= " + taskStatusD +
                " ," + TableNameAndColumnStatement.IS_NEW + "= " + isnew +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + isSync +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param taskid
     * @param id
     */
    public void UpdateTaskIdGrievanceTable(int taskid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.GRIEVANCE_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_ID + "= '" + taskid + "'" +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + "= '" + id + "'";
        Timber.d("UpdateTaskIdGrievanceTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param taskid
     * @param id
     */
    public void UpdateTaskIdComplaintTable(int taskid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.COMPLAINT_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_ID + "= '" + taskid + "'" +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + "= '" + id + "'";
        Timber.d("UpdateTaskIdGrievanceTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * onDutyChange Issueid in Grievance Table and Complaint Table
     *
     * @param Tablename
     * @param issueid
     * @param id
     */
    public void UpdateIssueIdTable(String Tablename, int issueid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ISSUE_ID + "= '" + issueid +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

    /**
     * @param Tablename
     * @param taskid
     * @param id
     */
    public void UpdateTaskiddMetadataTable(String Tablename, int taskid, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + taskid +
                "' ," + TableNameAndColumnStatement.TASK_ID + "= " + taskid +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + id + "'";
        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            sqlite.close();
        }
    }

}