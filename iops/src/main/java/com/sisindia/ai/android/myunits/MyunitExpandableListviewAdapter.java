package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.unitDTO.UnitLevelSelectionQuery;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitAddressLatlong;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 29-03-2016.
 */
public class MyunitExpandableListviewAdapter extends BaseExpandableListAdapter {
    @Bind(R.id.linearParentLayout)
    LinearLayout parentLayout;
    @Bind(R.id.myunits_divider_two)
    View myunitsDividerTwo;
    @Bind(R.id.myunits_homepage_billing_txt)
    CustomFontTextview myunitsHomepageBillingTxt;
    @Bind(R.id.myunits_homepage_billing_value_txt)
    CustomFontTextview myunitsHomepageBillingValueTxt;
    @Bind(R.id.myunits_homepage_collection_label_txt)
    CustomFontTextview myunitsHomepageCollectionLabelTxt;
    @Bind(R.id.myunits_homepage_collection_value_txt)
    CustomFontTextview myunitsHomepageCollectionValueTxt;
    @Bind(R.id.myunits_homepage_wages_label_txt)
    CustomFontTextview myunitsHomepageWagesLabelTxt;
    @Bind(R.id.myunits_homepage_wages_value_txt)
    CustomFontTextview myunitsHomepageWagesValueTxt;
    @Bind(R.id.myunits_homepage_issues_label_txt)
    CustomFontTextview myunitsHomepageIssuesLabelTxt;
    @Bind(R.id.myunits_homepage_issues_value_txt)
    CustomFontTextview myunitsHomepageIssuesValueTxt;
    @Bind(R.id.myunits_title_pendingValue_txt)
    CustomFontTextview pendingTaskList;
    private UnitLevelSelectionQuery unitQueryInstance;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private ArrayList<MyUnitHomeMO> myUnitHomeMOArrayList;
    private SingletonUnitDetails singletonUnitDetailsMO;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private Resources resources;
    private AppPreferences appPreferences;


    public MyunitExpandableListviewAdapter(Context mContext) {
        this.mContext = mContext;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        unitQueryInstance = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        resources = mContext.getResources();
        appPreferences = new AppPreferences(mContext);
    }

    public void setMyUnitHomeData(ArrayList<MyUnitHomeMO> myUnitHomeMOArrayList) {
        this.myUnitHomeMOArrayList = myUnitHomeMOArrayList;
    }

    @Override
    public int getGroupCount() {
        int count = myUnitHomeMOArrayList == null ? 0 : myUnitHomeMOArrayList.size();
        return count;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.myunits_homepage_expandable_parent, null, false);
        //  ButterKnife.bind(this,view);
        LinearLayout clickAbleLayout = (LinearLayout) view.findViewById(R.id.arrow_layout_click);
        ImageView myunits_homepage_down_arrow_image = (ImageView) view.findViewById(R.id.myunits_homepage_down_arrow_image);

        /*TextView myunits_homepage_unitname_label = (TextView) view.findViewById(R.id.myunits_homepage_unitname_label);
        TextView myunits_homepage_checkingdate_label = (TextView) view.findViewById(R.id.myunits_homepage_checkingdate_label);
        TextView myUnitsSerialNo = (TextView) view.findViewById(R.id.unit_serial_no_tv);
        TextView myUnitsCode = (TextView) view.findViewById(R.id.unit_code_tv);*/

        CustomFontTextview myunits_homepage_unitname_label = (CustomFontTextview) view.findViewById(R.id.myunits_homepage_unitname_label);
        CustomFontTextview myunits_homepage_checkingdate_label = (CustomFontTextview) view.findViewById(R.id.myunits_homepage_checkingdate_label);
        CustomFontTextview myUnitsSerialNo = (CustomFontTextview) view.findViewById(R.id.unit_serial_no_tv);
        CustomFontTextview myUnitsCode = (CustomFontTextview) view.findViewById(R.id.unit_code_tv);

        ImageView locationMap = (ImageView) view.findViewById(R.id.location_map);

        myunits_homepage_down_arrow_image.setFocusable(false);
        int imageResourceId = isExpanded ? R.drawable.up_arrow : R.drawable.down_arrow;
        myunits_homepage_down_arrow_image.setImageResource(imageResourceId);

        myunits_homepage_unitname_label.setText(myUnitHomeMOArrayList.get(groupPosition).getUnitName());
        myUnitsSerialNo.setText(String.valueOf(groupPosition + 1) + ".");
        myUnitsCode.setText(myUnitHomeMOArrayList.get(groupPosition).getUnitCode());

        if (myUnitHomeMOArrayList.get(groupPosition).getTaskType() != null && myUnitHomeMOArrayList
                .get(groupPosition).getDate() != null
                && !"NA".equalsIgnoreCase(myUnitHomeMOArrayList
                .get(groupPosition).getDate())) {

            if ("" != myUnitHomeMOArrayList.get(groupPosition).getTaskType()
                    && "" != myUnitHomeMOArrayList.get(groupPosition).getDate())
                myunits_homepage_checkingdate_label.setText(myUnitHomeMOArrayList
                        .get(groupPosition).getTaskType() + " - " + myUnitHomeMOArrayList
                        .get(groupPosition).getDate());
        } else {
            myunits_homepage_checkingdate_label.setText("NA");
        }

        singletonUnitDetailsMO = SingletonUnitDetails.getInstance();
        final UnitAddressLatlong unitAddressLatlong = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitLatLon(myUnitHomeMOArrayList.get(groupPosition).getUnitId());
//        unitAddressLatlong.setLatitude(myUnitHomeMOArrayList.get(groupPosition).getUnitAddressLatlong().getLatitude());
//        unitAddressLatlong.setLongitude(myUnitHomeMOArrayList.get(groupPosition).getUnitAddressLatlong().getLongitude());

        clickAbleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExpanded) ((ExpandableListView) parent).collapseGroup(groupPosition);
                else ((ExpandableListView) parent).expandGroup(groupPosition, true);
            }
        });


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singletonUnitDetailsMO.setUnitDetails(myUnitHomeMOArrayList.get(groupPosition), groupPosition);
                appPreferences.setUnitJsonData(myUnitHomeMOArrayList.get(groupPosition));
                Intent singleUnitIntent = new Intent(mContext, SingleUnitActivity.class);
                mContext.startActivity(singleUnitIntent);
            }
        });
        if (unitAddressLatlong.getLatitude() == 0 ||
                unitAddressLatlong.getLongitude() == 0) {
            locationMap.setEnabled(false);
            locationMap.setBackgroundResource(R.drawable.location_grey);
        } else {
            locationMap.setBackgroundResource(R.drawable.locationblue);
            locationMap.setEnabled(true);
        }
        locationMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://maps.google.com/maps?saddr=" + GpsTrackingService.latitude
                        + "," + GpsTrackingService.longitude + "&daddr=" +
                        unitAddressLatlong.getLatitude()
                        + "," + unitAddressLatlong.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(url));
                mContext.startActivity(intent);
            }
        });
        return view;

    }


    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.myunits_homepage_expandable_child, null, false);
        ButterKnife.bind(this, view);
        getPendingTaskList(groupPosition);
        // billingStatus(groupPosition);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isConnected) {
                    singletonUnitDetailsMO.setUnitDetails(myUnitHomeMOArrayList.get(groupPosition), groupPosition);
                    appPreferences.setUnitJsonData(myUnitHomeMOArrayList.get(groupPosition));
                    Intent singleUnitIntent = new Intent(mContext, SingleUnitActivity.class);
                    mContext.startActivity(singleUnitIntent);
                } else {
                    Util.customSnackBar(view, mContext, resources.getString(R.string.no_internet));
                }
            }
        });

        myunitsHomepageBillingValueTxt.setText(myUnitHomeMOArrayList.get(groupPosition).getBillingStaus());
        // if (!myUnitHomeMOArrayList.get(groupPosition).getCollectionStaus().equals("NA"))
        myunitsHomepageCollectionValueTxt.setText(myUnitHomeMOArrayList.get(groupPosition).getCollectionStaus());
        //if (!myUnitHomeMOArrayList.get(groupPosition).getWagesStatus().equals("NA"))
        myunitsHomepageWagesValueTxt.setText(myUnitHomeMOArrayList.get(groupPosition).getWagesStatus());
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void getPendingTaskList(int groupPosition) {

        String pendingStr = unitQueryInstance.getPendingList(dateTimeFormatConversionUtil.getCurrentDateTime(), myUnitHomeMOArrayList.get(groupPosition).getUnitId());
        if (null != pendingStr && 0 != pendingStr.length()) {
            pendingTaskList.setText(pendingStr);
        } else {
            pendingTaskList.setText(resources.getString(R.string.no_task_pending));
        }
    }


}
