package com.sisindia.ai.android.fcmnotification.fcmunits;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.UnitBarrack;

import java.util.List;

/**
 * Created by compass on 2/22/2017.
 */

public class FCMUnitBarrackModelInsertion  extends SISAITrackingDB{


        private SQLiteDatabase sqlite=null;
        private boolean isInsertedSuccessfully;
        private Context context;

        public FCMUnitBarrackModelInsertion(Context context) {
            super(context);
            this.context = context;
        }

        public synchronized boolean insertUnitBarrack(List<UnitBarrack> unitBarrackList) {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                try {
                    if(unitBarrackList != null && unitBarrackList.size() != 0 ) {
                        sqlite.beginTransaction();
                        FCMUnitBarrackDeletion fcmUnitBarrackDeletion =  new FCMUnitBarrackDeletion(context);
                        fcmUnitBarrackDeletion.deleteUnitBarrack(TableNameAndColumnStatement.UNIT_BARRACK_TABLE,sqlite);
                        insertIntoUnitBarrack(unitBarrackList);
                        sqlite.setTransactionSuccessful();
                        isInsertedSuccessfully = true;
                    }else {
                        isInsertedSuccessfully = false;
                    }
                } catch (Exception exception) {
                    Crashlytics.logException(exception);
                    isInsertedSuccessfully = false;
                } finally {
                    sqlite.endTransaction();

                }
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
                return isInsertedSuccessfully;
            }
        }

    public void insertIntoUnitBarrack(List<UnitBarrack> unitBarrackList) {
           for(UnitBarrack unitBarrack : unitBarrackList) {
               ContentValues values = new ContentValues();
               values.put(TableNameAndColumnStatement.UNIT_BARRACK_ID, unitBarrack.getId());
               values.put(TableNameAndColumnStatement.UNIT_ID, unitBarrack.getUnitId());
               values.put(TableNameAndColumnStatement.BARRACK_ID, unitBarrack.getBarrackId());
               values.put(TableNameAndColumnStatement.IS_ACTIVE, unitBarrack.getActive());
               sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, null, values);
           }
    }


}
