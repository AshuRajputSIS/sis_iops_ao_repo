package com.sisindia.ai.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.Bind;

/**
 * Created by Saruchi on 25-04-2016.
 */
public class VerticalBarGraph extends BaseFrame {
    @Bind(R.id.recommended)
    View recommended_view;
    @Bind(R.id.selected)
    View selected_view;
    @Bind(R.id.rejected)
    View rejected_view;
    @Bind(R.id.in_progress)
    View inProgress_view;
    TextView mrecommended_count;
    @Bind(R.id.selected_count)
    TextView mselected_count;
    @Bind(R.id.rejected_count)
    TextView mrejected_count;
    @Bind(R.id.inprogress_count)
    TextView minprogress_count;
    TextView mmonth_name;

    public VerticalBarGraph(Context context) {
        super(context);
        init(context, null);
    }

    public VerticalBarGraph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.vertical_bar_view, this);
        mrecommended_count = (TextView) mView.findViewById(R.id.recommended_count);
        minprogress_count = (TextView) mView.findViewById(R.id.inprogress_count);
        mselected_count = (TextView) mView.findViewById(R.id.selected_count);
        mrejected_count =(TextView) mView.findViewById(R.id.rejected_count);
        recommended_view = mView.findViewById(R.id.recommended);
        rejected_view = mView.findViewById(R.id.rejected);
        inProgress_view = mView.findViewById(R.id.in_progress);
        selected_view = mView.findViewById(R.id.selected);
        mmonth_name = (TextView)mView.findViewById(R.id.month_name);
    }

    public void setRecommendedCount(int count){

        mrecommended_count.setText(Integer.toString(count));

        recommended_view.setLayoutParams(new RelativeLayout.LayoutParams((int)mrecommended_count.getResources().getDimension(R.dimen.dimen_36), validateHeight(count)));

    }
    public void setRejectedCount(int count){

        mrejected_count.setText(Integer.toString(count));

        rejected_view.setLayoutParams(new RelativeLayout.LayoutParams((int)mrecommended_count.getResources().getDimension(R.dimen.dimen_36),  validateHeight(count)));
    }

    public void setSelectedCount(int count){

        mselected_count.setText(Integer.toString(count));

        selected_view.setLayoutParams(new RelativeLayout.LayoutParams((int)mrecommended_count.getResources().getDimension(R.dimen.dimen_36),  validateHeight(count)));
    }
    public void setInProgressCount(int count){

        minprogress_count.setText(Integer.toString(count));

        inProgress_view.setLayoutParams(new RelativeLayout.LayoutParams((int)mrecommended_count.getResources().getDimension(R.dimen.dimen_36),  validateHeight(count)));
    }

    public void setCurrentMonth(int month, Integer year){
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMM");
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        SimpleDateFormat YearDisplay = new SimpleDateFormat("yy");

        try {
            mmonth_name.setText(monthDisplay.format(monthParse.parse(Integer.toString(month))) +"' " +YearDisplay.format(df.parse(Integer.toString(year))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private int validateHeight(int value){
        int barChatHeight = 0;
        if(value == 0){
            barChatHeight = (int)mrecommended_count.getResources().getDimension(R.dimen.dimen_20);
        }else{
            barChatHeight = (int)mrecommended_count.getResources().getDimension(R.dimen.dimen_20)*value;
        }
        return barChatHeight;
    }

}
