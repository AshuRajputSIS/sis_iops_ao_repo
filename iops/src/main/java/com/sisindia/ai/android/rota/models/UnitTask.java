package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitTask {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("UnitName")
    @Expose
    private String unitName;
    @SerializedName("TaskTypeId")
    @Expose
    private Integer taskTypeId;
    @SerializedName("TaskName")
    @Expose
    private String taskName;
    @SerializedName("AssigneeId")
    @Expose
    private Integer assigneeId;
    @SerializedName("AssigneeName")
    @Expose
    private String assigneeName;
    @SerializedName("EstimatedExecutionTime")
    @Expose
    private Integer estimatedExecutionTime;
    @SerializedName("ActualExecutionTime")
    @Expose
    private Integer actualExecutionTime;
    @SerializedName("TaskStatusId")
    @Expose
    private Integer taskStatusId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("EstimatedTaskExecutionStartDateTime")
    @Expose
    private String estimatedTaskExecutionStartDateTime;
    @SerializedName("ActualTaskExecutionStartDateTime")
    @Expose
    private String actualTaskExecutionStartDateTime;
    @SerializedName("IsAutoCreation")
    @Expose
    private boolean isAutoCreation;
    @SerializedName("ExecutorId")
    @Expose
    private Integer executorId;
    @SerializedName("ExecutorName")
    @Expose
    private String executorName;
    @SerializedName("CreatedDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("CreatedById")
    @Expose
    private Integer createdById;
    @SerializedName("CreatedByName")
    @Expose
    private String createdByName;
    @SerializedName("ApprovedById")
    @Expose
    private Integer approvedById;
    @SerializedName("ApprovedByName")
    @Expose
    private String approvedByName;
    @SerializedName("RelatedTaskId")
    @Expose
    private Integer relatedTaskId;
    @SerializedName("UnitPostId")
    @Expose
    private Integer unitPostId;
    @SerializedName("UnitPostName")
    @Expose
    private String unitPostName;
    @SerializedName("AdditionalComments")
    @Expose
    private String additionalComments;
    @SerializedName("AssignedById")
    @Expose
    private Integer assignedById;
    @SerializedName("AssignedByName")
    @Expose
    private String assignedByName;
    @SerializedName("BarrackId")
    @Expose
    private Integer barrackId;
    @SerializedName("TaskExecutionResult")
    @Expose
    private String taskExecutionResult;
    @SerializedName("EstimatedTaskExecutionEndDateTime")
    @Expose
    private String estimatedTaskExecutionEndDateTime;
    @SerializedName("ActualTaskExecutionEndDateTime")
    @Expose
    private String actualTaskExecutionEndDateTime;
    @SerializedName("IsPostMandatory")
    @Expose
    private boolean isPostMandatory;
    @SerializedName("TaskReviewStatusId")
    @Expose
    private Integer taskReviewStatusId;
    @SerializedName("EstimatedTravelTime")
    @Expose
    private Integer estimatedTravelTime;
    @SerializedName("EstimatedDistance")
    @Expose
    private double estimatedDistance;
    @SerializedName("UnitTypeId")
    @Expose
    private Integer unitTypeId;
    @SerializedName("ChangeRouteReasonId")
    private Integer selectReasonId;

    @SerializedName("OtherReasonId")
    private Integer otherReasonId;

   /* public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    private String createdDate;*/


    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * @param unitName The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * @return The taskTypeId
     */
    public Integer getTaskTypeId() {
        return taskTypeId;
    }

    /**
     * @param taskTypeId The TaskTypeId
     */
    public void setTaskTypeId(Integer taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    /**
     * @return The taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * @param taskName The TaskName
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * @return The assigneeId
     */
    public Integer getAssigneeId() {
        return assigneeId;
    }

    /**
     * @param assigneeId The AssigneeId
     */
    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    /**
     * @return The assigneeName
     */
    public String getAssigneeName() {
        return assigneeName;
    }

    /**
     * @param assigneeName The AssigneeName
     */
    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    /**
     * @return The estimatedExecutionTime
     */
    public Integer getEstimatedExecutionTime() {
        return estimatedExecutionTime;
    }

    /**
     * @param estimatedExecutionTime The EstimatedExecutionTime
     */
    public void setEstimatedExecutionTime(Integer estimatedExecutionTime) {
        this.estimatedExecutionTime = estimatedExecutionTime;
    }

    /**
     * @return The actualExecutionTime
     */
    public Integer getActualExecutionTime() {
        return actualExecutionTime;
    }

    /**
     * @param actualExecutionTime The ActualExecutionTime
     */
    public void setActualExecutionTime(Integer actualExecutionTime) {
        this.actualExecutionTime = actualExecutionTime;
    }

    /**
     * @return The taskStatusId
     */
    public Integer getTaskStatusId() {
        return taskStatusId;
    }

    /**
     * @param taskStatusId The TaskStatusId
     */
    public void setTaskStatusId(Integer taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The estimatedTaskExecutionStartDateTime
     */
    public String getEstimatedTaskExecutionStartDateTime() {
        return estimatedTaskExecutionStartDateTime;
    }

    /**
     * @param estimatedTaskExecutionStartDateTime The EstimatedTaskExecutionStartDateTime
     */
    public void setEstimatedTaskExecutionStartDateTime(String estimatedTaskExecutionStartDateTime) {
        this.estimatedTaskExecutionStartDateTime = estimatedTaskExecutionStartDateTime;
    }

    /**
     * @return The actualTaskExecutionStartDateTime
     */
    public String getActualTaskExecutionStartDateTime() {
        return actualTaskExecutionStartDateTime;
    }

    /**
     * @param actualTaskExecutionStartDateTime The ActualTaskExecutionStartDateTime
     */
    public void setActualTaskExecutionStartDateTime(String actualTaskExecutionStartDateTime) {
        this.actualTaskExecutionStartDateTime = actualTaskExecutionStartDateTime;
    }

    /**
     * @return The isAutoCreation
     */
    public Boolean getIsAutoCreation() {
        return isAutoCreation;
    }

    /**
     * @param isAutoCreation The IsAutoCreation
     */
    public void setIsAutoCreation(Boolean isAutoCreation) {
        this.isAutoCreation = isAutoCreation;
    }

    /**
     * @return The executorId
     */
    public Integer getExecutorId() {
        return executorId;
    }

    /**
     * @param executorId The ExecutorId
     */
    public void setExecutorId(Integer executorId) {
        this.executorId = executorId;
    }

    /**
     * @return The executorName
     */
    public String getExecutorName() {
        return executorName;
    }

    /**
     * @param executorName The ExecutorName
     */
    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    /**
     * @return The createdDateTime
     */
    public String getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * @param createdDateTime The CreatedDateTime
     */
    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * @return The createdById
     */
    public Integer getCreatedById() {
        return createdById;
    }

    /**
     * @param createdById The CreatedById
     */
    public void setCreatedById(Integer createdById) {
        this.createdById = createdById;
    }

    /**
     * @return The createdByName
     */
    public String getCreatedByName() {
        return createdByName;
    }

    /**
     * @param createdByName The CreatedByName
     */
    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    /**
     * @return The approvedById
     */
    public Integer getApprovedById() {
        return approvedById;
    }

    /**
     * @param approvedById The ApprovedById
     */
    public void setApprovedById(Integer approvedById) {
        this.approvedById = approvedById;
    }

    /**
     * @return The approvedByName
     */
    public String getApprovedByName() {
        return approvedByName;
    }

    /**
     * @param approvedByName The ApprovedByName
     */
    public void setApprovedByName(String approvedByName) {
        this.approvedByName = approvedByName;
    }

    /**
     * @return The relatedTaskId
     */
    public Integer getRelatedTaskId() {
        return relatedTaskId;
    }

    /**
     * @param relatedTaskId The RelatedTaskId
     */
    public void setRelatedTaskId(Integer relatedTaskId) {
        this.relatedTaskId = relatedTaskId;
    }

    /**
     * @return The unitPostId
     */
    public Integer getUnitPostId() {
        return unitPostId;
    }

    /**
     * @param unitPostId The UnitPostId
     */
    public void setUnitPostId(Integer unitPostId) {
        this.unitPostId = unitPostId;
    }

    /**
     * @return The unitPostName
     */
    public String getUnitPostName() {
        return unitPostName;
    }

    /**
     * @param unitPostName The UnitPostName
     */
    public void setUnitPostName(String unitPostName) {
        this.unitPostName = unitPostName;
    }

    /**
     * @return The additionalComments
     */
    public String getAdditionalComments() {
        return additionalComments;
    }

    /**
     * @param additionalComments The AdditionalComments
     */
    public void setAdditionalComments(String additionalComments) {
        this.additionalComments = additionalComments;
    }

    /**
     * @return The assignedById
     */
    public Integer getAssignedById() {
        return assignedById;
    }

    /**
     * @param assignedById The AssignedById
     */
    public void setAssignedById(Integer assignedById) {
        this.assignedById = assignedById;
    }

    /**
     * @return The assignedByName
     */
    public String getAssignedByName() {
        return assignedByName;
    }

    /**
     * @param assignedByName The AssignedByName
     */
    public void setAssignedByName(String assignedByName) {
        this.assignedByName = assignedByName;
    }

    /**
     * @return The barrackId
     */
    public Integer getBarrackId() {
        return barrackId;
    }

    /**
     * @param barrackId The BarrackId
     */
    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    /**
     * @return The taskExecutionResult
     */
    public String getTaskExecutionResult() {
        return taskExecutionResult;
    }

    /**
     * @param taskExecutionResult The TaskExecutionResult
     */
    public void setTaskExecutionResult(String taskExecutionResult) {
        this.taskExecutionResult = taskExecutionResult;
    }

    /**
     * @return The estimatedTaskExecutionEndDateTime
     */
    public String getEstimatedTaskExecutionEndDateTime() {
        return estimatedTaskExecutionEndDateTime;
    }

    /**
     * @param estimatedTaskExecutionEndDateTime The EstimatedTaskExecutionEndDateTime
     */
    public void setEstimatedTaskExecutionEndDateTime(String estimatedTaskExecutionEndDateTime) {
        this.estimatedTaskExecutionEndDateTime = estimatedTaskExecutionEndDateTime;
    }

    /**
     * @return The actualTaskExecutionEndDateTime
     */
    public String getActualTaskExecutionEndDateTime() {
        return actualTaskExecutionEndDateTime;
    }

    /**
     * @param actualTaskExecutionEndDateTime The ActualTaskExecutionEndDateTime
     */
    public void setActualTaskExecutionEndDateTime(String actualTaskExecutionEndDateTime) {
        this.actualTaskExecutionEndDateTime = actualTaskExecutionEndDateTime;
    }

    /**
     * @return The isPostMandatory
     */
    public Boolean getIsPostMandatory() {
        return isPostMandatory;
    }

    /**
     * @param isPostMandatory The IsPostMandatory
     */
    public void setIsPostMandatory(Boolean isPostMandatory) {
        this.isPostMandatory = isPostMandatory;
    }

    /**
     * @return The taskReviewStatusId
     */
    public Integer getTaskReviewStatusId() {
        return taskReviewStatusId;
    }

    /**
     * @param taskReviewStatusId The TaskReviewStatusId
     */
    public void setTaskReviewStatusId(Integer taskReviewStatusId) {
        this.taskReviewStatusId = taskReviewStatusId;
    }

    /**
     * @return The estimatedTravelTime
     */
    public Integer getEstimatedTravelTime() {
        return estimatedTravelTime;
    }

    /**
     * @param estimatedTravelTime The EstimatedTravelTime
     */
    public void setEstimatedTravelTime(Integer estimatedTravelTime) {
        this.estimatedTravelTime = estimatedTravelTime;
    }

    /**
     * @return The estimatedDistance
     */
    public double getEstimatedDistance() {
        return estimatedDistance;
    }

    /**
     * @param estimatedDistance The EstimatedDistance
     */
    public void setEstimatedDistance(double estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    /**
     * @return The unitTypeId
     */
    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    /**
     * @param unitTypeId The UnitTypeId
     */
    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }


    public void setSelectReasonId(Integer selectReason) {
        this.selectReasonId = selectReason;
    }


    public Integer getSelectReasonId() {
        return selectReasonId;
    }

    public Integer getOtherReasonId() {
        return otherReasonId;
    }

    public void setOtherReasonId(Integer otherReasonId) {
        this.otherReasonId = otherReasonId;
    }
}