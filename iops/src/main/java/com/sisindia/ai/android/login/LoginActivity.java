package com.sisindia.ai.android.login;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.UpdatedConfigurationApiCalls;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.rotaDTO.RotaDBGenericStatement;
import com.sisindia.ai.android.database.unitDTO.UnitDBGenericStatement;
import com.sisindia.ai.android.fcmnotification.FCMTokenRegistration;
import com.sisindia.ai.android.fcmnotification.FCMTokenUpdateMO;
import com.sisindia.ai.android.fcmnotification.FcmGuardModelInsertion;
import com.sisindia.ai.android.fcmnotification.GuardForBranchBase;
import com.sisindia.ai.android.loadconfiguration.KitDistributionOR;
import com.sisindia.ai.android.loadconfiguration.LoadConfigBaseMO;
import com.sisindia.ai.android.loadconfiguration.LoadConfigurationData;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.network.request.OtpInputRequestMO;
import com.sisindia.ai.android.network.response.AccessTokenResponse;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.models.RotaTaskActivityListMO;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;


public class LoginActivity extends BaseActivity implements OnSMSReceivedListener {
    public static OnSMSReceivedListener onSMSReceivedListener;
    @Bind(R.id.edit_enterotp)
    EditText editTextEnterOtp;
    @Bind(R.id.button_login)
    Button loginButton;
    @Bind(R.id.button_resendotp)
    Button resendotpButton;
    @Bind(R.id.tv_register_number)
    TextView registerNumberLabel;
    @Bind(R.id.loginParentlayout)
    RelativeLayout mloginParentlayout;
    @Bind(R.id.textview_count_time)
    TextView counterTimerLabel;
    private String enterOtpString = "";
    private String registerNumber;
    private AppPreferences mAppPreferences;
    private SISAITrackingDB sisaiTrackingDBInstance;
    //    private AppPreferences appPreferences;
    private LoadConfigurationData loadConfigurationData;
    private boolean isAppUserAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        onSMSReceivedListener = this;
        ButterKnife.bind(this);
        mAppPreferences = new AppPreferences(LoginActivity.this);
        mAppPreferences.setFirstTimeLaunched(true);
        setUpViews();
    }

    private void initializeData() {
        sisaiTrackingDBInstance = IOPSApplication.getInstance().getSISAITrackingDBInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeData();
    }

    private void setUpViews() {
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            registerNumber = bundle.getString(TableNameAndColumnStatement.registerNumber);
        }
        registerNumberLabel.setText(getResources().getString(R.string.register_number_label) + " " + registerNumber);
        resendotpButton.setEnabled(false);
        CountDown();
    }

    public void doLogin(View view) {
        enterOtpString = editTextEnterOtp.getText().toString().trim();
        if (!TextUtils.isEmpty(enterOtpString)) {
            if (NetworkUtil.isConnected) {
                loginApiCall();
            } else {
                snackBarWithMesg(mloginParentlayout, getResources().getString(R.string.no_internet));
            }
        } else {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.enter_otp), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public void resendOtp(View view) {
        if (NetworkUtil.isConnected) {
            resendotpButton.setBackgroundResource(R.drawable.resendotp_button);
            resendotpButton.setTextColor(ContextCompat.getColor(LoginActivity.this,
                    R.color.color_white_trans_fifty));
            editTextEnterOtp.setText("");
            resendotpButton.setEnabled(false);
            resendOptAPICall();
            counterTimerLabel.setVisibility(View.VISIBLE);
            CountDown();
        } else {
            snackBarWithMesg(mloginParentlayout, getResources().getString(R.string.no_internet));
        }
    }

    private void CountDown() {
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                counterTimerLabel.setText(getResources().getString(R.string.buttonEnableTxt_login) + " " +
                        millisUntilFinished / 1000 + " sec");
            }

            public void onFinish() {
                resendotpButton.setBackgroundResource(R.drawable.sendotp_button);
                resendotpButton.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.textGery));
                counterTimerLabel.setVisibility(View.INVISIBLE);
                resendotpButton.setEnabled(true);
            }
        }.start();
    }

    private OtpInputRequestMO createOTPRequest() {
        OtpInputRequestMO otpRequest = new OtpInputRequestMO();
        otpRequest.setPhoneNumber(registerNumber);
        otpRequest.setIMEI1(mAppPreferences.getIMEIsim1().trim());
        otpRequest.setIMEI2(mAppPreferences.getIMEIsim2().trim());
        otpRequest.setOptFlag(true);
        otpRequest.setAppVersion(String.valueOf(BuildConfig.VERSION_NAME));
        otpRequest.setDeviceToken(mAppPreferences.getFCMRegToken());
        otpRequest.setLanguageId(mAppPreferences.getLanguageId());
        return otpRequest;
    }

    /**
     * api call to get the OTP
     *
     * @Query "phoneNumber" String phone,
     * @Query resendOtp boolean isresend
     * POST METHOD
     */
    private void resendOptAPICall() {
        showprogressbar(R.string.loading_please_wait);
        sisClient.getApi().sendOTP(createOTPRequest(),
                new Callback<CommonResponse>() {
                    @Override
                    public void success(CommonResponse commonResponses, Response response) {
                        hideprogressbar();
                        Timber.d("sendOtpAPICall %s", commonResponses);
                        switch (commonResponses.getStatusCode()) {
                            case 200:
                                Toast.makeText(LoginActivity.this, commonResponses.getStatusMessage(), Toast.LENGTH_SHORT).show();
                                Timber.i("Content Length :" + response.getBody().length());
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideprogressbar();
                        Crashlytics.logException(error);
                    }
                });
    }

    /**
     * api call to get the acCess token from the server
     *
     * @Param String granttype,
     * @Param String phoneno,
     * @Param String otp
     * POST METHOD
     */

    private void loginApiCall() {

        showprogressbar(R.string.loadConfigureText);
        sisClient.getApi().login("password", registerNumber, enterOtpString,
                new Callback<AccessTokenResponse>() {
                    @Override
                    public void success(AccessTokenResponse responses, Response response) {
                        editTextEnterOtp.setText("");
                        if (null != response && response.getStatus() == 200 && !responses.getToken_type().equalsIgnoreCase("")) {
                            mAppPreferences.saveAccessToken(responses.getAccess_token());
                            mAppPreferences.saveToken_type(responses.getToken_type());
                            mAppPreferences.saveAppVersion(BuildConfig.VERSION_CODE);
                            mAppPreferences.saveloginstatus(true);
//                            NotificationUtils notificationUtils = new NotificationUtils(LoginActivity.this);
//                            notificationUtils.sendRegistrationToServer(mAppPreferences.getFCMRegToken());
                            sendRegistrationToServer(mAppPreferences.getFCMRegToken());
                            sisClient.getApi().loadConfiguration(new Callback<LoadConfigBaseMO>() {
                                @Override
                                public void success(LoadConfigBaseMO loadConfigurationBaseMO, Response response) {
                                    if (null != loadConfigurationBaseMO) {
                                        switch (loadConfigurationBaseMO.getStatusCode()) {
                                            case 200:
                                                loadConfigurationData = loadConfigurationBaseMO.getLoadConfigurationData();
                                                Timber.i("Content Length Load:" + response.getBody().length());
                                                if (null != loadConfigurationData) {
                                                    new LoadConfiguration(loadConfigurationData).execute();
                                                } else {
                                                    hideprogressbar();
                                                    snackBarWithMesg(mloginParentlayout, getResources().getString(R.string.NoLoadCOnfigData));
                                                }
                                                break;
                                            default:
                                                Toast.makeText(LoginActivity.this, loadConfigurationBaseMO.getStatusMessage(), Toast.LENGTH_SHORT).show();
                                                hideprogressbar();
                                                break;
                                        }
                                    } else {
                                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.API_FAIL_MSG), Toast.LENGTH_SHORT).show();
                                        hideprogressbar();
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Toast.makeText(LoginActivity.this, "Sorry for inconvenience, API Error", Toast.LENGTH_SHORT).show();
                                    hideprogressbar();
                                }
                            });
                        } else {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.API_FAIL_MSG), Toast.LENGTH_SHORT).show();
                            hideprogressbar();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideprogressbar();
                        Crashlytics.logException(error);
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.API_FAIL_MSG), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addRotaTaskActivityList() {
        RotaTaskActivityListMO rotaTaskActivityListMO = new RotaTaskActivityListMO();
        RotaTaskListMO rotaTaskListMO;
        ArrayList<RotaTaskListMO> rotaTaskList;
        int titleSize = RotaTaskTypeEnum.values().length;
        rotaTaskList = new ArrayList<>();
        rotaTaskListMO = new RotaTaskListMO();
        rotaTaskListMO.setRotaTaskName("Select a Task");
        rotaTaskListMO.setRotaTaskBlackIcon(0);
        rotaTaskListMO.setRotaTaskWhiteIcon(0);
        rotaTaskListMO.setRotaTaskActivityId(0);
        rotaTaskList.add(rotaTaskListMO);
        rotaTaskActivityListMO.setRotaTaskListMO(rotaTaskList);
        for (int index = 1; index <= titleSize; index++) {
            rotaTaskListMO = new RotaTaskListMO();
            rotaTaskListMO.setRotaTaskName(RotaTaskTypeEnum.valueOf(index).name().replace("_", " "));
            rotaTaskListMO.setRotaTaskActivityId(index);
            rotaTaskListMO.setRotaTaskWhiteIcon(index);
            rotaTaskListMO.setRotaTaskBlackIcon(index);
            rotaTaskList.add(rotaTaskListMO);
            rotaTaskActivityListMO.setRotaTaskListMO(rotaTaskList);
        }
        IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertRotaTaskActivityList(rotaTaskActivityListMO);
    }

    private synchronized void insertLoadConfigurationData(LoadConfigurationData loadConfigurationData) {
        try {
            addRotaTaskActivityList();
            if (null != loadConfigurationData.getUserProfile()) {
                isAppUserAvailable = IOPSApplication.getInstance().getInsertionInstance().insertAppUserTable(loadConfigurationData.getUserProfile());
            }
            if (null != loadConfigurationData.getRankMaster() && loadConfigurationData.getRankMaster().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertRankMasterTable(loadConfigurationData.getRankMaster());
            }
            if (null != loadConfigurationData.getHolidayCalendar() && loadConfigurationData.getHolidayCalendar().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertHolidayCalendar(loadConfigurationData.getHolidayCalendar());
            }
            if (null != loadConfigurationData.getEmployeeLeaveCalendar() && loadConfigurationData.getEmployeeLeaveCalendar().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertLeaveTable(loadConfigurationData.getEmployeeLeaveCalendar());
            }
            if (null != loadConfigurationData.getBranch()) {
                IOPSApplication.getInstance().getInsertionInstance().insertBranchTable(loadConfigurationData.getBranch());
            }
            if (null != loadConfigurationData.getBarrack() && loadConfigurationData.getBarrack().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertBarrackTable(loadConfigurationData.getBarrack());
                /**
                 * Unit Authorized strength table insertion (Barrack Strength)
                 */
                for (Barrack barrack : loadConfigurationData.getBarrack()) {
                    if (null != barrack.getBarrackStrengthList()) {
                        IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().
                                insertUnitBarrackAuthorizedStrength(barrack.getBarrackStrengthList(), false);
                    }
                }
            }
            if (null != loadConfigurationData.getAreas()) {
                IOPSApplication.getInstance().getInsertionInstance().insertInAreaTable(loadConfigurationData.getAreas());
            }
            if (null != loadConfigurationData.getmSecurityRiskMOs() && loadConfigurationData.getmSecurityRiskMOs().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertSecurityRiskQuestionToTable(loadConfigurationData.getmSecurityRiskMOs());
            }
            if (null != loadConfigurationData.getmSecurityRiskQuestionOptionMappingMOs() && loadConfigurationData.getmSecurityRiskQuestionOptionMappingMOs().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertSecurityRiskQuestionOptionsToTable(loadConfigurationData.getmSecurityRiskQuestionOptionMappingMOs());
            }
            if (null != loadConfigurationData.getLookupModel() && loadConfigurationData.getLookupModel().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertLookUpDataToTable(loadConfigurationData.getLookupModel());
            }
            if (loadConfigurationData.clientCoordinationQuestions != null && loadConfigurationData.clientCoordinationQuestions.size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertIntoClientCoordinationTable(loadConfigurationData.clientCoordinationQuestions);
//                    appPreferences.updateCCRQuestionAPIFlag(true);
            }
            if (null != loadConfigurationData.getBarrackInspectionQuestionMO() && loadConfigurationData.getBarrackInspectionQuestionMO().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertBarrackInspectionQuestionTable(loadConfigurationData.getBarrackInspectionQuestionMO());
            }
            if (null != loadConfigurationData.getBarrackInspectionOptionMO() && loadConfigurationData.getBarrackInspectionOptionMO().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertBarrackInspectionAnswerTable(loadConfigurationData.getBarrackInspectionOptionMO());
            }
            if (null != loadConfigurationData.getIssueMatrixModel() && loadConfigurationData.getIssueMatrixModel().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertIssueMatrixTable(loadConfigurationData.getIssueMatrixModel());
                appPreferences.updateCCRIssueMatrixAPIFlag(true);
            }
            if (null != loadConfigurationData.getMasterActionPlanModel() && loadConfigurationData.getMasterActionPlanModel().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertActionPlanToTable(loadConfigurationData.getMasterActionPlanModel());
//                appPreferences.updateMasterActionPlanAPIFlag(true);
            }
            if (loadConfigurationData.getKitItemModel() != null && loadConfigurationData.getKitItemModel().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertKitItemTable(loadConfigurationData.getKitItemModel());
            }
            if (loadConfigurationData.getKitApparelSizes() != null && loadConfigurationData.getKitApparelSizes().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertKitSizeTable(loadConfigurationData.getKitApparelSizes());
            }
            if (loadConfigurationData.getKitDistributions() != null && loadConfigurationData.getKitDistributions().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertKitDistributonTable(loadConfigurationData.getKitDistributions());
            }
            if (loadConfigurationData.getKitDistributions() != null && loadConfigurationData.getKitDistributions().size() != 0) {
                for (KitDistributionOR kitDistributionOR : loadConfigurationData.getKitDistributions()) {
                    if (kitDistributionOR != null) {
                        IOPSApplication.getInstance().getInsertionInstance().insertKitDistributonItemTable(kitDistributionOR.getKitDistributionItem(), null);
                    }
                }
            }

            if (loadConfigurationData.getKitItemSizes() != null && loadConfigurationData.getKitItemSizes().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertKitItemSizesTable(loadConfigurationData.getKitItemSizes());
            }
            if (null != loadConfigurationData.getUnits() && loadConfigurationData.getUnits().size() != 0) {
                UnitDBGenericStatement.UnitModelInsertion(loadConfigurationData.getUnits(), false, null);
            }
            if (null != loadConfigurationData.getRotaMO()) {
                RotaDBGenericStatement.RotaModelInsertion(loadConfigurationData.getRotaMO(), false, null);
            }
            if (null != loadConfigurationData.getEquipmentsList()) {
                IOPSApplication.getInstance().getInsertionInstance().insertEquipmentListTable(loadConfigurationData.getEquipmentsList());
            }
            if (null != loadConfigurationData.getRecuirtementMOList() && loadConfigurationData.getRecuirtementMOList().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertRecuirtmentListTable(loadConfigurationData.getRecuirtementMOList());
            }

            //Get and update 'UnitType table details' and update app preference values respectively
            if (null != loadConfigurationData.getUnitTypeList() && loadConfigurationData.getUnitTypeList().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertUnitTypeDetails(loadConfigurationData.getUnitTypeList());
                appPreferences.updateUnitTypeTableSyncedFlag(true);
            }

            appPreferences.setLoadConfigurationFlag(true);

        } catch (SQLiteException sqliteException) {
            Timber.e("SqlException %s", sqliteException.getCause());
            sqliteException.printStackTrace();
        } catch (Exception exception) {
            Timber.e("Exception %s", exception.getCause());
            exception.printStackTrace();
        }
    }

    private void showprogressbar(int mesg) {
        Util.showProgressBar(LoginActivity.this, mesg);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }

    @Override
    public void onSMSReceived(String otp) {
        editTextEnterOtp.setText(otp);
    }

    public class LoadConfiguration extends AsyncTask<String, String, String> {

        public LoadConfiguration(LoadConfigurationData data) {
            loadConfigurationData = data;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showprogressbar(R.string.loadConfigureText);
        }

        @Override
        protected String doInBackground(String... params) {
            insertLoadConfigurationData(loadConfigurationData);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideprogressbar();
            if (isAppUserAvailable) {

                /*
                    @Ashu: Inserting the guard related information from API to local DB
                */
                guardInformationUpdate();

                mAppPreferences.setAppUser(IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserName());
                mAppPreferences.setAppUserId(IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails().getEmployeeId());
                Intent onBoardingIntent = new Intent(LoginActivity.this, OnBoardingActivity.class);
                onBoardingIntent.putExtra(OnBoardingFactory.NO_OF_IMAGES, 4);
                onBoardingIntent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, OnBoardingFactory.POST_LOGIN);
                startActivity(onBoardingIntent);
                finish();
            } else {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.API_FAIL_MSG), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

        FCMTokenRegistration fcmTokenRegistration = new FCMTokenRegistration();
        fcmTokenRegistration.setUniqueDeviceId(token);

        sisClient.getApi().updateFCMToken(fcmTokenRegistration, new Callback<FCMTokenUpdateMO>() {
            @Override
            public void success(FCMTokenUpdateMO fcmTokenUpdateMO, Response response) {
                if (null != fcmTokenUpdateMO) {
                    switch (fcmTokenUpdateMO.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            // Toast.makeText(getApplicationContext(), getString(R.string.FCM_TOKEN_UPDATE), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);
            }
        });
    }

    //@Ashu : Writing method to update guard related information from API to Local DB

    UpdatedConfigurationApiCalls updatedConfigurationApiCalls;

    private void guardInformationUpdate() {
        int branchId = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails().getBranchId();
        sisClient.getApi().getBranchGuardsInformation(branchId, new Callback<GuardForBranchBase>() {
            @Override
            public void success(GuardForBranchBase guardForBranchBase, Response response) {
                if (response != null && response.getStatus() == HttpsURLConnection.HTTP_OK) {
                    if (null != guardForBranchBase && guardForBranchBase.statusCode == HttpURLConnection.HTTP_OK && null != guardForBranchBase.data) {
                        FcmGuardModelInsertion fcmGuardModelInsertion = new FcmGuardModelInsertion(LoginActivity.this);
                        if (fcmGuardModelInsertion.insertGuardInfo(guardForBranchBase.data.guardsForBranch))
                            appPreferences.updateGuardDetailsInsertedFlag(true);

                        /*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                        updatedConfigurationApiCalls.isGuardInformationUpdated = fcmGuardModelInsertion.insertGuardInfo(guardForBranchBase.data.guardsForBranch);
                        appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*/
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                /*updatedConfigurationApiCalls = appPreferences.getUpdatedConfigurationApiCalls();
                updatedConfigurationApiCalls.isGuardInformationUpdated = false;
                appPreferences.setUpdatedConfigurationApiCalls(updatedConfigurationApiCalls);*/

                appPreferences.updateGuardDetailsInsertedFlag(false);

                Crashlytics.logException(error);
            }
        });
    }

}
