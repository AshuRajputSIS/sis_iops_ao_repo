package com.sisindia.ai.android.database.unitRaisingDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.unitraising.RaisingDetailsMo;
import com.sisindia.ai.android.unitraising.UnitRaisingDeferredOrCancelledMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by compass on 8/2/2017.
 */

public class UnitRaisingInsertionStatementDB extends SISAITrackingDB {
    DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public UnitRaisingInsertionStatementDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public void insertUnitRaisingDetails(RaisingDetailsMo raisingDetailsMo) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues unitRaisingValue = new ContentValues();
                unitRaisingValue.put(TableNameAndColumnStatement.UNIT_ID, raisingDetailsMo.unitId);
                unitRaisingValue.put(TableNameAndColumnStatement.UNIT_RAISING_ID, raisingDetailsMo.raisingId);
                unitRaisingValue.put(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo.raisingDetails);
                unitRaisingValue.put(TableNameAndColumnStatement.UNIT_RAISING_LOCATION, raisingDetailsMo.raisingLocation);
                unitRaisingValue.put(TableNameAndColumnStatement.REMARKS, raisingDetailsMo.remarks);
                unitRaisingValue.put(TableNameAndColumnStatement.CREATED_DATE_TIME, dateTimeFormatConversionUtil.getCurrentDateTime());
                unitRaisingValue.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                sqlite.insert(TableNameAndColumnStatement.UNIT_RAISING, null, unitRaisingValue);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    public void insertUnitRaisingDeferredDetails(UnitRaisingDeferredOrCancelledMo unitRaisingDeferredOrCancelledMo) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues unitRaisingDeferredValue = new ContentValues();
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.UNIT_ID, unitRaisingDeferredOrCancelledMo.unitId);
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID, unitRaisingDeferredOrCancelledMo.unitRaisingDeferredId);
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME, unitRaisingDeferredOrCancelledMo.plannedRaisingDate);
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.DEFERRED_REASON, unitRaisingDeferredOrCancelledMo.reasonForChange);
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.CREATED_DATE_TIME, dateTimeFormatConversionUtil.getCurrentDateTime());
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.NEW_RAISING_DATE_TIME, unitRaisingDeferredOrCancelledMo.newRaisingDate + " 00:00:00");
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.REMARKS, unitRaisingDeferredOrCancelledMo.remarks);
                unitRaisingDeferredValue.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                sqlite.insert(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED, null, unitRaisingDeferredValue);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    public void insertUnitRaisingCancelledDetails(UnitRaisingDeferredOrCancelledMo unitRaisingDeferredOrCancelledMo) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                ContentValues unitRaisingCancelledValue = new ContentValues();
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.UNIT_ID, unitRaisingDeferredOrCancelledMo.unitId);
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID, unitRaisingDeferredOrCancelledMo.unitRaisingCancellationId);
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME, unitRaisingDeferredOrCancelledMo.plannedRaisingDate);
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.CANCELLATION_REASON, unitRaisingDeferredOrCancelledMo.reasonForCancel);
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.CREATED_DATE_TIME, dateTimeFormatConversionUtil.getCurrentDateTime());
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.REMARKS, unitRaisingDeferredOrCancelledMo.remarks);
                unitRaisingCancelledValue.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                sqlite.insert(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED, null, unitRaisingCancelledValue);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    public void insertUnitRaisingStrengthDetails(ArrayList<UnitRaisingStrengthMo> unitRaisingStrengthMoList) {
        SQLiteDatabase sqlite = this.getWritableDatabase();

        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                if (unitRaisingStrengthMoList != null && unitRaisingStrengthMoList.size() != 0) {

                    for (UnitRaisingStrengthMo unitRaisingStrengthMo : unitRaisingStrengthMoList) {
                        ContentValues unitRaisingStrengthValue = new ContentValues();
                        unitRaisingStrengthValue.put(TableNameAndColumnStatement.UNIT_RAISING_ID, getUnitRaisingLocalId(sqlite));
                        unitRaisingStrengthValue.put(TableNameAndColumnStatement.RANK_ID, unitRaisingStrengthMo.rankId);
                        unitRaisingStrengthValue.put(TableNameAndColumnStatement.ACTUAL_STRENGTH, unitRaisingStrengthMo.actualStrength);
                        unitRaisingStrengthValue.put(TableNameAndColumnStatement.CREATED_DATE_TIME, dateTimeFormatConversionUtil.getCurrentDateTime());
                        unitRaisingStrengthValue.put(TableNameAndColumnStatement.IS_SYNCED, 0);
                        sqlite.insert(TableNameAndColumnStatement.UNIT_RAISING_STRENGTH_DETAIL, null, unitRaisingStrengthValue);
                    }
                }
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
            }
        }
    }

    public int getUnitRaisingLocalId(SQLiteDatabase sqlite) {

        int unitRaisingLocalId = 0;
        String query = "select " + TableNameAndColumnStatement.ID + " from unit_raising where unit_raising_id = 0 ";

        Timber.i("Unit Raising %s", query);

        try {
            Cursor cursor = sqlite.rawQuery(query, null);

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        unitRaisingLocalId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID));
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return unitRaisingLocalId;
    }
}
