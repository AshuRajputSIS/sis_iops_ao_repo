package com.sisindia.ai.android.notifications;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga Prasad on 27-04-2016.
 */
public class NotificationListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NotificationReceivingMO> addNotificationList;
    Context context;
    private NotificationsViewHolder notificationViewHolder;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public NotificationListRecyclerAdapter(Context context) {
        this.context = context;
        addNotificationList = new ArrayList<NotificationReceivingMO>();

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    public void setNotificationData(List<NotificationReceivingMO> addNotificationList) {
        this.addNotificationList = addNotificationList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.notification_list_row, parent, false);
        notificationViewHolder = new NotificationsViewHolder(itemView);
        return notificationViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotificationsViewHolder notificationViewHolder = (NotificationsViewHolder) holder;
        notificationViewHolder.notificationMessage.setText(addNotificationList.get(position).getMessage());
        notificationViewHolder.notificationTitle.setText(addNotificationList.get(position).getTitle());
    }


    @Override
    public int getItemCount() {
        int count = 0;
        if (addNotificationList != null) {
            count = addNotificationList.size() == 0 ? 0 : addNotificationList.size();
        }
        return count;
    }

    private Object getObject(int position) {
        return addNotificationList.get(position);
    }

    public class NotificationsViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        TextView notificationMessage, notificationTitle;
        ImageView notificationTypeImage;

        public NotificationsViewHolder(View view) {
            super(view);
            this.notificationTitle = (CustomFontTextview) view.findViewById(R.id.notification_titile);
            this.notificationMessage = (CustomFontTextview) view.findViewById(R.id.notification_message);
            this.notificationTypeImage = (ImageView) view.findViewById(R.id.notification_type_image);
            view.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v,getLayoutPosition());
        }
    }

}

