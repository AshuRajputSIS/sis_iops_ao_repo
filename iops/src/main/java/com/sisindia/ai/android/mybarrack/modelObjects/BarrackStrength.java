package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.SerializedName;

public class BarrackStrength {

@SerializedName("Id")
public Integer id;
@SerializedName("UnitId")
public Object unitId;
@SerializedName("ShiftId")
public Object shiftId;
@SerializedName("RankId")
public Object rankId;
@SerializedName("BarrackId")
public Integer barrackId;
@SerializedName("RankAbbrevation")
public Object rankAbbrevation;
@SerializedName("RankCount")
public Integer rankCount;
@SerializedName("BarrackStrength")
public Integer barrackStrength;
@SerializedName("UnitName")
public String unitName;
@SerializedName("ShiftRankDetails")
public Object shiftRankDetails;
@SerializedName("ShiftName")
public Object shiftName;
@SerializedName("RankName")
public Object rankName;
@SerializedName("BarrackName")
public String barrackName;
@SerializedName("Strength")
public Integer strength;
@SerializedName("IsArmed")
public Boolean isArmed;
}

