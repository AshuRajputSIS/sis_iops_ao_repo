package com.sisindia.ai.android.fcmnotification;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import java.util.List;

/**
 * Created by compass on 9/19/2017.
 */

public class FcmGuardModelInsertion extends SISAITrackingDB {
    private SQLiteDatabase sqlite;

    public FcmGuardModelInsertion(Context context) {
        super(context);
    }

    public boolean insertGuardInfo(List<GuardsForBranch> guardsForBranchList) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            boolean isInsertedSuccessfully;
            try {
                if (guardsForBranchList != null && guardsForBranchList.size() != 0) {
                    sqlite.beginTransaction();
                    IOPSApplication.getInstance().getDeletionStatementDBInstance().deleteTable(TableNameAndColumnStatement.GUARDS_TABLE, sqlite);
                    insertGuardDetails(guardsForBranchList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                } else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertGuardDetails(List<GuardsForBranch> guardsForBranchList) {
        for (GuardsForBranch guardForBranch : guardsForBranchList) {
            ContentValues values = new ContentValues();
            values.put(TableNameAndColumnStatement.GUARD_NAME, guardForBranch.getEmployeeFullName());
            values.put(TableNameAndColumnStatement.GUARD_CODE, guardForBranch.getEmployeeCode());
            values.put(TableNameAndColumnStatement.GUARD_ID, guardForBranch.getEmployeeId());
            values.put(TableNameAndColumnStatement.UNIT_ID, guardForBranch.getUnitId());
            values.put(TableNameAndColumnStatement.FINE_INSTANCE, 0);
            values.put(TableNameAndColumnStatement.FINE_AMOUNT, guardForBranch.getPunishment());
            values.put(TableNameAndColumnStatement.REWARD_AMOUNT, guardForBranch.getReward());
            values.put(TableNameAndColumnStatement.LAST_FINE, guardForBranch.getLastFine());
            sqlite.insertOrThrow(TableNameAndColumnStatement.GUARDS_TABLE, null, values);
        }
    }

    public void getBranchIdOfAI(){

    }

}
