package com.sisindia.ai.android.views;

/**
 * Created by Saruchi on 18-03-2016.
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sisindia.ai.android.R;

public class CustomFontTextview extends TextView {


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomFontTextview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public CustomFontTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public CustomFontTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

    }

    public CustomFontTextview(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = a.getString(R.styleable.CustomTextView_font);
            try {
                if (!TextUtils.isEmpty(fontName)) {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" +
                            context.getResources().getString(R.string.font_medium));
                    setTypeface(myTypeface);
                } else {
                    Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),
                            "fonts/" + context.getResources().getString(R.string.font_medium));
                    setTypeface(myTypeface);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            a.recycle();
        }
    }

}