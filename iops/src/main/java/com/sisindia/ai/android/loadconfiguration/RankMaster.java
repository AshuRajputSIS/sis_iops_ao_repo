package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class RankMaster {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("RankName")
    @Expose
    private String rankName;
    @SerializedName("RankAbbrevation")
    @Expose
    private String rankAbbrevation;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("CanPossessArms")
    @Expose
    private Boolean canPossessArms;
    @SerializedName("RankCode")
    @Expose
    private String rankCode;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The rankName
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * @param rankName The RankName
     */
    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    /**
     * @return The rankAbbrevation
     */
    public String getRankAbbrevation() {
        return rankAbbrevation;
    }

    /**
     * @param rankAbbrevation The RankAbbrevation
     */
    public void setRankAbbrevation(String rankAbbrevation) {
        this.rankAbbrevation = rankAbbrevation;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The canPossessArms
     */
    public Boolean getCanPossessArms() {
        return canPossessArms;
    }

    /**
     * @param canPossessArms The CanPossessArms
     */
    public void setCanPossessArms(Boolean canPossessArms) {
        this.canPossessArms = canPossessArms;
    }

    /**
     * @return The rankCode
     */
    public String getRankCode() {
        return rankCode;
    }

    /**
     * @param rankCode The RankCode
     */
    public void setRankCode(String rankCode) {
        this.rankCode = rankCode;
    }

}
