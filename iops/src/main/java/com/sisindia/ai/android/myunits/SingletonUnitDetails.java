package com.sisindia.ai.android.myunits;

/**
 * Created by Durga Prasad on 05-05-2016.
 */
public class SingletonUnitDetails {

    private static SingletonUnitDetails singletonUnitDetails;
    private static Object myUnitHomeMO;

    private SingletonUnitDetails() {
    }

    public static synchronized SingletonUnitDetails getInstance() {
        if (singletonUnitDetails == null) {
            singletonUnitDetails = new SingletonUnitDetails();
        }
        return singletonUnitDetails;
    }

    public void setUnitDetails(Object object, int position) {
        myUnitHomeMO = null;
        myUnitHomeMO = object;

    }

    public Object getUnitDetails() {
        return myUnitHomeMO;
    }
}
