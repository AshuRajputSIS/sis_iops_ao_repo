package com.sisindia.ai.android.issues;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Durga Prasad on 28-07-2016.
 */
public class IssuesMO implements Parcelable {

    private int issueId;
    private int issueTypeId;
    private int sourceTaskId;
    private int unitId;
    private int barrackId;
    private int guardId;
    private int issueCategoryId;
    private int issueModeId;
    private int issueNatureId;
    private int issueCauseId;
    private int issueSeverityId;
    private int issueStatusId;
    private int actionPlanId;
    private String createdDateTime;
    private int createdById;
    private int assignedToId;
    private String targetActionEndDate;
    private String closureEndDate;
    private String remarks;
    private String closureRemarks;
    private String unitName;
    private String guardName;
    private String natureOfGrievance;
    private String targetClosureEndDate;
    private String employeeId;
    private int pId;
    private String ModeOfComplaint;
    private String natureOfComplaint;
    private String causeOfComplaint;
    private String complaintBy;
    private String reportedTime;
    private String actionPlan;
    private String assignedToName;
    private String status;
    private int unitContactId;
    private String unitContactName;
    private int issueMatrixId;
    private String guardCode;
    private String phoneNumber;

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getNatureOfGrievance() {
        return natureOfGrievance;
    }

    public void setNatureOfGrievance(String natureOfGrievance) {
        this.natureOfGrievance = natureOfGrievance;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTargetClosureEndDate() {
        return targetClosureEndDate;
    }

    public void setTargetClosureEndDate(String targetClosureEndDate) {
        this.targetClosureEndDate = targetClosureEndDate;
    }

    public int getIssueMatrixId() {
        return issueMatrixId;
    }

    public void setIssueMatrixId(int issueMatrixId) {
        this.issueMatrixId = issueMatrixId;
    }

    public String getUnitContactName() {
        return unitContactName;
    }

    public void setUnitContactName(String unitContactName) {
        this.unitContactName = unitContactName;
    }

    public int getUnitContactId() {
        return unitContactId;
    }

    public void setUnitContactId(int unitContactId) {
        this.unitContactId = unitContactId;
    }

    public int getActionPlanId() {
        return actionPlanId;
    }

    public void setActionPlanId(int actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    public int getAssignedToId() {
        return assignedToId;
    }

    public void setAssignedToId(int assignedToId) {
        this.assignedToId = assignedToId;
    }

    public int getBarrackId() {
        return barrackId;
    }

    public void setBarrackId(int barrackId) {
        this.barrackId = barrackId;
    }

    public String getClosureEndDate() {
        return closureEndDate;
    }

    public void setClosureEndDate(String closureEndDate) {
        this.closureEndDate = closureEndDate;
    }

    public String getClosureRemarks() {
        return closureRemarks;
    }

    public void setClosureRemarks(String closureRemarks) {
        this.closureRemarks = closureRemarks;
    }

    public int getCreatedById() {
        return createdById;
    }

    public void setCreatedById(int createdById) {
        this.createdById = createdById;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public int getGuardId() {
        return guardId;
    }

    public void setGuardId(int guardId) {
        this.guardId = guardId;
    }

    public int getIssueCategoryId() {
        return issueCategoryId;
    }

    public void setIssueCategoryId(int issueCategoryId) {
        this.issueCategoryId = issueCategoryId;
    }

    public int getIssueCauseId() {
        return issueCauseId;
    }

    public void setIssueCauseId(int issueCauseId) {
        this.issueCauseId = issueCauseId;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getIssueModeId() {
        return issueModeId;
    }

    public void setIssueModeId(int issueModeId) {
        this.issueModeId = issueModeId;
    }

    public int getIssueNatureId() {
        return issueNatureId;
    }

    public void setIssueNatureId(int issueNatureId) {
        this.issueNatureId = issueNatureId;
    }

    public int getIssueSeverityId() {
        return issueSeverityId;
    }

    public void setIssueSeverityId(int issueSeverityId) {
        this.issueSeverityId = issueSeverityId;
    }

    public int getIssueStatusId() {
        return issueStatusId;
    }

    public void setIssueStatusId(int issueStatusId) {
        this.issueStatusId = issueStatusId;
    }

    public int getIssueTypeId() {
        return issueTypeId;
    }

    public void setIssueTypeId(int issueTypeId) {
        this.issueTypeId = issueTypeId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getSourceTaskId() {
        return sourceTaskId;
    }

    public void setSourceTaskId(int sourceTaskId) {
        this.sourceTaskId = sourceTaskId;
    }

    public String getTargetActionEndDate() {
        return targetActionEndDate;
    }

    public void setTargetActionEndDate(String targetActionEndDate) {
        this.targetActionEndDate = targetActionEndDate;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public String getModeOfComplaint() {
        return ModeOfComplaint;
    }

    public void setModeOfComplaint(String modeOfComplaint) {
        ModeOfComplaint = modeOfComplaint;
    }

    public String getNatureOfComplaint() {
        return natureOfComplaint;
    }

    public void setNatureOfComplaint(String natureOfComplaint) {
        this.natureOfComplaint = natureOfComplaint;
    }

    public String getCauseOfComplaint() {
        return causeOfComplaint;
    }

    public void setCauseOfComplaint(String causeOfComplaint) {
        this.causeOfComplaint = causeOfComplaint;
    }


    public String getComplaintBy() {
        return complaintBy;
    }

    public void setComplaintBy(String complaintBy) {
        this.complaintBy = complaintBy;
    }

    public String getReportedTime() {
        return reportedTime;
    }

    public void setReportedTime(String reportedTime) {
        this.reportedTime = reportedTime;
    }

    public String getActionPlan() {
        return actionPlan;
    }

    public void setActionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
    }

    public String getAssignedToName() {
        return assignedToName;
    }

    public void setAssignedToName(String assignedToName) {
        this.assignedToName = assignedToName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public IssuesMO() {
    }


    public void setGuardCode(String guardCode) {
        this.guardCode = guardCode;
    }

    public String getGuardCode() {
        return guardCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.issueId);
        dest.writeInt(this.issueTypeId);
        dest.writeInt(this.sourceTaskId);
        dest.writeInt(this.unitId);
        dest.writeInt(this.barrackId);
        dest.writeInt(this.guardId);
        dest.writeInt(this.issueCategoryId);
        dest.writeInt(this.issueModeId);
        dest.writeInt(this.issueNatureId);
        dest.writeInt(this.issueCauseId);
        dest.writeInt(this.issueSeverityId);
        dest.writeInt(this.issueStatusId);
        dest.writeInt(this.actionPlanId);
        dest.writeString(this.createdDateTime);
        dest.writeInt(this.createdById);
        dest.writeInt(this.assignedToId);
        dest.writeString(this.targetActionEndDate);
        dest.writeString(this.closureEndDate);
        dest.writeString(this.remarks);
        dest.writeString(this.closureRemarks);
        dest.writeString(this.unitName);
        dest.writeString(this.guardName);
        dest.writeString(this.natureOfGrievance);
        dest.writeString(this.targetClosureEndDate);
        dest.writeString(this.employeeId);
        dest.writeInt(this.pId);
        dest.writeString(this.ModeOfComplaint);
        dest.writeString(this.natureOfComplaint);
        dest.writeString(this.causeOfComplaint);
        dest.writeString(this.complaintBy);
        dest.writeString(this.reportedTime);
        dest.writeString(this.actionPlan);
        dest.writeString(this.assignedToName);
        dest.writeString(this.status);
        dest.writeInt(this.unitContactId);
        dest.writeString(this.unitContactName);
        dest.writeInt(this.issueMatrixId);
        dest.writeString(this.guardCode);
    }

    protected IssuesMO(Parcel in) {
        this.issueId = in.readInt();
        this.issueTypeId = in.readInt();
        this.sourceTaskId = in.readInt();
        this.unitId = in.readInt();
        this.barrackId = in.readInt();
        this.guardId = in.readInt();
        this.issueCategoryId = in.readInt();
        this.issueModeId = in.readInt();
        this.issueNatureId = in.readInt();
        this.issueCauseId = in.readInt();
        this.issueSeverityId = in.readInt();
        this.issueStatusId = in.readInt();
        this.actionPlanId = in.readInt();
        this.createdDateTime = in.readString();
        this.createdById = in.readInt();
        this.assignedToId = in.readInt();
        this.targetActionEndDate = in.readString();
        this.closureEndDate = in.readString();
        this.remarks = in.readString();
        this.closureRemarks = in.readString();
        this.unitName = in.readString();
        this.guardName = in.readString();
        this.natureOfGrievance = in.readString();
        this.targetClosureEndDate = in.readString();
        this.employeeId = in.readString();
        this.pId = in.readInt();
        this.ModeOfComplaint = in.readString();
        this.natureOfComplaint = in.readString();
        this.causeOfComplaint = in.readString();
        this.complaintBy = in.readString();
        this.reportedTime = in.readString();
        this.actionPlan = in.readString();
        this.assignedToName = in.readString();
        this.status = in.readString();
        this.unitContactId = in.readInt();
        this.unitContactName = in.readString();
        this.issueMatrixId = in.readInt();
        this.guardCode = in.readString();
    }

    public static final Creator<IssuesMO> CREATOR = new Creator<IssuesMO>() {
        @Override
        public IssuesMO createFromParcel(Parcel source) {
            return new IssuesMO(source);
        }

        @Override
        public IssuesMO[] newArray(int size) {
            return new IssuesMO[size];
        }
    };

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }


}
