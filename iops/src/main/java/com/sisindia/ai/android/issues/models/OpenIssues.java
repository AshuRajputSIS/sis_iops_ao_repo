package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.SerializedName;

public class OpenIssues {

    @SerializedName("GrievanceCount")
    private GrievanceCount grievanceCount;
    @SerializedName("ComplaintCount")
    private ComplaintCount complaintCount;
    @SerializedName("ActionPlanCount")
    private ActionPlanCount actionPlanCount;

    /**
     * @return The grievanceCount
     */
    public GrievanceCount getGrievanceCount() {
        return grievanceCount;
    }

    /**
     * @param grievanceCount The GrievanceCount
     */
    public void setGrievanceCount(GrievanceCount grievanceCount) {
        this.grievanceCount = grievanceCount;
    }

    /**
     * @return The complaintCount
     */
    public ComplaintCount getComplaintCount() {
        return complaintCount;
    }

    /**
     * @param complaintCount The ComplaintCount
     */
    public void setComplaintCount(ComplaintCount complaintCount) {
        this.complaintCount = complaintCount;
    }

    /**
     * @return The actionPlanCount
     */
    public ActionPlanCount getActionPlanCount() {
        return actionPlanCount;
    }

    /**
     * @param actionPlanCount The ActionPlanCount
     */
    public void setActionPlanCount(ActionPlanCount actionPlanCount) {
        this.actionPlanCount = actionPlanCount;
    }
}
