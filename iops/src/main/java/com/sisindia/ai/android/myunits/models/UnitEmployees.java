package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shankar on 23/6/16.
 */

public class UnitEmployees implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("UnitId")
    private int UnitId;
    @SerializedName("EmployeeId")
    private int EmployeeId;
    @SerializedName("IsActive")
    private boolean IsActive;
    @SerializedName("DeploymentDate")
    private String DeploymentDate;
    @SerializedName("WeeklyOffDay")
    private int WeeklyOffDay;
    @SerializedName("EmployeeNo")
    private String EmployeeNo;
    @SerializedName("EmployeeFullName")
    private String EmployeeFullName;
    @SerializedName("EmployeContactNo")
    private String EmployeContactNo;
    @SerializedName("EmployeeRank")
    private List<EmployeeRank> employeeRank;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUnitId() {
        return UnitId;
    }

    public void setUnitId(int unitId) {
        UnitId = unitId;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getDeploymentDate() {
        return DeploymentDate;
    }

    public void setDeploymentDate(String deploymentDate) {
        DeploymentDate = deploymentDate;
    }

    public int getWeeklyOffDay() {
        return WeeklyOffDay;
    }

    public void setWeeklyOffDay(int weeklyOffDay) {
        WeeklyOffDay = weeklyOffDay;
    }

    public String getEmployeeNo() {
        return EmployeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        EmployeeNo = employeeNo;
    }

    public String getEmployeeFullName() {
        return EmployeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        EmployeeFullName = employeeFullName;
    }

    public String getEmployeContactNo() {
        return EmployeContactNo;
    }

    public void setEmployeContactNo(String employeContactNo) {
        EmployeContactNo = employeContactNo;
    }

    public List<EmployeeRank> getEmployeeRank() {
        return employeeRank;
    }

    public void setEmployeeRank(List<EmployeeRank> employeeRank) {
        this.employeeRank = employeeRank;
    }
}
