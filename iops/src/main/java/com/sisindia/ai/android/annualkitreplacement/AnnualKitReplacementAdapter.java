package com.sisindia.ai.android.annualkitreplacement;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.issues.models.NoDataAvailable;
import com.sisindia.ai.android.rota.daycheck.LastInspectionReviewAdapter;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 21-09-2016.
 */

public class AnnualKitReplacementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Resources resources;
    private ArrayList<Object> kitReplacementDetails;
    private  Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private int VIEW_TYPE_HEADER = 1;
    private int VIEW_TYPE_ROW = 2;
    private int VIEW_TYPE_NO_DATA = 3;
    private LayoutInflater layoutInflater;


    public AnnualKitReplacementAdapter(Context context) {
        //this.screenName = screenName;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        resources = context.getResources();
        kitReplacementDetails = new ArrayList<>();

    }


    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setAnnualKitReplacemntDetails(ArrayList<Object> kitReplacementDetailsList) {
        if(kitReplacementDetailsList != null) {
            kitReplacementDetails.clear();
            kitReplacementDetails.addAll(kitReplacementDetailsList);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;


        switch (viewType) {

            case 1:
                View headerItemView = layoutInflater.inflate(R.layout.kit_replacement_header, parent, false);
                recycleViewHolder = new AnnualKitReplacementHeaderViewHolder(headerItemView);
                break;

            case 2:
                View rowItemView = layoutInflater.inflate(R.layout.kit_replacement_row, parent, false);
                recycleViewHolder = new AnnualKitReplacementRowViewHolder(rowItemView);
                break;
            case 3:
                View noDataAvailableView = layoutInflater.inflate(R.layout.nodata_available, parent, false);
                recycleViewHolder = new NoDataAvailableViewHolder(noDataAvailableView);
                break;

            default:
                break;
        }

        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof AnnualKitReplacementHeaderViewHolder ) {
            AnnualKitReplacementHeaderViewHolder annualKitReplacementHeaderViewHolder = (AnnualKitReplacementHeaderViewHolder) holder;
            Object object = getObject(position);
            if(object instanceof AnnualKitReplacementMO) {
                AnnualKitReplacementMO annualKitReplacementMO = (AnnualKitReplacementMO) object;
                if(annualKitReplacementMO.getAddedThisMonth() != null){
                    annualKitReplacementHeaderViewHolder.addedThisMonthCountTv.setText(String.valueOf(annualKitReplacementMO.getAddedThisMonth()));
                }
                if(annualKitReplacementMO.getDistributedThisMonth() != null){

                    annualKitReplacementHeaderViewHolder.distributedCountTv.setText(String.valueOf(annualKitReplacementMO.getDistributedThisMonth()));
                }
                if(annualKitReplacementMO.getDueLastMonth() != null){
                    annualKitReplacementHeaderViewHolder.dueSinceCountTv.setText(String.valueOf(annualKitReplacementMO.getDueLastMonth()));
                }
                if(annualKitReplacementMO.getPending() != null){
                    annualKitReplacementHeaderViewHolder.pendingCountTv.setText(String.valueOf(annualKitReplacementMO.getPending()));
                }


            }

        }

        else if(holder instanceof AnnualKitReplacementRowViewHolder ){

            AnnualKitReplacementRowViewHolder annualKitReplacementRowViewHolder = (AnnualKitReplacementRowViewHolder) holder;
            Object object = getObject(position);
            if(object instanceof  AnnualOrAdhocKitItemMO){
                AnnualOrAdhocKitItemMO annualOrAdhocKitItemMO = (AnnualOrAdhocKitItemMO) object;
                if(annualOrAdhocKitItemMO.getUnitName() != null){
                    annualKitReplacementRowViewHolder.unitNameKitTv.setText(String.valueOf(annualOrAdhocKitItemMO.getUnitName()));
                }
                else {
                    annualKitReplacementRowViewHolder.unitNameKitTv.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
                }
                if(annualOrAdhocKitItemMO.getIssuedKitCount() != null){
                    int IssueKitCount = annualOrAdhocKitItemMO.getIssuedKitCount();
                    String quantityString = resources.getQuantityString(R.plurals.distributed_kit_item, IssueKitCount, IssueKitCount);
                    annualKitReplacementRowViewHolder.distributedKitCount.setText(quantityString);
                }
                else {
                    annualKitReplacementRowViewHolder.distributedKitCount.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
                }
                if(annualOrAdhocKitItemMO.getUnPaidKits() != null){
                    int unpaidKitCount = annualOrAdhocKitItemMO.getUnPaidKits();
                    String quantityString = resources.getQuantityString(R.plurals.unpaid_kit_count, unpaidKitCount, unpaidKitCount);
                    annualKitReplacementRowViewHolder.unPaidKitCountTv.setText(", "+quantityString);
                }
                else {
                    annualKitReplacementRowViewHolder.unPaidKitCountTv.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
                }
                if(annualOrAdhocKitItemMO.getPendingKitCount() != null){

                    int pendingKitCount = annualOrAdhocKitItemMO.getPendingKitCount();
                    String quantityString = resources.getQuantityString(R.plurals.pending_kit_item, pendingKitCount, pendingKitCount);
                    annualKitReplacementRowViewHolder.pendingKitTv.setText(quantityString);
                }
                else {
                    annualKitReplacementRowViewHolder.pendingKitTv.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
                }
                if(annualOrAdhocKitItemMO.getTotalItems() != null){
                    int totalKitCount = annualOrAdhocKitItemMO.getTotalItems();
                    String quantityString = resources.getQuantityString(R.plurals.kit_item, totalKitCount, totalKitCount);
                    annualKitReplacementRowViewHolder.totalItemsTv.setText(", "+quantityString );
                }
                else {
                    annualKitReplacementRowViewHolder.totalItemsTv.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
                }
            }
        }
        else if(holder instanceof  NoDataAvailableViewHolder) {

            NoDataAvailableViewHolder noDataAvailableViewHolder = (NoDataAvailableViewHolder) holder;
            noDataAvailableViewHolder.noDataAvailable.setText(context.getResources().getString(R.string.NO_DATA_AVAILABLE));
            noDataAvailableViewHolder.noDataAvailable.setGravity(Gravity.CENTER);

        }

    }


    @Override
    public int getItemCount() {

        int count = kitReplacementDetails == null ? 0 : kitReplacementDetails.size();
        return count;
    }


    public Object getObject(int position) {
        return kitReplacementDetails.get(position);
    }


    @Override
    public int getItemViewType(int position) {

        if(getObject(position) instanceof AnnualKitReplacementMO)
            return  VIEW_TYPE_HEADER;
        if(getObject(position) instanceof AnnualOrAdhocKitItemMO)
            return  VIEW_TYPE_ROW;
        if(getObject(position) instanceof NoDataAvailable)
            return  VIEW_TYPE_NO_DATA;


        return -1;

    }

    public class AnnualKitReplacementHeaderViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.addedThisMonthTv)
        CustomFontTextview addedThisMonthTv;
        @Bind(R.id.distributedTv)
        CustomFontTextview distributedTv;
        @Bind(R.id.pendingTv)
        CustomFontTextview pendingTv;
        @Bind(R.id.distributedCountTv)
        CustomFontTextview distributedCountTv;
        @Bind(R.id.dueSinceCountTv)
        CustomFontTextview dueSinceCountTv;
        @Bind(R.id.pendingCountTv)
        CustomFontTextview pendingCountTv;
        @Bind(R.id.dueSinceTv)
        CustomFontTextview dueSinceTv;
        @Bind(R.id.addedThisMonthCountTv)
        CustomFontTextview addedThisMonthCountTv;

        AnnualKitReplacementHeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }


    public class AnnualKitReplacementRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.kitImageView)
        ImageView kitImageView;
        @Bind(R.id.unitNameKitTv)
        CustomFontTextview unitNameKitTv;
        @Bind(R.id.distributedKitCount)
        CustomFontTextview distributedKitCount;
        @Bind(R.id.totalItemsTv)
        CustomFontTextview totalItemsTv;
        @Bind(R.id.pendingKitTv)
        CustomFontTextview pendingKitTv;
        @Bind(R.id.unPaidKitCount)
        CustomFontTextview unPaidKitCountTv;


        AnnualKitReplacementRowViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v,getLayoutPosition());
        }
    }

    public class NoDataAvailableViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.noDataAvailable)
        TextView noDataAvailable;

        public NoDataAvailableViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}




