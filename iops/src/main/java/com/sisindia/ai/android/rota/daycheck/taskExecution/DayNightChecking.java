package com.sisindia.ai.android.rota.daycheck.taskExecution;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DayNightChecking {

    @SerializedName("GuardDetails")
    private List<GuardDetail> guardDetails = new ArrayList<>();

    @SerializedName("DutyRegisterCheck")
    private DutyRegisterCheck dutyRegisterCheck;

    @SerializedName("ClientRegisterCheck")
    private ClientRegisterCheck clientRegisterCheck;

    @SerializedName("SecurityRiskObservation")
    private SecurityRiskObservation securityRiskObservation;

    @SerializedName("ClientHandShake")
    private ClientHandShakeMO clientHandShakeMO;

    @SerializedName("UnitPost")
    public List<NfcUnitPost> nfcUnitPost = new ArrayList<>();

    @SerializedName("SecurityRisks")
    private List<SecurityRisk> securityRisks = new ArrayList<>();

    @SerializedName("GEOLocation")
    private String geoLocation;

    @SerializedName("IsSelfieTaken")
    public boolean isSelfieTaken;

    /**
     * @return The securityRisks
     */
    public List<SecurityRisk> getSecurityRisks() {
        return securityRisks;
    }

    /**
     * @param securityRisks The SecurityRisks
     */
    public void setSecurityRisks(List<SecurityRisk> securityRisks) {
        this.securityRisks = securityRisks;
    }


    /**
     * @return The guardDetails
     */
    public List<GuardDetail> getGuardDetails() {
        return guardDetails;
    }

    /**
     * @param guardDetails The GuardDetails
     */
    public void setGuardDetails(List<GuardDetail> guardDetails) {
        this.guardDetails = guardDetails;
    }

    /**
     * @return The dutyRegisterCheck
     */
    public DutyRegisterCheck getDutyRegisterCheck() {
        return dutyRegisterCheck;
    }

    /**
     * @param dutyRegisterCheck The DutyRegisterCheck
     */
    public void setDutyRegisterCheck(DutyRegisterCheck dutyRegisterCheck) {
        this.dutyRegisterCheck = dutyRegisterCheck;
    }

    /**
     * @return The clientRegisterCheck
     */
    public ClientRegisterCheck getClientRegisterCheck() {
        return clientRegisterCheck;
    }

    /**
     * @param clientRegisterCheck The ClientRegisterCheck
     */
    public void setClientRegisterCheck(ClientRegisterCheck clientRegisterCheck) {
        this.clientRegisterCheck = clientRegisterCheck;
    }

    /**
     * @return The securityRiskObservation
     */
    public SecurityRiskObservation getSecurityRiskObservation() {
        return securityRiskObservation;
    }

    /**
     * @param securityRiskObservation The SecurityRiskObservation
     */
    public void setSecurityRiskObservation(SecurityRiskObservation securityRiskObservation) {
        this.securityRiskObservation = securityRiskObservation;
    }

    /**
     * @return The clientHandShakeMO
     */
    public ClientHandShakeMO getClientHandShakeMO() {
        return clientHandShakeMO;
    }

    /**
     * @param clientHandShakeMO The ClientHandShakeMO
     */
    public void setClientHandShakeMO(ClientHandShakeMO clientHandShakeMO) {
        this.clientHandShakeMO = clientHandShakeMO;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }

    @Override
    public String toString() {
        return "DayNightChecking{" +
                "guardDetails=" + guardDetails +
                ", dutyRegisterCheck=" + dutyRegisterCheck +
                ", clientRegisterCheck=" + clientRegisterCheck +
                ", securityRiskObservation=" + securityRiskObservation +
                ", clientHandShakeMO=" + clientHandShakeMO +
                ", nfcUnitPost=" + nfcUnitPost +
                ", securityRisks=" + securityRisks +
                ", geoLocation='" + geoLocation + '\'' +
                '}';
    }
}


