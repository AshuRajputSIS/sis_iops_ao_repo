package com.sisindia.ai.android.unitraising;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.unitRaisingDTO.UnitRaisingInsertionStatementDB;
import com.sisindia.ai.android.issues.IssuesViewPagerAdapter;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 7/25/2017.
 */

public class UnitRaisingStrengthAndDetailsActivity extends DayCheckBaseActivity {

    private UnitRaisingStrengthFragment strengthFragment;
    private UnitRaisingDetailsFragment detailsFragment;
    @Bind(R.id.unitRaisingTabLayout)
    TabLayout unitRaisingTabLayout;
    @Bind(R.id.unitRaisingViewPager)
    ViewPager unitRaisingViewPager;
    private ColorStateList colorStateList;
    private RaisingDetailsMo raisingDetailsMo;

    @SuppressWarnings("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.unit_raising_fragment);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent != null) {
            raisingDetailsMo = (RaisingDetailsMo)
                    intent.getSerializableExtra(TableNameAndColumnStatement.RAISING_DETAILS);
        }

        setUpToolBar();
        setupViewPager();
        colorStateList = ContextCompat.getColorStateList(this, R.drawable.tab_label_indicator);
    }

    private void setUpToolBar() {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("RAISING");
    }

    public void setUpMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTableFromHashMap(attachmentMetaArrayList);
        attachmentMetaArrayList.clear();
    }

    private void setupViewPager() {
        IssuesViewPagerAdapter viewPagerAdapter = new IssuesViewPagerAdapter(getSupportFragmentManager());
        if (strengthFragment == null) {
            strengthFragment = UnitRaisingStrengthFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putSerializable(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo);
            strengthFragment.setArguments(bundle);
            viewPagerAdapter.addFragment(strengthFragment, getString(R.string.unitRaisingStrengthTab));
        }
        if (detailsFragment == null) {
            detailsFragment = UnitRaisingDetailsFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putSerializable(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo);
            detailsFragment.setArguments(bundle);
            viewPagerAdapter.addFragment(detailsFragment, getString(R.string.unitRaisingDetailsTab));
        }
        unitRaisingViewPager.setAdapter(viewPagerAdapter);
        unitRaisingTabLayout.setupWithViewPager(unitRaisingViewPager);
        unitRaisingTabLayout.setTabTextColors(colorStateList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unit_raising, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        } else if (id == R.id.submit) {
            if (null != unitRaisingViewPager) {
                if (strengthFragment != null) {
                    // 1. VALIDATING ALL EDIT TEXTs OF STRENGTH FRAGMENT.
                    // 2. ONCE ALL E.T. VALIDATES SUCCESSFULLY, VALIDATING FIELDS OF DETAIL FRAGMENTS
                    if (strengthFragment.validateActualStrengthFields() != null) {
                        if (strengthFragment.validateActualStrengthFields().validationStatus) {
                            if (detailsFragment.validateDetailsFragmentFields() != null) {
                                if (detailsFragment.validateDetailsFragmentFields().validationStatus) {
                                    IOPSApplication.getInstance().getUnitRaisingInsertionStatementDB()
                                            .insertUnitRaisingDetails(detailsFragment.validateDetailsFragmentFields());

                                    IOPSApplication.getInstance().getUnitRaisingInsertionStatementDB()
                                            .insertUnitRaisingStrengthDetails(strengthFragment.validateActualStrengthFields().updatedActualStrength);
                                    UnitRaisingInsertionStatementDB unitRaisingInsertionStatementDB = IOPSApplication.getInstance().getUnitRaisingInsertionStatementDB();
                                    IOPSApplication.getInstance().getUnitRaisingInsertionStatementDB().getReadableDatabase();

                                    detailsFragment.setupMetaData(unitRaisingInsertionStatementDB.getUnitRaisingLocalId(unitRaisingInsertionStatementDB.getReadableDatabase()));
                                    IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().updateUnitStatus(raisingDetailsMo.unitId, 1);
                                    triggerSyncAdapter();
                                    finish();

                                } else {
                                    unitRaisingViewPager.setCurrentItem(1);
                                }
                            }

                        } else {
                            unitRaisingViewPager.setCurrentItem(0);
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }
}