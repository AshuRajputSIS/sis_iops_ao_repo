package com.sisindia.ai.android.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashu Rajput on 6/19/2018.
 */

public class CCRQuestionRequestMO {

    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;

    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;

    @SerializedName("Data")
    @Expose
    public List<QuestionsData> clientCoordinationQuestions = new ArrayList<>();

}
