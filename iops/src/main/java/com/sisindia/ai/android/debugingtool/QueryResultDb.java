package com.sisindia.ai.android.debugingtool;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.BarrackStrength;

/**
 * Created by compass on 9/20/2017.
 */

public class QueryResultDb extends SISAITrackingDB {
    private SQLiteDatabase sqlite;
    private boolean isInsertedSuccessfully;

    public QueryResultDb(Context context) {
        super(context);
    }

    public boolean insertQueryResult(int queryId, String queryResult) {
        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                sqlite.beginTransaction();
                insertQueryResultIntoDb(queryId, queryResult);
                sqlite.setTransactionSuccessful();
                isInsertedSuccessfully = true;

            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertQueryResultIntoDb(int queryId, String queryResult) {
        ContentValues values = new ContentValues();
        values.put(TableNameAndColumnStatement.QUERY_ID, queryId);
        values.put(TableNameAndColumnStatement.QUERY_RESULT, queryResult);
        if (queryResult.equalsIgnoreCase("NULL_ARRAY") || queryResult.equalsIgnoreCase("NULL_CURSOR")) {
            values.put(TableNameAndColumnStatement.QUERY_STATUS, 0);
        } else if (queryResult.contains("EXCEPTION")) {
            values.put(TableNameAndColumnStatement.QUERY_STATUS, -1);
        } else {
            values.put(TableNameAndColumnStatement.QUERY_STATUS, 1);
        }

        values.put(TableNameAndColumnStatement.IS_SYNCED, 0);
        sqlite.insertOrThrow(TableNameAndColumnStatement.QUERY_TABLE_NAME, null, values);

    }
}
