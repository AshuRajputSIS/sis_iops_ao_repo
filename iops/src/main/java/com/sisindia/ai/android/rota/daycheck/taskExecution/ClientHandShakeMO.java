package com.sisindia.ai.android.rota.daycheck.taskExecution;


import com.google.gson.annotations.SerializedName;

public class ClientHandShakeMO {

    @SerializedName("MeetClient")
    private String meetClient;
    @SerializedName("ClientName")
    private String clientName;
    @SerializedName("Designation")
    private String designation;
    @SerializedName("ContactNo")
    private String contactNo;
    @SerializedName("ClientSatisfied")
    private String clientSatisfied;
    @SerializedName("Reason")
    private String reason;

    /**
     * @return The meetClient
     */
    public String getMeetClient() {
        return meetClient;
    }

    /**
     * @param meetClient The MeetClient
     */
    public void setMeetClient(String meetClient) {
        this.meetClient = meetClient;
    }

    /**
     * @return The clientName
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * @param clientName The ClientName
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * @return The designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation The Designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return The contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo The ContactNo
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return The clientSatisfied
     */
    public String getClientSatisfied() {
        return clientSatisfied;
    }

    /**
     * @param clientSatisfied The ClientSatisfied
     */
    public void setClientSatisfied(String clientSatisfied) {
        this.clientSatisfied = clientSatisfied;
    }

    /**
     * @return The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason The Reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ClientHandShakeMO{" +
                "meetClient='" + meetClient + '\'' +
                ", clientName='" + clientName + '\'' +
                ", designation='" + designation + '\'' +
                ", contactNo='" + contactNo + '\'' +
                ", clientSatisfied='" + clientSatisfied + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
