package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 28/6/16.
 */

public class BillAcceptedMO {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Designation")
    @Expose
    private String designation;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     *
     * @param designation
     * The Designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }
}
