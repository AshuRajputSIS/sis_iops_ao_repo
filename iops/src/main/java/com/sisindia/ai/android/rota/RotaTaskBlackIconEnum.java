package com.sisindia.ai.android.rota;

import com.sisindia.ai.android.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 09-05-2016.
 */


public enum RotaTaskBlackIconEnum {

//    Duty_Off(R.drawable.others),
//    Duty_On(R.drawable.others),

    Day_Checking(R.drawable.day_check),
    Night_Checking(R.drawable.night_check),
    Barrack_Inspection(R.drawable.barrack),
    Client_Coordination(R.drawable.client_cord),
    Bill_Submission(R.drawable.bill_submission),
    Bill_Collection(R.drawable.collection),
    Onsite_Training(R.drawable.training),
    Raising(R.drawable.others),
    Recruitment(R.drawable.others),
    Kit_Replacement(R.drawable.kit),
    Others(R.drawable.others);


    private static Map map = new HashMap<>();

    static {
        for (RotaTaskBlackIconEnum taskTypeIcon : RotaTaskBlackIconEnum.values()) {
            map.put(taskTypeIcon.value, taskTypeIcon);
        }
    }

    private int value;

    RotaTaskBlackIconEnum(int value) {
        this.value = value;
    }

    public static RotaTaskTypeEnum valueOf(int taskType) {
        return (RotaTaskTypeEnum) map.get(taskType);
    }

    public int getValue() {
        return value;
    }
}
