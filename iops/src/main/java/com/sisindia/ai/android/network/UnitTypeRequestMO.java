package com.sisindia.ai.android.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.network.request.UnitTypeDataMO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashu Rajput on 9/27/2018.
 */

public class UnitTypeRequestMO {

    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;

    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;

    @SerializedName("Data")
    @Expose
    public List<UnitTypeDataMO> unitTypeDataMO = new ArrayList<>();

}
