package com.sisindia.ai.android.appuser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sisindia.ai.android.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saruchi on 03-10-2016.
 */

public class ImageSyncingRecylerAdapter extends RecyclerView.Adapter<ImageSyncingRecylerAdapter.ViewHolder> {

    //define source of MediaStore.Images.Media, internal or external storage
    private Context context;
    private LayoutInflater inflater;
    private List<String> data = new ArrayList<>();

    public ImageSyncingRecylerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setData(List<String> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.recycler_gallery_row, parent, false);
        return new ViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Glide.with(context).load(new File(data.get(position)))
                // .placeholder(R.drawable.iops)
                .into(holder.displayImage);
//        File imgFile = new  File(data.get(position));
//
//        if(imgFile.exists()){
//
//            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//            holder.displayImage.setImageBitmap(myBitmap);
//
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View row;
        ImageView displayImage;

        public ViewHolder(View itemView) {
            super(itemView);
            row = itemView;
            displayImage = (ImageView) row.findViewById(R.id.img_display);
        }
    }
}

