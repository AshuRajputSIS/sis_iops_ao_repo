package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 8/9/16.
 */

public class BarrackSyncMO {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Lat")
    @Expose
    private String lat;
    @SerializedName("Long")
    @Expose
    private String _long;

    @SerializedName("DeviceNo")
    @Expose
    private String DeviceNo;

    /*@SerializedName("IsNFCTouched")
    @Expose
    private String IsNFCTouched;*/

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * @param lat The Lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * @return The _long
     */
    public String getLong() {
        return _long;
    }

    /**
     * @param _long The Long
     */
    public void setLong(String _long) {
        this._long = _long;
    }


    public String getDeviceNo() {
        return DeviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        DeviceNo = deviceNo;
    }
}
