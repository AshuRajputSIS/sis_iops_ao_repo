package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import java.io.Serializable;

/**
 * Created by Shruti Garg.
 */
public class BillSubmissionCheckListItemMO  implements Serializable{

    private String CheckListItem;
    private int checkListItemId;
    private boolean isChecked;
    public int getCheckListItemId() {
        return checkListItemId;
    }

    public void setCheckListItemId(int checkListItemId) {
        this.checkListItemId = checkListItemId;
    }
    public String getCheckListItem() {
        return CheckListItem;
    }

    public void setCheckListItem(String checkListItem) {
        CheckListItem = checkListItem;
    }


    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
