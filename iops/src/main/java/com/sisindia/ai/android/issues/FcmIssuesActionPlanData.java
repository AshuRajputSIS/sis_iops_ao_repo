package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.network.request.IssueIMO;

/**
 * Created by Durga Prasad on 03-12-2016.
 */
public class FcmIssuesActionPlanData {

    @SerializedName("IssueModel")
    private IssueIMO issueIMO;
    @SerializedName("ActionPlanModel")
    private ActionPlanMO actionPlanMO;

    public ActionPlanMO getActionPlanMO() {
        return actionPlanMO;
    }

    public void setActionPlanMO(ActionPlanMO actionPlanMO) {
        this.actionPlanMO = actionPlanMO;
    }

    public IssueIMO getIssueIMO() {
        return issueIMO;
    }

    public void setIssueIMO(IssueIMO issueIMO) {
        this.issueIMO = issueIMO;
    }

    @Override
    public String toString() {
        return "FcmIssuesActionPlanData{" +
                "actionPlanMO=" + actionPlanMO +
                ", issueIMO=" + issueIMO +
                '}';
    }

}
