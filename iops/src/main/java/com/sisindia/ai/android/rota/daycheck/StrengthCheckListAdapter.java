package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sisindia.ai.android.R;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 18-06-2016.
 */
public class StrengthCheckListAdapter extends BaseAdapter {

    ArrayList<Object> strengthCheckMOArrayList;
    Context context;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ROW = 2;
    private LayoutInflater layoutInflater;

    public StrengthCheckListAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

    }



    public void setStrengthCheckList(ArrayList<Object> strengthCheckMOArrayList) {
        this.strengthCheckMOArrayList = strengthCheckMOArrayList;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == context.getResources().getInteger(R.integer.number_0))
            return TYPE_HEADER;
        else
            return TYPE_ROW;

    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        switch (getItemViewType(position)){
            case 0 : view = layoutInflater.inflate(R.layout.strength_check_header, parent, false);
                break;
            default: view = layoutInflater.inflate(R.layout.strength_check_row_item, parent, false);
        }

        return view;
    }
}
