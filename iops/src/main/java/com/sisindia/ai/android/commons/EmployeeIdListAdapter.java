package com.sisindia.ai.android.commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.network.response.UnitEmployee;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;


/**
 * Created by nischala on 11/5/16.
 */
public class EmployeeIdListAdapter extends ArrayAdapter<UnitEmployee> {
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((UnitEmployee) resultValue).getEmployeeId().toString();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (UnitEmployee people : tempItems) {
                    if (people.getEmployeeNo().toString().contains(constraint.toString().toUpperCase())) {
                        suggestions.add(people);
                    }

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<UnitEmployee> filterList = (ArrayList<UnitEmployee>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (UnitEmployee people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<UnitEmployee> arrayListEmployeeIds = new ArrayList<UnitEmployee>();
    private int resource, textViewResourceId;
    private List<UnitEmployee> items, tempItems, suggestions;

    public EmployeeIdListAdapter(Context context, int resource, int textViewResourceId, List<UnitEmployee> items) {
        super(context, resource, textViewResourceId, items);
        this.mContext = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<UnitEmployee>(items);
        suggestions = new ArrayList<UnitEmployee>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_row_view, parent, false);
        }
        else {
            UnitEmployee people = items.get(position);
            if (people != null) {
                TextView lblName = (TextView) convertView.findViewById(R.id.row_text_view);
                if (lblName != null) {
                    if (null != people.getEmployeeNo() || people.getEmployeeNo().length() != 0) {
                        lblName.setText(people.getEmployeeNo().toString());
                        Timber.e("THe name selected is::" + people.getEmployeeNo().toString());
                    } else {
                        lblName.setText("Anonymous");
                    }


                }

                //lblName.setText(people.getEmployeeId().toString());
            }
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

}