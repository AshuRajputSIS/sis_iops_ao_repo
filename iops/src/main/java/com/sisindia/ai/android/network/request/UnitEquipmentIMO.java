package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

public class UnitEquipmentIMO {

    @SerializedName("Id")
    private Integer id;
    @SerializedName("UnitId")
    private Integer unitId;
    @SerializedName("EquipmentId")
    private Integer equipmentId;
    @SerializedName("IsCustomerProvided")
    private Integer isCustomerProvided;
    @SerializedName("Status")
    private Integer status;
    @SerializedName("EquipmentUnique No")
    private String equipmentUniqueNo;
    @SerializedName("EquipmentManufacturer")
    private String equipmentManufacturer;
    @SerializedName("PostId")
    private Integer postId;
    @SerializedName("Quantity")
    private Integer quantity;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The equipmentId
     */
    public Integer getEquipmentId() {
        return equipmentId;
    }

    /**
     * @param equipmentId The EquipmentId
     */
    public void setEquipmentId(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    /**
     * @return The isCustomerProvided
     */
    public Integer getIsCustomerProvided() {
        return isCustomerProvided;
    }

    /**
     * @param isCustomerProvided The IsCustomerProvided
     */
    public void setIsCustomerProvided(Integer isCustomerProvided) {
        this.isCustomerProvided = isCustomerProvided;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The Status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The equipmentUniqueNo
     */
    public String getEquipmentUniqueNo() {
        return equipmentUniqueNo;
    }

    /**
     * @param equipmentUniqueNo The EquipmentUnique No
     */
    public void setEquipmentUniqueNo(String equipmentUniqueNo) {
        this.equipmentUniqueNo = equipmentUniqueNo;
    }

    /**
     * @return The equipmentManufacturer
     */
    public String getEquipmentManufacturer() {
        return equipmentManufacturer;
    }

    /**
     * @param equipmentManufacturer The EquipmentManufacturer
     */
    public void setEquipmentManufacturer(String equipmentManufacturer) {
        this.equipmentManufacturer = equipmentManufacturer;
    }

    /**
     * @return The postId
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * @param postId The PostId
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "UnitEquipmentIMO{" +
                "id=" + id +
                ", unitId=" + unitId +
                ", equipmentId=" + equipmentId +
                ", isCustomerProvided=" + isCustomerProvided +
                ", status=" + status +
                ", equipmentUniqueNo='" + equipmentUniqueNo + '\'' +
                ", equipmentManufacturer='" + equipmentManufacturer + '\'' +
                ", postId=" + postId +
                ", quantity=" + quantity +
                '}';
    }
}