package com.sisindia.ai.android.views;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.database.AppPreferences;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by Saruchi on 31-03-2016.
 */
public class MapView extends BaseFrame implements OnMapReadyCallback {

    @Bind(R.id.currentLatLon)
    TextView currentLatLontext;
    @Bind(R.id.refreshLocation)
    CustomFontButton refreshLocationBtn;
    private GoogleMap mMap;
    private Context mcontext;
    private double latitude;
    private double longitude;
    private boolean isrelocate = false;  // true= map not in current location & false=map loaded with curretn location
    AppPreferences appPreferences;

    public MapView(Context context) {
        super(context);
        init(context, null);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        Timber.d("init");
        mcontext = context;

        LayoutInflater.from(getContext()).inflate(R.layout.map_view, this);
        ButterKnife.bind(this);
        //MapsInitializer.initialize(getContext());

        FragmentManager fragmentManager = BaseActivity.getInstance().getSupportFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        //SupportMapFragment mapFragment = (SupportMapFragment) c getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else {
            Timber.e("ERORR dint load map");
        }
        appPreferences = new AppPreferences(context);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap = googleMap;


//        PolygonOptions rectOptions = new PolygonOptions();
//        for(int i=0;i<GpsTrackingService.geoPointsArray.size();i++){
//
//            latitude =GpsTrackingService.geoPointsArray.get(i).getLatitude();
//            longitude =GpsTrackingService.geoPointsArray.get(i).getLongitude();
//Timber.d("geoPoint : ", latitude+","+longitude);
//            rectOptions.add(new LatLng(latitude,longitude));
//        }


        // Get back the mutable Polygon
        //  Polygon polygon = mMap.addPolygon(rectOptions);
        googleMap.setMyLocationEnabled(true);
        if (getIsRelocate()) {
            setLocation(latitude, longitude);
        } else {
            RefreshLocatin();
        }

    }

    @OnClick(R.id.refreshLocation)
    public void onRefreshLocation() {
        RefreshLocatin();
    }

    private void RefreshLocatin() {
        latitude = GpsTrackingService.latitude;
        longitude = GpsTrackingService.longitude;
        currentLatLontext.setText(GpsTrackingService.latitude + "," + GpsTrackingService.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(GpsTrackingService.latitude,
                GpsTrackingService.longitude), 15.0f));
    }

    public void setLocation(double latitude, double longitude) {
        DecimalFormat f = new DecimalFormat("##.0000000");
        currentLatLontext.setText(f.format(latitude) + "," + f.format(longitude));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f));
    }

    public boolean getIsRelocate() {
        return isrelocate;
    }

    public void setisRelocate(boolean locate, double lat, double lon) {
        Timber.d("lat & lon ==  %s" + lat + "," + lon);
        isrelocate = locate;
        latitude = lat;
        longitude = lon;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatLongOnMap(String latlong){
        currentLatLontext.setText(latlong);
    }

}
