package com.sisindia.ai.android.myunits.models;

/**
 * Created by Shushrut on 12-05-2016.
 */

import com.google.gson.annotations.SerializedName;


public class EquipmentList {

    @SerializedName("EquipmentId")
    private Integer EquipmentId;
    @SerializedName("EquipmentName")
    private String EquipmentName;
    @SerializedName("IsNotFunctional")
    private boolean IsNotFunctional;
    @SerializedName("IsCusomterProvided")
    private int IsCusomterProvided;

    /**
     * @return The EquipmentId
     */
    public Integer getEquipmentId() {
        return EquipmentId;
    }

    /**
     * @param EquipmentId The EquipmentId
     */
    public void setEquipmentId(Integer EquipmentId) {
        this.EquipmentId = EquipmentId;
    }

    /**
     * @return The EquipmentName
     */
    public String getEquipmentName() {
        return EquipmentName;
    }

    /**
     * @param EquipmentName The EquipmentName
     */
    public void setEquipmentName(String EquipmentName) {
        this.EquipmentName = EquipmentName;
    }

    /**
     * @return The IsNotFunctional
     */
    public Boolean getIsNotFunctional() {
        return IsNotFunctional;
    }

    /**
     * @param IsNotFunctional The IsNotFunctional
     */
    public void setIsNotFunctional(Boolean IsNotFunctional) {
        this.IsNotFunctional = IsNotFunctional;
    }

    public int isCusomterProvided() {
        return IsCusomterProvided;
    }

    public void setIsCusomterProvided(int isCusomterProvided) {
        IsCusomterProvided = isCusomterProvided;
    }
}