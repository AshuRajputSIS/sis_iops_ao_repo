package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.annualkitreplacement.KitItemApiResponse;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestItem;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemUpdateStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Durga Prasad on 08-09-2016.
 */
public class KitRequestSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<KitItemRequestMO> kitItemRequestMOList;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private KitItemUpdateStatementDB kitItemUpdateStatementDB;
    private ArrayList<KitDeliveredItemsIR> kitDeliveredItemsIRList;
    private int taskId;
    private AppPreferences appPreferences;

    public KitRequestSyncing(Context mcontext, int taskId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
        kitItemUpdateStatementDB = IOPSApplication.getInstance().getKitItemUpdateStatementDB();
        this.kitItemRequestMOList = new ArrayList<>();
        this.taskId = taskId ;
        appPreferences = new AppPreferences(mcontext);
        kitItemsSyncing();
    }

    private void kitItemsSyncing() {
        kitItemRequestMOList = getKitItemRequestList();
        if(kitItemRequestMOList != null && kitItemRequestMOList.size() != 0) {
            updateKitItemSyncCount(kitItemRequestMOList.size());
            for (KitItemRequestMO kitItemRequestMO : kitItemRequestMOList) {
                kitItemRequestApiCall(kitItemRequestMO);
            }
        } else {
            Timber.d("LINK %s","###########################################################################");
            updateKitItemSyncCount(0);
            Timber.d("LINK %s","$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Timber.d("kitItemRequestSynciing -->nothing to sync ");
            dependentFailureSync();

        }

    }

    public void updateKitItemSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(taskId == dependentSyncsStatus.taskId){
                    dependentSyncsStatus.isKitRequestSynced = count;
                    updateKitItemSyncCount(dependentSyncsStatus);
                }
            }
        }
    }

    private void updateKitItemSyncCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(taskId,
                Constants.TASK_RELATED_SYNC, TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED,
                dependentSyncsStatus.isKitRequestSynced,dependentSyncsStatus.taskTypeId
        );
    }




    public ArrayList<KitItemRequestMO> getKitItemRequestList(){

        ArrayList<KitItemRequestMO> kitItemRequestMOList = new ArrayList<>();

        String kitItemQuery = "select * from " +
                TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE+ " where  " +
                TableNameAndColumnStatement.SOURCE_TASK_ID + " > " + 0 + " AND " +
                TableNameAndColumnStatement.SOURCE_TASK_ID + " = " + taskId + " AND ("+
                TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' or " +
                TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + " = '" + 0 + "')";
        Timber.d("SequencePostId selectedQuery  %s", kitItemQuery);
        Cursor cursor = null;
        Gson gson =IOPSApplication.getGsonInstance();
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(kitItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        KitItemRequestMO kitItemRequestMO = new KitItemRequestMO();
                        ArrayList<KitItemRequestItem> kitItemRequestList = gson.fromJson(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REQUESTED_ITEMS)), new TypeToken<ArrayList<KitItemRequestItem>>(){}.getType());
                        kitItemRequestMO.setKitItemRequestItems(kitItemRequestList);
                        kitItemRequestMO.setRequestedOn(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REQUESTED_ON)));
                        kitItemRequestMO.setRequestedStatus(null);
                        kitItemRequestMO.setGuardId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        kitItemRequestMO.setGuardCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        kitItemRequestMO.setSourceTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)));
                        kitItemRequestMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        kitItemRequestMOList.add(kitItemRequestMO);

                        Timber.d("kitItemJosnData %s", new Gson().toJson(kitItemRequestMO));

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", kitItemRequestMOList);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(cursor != null && sqLiteDatabase != null) {
                cursor.close();
                sqLiteDatabase.close();
            }
        }
        return kitItemRequestMOList;

    }




    private synchronized void kitItemRequestApiCall(final KitItemRequestMO kitItemRequestMO) {
        sisClient.getApi().syncKitItemRequest(kitItemRequestMO, new Callback<KitItemApiResponse>() {
                    @Override
                    public void success(KitItemApiResponse kitItemApiResponse, Response response) {
                        Timber.d("kitItemRequest response  %s", kitItemApiResponse);

                        switch (kitItemApiResponse.getStatusCode())
                        {
                            case HttpURLConnection.HTTP_OK:
                                if(kitItemApiResponse != null ){

                                    if(kitItemApiResponse.getData() != null && kitItemApiResponse.getData().getKitItemRequestedId() != null){
                                        kitItemUpdateStatementDB.updateKitItemRequestedId(kitItemRequestMO.getId(),kitItemApiResponse.getData().getKitItemRequestedId());
                                        updateSyncCountAndStatus();

                                    }
                                }


                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Util.printRetorfitError("kitItemSyncing", error);

                    }
                }

        );


    }


    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                dependentSyncs(dependentSyncsStatus);
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {
        if(dependentSyncsStatus.isMetaDataSynced  != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            metaDataSyncing.setTaskId(dependentSyncsStatus.taskId);
        }
        else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setTaskId(dependentSyncsStatus.taskId);
            }
        }
    }

    private void updateSyncCountAndStatus() {
        if(taskId != 0){
            int count = dependentSyncDb.getSyncCount(taskId,TableNameAndColumnStatement.TASK_ID,
                    TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED);
            if (count != 0) {
                count = count -1;
                updateKitItemSyncCount(count);
                if(count == 0){
                    MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
                    metaDataSyncing.setTaskId(taskId);
                }
            }

        }
    }

}
