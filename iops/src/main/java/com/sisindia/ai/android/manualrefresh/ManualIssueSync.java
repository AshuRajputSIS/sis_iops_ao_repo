package com.sisindia.ai.android.manualrefresh;

import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.ImprovementPlanTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.IssuesTableCountMO;
import com.sisindia.ai.android.issues.BaseIssuesSync;
import com.sisindia.ai.android.issues.IssuesSyncListener;
import com.sisindia.ai.android.issues.IssuesUpdateAsyncTask;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 3/5/2017.
 */

public class ManualIssueSync {

    public static void getIssuesApiCall(final IssuesSyncListener issuesSyncListener, final Context mContext, final boolean isDetroyed) {
        if (tablesToSync()) {
            showprogressbar(mContext);
            new SISClient(mContext).getApi().getIssues(new Callback<BaseIssuesSync>() {
                @Override
                public void success(BaseIssuesSync baseIssuesSync, Response response) {
                    Timber.d("BaseUnitSync response  %s", baseIssuesSync);
                    switch (baseIssuesSync.statusCode) {
                        case HttpURLConnection.HTTP_OK:
                            new IssuesUpdateAsyncTask(mContext, baseIssuesSync.issuesImprovementData, issuesSyncListener).execute();
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(mContext, mContext.getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                @Override
                public void failure(RetrofitError error) {
                    hideprogressbar(isDetroyed);
                    Crashlytics.logException(error);
                }
            });
        } else {
            Util.showToast(mContext, mContext.getString(R.string.please_sync_older_records));
        }
    }

    private static void showprogressbar(Context mContext) {
        Util.showProgressBar(mContext, R.string.loading_please_wait);
    }

    private static void hideprogressbar(boolean isDetroyed) {
        Util.dismissProgressBar(isDetroyed);
    }

    private synchronized static boolean tablesToSync() {
        IssuesTableCountMO issuesTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getIssueTableCount();
        ImprovementPlanTableCountMO improvementPlanTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getImprovementPlanTableCount();
        if (issuesTableCountMO.getSyncCount() != 0 ||
                improvementPlanTableCountMO.getSyncCount() != 0 ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * order in which tables will be deleted
     */
}
