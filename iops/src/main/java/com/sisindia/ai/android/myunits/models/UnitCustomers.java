package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitCustomers implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("CustomerName")
    private String CustomerName;
    @SerializedName("CustomerDescription")
    private String CustomerDescription;
    @SerializedName("IsActive")
    private boolean IsActive;
    @SerializedName("CustomerHoAddressId")
    private int CustomerHoAddressId;
    @SerializedName("CustomerPrimaryContactId")
    private int CustomerPrimaryContactId;
    @SerializedName("CustomerSecondaryContactId")
    private int CustomerSecondaryContactId;
    @SerializedName("IsSmsToBeSent")
    private boolean IsSmsToBeSent;
    @SerializedName("IsEmailToBeSent")
    private boolean IsEmailToBeSent;
    @SerializedName("CustomerHoAddress")
    private String CustomerHoAddress;
    @SerializedName("CustomerPrimaryContactName")
    private String CustomerPrimaryContactName;
    @SerializedName("CustomerSecondaryContactName")
    private String CustomerSecondaryContactName;
    @SerializedName("PrimaryContactEmail")
    private String PrimaryContactEmail;
    @SerializedName("PrimaryContactPhone")
    private String PrimaryContactPhone;
    @SerializedName("SecondaryContactEmail")
    private String SecondaryContactEmail;
    @SerializedName("SecondaryContactPhone")
    private String SecondaryContactPhone;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerDescription() {
        return CustomerDescription;
    }

    public void setCustomerDescription(String customerDescription) {
        CustomerDescription = customerDescription;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public int getCustomerHoAddressId() {
        return CustomerHoAddressId;
    }

    public void setCustomerHoAddressId(int customerHoAddressId) {
        CustomerHoAddressId = customerHoAddressId;
    }

    public int getCustomerPrimaryContactId() {
        return CustomerPrimaryContactId;
    }

    public void setCustomerPrimaryContactId(int customerPrimaryContactId) {
        CustomerPrimaryContactId = customerPrimaryContactId;
    }

    public int getCustomerSecondaryContactId() {
        return CustomerSecondaryContactId;
    }

    public void setCustomerSecondaryContactId(int customerSecondaryContactId) {
        CustomerSecondaryContactId = customerSecondaryContactId;
    }

    public boolean isSmsToBeSent() {
        return IsSmsToBeSent;
    }

    public void setSmsToBeSent(boolean smsToBeSent) {
        IsSmsToBeSent = smsToBeSent;
    }

    public boolean isEmailToBeSent() {
        return IsEmailToBeSent;
    }

    public void setEmailToBeSent(boolean emailToBeSent) {
        IsEmailToBeSent = emailToBeSent;
    }

    public String getCustomerHoAddress() {
        return CustomerHoAddress;
    }

    public void setCustomerHoAddress(String customerHoAddress) {
        CustomerHoAddress = customerHoAddress;
    }

    public String getCustomerPrimaryContactName() {
        return CustomerPrimaryContactName;
    }

    public void setCustomerPrimaryContactName(String customerPrimaryContactName) {
        CustomerPrimaryContactName = customerPrimaryContactName;
    }

    public String getCustomerSecondaryContactName() {
        return CustomerSecondaryContactName;
    }

    public void setCustomerSecondaryContactName(String customerSecondaryContactName) {
        CustomerSecondaryContactName = customerSecondaryContactName;
    }

    public String getPrimaryContactEmail() {
        return PrimaryContactEmail;
    }

    public void setPrimaryContactEmail(String primaryContactEmail) {
        PrimaryContactEmail = primaryContactEmail;
    }

    public String getPrimaryContactPhone() {
        return PrimaryContactPhone;
    }

    public void setPrimaryContactPhone(String primaryContactPhone) {
        PrimaryContactPhone = primaryContactPhone;
    }

    public String getSecondaryContactEmail() {
        return SecondaryContactEmail;
    }

    public void setSecondaryContactEmail(String secondaryContactEmail) {
        SecondaryContactEmail = secondaryContactEmail;
    }

    public String getSecondaryContactPhone() {
        return SecondaryContactPhone;
    }

    public void setSecondaryContactPhone(String secondaryContactPhone) {
        SecondaryContactPhone = secondaryContactPhone;
    }
}
