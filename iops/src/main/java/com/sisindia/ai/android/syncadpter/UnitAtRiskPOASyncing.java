package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitAtRiskIR;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskSyncOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by shankar on 19/7/16.
 */

public class UnitAtRiskPOASyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<ActionPlanMO> actionPlanMOList;

    public UnitAtRiskPOASyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        uarPoaToSyncing();
    }

    private void uarPoaToSyncing() {
        actionPlanMOList = IOPSApplication.getInstance().getUARSelectionInstance().
                actionPlanToSync(1,0);
        if (actionPlanMOList != null && actionPlanMOList.size() != 0) {

            for (ActionPlanMO actionPlanMO : actionPlanMOList) {
                UnitAtRiskIR unitAtRiskIR = new UnitAtRiskIR();
                unitAtRiskIR.setCompletedDate(actionPlanMO.getCompletedDate());
                unitAtRiskIR.setId(actionPlanMO.getActionPlanId());
                if(actionPlanMO.getIsCompleted()){
                    unitAtRiskIR.setIsCompleted(1);
                }else{
                    unitAtRiskIR.setIsCompleted(0);
                }
               unitAtRiskIR.setRemarks(actionPlanMO.getRemarks());

                unitAtRiskSyncAPI(unitAtRiskIR,actionPlanMO.getUnitRiskId(),actionPlanMO.getActionPlanId(),actionPlanMO.getId());

                //Timber.d("unitContactSyncing   %s", "key: " + key + " value: " + contactInputRequestList.get(key));
            }

        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"unitAtRiskSync -->nothing to sync ");


        }

    }

    private void unitAtRiskSyncAPI(UnitAtRiskIR unitAtRiskIR, final int unitRiskId, final int actionPlanId, final int id) {



        sisClient.getApi().unitAtRiskPOASync(unitAtRiskIR, new Callback<UnitRiskSyncOR>() {
            @Override
            public void success(UnitRiskSyncOR  unitRiskSyncResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitAtRiskSync response  %s", unitRiskSyncResponse.getStatusCode());
                switch (unitRiskSyncResponse.getStatusCode())
                {
                    case HttpURLConnection.HTTP_OK:
                        //unitRiskSyncResponse.getData().getActionPlanId()
                        IOPSApplication.getInstance().getUARUpdateInstance().
                                updateUnitRiskPOA(unitRiskId,actionPlanId,id);

                        Timber.d(Constants.TAG_SYNCADAPTER+"unitAtRiskSync count   %s", "count: ");

                        break;
                    default:

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitAtRiskSync count   %s", "count: ");
                Util.printRetorfitError("unitAtRiskSync", error);
            }
        });

    }
}
