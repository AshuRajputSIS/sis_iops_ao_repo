package com.sisindia.ai.android.fcmnotification.fcmunits;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import java.util.ArrayList;

/**
 * Created by Saruchi on 05-12-2016.
 */

public class FCMUnitDeletion extends SISAITrackingDB {
    private boolean isDeletedSuccessfully;
    private SQLiteDatabase sqlite;
    private ArrayList<String> unitTableList;

    public FCMUnitDeletion(Context context) {
        super(context);
        unitTableList();
    }

    public boolean deleteUnitRecord(int unitId) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if (unitId != 0) {
                    sqlite.beginTransaction();
                    for (String tableName : unitTableList) {
                        IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                .deleteRecordById(unitId, TableNameAndColumnStatement.UNIT_ID, tableName, sqlite);
                    }
                    sqlite.setTransactionSuccessful();
                    isDeletedSuccessfully = true;
                } else {
                    isDeletedSuccessfully = false;
                }
            } catch (Exception exception) {
                //sqlite.endTransaction();
                Crashlytics.logException(exception);
                isDeletedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
            return isDeletedSuccessfully;
        }
    }

    public boolean deleteUnitContactRecord(int unitId) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                if (unitId != 0) {
                    sqlite.beginTransaction();

                    IOPSApplication.getInstance().getDeletionStatementDBInstance()
                            .deleteRecordById(unitId, TableNameAndColumnStatement.UNIT_CONTACT_ID,
                                    TableNameAndColumnStatement.UNIT_CONTACT_TABLE, sqlite);

                    sqlite.setTransactionSuccessful();


                    isDeletedSuccessfully = true;
                } else {
                    isDeletedSuccessfully = false;
                }
            } catch (Exception exception) {
                //sqlite.endTransaction();
                Crashlytics.logException(exception);
                isDeletedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
            return isDeletedSuccessfully;
        }
    }

    /**
     * order in which the records wll be deleted
     */
    private void unitTableList() {
        unitTableList = new ArrayList<>();
        unitTableList.add(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_POST_TABLE);
        //  unitTableList.add(TableNameAndColumnStatement.CUSTOMER_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_CONTACT_TABLE);
        //  unitTableList.add(TableNameAndColumnStatement.MESS_VENDOR_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_BARRACK_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_TABLE);

    }


}

