package com.sisindia.ai.android.annualkitreplacement;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KitDeliveredFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KitDeliveredFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<Object> mDataValues = new ArrayList<>();
    CustomKitReplacementAdapter customIssuesAdapter;
    @Bind(R.id.unitName)
    CustomFontTextview unitNameLabel;
    @Bind(R.id.kit_assigned_recyclerview)
    RecyclerView kitAssignedRecyclerview;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private ArrayList<AnnualOrAdhocKitItemMO> kitDeliveredList;
    private int unitId;
    private int kitTypeId;
    private String unitName;

    public KitDeliveredFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KitDeliveredFragment newInstance(String param1, String param2) {

        KitDeliveredFragment kitDeliveredFragment = new KitDeliveredFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        kitDeliveredFragment.setArguments(args);

        return kitDeliveredFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.kit_assign_layout, container, false);
        ButterKnife.bind(this, view);
        initComplaintsViews();
        unitNameLabel.setText(unitName);

        return view;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public void setKitTypeId(int kitTypeId) {
        this.kitTypeId = kitTypeId;
    }

    private void initComplaintsViews() {
        kitAssignedRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));

        kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
        kitDeliveredList = kitItemSelectionStatementDB.getAssignedOrDeliveredKitItems(unitId, kitTypeId, 3);

        customIssuesAdapter = new CustomKitReplacementAdapter(getActivity());
        customIssuesAdapter.enableClick(false);
        customIssuesAdapter.setKitDetails(kitDeliveredList);
        kitAssignedRecyclerview.setAdapter(customIssuesAdapter);
    }




    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}