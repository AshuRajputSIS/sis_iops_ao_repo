package com.sisindia.ai.android.help;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.VideoView;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoViewActivity extends BaseActivity implements SurfaceHolder.Callback{

    @Bind(R.id.videoview) VideoView videoview;

    private MediaPlayer mediaPlayer;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private String videoLink="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_view);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            videoLink = getIntent().getStringExtra(Constants.VIDEO_LINK);
        }
        if(!TextUtils.isEmpty(videoLink.trim())){
            try {
                //Displays a video file.
                VideoView mVideoView = (VideoView)findViewById(R.id.videoview);
                Uri uri = Uri.parse(videoLink);
                mVideoView.setVideoURI(uri);
                mVideoView.requestFocus();
                mVideoView.start();
            }catch (Exception e){
                Crashlytics.logException(e);
            }

        }else {
            Crashlytics.log("videoLink is empty");
        }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
        int height) {
            // TODO Auto-generated method stub

        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }


    }
