package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddMetadataResponse {

    @SerializedName("StatusCode")
    @Expose
    private Integer StatusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String StatusMessage;
    @SerializedName("Data")
    @Expose
    private AttachmentData Data;

    /**
     * @return The StatusCode
     */
    public Integer getStatusCode() {
        return StatusCode;
    }

    /**
     * @param StatusCode The StatusCode
     */
    public void setStatusCode(Integer StatusCode) {
        this.StatusCode = StatusCode;
    }

    /**
     * @return The StatusMessage
     */
    public String getStatusMessage() {
        return StatusMessage;
    }

    /**
     * @param StatusMessage The StatusMessage
     */
    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    /**
     * @return The Data
     */
    public AttachmentData getData() {
        return Data;
    }

    /**
     * @param Data The Data
     */
    public void setData(AttachmentData Data) {
        this.Data = Data;
    }

    @Override
    public String toString() {
        return "AddMetadataResponse{" +
                "StatusCode=" + StatusCode +
                ", StatusMessage='" + StatusMessage + '\'' +
                ", Data=" + Data +
                '}';
    }
}