package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 22/6/16.
 */

public class RotaMO implements Serializable {

    /*@SerializedName("RotaModel")
    @Expose
    private RotaModelMO rotaModelMO;*/

    @SerializedName("RotaModel")
    @Expose
    private List<RotaModelMO> rotaModelMO;

    @SerializedName("RotaTasks")
    @Expose
    private List<RotaTask> rotaTasks = new ArrayList<>();

    @SerializedName("UnitTasks")
    @Expose
    private List<UnitTask> unitTasks = new ArrayList<>();

    @SerializedName("UnitMonthlyBills")
    @Expose
    private List<UnitMonthlyBillDetail> unitMonthlyBillDetail = new ArrayList<>();

    /**
     * @return The unitMonthlyBillDetail
     */
    public List<UnitMonthlyBillDetail> getUnitMonthlyBillDetail() {
        return unitMonthlyBillDetail;
    }

    /**
     * @param unitMonthlyBillDetail The UnitMonthlyBillDetail
     */
    public void setUnitMonthlyBillDetail(List<UnitMonthlyBillDetail> unitMonthlyBillDetail) {
        this.unitMonthlyBillDetail = unitMonthlyBillDetail;
    }

/*    public RotaModelMO getRotaModelMO() {
        return rotaModelMO;
    }

    public void setRotaModelMO(RotaModelMO rotaModelMO) {
        this.rotaModelMO = rotaModelMO;
    }*/

    public List<RotaModelMO> getRotaModelMO() {
        return rotaModelMO;
    }

    public void setRotaModelMO(List<RotaModelMO> rotaModelMO) {
        this.rotaModelMO = rotaModelMO;
    }

    /**
     * @return The rotaTasks
     */
    public List<RotaTask> getRotaTasks() {
        return rotaTasks;
    }

    /**
     * @param rotaTasks The RotaTasks
     */
    public void setRotaTasks(List<RotaTask> rotaTasks) {
        this.rotaTasks = rotaTasks;
    }

    /**
     * @return The unitTasks
     */
    public List<UnitTask> getUnitTasks() {
        return unitTasks;
    }

    /**
     * @param unitTasks The UnitTasks
     */
    public void setUnitTasks(List<UnitTask> unitTasks) {
        this.unitTasks = unitTasks;
    }

}
