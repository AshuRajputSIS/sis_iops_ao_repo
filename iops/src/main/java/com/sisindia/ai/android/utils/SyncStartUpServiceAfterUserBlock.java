package com.sisindia.ai.android.utils;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.DeletionStatementDB;


/**
 * Created by compass on 9/5/2017.
 */

public class SyncStartUpServiceAfterUserBlock extends IntentService {
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private static final String TAG = SyncStartUpServiceAfterUserBlock.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SyncStartUpServiceAfterUserBlock() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        appPreferences = new AppPreferences(SyncStartUpServiceAfterUserBlock.this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        appPreferences.clearSharedPreferences();
        new DeletionStatementDB(this).deleteTableData();
    }
}
