package com.sisindia.ai.android.fcmnotification.fcmAKR;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.annualkitreplacement.FCMAkrDistributionData;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.KitDistributionItemOR;

import java.util.List;

/**
 * Created by Durga Prasad on 01-12-2016.
 */

public class FcmAKRModelInsertion extends SISAITrackingDB {
    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite = null;

    public FcmAKRModelInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertAkrDistributionModel(List<FCMAkrDistributionData> fcmAkrDistributionDataList) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if (fcmAkrDistributionDataList != null && fcmAkrDistributionDataList.size() != 0) {
                    sqlite.beginTransaction();
                    for (FCMAkrDistributionData fcmAkrDistributionData : fcmAkrDistributionDataList) {
                        if (fcmAkrDistributionData != null) {
                            insertKitDistributon(fcmAkrDistributionData);
                            insertKitDistributionItems(fcmAkrDistributionData);
                        }
                    }
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                } else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertKitDistributionItems(FCMAkrDistributionData fcmAkrDistributionData) {
        insertKitDistributonItemTable(fcmAkrDistributionData.getKitDistributionItem());
    }

    public void insertKitDistributon(FCMAkrDistributionData kitDistributionOR) {
        try {
            deleteKitDistributionRow(TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, kitDistributionOR.getId(), sqlite);
            if (kitDistributionOR != null) {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionOR.getId());
                values.put(TableNameAndColumnStatement.ISSUE_OFFICER_ID, kitDistributionOR.getIssuingOfficerId());
                values.put(TableNameAndColumnStatement.UNIT_ID, kitDistributionOR.getUnitId());
                values.put(TableNameAndColumnStatement.DISTRIBUTION_STATUS, kitDistributionOR.getDistributionStatus());
                values.put(TableNameAndColumnStatement.RECIPIENT_ID, kitDistributionOR.getRecipientId());
                values.put(TableNameAndColumnStatement.KIT_TYPE_ID, kitDistributionOR.getKitTypeId());
                values.put(TableNameAndColumnStatement.BRANCH_ID, kitDistributionOR.getBranchId());
                values.put(TableNameAndColumnStatement.GUARD_CODE, kitDistributionOR.getGuardNo());
                values.put(TableNameAndColumnStatement.GUARD_NAME, kitDistributionOR.getGuardName());
                values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, kitDistributionOR.getCreatedDateTime());
                values.put(TableNameAndColumnStatement.DISTRIBUTION_DATE, kitDistributionOR.getDistributionDate());
                sqlite.insertOrThrow(TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, null, values);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void insertKitDistributonItemTable(List<KitDistributionItemOR> kitDistributionORList) {

        try {
            // Inserting Row
            if (kitDistributionORList != null && kitDistributionORList.size() > 0) {
                for (KitDistributionItemOR kitDistributionItemOR : kitDistributionORList) {

                    if (kitDistributionItemOR != null) {
                        deleteKitDistributionItemRow(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE, kitDistributionItemOR.getId(), sqlite);
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionItemOR.getKitDistributionId());
                        values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID, kitDistributionItemOR.getId());
                        values.put(TableNameAndColumnStatement.IS_ISSUED, kitDistributionItemOR.getIsIssued());
                        values.put(TableNameAndColumnStatement.IS_UNPAID, kitDistributionItemOR.getIsUnPaid());
                        if (kitDistributionItemOR.getNonIssueReasonId() == null) {
                            values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID, 0);
                        } else {
                            values.put(TableNameAndColumnStatement.NON_ISSUE_REASON_ID,
                                    kitDistributionItemOR.getNonIssueReasonId());
                        }
                        values.put(TableNameAndColumnStatement.ISSUED_DATE, kitDistributionItemOR.getIssuedDate());
                        values.put(TableNameAndColumnStatement.KIT_ITEM_ID, kitDistributionItemOR.getKitItemId());
                        values.put(TableNameAndColumnStatement.KIT_SIZE_ID, kitDistributionItemOR.getKitApparelSizeId());
                        values.put(TableNameAndColumnStatement.QUANTITY, kitDistributionItemOR.getQuantity());
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                        sqlite.insertOrThrow(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE, null, values);
                    }

                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void deleteKitDistributionTables(String tableName, SQLiteDatabase sqlite) {


        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

    }

    public void deleteKitDistributionRow(String tableName, int kitDistributionId, SQLiteDatabase sqlite) {


        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " = " + kitDistributionId, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

    }

    public void deleteKitDistributionItemRow(String tableName, int kitDistributionItemId, SQLiteDatabase sqlite) {


        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID + " = " + kitDistributionItemId, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

    }
}
