package com.sisindia.ai.android.unitatriskpoa;

import android.view.View;

import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;


/**
 * Created by shankar on 18/7/16.
 */

public interface PendingCheckListItemListener {

    void onItemClick(ActionPlanMO actionPlanMO);
}
