package com.sisindia.ai.android.database.unitatriskpoaDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.DataMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskCountMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskPendingCountMO;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 14/7/16.
 */

public class UARSelectionStatementDB extends SISAITrackingDB {

    public UARSelectionStatementDB(Context context) {
        super(context);
    }

    public DataMO getUnitAtRiskPOA() {
        DataMO unitRiskDataMO = new DataMO();
        List<RiskDetailModelMO> riskDetailModelMOList = new ArrayList<>();
        UnitRiskCountMO unitRiskCountMO = new UnitRiskCountMO();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_RISK_TABLE;

        Cursor cursor = null;

        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {


                        RiskDetailModelMO riskDetailModelMO = new RiskDetailModelMO();

                        riskDetailModelMO.setId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ID)));
                        riskDetailModelMO.setUnitRiskId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ID)));
                        riskDetailModelMO.setUnitId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        riskDetailModelMO.setUnitName(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        riskDetailModelMO.setCurrentMonth(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CURRENT_MONTH)));

                        riskDetailModelMO.setCurrentYear(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CURRENT_YEAR)));
                        riskDetailModelMO.setUnitRiskStatusId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_STATUS_ID)));

                        riskDetailModelMO.setTotalItems(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.TOTAL_ITEM_COUNT)));
                        riskDetailModelMO.setCompletionDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CLOSED_AT)));

                        riskDetailModelMOList.add(riskDetailModelMO);
                        unitRiskDataMO.setRiskDetailModels(riskDetailModelMOList);
                        unitRiskCountMO.setRiskCount(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.RISK_COUNT)));
                        unitRiskCountMO.setRiskCountByAreaInspector(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.RISK_COUNT_BY_AI)));

                        unitRiskCountMO.setActionCount(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTION_COUNT)));
                        unitRiskCountMO.setActionPendingCount(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTION_PENDING_COUNT)));
                        unitRiskDataMO.setUnitRiskCount(unitRiskCountMO);


                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return unitRiskDataMO;
    }


    public List<ActionPlanMO> getUARPendingList(int unitRiskID, int isPending) {

        List<ActionPlanMO> actionPlanMOList = new ArrayList<>();


        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE + " where " +
                TableNameAndColumnStatement.IS_COMPLETED + "='" + isPending + "' and " +
                TableNameAndColumnStatement.UNIT_RISK_ID + "='" + unitRiskID + "'";

        System.out.println("UAR List :" +selectQuery);
        Cursor cursor = null;

        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        ActionPlanMO actionPlanMO = new ActionPlanMO();
                        actionPlanMO.setId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ID)));
                        actionPlanMO.setActionPlanId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID)));
                        actionPlanMO.setUnitRiskId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ID)));
                        actionPlanMO.setReason(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_REASON)));
                        actionPlanMO.setActionPlan(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN)));
                        actionPlanMO.setPlanDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_DATE)));
                        actionPlanMO.setAssignTo(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_ID)));
                        actionPlanMO.setAssignToName(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO)));
                        actionPlanMO.setCompletedDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.COMPLETION_DATE)));
                        int iscompleted = cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.IS_COMPLETED));
                        if (iscompleted == 0) {
                            actionPlanMO.setIsCompleted(false);
                        } else {
                            actionPlanMO.setIsCompleted(true);
                        }
                        actionPlanMO.setRemarks(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        actionPlanMO.setClosedDate(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CLOSED_DATE)));
                        actionPlanMOList.add(actionPlanMO);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return actionPlanMOList;
    }
    public List<ActionPlanMO> getUARCompletedList(int unitRiskID, int isCompleted) {

        List<ActionPlanMO> actionPlanMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE + " where " +
                TableNameAndColumnStatement.IS_COMPLETED + "='" + isCompleted + "' and " +
                TableNameAndColumnStatement.UNIT_RISK_ID + "='" + unitRiskID + "'";
        System.out.println("UAR List :" +selectQuery);
        Cursor cursor = null;

        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        ActionPlanMO actionPlanMO = new ActionPlanMO();
                        actionPlanMO.setId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ID)));
                        actionPlanMO.setActionPlanId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID)));
                        actionPlanMO.setUnitRiskId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ID)));
                        actionPlanMO.setReason(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_REASON)));
                        actionPlanMO.setActionPlan(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN)));
                        actionPlanMO.setPlanDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_DATE)));
                        actionPlanMO.setAssignTo(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_ID)));
                        actionPlanMO.setAssignToName(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO)));
                        actionPlanMO.setCompletedDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.COMPLETION_DATE)));

                        int iscompleted = cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.IS_COMPLETED));
                        if (iscompleted == 0) {
                            actionPlanMO.setIsCompleted(false);
                        } else {
                            actionPlanMO.setIsCompleted(true);
                        }
                        actionPlanMO.setRemarks(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        actionPlanMO.setClosedDate(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CLOSED_DATE)));
                        actionPlanMOList.add(actionPlanMO);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return actionPlanMOList;
    }



    public List<ActionPlanMO> actionPlanToSync(int isCompleted ,int isSynced){

        List<ActionPlanMO> actionPlanMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE + " where " +
                TableNameAndColumnStatement.IS_COMPLETED + "='" + isCompleted + "' and " +
                TableNameAndColumnStatement.IS_SYNCED + "='" + isSynced + "'";
        System.out.println("UAR sync List :" +selectQuery);
        Cursor cursor = null;

        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ActionPlanMO actionPlanMO = new ActionPlanMO();
                        actionPlanMO.setId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ID)));
                        actionPlanMO.setActionPlanId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID)));
                        actionPlanMO.setUnitRiskId(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ID)));
                        actionPlanMO.setReason(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_REASON)));
                        actionPlanMO.setActionPlan(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN)));
                        actionPlanMO.setPlanDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_DATE)));
                        actionPlanMO.setAssignTo(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_ID)));
                        actionPlanMO.setAssignToName(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO)));
                        actionPlanMO.setCompletedDate(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.COMPLETION_DATE)));
                        int iscompleted = cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.IS_COMPLETED));
                        if (iscompleted == 0) {
                            actionPlanMO.setIsCompleted(false);
                        } else {
                            actionPlanMO.setIsCompleted(true);
                        }
                        actionPlanMO.setRemarks(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        actionPlanMO.setClosedDate(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.CLOSED_DATE)));
                        actionPlanMOList.add(actionPlanMO);
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return actionPlanMOList;
    }

    public synchronized UnitRiskPendingCountMO getUARPendingCount(){

        UnitRiskPendingCountMO unitRiskPendingCountMO = new UnitRiskPendingCountMO();

        unitRiskPendingCountMO.setTotalCount(getTotalPendingCount());
        String selectQuery ="select count(id) as CompletedCount from "+ TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE
                +" where "+ TableNameAndColumnStatement.IS_COMPLETED+"='0'";
        Cursor cursor = null;
        SQLiteDatabase sqLiteDatabase = null;
        try {
            sqLiteDatabase = this.getReadableDatabase();
            cursor = sqLiteDatabase.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        unitRiskPendingCountMO.setCompletedCount(cursor.getInt(cursor.
                                getColumnIndex("CompletedCount")));
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

        return unitRiskPendingCountMO;

    }

    private synchronized int getTotalPendingCount() {

        String selectQuery ="select count(id) as TotalCount from "
                + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE;

        Cursor totalCountCursor = null;
        int totalCount=0;
        SQLiteDatabase sqLiteDatabase = null;
        try {

        sqLiteDatabase = this.getReadableDatabase();
            totalCountCursor = sqLiteDatabase.rawQuery(selectQuery, null);
        if (totalCountCursor != null && !totalCountCursor.isClosed()) {
            // looping through all rows and adding to list
            if (totalCountCursor.moveToFirst()) {
                do {
                     totalCount = totalCountCursor.getInt(totalCountCursor.
                            getColumnIndex("TotalCount"));
                } while (totalCountCursor.moveToNext());
            }
        }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqLiteDatabase && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return totalCount;
    }
}
