package com.sisindia.ai.android.unitatriskpoa;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.utils.Util;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 15/7/16.
 */

public class UARActionPlanActivity extends BaseActivity
       {
    @Bind(R.id.uar_action_plan_tabLayout)
    TabLayout uarActionPlanTab;
    @Bind(R.id.uar_action_plan_viewpager)
    ViewPager uarViewPager;
    @Bind(R.id.toolbar)
    Toolbar appToolBar;


    int currentTab = 0;
    private ColorStateList colorStateList;
    private int unitRiskId;
    private String unitName;
    private UARActionPlanFrgamentAdapter uarActionPlanAdapter;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uar_action_plan);
        setUpToolBar();
        initViews();


    }

    private void setUpToolBar() {
        ButterKnife.bind(this);
        setSupportActionBar(appToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.unit_at_risk);
    }

    private void initViews() {
         extras = getIntent().getExtras();
        if (null!=extras) {
            unitRiskId = extras.getInt("UnitRiskId");
            unitName = extras.getString("UnitName");

        }
        colorStateList = ContextCompat.getColorStateList(this, R.drawable.tab_label_indicator);
        setupViewPager(uarViewPager);
        uarActionPlanTab.setupWithViewPager(uarViewPager);
        uarActionPlanTab.setTabTextColors(colorStateList);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupViewPager(ViewPager viewPager) {
       viewPager.setClickable(true);
        uarActionPlanAdapter = new UARActionPlanFrgamentAdapter(getSupportFragmentManager());
        uarActionPlanAdapter.addFragment(new UARPendingFragment(unitRiskId,unitName).newInstance("", "", unitRiskId, currentTab,unitName), TableNameAndColumnStatement.Pending);
        uarActionPlanAdapter.addFragment(new UARCompletedFrgament(unitRiskId,unitName).newInstance("", "", unitRiskId, currentTab,unitName), TableNameAndColumnStatement.COMPLETED);
        viewPager.setAdapter(uarActionPlanAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                Util.currentTab = position;
                uarActionPlanAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

/* */
       }