package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 13-07-2016.
 */

public class UnitBillingStatusMO {
    private int taskStatusId;
    private int taskTypeId;
    private String estimatedTaskExecutionStartTime;
    private String estimatedTaskExecutionEndTime;
    private String actualTaskExecutionStartTime;
    private String actualTaskExecutionEndTime;



    private String billCollectionDate;
    private String wageDate;
    public int getTaskStatusId() {
        return taskStatusId;
    }

    public void setTaskStatusId(int taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    public int getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public String getEstimatedTaskExecutionStartTime() {
        return estimatedTaskExecutionStartTime;
    }

    public void setEstimatedTaskExecutionStartTime(String estimatedTaskExecutionStartTime) {
        this.estimatedTaskExecutionStartTime = estimatedTaskExecutionStartTime;
    }

    public String getEstimatedTaskExecutionEndTime() {
        return estimatedTaskExecutionEndTime;
    }

    public void setEstimatedTaskExecutionEndTime(String estimatedTaskExecutionEndTime) {
        this.estimatedTaskExecutionEndTime = estimatedTaskExecutionEndTime;
    }

    public String getActualTaskExecutionStartTime() {
        return actualTaskExecutionStartTime;
    }

    public void setActualTaskExecutionStartTime(String actualTaskExecutionStartTime) {
        this.actualTaskExecutionStartTime = actualTaskExecutionStartTime;
    }

    public String getActualTaskExecutionEndTime() {
        return actualTaskExecutionEndTime;
    }

    public void setActualTaskExecutionEndTime(String actualTaskExecutionEndTime) {
        this.actualTaskExecutionEndTime = actualTaskExecutionEndTime;
    }
    public String getBillCollectionDate() {
        return billCollectionDate;
    }

    public void setBillCollectionDate(String billCollectionDate) {
        this.billCollectionDate = billCollectionDate;
    }

    public String getWageDate() {
        return wageDate;
    }

    public void setWageDate(String wageDate) {
        this.wageDate = wageDate;
    }

    @Override
    public String toString() {
        return "UnitBillingStatusMO{" +
                "taskStatusId=" + taskStatusId +
                ", taskTypeId=" + taskTypeId +
                ", estimatedTaskExecutionStartTime='" + estimatedTaskExecutionStartTime + '\'' +
                ", estimatedTaskExecutionEndTime='" + estimatedTaskExecutionEndTime + '\'' +
                ", actualTaskExecutionStartTime='" + actualTaskExecutionStartTime + '\'' +
                ", actualTaskExecutionEndTime='" + actualTaskExecutionEndTime + '\'' +
                ", billCollectionDate='" + billCollectionDate + '\'' +
                ", wageDate='" + wageDate + '\'' +
                '}';
    }
}
