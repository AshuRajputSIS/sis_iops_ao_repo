package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 14-07-2016.
 */
public class SecurityRiskOR {
    @SerializedName("securityRiskObservationId")
    @Expose
    private Integer securityRiskObservationId;

    /**
     *
     * @return
     * The securityRiskObservationId
     */
    public Integer getSecurityRiskObservationId() {
        return securityRiskObservationId;
    }

    /**
     *
     * @param securityRiskObservationId
     * The securityRiskObservationId
     */
    public void setSecurityRiskObservationId(Integer securityRiskObservationId) {
        this.securityRiskObservationId = securityRiskObservationId;
    }

    @Override
    public String toString() {
        return "SecurityRiskOR{" +
                "securityRiskObservationId=" + securityRiskObservationId +
                '}';
    }
}
