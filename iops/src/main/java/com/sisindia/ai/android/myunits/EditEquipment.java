package com.sisindia.ai.android.myunits;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import butterknife.Bind;
import butterknife.ButterKnife;
/**
 * Created by Saruchi on 16-11-2016.
 */

public class EditEquipment extends BaseActivity {
    public static final String EDIT_EQUIPMENT_KEY = "edit_equipment";
    public static final String EDIT_EQUIPMENT_POS_KEY = "edit_pos_equipment";
    protected static final int EDIT_REQUEST_CODE=10;
    protected static final int EDIT_EQUIPMENT_CODE=1001;


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private ActionBar actionBar;
    private Resources mResources;
    private SyncingUnitEquipmentModel syncingUnitEquipmentModel;
    @Bind(R.id.addEquipmentTypeRelativeLayout)
    RelativeLayout addEquipmentTypeRelativeLayout;
    @Bind(R.id.add_equipmentType_value)
    CustomFontTextview equipmentNameTxtView;
    @Bind(R.id.addEquipmentCountRelativeLayout)
    RelativeLayout equipmentCountRelativeLayout;
    @Bind(R.id.add_equipment_count)
    CustomFontEditText equipmentCountEditText;
    @Bind(R.id.addEquipmentProvidedByRelativeLayout)
    RelativeLayout equipmentProvidedByRelativeLayout;
    @Bind(R.id.add_Equipment_provided_by)
    CustomFontTextview providedByTxtView;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_eqipment_activity);
        ButterKnife.bind(this);
        mResources = getResources();
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mResources.getString(R.string.edit_equipment));
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if(getIntent().hasExtra(EDIT_EQUIPMENT_KEY)) {
            syncingUnitEquipmentModel = getIntent().getExtras().getParcelable(EDIT_EQUIPMENT_KEY);
            position =getIntent().getExtras().getInt(EDIT_EQUIPMENT_POS_KEY);
        }
        setUpViews();
    }

    private void setUpViews(){
        if(null!=syncingUnitEquipmentModel){
            equipmentNameTxtView.setText(syncingUnitEquipmentModel.getAddEquipmentLocalMO()
                    .getEquipmentType());
            equipmentCountEditText.setText(String.valueOf(syncingUnitEquipmentModel.getAddEquipmentLocalMO()
                    .getEquipmentCount()));
            equipmentCountEditText.setSelection(equipmentCountEditText.getText().length());

            if(syncingUnitEquipmentModel.getIsCustomerProvided()==0){
                providedByTxtView.setText(mResources.getString(R.string.add_provided_by_sis));
            }
            else{
                providedByTxtView.setText(mResources.getString(R.string.client_str));
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_post_save: {
                Util.hideKeyboard(this);
                validate();
                break;
            }
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
private void validate(){
    int count =Integer.valueOf(equipmentCountEditText.getText().toString());
    if(!TextUtils.isEmpty(equipmentCountEditText.getText().toString()) &&
            count!=0 ){
        if(count==syncingUnitEquipmentModel.getAddEquipmentLocalMO().getEquipmentCount()){
            // same count . So no need to update
            finish();
        }
        else{
            syncingUnitEquipmentModel.getAddEquipmentLocalMO()
                    .setEquipmentCount(count);
            Intent editIntent = new Intent();
            editIntent.putExtra(EDIT_EQUIPMENT_POS_KEY, position);
            editIntent.putExtra(EDIT_EQUIPMENT_KEY, syncingUnitEquipmentModel);
            setResult(RESULT_OK, editIntent);
        }
    }
    else{
        snackBarWithMesg(addEquipmentTypeRelativeLayout,
                mResources.getString(R.string.error_enter_valid_count));
    }
}
}
