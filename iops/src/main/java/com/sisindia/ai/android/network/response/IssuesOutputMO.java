package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.network.response.IssuesData;

public class IssuesOutputMO {

    @SerializedName("StatusCode")
   
    private Integer statusCode;
    @SerializedName("StatusMessage")
   
    private String statusMessage;
    @SerializedName("Data")
   
    private IssuesData actionPlanData;

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public IssuesData getActionPlanData() {
        return actionPlanData;
    }

    public void setActionPlanData(IssuesData actionPlanData) {
        this.actionPlanData = actionPlanData;
    }

    @Override
    public String toString() {
        return "IssuesOutputMO{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", actionPlanData=" + actionPlanData +
                '}';
    }
}