package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 06-05-2016.
 */
public class UnitAddPostData {

    @SerializedName("postId")
    @Expose
    private Integer postId;

    /**
     * @return The postId
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * @param postId The postId
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "UnitAddPostData{" +
                "postId=" + postId +
                '}';
    }
}
