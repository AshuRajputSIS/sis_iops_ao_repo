package com.sisindia.ai.android.issues.complaints;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.AddComplaintsAdapter;
import com.sisindia.ai.android.issues.IssueTypeEnum;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.bottomsheet.BottomSheetAdapter;
import com.sisindia.ai.android.utils.bottomsheet.CustomBottomSheetFragment;
import com.sisindia.ai.android.utils.bottomsheet.OnBottomSheetItemClickListener;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AddComplaintActivity extends BaseActivity implements OnRecyclerViewItemClickListener,
        ShowBottomSheetListener, OnBottomSheetItemClickListener, DialogClickListener,ValidationListener {
    public static int mUnitId;
    public static String mUnitName;
    public static boolean isComplaintFromUnitLevel = false;
    ArrayList<Object> mDataArray;
    @Bind(R.id.complaint_list_holder)
    LinearLayout compalintListHolder;
    @Bind(R.id.complaint_list)
    ListView mcomplaintList;
    @Bind(R.id.main_content)
    CoordinatorLayout mainContent;
    private RecyclerView addComplaintRecylerView;
    private Drawable dividerDrawable;
    private AddComplaintsAdapter addComplaintsAdapter;
    private BottomSheetAdapter bottomSheetAdapter;
    private BottomSheetBehavior<LinearLayout> bottomSheetBehavior;
    private ShowBottomSheetListener mShowBottomSheetListener;
    private RotaTaskModel rotaTaskModel;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    private ArrayList<IssuesMO> issuesDayCheckList;
    private ArrayList<IssuesMO> issuesMOList;
    private IssuesMO issuesMO;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ValidationListener mValidationListener;
    private boolean actionPlanValidation = true;
    private boolean actionPlanSelected = false;
    private boolean isFromRotaLevel= false;
    private ArrayList<IssuesMO> complaintListMo;
    private Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_complaint);
        ButterKnife.bind(this);
        Bundle data = getIntent().getExtras();
        isComplaintFromUnitLevel = false;
        resources = getResources();
        if (data.getInt(TableNameAndColumnStatement.UNIT_ID) > 0) {
            mUnitId = getIntent().getExtras().getInt(TableNameAndColumnStatement.UNIT_ID);
            mUnitName = getIntent().getExtras().getString(TableNameAndColumnStatement.UNIT_NAME);
            isFromRotaLevel = getIntent().getExtras().getBoolean(TableNameAndColumnStatement.CHECKING_TYPE_DAY_CHECKING);
            isComplaintFromUnitLevel = true;

        }
        mValidationListener = this;
        addComplaintRecylerView = (RecyclerView) findViewById(R.id.add_complaints_recylerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        compalintListHolder = (LinearLayout) findViewById(R.id.complaint_list_holder);
        addComplaintRecylerView.setLayoutManager(linearLayoutManager);
        addComplaintRecylerView.setItemAnimator(new DefaultItemAnimator());
        mDataArray = new ArrayList<>();
        issuesMOList = new ArrayList<>();
        issuesDayCheckList=new ArrayList<>();
        mShowBottomSheetListener = this;
        issuesMO = new IssuesMO();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        rotaTaskModel =  appPreferences.getRotaTaskJsonData();
        if(isFromRotaLevel && rotaTaskModel!= null) {
              issuesMO.setSourceTaskId(rotaTaskModel.getTaskId());
        }
        setBaseToolbar(mToolbar, getResources().getString(R.string.addComplaint));

        addComplaintsAdapter = new AddComplaintsAdapter(this, mDataArray, compalintListHolder, mShowBottomSheetListener, "my NAME", issuesMO,mValidationListener);
        addComplaintsAdapter.setRotaData(null);
        addComplaintRecylerView.setAdapter(addComplaintsAdapter);
        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        addComplaintRecylerView.addItemDecoration(dividerItemDecoration);
        addComplaintsAdapter.setOnRecyclerViewItemClickListener(this);
        issuesMO.setBarrackId(0);
        issuesMO.setClosureEndDate("");
        issuesMO.setIssueTypeId(IssueTypeEnum.Complaint.getValue());
        issuesMO.setClosureRemarks("");
        issuesMO.setCreatedById(0);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_complaint, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_complaint_done:
                if (actionPlanValidation && actionPlanSelected) {

                    issuesMOList.clear();
                    issuesMO.setBarrackId(0);
                    issuesMO.setClosureEndDate("");
                    issuesMO.setIssueTypeId(IssueTypeEnum.Complaint.getValue());
                    issuesMO.setClosureRemarks("");
                    issuesMO.setCreatedById(0);
                    issuesMO.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                    issuesMO.setCreatedById(appPreferences.getAppUserId());
                    issuesMO.setIssueStatusId(1);
                    issuesMOList.add(issuesMO);
                    complaintListMo = new ArrayList<>();
                    complaintListMo = Util.getmComplaintModelHolder();
                    if(null!=complaintListMo && complaintListMo.size()!=0){
                        if (!improvementPlanValidation(complaintListMo)) {
                            setComplaintData();
                        } else {
                            Toast.makeText(this, "Please select different Action plan", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        setComplaintData();
                    }



                } else {
                    snackBarWithMesg(mainContent, getResources().getString(R.string.client_handshake_validation));
                    actionPlanValidation = true;
                }

                break;
            case android.R.id.home:
                showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setComplaintData() {
        if(isFromRotaLevel){
            Util.setmComplaintModelHolder(issuesMO);
        }else {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(issuesMOList);
            Util.setmComplaintModelHolder(issuesMO);
            Bundle bundel = new Bundle();
            IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
            //ComplaintsFragment.newInstance("", "").initComplaintsViews();
            appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
            IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_complaints));
        }
        finish();
    }

    private boolean improvementPlanValidation(ArrayList<IssuesMO> grivanceListMo) {
      boolean isCombinationAvailable=false;
        if (null != grivanceListMo && grivanceListMo.size() != 0) {
            for (int i = 0; i < grivanceListMo.size(); i++) {

                if (grivanceListMo.get(i).getIssueModeId()==(issuesMO.getIssueModeId())) {

                    if (grivanceListMo.get(i).getIssueNatureId() == issuesMO.getIssueNatureId()) {


                        if (grivanceListMo.get(i).getActionPlanId() == issuesMO.getActionPlanId()) {


                            if (grivanceListMo.get(i).getUnitId() == issuesMO.getUnitId()) {
                                isCombinationAvailable = true;
                                break;
                            }

                        }
                    }
                }
            }
        }
        return isCombinationAvailable;
    }

    @Override
    public void onSheetItemClick(String itemSelected, int viewId) {
        addComplaintsAdapter.setValue(itemSelected, viewId);
    }

    @Override
    public void onUnitSheetItemClick(MyUnitHomeMO unitHomeMOList, int viewId) {

    }

    @Override
    public void showBottomSheet(ArrayList<String> elements, int viewId) {
        setBottomSheetFragment(elements, viewId);
    }

    @Override
    public void showUnitModelBottomSheet(List<MyUnitHomeMO> myUnitHomeMOs, int viewId) {

    }

    public void setBottomSheetFragment(ArrayList<String> elements, int viewId) {

        CustomBottomSheetFragment bottomSheetDialogFragment = new CustomBottomSheetFragment();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        bottomSheetDialogFragment.setSheetItemClickListener(this);
        bottomSheetDialogFragment.setBottomSheetData(elements, viewId);

    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        finish();
    }

    @Override
    public void showSnackBar(String msg) {

        snackBarWithMesg(mainContent, msg);
    }

    @Override
    public void setValidationStatus(boolean value) {
        actionPlanValidation = value;
    }

    @Override
    public void setActionPlanSelection(boolean value) {
        actionPlanSelected = value;
    }

}
