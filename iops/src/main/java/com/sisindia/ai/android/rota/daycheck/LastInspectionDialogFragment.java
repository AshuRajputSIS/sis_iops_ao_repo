package com.sisindia.ai.android.rota.daycheck;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 19-06-2016.
 */
public class LastInspectionDialogFragment extends DialogFragment {






    private AdapterView.OnItemClickListener onItemClickListener;
    public static final String TAG = "LastInspectionDialogFragment";
    private View view;
    private View titleView;
    private int pageNumber;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private DialogClickListener dialogClickListener;
    private Object dialogContent;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private TextView dialogTitle;


    public LastInspectionDialogFragment() {
        // Required empty public constructor
    }


    public static LastInspectionDialogFragment newInstance() {
        LastInspectionDialogFragment lastInspectionDialogFragment = new LastInspectionDialogFragment();
        Bundle args = new Bundle();
        lastInspectionDialogFragment.setArguments(args);
        return lastInspectionDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if (context instanceof DialogClickListener) {
            dialogClickListener = (DialogClickListener) context;
        }
    }

    public void setLayout(int whichPage) {
        this.pageNumber = whichPage;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        titleView = LayoutInflater.from(mContext).inflate(R.layout.dialog_title, null);
        dialogTitle = (TextView) titleView.findViewById(R.id.dialogTitle);


        switch (pageNumber) {
            case 1:
                View issuesView = LayoutInflater.from(mContext).inflate(R.layout.generic_cardview, null);
                view = setData(issuesView, dialogContent);
                builder.setCustomTitle(titleView);
                builder = setPositiveNegativeButtons(builder, false, getResources().getString(R.string.CANCEL));
                break;
            case 2:
                View unitAtRiskview = LayoutInflater.from(mContext).inflate(R.layout.unitat_risk_dialog_content, null);
                view = setData(unitAtRiskview, dialogContent);
                builder.setCustomTitle(titleView);
                builder = setPositiveNegativeButtons(builder, false, getResources().getString(R.string.CANCEL));
                break;
        }

        builder.setView(view);
        return builder.create();
    }

    private View setData(View layOutview, Object dialogContent) {

        if (dialogContent instanceof LastCheckInspectionGrievancesMO) {
            ViewHolder viewHolder = new ViewHolder(layOutview);
            LastCheckInspectionGrievancesMO lastCheckInspectionGrievancesMO = (LastCheckInspectionGrievancesMO) dialogContent;
            viewHolder.unitName.setText(lastCheckInspectionGrievancesMO.getUnitName());
            viewHolder.personName.setText(lastCheckInspectionGrievancesMO.getGuardName());
            viewHolder.assignedTo.setText(lastCheckInspectionGrievancesMO.getAssignedToName());
            viewHolder.lastInspectionStatus.setText(lastCheckInspectionGrievancesMO.getStatus());
            viewHolder.issueTargetClosureDate.setText(lastCheckInspectionGrievancesMO.getGrievanceClosedDate());
            String createdDateTime = lastCheckInspectionGrievancesMO.getCreatedDateTime();
            long millis = dateTimeFormatConversionUtil.convertStringToDate(createdDateTime).getTime();
            viewHolder.timeDurationLeft.setReferenceTime(millis);
            dialogTitle.setText(lastCheckInspectionGrievancesMO.getGrievanceNature());
            dialogTitle.setTextColor(getResources().getColor(R.color.black));

        } else if (dialogContent instanceof LastCheckInspectionComplaintsMO) {
            ViewHolder viewHolder = new ViewHolder(layOutview);
            LastCheckInspectionComplaintsMO lastCheckInspectionComplaintsMO = (LastCheckInspectionComplaintsMO) dialogContent;
            viewHolder.unitName.setText(lastCheckInspectionComplaintsMO.getUnitName());
            viewHolder.personName.setText(lastCheckInspectionComplaintsMO.getUnitContactPerson());
            viewHolder.assignedTo.setText(lastCheckInspectionComplaintsMO.getAssignedName());
            viewHolder.lastInspectionStatus.setText(lastCheckInspectionComplaintsMO.getComplaintStatus());
            viewHolder.issueTargetClosureDate.setText(lastCheckInspectionComplaintsMO.getComplaintClosedDate());
            String createdDateTime = lastCheckInspectionComplaintsMO.getCreatedDateTime();
            long millis = dateTimeFormatConversionUtil.convertStringToDate(createdDateTime).getTime();
            viewHolder.timeDurationLeft.setReferenceTime(millis);
            dialogTitle.setText(lastCheckInspectionComplaintsMO.getNatureOfComplaint());
            dialogTitle.setTextColor(getResources().getColor(R.color.black));

        } else if (dialogContent instanceof LastCheckInspectionUnitAtRiskMO) {
            UnitAtRiskViewHolder unitAtRiskViewHolder = new UnitAtRiskViewHolder(layOutview);
            LastCheckInspectionUnitAtRiskMO lastCheckInspectionUnitAtRiskMO = (LastCheckInspectionUnitAtRiskMO) dialogContent;
            dialogTitle.setText(lastCheckInspectionUnitAtRiskMO.getPlanOfAction());
            String date = dateTimeFormatConversionUtil.convertDateTimeToDate(lastCheckInspectionUnitAtRiskMO.getPlannedClosureDate());
            dialogTitle.setTextColor(getResources().getColor(R.color.black));
            unitAtRiskViewHolder.unitatRiskAssignedtoTv.setText(lastCheckInspectionUnitAtRiskMO.getAssignedTo());
            unitAtRiskViewHolder.unitatRiskAssignedtoDateTv.setText(date);
        }
        else if (dialogContent instanceof KitItemRequestMO) {
            ViewHolder viewHolder = new ViewHolder(layOutview);
            KitItemRequestMO lastCheckInspectionKitItemMO = (KitItemRequestMO) dialogContent;
            viewHolder.unitName.setVisibility(View.GONE);
            ArrayList<String> kitRequestNames = lastCheckInspectionKitItemMO.getKitItemRequestNames();
            String builder = "";
            if(kitRequestNames != null && kitRequestNames.size() != 0){
              for(String s :kitRequestNames )
               builder = builder + s + "\n";
            }

            viewHolder.personName.setText(builder);

            viewHolder.assignedTo.setVisibility(View.GONE);
            viewHolder.lastInspectionStatus.setVisibility(View.GONE);
            viewHolder.issueTargetClosureDate.setVisibility(View.GONE);
            viewHolder.timeDurationLeft.setVisibility(View.GONE);
            viewHolder.assignedToLabel.setVisibility(View.GONE);
            viewHolder.statusLabel.setVisibility(View.GONE);

            dialogTitle.setText(getResources().getString(R.string.KIT_ITEMS_LABEL));
            dialogTitle.setTextColor(getResources().getColor(R.color.black));
        }
        else if (dialogContent instanceof ImprovementPlanListMO) {
            ViewHolder viewHolder = new ViewHolder(layOutview);
            ImprovementPlanListMO lastCheckInspectionImprovementMO = (ImprovementPlanListMO) dialogContent;
            if (TextUtils.isEmpty(lastCheckInspectionImprovementMO.getUnitName()))
                viewHolder.unitName.setText(lastCheckInspectionImprovementMO.getBarrackName());
            else
                viewHolder.unitName.setText(lastCheckInspectionImprovementMO.getUnitName());
            // personName.setText(((ImprovementPlanListMO) object).getImprovementStatus());
            viewHolder.assignedTo.setText(lastCheckInspectionImprovementMO.getAssignedToName());
            viewHolder.lastInspectionStatus.setText(lastCheckInspectionImprovementMO.getImprovementStatus());
            String closureDateTime = lastCheckInspectionImprovementMO.getExpectedClosureDate();
            String createdDateTime = lastCheckInspectionImprovementMO.getCreatedDateTime();
            long millis = dateTimeFormatConversionUtil.convertStringToDate(createdDateTime).getTime();
            if(!TextUtils.isEmpty(closureDateTime)) {
                viewHolder.issueTargetClosureDate.setText(dateTimeFormatConversionUtil.convertDateTimeToDate(closureDateTime));
            }
            viewHolder.timeDurationLeft.setReferenceTime(millis);
            dialogTitle.setText(lastCheckInspectionImprovementMO.getActionPlanName());
            dialogTitle.setTextColor(getResources().getColor(R.color.black));

        }
        return layOutview;
    }

    private AlertDialog.Builder setPositiveNegativeButtons(AlertDialog.Builder builder, boolean isBothButtonsReq, String buttonCaption) {

        builder.setNegativeButton(buttonCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        return builder;
    }

    public void setContent(Object object) {
        dialogContent = object;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    static class UnitAtRiskViewHolder {
        @Bind(R.id.unitat_risk_assignedto_label_tv)
        CustomFontTextview unitatRiskAssignedtoLabelTv;
        @Bind(R.id.unitat_risk_assignedto_tv)
        CustomFontTextview unitatRiskAssignedtoTv;
        @Bind(R.id.unitat_risk_assignedto_date_tv)
        CustomFontTextview unitatRiskAssignedtoDateTv;

        UnitAtRiskViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    static class ViewHolder {
            /*
*
* Grievances and Complaints Fields
*
* */

        @Bind(R.id.lastInspectionUnitName)
        CustomFontTextview unitName;
        @Bind(R.id.guradName_or_clientName)
        CustomFontTextview personName;
        @Bind(R.id.lastInspectionAssignedTo)
        CustomFontTextview assignedTo;
        @Bind(R.id.lastInspectionStatus)
        CustomFontTextview lastInspectionStatus;
        @Bind(R.id.issueTargetClosureDate)
        CustomFontTextview issueTargetClosureDate;
        @Bind(R.id.timeDurationLeft)
        RelativeTimeTextView timeDurationLeft;
        @Bind(R.id.checking_grievance_complaint_assigned_to_label_txt)
        TextView assignedToLabel;
        @Bind(R.id.checking_grievance_complaint_status_label_txt)
        TextView statusLabel;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}


