package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.AddRecruitmentIR;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecruitmentOR;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by shankar on 27/10/16.
 */

public class RecruitmentTableSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<AddRecruitmentIR> recruitmentIRList = new ArrayList<>();

    public RecruitmentTableSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);

        RecruitmentDataSyncing();
    }

    private synchronized void RecruitmentDataSyncing() {
        recruitmentIRList = getRecruitmentList();
        if (recruitmentIRList != null && recruitmentIRList.size() != 0) {
            for (AddRecruitmentIR recruitmentIR : recruitmentIRList) {
                RecruitmentSyncing(recruitmentIR);
                Timber.d(Constants.TAG_SYNCADAPTER+"RecruitmentDataSyncing count   %s", "count: ");
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"RecruitmentDataSyncing -->nothing to sync ");
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
        }
    }

    private void RecruitmentSyncing(final AddRecruitmentIR addRecruitmentIR) {

        Timber.d(Constants.TAG_SYNCADAPTER+"RecruitmentSyncing object  %s", addRecruitmentIR);
        sisClient.getApi().syncRecuriment(addRecruitmentIR, new Callback<ApiResponse<RecruitmentOR>>() {
            @Override
            public void success(ApiResponse<RecruitmentOR> recruitmentORApiResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"RecruitmentSyncing response  %s", recruitmentORApiResponse.statusCode);
                if(null!=recruitmentORApiResponse){
                    switch (recruitmentORApiResponse.statusCode)
                    {
                        case HttpURLConnection.HTTP_OK:


                            if(null!=recruitmentORApiResponse.data){
                                updateRecruitmentTable(addRecruitmentIR.getId(),recruitmentORApiResponse.data.getRecruitmentId()); // uplaod  the Attachment id to Metadata tables

                            }

                            break;

                        default:

                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }

        });

    }

    private List<AddRecruitmentIR> getRecruitmentList() {
        List<AddRecruitmentIR> recruitmentIRList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.RECRUITMENT_TABLE +
                " where " + TableNameAndColumnStatement.IS_SYNCED + "=0";
        Timber.d("getRecruitmentList selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        AddRecruitmentIR addRecruitmentIR = new AddRecruitmentIR();
                        addRecruitmentIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        addRecruitmentIR.setFullName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FULL_NAME)));
                        addRecruitmentIR.setDateOfBirth(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DATE_OF_BIRTH)));
                        addRecruitmentIR.setContactNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CONTACT_NO)));
                        addRecruitmentIR.setIsRejected(0);
                        addRecruitmentIR.setIsSelected(0);
                        recruitmentIRList.add(addRecruitmentIR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {

            if (null!=sqlite && sqlite.isOpen()){
                sqlite.close();
            }


        }

        return recruitmentIRList;
    }





    private void updateRecruitmentTable(int id, int recruitId) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.RECRUITMENT_TABLE +
                " SET " + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "," +TableNameAndColumnStatement.RECRUIT_ID + "= '" + recruitId + "'" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"updateRecruitmentTable  %s", updateQuery);

        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null!=sqlite && sqlite.isOpen()){
                sqlite.close();
            }

        }
    }

}
