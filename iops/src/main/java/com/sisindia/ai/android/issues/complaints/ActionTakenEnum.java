package com.sisindia.ai.android.issues.complaints;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum ActionTakenEnum {

    Temporary_Deployment(1),
    Job_Training(2),
    Change_In_Manpower(3);


    private static Map map = new HashMap<>();

    static {
        for (ActionTakenEnum postStatusEM : ActionTakenEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    ActionTakenEnum(int value) {
        this.value = value;
    }

    public static ActionTakenEnum valueOf(int postStatus) {
        return (ActionTakenEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
