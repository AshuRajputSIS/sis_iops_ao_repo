package com.sisindia.ai.android.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Durga Prasad on 08-11-2016.
 */
public class DutyAttendanceMo implements Parcelable {

    private String dutyOffTime;
    private String dutyOnTime;
    private Integer dutyResponseId;

    /*private String employeeId;
    private String taskAssigned;
    private String taskCompleted;*/


    public String getDutyOffTime() {
        return dutyOffTime;
    }

    public void setDutyOffTime(String dutyOffTime) {
        this.dutyOffTime = dutyOffTime;
    }

    public String getDutyOnTime() {
        return dutyOnTime;
    }

    public void setDutyOnTime(String dutyOnTime) {
        this.dutyOnTime = dutyOnTime;
    }

    public Integer getDutyResponseId() {
        return dutyResponseId;
    }

    public void setDutyResponseId(Integer dutyResponseId) {
        this.dutyResponseId = dutyResponseId;
    }

    /*public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getTaskAssigned() {
        return taskAssigned;
    }

    public void setTaskAssigned(String taskAssigned) {
        this.taskAssigned = taskAssigned;
    }

    public String getTaskCompleted() {
        return taskCompleted;
    }

    public void setTaskCompleted(String taskCompleted) {
        this.taskCompleted = taskCompleted;
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dutyOffTime);
        dest.writeString(this.dutyOnTime);
        dest.writeValue(this.dutyResponseId);
        /*dest.writeString(this.employeeId);
        dest.writeString(this.taskAssigned);
        dest.writeString(this.taskCompleted);*/
    }

    public DutyAttendanceMo() {
    }

    protected DutyAttendanceMo(Parcel in) {
        this.dutyOffTime = in.readString();
        this.dutyOnTime = in.readString();
        this.dutyResponseId = (Integer) in.readValue(Integer.class.getClassLoader());
        /*this.employeeId = in.readString();
        this.taskAssigned = in.readString();
        this.taskCompleted = in.readString();*/
    }

    public static final Parcelable.Creator<DutyAttendanceMo> CREATOR = new Parcelable.Creator<DutyAttendanceMo>() {
        @Override
        public DutyAttendanceMo createFromParcel(Parcel source) {
            return new DutyAttendanceMo(source);
        }

        @Override
        public DutyAttendanceMo[] newArray(int size) {
            return new DutyAttendanceMo[size];
        }
    };
}
