package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 11-05-2016.
 */
public class UnitEquipmentData {
    @SerializedName("unitEquipmentId")
    @Expose
    private Integer unitEquipmentId;

    /**
     * @return The unitEquipmentId
     */
    public Integer getUnitEquipmentId() {
        return unitEquipmentId;
    }

    /**
     * @param unitEquipmentId The unitEquipmentId
     */
    public void setUnitEquipmentId(Integer unitEquipmentId) {
        this.unitEquipmentId = unitEquipmentId;
    }

    @Override
    public String toString() {
        return "UnitEquipmentData{" +
                "unitEquipmentId=" + unitEquipmentId +
                '}';
    }
}
