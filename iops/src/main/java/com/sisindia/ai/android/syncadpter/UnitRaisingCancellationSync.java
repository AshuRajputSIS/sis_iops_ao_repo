package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.UnitPostIR;
import com.sisindia.ai.android.network.response.UnitAddPostResponse;
import com.sisindia.ai.android.unitraising.UnitRaisingDeferredOrCancelledMo;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingCancelOR;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;




public class UnitRaisingCancellationSync extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private int count = 0;
    private HashMap<Integer, UnitRaisingCancelOR> unitRaisingCancelList;
    private AppPreferences appPreferences;

    public UnitRaisingCancellationSync(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mcontext);
        //  unitEquipmentSync = new UnitEquipmentSyncing(mContext);
        unitRaisingCancellationSyncing();
    }

    private  void unitRaisingCancellationSyncing() {
        unitRaisingCancelList = getUnitRaisingCancellationList();
        if (unitRaisingCancelList != null && unitRaisingCancelList.size() != 0) {
            for (Integer key : unitRaisingCancelList.keySet()) {
                UnitRaisingCancelOR unitRaisingCancelOR =
                        unitRaisingCancelList.get(key);
                unitRaisingCancelApi(unitRaisingCancelOR, key);

                Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingDeferredSyncing count   %s", "count: " + count);
                Timber.d("unitRaisingDeferredSyncing   %s", "key: " + key + " value: " +
                        unitRaisingCancelList.get(key));
            }

        } else {
            Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingDeferredSyncing -->nothing to sync ");
            dependentFailureSync();
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                Timber.d(Constants.TAG_SYNCADAPTER+" UnitRaisingCancelllaionSync # dependentFailureSync --> "+dependentSyncsStatus.unitRaisingCancellationId );
                if(dependentSyncsStatus != null && dependentSyncsStatus.unitRaisingCancellationId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {

        if(dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            if(dependentSyncsStatus != null){
                if(dependentSyncsStatus.unitRaisingCancellationId != 0) {
                    metaDataSyncing.setUnitRaisingCancellationId(
                            dependentSyncsStatus.unitRaisingCancellationId);
                }
            }
        }
        else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                if(dependentSyncsStatus != null) {
                    if(dependentSyncsStatus.unitRaisingCancellationId != 0) {
                        imageSyncing.setUnitRaisingCancellationId(
                                dependentSyncsStatus.unitRaisingCancellationId);
                    }
                }
            }
        }
    }




    private HashMap<Integer, UnitRaisingCancelOR> getUnitRaisingCancellationList() {
        HashMap<Integer, UnitRaisingCancelOR> unitRaisingCancelMap = new HashMap<>();
        SQLiteDatabase sqlite = null;

        String selectQuery = "SELECT  u.unit_id  as unit_id,u.unit_code as unit_code" +
                ",urc.expected_raising_date_time as expected_raising_date_time,urc.cancellation_reason" +
                " as cancellation_reason ," +
                "urc.remarks as remarks,urc.id as id,urc.CreatedDateTime as CreatedDateTime" +
                "  FROM " + TableNameAndColumnStatement.UNIT_RAISING_CANCELLED + " urc join unit u on u.unit_id = urc.unit_id "+
                " where " + TableNameAndColumnStatement.IS_SYNCED + "=" + 0;
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitRaisingCancelled selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitRaisingCancelOR unitRaisingCancelOR = new
                                UnitRaisingCancelOR();
                        unitRaisingCancelOR.unitId =
                                cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        unitRaisingCancelOR.unitCode =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE));
                        unitRaisingCancelOR.expectedRaisingDate =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME));
                        unitRaisingCancelOR.reason =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CANCELLATION_REASON));
                        unitRaisingCancelOR.remarks =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS));
                        unitRaisingCancelOR.cancellationDateTime =
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME));
                        unitRaisingCancelOR.createdBy  =  appPreferences.getAppUserId();
                        unitRaisingCancelMap.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)),
                                unitRaisingCancelOR);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"addPostList object  %s",
                    IOPSApplication.getGsonInstance().toJson(unitRaisingCancelMap));

        }
        return unitRaisingCancelMap;
    }


    private void updateUnitRaisingCancelled(int unitRaisingCancellationId, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_RAISING_CANCELLED +
                " SET " + TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " = " +
                unitRaisingCancellationId +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + "= " + id;
        Timber.d(Constants.TAG_SYNCADAPTER+"updateUnitRaisingCancellation updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }



    private void unitRaisingCancelApi(final UnitRaisingCancelOR unitRaisingCancelOR, final int key) {
        sisClient.getApi().cancelUnitRaising(unitRaisingCancelOR, new Callback<UnitRaisingCancelResponse>() {
            @Override
            public void success(UnitRaisingCancelResponse unitRaisingCancelResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingCancelApi response  %s", unitRaisingCancelResponse);
                switch (unitRaisingCancelResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        updateUnitRaisingCancelled(unitRaisingCancelResponse
                                .unitRaisingCancelData
                                .unitRaisingCancellationId, key);  //updating PostID in UnitPost Table
                        updateUnitRaisingMetaData(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE,
                                        unitRaisingCancelResponse
                                        .unitRaisingCancelData
                                        .unitRaisingCancellationId, key, Util.
                                        getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED, mContext));
                         updateStatus(unitRaisingCancelResponse,unitRaisingCancelOR);
                        //updating PostID in Metadata Table
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UNIT_RAISING_CANCEL_SYNC", error);
            }
        });

        Timber.d(Constants.TAG_SYNCADAPTER+"unitRaisingCancelApi  count   %s", "count: " + count);


    }


    private void updateStatus(UnitRaisingCancelResponse unitRaisingCancelResponse,
                              UnitRaisingCancelOR unitRaisingCancelOR) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.unitRaisingCancellationId = unitRaisingCancelResponse
                .unitRaisingCancelData.unitRaisingCancellationId;
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;


        insertDepedentSyncData(dependentSyncsStatus);
        MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
        metaDataSyncing.setUnitRaisingCancellationId(dependentSyncsStatus.unitRaisingCancellationId);
    }


    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus) {
        if(!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.unitRaisingCancellationId,
                TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID,true)){
            dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus,1);
        }
    }
}

