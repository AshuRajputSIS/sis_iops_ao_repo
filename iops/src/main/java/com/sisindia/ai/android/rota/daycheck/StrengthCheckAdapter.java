package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 11-06-2016.
 */
public class StrengthCheckAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Object> strengthCheckMOArrayList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ROW = 2;


    public StrengthCheckAdapter(Context context) {
        this.context = context;

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    public void setStrengthCheckList(ArrayList<Object> strengthCheckMOArrayList) {
        this.strengthCheckMOArrayList = strengthCheckMOArrayList;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == context.getResources().getInteger(R.integer.number_0))
            return TYPE_HEADER;
        else
            return TYPE_ROW;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView = null;
        switch (viewType) {
            case TYPE_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.strength_check_header, parent, false);
                recycleViewHolder = new StrengthCheckHeaderViewHolder(itemView);
                break;

            case TYPE_ROW:
                itemView = LayoutInflater.from(context).inflate(R.layout.strength_check_row_item, parent, false);
                recycleViewHolder = new StrengthCheckViewHolder(itemView);
                break;
        }


        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StrengthCheckViewHolder strengthCheckViewHolder = null;
        if (holder instanceof StrengthCheckViewHolder) {
            strengthCheckViewHolder = (StrengthCheckViewHolder) holder;
            String editedActualCount = strengthCheckViewHolder.strengthCheckActualEt.getText().toString();

        }


    }

    @Override
    public int getItemCount() {
        int count = 10;

        //count = strengthCheckMOArrayList== null ? 0 : strengthCheckMOArrayList.size();
        return count;
    }


    public class StrengthCheckViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.strengthCheckRankName_tv)
         public CustomFontTextview strengthCheckRankNameTv;
        @Bind(R.id.strengthCheckAuthorized_tv)
         public CustomFontTextview strengthCheckAuthorizedTv;
        @Bind(R.id.strengthCheckActual_et)
         public CustomFontEditText strengthCheckActualEt;
        @Bind(R.id.errorMessage)
        public CustomFontTextview errorMessage;

        public StrengthCheckViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.edit_contact_label_txt || view.getId() == R.id.imageEdit)
                onRecyclerViewItemClickListener.onRecyclerViewItemClick(view, getLayoutPosition());
        }
    }

    private class StrengthCheckHeaderViewHolder extends RecyclerView.ViewHolder {
        public StrengthCheckHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }


}

