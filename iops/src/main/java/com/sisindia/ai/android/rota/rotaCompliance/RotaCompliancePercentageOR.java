package com.sisindia.ai.android.rota.rotaCompliance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RotaCompliancePercentageOR implements Parcelable {


    @SerializedName("StatusCode")
    public Integer statusCode;
    @SerializedName("StatusMessage")
    public String statusMessage;
    @SerializedName("Data")
    public MonthlyDailyRotaCompliance monthlyDailyRotaCompliance;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.statusCode);
        dest.writeString(this.statusMessage);
        dest.writeParcelable(this.monthlyDailyRotaCompliance, flags);
    }

    public RotaCompliancePercentageOR() {
    }

    protected RotaCompliancePercentageOR(Parcel in) {
        this.statusCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.statusMessage = in.readString();
        this.monthlyDailyRotaCompliance = in.readParcelable(MonthlyDailyRotaCompliance.class.getClassLoader());
    }

    public static final Creator<RotaCompliancePercentageOR> CREATOR = new Creator<RotaCompliancePercentageOR>() {
        @Override
        public RotaCompliancePercentageOR createFromParcel(Parcel source) {
            return new RotaCompliancePercentageOR(source);
        }

        @Override
        public RotaCompliancePercentageOR[] newArray(int size) {
            return new RotaCompliancePercentageOR[size];
        }
    };
}