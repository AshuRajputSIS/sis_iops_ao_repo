package com.sisindia.ai.android.annualkitreplacement;

/**
 * Created by Durga Prasad on 26-09-2016.
 */
public class AnnualOrAdhocKitItemMO {

    private Integer unitId;
    private String unitName;
    private Integer issuedKitCount;
    private Integer pendingKitCount;
    private Integer totalItems;
    private String guardName;
    private Integer guardId;
    private String guardCode;
    private Integer deliveryStatusId;
    private  String deliveryStatus;
    private int count;
    private String issueDate;
    private int taskId;
    private Integer kitDistributionId;
    private boolean isUnPaid;
    private Integer unPaidKits;

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Integer getDeliveryStatusId() {
        return deliveryStatusId;
    }

    public void setDeliveryStatusId(Integer deliveryStatusId) {
        this.deliveryStatusId = deliveryStatusId;
    }

    public Integer getGuardId() {
        return guardId;
    }

    public void setGuardId(Integer guardId) {
        this.guardId = guardId;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public Integer getIssuedKitCount() {
        return issuedKitCount;
    }

    public void setIssuedKitCount(Integer issuedKitCount) {
        this.issuedKitCount = issuedKitCount;
    }

    public Integer getPendingKitCount() {
        return pendingKitCount;
    }

    public void setPendingKitCount(Integer pendingKitCount) {
        this.pendingKitCount = pendingKitCount;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDate() {
        return issueDate;
    }


    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setKitDistributionId(Integer kitDistributionId) {
        this.kitDistributionId = kitDistributionId;
    }

    public Integer getKitDistributionId() {
        return kitDistributionId;
    }


    public void setUnpiad(boolean isUnPaid) {
        this.isUnPaid = isUnPaid;
    }

    public boolean isUnPaid() {
        return isUnPaid;
    }

    public void setUnPaidKits(int unPaidKits) {
        this.unPaidKits = unPaidKits;
    }

    public Integer getUnPaidKits() {
        return unPaidKits;
    }

    public String getGuardCode() {
        return guardCode;
    }

    public void setGuardCode(String guardCode) {
        this.guardCode = guardCode;
    }
}
