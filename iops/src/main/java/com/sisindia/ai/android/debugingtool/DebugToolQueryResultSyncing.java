package com.sisindia.ai.android.debugingtool;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorBaseOR;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.syncadpter.CommonSyncDbOperation;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 9/20/2017.
 */

public class DebugToolQueryResultSyncing extends CommonSyncDbOperation {
    private SISClient sisClient;
    private Context context;
    private ArrayList<QueryExecutorMO> queryExecutorResultList;


    public DebugToolQueryResultSyncing(Context mcontext) {
        super(mcontext);
        context = mcontext;
        sisClient = new SISClient(mcontext);
        debugToolQueryResultSyncing();
    }

    private void debugToolQueryResultSyncing() {
        queryExecutorResultList = getQueryResult();
        if (queryExecutorResultList != null && queryExecutorResultList.size() != 0) {
            for (QueryExecutorMO queryExecutorMO : queryExecutorResultList) {
                posyQueryExecutorResult(queryExecutorMO);
            }
        }
        else
        {
            Timber.d(Constants.TAG_SYNCADAPTER + "debugToolQueryResultSyncing -->nothing to sync ");
        }
    }

    private ArrayList<QueryExecutorMO> getQueryResult() {
        ArrayList<QueryExecutorMO> queryExecutorMOList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.QUERY_TABLE_NAME + " where " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                       QueryExecutorMO queryExecutorMO = new QueryExecutorMO();
                        queryExecutorMO.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.QUERY_ID))));
                        queryExecutorMO.setQueryResult(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.QUERY_RESULT)));
                        queryExecutorMO.setQueryStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.QUERY_STATUS)));
                        queryExecutorMOList.add(queryExecutorMO);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER + "QueryResult object  %s",
                    IOPSApplication.getGsonInstance().toJson(queryExecutorMOList));
        }
        return queryExecutorMOList;
    }


    private synchronized void posyQueryExecutorResult(final QueryExecutorMO queryExecutorMO) {
        SISClient sisClient = new SISClient(context);
        sisClient.getApi().postQueryExecutorDetails(queryExecutorMO, new Callback<QueryExecutorBaseOR>() {
            @Override
            public void success(final QueryExecutorBaseOR queryExecutorOR, Response response) {
                switch (queryExecutorOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        deleteQueryExecutionTable(Integer.valueOf(queryExecutorMO.getId()));
                        break;
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);
            }
        });
        }

    private boolean deleteQueryExecutionTable(int queryId) {
        SQLiteDatabase sqlite = null;
        boolean isDeletedSuccessfully = false;
        String updateQuery=  " delete  from "+ TableNameAndColumnStatement.QUERY_TABLE_NAME +
                " WHERE "+ TableNameAndColumnStatement.QUERY_ID +" = "+ queryId ;
        Timber.d(Constants.TAG_SYNCADAPTER+"deleteQueryExecutionTable  %s", updateQuery);
        Cursor cursor = null;
        try{
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance()
                    .getWritableDatabase();
            sqlite.execSQL(updateQuery);
            isDeletedSuccessfully = true;
        }
        catch (Exception exception){
            isDeletedSuccessfully = false;
            exception.getCause();
        }
        finally {
            if(sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return  isDeletedSuccessfully;
    }
}

