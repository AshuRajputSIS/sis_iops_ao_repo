package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.rota.models.RotaMO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 22/6/16.
 */

public class Data {

    @SerializedName("UserProfile")
    @Expose
    private UserProfile userProfile;
    @SerializedName("Units")
    @Expose
    private List<Unit> units = new ArrayList<Unit>();
    @SerializedName("Barrack")
    @Expose
    private List<Barrack> barrack = new ArrayList<Barrack>();
    @SerializedName("Branch")
    @Expose
    private Branch branch;
    @SerializedName("Areas")
    @Expose
    private List<Object> areas = new ArrayList<Object>();
    @SerializedName("HolidayCalendar")
    @Expose
    private List<HolidayCalendar> holidayCalendar = new ArrayList<HolidayCalendar>();
    @SerializedName("Rota")
    @Expose
    private RotaMO rotaMO;
    @SerializedName("RankMaster")
    @Expose
    private List<RankMaster> rankMaster = new ArrayList<RankMaster>();



    /**
     *
     * @return
     * The userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    /**
     *
     * @param userProfile
     * The UserProfile
     */
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    /**
     *
     * @return
     * The units
     */
    public List<Unit> getUnits() {
        return units;
    }

    /**
     *
     * @param units
     * The Units
     */
    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    /**
     *
     * @return
     * The barrack
     */
    public List<Barrack> getBarrack() {
        return barrack;
    }

    /**
     *
     * @param barrack
     * The Barrack
     */
    public void setBarrack(List<Barrack> barrack) {
        this.barrack = barrack;
    }

    /**
     *
     * @return
     * The branch
     */
    public Branch getBranch() {
        return branch;
    }

    /**
     *
     * @param branch
     * The Branch
     */
    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    /**
     *
     * @return
     * The areas
     */
    public List<Object> getAreas() {
        return areas;
    }

    /**
     *
     * @param areas
     * The Areas
     */
    public void setAreas(List<Object> areas) {
        this.areas = areas;
    }

    /**
     *
     * @return
     * The holidayCalendar
     */
    public List<HolidayCalendar> getHolidayCalendar() {
        return holidayCalendar;
    }

    /**
     *
     * @param holidayCalendar
     * The HolidayCalendar
     */
    public void setHolidayCalendar(List<HolidayCalendar> holidayCalendar) {
        this.holidayCalendar = holidayCalendar;
    }

    /**
     *
     * @return
     * The rotaMO
     */
    public RotaMO getRotaMO() {
        return rotaMO;
    }

    /**
     *
     * @param rotaMO
     * The RotaMO
     */
    public void setRotaMO(RotaMO rotaMO) {
        this.rotaMO = rotaMO;
    }

    /**
     *
     * @return
     * The rankMaster
     */
    public List<RankMaster> getRankMaster() {
        return rankMaster;
    }

    /**
     *
     * @param rankMaster
     * The RankMaster
     */
    public void setRankMaster(List<RankMaster> rankMaster) {
        this.rankMaster = rankMaster;
    }

}
