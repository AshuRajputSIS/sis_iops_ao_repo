package com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskCountMO;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMUnitAtRiskInsertion extends SISAITrackingDB {

    private SQLiteDatabase sqlite=null;
    private boolean isInsertedSuccessfully;

    public FCMUnitAtRiskInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertUnitAtRisk(List<RiskDetailModelMO> riskDetailModelMOList, UnitRiskCountMO unitRiskCount) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {
                    sqlite.beginTransaction();
               /* IOPSApplication.getInstance().getDeletionStatementDBInstance().
                        deleteTable(TableNameAndColumnStatement.UNIT_RISK_TABLE,sqlite);
                IOPSApplication.getInstance().getDeletionStatementDBInstance().
                        deleteTable(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE,sqlite);*/

               if(riskDetailModelMOList != null && riskDetailModelMOList.size() != 0)
               {
                   for(RiskDetailModelMO riskDetailModelMO : riskDetailModelMOList){
                       if(!isUnitRiskCompleted(riskDetailModelMO.getUnitRiskId(),TableNameAndColumnStatement.UNIT_RISK_ID,
                               TableNameAndColumnStatement.UNIT_RISK_STATUS_ID,
                               TableNameAndColumnStatement.UNIT_RISK_TABLE,sqlite)){
                           IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                   deleteBySpecificId(TableNameAndColumnStatement.UNIT_RISK_TABLE,
                                           TableNameAndColumnStatement.UNIT_RISK_ID,2,sqlite);
                           unitAtRiskModel(riskDetailModelMO,unitRiskCount);

                       }
                       for(ActionPlanMO actionPlanMO : riskDetailModelMO.getActionPlans()){
                           if(!isUnitRiskCompleted(riskDetailModelMO.getUnitRiskId(),TableNameAndColumnStatement
                                           .UNIT_RISK_ACTION_PLAN_ID,
                                   TableNameAndColumnStatement.IS_COMPLETED,
                                   TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE,sqlite)){

                               IOPSApplication.getInstance().getDeletionStatementDBInstance().
                                       deleteBySpecificId(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE,
                                               TableNameAndColumnStatement.UNIT_RISK_ID,2,sqlite);
                               unitRiskActionPlan(actionPlanMO,riskDetailModelMO.getUnitRiskId());
                           }
                       }

                   }
               }


                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void unitAtRiskModel(RiskDetailModelMO reRiskDetailModelMO, UnitRiskCountMO unitRiskCount) {
        ContentValues unitRiskValue = new ContentValues();
        unitRiskValue.put(TableNameAndColumnStatement.UNIT_RISK_ID,
                    reRiskDetailModelMO.getUnitRiskId());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_ID,
                    reRiskDetailModelMO.getUnitId());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_NAME,
                    reRiskDetailModelMO.getUnitName());
            unitRiskValue.put(TableNameAndColumnStatement.CURRENT_MONTH,
                    reRiskDetailModelMO.getCurrentMonth());
            unitRiskValue.put(TableNameAndColumnStatement.CURRENT_YEAR,
                    reRiskDetailModelMO.getCurrentYear());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_RISK_STATUS_ID,
                    reRiskDetailModelMO.getUnitRiskStatusId());
            unitRiskValue.put(TableNameAndColumnStatement.CLOSED_AT,
                    reRiskDetailModelMO.getCompletionDate());
            unitRiskValue.put(TableNameAndColumnStatement.RISK_COUNT,
                    unitRiskCount.getRiskCount());
            unitRiskValue.put(TableNameAndColumnStatement.RISK_COUNT_BY_AI,
                    unitRiskCount.getRiskCountByAreaInspector());
            unitRiskValue.put(TableNameAndColumnStatement.ACTION_COUNT,
                    unitRiskCount.getActionCount());
            unitRiskValue.put(TableNameAndColumnStatement.ACTION_PENDING_COUNT,
                    unitRiskCount.getActionPendingCount());
            unitRiskValue.put(TableNameAndColumnStatement.TOTAL_ITEM_COUNT,
                    reRiskDetailModelMO.getTotalItems());

            sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_RISK_TABLE, null, unitRiskValue);
        }

    private void unitRiskActionPlan(ActionPlanMO actionPlanMO, int unitRiskId){
        ContentValues actionPlanValue = new ContentValues();

        actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID,
                actionPlanMO.getActionPlanId());
        actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ID,
                unitRiskId);
        actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_REASON,
                actionPlanMO.getReason());
        actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN,
                actionPlanMO.getActionPlan());
        actionPlanValue.put(TableNameAndColumnStatement.ACTION_PLAN_DATE,
                actionPlanMO.getPlanDate());
        actionPlanValue.put(TableNameAndColumnStatement.ASSIGNED_ID,
                actionPlanMO.getAssignTo());
        actionPlanValue.put(TableNameAndColumnStatement.ASSIGNED_TO,
                actionPlanMO.getAssignToName());
        actionPlanValue.put(TableNameAndColumnStatement.COMPLETION_DATE,
                actionPlanMO.getCompletedDate());
        actionPlanValue.put(TableNameAndColumnStatement.IS_COMPLETED,
                actionPlanMO.getIsCompleted());
        actionPlanValue.put(TableNameAndColumnStatement.REMARKS,
                actionPlanMO.getRemarks());
        actionPlanValue.put(TableNameAndColumnStatement.CLOSED_DATE,
                actionPlanMO.getClosedDate());
        actionPlanValue.put(TableNameAndColumnStatement.IS_SYNCED,
                1);
        sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE, null, actionPlanValue);

    }

    public boolean isUnitRiskCompleted(int id, String columnName1, String columnName2,
                                       String tableName,SQLiteDatabase sqLiteDatabase){
        boolean isUnitRiskCompleted  = false;
        String  query = "";
        if(tableName.equalsIgnoreCase(TableNameAndColumnStatement.UNIT_RISK_TABLE)){
            query = " select   count(id)   from " +
                    tableName +
                    " where "+ columnName1 +
                    " =  "+ id +" and "+ columnName2 + " = "+ 2;
        }
        else{
            query = " select   count(id)   from " +
                    tableName +
                    " where "+ columnName1 +
                    " =  "+ id +" and "+ columnName2 + " = "+ 1;
        }
        Timber.d("UNITATRISK %s", query);
        try {

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if(cursor.moveToFirst()) {
                    isUnitRiskCompleted = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }

        return isUnitRiskCompleted;
    }

}
