package com.sisindia.ai.android.myunits;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;

/**
 * Created by shushruth on 27/3/16.
 */
public class PostNameDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.post_dialog, container, false);
        Toolbar mToolBar = (Toolbar) rootView.findViewById(R.id.toolbar_post);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        //actionBar.setSupportActionBar(mToolBar);

        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle(R.string.post_name);
        getDialog().setTitle("");


        return rootView;
    }
}
