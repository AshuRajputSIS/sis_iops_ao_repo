package com.sisindia.ai.android.network.response;

/**
 * Created by Durga Prasad on 15-03-2016.
 */
public class ImageStatusResponse {


    private int status;
    private String statusMessage;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
