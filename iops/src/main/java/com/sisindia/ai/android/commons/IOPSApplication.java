package com.sisindia.ai.android.commons;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.log.CustomLogger;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;
import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.sisindia.ai.android.database.ActivityLogDB;
import com.sisindia.ai.android.database.DeletionStatementDB;
import com.sisindia.ai.android.database.DutyAttendanceDB;
import com.sisindia.ai.android.database.InsertionStatementDB;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.SelectionStatementDB;
import com.sisindia.ai.android.database.UpdateStatementDB;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemInsertionStatementDB;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemUpdateStatementDB;
import com.sisindia.ai.android.database.issueDTO.IssueLevelInsertionStatemeentDB;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.database.issueDTO.IssueLevelUpdateStatementDB;
import com.sisindia.ai.android.database.mybarrackDTO.MyBarrackStatementDB;
import com.sisindia.ai.android.database.myperformanceDTO.MyPerformanceStatementsDB;
import com.sisindia.ai.android.database.notificationDTO.NotificationStatementDB;
import com.sisindia.ai.android.database.notificationDTO.NotificationsReceiptDB;
import com.sisindia.ai.android.database.recuirtmentDTO.RecuritmentLevelStatementDB;
import com.sisindia.ai.android.database.rotaDTO.RotaLevelInsertionStatementDB;
import com.sisindia.ai.android.database.rotaDTO.RotaLevelSelectionStatementDB;
import com.sisindia.ai.android.database.rotaDTO.RotaLevelUpdateStatementDB;
import com.sisindia.ai.android.database.settingsDTO.SettingsLevelSelectionStatmentDB;
import com.sisindia.ai.android.database.settingsDTO.SettingsLevelUpdateStatementDB;
import com.sisindia.ai.android.database.tableSyncCountDTO.TableSyncCount;
import com.sisindia.ai.android.database.unitDTO.UnitLevelInsertionStatementDB;
import com.sisindia.ai.android.database.unitDTO.UnitLevelSelectionQuery;
import com.sisindia.ai.android.database.unitDTO.UnitPostUpdateStatementDB;
import com.sisindia.ai.android.database.unitRaisingDTO.UnitRaisingInsertionStatementDB;
import com.sisindia.ai.android.database.unitRaisingDTO.UnitRaisingSelectStatementDB;
import com.sisindia.ai.android.database.unitatriskpoaDTO.UARDeletionStatementDB;
import com.sisindia.ai.android.database.unitatriskpoaDTO.UARInsertionStatementDB;
import com.sisindia.ai.android.database.unitatriskpoaDTO.UARSelectionStatementDB;
import com.sisindia.ai.android.database.unitatriskpoaDTO.UARUpdateStatementDB;
import com.sisindia.ai.android.fcmnotification.fcmAKR.FcmAKRModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave.FCMEmployeeLeaveInsertion;
import com.sisindia.ai.android.fcmnotification.fcmHolidayAndLeave.FCMHolidayModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmIssues.FCMIssueStausUpdate;
import com.sisindia.ai.android.fcmnotification.fcmIssues.FCMIssuesModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk.FCMUnitAtRiskActionPlan;
import com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk.FCMUnitAtRiskInsertion;
import com.sisindia.ai.android.fcmnotification.fcmbarrack.FCMBarrackModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmbarrack.FCMBarrackStrengthInsertion;
import com.sisindia.ai.android.fcmnotification.fcmrota.FCMRotaModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitContactModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitModelInsertion;
import com.sisindia.ai.android.fcmnotification.fcmunits.FCMUnitStrengthModelInsertion;
import com.sisindia.ai.android.fcmnotification.job.MyJobService;
import com.sisindia.ai.android.syncadpter.SyncAdapterInitialization;
import com.sisindia.ai.android.utils.logger.MyLogger;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by Saruchi on 29-03-2016.
 */
public class IOPSApplication extends MultiDexApplication {

    private static IOPSApplication instance;
    private SISAITrackingDB dbInstance;
    private SyncAdapterInitialization syncAdapterInstance;
    private static Gson gsonInstance;
    private FCMIssuesModelInsertion fcmIssuesModelInsertion;
    private FcmAKRModelInsertion fcmAKRModelInsertion;
    private FCMIssueStausUpdate fcmIssueStausUpdate;
    private FCMRotaModelInsertion fcmRotaModelInsertionDBInstance;
    private FCMUnitModelInsertion fcmUnitModelInsertionDBInstance;
    private FCMBarrackModelInsertion fcmBarrackModelInsertion;
    private FCMBarrackStrengthInsertion fcmBarrackStrengthInsertion;
    private FCMUnitStrengthModelInsertion fcmUnitStrengthModelInsertion;
    private FCMUnitContactModelInsertion fcmUnitContactModelInsertion;
    private FCMUnitAtRiskInsertion fcmUnitAtRiskInsertion;
    private FCMUnitAtRiskActionPlan fcmUnitAtRiskActionPlan;
    private FCMEmployeeLeaveInsertion fcmEmployeeLeaveInsertion;
    private FCMHolidayModelInsertion fcmHolidayModelInsertion;
    private UnitLevelSelectionQuery unitLevelSelectionQuery;
    private InsertionStatementDB insertionStatementDBInstance;
    private RotaLevelInsertionStatementDB rotaLevelInsertionDBInstance;
    private UARInsertionStatementDB uarInsertionStatementDBInstance;
    private UARSelectionStatementDB uarSelectionStatementDBInstance;
    private UARDeletionStatementDB uarDeletionStatementDBInstance;
    private UARUpdateStatementDB uarUpdateStatementDBInstance;
    private RotaLevelSelectionStatementDB rotaLevelSelectionDBInstance;
    private RotaLevelUpdateStatementDB rotaLevelUpdateDBInstance;
    private IssueLevelSelectionStatementDB issueLevelSelectionDBInstance;
    private UnitLevelInsertionStatementDB unitLevelInsertionDBInstance;
    private SelectionStatementDB selectionStatementDBInstance;
    private DeletionStatementDB deletionStatementDBInstance;
    private UpdateStatementDB updateStatementDBInstance;
    private MyPerformanceStatementsDB myPerformanceStatementsDB;
    private IssueLevelInsertionStatemeentDB issueLevelInsertionStatemeentDB;
    private MyBarrackStatementDB myBarrackStatementDBInstance;
    private IssueLevelUpdateStatementDB issueLevelUpdateStatementDB;
    private RecuritmentLevelStatementDB mRecuritmentLevelUpdateStatementDB;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private KitItemInsertionStatementDB kitItemInsertionStatementDB;
    private KitItemUpdateStatementDB kitItemUpdateStatementDB;
    private SettingsLevelUpdateStatementDB settingsLevelUpdateStatementDB;
    private SettingsLevelSelectionStatmentDB mSettingsLevelSelectionStatmentDB;
    private NotificationStatementDB notificationStatementDB;
    private NotificationsReceiptDB notificationsReceiptDB;
    private static TableSyncCount mTableSyncCount;
    private DutyAttendanceDB dutyAttendanceDB;
    private ActivityLogDB activityLogDB;
    private JobManager jobManager;
    private UnitRaisingSelectStatementDB unitRaisingSelectStatementDB;
    private UnitRaisingInsertionStatementDB unitRaisingInsertionStatementDB;
    private UnitPostUpdateStatementDB unitPostUpdateStatementDB;


    public static IOPSApplication getInstance() {
        return instance;
    }

    public SISAITrackingDB getSISAITrackingDBInstance() {
        if (dbInstance == null) {
            return new SISAITrackingDB(this);
        }
        return dbInstance;
    }

    public UnitLevelSelectionQuery getUnitLevelSelectionQueryInstance() {
        if (unitLevelSelectionQuery == null) {
            return new UnitLevelSelectionQuery(this);
        }
        return unitLevelSelectionQuery;
    }

    public InsertionStatementDB getInsertionInstance() {
        if (this.insertionStatementDBInstance == null) {
            return new InsertionStatementDB(this);
        }
        return insertionStatementDBInstance;
    }

    public FCMRotaModelInsertion getFcmRotaModelInsertionDBInstance() {
        if (this.fcmRotaModelInsertionDBInstance == null) {
            return new FCMRotaModelInsertion(this);
        }
        return fcmRotaModelInsertionDBInstance;
    }

    public FCMUnitModelInsertion getFcmUnitModelInsertion() {
        if (this.fcmUnitModelInsertionDBInstance == null) {
            return new FCMUnitModelInsertion(this);
        }
        return fcmUnitModelInsertionDBInstance;
    }

    public FCMBarrackStrengthInsertion getFcmBarrackStrengthInsertion() {
        if (this.fcmBarrackStrengthInsertion == null) {
            return new FCMBarrackStrengthInsertion(this);
        }
        return fcmBarrackStrengthInsertion;
    }

    public FCMBarrackModelInsertion getFcmBarrackModelInsertion() {
        if (this.fcmBarrackModelInsertion == null) {
            return new FCMBarrackModelInsertion(this);
        }
        return fcmBarrackModelInsertion;
    }

    public FCMUnitStrengthModelInsertion getFcmUnitStrengthModelInsertion() {
        if (this.fcmUnitStrengthModelInsertion == null) {
            return new FCMUnitStrengthModelInsertion(this);
        }
        return fcmUnitStrengthModelInsertion;
    }

    public FCMUnitContactModelInsertion getFcmUnitContactModelInsertion() {
        if (this.fcmUnitContactModelInsertion == null) {
            return new FCMUnitContactModelInsertion(this);
        }
        return fcmUnitContactModelInsertion;
    }

    public FCMHolidayModelInsertion getFcmHolidayModelInsertion() {
        if (this.fcmHolidayModelInsertion == null) {
            return new FCMHolidayModelInsertion(this);
        }
        return fcmHolidayModelInsertion;
    }

    public FCMEmployeeLeaveInsertion getFcmEmployeeLeaveInsertion() {
        if (this.fcmEmployeeLeaveInsertion == null) {
            return new FCMEmployeeLeaveInsertion(this);
        }
        return fcmEmployeeLeaveInsertion;
    }

    public FCMUnitAtRiskInsertion getFcmUnitAtRiskInsertion() {
        if (this.fcmUnitAtRiskInsertion == null) {
            return new FCMUnitAtRiskInsertion(this);
        }
        return fcmUnitAtRiskInsertion;
    }

    public FCMUnitAtRiskActionPlan getFcmUnitAtRiskActionPlan() {
        if (this.fcmUnitAtRiskActionPlan == null) {
            return new FCMUnitAtRiskActionPlan(this);
        }
        return fcmUnitAtRiskActionPlan;
    }

    public RotaLevelInsertionStatementDB getRotaLevelInsertionInstance() {
        if (this.rotaLevelInsertionDBInstance == null) {
            return new RotaLevelInsertionStatementDB(this);
        }
        return rotaLevelInsertionDBInstance;
    }

    public UARInsertionStatementDB getUARInsertionInstance() {
        if (this.uarInsertionStatementDBInstance == null) {
            return new UARInsertionStatementDB(this);
        }
        return uarInsertionStatementDBInstance;
    }

    public UARSelectionStatementDB getUARSelectionInstance() {
        if (this.uarSelectionStatementDBInstance == null) {
            return new UARSelectionStatementDB(this);
        }
        return uarSelectionStatementDBInstance;
    }

    public UARDeletionStatementDB getUarDeletionInstance() {
        if (this.uarDeletionStatementDBInstance == null) {
            return new UARDeletionStatementDB(this);
        }
        return uarDeletionStatementDBInstance;
    }

    public UARUpdateStatementDB getUARUpdateInstance() {
        if (this.uarUpdateStatementDBInstance == null) {
            return new UARUpdateStatementDB(this);
        }
        return uarUpdateStatementDBInstance;
    }


    public RotaLevelSelectionStatementDB getRotaLevelSelectionInstance() {
        if (this.rotaLevelSelectionDBInstance == null) {
            return new RotaLevelSelectionStatementDB(this);
        }
        return rotaLevelSelectionDBInstance;
    }

    public RotaLevelUpdateStatementDB getRotaLevelUpdateDBInstance() {
        if (this.rotaLevelUpdateDBInstance == null) {
            return new RotaLevelUpdateStatementDB(this);
        }
        return rotaLevelUpdateDBInstance;
    }

    public IssueLevelSelectionStatementDB getIssueSelectionDBInstance() {
        if (this.issueLevelSelectionDBInstance == null) {
            return new IssueLevelSelectionStatementDB(this);
        }
        return issueLevelSelectionDBInstance;
    }


    public IssueLevelUpdateStatementDB getIssueUpdateDBInstance() {
        if (this.issueLevelUpdateStatementDB == null) {
            return new IssueLevelUpdateStatementDB(this);
        }
        return issueLevelUpdateStatementDB;
    }

    public UnitLevelInsertionStatementDB getUnitLevelInsertionDBInstance() {
        if (this.unitLevelSelectionQuery == null) {
            return new UnitLevelInsertionStatementDB(this);
        }
        return unitLevelInsertionDBInstance;
    }

    public TableSyncCount getTableSyncCount() {
        if (this.mTableSyncCount == null) {
            return new TableSyncCount(this);
        }
        return mTableSyncCount;
    }


    public NotificationStatementDB getNotificationStatementDB() {
        if (this.notificationStatementDB == null) {
            return new NotificationStatementDB(this);
        }
        return notificationStatementDB;
    }


    public UnitRaisingSelectStatementDB getUnitRaisingSelectStatementDB() {
        if (this.unitRaisingSelectStatementDB == null) {
            return new UnitRaisingSelectStatementDB(this);
        }
        return unitRaisingSelectStatementDB;
    }

    public UnitRaisingInsertionStatementDB getUnitRaisingInsertionStatementDB() {
        if (this.unitRaisingInsertionStatementDB == null) {
            return new UnitRaisingInsertionStatementDB(this);
        }
        return unitRaisingInsertionStatementDB;
    }

    public NotificationsReceiptDB getNotificationsReceiptDB() {
        if (this.notificationsReceiptDB == null) {
            return new NotificationsReceiptDB(this);
        }
        return notificationsReceiptDB;
    }


    public SelectionStatementDB getSelectionStatementDBInstance() {
        if (this.selectionStatementDBInstance == null) {
            return new SelectionStatementDB(this);
        }
        return selectionStatementDBInstance;
    }

    public DeletionStatementDB getDeletionStatementDBInstance() {
        if (this.deletionStatementDBInstance == null) {
            return new DeletionStatementDB(this);
        }
        return deletionStatementDBInstance;
    }

    public MyBarrackStatementDB getMyBarrackStatementDB() {
        if (this.myBarrackStatementDBInstance == null) {
            return new MyBarrackStatementDB(this);
        }
        return myBarrackStatementDBInstance;
    }

    public UpdateStatementDB getUpdateStatementDBInstance() {
        if (this.updateStatementDBInstance == null) {
            return new UpdateStatementDB(this);
        }
        return updateStatementDBInstance;
    }

    public SyncAdapterInitialization getSyncAdapterInstance() {
        if (this.syncAdapterInstance == null) {
            return new SyncAdapterInitialization(this);
        }
        return syncAdapterInstance;
    }

    public static Gson getGsonInstance() {
        return gsonInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public MyPerformanceStatementsDB getMyPerformanceStatementsDB() {


        if (this.myPerformanceStatementsDB == null) {
            return new MyPerformanceStatementsDB(this);
        }
        return myPerformanceStatementsDB;

    }


    public ActivityLogDB getActivityLogDB() {


        if (this.activityLogDB == null) {
            return new ActivityLogDB(this);
        }
        return activityLogDB;

    }

    public IssueLevelInsertionStatemeentDB getIssueInsertionDBInstance() {
        if (this.issueLevelInsertionStatemeentDB == null) {
            return new IssueLevelInsertionStatemeentDB(this);
        }
        return issueLevelInsertionStatemeentDB;
    }

    public IssueLevelUpdateStatementDB getIssueUpdateStatementDB() {
        if (this.issueLevelUpdateStatementDB == null) {
            return new IssueLevelUpdateStatementDB(this);
        }
        return issueLevelUpdateStatementDB;
    }

    public RecuritmentLevelStatementDB getRecurimentUpdateStatementDB() {
        if (this.mRecuritmentLevelUpdateStatementDB == null) {
            return new RecuritmentLevelStatementDB(this);
        }
        return mRecuritmentLevelUpdateStatementDB;
    }

    public KitItemSelectionStatementDB getKitItemSelectionStatementDB() {
        if (this.kitItemSelectionStatementDB == null) {
            return new KitItemSelectionStatementDB(this);
        }
        return kitItemSelectionStatementDB;
    }

    public KitItemInsertionStatementDB getKitItemInsertionStatementDB() {
        if (this.kitItemInsertionStatementDB == null) {
            return new KitItemInsertionStatementDB(this);
        }
        return kitItemInsertionStatementDB;
    }

    public KitItemUpdateStatementDB getKitItemUpdateStatementDB() {
        if (this.kitItemUpdateStatementDB == null) {
            return new KitItemUpdateStatementDB(this);
        }
        return kitItemUpdateStatementDB;
    }


    public KitItemSelectionStatementDB getKitItemRequestSelectionStatementDB() {
        if (this.issueLevelInsertionStatemeentDB == null) {
            return new KitItemSelectionStatementDB(this);
        }
        return kitItemSelectionStatementDB;
    }

    public SettingsLevelUpdateStatementDB getSettingsLevelUpdateStatementDB() {
        if (this.settingsLevelUpdateStatementDB == null) {
            return new SettingsLevelUpdateStatementDB(this);
        }
        return settingsLevelUpdateStatementDB;
    }

    public SettingsLevelSelectionStatmentDB getSettingsLevelSelectionStatmentDB() {
        if (this.mSettingsLevelSelectionStatmentDB == null) {
            return new SettingsLevelSelectionStatmentDB(this);
        }
        return mSettingsLevelSelectionStatmentDB;
    }

    public DutyAttendanceDB getDutyAttendanceDB() {
        if (this.dutyAttendanceDB == null) {
            return new DutyAttendanceDB(this);
        }
        return dutyAttendanceDB;
    }

    public FCMIssuesModelInsertion getFcmIssuesModelInsertionDBInstance() {
        if (this.fcmIssuesModelInsertion == null) {
            return new FCMIssuesModelInsertion(this);
        }
        return fcmIssuesModelInsertion;
    }

    public FCMIssueStausUpdate getFcmIssueStausUpdateDBInstance() {
        if (this.fcmIssueStausUpdate == null) {
            return new FCMIssueStausUpdate(this);
        }
        return fcmIssueStausUpdate;
    }

    public FcmAKRModelInsertion getFcmAkrModelInsertionDBInstance() {
        if (this.fcmAKRModelInsertion == null) {
            return new FcmAKRModelInsertion(this);
        }
        return fcmAKRModelInsertion;
    }

    public UnitPostUpdateStatementDB getUnitPostUpdateStatementDB() {
        if (this.unitPostUpdateStatementDB == null) {
            return new UnitPostUpdateStatementDB(this);
        }
        return unitPostUpdateStatementDB;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        try {
            //        Fabric.with(this, new Crashlytics());
            final Fabric fabric = new Fabric.Builder(this)
                    .kits(new Crashlytics())
                    .debuggable(true)
                    .build();
            Fabric.with(fabric);
        } catch (Exception e) {
        }

        getJobManager();// ensure it is created

        //File Logger setup
        MyLogger.setup(this);

        instance = this;
        dbInstance = new SISAITrackingDB(this);
        fcmRotaModelInsertionDBInstance = new FCMRotaModelInsertion(this);
        fcmUnitModelInsertionDBInstance = new FCMUnitModelInsertion(this);
        fcmBarrackModelInsertion = new FCMBarrackModelInsertion(this);
        fcmBarrackStrengthInsertion = new FCMBarrackStrengthInsertion(this);
        fcmUnitStrengthModelInsertion = new FCMUnitStrengthModelInsertion(this);
        fcmEmployeeLeaveInsertion = new FCMEmployeeLeaveInsertion(this);
        fcmHolidayModelInsertion = new FCMHolidayModelInsertion(this);
        fcmUnitContactModelInsertion = new FCMUnitContactModelInsertion(this);
        fcmUnitAtRiskActionPlan = new FCMUnitAtRiskActionPlan(this);

        insertionStatementDBInstance = new InsertionStatementDB(this);
        rotaLevelInsertionDBInstance = new RotaLevelInsertionStatementDB(this);
        rotaLevelSelectionDBInstance = new RotaLevelSelectionStatementDB(this);
        rotaLevelUpdateDBInstance = new RotaLevelUpdateStatementDB(this);
        issueLevelSelectionDBInstance = new IssueLevelSelectionStatementDB(this);
        unitLevelInsertionDBInstance = new UnitLevelInsertionStatementDB(this);
        unitLevelSelectionQuery = new UnitLevelSelectionQuery(this);
        selectionStatementDBInstance = new SelectionStatementDB(this);
        updateStatementDBInstance = new UpdateStatementDB(this);
        deletionStatementDBInstance = new DeletionStatementDB(this);
        syncAdapterInstance = new SyncAdapterInitialization(this);
        uarInsertionStatementDBInstance = new UARInsertionStatementDB(this);
        uarSelectionStatementDBInstance = new UARSelectionStatementDB(this);
        uarDeletionStatementDBInstance = new UARDeletionStatementDB(this);
        uarUpdateStatementDBInstance = new UARUpdateStatementDB(this);
        myPerformanceStatementsDB = new MyPerformanceStatementsDB(this);
        issueLevelInsertionStatemeentDB = new IssueLevelInsertionStatemeentDB(this);
        myBarrackStatementDBInstance = new MyBarrackStatementDB(this);
        issueLevelUpdateStatementDB = new IssueLevelUpdateStatementDB(this);
        mRecuritmentLevelUpdateStatementDB = new RecuritmentLevelStatementDB(this);
        mTableSyncCount = new TableSyncCount(this);
        notificationStatementDB = new NotificationStatementDB(this);
        unitRaisingSelectStatementDB = new UnitRaisingSelectStatementDB(this);
        gsonInstance = new Gson();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());

        Timber.plant(new Timber.DebugTree());
    }

    private void configureJobManager() {
        Configuration.Builder builder = new Configuration.Builder(this)
                .customLogger(new CustomLogger() {
                    private static final String TAG = "JOBS";

                    @Override
                    public boolean isDebugEnabled() {
                        return true;
                    }

                    @Override
                    public void d(String text, Object... args) {
                        Log.d(TAG, String.format(text, args));
                    }

                    @Override
                    public void e(Throwable t, String text, Object... args) {
                        Log.e(TAG, String.format(text, args), t);
                    }

                    @Override
                    public void e(String text, Object... args) {
                        Log.e(TAG, String.format(text, args));
                    }

                    @Override
                    public void v(String text, Object... args) {

                    }
                })
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120);//wait 2 minute
        builder.scheduler(FrameworkJobSchedulerService.createSchedulerFor(this,
                MyJobService.class), true);

        jobManager = new JobManager(builder.build());
    }

    public synchronized JobManager getJobManager() {
        if (jobManager == null) {
            configureJobManager();
        }
        return jobManager;
    }


}

