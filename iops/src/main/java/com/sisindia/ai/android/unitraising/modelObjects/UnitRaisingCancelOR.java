package com.sisindia.ai.android.unitraising.modelObjects;



import com.google.gson.annotations.SerializedName;

public class UnitRaisingCancelOR {

@SerializedName("UnitId")

public Integer unitId;
@SerializedName("UnitCode")

public String unitCode;
@SerializedName("ExpectedRaisingDate")

public String expectedRaisingDate;
@SerializedName("Reason")

public String reason;
@SerializedName("Remarks")

public String remarks;
@SerializedName("CancellationDateTime")

public String cancellationDateTime;
@SerializedName("CreatedBy")

public Integer createdBy;

}