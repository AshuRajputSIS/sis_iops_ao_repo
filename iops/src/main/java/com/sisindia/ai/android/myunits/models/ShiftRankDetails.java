package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 23/6/16.
 */

public class ShiftRankDetails implements Serializable {
    @SerializedName("Id")
    public int Id;
    @SerializedName("ShiftName")
    public String ShiftName;
    @SerializedName("Description")
    public String Description ;
    @SerializedName("ShiftInTime")
    public String ShiftInTime ;
    @SerializedName("ShiftOutTime")
    public String ShiftOutTime ;
    @SerializedName("IsActive")
    public boolean IsActive;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getShiftName() {
        return ShiftName;
    }

    public void setShiftName(String shiftName) {
        ShiftName = shiftName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getShiftInTime() {
        return ShiftInTime;
    }

    public void setShiftInTime(String shiftInTime) {
        ShiftInTime = shiftInTime;
    }

    public String getShiftOutTime() {
        return ShiftOutTime;
    }

    public void setShiftOutTime(String shiftOutTime) {
        ShiftOutTime = shiftOutTime;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }
}
