package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 28/6/16.
 */

public class BillSubmissionMO {

    @SerializedName("BillHandOverTO")
    @Expose
    private String billHandOverTO;
    @SerializedName("BillAccepted")
    @Expose
    private BillAcceptedMO billAccepted;

    public String getBillingCheckList() {
        return billingCheckList;
    }

    public void setBillingCheckList(String billingCheckList) {
        this.billingCheckList = billingCheckList;
    }

    @SerializedName("BillingCheckList")
    @Expose
    private String billingCheckList ;

    /**
     *
     * @return
     * The billHandOverTO
     */
    public String getBillHandOverTO() {
        return billHandOverTO;
    }

    /**
     *
     * @param billHandOverTO
     * The BillHandOverTO
     */
    public void setBillHandOverTO(String billHandOverTO) {
        this.billHandOverTO = billHandOverTO;
    }

    /**
     *
     * @return
     * The billAccepted
     */
    public BillAcceptedMO getBillAccepted() {
        return billAccepted;
    }

    /**
     *
     * @param billAccepted
     * The BillAccepted
     */
    public void setBillAccepted(BillAcceptedMO billAccepted) {
        this.billAccepted = billAccepted;
    }


}
