package com.sisindia.ai.android.rota.nightcheck;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shushrut on 10-06-2016.
 */
public enum NightCheckEnum {
    Strength_Check(1),
    Guard_Check(2),
    Security_Risk_Observations(3);

    //Equipment_Check(7);

    private static Map map = new HashMap<>();

    static {
        for (NightCheckEnum mnightCheckValue : NightCheckEnum.values()) {
            map.put(mnightCheckValue.value, mnightCheckValue);
        }
    }

    private int value;

    NightCheckEnum(int i) {
        value = i;
    }

    public static NightCheckEnum valueOf(int positionValue) {
        return (NightCheckEnum) map.get(positionValue);
    }

    public int getValue() {
        return value;
    }
}
