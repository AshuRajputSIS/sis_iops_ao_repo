package com.sisindia.ai.android.database.unitDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.google.gson.reflect.TypeToken;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.loadconfiguration.Equipments;
import com.sisindia.ai.android.myunits.ContactMO;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitAddressLatlong;
import com.sisindia.ai.android.myunits.models.UnitBarrack;
import com.sisindia.ai.android.myunits.models.UnitBillingStatusMO;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.myunits.models.UnitList;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.network.response.UnitBoundary;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Saruchi on 27-06-2016.
 */

public class UnitLevelSelectionQuery extends SISAITrackingDB {
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public UnitLevelSelectionQuery(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    // checking the duplication post name
    public boolean duplicatePostExist(String postname, int unitid) {
        boolean isDuplicate = false;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {


                if (postname.contains("'")) {

                    postname = postname.replace("'", "''");
                }

                String selectQuery = "SELECT " + TableNameAndColumnStatement.UNIT_POST_NAME +
                        " FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE LOWER(" +
                        TableNameAndColumnStatement.UNIT_POST_NAME + ") = '" + postname + "' AND " +
                        TableNameAndColumnStatement.UNIT_ID + "= '" + unitid + "'";
                Timber.d("duplicatePostExist selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    isDuplicate = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            Timber.d("isDuplicate object  %s", isDuplicate);

            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isDuplicate;
    }

    // checking the duplication post name
    public boolean duplicateQRCodeExist(String scannedQRCode) {
        boolean isDuplicate = false;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "SELECT " + TableNameAndColumnStatement.DEVICE_NO +
                        " FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                        TableNameAndColumnStatement.DEVICE_NO + "= '" + scannedQRCode + "'";

                Timber.d("duplicateQRCOdeExist selectQuery  %s", selectQuery);

                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    isDuplicate = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            Timber.d("isDuplicate object  %s", isDuplicate);
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isDuplicate;
    }

    public ArrayList<ContactMO> getUnitContact(int UnitId) {
        ArrayList<ContactMO> clientHandShakeIRList = new ArrayList<ContactMO>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE
                + " where " + TableNameAndColumnStatement.UNIT_ID + "='" + UnitId + "'";
        Timber.d("getUnitContact selectQuery  %s", selectQuery);
        ContactMO contactMO = new ContactMO();
        contactMO.setName("Select Contact");
        clientHandShakeIRList.add(contactMO);
        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ContactMO clientHandShakeIR = new ContactMO();
                        clientHandShakeIR.setName(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)) + " " +
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME)));
                        clientHandShakeIR.setDesignation(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.DESIGNATION)));
                        clientHandShakeIR.setContactNo(cursor.getString(cursor.
                                getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        clientHandShakeIR.setUnitcontactID(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_ID)));
                        clientHandShakeIRList.add(clientHandShakeIR);
                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d("clientHandShakeIRList object  %s", IOPSApplication.getGsonInstance().toJson(clientHandShakeIRList));
        }
        return clientHandShakeIRList;
    }

    public UnitPost getPostInformation(String uniqueId, int unitId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery;
                if (unitId > 0) {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                            TableNameAndColumnStatement.UNIT_ID + " = " + unitId + " and " + TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                } else {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                            TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                }
                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    UnitPost unitPost = new UnitPost();
                    unitPost.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                    unitPost.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                    unitPost.setUnitPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                    unitPost.setGeoPoint(
                            cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) +
                                    "," +
                                    cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                    return unitPost;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return null;
    }


    public UnitPost getPostInformationUsingNfc(String uniqueId, int unitId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "";
                if (unitId > 0) {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                            TableNameAndColumnStatement.UNIT_ID + " != " + unitId + " and " + TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                } else {
                    selectQuery = " SELECT * FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                            TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                }
                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    UnitPost unitPost = new UnitPost();
                    unitPost.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                    unitPost.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                    unitPost.setUnitPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                    unitPost.setGeoPoint(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) +
                            "," + cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                    return unitPost;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return null;
    }

    public List<UnitPost> getNfcPostInformation(int unitId) {
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select * from " + TableNameAndColumnStatement.UNIT_POST_TABLE + " where "
                        + TableNameAndColumnStatement.UNIT_ID + " = " + unitId
                        + " and " + TableNameAndColumnStatement.DEVICE_NO + " <> ''";

                Timber.d("unit nfc information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                List<UnitPost> unitPostList = new ArrayList<>();
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UnitPost unitPost = new UnitPost();
                        unitPost.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        unitPost.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        unitPost.setUnitPostName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_NAME)));
                        unitPost.setGeoPoint(
                                cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)) +
                                        "," +
                                        cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        unitPostList.add(unitPost);
                    }
                    while (cursor.moveToNext());
                }
                return unitPostList;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return null;
    }

    // checking the duplication ContactExist
    public boolean duplicateContactExist(String contact, String desigation, int untiId) {
        boolean isDuplicate = false;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = " SELECT " + TableNameAndColumnStatement.PHONE_NO + " ," +
                        TableNameAndColumnStatement.DESIGNATION +
                        " FROM " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE + " WHERE " +
                        TableNameAndColumnStatement.UNIT_ID + " = '" + untiId + "' AND (" +
                        TableNameAndColumnStatement.PHONE_NO + " = '" + contact + "' OR " +
                        TableNameAndColumnStatement.DESIGNATION + " = '" + desigation + "')";
                Timber.d("duplicateContactExist selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    isDuplicate = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            Timber.d("isDuplicate object  %s", isDuplicate);
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isDuplicate;
    }

    // get list of contacts other than the selected contact in edit case to check duplicate ph or desigantion
    public ArrayList<UnitContactsMO> getListofContact(int sequenceid, int unitId) {
        ArrayList<UnitContactsMO> unitContactList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = " SELECT *" +
                        " FROM " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE + " WHERE " +
                        TableNameAndColumnStatement.ID + " !=  '" + sequenceid + "' and " +
                        TableNameAndColumnStatement.UNIT_ID + " =  '" + unitId + "'";
                Timber.d("duplicateContactExist selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UnitContactsMO unitContactsMO = new UnitContactsMO();
                        unitContactsMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        unitContactsMO.setUnitContactId(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_ID));
                        unitContactsMO.setContactFirstName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FIRST_NAME)));
                        unitContactsMO.setContactLastName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LAST_NAME)));
                        unitContactsMO.setContactNumber(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        unitContactsMO.setContactEmail(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EMAIL_ADDRESS)));
                        unitContactsMO.setClientDesignation(cursor.
                                getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESIGNATION)));
                        unitContactList.add(unitContactsMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitContactList;
    }

    public String getPendingList(String currentDateTime, int unitID) {
        String penadingTask = "";
        String query = "SELECT group_concat(CASE " +
                "WHEN t.task_type_id = 1 THEN 'Day Checking' " +
                "WHEN t.task_type_id = 2 THEN 'Night Checking' " +
                "WHEN t.task_type_id = 3 THEN 'Barrack Inspection'" +
                "WHEN t.task_type_id = 4 THEN 'Client Coordination'" +
                "WHEN t.task_type_id = 5 THEN 'Bill Submission'" +
                "WHEN t.task_type_id = 6 THEN 'Bill Collection'" +
                "WHEN t.task_type_id = 7 THEN 'Others'" +
                "ELSE ' others' END, ',') AS 'PendingTasks'" + // new temp pendingtask column will be created
                "from task t where t.task_status_id != 4 and t.task_status_id != 3 " +
                " and t.estimated_execution_start_date_time < '" + currentDateTime + "'" +
                "  and is_auto =1 and unit_id = " + unitID;
        Timber.d("getUnitContact selectQuery  %s", query);

        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(query, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        penadingTask = cursor.getString(cursor.getColumnIndex("PendingTasks"));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d("penadingTask object  %s", penadingTask);
        }
        return penadingTask;
    }

    public ArrayList<UnitBillingStatusMO> getBillingStatus(int unitId) {
        // Select All Query
        ArrayList<UnitBillingStatusMO> billingStatusArrayList = new ArrayList<>();
        String selectQuery = " SELECT *" +
                " FROM " + TableNameAndColumnStatement.TASK_TABLE + " WHERE " +
                TableNameAndColumnStatement.TASK_TYPE_ID + " in (5 , 6) and " +
                TableNameAndColumnStatement.UNIT_ID + " =  '" + unitId + "'";
        Timber.d("getBiillingStatus selectQuery  %s", selectQuery);

        Cursor billcursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            billcursor = db.rawQuery(selectQuery, null);
            if (billcursor != null && !billcursor.isClosed()) {
                // looping through all rows and adding to list
                if (billcursor.moveToFirst()) {
                    do {
                        UnitBillingStatusMO unitBillingStatusMO = new UnitBillingStatusMO();
                        unitBillingStatusMO.setTaskStatusId(billcursor.getInt(billcursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                        unitBillingStatusMO.setTaskTypeId(billcursor.getInt(billcursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                        unitBillingStatusMO.setEstimatedTaskExecutionStartTime(billcursor.getString(billcursor.getColumnIndex(
                                TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        unitBillingStatusMO.setEstimatedTaskExecutionEndTime(billcursor.getString(billcursor.
                                getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                        unitBillingStatusMO.setActualTaskExecutionStartTime(billcursor.getString(billcursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME)));
                        unitBillingStatusMO.setActualTaskExecutionEndTime(billcursor.getString(billcursor.
                                getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME)));
                        unitCollectionWagesDate(unitBillingStatusMO, unitId, billingStatusArrayList);
                    } while (billcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (billcursor != null) {
                billcursor.close();
            }
            Timber.d("getBiillingStatus object  %s", IOPSApplication.getGsonInstance().toJson(billingStatusArrayList));

        }
        return billingStatusArrayList;
    }

    private void unitCollectionWagesDate(UnitBillingStatusMO unitBillingStatusMO, int unitId, ArrayList<UnitBillingStatusMO> billingStatusArrayList) {
        String selectQuery = " SELECT " + TableNameAndColumnStatement.BILL_COLLECTION_DATE + " , " +
                TableNameAndColumnStatement.WAGE_DATE +
                " FROM " + TableNameAndColumnStatement.UNIT_TABLE + " WHERE " +
                TableNameAndColumnStatement.UNIT_ID + " =  '" + unitId + "'";
        Timber.d("unitCollectionWagesDate selectQuery  %s", selectQuery);

        Cursor unitcursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            unitcursor = db.rawQuery(selectQuery, null);

            if (unitcursor != null && !unitcursor.isClosed()) {

                // looping through all rows and adding to list
                if (unitcursor.moveToFirst()) {
                    do {
                        unitBillingStatusMO.setBillCollectionDate(unitcursor.getString(unitcursor.
                                getColumnIndex(TableNameAndColumnStatement.BILL_COLLECTION_DATE)));
                        unitBillingStatusMO.setWageDate(unitcursor.getString(unitcursor.
                                getColumnIndex(TableNameAndColumnStatement.WAGE_DATE)));

                        billingStatusArrayList.add(unitBillingStatusMO);

                    } while (unitcursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (unitcursor != null) {
                unitcursor.close();
                Timber.d("unitCollectionWagesDate object  %s", IOPSApplication.getGsonInstance().toJson(billingStatusArrayList));
            }
        }
    }

    public String getWageDate(int unitID) {
        String dateStr = "";
        String selectQuery = " SELECT " + TableNameAndColumnStatement.WAGE_DATE +
                " FROM " + TableNameAndColumnStatement.UNIT_TABLE + " WHERE " +
                TableNameAndColumnStatement.UNIT_ID + " =  '" + unitID + "'";
        Timber.d("unitCollectionWagesDate selectQuery  %s", selectQuery);

        Cursor unitcursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            unitcursor = db.rawQuery(selectQuery, null);

            if (unitcursor != null && !unitcursor.isClosed()) {

                // looping through all rows and adding to list
                if (unitcursor.moveToFirst()) {
                    do {
                        dateStr = unitcursor.getString(unitcursor.getColumnIndex(TableNameAndColumnStatement.WAGE_DATE));

                    } while (unitcursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (unitcursor != null) {
                unitcursor.close();
                Timber.d("taskTypeId object  %s", dateStr);
            }
        }
        return dateStr;
    }

    public int getBillSubmissionStatus(int unitID) {
        int taskTypeId = 0;
        String selectQuery = " SELECT " + TableNameAndColumnStatement.TASK_STATUS_ID +
                " FROM " + TableNameAndColumnStatement.TASK_TABLE + " WHERE " +
                TableNameAndColumnStatement.TASK_TYPE_ID + "= 5  and " +
                TableNameAndColumnStatement.UNIT_ID + " =  '" + unitID + "'";
        Timber.d("unitCollectionWagesDate selectQuery  %s", selectQuery);

        Cursor unitcursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            unitcursor = db.rawQuery(selectQuery, null);

            if (unitcursor != null && !unitcursor.isClosed()) {

                // looping through all rows and adding to list
                if (unitcursor.moveToFirst()) {
                    do {
                        taskTypeId = unitcursor.getInt(unitcursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));

                    } while (unitcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (unitcursor != null) {
                unitcursor.close();
                Timber.d("taskTypeId object  %s", taskTypeId);

            }
        }
        return taskTypeId;
    }

    public UnitAddressLatlong getUnitLatLon(int unitID) {
        UnitAddressLatlong unitPostGeoPoint = new UnitAddressLatlong();
        String selectQuery = " SELECT " + TableNameAndColumnStatement.GEO_LATITUDE + " , " +
                TableNameAndColumnStatement.GEO_LONGITUDE +
                " FROM " + TableNameAndColumnStatement.UNIT_POST_TABLE + " WHERE " +
                TableNameAndColumnStatement.IS_MAIN_GATE + "= 1  and " +
                TableNameAndColumnStatement.UNIT_ID + " =  '" + unitID + "'";
        Timber.d("getUnitLatLon selectQuery  %s", selectQuery);

        Cursor unitcursor = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            unitcursor = db.rawQuery(selectQuery, null);
            if (unitcursor != null && !unitcursor.isClosed()) {
                // looping through all rows and adding to list
                if (unitcursor.moveToFirst()) {
                    do {
                        unitPostGeoPoint.setLatitude(unitcursor.getDouble(unitcursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        unitPostGeoPoint.setLongitude(unitcursor.getDouble(unitcursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                    } while (unitcursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (unitcursor != null) {
                unitcursor.close();
            }
        }
        return unitPostGeoPoint;
    }

    public List<UnitList> getListOfUnitNameByBarrackId(Integer barrackId) {
        List<UnitList> unitLists = new ArrayList<>();
        String selectQuery = "select u." + TableNameAndColumnStatement.UNIT_ID + ",u."
                + TableNameAndColumnStatement.UNIT_NAME + ",ub."
                + TableNameAndColumnStatement.BARRACK_ID
                + " from " + TableNameAndColumnStatement.UNIT_TABLE
                + " u inner join " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE
                + " ub on u." + TableNameAndColumnStatement.UNIT_ID +
                "= ub." + TableNameAndColumnStatement.UNIT_ID
                + " where ub." + TableNameAndColumnStatement.BARRACK_ID + "=" + barrackId + "";
        Timber.d("getUnitLatLon selectQuery  %s", selectQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitList unitList = new UnitList();
                        unitList.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        unitList.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        unitLists.add(unitList);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitLists;
    }

    public int getBillCollectionDays(int unitId) {
        int days = 0;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = " SELECT " + TableNameAndColumnStatement.BILL_COLLECTION_DATE +
                        " FROM " + TableNameAndColumnStatement.UNIT_TABLE + " WHERE " +
                        TableNameAndColumnStatement.UNIT_ID + " = " + unitId;
                Timber.d("BillCollectionDay selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    days = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BILL_COLLECTION_DATE));
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            Timber.d("days object  %s", days);
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return days;
    }

    public List<LookupModel> fetchFilterList(String lookupName) {
        List<LookupModel> lookupModelList = new ArrayList<>();
        String selectQuery = " SELECT * " +
                " FROM " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE + " WHERE " +
                TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " = '" + lookupName + "'";
        Timber.d("fetchFilterList selectQuery  %s", selectQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        LookupModel lookupModel = new LookupModel();

                        lookupModel.setLookupIdentifier(cursor.getInt(cursor.
                                getColumnIndex(TableNameAndColumnStatement.LOOKUP_IDENTIFIER)));
                        lookupModel.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_NAME)));
                        lookupModel.setLookupTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LOOKUP_TYPE_ID)));
                        lookupModelList.add(lookupModel);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return lookupModelList;
    }

    public ArrayList<MyUnitHomeMO> getFilterUnitDetails(String ids) {
        int taskStatusId = 1;
        ArrayList<MyUnitHomeMO> myUnitsList = new ArrayList<MyUnitHomeMO>();

        String query = "select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                "t." + TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME + "," +
                "u." + TableNameAndColumnStatement.BILLING_MODE + "," +
                "u." + TableNameAndColumnStatement.GEO_LATITUDE + "," +
                "u." + TableNameAndColumnStatement.GEO_LONGITUDE + "," +
                "u." + TableNameAndColumnStatement.UNIT_BOUNDARY + "," +
                "u." + TableNameAndColumnStatement.COLLECTION_STATUS + "," +
                "u." + TableNameAndColumnStatement.WAGE_DATE + "," +
                "u." + TableNameAndColumnStatement.BILLING_DATE + "," +
                "ub." + TableNameAndColumnStatement.BARRACK_ID + "," +
                "u." + TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID + "," +
                " min( " + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME
                + " ) as upcoming_activity ," +
                " u." + TableNameAndColumnStatement.BILLING_DATE +
                " , ub. " + TableNameAndColumnStatement.BARRACK_ID +
                " from " + TableNameAndColumnStatement.UNIT_TABLE +
                "  u left join " + TableNameAndColumnStatement.TASK_TABLE +
                " t on t." + TableNameAndColumnStatement.UNIT_ID +
                " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                " and t." + TableNameAndColumnStatement.TASK_STATUS_ID + " in (1, 2)" +
                " left join " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE +
                " ub on ub." + TableNameAndColumnStatement.UNIT_ID + " = " +
                " u." + TableNameAndColumnStatement.UNIT_ID +
                " where u." + TableNameAndColumnStatement.UNIT_ID + " IN (" + ids + ")" +
                " group by " + " u." + TableNameAndColumnStatement.UNIT_ID;

        Timber.i("UnitDetails %s", query);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            String billingStatus = "";

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        MyUnitHomeMO myUnitHomeMO = new MyUnitHomeMO();
                        myUnitHomeMO.setBillAuthoritativeUnit(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID)));
                        myUnitHomeMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        myUnitHomeMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        String collectonDaysStr = getCollectionDays(myUnitHomeMO.getUnitId());
                        if (collectonDaysStr != null & collectonDaysStr.length() != 0) {
                            int collectonDays = Integer.valueOf(collectonDaysStr);
                            if (collectonDays == 0) { //System.out.println("Number is equal to zero");
                                myUnitHomeMO.setCollectionStaus(dateTimeFormatConversionUtil.getCurrentDate()); // zero
                            } else if (collectonDays > 0) { //System.out.println("Number is positive");
                                myUnitHomeMO.setCollectionStaus(collectonDays + " days due"); // positive
                            } else {
                                collectonDays = Math.abs(collectonDays); // coverting negative to positive
                                myUnitHomeMO.setCollectionStaus(dateTimeFormatConversionUtil.addDaysToCurrentDate(collectonDays));
                            }
                        } else {
                            myUnitHomeMO.setCollectionStaus("NA");
                        }

                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.WAGE_DATE)) == null)
                            myUnitHomeMO.setWagesStatus("NA");
                        else
                            myUnitHomeMO.setWagesStatus(dateTimeFormatConversionUtil.dayToCompleteDate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.WAGE_DATE))));

                        // setting the unit Lat & Lon
                        UnitAddressLatlong latlon = new UnitAddressLatlong();
                        latlon.setLatitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        latlon.setLongitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        myUnitHomeMO.setUnitAddressLatlong(latlon);

                        int taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        if (taskTypeId != 0) {
                            String taskType = RotaTaskTypeEnum.valueOf(taskTypeId).name().replaceAll("_", " ");
                            myUnitHomeMO.setTaskType(taskType);
                        }

                        if (taskTypeId == 5) {
                            taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                            if (taskStatusId == 3 || taskStatusId == 4) {
                                // completed
                                billingStatus = TableNameAndColumnStatement.MYUNITS_HOMEPAGE_COMPLETED;
                            } else {
                                // pedning
                                billingStatus = TableNameAndColumnStatement.MYUNITS_HOMEPAGE_PENDING;
                            }
                            myUnitHomeMO.setBillingStaus(billingStatus);
                        } else {
                            myUnitHomeMO.setBillingStaus("NA");
                        }
                        String taskStartedDateTime = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME));
                        if (taskStartedDateTime != null) {
                            String[] taskStartedDate = taskStartedDateTime.split(" ");

                            myUnitHomeMO.setDate(dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(taskStartedDate[0]));
                        } else {
                            myUnitHomeMO.setDate("NA");
                        }
                        myUnitHomeMO.setBillingDate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BILLING_DATE)));
                        myUnitHomeMO.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                        myUnitHomeMO.setUnitFullAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                        myUnitHomeMO.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        String unitBoundariesString = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_BOUNDARY));

                        if (unitBoundariesString != null) {
                            myUnitHomeMO.setUnitBoundariesString(unitBoundariesString);
                            ArrayList<UnitBoundary> unitBoundaries = IOPSApplication.getGsonInstance().
                                    fromJson(unitBoundariesString,
                                            new TypeToken<ArrayList<UnitBoundary>>() {
                                            }.getType());
                            myUnitHomeMO.setUnitBoundaries(unitBoundaries);
                        }
                        myUnitsList.add(myUnitHomeMO);
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return myUnitsList;
    }

    public String getCollectionDays(int unitid) {
        String collectionDays = "";
        SQLiteDatabase sqlite = null;
        String updateQuery = "select " + "MAX(" + TableNameAndColumnStatement.OUTSTANDING_DAYS + ") " +
                "as " + TableNameAndColumnStatement.OUTSTANDING_DAYS + " from " +
                TableNameAndColumnStatement.UnitBillingStatusTable +
                " where " + TableNameAndColumnStatement.UNIT_ID + " =  " + unitid;
        Timber.d("getCollectionDays updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int cunt = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OUTSTANDING_DAYS));
                        collectionDays = String.valueOf(cunt);
                    } while (cursor.moveToNext());
                }
            }

            Timber.d("cursor detail object  %s", collectionDays);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            collectionDays = "";
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                cursor.close();
                sqlite.close();
            }
        }
        return collectionDays;
    }

    public ArrayList<MyUnitHomeMO> getUnitDetails(int unit_id) {
        int taskStatusId = 1;
        ArrayList<MyUnitHomeMO> myUnitsList = new ArrayList<MyUnitHomeMO>();

        String query;
        if (unit_id == 0) {
            query = "select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                    "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                    "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME + "," +
                    "u." + TableNameAndColumnStatement.BILLING_MODE + "," +
                    "u." + TableNameAndColumnStatement.GEO_LATITUDE + "," +
                    "u." + TableNameAndColumnStatement.GEO_LONGITUDE + "," +
                    "u." + TableNameAndColumnStatement.UNIT_BOUNDARY + "," +
                    "u." + TableNameAndColumnStatement.COLLECTION_STATUS + "," +
                    "u." + TableNameAndColumnStatement.WAGE_DATE + "," +
                    "u." + TableNameAndColumnStatement.BILLING_DATE + "," +
                    "ub." + TableNameAndColumnStatement.BARRACK_ID + "," +
                    "u." + TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID + "," +
                    " min( " + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME
                    + " ) as upcoming_activity ," +
                    " u." + TableNameAndColumnStatement.BILLING_DATE +
                    " , ub. " + TableNameAndColumnStatement.BARRACK_ID +
                    " from " + TableNameAndColumnStatement.UNIT_TABLE +
                    "  u left join " + TableNameAndColumnStatement.TASK_TABLE +
                    " t on t." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                    " and t." + TableNameAndColumnStatement.TASK_STATUS_ID + " in (1, 2)" +
                    " left join " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE +
                    " ub on ub." + TableNameAndColumnStatement.UNIT_ID + " = " +
                    " u." + TableNameAndColumnStatement.UNIT_ID +
                    " where u.unit_status = " + 1 +
                    " group by " + " u." + TableNameAndColumnStatement.UNIT_ID + " order by u." +
                    TableNameAndColumnStatement.UNIT_NAME;

        } else {
            query = "select u." + TableNameAndColumnStatement.UNIT_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_NAME + "," +
                    "u." + TableNameAndColumnStatement.UNIT_CODE + "," +
                    "t." + TableNameAndColumnStatement.TASK_TYPE_ID + "," +
                    "t." + TableNameAndColumnStatement.TASK_STATUS_ID + "," +
                    "u." + TableNameAndColumnStatement.UNIT_FULL_ADDRESS + "," +
                    "t." + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + "," +
                    "t." + TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME + "," +
                    "u." + TableNameAndColumnStatement.BILLING_MODE + "," +
                    "u." + TableNameAndColumnStatement.GEO_LATITUDE + "," +
                    "u." + TableNameAndColumnStatement.GEO_LONGITUDE + "," +
                    "u." + TableNameAndColumnStatement.UNIT_BOUNDARY + "," +
                    "u." + TableNameAndColumnStatement.COLLECTION_STATUS + "," +
                    "u." + TableNameAndColumnStatement.WAGE_DATE + "," +
                    "u." + TableNameAndColumnStatement.BILLING_DATE + "," +
                    "ub." + TableNameAndColumnStatement.BARRACK_ID + "," +
                    "u." + TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID + "," +
                    " min( " + TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME
                    + " ) as upcoming_activity ," +
                    " u." + TableNameAndColumnStatement.BILLING_DATE +
                    " , ub. " + TableNameAndColumnStatement.BARRACK_ID +
                    " from " + TableNameAndColumnStatement.UNIT_TABLE +
                    "  u left join " + TableNameAndColumnStatement.TASK_TABLE +
                    " t on t." + TableNameAndColumnStatement.UNIT_ID +
                    " = " + "u." + TableNameAndColumnStatement.UNIT_ID +
                    " and t." + TableNameAndColumnStatement.TASK_STATUS_ID + " in (1, 2)" +
                    " left join " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE +
                    " ub on ub." + TableNameAndColumnStatement.UNIT_ID + " = " +
                    " u." + TableNameAndColumnStatement.UNIT_ID +
                    " where u." +
                    TableNameAndColumnStatement.UNIT_ID + "='" + unit_id + "'" +
                    " group by " + " u." + TableNameAndColumnStatement.UNIT_ID
                    + " order by u." + TableNameAndColumnStatement.UNIT_NAME;
        }

        Timber.i("c %s", query);
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            String billingStatus = "";

            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        MyUnitHomeMO myUnitHomeMO = new MyUnitHomeMO();
                        myUnitHomeMO.setBillAuthoritativeUnit(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID)));
                        myUnitHomeMO.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        myUnitHomeMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        String collectonDaysStr = getCollectionDays(myUnitHomeMO.getUnitId());
                        if (collectonDaysStr != null & collectonDaysStr.length() != 0) {
                            int collectonDays = Integer.valueOf(collectonDaysStr);
                            if (collectonDays == 0) { //System.out.println("Number is equal to zero");
                                myUnitHomeMO.setCollectionStaus(dateTimeFormatConversionUtil.getCurrentDate()); // zero
                            } else if (collectonDays > 0) { //System.out.println("Number is positive");
                                myUnitHomeMO.setCollectionStaus(collectonDays + " days due"); // positive
                            } else {
                                collectonDays = Math.abs(collectonDays); // coverting negative to positive
                                myUnitHomeMO.setCollectionStaus(dateTimeFormatConversionUtil.addDaysToCurrentDate(collectonDays));
                            }
                        } else {
                            myUnitHomeMO.setCollectionStaus("NA");
                        }

                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.WAGE_DATE)) == null)
                            myUnitHomeMO.setWagesStatus("NA");
                        else
                            myUnitHomeMO.setWagesStatus(dateTimeFormatConversionUtil.dayToCompleteDate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.WAGE_DATE))));

                        // setting the unit Lat & Lon
                        UnitAddressLatlong latlon = new UnitAddressLatlong();
                        latlon.setLatitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        latlon.setLongitude(cursor.getFloat(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        myUnitHomeMO.setUnitAddressLatlong(latlon);

                        int taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        if (taskTypeId != 0) {
                            String taskType = RotaTaskTypeEnum.valueOf(taskTypeId).name().replaceAll("_", " ");
                            myUnitHomeMO.setTaskType(taskType);
                        }
                        if (taskTypeId == 5) {
                            taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                            if (taskStatusId == 3 || taskStatusId == 4) {
                                // completed
                                billingStatus = TableNameAndColumnStatement.MYUNITS_HOMEPAGE_COMPLETED;
                            } else {
                                // pedning
                                billingStatus = TableNameAndColumnStatement.MYUNITS_HOMEPAGE_PENDING;
                            }
                            myUnitHomeMO.setBillingStaus(billingStatus);
                        } else {
                            myUnitHomeMO.setBillingStaus("NA");
                        }
                        String taskStartedDateTime = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME));
                        if (taskStartedDateTime != null) {
                            String[] taskStartedDate = taskStartedDateTime.split(" ");

                            myUnitHomeMO.setDate(dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(taskStartedDate[0]));
                        } else {
                            myUnitHomeMO.setDate("NA");
                        }
                        myUnitHomeMO.setBillingDate(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BILLING_DATE)));
                        myUnitHomeMO.setUnitCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CODE)));
                        myUnitHomeMO.setUnitFullAddress(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_FULL_ADDRESS)));
                        myUnitHomeMO.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        String unitBoundariesString = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_BOUNDARY));

                        if (unitBoundariesString != null) {
                            myUnitHomeMO.setUnitBoundariesString(unitBoundariesString);
                            ArrayList<UnitBoundary> unitBoundaries = IOPSApplication.getGsonInstance().
                                    fromJson(unitBoundariesString,
                                            new TypeToken<ArrayList<UnitBoundary>>() {
                                            }.getType());
                            myUnitHomeMO.setUnitBoundaries(unitBoundaries);
                        }
                        myUnitsList.add(myUnitHomeMO);
                    } while (cursor.moveToNext());
                }
                Timber.i("count %s", cursor.getCount());
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return myUnitsList;
    }

    public void updateMainGateImageUrl(String imagePath, int unitId) {

        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL, imagePath);
                sqlite.update(TableNameAndColumnStatement.UNIT_TABLE, values, TableNameAndColumnStatement.UNIT_ID
                        + " = " + unitId, null);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.close();
            }
        }
    }


    public void updateUnitStatus(int unitId, int unitStatus) {

        SQLiteDatabase sqlite = null;
        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.UNIT_STATUS, unitStatus);
                sqlite.update(TableNameAndColumnStatement.UNIT_TABLE, values, TableNameAndColumnStatement.UNIT_ID
                        + " = " + unitId, null);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.close();
            }
        }
    }

    public String fetchUnitMainGateImage(int unitId) {
        String path = null;
        String selectQuery = " SELECT  " + TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL +
                " FROM " + TableNameAndColumnStatement.UNIT_TABLE + " WHERE " +
                TableNameAndColumnStatement.UNIT_ID + " = '" + unitId + "'";
        Timber.d("fetchFilterList selectQuery  %s", selectQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        path = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = null;
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return path;
    }

    public ArrayList<Equipments> getEquipmentList() {

        ArrayList<Equipments> equipmentsList = new ArrayList<>();

        String selectQuery = " SELECT  *" +
                " FROM " + TableNameAndColumnStatement.EQUIPMENT_LIST_TABLE;
        Timber.d("getEquipmentList selectQuery  %s", selectQuery);

        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        Equipments equipments = new Equipments();
                        equipments.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        equipments.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTION)));
                        equipments.setEquipmentName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_NAME)));
                        equipments.setEquipmentCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_CODE)));
                        equipments.setEquipmentTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE_ID)));
                        // equipments.setIsActive(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ACTIVE)));
                        equipmentsList.add(equipments);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();


            }
        }
        return equipmentsList;


    }

    public ArrayList<String> getConfiguredNfclist() {
        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select * from " + TableNameAndColumnStatement.UNIT_POST_TABLE + " where "
                        //  + TableNameAndColumnStatement.UNIT_ID + " = " + unitId + " and "
                        + TableNameAndColumnStatement.DEVICE_NO + " <> ''";

                Timber.d("unit nfc information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                ArrayList<String> unitPostList = new ArrayList<>();
                if (cursor != null && cursor.moveToFirst()) {
                    do {

                        unitPostList.add(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DEVICE_NO)));
                    }
                    while (cursor.moveToNext());
                }
                return unitPostList;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return null;
    }


    public List<UnitPostGeoCoordinates> getPostGeoCoordinates(int unitId) {
        List<UnitPostGeoCoordinates> unitPostGeoCoordinatesList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = " select " + TableNameAndColumnStatement.GEO_LATITUDE +
                        " , " + TableNameAndColumnStatement.GEO_LONGITUDE +
                        " , " + TableNameAndColumnStatement.IS_MAIN_GATE +
                        " from " + TableNameAndColumnStatement.UNIT_POST_TABLE +
                        "  where " + TableNameAndColumnStatement.UNIT_ID + " = " + unitId;

                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UnitPostGeoCoordinates unitPostGeoCoordinate = new UnitPostGeoCoordinates();
                        if (cursor.getInt(cursor.getColumnIndex
                                (TableNameAndColumnStatement.IS_MAIN_GATE)) == 0) {
                            unitPostGeoCoordinate.isMainGate = false;
                        } else {
                            unitPostGeoCoordinate.isMainGate = true;
                        }
                        unitPostGeoCoordinate.longitude = cursor.getDouble(cursor.getColumnIndex
                                (TableNameAndColumnStatement.GEO_LONGITUDE));
                        unitPostGeoCoordinate.latitude = cursor.getDouble(cursor.getColumnIndex
                                (TableNameAndColumnStatement.GEO_LATITUDE));
                        unitPostGeoCoordinatesList.add(unitPostGeoCoordinate);
                    }
                    while (cursor.moveToNext());

                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return unitPostGeoCoordinatesList;
    }

    public List<Integer> getUnitIdsAfterDeletion(SQLiteDatabase sqlite) {
        List<Integer> unitIds = new ArrayList<>();
        try {
            synchronized (sqlite) {
                String selectQuery = " select unit_id from unit";

                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        unitIds.add(cursor.getInt(
                                cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return unitIds;
    }

    public List<UnitBarrack> getUnitBarracks(SQLiteDatabase sqlite) {
        List<UnitBarrack> unitBarracks = new ArrayList<>();
        try {
            synchronized (sqlite) {
                String selectQuery = " select * from " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE;

                Timber.d("getUnitBarracks selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UnitBarrack unitBarrack = new UnitBarrack();
                        unitBarrack.setBarrackId(cursor.getInt(
                                cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        unitBarrack.setId(cursor.getInt(
                                cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        unitBarrack.setUnitId(cursor.getInt(
                                cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        if (cursor.getInt(
                                cursor.getColumnIndex(TableNameAndColumnStatement.IS_ACTIVE)) == 1) {
                            unitBarrack.setActive(true);
                        } else {
                            unitBarrack.setActive(false);
                        }

                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return unitBarracks;
    }

    public boolean isNfcConfiguredForBarrackOffline(String uniqueId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select barrack_id from barrack where " + TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "'";
                Timber.d("Barrack information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        return true;
                    }
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return false;
    }

    public boolean isNfcConfiguredForUnitPostOffline(String uniqueId, int unitId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            synchronized (sqlite) {
                String selectQuery = "select unit_id from unit_post where " + TableNameAndColumnStatement.DEVICE_NO + " = '" + uniqueId + "' and unit_id != " + unitId;
                Timber.d("post information selectQuery  %s", selectQuery);
                Cursor cursor = sqlite.rawQuery(selectQuery, null);
                if (cursor != null && cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        return true;
                    }
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            return false;
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return false;
    }
}
