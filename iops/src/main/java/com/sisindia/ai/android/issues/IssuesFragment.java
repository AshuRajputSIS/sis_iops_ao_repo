package com.sisindia.ai.android.issues;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.issues.complaints.ComplaintsFragment;
import com.sisindia.ai.android.issues.grievances.GrievanceFragment;
import com.sisindia.ai.android.issues.improvements.ImprovementPlansFragment;
import com.sisindia.ai.android.issues.models.OpenIssues;
import com.sisindia.ai.android.manualrefresh.ManualIssueSync;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class IssuesFragment extends Fragment implements IssuesSyncListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @Bind(R.id.issuesTabLayout)
    TabLayout issuesTabLayout;
    @Bind(R.id.issues_viewpager)
    ViewPager issuesViewpager;
    Resources resources;
    private ArrayList<String> viewPagerTabTiles;
    private String mParam1;
    private String issueType;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private ColorStateList colorStateList;
    private OpenIssues openIssuesData;
    private GrievanceFragment grievanceFragment;
    private ComplaintsFragment complaintFragment;
    private ImprovementPlansFragment improvementPlanFragment;
    private static final int GRIEVANCE_TAB = 0;
    private static final int COMPLAINTS_TAB = 1;
    private static final int IMPROVEMENT_PLAN_TAB = 2;
    private IssuesSyncListener issuesSyncListener;


    public IssuesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IssuesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IssuesFragment newInstance(String param1, String param2) {
        IssuesFragment fragment = new IssuesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);////for enable the menu in fragment
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            issueType = getArguments().getString(ARG_PARAM2);
        }
        colorStateList = ContextCompat.getColorStateList(getActivity(),R.drawable.tab_label_indicator);

        issuesSyncListener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_issues_homepage, container, false);
        updateUi(view);
        updateOpenIssuesHeader();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
            issuesSyncListener = this;

    }


    public void updateUi(View view) {
        ButterKnife.bind(this, view);
        viewPagerTabTiles = new ArrayList<String>();
        resources = getResources();
        // setupViewPager(issuesViewpager);

    }
//


    private void setupViewPager(ViewPager viewPager) {
        IssuesViewPagerAdapter issuesViewPagerAdapter = new IssuesViewPagerAdapter(getChildFragmentManager());
        Util.showAssignedTo = false;
        grievanceFragment = GrievanceFragment.newInstance("", "");
        complaintFragment = ComplaintsFragment.newInstance("", "");
        improvementPlanFragment = ImprovementPlansFragment.newInstance("", "");

        if (openIssuesData != null) {
            grievanceFragment.setOpenGrievanceData(openIssuesData.getGrievanceCount());
            complaintFragment.setOpenComplaintCount(openIssuesData.getComplaintCount());
            improvementPlanFragment.setOpenActiontData(openIssuesData.getActionPlanCount());

        }
        issuesViewPagerAdapter.addFragment(grievanceFragment, getString(R.string.issues_homepage_Grievance_tab));
        issuesViewPagerAdapter.addFragment(complaintFragment, getString(R.string.issues_homepage_complaints_tab));
        issuesViewPagerAdapter.addFragment(improvementPlanFragment, getString(R.string.issues_homepage_improvementPlans_tab));
        issuesViewpager.setAdapter(issuesViewPagerAdapter);
        issuesTabLayout.setupWithViewPager(issuesViewpager);
        if (resources.getString(R.string.addGrievance).equalsIgnoreCase(issueType)) {
            viewPager.setCurrentItem(GRIEVANCE_TAB);
        } else if (resources.getString(R.string.add_complaints).equalsIgnoreCase(issueType)) {
            viewPager.setCurrentItem(COMPLAINTS_TAB);
        } else if (resources.getString(R.string.add_improvement_plans).equalsIgnoreCase(issueType)) {
            viewPager.setCurrentItem(IMPROVEMENT_PLAN_TAB);
        } else {
            viewPager.setCurrentItem(GRIEVANCE_TAB);
        }
        //issuesTabLayout.setOnTabSelectedListener(this);
        issuesTabLayout.setTabTextColors(colorStateList);
    }


/*    public void getOpenGrievanceApiCall() {
        SISClient sisClient = new SISClient(getActivity());
        showprogressbar();

        sisClient.getApi().getOpenIssuesCount(new Callback<OpenIssuesBaseMO>() {
            @Override
            public void success(final OpenIssuesBaseMO openIssuesBaseOR, Response response) {
                switch (openIssuesBaseOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        openIssuesData = openIssuesBaseOR.getOpenIssues();
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (openIssuesData != null)
                                        updateOpenIssuesHeader();

                                }
                            });
                        }

                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        Toast.makeText(getActivity(), getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                        updateOpenIssuesHeader();
                        break;

                    default:
                        Toast.makeText(getActivity(), getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                        updateOpenIssuesHeader();
                        break;
                }
                hideprogressbar();
            }

            @Override
            public void failure(RetrofitError error) {
                hideprogressbar();
                updateOpenIssuesHeader();
                Util.printRetorfitError("UnitTaskHistory", error);
            }
        });
    }*/




    public void updateOpenIssuesHeader() {
        setupViewPager(issuesViewpager);
    }


    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_issue_homepage, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {

            case R.id.syncIssues :
            if (NetworkUtil.isConnected) {
                ManualIssueSync.getIssuesApiCall(issuesSyncListener, getActivity(), isDetached());
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
            }
            break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    @Override
    public void issuesSyncing(boolean isSyned) {
        if(isSyned){
            updateOpenIssuesHeader();
        }

    }
}
