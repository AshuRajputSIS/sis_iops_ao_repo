package com.sisindia.ai.android.myunits;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitContactsMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UnitDetailsGeneralFragment extends Fragment implements View.OnClickListener, OnRecyclerViewItemClickListener {

    private static final int GENERAL_REQUEST_CODE = 1001;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static SingletonUnitDetails singletonUnitDetails;
    @Bind(R.id.mainScrollView)
    ScrollView mainScrollView;
    @Bind(R.id.unitdetails_generaltab_unitId_label_txt)
    CustomFontTextview unitdetailsGeneraltabUnitIdLabelTxt;
    @Bind(R.id.unitdetails_generaltab_unitId_value_txt)
    CustomFontTextview unitdetailsGeneraltabUnitIdValueTxt;
    @Bind(R.id.unitdetails_generaltab_address_label_txt)
    CustomFontTextview unitdetailsGeneraltabAddressLabelTxt;
    @Bind(R.id.unitdetails_generaltab_address_value_txt)
    CustomFontTextview unitdetailsGeneraltabAddressValueTxt;
    @Bind(R.id.unitdetails_generaltab_divider_one)
    View unitdetailsGeneraltabDividerOne;
    @Bind(R.id.unitdetails_generaltab_bill_submission_label_txt)
    CustomFontTextview unitdetailsGeneraltabBillSubmissionLabelTxt;
    @Bind(R.id.unitdetails_generaltab_bill_submission_date_txt)
    CustomFontTextview unitdetailsGeneraltabBillSubmissionDateTxt;
    @Bind(R.id.unitdetails_generaltab_collection_label_txt)
    CustomFontTextview unitdetailsGeneraltabCollectionLabelTxt;
    @Bind(R.id.unitdetails_generaltab_collection_date_txt)
    CustomFontTextview unitdetailsGeneraltabCollectionDateTxt;
    @Bind(R.id.unitdetails_generaltab_wages_label_txt)
    CustomFontTextview unitdetailsGeneraltabWagesLabelTxt;
    @Bind(R.id.unitdetails_generaltab_wages_date_txt)
    CustomFontTextview unitdetailsGeneraltabWagesDateTxt;
    @Bind(R.id.unitdetails_generaltab_divider_two)
    View unitdetailsGeneraltabDividerTwo;
    @Bind(R.id.unitdetails_generaltab_client_information_label_txt)
    CustomFontTextview unitdetailsGeneraltabClientInformationLabelTxt;
    @Bind(R.id.unitdetails_generaltab_addnew_contact_txt)
    CustomFontTextview unitdetailsGeneraltabAddNewContactTxt;
    private String mParam1;
    private String mParam2;
    private MyUnitHomeMO unitDetails;
    private RecyclerView addContactsRecyclerView;
    private AddContactCardViewAdapter addContactCardViewAdapter;

    private SISAITrackingDB sisaiTrackingDBInstance;
    private ArrayList<UnitContactsMO> unitContactList = new ArrayList<>();
    private Resources resources;
    private AppPreferences appPreferences;


    public UnitDetailsGeneralFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static UnitDetailsGeneralFragment newInstance(String param1, String param2) {
        UnitDetailsGeneralFragment fragment = new UnitDetailsGeneralFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sisaiTrackingDBInstance = IOPSApplication.getInstance().getSISAITrackingDBInstance();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        resources = getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View unitDetailGeneralView = inflater.inflate(R.layout.fragment_unit_details_general, container, false);
        ButterKnife.bind(this, unitDetailGeneralView);
        unitDetails = (MyUnitHomeMO) SingletonUnitDetails.getInstance().getUnitDetails();
        if(unitDetails == null){
            unitDetails = appPreferences.getUnitJsonData();
        }
        unitdetailsGeneraltabUnitIdValueTxt.setText(String.valueOf(unitDetails.getUnitCode()));

        if(unitDetails.getUnitFullAddress()!=null &&
                !TextUtils.isEmpty(unitDetails.getUnitFullAddress()) &&
                        !unitDetails.getUnitFullAddress().equalsIgnoreCase("null"))
        {
            unitdetailsGeneraltabAddressValueTxt.setText(unitDetails.getUnitFullAddress());


        }else {

            unitdetailsGeneraltabAddressValueTxt.setText(getResources().getString(R.string.NOT_AVAILABLE));

        }

        unitdetailsGeneraltabBillSubmissionDateTxt.setText(unitDetails.getBillingStaus());
        unitdetailsGeneraltabCollectionDateTxt.setText(unitDetails.getCollectionStaus());
        unitdetailsGeneraltabWagesDateTxt.setText(unitDetails.getWagesStatus());
        //unitdetailsGeneraltabAddNewContactTxt.setOnClickListener(this);
        addContactsRecyclerView = (RecyclerView) unitDetailGeneralView.findViewById(R.id.unitdetails_add_contact_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        addContactsRecyclerView.setLayoutManager(linearLayoutManager);
        addContactsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        addContactCardViewAdapter = new AddContactCardViewAdapter(getActivity());
        setData();
        addContactCardViewAdapter.setOnRecyclerViewItemClickListener(this);


        return unitDetailGeneralView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.unitdetails_generaltab_addnew_contact_txt:
                Intent addNewContactIntent = new Intent(getActivity(), AddEditContactActivity.class);
                addNewContactIntent.putExtra("toolbarTitle", "Add Contact");
                addNewContactIntent.putExtra("unitId", unitDetails.getUnitId());
                startActivityForResult(addNewContactIntent, GENERAL_REQUEST_CODE);
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        if (v.getId() == R.id.edit_contact_label_txt || v.getId() == R.id.imageEdit) {
            Intent addNewContactIntent = new Intent(getActivity(), AddEditContactActivity.class);
            addNewContactIntent.putExtra("toolbarTitle", resources.getString(R.string.edit_contact_str));
            addNewContactIntent.putExtra("unitId", unitDetails.getUnitId());
            addNewContactIntent.putExtra("unitModel", unitContactList.get(position));
            startActivityForResult(addNewContactIntent, GENERAL_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (GENERAL_REQUEST_CODE == requestCode) {
            if (data != null) {
                setData();
            }
        }
    }

    private void setData() {
        unitContactList.clear();
        unitContactList = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchUnitContacts(unitDetails.getUnitId());
        addContactCardViewAdapter.setContactList(unitContactList);
        addContactsRecyclerView.setAdapter(addContactCardViewAdapter);
        if (addContactCardViewAdapter != null) {
            addContactCardViewAdapter.notifyDataSetChanged();
        }
        unitdetailsGeneraltabAddNewContactTxt.setVisibility(View.GONE);
        /*if (unitContactList != null && unitContactList.size() >= 4)
            unitdetailsGeneraltabAddNewContactTxt.setVisibility(View.GONE);
        else
            unitdetailsGeneraltabAddNewContactTxt.setVisibility(View.VISIBLE);*/

        addContactsRecyclerView.setFocusable(false);
    }
}
