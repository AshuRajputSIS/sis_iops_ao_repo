package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shivam on 10/11/16.
 */

public class NfcCheckResponse {

    @SerializedName("Data")
    public NfcData nfcData;

    public static class NfcData {

        @SerializedName("NFCDeviceAvaiable")
        public boolean allowToAdd;
        @SerializedName("isAlreadyUnitTagged")
        public boolean isAlreadyUnitTagged;
    }

    @Override
    public String toString() {
        return String.valueOf(nfcData.allowToAdd);
    }

    public NfcData getNfcData() {
        return nfcData;
    }

}