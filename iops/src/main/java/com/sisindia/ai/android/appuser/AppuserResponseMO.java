package com.sisindia.ai.android.appuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 16-09-2016.
 */
public class AppuserResponseMO {
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private AppUserResponseData data;

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     *
     * @return
     * The data
     */
    public AppUserResponseData getData() {
        return data;
    }

    /**
     *
     * @param data
     * The Data
     */
    public void setData(AppUserResponseData data) {
        this.data = data;
    }

}

