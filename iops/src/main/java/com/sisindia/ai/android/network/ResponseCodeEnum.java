package com.sisindia.ai.android.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shankar on 20/4/16.
 */
public enum ResponseCodeEnum {

    SUCCESS(200),
    FAILURE(400),
    ERROR(404);

    private static Map map = new HashMap<>();

    static {
        for (ResponseCodeEnum responseCodeEM : ResponseCodeEnum.values()) {
            map.put(responseCodeEM.value, responseCodeEM);
        }
    }

    private int value;

    ResponseCodeEnum(int value) {
        this.value = value;
    }

    public static ResponseCodeEnum valueOf(int responseCode) {
        return (ResponseCodeEnum) map.get(responseCode);
    }

    public int getValue() {
        return value;
    }


}
