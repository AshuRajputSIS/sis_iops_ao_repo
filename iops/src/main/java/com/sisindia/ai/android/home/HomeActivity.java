package com.sisindia.ai.android.home;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.AnnualKitReplacementFragment;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.CustomReport;
import com.sisindia.ai.android.commons.EventBusIntentReceiver;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.MyKPIForm;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.DutyAttendanceDB;
import com.sisindia.ai.android.database.DutyAttendanceMo;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.help.HelpFragment;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.moninputandbs.MonInputAndBillSubmissionScreen;
import com.sisindia.ai.android.mybarrack.BarrackInspectionFragment;
import com.sisindia.ai.android.myperformance.GenerateConveyanceReport;
import com.sisindia.ai.android.myperformance.MyPerformanceFragment;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myunits.MyUnitsFragment;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.notifications.NotificationFragment;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.recruitment.RecruitmentFragment;
import com.sisindia.ai.android.rota.RotaFragment;
import com.sisindia.ai.android.rota.UpdateRotaTaskModelListener;
import com.sisindia.ai.android.unitatriskpoa.UnitAtRiskFragment;
import com.sisindia.ai.android.unitraising.UnitRaisingHomeFragment;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Saruchi on 19-03-2016.
 */
public class HomeActivity extends BaseActivity implements AdapterView.OnItemClickListener, ToolbarTitleUpdateListener, GPSListener,
        DutyChangeListener, UpdateDutyStatusListener, OnRecyclerViewItemClickListener, UpdateRotaTaskModelListener {

    public static final String FRAGMENT_TO_LOAD = "FragmentName";
    public static final String OPEN_MY_UNIT_FROM_NOTIFICATION = "my_unit_from_notification";
    public static final String OPEN_AKR_FROM_NOTIFICATION = "akr_from_notification";
    public static final int ROTA_FRAGMENT = 1;
    public static final int MY_UNIT_FRAGMENT = 2;
    public static final int PERFORMANCE_FRAGMENT = 3;
    public static final int ISSUE_FRAGMENT = 6;
    public static final int RECRUITMENT_FRAGMENT = 7;
    public static final int ANNUAL_KIT_REPLACEMENT_FRAGMENT = 8;
    public static final int UNIT_AT_RISK_FRAGMENT = 9;
    public static final int UNIT_RAISING_FRAGMENT = 10;
    public static final int MY_BARRACK_FRAGMENT = 11;
    //adding KPI item in between, hence shifting remaining index
    public static final int MY_KPI = 12;
    public static final int SETTINGS = 13;
    public static final int HELP = 14;
    public static final int CUSTOM_REPORT = 15;
    public static final int CONVEYANCE_TIMELINE = 16;
    public static final int MON_INPUT = 17;
    public static final int BILL_SUBMISSION = 18;
    public Toolbar toolbar;
    public ActionBarDrawerToggle mDrawerToggle;
    public SISAITrackingDB sisaiTrackingDBInstance;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.left_drawer)
    RecyclerView navigatinRecylerView;
    @Bind(R.id.container_fragment)
    FrameLayout containerFragment;
    private Resources resources;
    private NavigationRecyclerAdapter naviAdapter;
    private int fragmentToLaunch;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private boolean fromOnboarding;
    private DutyAttendanceDB dutyAttendanceDb;
    private ArrayList<NavigationModel> menuList;
    private boolean fromMyUnitNotification;
    private boolean fromAkrNotification;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);
        ButterKnife.bind(this);

        resources = getResources();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        sisaiTrackingDBInstance = IOPSApplication.getInstance().getSISAITrackingDBInstance();
        util = new Util();
        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            fragmentToLaunch = extras.getInt(FRAGMENT_TO_LOAD);
            fromMyUnitNotification = extras.getBoolean(OPEN_MY_UNIT_FROM_NOTIFICATION);
            fromAkrNotification = extras.getBoolean(OPEN_AKR_FROM_NOTIFICATION);
            fromOnboarding = extras.getBoolean(OnBoardingFactory.FROM_ONBOADING);
        } else {
            fragmentToLaunch = ROTA_FRAGMENT;
            fromOnboarding = false;
        }
        setupViews();
        checkNotification();
//        getAddress();
    }

   /* private void getAddress() {
        String email = null;
        Pattern gmailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (gmailPattern.matcher(account.name).matches()) {
                email = account.name;
            }
        }
    }*/

    private void checkNotification() {
        if (fromMyUnitNotification) {
            handlePositionClick(MY_UNIT_FRAGMENT);
        } else if (fromAkrNotification) {
            handlePositionClick(ANNUAL_KIT_REPLACEMENT_FRAGMENT);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        handlePositionClick(position);
    }

    private void handlePositionClick(int position) {
        fragmentToLaunch = position;
        if (fragmentToLaunch == ROTA_FRAGMENT) {
            RotaFragment rotaFragment = RotaFragment.newInstance(resources.getString(R.string.rotaTitle), "");
            addFragment(rotaFragment, getResources().getString(R.string.rotaFragment), true);
        } else if (fragmentToLaunch == MY_UNIT_FRAGMENT) {
            if (fromOnboarding) {
                MyUnitsFragment myUnitsFragment = MyUnitsFragment.newInstance(resources.getString(R.string.myunitTitle), "");
                addFragment(myUnitsFragment, getResources().getString(R.string.myUnitsFragment), true);
            } else {
                openOnBardingActivity(OnBoardingFactory.MY_UNIT, 6);
            }
        } else if (fragmentToLaunch == PERFORMANCE_FRAGMENT) {
            if (appPreferences.getDutyStatus()) {
                if (fromOnboarding) {
                    MyPerformanceFragment myPerformanceFragment = MyPerformanceFragment.newInstance(resources.getString(R.string.myPerformanceTitle), "");
                    addFragment(myPerformanceFragment, getResources().getString(R.string.myPerformanceFragment), true);
                } else {
                    openOnBardingActivity(OnBoardingFactory.MY_PERFORMANCE, 1);
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE), Toast.LENGTH_SHORT).show();
            }
        } else if (fragmentToLaunch == resources.getInteger(R.integer.number_4)) {
            NotificationFragment myNotificationTempFragment = NotificationFragment.newInstance(resources.getString(R.string.myNotificationTitle), "");
            addFragment(myNotificationTempFragment, getResources().getString(R.string.notificationFragment), true);
        } else if (fragmentToLaunch == resources.getInteger(R.integer.number_5)) {
            //DIVIDER CLICK DISBALE
        } else if (fragmentToLaunch == ISSUE_FRAGMENT) {
            if (fromOnboarding) {
                IssuesFragment mIssueFragment = null;
                if (appPreferences.getIssueType() == Constants.NavigationFlags.TYPE_GRIEVANCE) {
                    mIssueFragment = IssuesFragment.newInstance(resources.getString(R.string.Issues), resources.getString(R.string.add_greivance));
                } else if (appPreferences.getIssueType() == Constants.NavigationFlags.TYPE_COMPLAINTS) {
                    mIssueFragment = IssuesFragment.newInstance(resources.getString(R.string.Issues), resources.getString(R.string.add_complaints));
                } else if (appPreferences.getIssueType() == Constants.NavigationFlags.TYPE_IMPROVEMENT_PLAN) {
                    mIssueFragment = IssuesFragment.newInstance(resources.getString(R.string.Issues), resources.getString(R.string.add_improvement_plans));
                } else {
                    mIssueFragment = IssuesFragment.newInstance(resources.getString(R.string.Issues), "");
                }

                addFragment(mIssueFragment, getResources().getString(R.string.issuesFragment), true);
            } else {
                openOnBardingActivity(OnBoardingFactory.ISSUES, 1);
            }
        } else if (fragmentToLaunch == RECRUITMENT_FRAGMENT) {
            if (fromOnboarding) {
                RecruitmentFragment recuirtFragment = RecruitmentFragment.newInstance(resources.getString(R.string.recruitment_title), "");
                addFragment(recuirtFragment, getResources().getString(R.string.recruitmentFragment), true);
            } else {
                openOnBardingActivity(OnBoardingFactory.RECRUITMENT, 2);
            }
        } else if (fragmentToLaunch == ANNUAL_KIT_REPLACEMENT_FRAGMENT) {
            if (appPreferences.getDutyStatus()) {
                if (fromOnboarding) {
                    AnnualKitReplacementFragment kitReplacementFragment = AnnualKitReplacementFragment.newInstance(resources.getString(R.string.annual_kit_replacement), "");
                    addFragment(kitReplacementFragment, getResources().getString(R.string.kitReplacementFragment), true);
                } else {
                    openOnBardingActivity(OnBoardingFactory.ANNUAL_KIT_REPLACMENT, 2);
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE), Toast.LENGTH_SHORT).show();
            }
        } else if (fragmentToLaunch == UNIT_AT_RISK_FRAGMENT) {
            if (fromOnboarding) {
                UnitAtRiskFragment myUnitsFragment = UnitAtRiskFragment.newInstance(resources.getString(R.string.unit_risk_string), "");
                addFragment(myUnitsFragment, getResources().getString(R.string.unitAtRiskFragment), true);
            } else {
                openOnBardingActivity(OnBoardingFactory.UNITAT_RISK, 4);
            }
        } else if (fragmentToLaunch == UNIT_RAISING_FRAGMENT) {

            UnitRaisingHomeFragment unitRaisingHomeFragment = UnitRaisingHomeFragment.newInstance(resources.getString(R.string.unitAtRiskFragment));
            addFragment(unitRaisingHomeFragment, getResources().getString(R.string.unitAtRiskFragment), true);

        } else if (fragmentToLaunch == MY_BARRACK_FRAGMENT) {
            if (fromOnboarding) {
                BarrackInspectionFragment barrackInspectionFragment = BarrackInspectionFragment.newInstance(resources.getString(R.string.my_barrack), "");
                addFragment(barrackInspectionFragment, getResources().getString(R.string.barrackInspectionFragment), true);
            } else {
                openOnBardingActivity(OnBoardingFactory.MY_BARRACK, 3);
            }
        } else if (fragmentToLaunch == MY_KPI) {
            fragmentToLaunch = 12;
            startActivity(new Intent(HomeActivity.this, MyKPIForm.class));

        } else if (fragmentToLaunch == SETTINGS) {
            drawerLayout.closeDrawer(GravityCompat.START);
            drawerLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    openOnBardingActivity(OnBoardingFactory.SETTINGS, 2);
                }
            }, 300);
        } else if (fragmentToLaunch == HELP) {
            HelpFragment helpFragment = HelpFragment.newInstance(resources.getString(R.string.Help_videos));
            addFragment(helpFragment, getResources().getString(R.string.Help_videos), true);

        } else if (fragmentToLaunch == CUSTOM_REPORT) {
            startActivity(new Intent(HomeActivity.this, CustomReport.class));
        } else if (fragmentToLaunch == MON_INPUT) {
            startActivity(new Intent(HomeActivity.this, MonInputAndBillSubmissionScreen.class).putExtra("CALL_TYPE_MI_BS", 1));
        } else if (fragmentToLaunch == BILL_SUBMISSION) {
            startActivity(new Intent(HomeActivity.this, MonInputAndBillSubmissionScreen.class).putExtra("CALL_TYPE_MI_BS", 2));
        } else if (fragmentToLaunch == CONVEYANCE_TIMELINE) {
            startActivity(new Intent(HomeActivity.this, GenerateConveyanceReport.class).putExtra("IS_FROM_MENU", true));
        } else {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        clearRotaData();

    }

    private void clearRotaData() {
        SingletonUnitDetails.getInstance().setUnitDetails(null, 0);
        appPreferences.setRotaTaskJsonData(null);
    }

    private void openOnBardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(this, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }

    @Override
    public void changeToolbarTitle(final String title) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbar.setTitle(title);
                toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            }
        });
    }

    private void setupViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        populateNavigationListview();
//        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.rotaTitle);
        toolbar.setTitleTextColor(resources.getColor(R.color.white));
        drawerLayout.closeDrawer(GravityCompat.START);

        if (!Util.isServiceRunning(this, GpsTrackingService.class)) {
            startGpsTracking();
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (naviAdapter != null) {
                    naviAdapter.setupLanguage();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
    }

    /**
     * ADDING THE NAVIGATION ITEMS
     */
    private void populateNavigationListview() {
        menuList = new ArrayList<>();
        menuList.add(new NavigationModel(getResources().getString(R.string.navRota), R.mipmap.rota, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navunits), R.mipmap.my_units, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navPerformance), R.mipmap.my_performance, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navNotification), R.mipmap.notification, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navNotification), R.mipmap.notification, 0, false));
        menuList.add(new NavigationModel(getResources().getString(R.string.navIssues), R.mipmap.issues, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navRecruit), R.mipmap.recruit, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navKitReplacemet), R.mipmap.menu_kit, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navPOA), R.mipmap.unit_at_risk, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navUnitRaising), R.mipmap.my_units, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navbarrack), R.mipmap.barrack_icon, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navKPI), R.mipmap.kpi_icon, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.navSettings), R.drawable.settings, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.help), R.drawable.settings, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.customReport), R.mipmap.custom_report_icon, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.timeline), R.drawable.motorcycle_icon, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.monInput), R.mipmap.custom_report_icon, 0, true));
        menuList.add(new NavigationModel(getResources().getString(R.string.billSubmission), R.mipmap.bill_submission_icon, 0, true));
        setNavigationAdapter();
    }

    private void setNavigationAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        navigatinRecylerView.setLayoutManager(linearLayoutManager);
        navigatinRecylerView.setItemAnimator(new DefaultItemAnimator());
        AppUserData mData = IOPSApplication.getInstance().getSettingsLevelSelectionStatmentDB()
                .getAIPhotoWithAddress(appPreferences.getAppUserId());
        naviAdapter = new NavigationRecyclerAdapter(HomeActivity.this, this, mData);
        naviAdapter.setMenuListData(menuList);
        navigatinRecylerView.setAdapter(naviAdapter);
        naviAdapter.setOnRecyclerViewItemClickListener(this);
        naviAdapter.notifyDataSetChanged();
    }

    public void addFragment(final Fragment fragment, final String fragmentName, boolean closeNavigationDrawer) {
        if (closeNavigationDrawer) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_fragment, fragment, fragmentName);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commitAllowingStateLoss();
                    fromOnboarding = false;
                } catch (Exception exception) {
                    exception.printStackTrace();
                    Crashlytics.logException(exception);
                }
            }
        }, 300);
    }

    @Override
    public void enableGPSservice(boolean isenable, boolean isChangedFromSwitch) {
        landingRota(isenable, isChangedFromSwitch);
    }

    private void landingRota(boolean isenable, boolean isChangedFromSwitch) {
        boolean isFirstTime = appPreferences.isFirstTimeLaunched();
        boolean splashScreenFlag = appPreferences.getSplashScreenFlag();
        if (isFirstTime) {
            RotaFragment rotaFragment = RotaFragment.newInstance(resources.getString(R.string.rotaTitle), "");
            addFragment(rotaFragment, getResources().getString(R.string.rotaFragment), isChangedFromSwitch);
            appPreferences.setFirstTimeLaunched(false);
            appPreferences.setSplashScreenFlag(false);
        } else {
            if (isenable && !fromOnboarding) {
                if (fragmentToLaunch > 0)
                    handlePositionClick(fragmentToLaunch);
                else
                    handlePositionClick(ROTA_FRAGMENT);
            } else {
                if (splashScreenFlag && !fromOnboarding) {
                    if (fragmentToLaunch > 0)
                        handlePositionClick(fragmentToLaunch);
                    else
                        handlePositionClick(ROTA_FRAGMENT);

                    appPreferences.setSplashScreenFlag(false);
                }
            }
            setDutyAttendanceData(isChangedFromSwitch, isenable);
        }
    }

    private void setDutyAttendanceData(boolean isChangedFromSwitch, boolean isenable) {
        dutyAttendanceDb = IOPSApplication.getInstance().getDutyAttendanceDB();
        if (dutyAttendanceDb != null) {
            DutyAttendanceMo dutyAttendanceMo = new DutyAttendanceMo();
            if (isChangedFromSwitch) {
                if (isenable) {
                    dutyAttendanceMo.setDutyOnTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                    dutyAttendanceDb.insertDutyAttendanceDetails(dutyAttendanceMo);

                    //COMMENTING BELOW CODE AS ITS CREATING DUTY OFF ISSUE:
                    /*if (dateTimeFormatConversionUtil.isTimeGreaterThan10())
                        Util.setAlarmManager(this, Constants.NIGHT_DUTY_ON_OFF);*/

                } else {
//                    dutyAttendanceMo.setDutyOffTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                    dutyAttendanceDb.updateDutyOffTime(dateTimeFormatConversionUtil.getCurrentDateTime());
                }
            }
        }
        if (isChangedFromSwitch) {
            triggerSyncAdapter();
            captureActivityTaskLogs(isenable);
            drawerLayout.closeDrawers();

            /*if (BuildConfig.BUILD_VARIANT.equalsIgnoreCase("Prod")) {
                dutyUpdateDetails(isenable);
            }*/

            dutyUpdateDetails(isenable);
        }
    }

    private void dutyUpdateDetails(final boolean isenable) {
        appPreferences.saveDutyStatus(isenable);
        if (!Util.isServiceRunning(HomeActivity.this, GpsTrackingService.class)) {
            startGpsTracking();
        }
    }

    @Override
    public void onDutyChange(boolean dutyOffOn) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void updateDutySwitch(boolean status) {
    }

    /*private void showprogressbar() {
        Util.showProgressBar(HomeActivity.this, R.string.loading_please_wait);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(!isFinishing());
    }*/

    private void captureActivityTaskLogs(boolean isEnabled) {
        TimeLineModel timeLineModel = new TimeLineModel();
        timeLineModel.setActivity("Duty on");
        timeLineModel.setLocation("");
        timeLineModel.setTaskId("null");
        timeLineModel.setStatus("");
        timeLineModel.setTaskName("");
        timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
        timeLineModel.setEndTime("");
        timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());
        if (isEnabled) {
            timeLineModel.setActivity("Duty on");
            if (IOPSApplication.getInstance().getActivityLogDB().getActivityLogsCount() > 0) {
                IOPSApplication.getInstance().getActivityLogDB().deleteActivityLogs();
            }
        } else {
            timeLineModel.setActivity("Duty Off");
            //BELOW METHOD IS USED TO OPEN THE CONVEYANCE SCREEN
            startConveyanceActivity();
        }
        IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
        //check if duty on and off is present for todays date if not insert
        //else if duty on present dont do any thing
        //else if duty off present update existing one
        IOPSApplication.getInstance().getMyPerformanceStatementsDB()
                .insertIntoMyPerformanceTable(timeLineModel);
    }

    private void startConveyanceActivity() {
        openOnBoardingActivity(OnBoardingFactory.CONVEYANCE_REPORT, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentToLaunch == MY_KPI || fragmentToLaunch == CUSTOM_REPORT || fragmentToLaunch == MON_INPUT ||
                fragmentToLaunch == BILL_SUBMISSION || fragmentToLaunch == CONVEYANCE_TIMELINE) {
            return;
        } else {
            EventBus.getDefault().register(this);
            drawerLayout.closeDrawers();
            naviAdapter.setAiImage();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    handlePositionClick(fragmentToLaunch);
                }
            }, 1);
        }
    }

    @Override
    public void onBackPressed() {
        drawerLayout.closeDrawers();
        FragmentManager fragmentManager = getSupportFragmentManager();
        RotaFragment rotaFragment = (RotaFragment) fragmentManager.findFragmentByTag(getResources().getString(R.string.rotaFragment));
        if (rotaFragment != null && rotaFragment.isVisible()) {
            if (rotaFragment.robotoCalendarView.getVisibility() == View.VISIBLE) {
                rotaFragment.robotoCalendarView.setVisibility(View.GONE);
            } else {
                new AlertDialog.Builder(this)
                        .setMessage("Do you want to exit the Application..?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                System.exit(0);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        } else {
            handlePositionClick(ROTA_FRAGMENT);
        }
    }

    /*@Override
    public void updateRotaTask(Object object) {
    }*/

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(HomeActivity.this, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {

        if (ISSUE_FRAGMENT == position) {
            appPreferences.setIssueType(1);
        }
        appPreferences.setFilterIds(null); // clearing the filter ids
        handlePositionClick(position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void autoRotaRefresh(EventBusIntentReceiver intentServiceResult) {
    }

    @Override
    public void updateRotaTask(Object object) {

    }
}