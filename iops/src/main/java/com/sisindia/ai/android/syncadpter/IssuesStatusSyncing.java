package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.issueDTO.IssueLevelUpdateStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.IssueIMO;
import com.sisindia.ai.android.network.response.IssuesOutputMO;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 5/24/2017.
 */

class IssuesStatusSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<IssueIMO> issueInputRequestList;
    private IssueLevelUpdateStatementDB issueLevelUpdateStatementDB;
    private AppPreferences appPreferences;

    public IssuesStatusSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        issueLevelUpdateStatementDB = IOPSApplication.getInstance().getIssueUpdateDBInstance();
        this.issueInputRequestList = new ArrayList<>();
        appPreferences = new AppPreferences(mcontext);
        IssuesSync();
    }

    private void IssuesSync() {
        issueInputRequestList.clear();
        issueInputRequestList = getAllIssuesToSync();
        if (null != issueInputRequestList && issueInputRequestList.size() != 0) {

            for (IssueIMO issuesIMO : issueInputRequestList) {
                issuesApiCall(issuesIMO);
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER + "IssuesStatusSyncing -->nothing to sync ");
        }
    }

    private List<IssueIMO> getAllIssuesToSync() {
        List<IssueIMO> issueInputRequestList = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ISSUES_TABLE + " where (" +
                TableNameAndColumnStatement.IS_NEW + " = " + 1 + " or " +
                TableNameAndColumnStatement.IS_SYNCED + " = " + 0 + ") and ( " +
                TableNameAndColumnStatement.CLOSURE_END_DATE + "  is not null and " +
                TableNameAndColumnStatement.ISSUE_STATUS_ID + " = " + 3 + ")";

        Timber.d("AllIssuesQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            SQLiteDatabase db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        IssueIMO issueIMO = new IssueIMO();
                        issueIMO.setIssueTypeId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_TYPE_ID)));
                        issueIMO.setIssueMatrixId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        issueIMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        issueIMO.setRaisedById(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)) == 0) {
                            issueIMO.setIssueId(null);
                        } else {
                            issueIMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));
                        }
                        issueIMO.setIssueTypeId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_TYPE_ID)));
                        issueIMO.setIssueMatrixId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        issueIMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        issueIMO.setRaisedById(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)) != null &&
                                (!cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)).isEmpty() &&
                                        !cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)).equalsIgnoreCase("0"))) {
                            issueIMO.setUnitId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        } else {
                            issueIMO.setUnitId(null);
                        }
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)) != 0) {
                            issueIMO.setGuardId(String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID))));
                        } else {
                            issueIMO.setGuardId(null);
                        }
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)) != null) {
                            issueIMO.setGuardCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        } else {
                            issueIMO.setGuardCode(null);
                        }
                        issueIMO.setIssueSourceId(String.valueOf(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MODE_ID))));
                        issueIMO.setIssueStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_STATUS_ID)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)) != 0) {
                            issueIMO.setSourceTaskId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)));
                        } else {
                            issueIMO.setSourceTaskId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)) != 0) {
                            issueIMO.setAssignedToId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)));
                        } else {
                            issueIMO.setAssignedToId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CATEGORY_ID)) != 0) {
                            issueIMO.setIssueCategoryId(String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CATEGORY_ID))));
                        } else {
                            issueIMO.setIssueCategoryId(null);
                        }
                        String temp = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME));
                        issueIMO.setRaisedDateTime(temp);
                        issueIMO.setAssignedToDateTime(temp);
                        issueIMO.setIssueActionId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        String targetActionEndDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_ACTION_END_DATE));
                        if (targetActionEndDate != null && targetActionEndDate.length() > 0) {
                            targetActionEndDate = targetActionEndDate + " 00:00:00";
                            issueIMO.setTargetActionDateTime(targetActionEndDate);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)) != 0) {
                            issueIMO.setBarrackId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        } else {
                            issueIMO.setBarrackId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CAUSE_ID)) != 0) {
                            issueIMO.setIssueCauseId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CAUSE_ID)));
                        } else {
                            issueIMO.setIssueCauseId(null);
                        }

                        issueIMO.setSeqId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        String issueNatureId = String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_NATURE_ID)));
                        issueIMO.setIssueNatureId(issueNatureId);
                        issueIMO.setClientRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_REMARKS)));
                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_NAME)))) {
                            issueIMO.setClientContactName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_NAME)));
                        } else {
                            issueIMO.setClientContactName(null);
                        }
                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)))) {
                            issueIMO.setPhoneNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        } else {
                            issueIMO.setPhoneNo(null);
                        }

                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_END_DATE)))) {
                            issueIMO.setClosureDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_END_DATE)));
                        } else {
                            issueIMO.setClosureDateTime(null);
                        }
                        Timber.d("Issues jsonObject  %s", IOPSApplication.getGsonInstance().toJson(issueIMO));
                        issueIMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        issueInputRequestList.add(issueIMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing object  %s", IOPSApplication.getGsonInstance().toJson(issueInputRequestList));
        }
        return issueInputRequestList;
    }

    private synchronized void issuesApiCall(final IssueIMO inputIMO) {
        sisClient.getApi().syncIssueData(inputIMO, new Callback<IssuesOutputMO>() {
            @Override
            public void success(IssuesOutputMO issuesOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing response  %s", issuesOutputMO);
                switch (issuesOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        if (issuesOutputMO != null && issuesOutputMO.getActionPlanData() != null) {

                           /* if (issuesOutputMO.getActionPlanData().getIssueId() != null) {
                                issueLevelUpdateStatementDB.updateIssueSyncStatus(issuesOutputMO.getActionPlanData().getIssueId());
                            }*/

                            if (issuesOutputMO.getActionPlanData().getJsonActionPlan() != null && issuesOutputMO.getActionPlanData().getAssignedToId() != null) {

                            } else {
                                if (issuesOutputMO.getActionPlanData().getIssueId() != null) {
                                    issueLevelUpdateStatementDB.updateIssueSyncStatus(issuesOutputMO.getActionPlanData().getIssueId());
                                }
                            }
                        }
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing count   %s", "count:");
                Crashlytics.logException(error);
            }
        });
    }


}
