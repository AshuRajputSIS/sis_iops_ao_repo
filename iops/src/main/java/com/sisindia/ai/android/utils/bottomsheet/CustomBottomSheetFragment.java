package com.sisindia.ai.android.utils.bottomsheet;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

public class CustomBottomSheetFragment extends BottomSheetDialogFragment implements OnRecyclerViewItemClickListener {
    private OnBottomSheetItemClickListener onBottomSheetItemClickListener;

    private RecyclerView bottomSheetRecycleview;
    private BottomSheetAdapter bottomSheetAdapter;
    private ArrayList<String> elements;
    private Resources resources;
    private int viewId;
    private BottomSheetBehavior<LinearLayout> bottomSheetBehaviorNew;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };
    private ArrayList<String> bottomSheetList;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.onBottomSheetItemClickListener = (OnBottomSheetItemClickListener) context;
    }

    public void setBottomSheetData(ArrayList<String> bottomSheetList, int viewId) {
        this.bottomSheetList = bottomSheetList;
        this.viewId = viewId;
    }

    public void setSheetItemClickListener(OnBottomSheetItemClickListener onBottomSheetItemClickListener) {
        this.onBottomSheetItemClickListener = onBottomSheetItemClickListener;

    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        dialog.setContentView(contentView);
        /*resources = getResources();

        elements = new ArrayList<String>();
        *//*    for(int index=0;index<10;index++)
            elements.add("Client " + index);*//*
        elements.add(resources.getString(R.string.UNIT_NAME_SBI));
        elements.add(resources.getString(R.string.UNIT_NAME_ELETRONIC_CITY));
        elements.add(resources.getString(R.string.UNIT_NAME_ITPL));
        elements.add(resources.getString(R.string.UNIT_NAME_ECOSPACE));
        elements.add(resources.getString(R.string.UNIT_NAME_ELE_PHASE2));
        elements.add(resources.getString(R.string.UNIT_NAME_IBC_KNOW_PARK));*/

        bottomSheetRecycleview = (RecyclerView) contentView.findViewById(R.id.bottom_sheet_recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        bottomSheetRecycleview.setLayoutManager(linearLayoutManager);
        bottomSheetRecycleview.setItemAnimator(new DefaultItemAnimator());
        bottomSheetAdapter = new BottomSheetAdapter(getActivity());
        bottomSheetAdapter.setBottomSheetData(bottomSheetList);
        bottomSheetRecycleview.setAdapter(bottomSheetAdapter);
        bottomSheetAdapter.setOnRecyclerViewItemClickListener(this);
        showBottomSheet(contentView);

       /* CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }*/
    }


    void showBottomSheet(View view) {

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        final CoordinatorLayout.Behavior bottomSheetBehavior = params.getBehavior();
        bottomSheetBehaviorNew = (BottomSheetBehavior) bottomSheetBehavior;

        if (bottomSheetBehavior != null && bottomSheetBehavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) bottomSheetBehavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            ((BottomSheetBehavior) bottomSheetBehavior).setPeekHeight(Util.dpToPx(250));
        }

        LinearLayout bottomSheetViewgroup = (LinearLayout) view.findViewById(R.id.bottom_sheet_layout_fragment);
        //bottomSheetBehavior =  BottomSheetBehavior.from(bottomSheetViewgroup);

        // ((BottomSheetBehavior) bottomSheetBehavior).setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {

        onBottomSheetItemClickListener.onSheetItemClick(bottomSheetList.get(position), viewId);
        dismiss();
    }
}
