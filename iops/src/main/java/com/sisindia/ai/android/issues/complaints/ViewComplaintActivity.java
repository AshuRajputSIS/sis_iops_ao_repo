package com.sisindia.ai.android.issues.complaints;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.IssueClosureMo;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 04-08-2016.
 */
public class ViewComplaintActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener{
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private IssuesMO  value;
    @Bind(R.id.unit_name)
    CustomFontTextview mUnitName;
    @Bind(R.id.date_value)
    CustomFontTextview mDateValue;
    @Bind(R.id.complaint_mode)
    CustomFontTextview mComplaintMode;
    @Bind(R.id.complaint_nature)
    CustomFontTextview mComplaintNature;
    @Bind(R.id.complaint_by)
    CustomFontTextview mComplaintBy;
    @Bind(R.id.target_date)
    CustomFontTextview mTargetDate;
    @Bind(R.id.remark_view)
    View mRemarkView;
    @Bind(R.id.action_plan_input)
    CustomFontEditText mActionPlanInput;
    @Bind(R.id.assigned_to_input)
    CustomFontEditText mAssignedToInput;
    @Bind(R.id.days_ago)
    RelativeTimeTextView mDaysAgo;
    private LookUpSpinnerAdapter statusAdapter,assignedToAdapter;
    private ArrayList<LookupModel> issueStatus;
    @Bind(R.id.spinner_compliant_status)
    Spinner spinnerStatus;
    private IssueClosureMo issueClosureMo;
    @Bind(R.id.add_complaint_remarks)
    CustomFontEditText mAddComplaintRemarks;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    @Bind(R.id.footer_layout)
    LinearLayout buttonsLayout;
    @Bind(R.id.button_save)
    CustomFontButton buttonSave;
    @Bind(R.id.button_cancel)
    CustomFontButton buttonCancel;
    private Resources resources;
    @Bind(R.id.complaint_cause)
    CustomFontTextview mComplaintCause;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_complaint_details);
        ButterKnife.bind(this);
        resources= getResources();
        issueClosureMo = new IssueClosureMo();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        setBaseToolbar(toolbar, getResources().getString(R.string.view_complaint));
        if (getIntent().hasExtra(getResources().getString(R.string.complaint_details))) {
            value = getIntent().getParcelableExtra(getResources().getString(R.string.complaint_details));
            mUnitName.setText(value.getUnitName());
            mDateValue.setText( dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(value.getCreatedDateTime()));
            mComplaintMode.setText(value.getModeOfComplaint());
            mComplaintNature.setText(value.getNatureOfComplaint());
            mComplaintCause.setText(value.getCauseOfComplaint());
            mComplaintBy.setText(value.getUnitContactName());
            mTargetDate.setText(value.getTargetActionEndDate());
            long millis =dateTimeFormatConversionUtil.convertStringToDate((value.getCreatedDateTime())).getTime();
            mDaysAgo.setReferenceTime(millis);
                EditText mComments = (EditText) mRemarkView.findViewById(R.id.editTextTypeRemark);
                mComments.setText(value.getRemarks());
            mComments.setFocusable(false);
            mComments.setClickable(false);
                IssueLevelSelectionStatementDB issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
                String value_name = issueLevelSelectionStatementDB.getActionPlanName(value.getActionPlanId());
                mActionPlanInput.setText(value_name);
                mAssignedToInput.setText(value.getAssignedToName());
            statusAdapter = new LookUpSpinnerAdapter(this);
            assignedToAdapter = new LookUpSpinnerAdapter(this);
            buttonSave.setOnClickListener(this);
            buttonCancel.setOnClickListener(this);
            issueStatus = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.IssueStatus), null);
            statusAdapter.setSpinnerData(issueStatus);
            spinnerStatus.setAdapter(statusAdapter);
            spinnerStatus.setSelection(Util.getIndex(value.getStatus(),spinnerStatus,issueStatus));
            //spinnerStatus.setBackgroundColor(getResources().getColor(R.color.transpaent));
            setUpViewVisibility();


            }

    }


    public long getDaysBetweenDates(Date d1, Date d2){
        return TimeUnit.MILLISECONDS.toDays(d1.getTime() - d2.getTime());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_post_save:
                validateFields();
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.button_save:
                validateFields();
                break;
            case R.id.button_cancel:
                finish();
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String issueStatusString = issueStatus.get(position).getName();
        issueClosureMo.setStatusName(issueStatusString);
        issueClosureMo.setStatusId(issueStatus.get(position).getLookupIdentifier());
        if (issueStatus.get(position).getLookupIdentifier() == 3) {
            mAddComplaintRemarks.setVisibility(View.VISIBLE);
            buttonsLayout.setVisibility(View.VISIBLE);
            mAddComplaintRemarks.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    issueClosureMo.setClosureRemarks(s.toString());


                }
            });
        } else {

            mAddComplaintRemarks.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.GONE);
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public void updateIssueTable() {
        issueClosureMo.setClosureEndDate(dateTimeFormatConversionUtil.getCurrentDateTime());
        IOPSApplication.getInstance().getIssueUpdateStatementDB().updateIssuesStatus(value.getpId(), issueClosureMo);
        if(issueClosureMo.getStatusId() == 3) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance()
                    .updateImprovementPlanStatus(value.getIssueId(), 2,null,null);
        }
    }
    private void setUpViewVisibility(){
        if(value.getAssignedToId()!=0){
        if (appPreferences.getAppUserId() == value.getAssignedToId()) {
            if (resources.getString(R.string.Resolved).equalsIgnoreCase(value.getStatus())) {
                spinnerStatus.setEnabled(false);
                spinnerStatus.setBackgroundColor(getResources().getColor(R.color.white));
                mAddComplaintRemarks.setVisibility(View.GONE);
                buttonsLayout.setVisibility(View.GONE);

            }
            else{
                spinnerStatus.setEnabled(true);

                spinnerStatus.setOnItemSelectedListener(this);

            }


        }
        else {
            spinnerStatus.setEnabled(false);
            spinnerStatus.setBackgroundColor(getResources().getColor(R.color.white));
            mAddComplaintRemarks.setVisibility(View.GONE);
            buttonsLayout.setVisibility(View.GONE);

        }
        }
        else{
            spinnerStatus.setEnabled(false);

            snackBarWithMesg(buttonsLayout,getResources().getString(R.string.data_not_synced));

        }


    }

    private void validateFields(){
        if (issueClosureMo != null && issueClosureMo.getStatusId()!=0){
            if (resources.getString(R.string.Resolved).equalsIgnoreCase(issueClosureMo.getStatusName())) {
                if ( issueClosureMo.getClosureRemarks() != null){
                    updateIssueTable();
                    triggerSyncAdapter();
                    appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
                    IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_complaints));
                    finish();
                }
                else {
                    snackBarWithMesg(buttonsLayout,getResources().getString(R.string.CLOSURE_REMARKS_ERR_MSG));
                }
            }
            else{
                updateIssueTable();
                appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
                IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_complaints));
                finish();
            }

        }


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        MenuItem saveMenu = menu.findItem(R.id.add_post_save);
        if (appPreferences.getAppUserId() == value.getAssignedToId()) {
            if (resources.getString(R.string.Resolved).equalsIgnoreCase(value.getStatus())) {

                saveMenu.setVisible(false);

            }
            else {
                saveMenu.setVisible(true);
            }

        } else {
            saveMenu.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }
    private void triggerSyncAdapter(){
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }
}
