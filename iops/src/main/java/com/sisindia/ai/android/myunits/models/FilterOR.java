package com.sisindia.ai.android.myunits.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterOR {

@SerializedName("Units")
@Expose
private List<Integer> units = new ArrayList<Integer>();

/**
* 
* @return
* The units
*/
public List<Integer> getUnits() {
return units;
}

/**
* 
* @param units
* The Units
*/
public void setUnits(List<Integer> units) {
this.units = units;
}

    @Override
    public String toString() {
        return "FilterOR{" +
                "units=" + units +
                '}';
    }
}