package com.sisindia.ai.android.rota;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myperformance.TimeLineViewAdapter;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConveyanceReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConveyanceReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.conveyance_recyclerview)
    RecyclerView conveyanceRecyclerview;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private Context mContext;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private UpdateRotaTaskModelListener updateRotaTaskModelListener;


    public ConveyanceReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConveyanceReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConveyanceReportFragment newInstance(String param1, String param2) {
        ConveyanceReportFragment fragment = new ConveyanceReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_conveyance_report, container, false);
        ButterKnife.bind(this, view);
        generateTimeLineView();
        return view;
    }



    private void generateTimeLineView() {

        String querydate = dateTimeFormatConversionUtil.getCurrentDate();
        conveyanceRecyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        //appPreference = new App
        List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getActivityLogDB().
                getActivityLogs();

        /*List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getMyPerformanceStatementsDB()
                .getActivityTimelineByDuty("","");*/

        if (timeLineModel != null) {
            conveyanceRecyclerview.setAdapter(new TimeLineViewAdapter(timeLineModel,mContext ));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext =context;
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        if (context instanceof UpdateRotaTaskModelListener)
            updateRotaTaskModelListener = (UpdateRotaTaskModelListener) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
