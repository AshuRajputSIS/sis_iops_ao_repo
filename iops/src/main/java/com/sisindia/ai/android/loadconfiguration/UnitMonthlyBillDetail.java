package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 12/10/16.
 */

public class UnitMonthlyBillDetail {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("UnitName")
    @Expose
    private String unitName;
    @SerializedName("BillNo")
    @Expose
    private String billNo;
    @SerializedName("BillMonth")
    @Expose
    private Integer billMonth;
    @SerializedName("BillYear")
    @Expose
    private Integer billYear;
    @SerializedName("OutstandingAmount")
    @Expose
    private Integer outstandingAmount;
    @SerializedName("OutstandingDays")
    @Expose
    private Integer outstandingDays;
    @SerializedName("UpdatedDate")
    @Expose
    private String updatedDate;

    public int getIsBillCollected() {
        return isBillCollected;
    }

    public void setIsBillCollected(int isBillCollected) {
        this.isBillCollected = isBillCollected;
    }

    private int isBillCollected;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     *
     * @param unitId
     * The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     *
     * @return
     * The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     *
     * @param unitName
     * The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     *
     * @return
     * The billNo
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     *
     * @param billNo
     * The BillNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     *
     * @return
     * The billMonth
     */
    public Integer getBillMonth() {
        return billMonth;
    }

    /**
     *
     * @param billMonth
     * The BillMonth
     */
    public void setBillMonth(Integer billMonth) {
        this.billMonth = billMonth;
    }

    /**
     *
     * @return
     * The billYear
     */
    public Integer getBillYear() {
        return billYear;
    }

    /**
     *
     * @param billYear
     * The BillYear
     */
    public void setBillYear(Integer billYear) {
        this.billYear = billYear;
    }

    /**
     *
     * @return
     * The outstandingAmount
     */
    public Integer getOutstandingAmount() {
        return outstandingAmount;
    }

    /**
     *
     * @param outstandingAmount
     * The OutstandingAmount
     */
    public void setOutstandingAmount(Integer outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    /**
     *
     * @return
     * The outstandingDays
     */
    public Integer getOutstandingDays() {
        return outstandingDays;
    }

    /**
     *
     * @param outstandingDays
     * The OutstandingDays
     */
    public void setOutstandingDays(Integer outstandingDays) {
        this.outstandingDays = outstandingDays;
    }

    /**
     *
     * @return
     * The updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }

    /**
     *
     * @param updatedDate
     * The UpdatedDate
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
