package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SecurityRiskIR {

@SerializedName("UnitId")
@Expose
private Integer unitId;
@SerializedName("UnitTaskId")
@Expose
private Integer unitTaskId;
@SerializedName("SecurityRiskQuestionId")
@Expose
private Integer securityRiskQuestionId;
@SerializedName("SelectedSecurityOptionId")
@Expose
private Integer selectedSecurityOptionId;
@SerializedName("UserComments")
@Expose
private String userComments;

/**
* 
* @return
* The unitId
*/
public Integer getUnitId() {
return unitId;
}

/**
* 
* @param unitId
* The UnitId
*/
public void setUnitId(Integer unitId) {
this.unitId = unitId;
}

/**
* 
* @return
* The unitTaskId
*/
public Integer getUnitTaskId() {
return unitTaskId;
}

/**
* 
* @param unitTaskId
* The UnitTaskId
*/
public void setUnitTaskId(Integer unitTaskId) {
this.unitTaskId = unitTaskId;
}

/**
* 
* @return
* The securityRiskQuestionId
*/
public Integer getSecurityRiskQuestionId() {
return securityRiskQuestionId;
}

/**
* 
* @param securityRiskQuestionId
* The SecurityRiskQuestionId
*/
public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
this.securityRiskQuestionId = securityRiskQuestionId;
}

/**
* 
* @return
* The selectedSecurityOptionId
*/
public Integer getSelectedSecurityOptionId() {
return selectedSecurityOptionId;
}

/**
* 
* @param selectedSecurityOptionId
* The SelectedSecurityOptionId
*/
public void setSelectedSecurityOptionId(Integer selectedSecurityOptionId) {
this.selectedSecurityOptionId = selectedSecurityOptionId;
}

/**
* 
* @return
* The userComments
*/
public String getUserComments() {
return userComments;
}

/**
* 
* @param userComments
* The UserComments
*/
public void setUserComments(String userComments) {
this.userComments = userComments;
}

    @Override
    public String toString() {
        return "SecurityRiskIR{" +
                "unitId=" + unitId +
                ", unitTaskId=" + unitTaskId +
                ", securityRiskQuestionId=" + securityRiskQuestionId +
                ", selectedSecurityOptionId=" + selectedSecurityOptionId +
                ", userComments='" + userComments + '\'' +
                '}';
    }
}