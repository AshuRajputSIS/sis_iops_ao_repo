package com.sisindia.ai.android.database;

/**
 * Created by compass on 10/31/2017.
 */

public class LastAkrDetails {

    private String lastAkrIssueDate;
    private String lastAkrDue;

    public String getLastAkrIssueDate() {
        return lastAkrIssueDate;
    }

    public void setLastAkrIssueDate(String lastAkrIssueDate) {
        this.lastAkrIssueDate = lastAkrIssueDate;
    }

    public String getLastAkrDue() {
        return lastAkrDue;
    }

    public void setLastAkrDue(String lastAkrDue) {
        this.lastAkrDue = lastAkrDue;
    }
}
