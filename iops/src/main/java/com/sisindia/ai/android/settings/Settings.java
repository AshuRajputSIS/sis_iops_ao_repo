package com.sisindia.ai.android.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.debugingtool.IOPSQueryExecutor;
import com.sisindia.ai.android.home.HomeActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shushrut on 27-09-2016.
 */

public class Settings extends BaseActivity {
    /*@Bind(R.id.force_sync)
    public Button forceSyncBtn;
    @Bind(R.id.ai_screen)
    public Button aiLocationBtn;
    @Bind(R.id.image_sync)
    public Button imageSyncBtn;*/
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    //INITIALIZING AND CREATING ONCLICK LISTENER ON EXECUTE QUERY BUTTON
    @OnClick(R.id.executeQuery)
    void executeQuery(View v) {
        // Toast.makeText(this,getResources().getString(R.string.dev_msg),Toast.LENGTH_SHORT).show();
        startActivity(new Intent(Settings.this, IOPSQueryExecutor.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_fragment);
        ButterKnife.bind(this);
        setUpToolBar();
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.setting));
    }

    @OnClick(R.id.force_sync)
    public void onForceSync() {
        startActivity(new Intent(this, TableSyncCountActivity.class));
       /* Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);*/
    }

    @OnClick(R.id.image_sync)
    public void onImageSync() {
        startActivity(new Intent(this, ManualImageSyncing.class));
    }

    @OnClick(R.id.ai_screen)
    public void refreshAILocation() {
        startActivity(new Intent(this, AIGeoLocation.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            callHomeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void callHomeActivity() {
        Intent homeIntent = new Intent(IOPSApplication.getInstance(), HomeActivity.class);
        homeIntent.putExtra(HomeActivity.FRAGMENT_TO_LOAD, 1);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        callHomeActivity();
    }
}
