package com.sisindia.ai.android.issues;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.issues.complaints.CauseOfComplaintEnum;
import com.sisindia.ai.android.issues.complaints.ClientNameSpinnerAdapter;
import com.sisindia.ai.android.issues.complaints.ComplaintBySpinnerAdapter;
import com.sisindia.ai.android.issues.complaints.ComplaintInfoMO;
import com.sisindia.ai.android.issues.complaints.ModeOfComplaintEnum;
import com.sisindia.ai.android.issues.complaints.NatureOfComplaintEnum;
import com.sisindia.ai.android.issues.complaints.ValidationListener;
import com.sisindia.ai.android.issues.improvements.ImprovementPlanEnum;
import com.sisindia.ai.android.issues.models.AddComplaintMO;
import com.sisindia.ai.android.issues.models.ComplaintModel;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Date;

import timber.log.Timber;


/**
 * Created by Durga Prasad on 04-04-2016.
 */
public class AddComplaintsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    public static final int TYPE_ADDITIONAL_REMARKS = 1;
    public static final int TYPE_IMPROVEMENT_PLAN = 3;
    public static final int TYPE_ROW = 2;
    private final AddComplaintMO addComplaintMO;
    private final Util util;
    private ArrayList<Object> addComplaintsList;
    private Context context;
    private LinearLayout compalintListHolder;
    private CustomFontTextview mSelectClient;
    private CustomFontTextview mComplaintType;
    private RecyclerView.ViewHolder recycleViewHolder = null;
    private ShowBottomSheetListener mShowBottomSheetListener;
    private EditText etAddRemarks;
    private RowViewHolder mHolder;
    private ComplaintModel mComplaintModel;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private String[] mHeaderList;
    private String[] mDetailsCatList;
    private String unitName;
    private CalendarView calendar;
    private View CalenderView;
    private FragmentTransaction fragmentTransaction;
    private SelectDateFragment selectDateFragment;
    private FragmentManager fragmentManager;
    private int mUnitId = -1;
    private IssuesMO mIssuesMO;
    private CustomFontTextview mDateTextView;
    private int complaintItemSelected = 0;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<ImprovementActionPlanMO> improvementActionPlanList;
    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private ActionPlanAdapter actionPlanAdapter;
    private String targetdateStr;
    private LinearLayout mCalenderLayout;
    private Spinner mSpinnerActionTaken;
    private Spinner mClientNameSpinner;
    private Spinner mComplaintBy;
    private ArrayList<MyUnitHomeMO> rotaTaskDetail;
    private ValidationListener mValidationListener;


    public AddComplaintsAdapter(Context context, ArrayList<Object> addComplaintsList,
                                LinearLayout compalintListHolder,
                                ShowBottomSheetListener mShowBottomSheetListener,
                                String unitName, IssuesMO issuesMO,
                                ValidationListener mValidationListener) {
        this.context = context;
        this.addComplaintsList = addComplaintsList;
        this.compalintListHolder = compalintListHolder;
        this.mShowBottomSheetListener = mShowBottomSheetListener;
        this.unitName = unitName;
        addComplaintMO = new AddComplaintMO();
        addComplaintMO.setClientName(unitName);
        mComplaintModel = new ComplaintModel();
        mIssuesMO = issuesMO;
        improvementActionPlanList = new ArrayList();
        this.mValidationListener = mValidationListener;
        mValidationListener.setActionPlanSelection(false);
        issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(context);
        mHeaderList = context.getResources().getStringArray(R.array.ADD_COMPLAINT_LABELS);
        mDetailsCatList = context.getResources().getStringArray(R.array.ADD_COMPLAINT_VALUES);
        util = new Util();
    }

    public AddComplaintMO getAddComplaintMO() {
        return addComplaintMO;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setRotaData(ArrayList<Object> addComplaintsList) {
        this.addComplaintsList = addComplaintsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.add_complaints_row, parent, false);
        mSelectClient = (CustomFontTextview) itemView.findViewById(R.id.selectClient);
        mComplaintBy = (Spinner) itemView.findViewById(R.id.spinner_complaint_by);
        mClientNameSpinner = (Spinner) itemView.findViewById(R.id.spinner_client_name);
        populateClinetSpinnerData(mClientNameSpinner, mComplaintBy);
        final Spinner mModeOfComplaint = (Spinner) itemView.findViewById(R.id.spinner_complaint_mode);

        CalenderView = itemView.findViewById(R.id.clender_view);
        mDateTextView = (CustomFontTextview) itemView.findViewById(R.id.edittext_target_date);
        mCalenderLayout = (LinearLayout) CalenderView.findViewById(R.id.calenderLayout);
        mSpinnerActionTaken = (Spinner) itemView.findViewById(R.id.spinner_action_taken);
        TextView addActionPlan = (TextView) itemView.findViewById(R.id.action_plan);

        if (util != null) {
            addActionPlan.setText(util.createSpannableStringBuilder(context.getResources().getString(R.string.add_action_plan)));
        } else {
            Timber.d("Util :" + "Util object is null");
        }

        LinearLayout maddClientNameTypeRelativeLayout = (LinearLayout) itemView.findViewById(R.id.addImprovementTypeRelativeLayout);
        maddClientNameTypeRelativeLayout.setOnClickListener(this);

        populateSpinnerData(context.getResources().getString(R.string.MODE_OF_COMPLAINT), mModeOfComplaint);
        Spinner mTypeOfComplaint = (Spinner) itemView.findViewById(R.id.spinner_complaint_type);
        populateSpinnerData(context.getResources().getString(R.string.CAUSE_OF_COMPLAINT), mTypeOfComplaint);
        Spinner mNatureOfComplaint = (Spinner) itemView.findViewById(R.id.spinner_complaint_nature);
        populateSpinnerData(context.getResources().getString(R.string.NATURE_COMPLAINT), mNatureOfComplaint);
        mComplaintType = (CustomFontTextview) itemView.findViewById(R.id.complaint_type);

        etAddRemarks = (EditText) itemView.findViewById(R.id.editTextTypeRemark);
        etAddRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mIssuesMO.setRemarks(s.toString());
            }
        });

        return new RowViewHolder(itemView);
    }

    private void populatemComplaintBySpinnerData(Spinner mComplaintBy, int mUnitId) {
        if (mUnitId > 0) {
            final ArrayList<ComplaintInfoMO> mComplaintInfoMO = IOPSApplication.getInstance().
                    getSelectionStatementDBInstance().getClientContactNames(mUnitId);
            ComplaintBySpinnerAdapter mComplaintSpinnerAdapter = new ComplaintBySpinnerAdapter(context, 0, mComplaintInfoMO);
           /* if (mComplaintInfoMO.size() > 0) {
                mIssuesMO.setUnitContactId(mComplaintInfoMO.get(0).getmUnitContactId());
                mIssuesMO.setUnitContactName(mComplaintInfoMO.get(0).getmFirstName());
            }*/
            mComplaintBy.setAdapter(mComplaintSpinnerAdapter);
            mComplaintBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (mComplaintInfoMO.size() > 0) {
                        mIssuesMO.setUnitContactId(mComplaintInfoMO.get(position).getmUnitContactId());
                        mIssuesMO.setUnitContactName(mComplaintInfoMO.get(position).getmFirstName() +" "+mComplaintInfoMO.get(position).getmLastName() );
                        mIssuesMO.setComplaintBy(mComplaintInfoMO.get(position).getmFirstName() +" "+mComplaintInfoMO.get(position).getmLastName());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void populateClinetSpinnerData(Spinner mClientNameSpinner, final Spinner mComplaintBy) {
        rotaTaskDetail = new ArrayList<>();
        if (!AddComplaintActivity.isComplaintFromUnitLevel) {
            rotaTaskDetail = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitDetails(0);
        } else {
            MyUnitHomeMO myUnitHomeMO = new MyUnitHomeMO();
            myUnitHomeMO.setUnitName(AddComplaintActivity.mUnitName);
            myUnitHomeMO.setUnitId(AddComplaintActivity.mUnitId);
            rotaTaskDetail.add(myUnitHomeMO);
            if (AddComplaintActivity.mUnitId > 0) {
                populatemComplaintBySpinnerData(mComplaintBy, AddComplaintActivity.mUnitId);
            }
            mClientNameSpinner.setEnabled(false);
            mClientNameSpinner.setClickable(false);
            mClientNameSpinner.setBackgroundColor(context.getResources().getColor(R.color.white));

        }
        ClientNameSpinnerAdapter mClientNameSpinnerAdapter = new ClientNameSpinnerAdapter(context, 0, rotaTaskDetail);
        mClientNameSpinner.setAdapter(mClientNameSpinnerAdapter);
        mClientNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mUnitId = rotaTaskDetail.get(position).getUnitId();
                Timber.e("Client name %s", "Client name ::" + rotaTaskDetail.get(position).getUnitName());
                mIssuesMO.setUnitName(rotaTaskDetail.get(position).getUnitName());
                mIssuesMO.setUnitId(rotaTaskDetail.get(position).getUnitId());
                if (mUnitId > 0) {
                    populatemComplaintBySpinnerData(mComplaintBy, mUnitId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void populateSpinnerData(final String lookUpType, final Spinner mSpinnerInstance) {
        LookUpSpinnerAdapter mAdapter = null;
        if(lookUpType.equalsIgnoreCase(context.getResources().getString(R.string.CAUSE_OF_COMPLAINT))){
            mAdapter = getCauseOfSpinnerData(lookUpType, mSpinnerInstance);
        }
        else{
           mAdapter = getSpinnerData(lookUpType, mSpinnerInstance);
        }
        mSpinnerInstance.setAdapter(mAdapter);
    }

    private void setValuesToModel(String lookUpType, Integer lookUpValueId) {
        switch (lookUpType) {
            case "ModeOfComplaint":
                mIssuesMO.setIssueModeId(lookUpValueId);
                break;
            case "CauseOfComplaint":
                mIssuesMO.setIssueCauseId(lookUpValueId);
                break;
            case "NatureOfComplaint":
                mIssuesMO.setIssueNatureId(lookUpValueId);
                populateImprovementPlanSpinner(mIssuesMO.getIssueNatureId());
                break;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_ROW:
                RowViewHolder mHolder = (RowViewHolder) holder;
                this.mHolder = mHolder;
                break;
        }
    }

    @Override
    public int getItemCount() {
        int count = context.getResources().getInteger(R.integer.number_1);
        if (addComplaintsList != null) {
            count = addComplaintsList.size() == 0 ? 0 : addComplaintsList.size();
        }
        return count;
    }

    private Object getObject(int position) {
        return addComplaintsList.get(position);
    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.addClientNameTypeRelativeLayout) {


        } else if (view.getId() == R.id.modeOfComplaintRelativeLayout) {

            ModeOfComplaintEnum[] modeOfComplaintEms = ModeOfComplaintEnum.values();
            ArrayList<String> element = new ArrayList<String>();
            for (ModeOfComplaintEnum modeOfComplaintEm : modeOfComplaintEms) {
                element.add(modeOfComplaintEm.name());
            }
            mShowBottomSheetListener.showBottomSheet(element, view.getId());
        } else if (view.getId() == R.id.natureofcomplaintRelativeLayout) {
            NatureOfComplaintEnum[] natureOfComplaintList = NatureOfComplaintEnum.values();
            ArrayList<String> element = new ArrayList<String>();
            for (NatureOfComplaintEnum natureOfComplaint_em : natureOfComplaintList) {
                element.add(natureOfComplaint_em.name().replace("_", " "));
            }
            mShowBottomSheetListener.showBottomSheet(element, view.getId());
        } else if (view.getId() == R.id.causeRelativeLayout) {
            CauseOfComplaintEnum[] causeOfComplaint_ena = CauseOfComplaintEnum.values();
            ArrayList<String> element = new ArrayList<String>();
            for (CauseOfComplaintEnum causeOfComplaint_enum : causeOfComplaint_ena) {
                element.add(causeOfComplaint_enum.name().replace("_", " "));
            }
            mShowBottomSheetListener.showBottomSheet(element, view.getId());
        }


    }

    public void setValue(String value, int viewId) {


        switch (viewId) {
            case R.id.addClientNameTypeRelativeLayout:
                mSelectClient.setText(value);

                // equipmentList = value;
                break;
            case R.id.addImprovementTypeRelativeLayout:
                // mSelectImprovementPlan.setText(value);
                addComplaintMO.setImprovementPlanId(ImprovementPlanEnum.valueOf(value.replace(" ", "_")).getValue());
                break;
            case R.id.modeOfComplaintRelativeLayout:
                mComplaintType.setText(value);
                addComplaintMO.setModeOfComplaintId(1);
                break;
            case R.id.natureofcomplaintRelativeLayout:
                addComplaintMO.setNatureOfComplaintId(NatureOfComplaintEnum.valueOf(value.replace(" ", "_")).getValue());

                // equipmentList = value;
                break;
            case R.id.causeRelativeLayout:
                addComplaintMO.setCauseOfComplaintId(CauseOfComplaintEnum.valueOf(value.replace(" ", "_")).getValue());
                // equipmentList = value;
                break;

        }

    }

    public class RowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CustomFontTextview mMainTitleText, mSubTitleText;
        ImageView typeIcon, nextIcon;

        public RowViewHolder(View view) {
            super(view);
            mMainTitleText = (CustomFontTextview) view.findViewById(R.id.clienName);
            mSubTitleText = (CustomFontTextview) view.findViewById(R.id.selectClient);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView dutyoffTxt;
        RelativeTimeTextView lastSeenTxt;
        Switch mSwitch;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            this.dutyoffTxt = (TextView) itemView.findViewById(R.id.dutyoff_txt);
            this.lastSeenTxt = (RelativeTimeTextView) itemView.findViewById(R.id.lastseen_txt);
            this.mSwitch = (Switch) itemView.findViewById(R.id.switch1);

        }


    }

    public LookUpSpinnerAdapter getSpinnerData(final String lookupData, Spinner mSpinnerInstance) {


        final ArrayList<LookupModel> lookupModelArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(lookupData, null);
        LookUpSpinnerAdapter mComplaintsSpinnerAdapter = new LookUpSpinnerAdapter(context);
        mComplaintsSpinnerAdapter.setSpinnerData(lookupModelArrayList);

        mSpinnerInstance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position!=0 || position!=-1){
                    setValuesToModel(lookupData, lookupModelArrayList.get(position).getLookupIdentifier());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return mComplaintsSpinnerAdapter;
    }



    public LookUpSpinnerAdapter getCauseOfSpinnerData(final String lookupData, Spinner mSpinnerInstance) {

        final ArrayList<LookupModel> lookupModelArrayList = new ArrayList<>();

        if(lookupData.equalsIgnoreCase(context.getResources().getString(R.string.CAUSE_OF_COMPLAINT))){
            LookupModel lookupModel = new LookupModel();
            lookupModel.setName("Select cause of complaint");
            lookupModelArrayList.add(lookupModel);
        }
        lookupModelArrayList.addAll(IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(lookupData, null));
        LookUpSpinnerAdapter mComplaintsSpinnerAdapter = new LookUpSpinnerAdapter(context);


        mComplaintsSpinnerAdapter.setSpinnerData(lookupModelArrayList);

        mSpinnerInstance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position!=0 ){
                    setValuesToModel(lookupData, lookupModelArrayList.get(position).getLookupIdentifier());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return mComplaintsSpinnerAdapter;
    }

    private void populateImprovementPlanSpinner(int issuecategoryId) {
        improvementActionPlanList.clear();
       /* final ImprovementActionPlanMO improvementActionPlanMO = new ImprovementActionPlanMO();
        improvementActionPlanMO.setName("Select Improvement Plan");
        improvementActionPlanList.add(improvementActionPlanMO);*/
        if (!((-1 == issuecategoryId))) {
            improvementActionPlanList.addAll(issueLevelSelectionStatementDB.getImprovementActionPlanForComplaint(mIssuesMO.getUnitId(), 2, issuecategoryId));
        }
        mValidationListener.setValidationStatus(true);
        if (improvementActionPlanList.size() < 2 && complaintItemSelected > 2) {
           // mValidationListener.showSnackBar(context.getString(R.string.combination_msg));
            mValidationListener.setValidationStatus(false);
        }else if(improvementActionPlanList.size()< 2){
            mValidationListener.setValidationStatus(false);
        }
        if (actionPlanAdapter == null) {
            actionPlanAdapter = new ActionPlanAdapter(context);

            actionPlanAdapter.setSpinnerData(improvementActionPlanList);

            mSpinnerActionTaken.setAdapter(actionPlanAdapter);
        } else {
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            mSpinnerActionTaken.setAdapter(actionPlanAdapter);
        }

        mSpinnerActionTaken.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(improvementActionPlanList.get(position).getName()!= improvementActionPlanList.get(0).getName()) {
                    mValidationListener.setActionPlanSelection(true);
                    displayActionDate(improvementActionPlanList.get(position).getTurnAroundTime());
                    mIssuesMO.setActionPlan(improvementActionPlanList.get(position).getName());
                    mIssuesMO.setActionPlanId(improvementActionPlanList.get(position).getId());
                    mIssuesMO.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                }else{
                    mValidationListener.setActionPlanSelection(false);
                }
}

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void displayActionDate(int days) {
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        targetdateStr = dateTimeFormatConversionUtil.addDaysToCurrentDate(days);
        mDateTextView.setText(targetdateStr);
        mIssuesMO.setTargetActionEndDate(mDateTextView.getText().toString());
        mCalenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });

    }

    public void showCalendar() {
        Date mindate = dateTimeFormatConversionUtil.convertStringToDateFormat(dateTimeFormatConversionUtil.getCurrentDate());
        Date maxDate = dateTimeFormatConversionUtil.convertStringToDateFormat(targetdateStr);
        fragmentTransaction = ((AddComplaintActivity) context).getSupportFragmentManager().beginTransaction();
        DateRangeCalender selectDateFragment = DateRangeCalender.newInstance(mindate, maxDate);
        selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                String updateDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                mDateTextView.setText(updateDate);
            }
        });
    }
}

