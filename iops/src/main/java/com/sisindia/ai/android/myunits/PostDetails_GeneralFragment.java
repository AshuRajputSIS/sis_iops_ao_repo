package com.sisindia.ai.android.myunits;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.myunits.models.EquipmentListModel;
import com.sisindia.ai.android.myunits.models.PostGeneralModel;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.myunits.models.UnitPostChecking;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by shushruth on 25/3/16.
 */
public class PostDetails_GeneralFragment extends Fragment {
    private final int UPDATE_SOP_DATA = 0;
    private final int UPDATE_ATTENDENCE_DATA = 1;
    View mGaurdFragmentView;
    LinearLayout mparent_org_groupview;
    RadioGroup mRadioGroup_SOP;
    RadioGroup mRadioGroup_attendance;
    @Bind(R.id.nfcscan)
    CustomFontButton mnfcscan;
    ArrayList<EquipmentListModel> model;
    private String postName;
    private UnitPost mUnitPostDataHolder;
    private UnitPostChecking mUnitPostChecking;
    private ListView mEquipmentList;
    private TextView mno_dataexists;
    private NonOperationalEquipmentListAdapter mEquipmentListAdapter;
    private RotaTaskModel mRotaTaskModel;

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mGaurdFragmentView = inflater.inflate(R.layout.postdetails_generalfragment, container, false);
        TextView postNameTextview = (TextView) mGaurdFragmentView.findViewById(R.id.general_postname);
        mUnitPostDataHolder = PostGeneralModel.getmPostGeneralModelInstance().mGetUnitPostModel();
        mRotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        mEquipmentList = (ListView) mGaurdFragmentView.findViewById(R.id.list_equipment);
        mno_dataexists = (TextView) mGaurdFragmentView.findViewById(R.id.no_dataexists);
        mEquipmentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                model.get(position);
            }
        });
        mEquipmentListAdapter = new NonOperationalEquipmentListAdapter(fetchEquipmentList(), getContext(), mUnitPostDataHolder);

        mEquipmentList.setAdapter(mEquipmentListAdapter);
        mEquipmentListAdapter.notifyDataSetChanged();

        mRadioGroup_SOP = (RadioGroup) mGaurdFragmentView.findViewById(R.id.radio_group_sop_available);
        mRadioGroup_attendance = (RadioGroup) mGaurdFragmentView.findViewById(R.id.radio_attendence_available);
        PostGeneralModel.getmPostGeneralModelInstance().setRgSopAvailable(mRadioGroup_SOP);
        PostGeneralModel.getmPostGeneralModelInstance().setRgAttendance(mRadioGroup_attendance);
        mRadioGroup_SOP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (mRadioGroup_SOP.getCheckedRadioButtonId()) {
                    case R.id.radio_group_sop_available_yes:
                        notifyDataModelHolder(0, UPDATE_SOP_DATA);
                        break;
                    case R.id.radio_group_sop_available_no:
                        notifyDataModelHolder(1, UPDATE_SOP_DATA);
                        break;
                }
            }
        });
        mRadioGroup_attendance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (mRadioGroup_attendance.getCheckedRadioButtonId()) {
                    case R.id.radio_attendence_available_yes:
                        notifyDataModelHolder(0, UPDATE_ATTENDENCE_DATA);
                        break;
                    case R.id.radio_attendence_available_no:
                        notifyDataModelHolder(1, UPDATE_ATTENDENCE_DATA);
                        break;
                }
            }
        });
        postNameTextview.setText(postName);
       // mUnitPostDataHolder.setPostName(postName);
       /// mUnitPostDataHolder.setNFC("#$45%67778888888888");

        mparent_org_groupview = (LinearLayout) mGaurdFragmentView.findViewById(R.id.parent_org_groupview);
        mparent_org_groupview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent i =  new Intent(getContext(), PostNameActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);*/
            }
        });
        return mGaurdFragmentView;
    }

    private ArrayList<EquipmentListModel> fetchEquipmentList() {
        model = new ArrayList<>();
        model = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchEquipmentList(mRotaTaskModel.getUnitId(), Util.getmPostData());
        /*EquipmentListModel mModel = new EquipmentListModel();
        for(int i =0;i<4;i++){
            String name ="Equipment_NAME "+i;
            mModel.setEquipmentName(name);
            model.add(mModel);
        }*/
        //  Log.e("mmodel.getIsCustomerProvided()", "mmodel.getIsCustomerProvided()::" + IOPSApplication.getGsonInstance().toJson(model));
       /* for(EquipmentListModel mmodel : model){
            Log.e("mmodel.getIsCustomerProvided()","mmodel.getIsCustomerProvided()::"+mmodel.getIsCustomerProvided());
            mmodel.getIsCustomerProvided();
        }*/
        // ArrayList<EquipmentListModel>bmodel =  fetchEquipmentList();
        /*mEquipmentListAdapter = new NonOperationalEquipmentListAdapter(model,getContext(),mUnitPostDataHolder);
        mEquipmentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                model.get(position);
                Toast.makeText(getContext(), "position ::" + position, Toast.LENGTH_SHORT).show();
            }
        });
        mEquipmentList.setAdapter(mEquipmentListAdapter);
        mEquipmentListAdapter.notifyDataSetChanged();*/
        if (model.size() == 0) {
            mno_dataexists.setVisibility(View.VISIBLE);
            mEquipmentList.setVisibility(View.GONE);
        } else {
            mno_dataexists.setVisibility(View.GONE);
            mEquipmentList.setVisibility(View.VISIBLE);
        }
        return model;
    }

    @OnClick(R.id.nfcscan)
    public void scanNFCTag() {
      //  mUnitPostDataHolder.setNFC("#$45%67778888888888");
    }

    /**
     * Method to onDutyChange the unitPost model based on the item selected
     *
     * @param pos
     * @param modelToUpdate
     */
    private void notifyDataModelHolder(int pos, int modelToUpdate) {
        boolean msg = false;
        switch (pos) {
            case 0:
                msg = true;
                updateModelHolder(modelToUpdate, msg);
                break;
            case 1:
                msg = false;
                updateModelHolder(modelToUpdate, msg);
                break;
        }
        // Toast.makeText(getContext(), "Selected :"+msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method which updates the model with selection.
     *
     * @param modelToUpdate
     * @param msg
     */
    private void updateModelHolder(int modelToUpdate, boolean msg) {
        switch (modelToUpdate) {
            case 0:
               // mUnitPostDataHolder.setSOPAvailable(msg);
                //   Toast.makeText(getContext(), "mUnitPostDataHolder.getSOPAvailable() :"+ mUnitPostDataHolder.getSOPAvailable(), Toast.LENGTH_SHORT).show();
                break;
            case 1:
             //   mUnitPostDataHolder.setAttendanceRegisterAvailable(msg);
                //  Toast.makeText(getContext(), "mUnitPostDataHolder.getAttendanceRegisterAvailable() :"+ mUnitPostDataHolder.getAttendanceRegisterAvailable(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void setUpPostHolder(UnitPost postData) {
        mUnitPostDataHolder = postData;
    }
}
