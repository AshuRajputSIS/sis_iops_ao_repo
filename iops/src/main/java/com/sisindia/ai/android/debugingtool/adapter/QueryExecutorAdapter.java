package com.sisindia.ai.android.debugingtool.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.Collections;
import java.util.List;

import static com.sisindia.ai.android.R.id.date;

/**
 * Created by Ashu Rajput on 8/31/2017.
 */

public class QueryExecutorAdapter extends RecyclerView.Adapter<QueryExecutorAdapter.QueryExecutorViewHolder> {

    private LayoutInflater layoutInflater = null;
    private List<QueryExecutorMO> queryExecutorDetails = Collections.emptyList();
    private ExecutableQueryListener listener;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public QueryExecutorAdapter(Context context, List<QueryExecutorMO> queryExecutorDetails) {
        layoutInflater = LayoutInflater.from(context);
        this.queryExecutorDetails = queryExecutorDetails;
        this.listener = (ExecutableQueryListener) context;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    @Override
    public QueryExecutorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.query_executor_single_row, parent, false);
        QueryExecutorViewHolder holder = new QueryExecutorViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(QueryExecutorViewHolder holder, int position) {

        holder.queryLabel.setText(queryExecutorDetails.get(position).getName());
        //holder.queryValue.setText(queryExecutorDetails.get(position).getQuery());
        holder.queryValue.setText(dateTimeFormatConversionUtil.getCurrentDate());
        if (queryExecutorDetails.get(position).getQueryResult() != null &&
                queryExecutorDetails.get(position).getQueryResult().equals("1")){
            holder.executeQueryButton.setText("Executed");
            holder.executeQueryButton.setEnabled(false);
            holder.executeQueryButton.setBackgroundResource(R.drawable.bg_round_green);
        }
    }

    @Override
    public int getItemCount() {
        return queryExecutorDetails.size();
    }

    public class QueryExecutorViewHolder extends RecyclerView.ViewHolder {

        private TextView queryValue, queryLabel, executeQueryButton;

        public QueryExecutorViewHolder(View itemView) {
            super(itemView);
            queryValue = (TextView) itemView.findViewById(R.id.queryValue);
            queryLabel = (TextView) itemView.findViewById(R.id.queryLabel);
            executeQueryButton = (TextView) itemView.findViewById(R.id.executeQueryButton);

            executeQueryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null && queryExecutorDetails != null && queryExecutorDetails.size() > 0) {
                        listener.executeQuery(getLayoutPosition(), queryExecutorDetails.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    public interface ExecutableQueryListener {
        //        void executeQuery(int position, String queryToBeExecuted);
        void executeQuery(int position, QueryExecutorMO queryExecutorMO);
    }

}
