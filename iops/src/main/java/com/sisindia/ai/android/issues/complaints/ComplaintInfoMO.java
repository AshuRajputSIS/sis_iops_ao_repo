package com.sisindia.ai.android.issues.complaints;

/**
 * Created by Shushrut on 01-08-2016.
 */
public class ComplaintInfoMO {
    private String mFirstName;
    private String mLastName;
    private int mUnitContactId;

    public int getmUnitContactId() {
        return mUnitContactId;
    }

    public void setmUnitContactId(int mUnitContactId) {
        this.mUnitContactId = mUnitContactId;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }
}
