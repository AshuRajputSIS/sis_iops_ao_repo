package com.sisindia.ai.android.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;

import me.relex.circleindicator.CircleIndicator;

import static com.sisindia.ai.android.onboarding.OnBoardingFactory.MODULE_NAME_KEY;
import static com.sisindia.ai.android.onboarding.OnBoardingFactory.NO_OF_IMAGES;


public class OnBoardingFragment extends BaseFragment {

    private String moduleName;
    private int noOfImages;

    public static OnBoardingFragment newInstance(String moduleName, int noOfImages) {
        OnBoardingFragment fragment = new OnBoardingFragment();
        Bundle args = new Bundle();
        args.putString(MODULE_NAME_KEY, moduleName);
        args.putInt(NO_OF_IMAGES, noOfImages);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            moduleName = getArguments().getString(MODULE_NAME_KEY);
            noOfImages = getArguments().getInt(NO_OF_IMAGES);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.on_boarding_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkCurrentOnBoardingStatus();

        final ViewPager vpOnboarding = (ViewPager) view.findViewById(R.id.vp_onboarding);
        final CircleIndicator pageIndicator = (CircleIndicator) view.findViewById(R.id.cpi_onboarding);
        final OnBoardingPagerAdapter adapter = new OnBoardingPagerAdapter(getChildFragmentManager());

        vpOnboarding.setAdapter(adapter);
        pageIndicator.setViewPager(vpOnboarding);

        final TextView tvSkip = (TextView) view.findViewById(R.id.tv_skip);
        final TextView tvContinue = (TextView) view.findViewById(R.id.tv_continue);

        if (noOfImages == 1) {
            pageIndicator.setVisibility(View.INVISIBLE);
            showDoneButton(tvSkip, tvContinue, getString(R.string.done), View.INVISIBLE);
        }

        skipClicked(tvSkip);

        continueClicked(vpOnboarding, tvContinue);

        screenScrolled(vpOnboarding, tvSkip, tvContinue);
    }

    private void skipClicked(final TextView tvSkip) {
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreferences preferences = new AppPreferences(getActivity());
                if (tvSkip.getText().toString().equals(getString(R.string.done))) {
                    preferences.setOnBoardingStatus(moduleName, true);
                } else {
                    preferences.setOnBoardingStatus(moduleName, false);
                }
                Intent intent = OnBoardingFactory.getIntent(moduleName);

                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void continueClicked(final ViewPager vpOnboarding, TextView tvContinue) {
        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentItem = vpOnboarding.getCurrentItem();
                vpOnboarding.setCurrentItem(currentItem + 1, true);
            }
        });
    }

    private void screenScrolled(ViewPager vpOnboarding, final TextView tvSkip, final TextView tvContinue) {
        vpOnboarding.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                // Check if the user is at the last page and change skip to done
                if (position == noOfImages - 1 || noOfImages == 1) {
                    showDoneButton(tvSkip, tvContinue, getString(R.string.done), View.INVISIBLE);
                } else {
                    showDoneButton(tvSkip, tvContinue, getString(R.string.skip), View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void showDoneButton(TextView tvSkip, TextView tvContinue, String string, int invisible) {
        tvSkip.setText(string);
        tvContinue.setVisibility(invisible);
    }

    private void checkCurrentOnBoardingStatus() {
        AppPreferences preferences = new AppPreferences(getActivity());
        if (preferences.getOnBoardingStatus(moduleName)) {
            Intent intent = OnBoardingFactory.getIntent(moduleName);
            startActivity(intent);
            getActivity().finish();
        }
    }


    private class OnBoardingPagerAdapter extends FragmentPagerAdapter {

        OnBoardingPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            OnboardingModel onboardingModel = OnBoardingFactory.getOnboardingModel(moduleName, noOfImages);
            return OnBoardingItemFragment.newInstance(onboardingModel.imagesList.get(position),
                    onboardingModel.titlesList.get(position), onboardingModel.descList.get(position));
        }

        @Override
        public int getCount() {
            return noOfImages;
        }
    }
}