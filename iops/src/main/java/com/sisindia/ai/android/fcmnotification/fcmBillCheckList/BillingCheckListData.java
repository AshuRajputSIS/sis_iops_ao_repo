package com.sisindia.ai.android.fcmnotification.fcmBillCheckList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.UnitBillingCheckList;

import java.util.List;

/**
 * Created by Saruchi on 13-01-2017.
 */

public class BillingCheckListData {
    @SerializedName("UnitBillingCheckList")
    @Expose
    private List<UnitBillingCheckList> unitBillingCheckList = null;

    public List<UnitBillingCheckList> getUnitBillingCheckList() {
        return unitBillingCheckList;
    }

    public void setUnitBillingCheckList(List<UnitBillingCheckList> unitBillingCheckList) {
        this.unitBillingCheckList = unitBillingCheckList;
    }

}
