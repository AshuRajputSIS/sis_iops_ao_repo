package com.sisindia.ai.android.issues.models;

import com.google.gson.annotations.SerializedName;

public class IssuesBaseIR {

@SerializedName("IssueTypeId")

private String issueTypeId;
@SerializedName("IssueMatrixId")

private Integer issueMatrixId;
@SerializedName("Description")

private String description;
@SerializedName("UnitId")

private String unitId;
@SerializedName("BarrackId")

private Object barrackId;
@SerializedName("SourceTaskId")

private String sourceTaskId;
@SerializedName("UnitEmployeeId")

private String unitEmployeeId;
@SerializedName("IssueSourceId")

private String issueSourceId;
@SerializedName("IssueStatus")

private String issueStatus;
@SerializedName("RaisedDateTime")

private String raisedDateTime;
@SerializedName("AssignedToDateTime")

private String assignedToDateTime;
@SerializedName("ClosureDateTime")

private String closureDateTime;
@SerializedName("TargetActionDateTime")

private String targetActionDateTime;
@SerializedName("IssueCauseId")

private Object issueCauseId;
@SerializedName("IssueCategoryId")

private Integer issueCategoryId;
@SerializedName("IssueSeverityId")

private String issueSeverityId;
@SerializedName("IssueNatureId")

private String issueNatureId;
@SerializedName("Remarks")

private String remarks;
@SerializedName("IssueCheckList")

private String issueCheckList;
@SerializedName("GuardId")

private String guardId;
@SerializedName("IssueActionId")

private String issueActionId;

/**
* 
* @return
* The issueTypeId
*/
public String getIssueTypeId() {
return issueTypeId;
}

/**
* 
* @param issueTypeId
* The IssueTypeId
*/
public void setIssueTypeId(String issueTypeId) {
this.issueTypeId = issueTypeId;
}

/**
* 
* @return
* The issueMatrixId
*/
public Integer getIssueMatrixId() {
return issueMatrixId;
}

/**
* 
* @param issueMatrixId
* The IssueMatrixId
*/
public void setIssueMatrixId(Integer issueMatrixId) {
this.issueMatrixId = issueMatrixId;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The Description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The unitId
*/
public String getUnitId() {
return unitId;
}

/**
* 
* @param unitId
* The UnitId
*/
public void setUnitId(String unitId) {
this.unitId = unitId;
}

/**
* 
* @return
* The barrackId
*/
public Object getBarrackId() {
return barrackId;
}

/**
* 
* @param barrackId
* The BarrackId
*/
public void setBarrackId(Object barrackId) {
this.barrackId = barrackId;
}

/**
* 
* @return
* The sourceTaskId
*/
public String getSourceTaskId() {
return sourceTaskId;
}

/**
* 
* @param sourceTaskId
* The SourceTaskId
*/
public void setSourceTaskId(String sourceTaskId) {
this.sourceTaskId = sourceTaskId;
}

/**
* 
* @return
* The unitEmployeeId
*/
public String getUnitEmployeeId() {
return unitEmployeeId;
}

/**
* 
* @param unitEmployeeId
* The UnitEmployeeId
*/
public void setUnitEmployeeId(String unitEmployeeId) {
this.unitEmployeeId = unitEmployeeId;
}

/**
* 
* @return
* The issueSourceId
*/
public String getIssueSourceId() {
return issueSourceId;
}

/**
* 
* @param issueSourceId
* The IssueSourceId
*/
public void setIssueSourceId(String issueSourceId) {
this.issueSourceId = issueSourceId;
}

/**
* 
* @return
* The issueStatus
*/
public String getIssueStatus() {
return issueStatus;
}

/**
* 
* @param issueStatus
* The IssueStatus
*/
public void setIssueStatus(String issueStatus) {
this.issueStatus = issueStatus;
}

/**
* 
* @return
* The raisedDateTime
*/
public String getRaisedDateTime() {
return raisedDateTime;
}

/**
* 
* @param raisedDateTime
* The RaisedDateTime
*/
public void setRaisedDateTime(String raisedDateTime) {
this.raisedDateTime = raisedDateTime;
}

/**
* 
* @return
* The assignedToDateTime
*/
public String getAssignedToDateTime() {
return assignedToDateTime;
}

/**
* 
* @param assignedToDateTime
* The AssignedToDateTime
*/
public void setAssignedToDateTime(String assignedToDateTime) {
this.assignedToDateTime = assignedToDateTime;
}

/**
* 
* @return
* The closureDateTime
*/
public String getClosureDateTime() {
return closureDateTime;
}

/**
* 
* @param closureDateTime
* The ClosureDateTime
*/
public void setClosureDateTime(String closureDateTime) {
this.closureDateTime = closureDateTime;
}

/**
* 
* @return
* The targetActionDateTime
*/
public String getTargetActionDateTime() {
return targetActionDateTime;
}

/**
* 
* @param targetActionDateTime
* The TargetActionDateTime
*/
public void setTargetActionDateTime(String targetActionDateTime) {
this.targetActionDateTime = targetActionDateTime;
}

/**
* 
* @return
* The issueCauseId
*/
public Object getIssueCauseId() {
return issueCauseId;
}

/**
* 
* @param issueCauseId
* The IssueCauseId
*/
public void setIssueCauseId(Object issueCauseId) {
this.issueCauseId = issueCauseId;
}

/**
* 
* @return
* The issueCategoryId
*/
public Integer getIssueCategoryId() {
return issueCategoryId;
}

/**
* 
* @param issueCategoryId
* The IssueCategoryId
*/
public void setIssueCategoryId(Integer issueCategoryId) {
this.issueCategoryId = issueCategoryId;
}

/**
* 
* @return
* The issueSeverityId
*/
public String getIssueSeverityId() {
return issueSeverityId;
}

/**
* 
* @param issueSeverityId
* The IssueSeverityId
*/
public void setIssueSeverityId(String issueSeverityId) {
this.issueSeverityId = issueSeverityId;
}

/**
* 
* @return
* The issueNatureId
*/
public String getIssueNatureId() {
return issueNatureId;
}

/**
* 
* @param issueNatureId
* The IssueNatureId
*/
public void setIssueNatureId(String issueNatureId) {
this.issueNatureId = issueNatureId;
}

/**
* 
* @return
* The remarks
*/
public String getRemarks() {
return remarks;
}

/**
* 
* @param remarks
* The Remarks
*/
public void setRemarks(String remarks) {
this.remarks = remarks;
}

/**
* 
* @return
* The issueCheckList
*/
public String getIssueCheckList() {
return issueCheckList;
}

/**
* 
* @param issueCheckList
* The IssueCheckList
*/
public void setIssueCheckList(String issueCheckList) {
this.issueCheckList = issueCheckList;
}

/**
* 
* @return
* The guardId
*/
public String getGuardId() {
return guardId;
}

/**
* 
* @param guardId
* The GuardId
*/
public void setGuardId(String guardId) {
this.guardId = guardId;
}

/**
* 
* @return
* The issueActionId
*/
public String getIssueActionId() {
return issueActionId;
}

/**
* 
* @param issueActionId
* The IssueActionId
*/
public void setIssueActionId(String issueActionId) {
this.issueActionId = issueActionId;
}

}