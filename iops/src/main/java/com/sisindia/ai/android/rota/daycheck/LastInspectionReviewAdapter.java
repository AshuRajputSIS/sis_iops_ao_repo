package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.issues.models.NoDataAvailable;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 26-03-2016.
 */
public class LastInspectionReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String LAST_NIGHT_CHECK = "Last Night Check";
    private static final String LAST_DAY_CHECK = "Last Day Check";
    private static final String LAST_BARRACK_CHECK = "Last Barrack Inspection Check";
    private static final String LAST_CLIENT_CHECK = "Last Client Coordination Check";

    private final int taskTypeId;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<Object> addCheckingDataList;
    private Context context;
    private static final int TYPE_LAST_INSPECTION_HEADER = 1;
    private static final int TYPE_NO_DATA_AVAILABLE = 2;
    private static final int TYPE_CARDVIEW_GRIEVANCES = 3;
    private static final int TYPE_CARDVIEW_COMPLAINTS = 4;
    private static final int TYPE_CARDVIEW_UNIT_AT_RISK = 5;
    private static final int TYPE_HEADER = 6;
    private static final int TYPE_IMPROVEMENT_PLAN = 7;
    private static final int TYPE_KIT_ITEM_PLAN = 8;
//    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public LastInspectionReviewAdapter(Context context, int tasktypeid) {
        this.context = context;
        this.taskTypeId = tasktypeid;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

    }

    public void setCheckingData(ArrayList<Object> addCheckingDataList) {
        this.addCheckingDataList = addCheckingDataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView;

        switch (viewType) {
            case TYPE_LAST_INSPECTION_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.lastcheck_inspection_header, parent, false);
                recycleViewHolder = new LastInspectionHeaderViewHolder(itemView);
                break;

            case TYPE_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.lastcheck_inspection_middle_headers, parent, false);
                recycleViewHolder = new LastInspectionIssueHeaderViewHolder(itemView);
                break;

            case TYPE_CARDVIEW_GRIEVANCES:
                itemView = LayoutInflater.from(context).inflate(R.layout.last_inspection_cardview, parent, false);
                recycleViewHolder = new LastInspectionGrievancesHolder(itemView);
                break;

            case TYPE_CARDVIEW_COMPLAINTS:
                itemView = LayoutInflater.from(context).inflate(R.layout.last_inspection_complaint_cardview, parent, false);
                recycleViewHolder = new LastInspectionComplaintsHolder(itemView);
                break;
            case TYPE_CARDVIEW_UNIT_AT_RISK:
                itemView = LayoutInflater.from(context).inflate(R.layout.last_inspection_unitat_risk_carview, parent, false);
                recycleViewHolder = new LastInspectionUnitAtRiskHolder(itemView);
                break;

            case TYPE_NO_DATA_AVAILABLE:
                itemView = LayoutInflater.from(context).inflate(R.layout.nodata_available, parent, false);
                recycleViewHolder = new NoDataAvailableViewHolder(itemView);
                break;

            case TYPE_IMPROVEMENT_PLAN:
                itemView = LayoutInflater.from(context).inflate(R.layout.last_inspection_complaint_cardview, parent, false);
                recycleViewHolder = new LastImprovementPlanHolder(itemView);
                break;
            case TYPE_KIT_ITEM_PLAN:
                itemView = LayoutInflater.from(context).inflate(R.layout.last_inspection_complaint_cardview, parent, false);
                recycleViewHolder = new LastInspectionComplaintsHolder(itemView);
                break;
        }
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = addCheckingDataList.get(position);
        if (holder instanceof LastInspectionIssueHeaderViewHolder && object instanceof String) {
            LastInspectionIssueHeaderViewHolder lastInspectionIssueHeaderViewHolder = (LastInspectionIssueHeaderViewHolder) holder;
            lastInspectionIssueHeaderViewHolder.lastInspectionIssuesHeader.setText((String) object);
        }

        if (holder instanceof LastInspectionHeaderViewHolder && object instanceof LastCheckInspectionHeaderMO) {

            LastInspectionHeaderViewHolder lastInspectionHeaderViewHolder = (LastInspectionHeaderViewHolder) holder;

            if (null != ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getUnitName() && !((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getUnitName().isEmpty()) {
                lastInspectionHeaderViewHolder.unitValue.setText(((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getUnitName());
            } else {
                lastInspectionHeaderViewHolder.unitValue.setText(context.getResources().getString(R.string.NOT_AVAILABLE));
            }

            if (null != ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getBarrackName() &&
                    !((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getBarrackName().isEmpty()) {
                lastInspectionHeaderViewHolder.barrackValue.setText(((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getBarrackName());
            } else {
                lastInspectionHeaderViewHolder.barrackName.setVisibility(View.GONE);
                lastInspectionHeaderViewHolder.barrackValue.setVisibility(View.GONE);
            }

            lastInspectionHeaderViewHolder.inspectorName.setText(((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getInspectedBy());
            String inspectionDate = ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getLastInspectionDate();
            String inspectionTime = ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getLastInspectionTime();
            String inspectionName = ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getInspectedBy();

            /*String additionalNotes = ((LastCheckInspectionHeaderMO) addCheckingDataList.get(position)).getDescription();
            if (!TextUtils.isEmpty(additionalNotes)) {
                lastInspectionHeaderViewHolder.llAdditionalNotes.setVisibility(View.VISIBLE);
                lastInspectionHeaderViewHolder.additionalRemarks.setText(additionalNotes);
            }*/

            if (!TextUtils.isEmpty(inspectionDate))
                lastInspectionHeaderViewHolder.lastInspectionDate.setText(dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(inspectionDate));
            else
                lastInspectionHeaderViewHolder.lastInspectionDate.setText(context.getResources().getString(R.string.NOT_AVAILABLE));

            String lastCheckName = null;
            switch (taskTypeId) {
                case 1:
                    lastCheckName = LAST_DAY_CHECK;
                    break;
                case 2:
                    lastCheckName = LAST_NIGHT_CHECK;
                    break;
                case 3:
                    lastCheckName = LAST_BARRACK_CHECK;
                    break;
                case 4:
                    lastCheckName = LAST_CLIENT_CHECK;
                    break;
            }

            if (!TextUtils.isEmpty(lastCheckName))
                lastInspectionHeaderViewHolder.lastInspection.setText(lastCheckName);

            if (!TextUtils.isEmpty(inspectionTime))
                lastInspectionHeaderViewHolder.lastInspectionTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(inspectionTime));
            else
                lastInspectionHeaderViewHolder.lastInspectionTime.setText(context.getResources().getString(R.string.NOT_AVAILABLE));

            if (!TextUtils.isEmpty(inspectionName))
                lastInspectionHeaderViewHolder.inspectorName.setText(inspectionName);
            else
                lastInspectionHeaderViewHolder.inspectorName.setText(context.getResources().getString(R.string.NOT_AVAILABLE));

        } else if (holder instanceof LastInspectionGrievancesHolder && object instanceof LastCheckInspectionGrievancesMO) {
            LastInspectionGrievancesHolder lastInspectionGrievancesHolder = (LastInspectionGrievancesHolder) holder;

            if (null != ((LastCheckInspectionGrievancesMO) object).getGuardName() && !((LastCheckInspectionGrievancesMO) object).getGuardName().isEmpty())
                lastInspectionGrievancesHolder.grievanceGuardName.setText(((LastCheckInspectionGrievancesMO) object).getGuardName());
            else
                lastInspectionGrievancesHolder.grievanceGuardName.setText(context.getResources().getString(R.string.NOT_AVAILABLE));

            lastInspectionGrievancesHolder.natureOfIssue.setText(((LastCheckInspectionGrievancesMO) object).getGrievanceNature());
            lastInspectionGrievancesHolder.unitName.setText(((LastCheckInspectionGrievancesMO) object).getUnitName());

        } else if (holder instanceof LastInspectionComplaintsHolder && object instanceof LastCheckInspectionComplaintsMO) {
            LastInspectionComplaintsHolder lastInspectionCardViewHolder = (LastInspectionComplaintsHolder) holder;
            lastInspectionCardViewHolder.natureOfComplaint.setText(((LastCheckInspectionComplaintsMO) object).getNatureOfComplaint());
            lastInspectionCardViewHolder.complaintStatus.setText(((LastCheckInspectionComplaintsMO) object).getComplaintStatus());
        } else if (holder instanceof LastInspectionUnitAtRiskHolder && object instanceof LastCheckInspectionUnitAtRiskMO) {
            LastInspectionUnitAtRiskHolder lastInspectionCardViewHolder = (LastInspectionUnitAtRiskHolder) holder;
            lastInspectionCardViewHolder.planOfActionTv.setText(((LastCheckInspectionUnitAtRiskMO) object).getPlanOfAction());
            String date = dateTimeFormatConversionUtil.convertDateTimeToDate(((LastCheckInspectionUnitAtRiskMO) object).getPlannedClosureDate());
            lastInspectionCardViewHolder.plannedDateTv.setText(date);
            lastInspectionCardViewHolder.status.setText(((LastCheckInspectionUnitAtRiskMO) object).getStatus());

        } else if (holder instanceof NoDataAvailableViewHolder && object instanceof NoDataAvailable) {
            NoDataAvailableViewHolder noDataAvailableViewHolder = (NoDataAvailableViewHolder) holder;
            noDataAvailableViewHolder.noDataAvailable.setText(((NoDataAvailable) addCheckingDataList.get(position)).getMessage());
        } else if (holder instanceof LastImprovementPlanHolder && object instanceof ImprovementPlanListMO) {
            LastImprovementPlanHolder improvementPlanHolder = (LastImprovementPlanHolder) holder;
            improvementPlanHolder.natureOfComplaint.setText(((ImprovementPlanListMO) object).getActionPlanName());
            improvementPlanHolder.complaintStatus.setText(((ImprovementPlanListMO) object).getImprovementStatus());
        } else if (holder instanceof LastInspectionComplaintsHolder && object instanceof KitItemRequestMO) {
            LastInspectionComplaintsHolder lastInspectionKitItemsHolder = (LastInspectionComplaintsHolder) holder;
            lastInspectionKitItemsHolder.natureOfComplaint.setText(((KitItemRequestMO) object).getGuardName());
            String date = dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(((KitItemRequestMO) object).getRequestedOn());
            lastInspectionKitItemsHolder.complaintStatus.setText(date);
        }
    }

    @Override
    public int getItemCount() {
        return addCheckingDataList == null ? 0 : addCheckingDataList.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (addCheckingDataList.get(position) instanceof LastCheckInspectionHeaderMO)
            return TYPE_LAST_INSPECTION_HEADER;
        if (addCheckingDataList.get(position) instanceof String)
            return TYPE_HEADER;
        if (addCheckingDataList.get(position) instanceof LastCheckInspectionGrievancesMO)
            return TYPE_CARDVIEW_GRIEVANCES;
        if (addCheckingDataList.get(position) instanceof LastCheckInspectionComplaintsMO)
            return TYPE_CARDVIEW_COMPLAINTS;
        if (addCheckingDataList.get(position) instanceof LastCheckInspectionUnitAtRiskMO)
            return TYPE_CARDVIEW_UNIT_AT_RISK;
        if (addCheckingDataList.get(position) instanceof NoDataAvailable)
            return TYPE_NO_DATA_AVAILABLE;
        if (addCheckingDataList.get(position) instanceof ImprovementPlanListMO)
            return TYPE_IMPROVEMENT_PLAN;
        if (addCheckingDataList.get(position) instanceof KitItemRequestMO)
            return TYPE_KIT_ITEM_PLAN;
        return -1;
    }


    public class LastInspectionIssueHeaderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.lastInspection_middle_header)
        CustomFontTextview lastInspectionIssuesHeader;

        public LastInspectionIssueHeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            //this.headerTextview = (CustomFontTextview)view.findViewById(R.id.recycle_header);
        }

    }

    public class NoDataAvailableViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.noDataAvailable)
        TextView noDataAvailable;

        public NoDataAvailableViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class LastImprovementPlanHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.natureOfComplaint)
        CustomFontTextview natureOfComplaint;
        @Bind(R.id.complaintStatus)
        CustomFontTextview complaintStatus;

        LastImprovementPlanHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ((LastInspectionReviewActivity) context).showLastInspectionDialog(1, addCheckingDataList.get(getLayoutPosition()));
        }
    }

    public class LastInspectionHeaderViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.unitValue)
        CustomFontTextview unitValue;
        @Bind(R.id.barrackName)
        CustomFontTextview barrackName;
        @Bind(R.id.barrackValue)
        CustomFontTextview barrackValue;
        @Bind(R.id.lastInspection)
        CustomFontTextview lastInspection;
        @Bind(R.id.lastInspectionDate)
        CustomFontTextview lastInspectionDate;
        @Bind(R.id.lastInspectionTime)
        CustomFontTextview lastInspectionTime;
        @Bind(R.id.inspectorName)
        CustomFontTextview inspectorName;
        @Bind(R.id.ll_additional_notes)
        LinearLayout llAdditionalNotes;
        @Bind(R.id.additionalRemarks)
        CustomFontEditText additionalRemarks;

        public LastInspectionHeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public class LastInspectionGrievancesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.grievanceGuardName)
        CustomFontTextview grievanceGuardName;
        @Bind(R.id.nature_of_issue)
        CustomFontTextview natureOfIssue;
        @Bind(R.id.unitName)
        CustomFontTextview unitName;
        @Bind(R.id.viewDetailsImage)
        ImageView viewDetailsImage;

        public LastInspectionGrievancesHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ((LastInspectionReviewActivity) context).showLastInspectionDialog(1, addCheckingDataList.get(getLayoutPosition()));
        }
    }


    public class LastInspectionComplaintsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.natureOfComplaint)
        CustomFontTextview natureOfComplaint;
        @Bind(R.id.complaintStatus)
        CustomFontTextview complaintStatus;
        @Bind(R.id.viewDetailsImage)
        ImageView viewDetailsImage;

        LastInspectionComplaintsHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ((LastInspectionReviewActivity) context).showLastInspectionDialog(1, addCheckingDataList.get(getLayoutPosition()));
        }
    }

    public class LastInspectionUnitAtRiskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.planOfActionTv)
        CustomFontTextview planOfActionTv;
        @Bind(R.id.plannedDateTv)
        CustomFontTextview plannedDateTv;
        @Bind(R.id.status)
        CustomFontTextview status;
        @Bind(R.id.viewDetailsImage)
        ImageView viewDetailsImage;

        LastInspectionUnitAtRiskHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ((LastInspectionReviewActivity) context).showLastInspectionDialog(2, addCheckingDataList.get(getLayoutPosition()));
        }
    }
}
