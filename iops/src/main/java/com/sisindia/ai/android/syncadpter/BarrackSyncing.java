package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackSyncMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.network.response.BarrackSyncOR;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingOR;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by shankar on 8/9/16.
 */

public class BarrackSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<BarrackSyncMO> barrackGeoLocationList;

    public BarrackSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        barrackGeoLocationList = new ArrayList<>();
        BarrackTableSyncing();
    }

    private synchronized void BarrackTableSyncing() {
        barrackGeoLocationList = getBarrackGeoLocationSync();
        if (barrackGeoLocationList != null && barrackGeoLocationList.size() != 0) {
            for (BarrackSyncMO barrackSync : barrackGeoLocationList) {
                barrackGeoLocationSync(barrackSync);
                Timber.d(Constants.TAG_SYNCADAPTER + "barrackGeoLocationSync count   %s", "count: ");
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER + " BarrackSync --> nothing to sync ");
            dependentFailureSync();
        }
    }

    private void barrackGeoLocationSync(final BarrackSyncMO barrackSync) {

        Timber.d(Constants.TAG_SYNCADAPTER + "barrackGeoLocationSync object  %s", barrackSync);
        sisClient.getApi().barrackGeoLocationSync(barrackSync, new Callback<ApiResponse<BarrackSyncOR>>() {
            @Override
            public void success(ApiResponse<BarrackSyncOR> barrackSyncORApiResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "barrackGeoLocationSync response  %s", barrackSyncORApiResponse.statusCode);

                switch (barrackSyncORApiResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        updateBarrackTable(barrackSync.getId()); // uplaod  the Attachment id to Metadata tables
                        break;
                    default:
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private List<BarrackSyncMO> getBarrackGeoLocationSync() {
        List<BarrackSyncMO> barrackSyncMOList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.BARRACK_TABLE +
                " where " + TableNameAndColumnStatement.IS_SYNCED + "=0";
        Timber.d("getBarrackGeoLocationSync selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        BarrackSyncMO barrackSyncMO = new BarrackSyncMO();
                        barrackSyncMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        barrackSyncMO.setLat(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LATITUDE)));
                        barrackSyncMO.setLong(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GEO_LONGITUDE)));
                        barrackSyncMO.setDeviceNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DEVICE_NO)));
//                        barrackSyncMO.setDeviceNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED)));
                        barrackSyncMOList.add(barrackSyncMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return barrackSyncMOList;
    }

    /**
     * @param barrackId
     */
    private void updateBarrackTable(int barrackId) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.BARRACK_TABLE +
                " SET " + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                " WHERE " + TableNameAndColumnStatement.BARRACK_ID + "= '" + barrackId + "'";
        Timber.d(Constants.TAG_SYNCADAPTER + "updateBarrackTable  %s", updateQuery);

        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            updateStatus(barrackId);
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb
                .getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                Timber.d(Constants.TAG_SYNCADAPTER + " BarrackSync # dependentSyncs --> " + dependentSyncsStatus.barrackId);
                if (dependentSyncsStatus != null && dependentSyncsStatus.barrackId != 0) {
                    dependentSyncs(dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {

        if (dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            if (dependentSyncsStatus.barrackId != 0) {
                metaDataSyncing.setBarrackId(dependentSyncsStatus.barrackId);
            }
        } else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                if (dependentSyncsStatus.unitRaisingId != 0) {
                    imageSyncing.setBarrackId(dependentSyncsStatus.barrackId);
                }
            }
        }
    }

    private void updateStatus(int barrackId) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.barrackId = barrackId;
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isUnitRaisingStrengthSynced = Constants.NEG_DEFAULT_COUNT;


        if (barrackId != 0) {
            insertDependentSyncData(dependentSyncsStatus, true);
        }
        MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
        metaDataSyncing.setBarrackId(barrackId);
    }

    private void insertDependentSyncData(DependentSyncsStatus dependentSyncsStatus, boolean isNewlyCreated) {
        dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus, 1);
    }
}

