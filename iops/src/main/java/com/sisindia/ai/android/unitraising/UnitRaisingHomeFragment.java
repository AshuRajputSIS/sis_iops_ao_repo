package com.sisindia.ai.android.unitraising;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.unitraising.adapter.UnitRaisingDetailsAdapter;
import com.sisindia.ai.android.unitraising.adapter.UnitRaisingDetailsAdapter.OnRaisingUnitsClickListener;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ashu Rajput on 7/26/2017.
 */

public class UnitRaisingHomeFragment extends BaseFragment implements OnRaisingUnitsClickListener {

    @Bind(R.id.unitRaisingDetailsRV)
    RecyclerView unitRaisingDetailsRecyclerView;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private boolean isAnyUnitSelected = false;
    private ArrayList<RaisingDetailsMo> raisingDetails;
    private RaisingDetailsMo raisingDetailsMo;

    public static UnitRaisingHomeFragment newInstance(String toolbarTitle) {
        UnitRaisingHomeFragment unitRaisingFragment = new UnitRaisingHomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", toolbarTitle);
        unitRaisingFragment.setArguments(bundle);
        return unitRaisingFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle("UNIT RAISING");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.unit_raising_main_fragment, container, false);
        ButterKnife.bind(this, view);
        raisingDetails = IOPSApplication.getInstance().getUnitRaisingSelectStatementDB().getUnitRaisingDetails();
        setUpRecyclerView();
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.unit_raising_main_fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_unit_raising, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit) {
        }
        return true;
    }

    private void setUpRecyclerView() {
        unitRaisingDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        UnitRaisingDetailsAdapter unitRaisingDetailsAdapter = new UnitRaisingDetailsAdapter(getActivity(), this);
        if (raisingDetails != null && raisingDetails.size() != 0) {
            unitRaisingDetailsAdapter.setUnitRaisingDetailsList(raisingDetails);
        }
        unitRaisingDetailsRecyclerView.setAdapter(unitRaisingDetailsAdapter);
        Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        unitRaisingDetailsRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    @OnClick(R.id.unitRaisingStart)
    public void startRaisingClick() {
        Intent intent = new Intent(getActivity(), UnitRaisingStrengthAndDetailsActivity.class);
        intent.putExtra(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo);
        if (isAnyUnitSelected) {
            if (appPreferences.getDutyStatus()) {
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE_FOR_UNIT_RAISING), Toast.LENGTH_SHORT).show();
            }
        } else {
            Snackbar.make(getView(), "Please select unit", Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.unitRaisingDeferred)
    public void raisingDeferredClick() {
        Intent intent = new Intent(getActivity(), UnitRaisingDeferredOrCancelled.class);
        intent.putExtra("UNIT_RAISING_TITLE", "RAISING DEFERRED");
        intent.putExtra(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo);
        if (isAnyUnitSelected) {
            if (IOPSApplication.getInstance().getUnitRaisingSelectStatementDB().getRaisingDeferredCount(raisingDetailsMo.unitId) < 3) {
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.UNIT_RAISING_DEFERRED_MAX_COUNT_MSG), Toast.LENGTH_SHORT).show();
            }
        } else {
            Snackbar.make(getView(), "Please select unit", Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.unitRaisingCancelled)
    public void raisingCancelledClick() {
        Intent intent = new Intent(getActivity(), UnitRaisingDeferredOrCancelled.class);
        intent.putExtra("UNIT_RAISING_TITLE", "RAISING CANCELLED");
        intent.putExtra(TableNameAndColumnStatement.RAISING_DETAILS, raisingDetailsMo);
        if (isAnyUnitSelected)
            startActivity(intent);
        else
            Snackbar.make(getView(), "Please select unit", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onUnitSelectEvent(boolean isUnitSelected, int position) {
        isAnyUnitSelected = isUnitSelected;
        raisingDetailsMo = raisingDetails.get(position);
    }

    @Override
    public void onResume() {
        super.onResume();
        isAnyUnitSelected = false;
    }

}
