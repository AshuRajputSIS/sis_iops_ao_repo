package com.sisindia.ai.android.fcmnotification.fcmbarrack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 2/12/16.
 */

public class FCMBaseBarrackMO implements Serializable {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;

    @Override
    public String toString() {
        return "FCMBaseBarrackMO{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", data=" + data +
                '}';
    }

    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("Data")
    @Expose
    private List<Barrack> data = new ArrayList<Barrack>();

    /**
     *
     * @return
     * The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     *
     * @param statusCode
     * The StatusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     * The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     *
     * @param statusMessage
     * The StatusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     *
     * @return
     * The data
     */
    public List<Barrack> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The Data
     */
    public void setData(List<Barrack> data) {
        this.data = data;
    }

}
