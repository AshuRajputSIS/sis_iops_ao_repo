package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 19/7/16.
 */

public class DataOR {

    @SerializedName("actionPlanId")
    @Expose
    private Integer actionPlanId;

    /**
     *
     * @return
     * The actionPlanId
     */
    public Integer getActionPlanId() {
        return actionPlanId;
    }

    /**
     *
     * @param actionPlanId
     * The actionPlanId
     */
    public void setActionPlanId(Integer actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    @Override
    public String toString() {
        return "DataOR{" +
                "actionPlanId=" + actionPlanId +
                '}';
    }
}
