package com.sisindia.ai.android.home;

/**
 * Created by Durga Prasad on 04-04-2016.
 */
public interface DutyChangeListener {
    void onDutyChange(boolean dutyOffOn);
}
