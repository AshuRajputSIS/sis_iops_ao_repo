package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by shankar on 27/6/16.
 */

public class EmployeeAuthorizedStrengthMO {

    private Integer id;

    private Integer rankId;

    private String rankAbbrevation;

    private Integer rankCount;

    private Integer barrackId;

    private String unitName;

    private String rankName;

    private String barrackName;


    private Integer unitTakID;
    private Integer strength;

    private Boolean isArmed;
    private int actual;
    private Boolean isSyned;
    private Integer unitId;

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Integer getLeaveCount() {
        return leaveCount;
    }

    public void setLeaveCount(Integer leaveCount) {
        this.leaveCount = leaveCount;
    }

    private String createdDateTime;


    private Integer leaveCount;

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }


    public Boolean getSyned() {
        return isSyned;
    }

    public void setSyned(Boolean syned) {
        isSyned = syned;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    public String getRankAbbrevation() {
        return rankAbbrevation;
    }

    public void setRankAbbrevation(String rankAbbrevation) {
        this.rankAbbrevation = rankAbbrevation;
    }

    public Integer getRankCount() {
        return rankCount;
    }

    public void setRankCount(Integer rankCount) {
        this.rankCount = rankCount;
    }

    public Integer getBarrackId() {
        return barrackId;
    }

    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Boolean getArmed() {
        return isArmed;
    }

    public void setArmed(Boolean armed) {
        isArmed = armed;
    }

    public int getActual() {
        return actual;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    public Integer getUnitTakID() {
        return unitTakID;
    }

    public void setUnitTakID(Integer unitTakID) {
        this.unitTakID = unitTakID;
    }

    @Override
    public String toString() {
        return "EmployeeAuthorizedStrengthMO{" +
                "id=" + id +
                ", rankId=" + rankId +
                ", rankAbbrevation='" + rankAbbrevation + '\'' +
                ", rankCount=" + rankCount +
                ", barrackId=" + barrackId +
                ", unitName='" + unitName + '\'' +
                ", rankName='" + rankName + '\'' +
                ", barrackName='" + barrackName + '\'' +
                ", unitTakID=" + unitTakID +
                ", strength=" + strength +
                ", isArmed=" + isArmed +
                ", actual=" + actual +
                ", isSyned=" + isSyned +
                ", unitId=" + unitId +
                '}';
    }
}
