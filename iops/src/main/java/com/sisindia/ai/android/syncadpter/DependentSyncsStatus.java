package com.sisindia.ai.android.syncadpter;

/**
 * Created by compass on 5/9/2017.
 */

public class DependentSyncsStatus {
    public  int taskId;
    public  int taskTypeId;
    public  int taskStatusId;
    public int unitPostId;
    public int kitDistributionId;
    public int issueId;
    public  int isUnitStrengthSynced;
    public  int isIssuesSynced;
    public  int isKitRequestSynced;
    public  int isImprovementPlanSynced;
    public  int isMetaDataSynced;
    public  int isImagesSynced;
    public  boolean isAllDepemdentModilesSynced;
    public int isUnitEquipmentSynced;
    public int isUnitRaisingStrengthSynced;
    public int unitRaisingId;
    public int unitRaisingDeferredId;
    public int unitRaisingCancellationId;
    public int barrackId;
}
