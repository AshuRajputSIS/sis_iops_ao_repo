package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 28/6/16.
 */
public class BillingCheckListMO {

    @SerializedName("CheckListItem")
    @Expose
    private Integer checkListItem;
    @SerializedName("CheckListItemValue")
    @Expose
    private Boolean checkListItemValue;

    /**
     *
     * @return
     * The checkListItem
     */
    public Integer getCheckListItem() {
        return checkListItem;
    }

    /**
     *
     * @param checkListItem
     * The CheckListItem
     */
    public void setCheckListItem(Integer checkListItem) {
        this.checkListItem = checkListItem;
    }

    /**
     *
     * @return
     * The checkListItemValue
     */
    public Boolean getCheckListItemValue() {
        return checkListItemValue;
    }

    /**
     *
     * @param checkListItemValue
     * The CheckListItemValue
     */
    public void setCheckListItemValue(Boolean checkListItemValue) {
        this.checkListItemValue = checkListItemValue;
    }


}
