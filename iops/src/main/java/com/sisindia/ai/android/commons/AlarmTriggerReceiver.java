package com.sisindia.ai.android.commons;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by compass on 4/10/2017.
 */

public class AlarmTriggerReceiver extends BroadcastReceiver {
    private Calendar calendar;
    private int triggerCount = 0;
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    @Override
    public void onReceive(Context context, Intent intent) {
        appPreferences = new AppPreferences(context);
        triggerCount = appPreferences.getAlarmTriggeredCount();
        triggerCount = triggerCount + 1;
        Toast.makeText(context, "Alarm Triggered" + triggerCount, Toast.LENGTH_LONG).show();
        Calendar calendar = Calendar.getInstance();

        if(calendar.get(Calendar.HOUR_OF_DAY) > 5 && calendar.get(Calendar.HOUR_OF_DAY) < 8){
            if (appPreferences.getAlarmTriggeredCount() == Constants.MORNING_MAX_ALARM_TRIGGER_COUNT) {
                setAlarmForNextDay(context,true);
            }
            else{
                dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
                try {
                    if(dateTimeFormatConversionUtil.scheduleAlarmRangeChecking(dateTimeFormatConversionUtil
                            .getCurrentDate(),Calendar.getInstance().getTimeInMillis())) {
                        appPreferences.setAlarmTriggeredCount(triggerCount);
                        Intent serviceIntent = new Intent(context, ScheduleSyncJobService.class);
                        context.startService(serviceIntent);
                    }
                    else{
                        setAlarmForNextDay(context,true);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }
        else
        if (calendar.get(Calendar.HOUR_OF_DAY) > 19 && calendar.get(Calendar.HOUR_OF_DAY) < 23) {
            if (appPreferences.getAlarmTriggeredCount() == Constants.MAX_ALARM_TRIGGER_COUNT) {
                setAlarmForNextDay(context, false);
            } else {
                dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
                try {
                    if (dateTimeFormatConversionUtil.scheduleAlarmRangeChecking(dateTimeFormatConversionUtil
                            .getCurrentDate(), Calendar.getInstance().getTimeInMillis())) {
                        appPreferences.setAlarmTriggeredCount(triggerCount);
                        Intent serviceIntent = new Intent(context, ScheduleSyncJobService.class);
                        context.startService(serviceIntent);
                    } else {
                        setAlarmForNextDay(context, false);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setAlarmForNextDay(Context context,boolean isItNightAlarmSet) {
        if(isItNightAlarmSet) {
            calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Constants.SYNC_ALARM_START_TIME_IN_HOURS);
        }
        else {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            calendar.set(Calendar.HOUR_OF_DAY, Constants.MORNING_SYNC_ALARM_START_TIME_IN_HOURS);
            appPreferences.setAlarmTriggeredCount(0);
        }
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService
                (Context.ALARM_SERVICE);
        Intent triggerIntent = new Intent(context, AlarmTriggerReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.ALARM_SERVICE_ID, triggerIntent, 0);
        alarmManager.cancel(pendingIntent);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                Constants.ALRAM_TRIGGER_TIME_INTERVAL,
                pendingIntent);
    }
}
