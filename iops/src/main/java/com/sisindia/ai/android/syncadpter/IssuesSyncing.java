package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.issueDTO.IssueLevelUpdateStatementDB;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.IssueIMO;
import com.sisindia.ai.android.network.response.IssuesOutputMO;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */

public class IssuesSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<IssueIMO> issueInputRequestList;
    private IssueLevelUpdateStatementDB issueLevelUpdateStatementDB;
    private boolean isAdhocSync;
    private int taskId;
    private AppPreferences appPreferences;

    public IssuesSyncing(Context mcontext, boolean isAdhocSync, int taskId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        issueLevelUpdateStatementDB = IOPSApplication.getInstance().getIssueUpdateDBInstance();
        this.issueInputRequestList = new ArrayList<>();
        this.isAdhocSync = isAdhocSync;
        this.taskId = taskId;
        appPreferences = new AppPreferences(mcontext);
        IssuesSync();
    }

    private void IssuesSync() {
        issueInputRequestList.clear();
        issueInputRequestList = getAllIssuesToSync();
        if (null != issueInputRequestList && issueInputRequestList.size() != 0) {
            if (!isAdhocSync) {
                updateIssueSyncCount(issueInputRequestList.size());
            }
            for (IssueIMO issuesIMO : issueInputRequestList) {
                issuesApiCall(issuesIMO);
            }
        } else {
            Timber.d(Constants.TAG_SYNCADAPTER + " depended IssuesSyncing ");
            updateIssueSyncCount(0);
            dependentFailureSync();
        }
    }

    public void updateIssueSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                if (taskId == dependentSyncsStatus.taskId) {
                    dependentSyncsStatus.isIssuesSynced = count;
                    updateIssueSyncCount(dependentSyncsStatus);
                }
            }
        }
    }

    private void updateIssueSyncCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(dependentSyncsStatus.taskId,
                Constants.TASK_RELATED_SYNC,
                TableNameAndColumnStatement.IS_ISSUES_SYNCED,
                dependentSyncsStatus.isIssuesSynced, dependentSyncsStatus.taskTypeId
        );
    }


    private List<IssueIMO> getAllIssuesToSync() {
        List<IssueIMO> issueInputRequestList = new ArrayList<>();
        String selectQuery = "";
        if (isAdhocSync) {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ISSUES_TABLE + " where " +
                    TableNameAndColumnStatement.SOURCE_TASK_ID + " = " + 0 + " and " +
                    TableNameAndColumnStatement.ISSUE_ID + " = '" + 0 + "'  and  (" +
                    TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                    TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' ) ";

        } else {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ISSUES_TABLE + " where " +
                    TableNameAndColumnStatement.SOURCE_TASK_ID + " > " + 0 + " and " +
                    TableNameAndColumnStatement.SOURCE_TASK_ID + " = " + taskId + " and " +
                    TableNameAndColumnStatement.ISSUE_ID + " = '" + 0 + "'  and  (" +
                    TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                    TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' or " +
                    TableNameAndColumnStatement.ISSUE_ID + " = '" + 0 + "' )";
        }


        Timber.d("AllIssuesQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            SQLiteDatabase db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        IssueIMO issueIMO = new IssueIMO();
                        issueIMO.setIssueTypeId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_TYPE_ID)));
                        issueIMO.setIssueMatrixId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        issueIMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        issueIMO.setRaisedById(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)) == 0) {
                            issueIMO.setIssueId(null);
                        } else {
                            issueIMO.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));
                        }
                        issueIMO.setIssueTypeId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_TYPE_ID)));
                        issueIMO.setIssueMatrixId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        issueIMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        issueIMO.setRaisedById(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)) != null &&
                                (!cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)).isEmpty() &&
                                        !cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)).equalsIgnoreCase("0"))) {
                            issueIMO.setUnitId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        } else {
                            issueIMO.setUnitId(null);
                        }
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)) != 0) {
                            issueIMO.setGuardId(String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID))));
                        } else {
                            issueIMO.setGuardId(null);
                        }
                        if (cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)) != null) {
                            issueIMO.setGuardCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        } else {
                            issueIMO.setGuardCode(null);
                        }
                        issueIMO.setIssueSourceId(String.valueOf(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MODE_ID))));
                        issueIMO.setIssueStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_STATUS_ID)));
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)) != 0) {
                            issueIMO.setSourceTaskId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.SOURCE_TASK_ID)));
                        } else {
                            issueIMO.setSourceTaskId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)) != 0) {
                            issueIMO.setAssignedToId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID)));
                        } else {
                            issueIMO.setAssignedToId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CATEGORY_ID)) != 0) {
                            issueIMO.setIssueCategoryId(String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CATEGORY_ID))));
                        } else {
                            issueIMO.setIssueCategoryId(null);
                        }
                        String temp = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME));
                        issueIMO.setRaisedDateTime(temp);
                        issueIMO.setAssignedToDateTime(temp);
                        issueIMO.setIssueActionId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        String targetActionEndDate = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_ACTION_END_DATE));
                        if (targetActionEndDate != null && targetActionEndDate.length() > 0) {
                            targetActionEndDate = targetActionEndDate + " 00:00:00";
                            issueIMO.setTargetActionDateTime(targetActionEndDate);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)) != 0) {
                            issueIMO.setBarrackId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        } else {
                            issueIMO.setBarrackId(null);
                        }

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CAUSE_ID)) != 0) {
                            issueIMO.setIssueCauseId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_CAUSE_ID)));
                        } else {
                            issueIMO.setIssueCauseId(null);
                        }

                        issueIMO.setSeqId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        String issueNatureId = String.valueOf(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_NATURE_ID)));
                        issueIMO.setIssueNatureId(issueNatureId);
                        issueIMO.setClientRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_REMARKS)));
                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_NAME)))) {
                            issueIMO.setClientContactName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_CONTACT_NAME)));
                        } else {
                            issueIMO.setClientContactName(null);
                        }
                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)))) {
                            issueIMO.setPhoneNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PHONE_NO)));
                        } else {
                            issueIMO.setPhoneNo(null);
                        }

                        if (!TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_END_DATE)))) {
                            issueIMO.setClosureDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_END_DATE)));
                        } else {
                            issueIMO.setClosureDateTime(null);
                        }
                        Timber.d("Issues jsonObject  %s", IOPSApplication.getGsonInstance().toJson(issueIMO));
                        issueIMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        issueInputRequestList.add(issueIMO);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing object  %s", IOPSApplication.getGsonInstance().toJson(issueInputRequestList));
        }
        return issueInputRequestList;
    }

    private synchronized void issuesApiCall(final IssueIMO inputIMO) {
        sisClient.getApi().syncIssueData(inputIMO, new Callback<IssuesOutputMO>() {
            @Override
            public void success(IssuesOutputMO issuesOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing---CatchIt  %s", issuesOutputMO);
                switch (issuesOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        if (issuesOutputMO != null && issuesOutputMO.getActionPlanData() != null) {
                            if (issuesOutputMO.getActionPlanData().getJsonActionPlan() != null && issuesOutputMO.getActionPlanData().getAssignedToId() != null) {
                                setIssueAndImprovementData(inputIMO, issuesOutputMO);
                            } else {
                                setIssuesData(inputIMO, issuesOutputMO);
                            }
                            /*else {
                                if(issuesOutputMO.getActionPlanData().getIssueId() != null) {
                                    insertDepedentSyncData(setDependentSyncDetails(
                                            issuesOutputMO.getActionPlanData().getIssueId()),
                                            false,Integer.parseInt(inputIMO.getIssueStatus()));
                                    issueLevelUpdateStatementDB.updateIssueSyncStatus(
                                            issuesOutputMO.getActionPlanData().getIssueId());
                                }
                            }*/

                            if (taskId == -1 && isAdhocSync)
                                new MetaDataSyncing(mContext, issuesOutputMO.getActionPlanData().getIssueId(), isAdhocSync);
                            else
                                updateSyncCountAndStatus();
                        }

                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER + "IssuesSyncing count   %s", "count:");
                Crashlytics.logException(error);
            }
        });
    }

    private void setIssueAndImprovementData(IssueIMO inputIMO, IssuesOutputMO issuesOutputMO) {
        issueLevelUpdateStatementDB.updateIssueId(inputIMO.getSeqId(), issuesOutputMO.getActionPlanData());
        String actionPlan = issuesOutputMO.getActionPlanData().getJsonActionPlan();
        if (!TextUtils.isEmpty(actionPlan)) {
            ActionPlanMO actionPlanMO = IOPSApplication.getGsonInstance().fromJson(actionPlan, ActionPlanMO.class);
            Timber.d(Constants.TAG_SYNCADAPTER + "grievanceSyncing response ActionPlanMO %s", actionPlanMO);
            actionPlanMO.setAssigneeName(issuesOutputMO.getActionPlanData().getAssigneeName());
            if (!TextUtils.isEmpty(inputIMO.getBarrackId())) {
                actionPlanMO.setBarrackId(Integer.valueOf(inputIMO.getBarrackId()));
            }
            if (null != inputIMO.getUnitId() && !inputIMO.getUnitId().isEmpty())
                if (!TextUtils.isEmpty(inputIMO.getUnitId()))
                    actionPlanMO.setUnitId(Integer.valueOf(inputIMO.getUnitId()));

            updateidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, issuesOutputMO.getActionPlanData().getIssueId(),
                    inputIMO.getSeqId(), Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, mContext));

            if (TextUtils.isEmpty(inputIMO.getSourceTaskId())) {
                issueLevelUpdateStatementDB.updateImprovementPlanDetails(0, inputIMO.getSeqId(), actionPlanMO);
                // new ImprovementPlanSyncing(mContext,false,-1);
            } else {
                issueLevelUpdateStatementDB.updateImprovementPlanDetails(Integer.parseInt(inputIMO.getSourceTaskId()), inputIMO.getSeqId(), actionPlanMO);
                new ImprovementPlanSyncing(mContext, false, Integer.parseInt(inputIMO.getSourceTaskId()));
            }
        }

        if (taskId == -1) {
            setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId());
            if (inputIMO.getIssueId() == null) {
                insertDepedentSyncData(setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId()), true, Integer.parseInt(inputIMO.getIssueStatus()));
            } else {
                if (inputIMO.getIssueId() == 0) {
                    insertDepedentSyncData(setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId()), true, Integer.parseInt(inputIMO.getIssueStatus()));
                }
            }
        }
    }

    private void setIssuesData(IssueIMO inputIMO, IssuesOutputMO issuesOutputMO) {
        issueLevelUpdateStatementDB.updateIssueId(inputIMO.getSeqId(), issuesOutputMO.getActionPlanData());
        if (taskId == -1) {
            setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId());
            if (inputIMO.getIssueId() == null) {
                insertDepedentSyncData(setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId()), true, Integer.parseInt(inputIMO.getIssueStatus()));
            } else {
                if (inputIMO.getIssueId() == 0) {
                    insertDepedentSyncData(setDependentSyncDetails(issuesOutputMO.getActionPlanData().getIssueId()), true, Integer.parseInt(inputIMO.getIssueStatus()));
                }
            }
        }
        updateidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, issuesOutputMO.getActionPlanData().getIssueId(),
                inputIMO.getSeqId(), Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, mContext));

    }

    private DependentSyncsStatus setDependentSyncDetails(int issueId) {
        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
        dependentSyncsStatus.issueId = issueId;
        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
        dependentSyncsStatus.isUnitEquipmentSynced = Constants.NEG_DEFAULT_COUNT;
        return dependentSyncsStatus;
    }


    private void insertDepedentSyncData(DependentSyncsStatus dependentSyncsStatus, boolean isNewlyCreated, int issueStatus) {
        if (taskId > 0) {

        } else {
            if (!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.issueId,
                    TableNameAndColumnStatement.ISSUE_ID, isNewlyCreated)) {
                if (isNewlyCreated || issueStatus == 1) {
                    dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus, issueStatus);
                } else {
                    if (!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.issueId,
                            TableNameAndColumnStatement.ISSUE_ID, isNewlyCreated)) {
                        dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus, issueStatus);
                    } else {
                        dependentSyncDb.updateDependentTaskSyncDetails(dependentSyncsStatus.issueId, issueStatus);
                    }
                }
            }
        }

    }


    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                dependentSyncs(dependentSyncsStatus);
            }
        }
    }

    private void dependentSyncs(DependentSyncsStatus dependentSyncsStatus) {
        if (dependentSyncsStatus != null) {
            if (dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
                MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
                if (taskId > 0) {
                    metaDataSyncing.setTaskWithIssues(dependentSyncsStatus.taskId, Constants.LOOKUP_TYPE_AUDIO);
                } else if (isAdhocSync && dependentSyncsStatus.issueId != 0) {
                    metaDataSyncing.setIssueId(dependentSyncsStatus.issueId);
                }
            } else {
                if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                    ImageSyncing imageSyncing = new ImageSyncing(mContext);
                    if (taskId > 0) {
                        imageSyncing.setTaskIdWithIssues(dependentSyncsStatus.taskId, Constants.LOOKUP_TYPE_AUDIO);
                    } else if (isAdhocSync && dependentSyncsStatus.issueId != 0) {
                        imageSyncing.setIssueId(dependentSyncsStatus.issueId);
                    }
                }
            }
        }

    }

    private void updateSyncCountAndStatus() {
        if (taskId > 0) {
            int count = dependentSyncDb.getSyncCount(taskId, TableNameAndColumnStatement.TASK_ID, TableNameAndColumnStatement.IS_ISSUES_SYNCED);
            if (count != 0) {
                count = count - 1;
                Timber.i("NewSync %s", " Issue Syncing count update in ----- " + taskId + "-------- " + count);
                updateIssueSyncCount(count);
                if (count == 0) {
                    MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
                    metaDataSyncing.setTaskWithIssues(taskId, Constants.LOOKUP_TYPE_AUDIO);
                }
            }
        }

    }
}
