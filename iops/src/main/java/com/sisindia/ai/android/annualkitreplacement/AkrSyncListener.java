package com.sisindia.ai.android.annualkitreplacement;

/**
 * Created by compass on 6/1/2017.
 */

public interface AkrSyncListener {
    void akrSyncListener(boolean isSynced);
}
