package com.sisindia.ai.android.rota;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DutyResponseData {

@SerializedName("EmployeeDutyId")
@Expose
private Integer employeeDutyId;

/**
* 
* @return
* The employeeDutyId
*/
public Integer getEmployeeDutyId() {
return employeeDutyId;
}

/**
* 
* @param employeeDutyId
* The EmployeeDutyId
*/
public void setEmployeeDutyId(Integer employeeDutyId) {
this.employeeDutyId = employeeDutyId;
}

}

