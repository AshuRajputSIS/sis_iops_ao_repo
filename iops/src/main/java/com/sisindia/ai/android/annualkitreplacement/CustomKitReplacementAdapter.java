package com.sisindia.ai.android.annualkitreplacement;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 20-04-2016.
 */
public class CustomKitReplacementAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mcontext;
    ArrayList<AnnualOrAdhocKitItemMO> kitDetailsList;
    boolean isEnabled;

    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public CustomKitReplacementAdapter(Context mcontext) {
        this.mcontext = mcontext;
    }

    public void setKitDetails(ArrayList<AnnualOrAdhocKitItemMO> kitDetailsList) {

        this.kitDetailsList = kitDetailsList;

    }

    public void enableClick(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder recycleViewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.annual_kit_row, parent, false);
        recycleViewHolder = new KitItemDescriptionHolder(view);
        return recycleViewHolder;


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        KitItemDescriptionHolder kitItemDescriptionHolder = (KitItemDescriptionHolder) holder;

        if (kitDetailsList.get(position) != null) {
            kitItemDescriptionHolder.gaurdName.setText(kitDetailsList.get(position).getGuardName());
            if(kitDetailsList.get(position).getCount() != 0) {
                if (kitDetailsList.get(position).getCount() == 1){
                    kitItemDescriptionHolder.kitItemCount.setText(String.valueOf(kitDetailsList.get(position).getCount()) +  " " + mcontext.getResources().getString(R.string.Item));
                }
                else {
                    kitItemDescriptionHolder.kitItemCount.setText(String.valueOf(kitDetailsList.get(position).getCount()) +  " " + mcontext.getResources().getString(R.string.Items));
                }
                kitItemDescriptionHolder.guardCode.setText(kitDetailsList.get(position).getGuardCode());

            }
            else{
                kitItemDescriptionHolder.kitItemCount.setText(String.valueOf(kitDetailsList.get(position).getCount()));
            }
            if(!isEnabled){
                kitItemDescriptionHolder.dateLayout.setVisibility(View.VISIBLE);
                kitItemDescriptionHolder.issuedDate.setText(kitDetailsList.get(position).getIssueDate());
            }
            if(kitDetailsList.get(position).isUnPaid()){
                kitItemDescriptionHolder.itemView.setAlpha(0.3f);
                kitItemDescriptionHolder.itemView.setClickable(false);
            }

        }


    }


    @Override
    public int getItemCount() {

        int count = kitDetailsList == null ? 0 : kitDetailsList.size();
        return count;
    }


    class KitItemDescriptionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.gaurd_photo)
        ImageView gaurdPhoto;
        @Bind(R.id.gaurd_name)
        CustomFontTextview gaurdName;
        @Bind(R.id.kit_item_count)
        CustomFontTextview kitItemCount;
        @Bind(R.id.issued_date_label)
        CustomFontTextview issuedDateLabel;
        @Bind(R.id.issued_date)
        CustomFontTextview issuedDate;
        @Bind(R.id.dateLayout)
        LinearLayout dateLayout;
        @Bind(R.id.guard_code)
        CustomFontTextview guardCode;
        private boolean isUnPaid;


        public KitItemDescriptionHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (isEnabled)
                onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }

    }


}

