package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecuirtementMO {

@SerializedName("RecruitmentId")
@Expose
private Integer recruitmentId;
@SerializedName("FullName")
@Expose
private String fullName;
@SerializedName("ContactNo")
@Expose
private String contactNo;
@SerializedName("DOB")
@Expose
private String dOB;
@SerializedName("ActionTakenOn")
@Expose
private String actionTakenOn;
@SerializedName("Status")
@Expose
private String status;
@SerializedName("IsSelected")
@Expose
private Boolean isSelected;
@SerializedName("IsRejected")
@Expose
private Boolean isRejected;

public Integer getRecruitmentId() {
return recruitmentId;
}

public void setRecruitmentId(Integer recruitmentId) {
this.recruitmentId = recruitmentId;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getContactNo() {
return contactNo;
}

public void setContactNo(String contactNo) {
this.contactNo = contactNo;
}

public String getDOB() {
return dOB;
}

public void setDOB(String dOB) {
this.dOB = dOB;
}

public String getActionTakenOn() {
return actionTakenOn;
}

public void setActionTakenOn(String actionTakenOn) {
this.actionTakenOn = actionTakenOn;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Boolean getIsSelected() {
return isSelected;
}

public void setIsSelected(Boolean isSelected) {
this.isSelected = isSelected;
}

public Boolean getIsRejected() {
return isRejected;
}

public void setIsRejected(Boolean isRejected) {
this.isRejected = isRejected;
}

}