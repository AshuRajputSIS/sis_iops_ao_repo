package com.sisindia.ai.android.commons;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.utils.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import timber.log.Timber;


public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    //    EditText mEdit;
    DateTimeUpdateListener dateTimeUpdateListener;
    private boolean ispast;
    private int year;
    private int month;
    private int dayOfMonth;
    private View mTaskDetailActivity;
    private String selectedDate = null;
    private boolean isFromPerforanceCalendar;

    public SelectDateFragment() {

    }

    public static SelectDateFragment newInstance() {
        SelectDateFragment fragment = new SelectDateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SelectDateFragment(View mparentRelativeLayout, String selectDate) {
        mTaskDetailActivity = mparentRelativeLayout;
        selectedDate = selectDate;
    }

    public void setPerformanceCalendar(boolean isFromPerforanceCalendar) {
        this.isFromPerforanceCalendar = isFromPerforanceCalendar;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar maxDateForPerformance = null, minDateForPerformance = null, minCalendar = null, maxCalendar = null;
        DatePickerDialog datePickerDialog = null;
        if (isFromPerforanceCalendar) {
            maxDateForPerformance = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat(getActivity().getResources().getString(R.string.month_date_format));

            Timber.e("date: " + format.format(maxDateForPerformance.getTime()));
            year = maxDateForPerformance.get(Calendar.YEAR);
            month = maxDateForPerformance.get(Calendar.MONTH);
            dayOfMonth = maxDateForPerformance.get(Calendar.DAY_OF_MONTH);
            minDateForPerformance = Calendar.getInstance();
            if (dayOfMonth > 11) {
                minDateForPerformance.set(year, month, 12);
                maxDateForPerformance.set(year, month + 1, 11);
            } else {
                minDateForPerformance.set(year, month - 1, 12);
                maxDateForPerformance.set(year, month, 11);
            }

        } else {
            minCalendar = Calendar.getInstance();
            getActivity().getResources().getString(R.string.date_format);
            SimpleDateFormat format = new SimpleDateFormat(getActivity().getResources().getString(R.string.month_date_format));

            Timber.e("date: " + format.format(minCalendar.getTime()));
            year = minCalendar.get(Calendar.YEAR);
            month = minCalendar.get(Calendar.MONTH);
            dayOfMonth = minCalendar.get(Calendar.DAY_OF_MONTH);

            maxCalendar = Calendar.getInstance();
            maxCalendar.set(year, Calendar.DECEMBER, 31);
        }

        if (selectedDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(getActivity().getResources().getString(R.string.month_date_format));
            Date parse = null;
            try {
                parse = sdf.parse(selectedDate);
                Calendar c = Calendar.getInstance();
                c.setTime(parse);
                month = c.get(Calendar.MONTH);
                year = c.get(Calendar.YEAR);
                dayOfMonth = c.get(Calendar.DATE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        datePickerDialog = new DatePickerDialog(getActivity(), R.style.MyDatePickerDialogTheme, this, year, month, dayOfMonth);
        final DatePicker datePicker = datePickerDialog.getDatePicker();
        if (!ispast) {
            if (isFromPerforanceCalendar) {
                datePicker.setMinDate(minDateForPerformance.getTimeInMillis());
                datePicker.setMaxDate(maxDateForPerformance.getTimeInMillis());
            } else {
                datePicker.setMinDate(minCalendar.getTimeInMillis());
                datePicker.setMaxDate(maxCalendar.getTimeInMillis());
            }

        } else {
            datePicker.setMaxDate(maxCalendar.getTimeInMillis());
        }
        return datePickerDialog;
    }

    public void setTimeListener(DateTimeUpdateListener dateTimeUpdateListener) {
        this.dateTimeUpdateListener = dateTimeUpdateListener;
    }

    public void setPastDate(boolean ispast) {
        this.ispast = ispast;
    }
   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dateTimeUpdateListener = (DateTimeUpdateListener)context;
    }*/

    @Override
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        //Mon, Feb 22 2016
//        Date endDate = null;
        Calendar calender = Calendar.getInstance();
        calender.set(yy, mm, dd, 0, 0);
        Date date = calender.getTime();
        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd yyyy");
        String requiredFormat = dateFormat.format(date);
        int selectedDate = yy * 1000 + mm * 100 + dd;
        int currentDate = year * 1000 + month * 100 + dayOfMonth;
        if (Util.isFromRecuirmentFragment()) {
            validateDateForRecuriment(requiredFormat, selectedDate, currentDate);
        } else {
            simpleDateValidation(requiredFormat, selectedDate, currentDate);
        }
    }

    /**
     * Method to validate date selection
     *
     * @param requiredFormat
     * @param selectedDate
     * @param currentDate
     */
    private void simpleDateValidation(String requiredFormat, int selectedDate, int currentDate) {
        Timber.e(" slected date: " + this.selectedDate + " current date:" + this.selectedDate);
        dateTimeUpdateListener.changeTime(requiredFormat);
        if (selectedDate >= currentDate) {

        } /*else {
            snackBarWithMesg(mTaskDetailActivity, getResources().getString(R.string.select_date));
        }*/
    }

    /**
     * Method to validate the date when date picked from Recuriment
     *
     * @param requiredFormat
     * @param selectedDate
     * @param currentDate
     */
    private void validateDateForRecuriment(String requiredFormat, int selectedDate, int currentDate) {
        if (selectedDate > currentDate) {
            snackBarWithMesg(mTaskDetailActivity, getResources().getString(R.string.select_date));
            Util.setIsFromRecuirmentFragment(false);
        } else {
            dateTimeUpdateListener.changeTime(requiredFormat);
            Util.setIsFromRecuirmentFragment(false);
        }
    }

    public void snackBarWithMesg(View view, String mesg) {
        Snackbar mSnackbar = Snackbar.make(view, mesg, Snackbar.LENGTH_SHORT);
        snackBarColor(mSnackbar);
        mSnackbar.show();
    }

    private void snackBarColor(Snackbar mSnackbar) {
        View view = mSnackbar.getView();
        mSnackbar.setActionTextColor(Color.WHITE);
        view.setBackgroundResource(R.color.colorPrimary);
    }

    private int getAge(int _year, int _month, int _day) {
        Calendar cal = Calendar.getInstance();
        int y, m, d, a;
        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH)) || ((m == cal.get(Calendar.MONTH)) && (d < cal.get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if (a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }
}



