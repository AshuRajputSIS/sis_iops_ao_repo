package com.sisindia.ai.android.commons;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.utils.Util;

import java.util.Calendar;

/**
 * Created by compass on 4/10/2017.
 */

public class AlarmConfigureBroadcastReceiver extends BroadcastReceiver {
    private AppPreferences appPreferences;
    private AlarmManager alarmManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        appPreferences = new AppPreferences(context);
        if (!Util.isServiceRunning(context, ScheduleSyncJobService.class)) {
            setAlarmManager(context);
        }
    }


    private void setAlarmManager(Context context) {
        long currentMillis = 0l;
        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.HOUR_OF_DAY) < 6){
            calendar.set(Calendar.HOUR_OF_DAY,Constants.MORNING_SYNC_ALARM_START_TIME_IN_HOURS);

        }
        else if(calendar.get(Calendar.HOUR_OF_DAY) < 19 ){
            calendar.set(Calendar.HOUR_OF_DAY,Constants.SYNC_ALARM_START_TIME_IN_HOURS);

        }
        else if(calendar.get(Calendar.HOUR_OF_DAY) > 21){
            calendar.add(Calendar.DATE,1);
            calendar.set(Calendar.HOUR_OF_DAY,Constants.MORNING_SYNC_ALARM_START_TIME_IN_HOURS);
        }
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentMillis = calendar.getTimeInMillis();

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = getSpecificPendingIntent(context);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                currentMillis, Constants.ALRAM_TRIGGER_TIME_INTERVAL,
                pendingIntent);
        appPreferences.setFirstTimeAlarmConfigured(true);
    }


    private PendingIntent getSpecificPendingIntent(Context context) {
        Intent intent = new Intent(context, AlarmTriggerReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                Constants.ALARM_SERVICE_ID, intent, 0);
        return pendingIntent;
    }
}
