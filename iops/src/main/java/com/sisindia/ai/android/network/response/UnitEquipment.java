package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class UnitEquipment {

    @SerializedName("Id")
    private Integer id;
    @SerializedName("UnitId")
    private Integer unitId;
    @SerializedName("EquipmentId")
    private Integer equipmentId;
    @SerializedName("IsCustomerProvided")
    private Boolean isCustomerProvided;
    @SerializedName("Status")
    private Integer status;
    @SerializedName("EquipmentUniqueNo")
    private String equipmentUniqueNo;
    @SerializedName("EquipmentManufacturer")
    private String equipmentManufacturer;
    @SerializedName("PostId")
    private Integer postId;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The equipmentId
     */
    public Integer getEquipmentId() {
        return equipmentId;
    }

    /**
     * @param equipmentId The EquipmentId
     */
    public void setEquipmentId(Integer equipmentId) {
        this.equipmentId = equipmentId;
    }

    /**
     * @return The isCustomerProvided
     */
    public Boolean getIsCustomerProvided() {
        return isCustomerProvided;
    }

    /**
     * @param isCustomerProvided The IsCustomerProvided
     */
    public void setIsCustomerProvided(Boolean isCustomerProvided) {
        this.isCustomerProvided = isCustomerProvided;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The Status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The equipmentUniqueNo
     */
    public String getEquipmentUniqueNo() {
        return equipmentUniqueNo;
    }

    /**
     * @param equipmentUniqueNo The EquipmentUniqueNo
     */
    public void setEquipmentUniqueNo(String equipmentUniqueNo) {
        this.equipmentUniqueNo = equipmentUniqueNo;
    }

    /**
     * @return The equipmentManufacturer
     */
    public String getEquipmentManufacturer() {
        return equipmentManufacturer;
    }

    /**
     * @param equipmentManufacturer The EquipmentManufacturer
     */
    public void setEquipmentManufacturer(String equipmentManufacturer) {
        this.equipmentManufacturer = equipmentManufacturer;
    }

    /**
     * @return The postId
     */
    public Integer getPostId() {
        return postId;
    }

    /**
     * @param postId The PostId
     */
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

}