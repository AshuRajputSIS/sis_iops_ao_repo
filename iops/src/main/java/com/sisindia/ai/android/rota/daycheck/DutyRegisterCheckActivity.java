package com.sisindia.ai.android.rota.daycheck;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.AttachmentMetaDataKeys;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.taskExecution.DutyRegisterCheck;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;


public class DutyRegisterCheckActivity extends DayCheckBaseActivity implements View.OnClickListener, DialogClickListener
        , RadioGroup.OnCheckedChangeListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.availableAtPost)
    View availableAtPostView;
    @Bind(R.id.uptoDate)
    View upToDateView;
    TextView availableAtPostTextView;
    TextView uptoDateTextView;
    @Bind(R.id.duty_register_horizontal_photo)
    View dutyRegisterPhotosLayout;
    @Bind(R.id.scrollView)
    ScrollView scrollView;
    TextView dutyRegisterPhotosHeaderTextview;
    TextView dutyRegisterScanCheckPointsHeaderTextview;
    @Bind(R.id.lastpages)
    CustomFontTextview lastpages;
    @Bind(R.id.dayCheckHeader)
    View mStepperViewParent;
    @Bind(R.id.task_client_check)
    View stepperFourView;
    @Bind(R.id.task_security_check)
    View stepperFiveView;
    @Bind(R.id.stepper_four_completed)
    View stepperFourViewCompleted;
    @Bind(R.id.stepper_three_completed)
    View stepperThreeViewCompleted;
    @Bind(R.id.framelayout_stepper_four)
    FrameLayout framelayout_stepper_four;
    @Bind(R.id.framelayout_stepper_five)
    FrameLayout framelayout_stepper_five;
    @Bind(R.id.stepper_frame_six)
    FrameLayout mstepper_frame_six;
    @Bind(R.id.stepper_five_completed)
    View mstepper_five_completed;
    private RadioButton uptoDateRadioButtonYes;
    private RadioButton availableAtPostRadioButtonYes;
    private RadioButton uptoDateRadioButtonNo;
    private RadioButton availableAtPostRadioButtonNo;
    private ImageView dutyRegisterPicImageView;
    private ImageView scanCheckPointsImageView;
    private ArrayList<View> viewList;
    private ImageView firstPostImage;
    private ImageView secondPostImage;
    private ArrayList<String> imageList;
    private RadioGroup availableAtPostRadioGroup;
    private RadioGroup uptoDateRadioGroup;
    private DutyRegisterCheck dutyRegisterCheck;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duty_register_check);
        ButterKnife.bind(this);
        //setBaseToolbar(toolbar,getResources().getString(R.string.dayCheckingTitle));
        if (getIntent().hasExtra(getResources().getString(R.string.navigationcheckingTitle))) {
            String data = getIntent().getExtras().getString(getResources().getString(R.string.navigationcheckingTitle), getResources().getString(R.string.dayCheckingTitle));

            if (data.equalsIgnoreCase(getResources().getString(R.string.CHECKING_TYPE_NIGHT_CHECKING))) {
                setBaseToolbar(toolbar, data);
                stepperFourView.setVisibility(View.GONE);
                stepperFourViewCompleted.setVisibility(View.GONE);
                stepperThreeViewCompleted.setVisibility(View.GONE);
                framelayout_stepper_four.setVisibility(View.GONE);
                framelayout_stepper_five.setVisibility(View.GONE);
                mstepper_five_completed.setVisibility(View.GONE);
                stepperFiveView.setVisibility(View.GONE);
                mstepper_frame_six.setVisibility(View.GONE);
            } else {
                setBaseToolbar(toolbar, data);
            }
        } else {
            setBaseToolbar(toolbar, getResources().getString(R.string.dayCheckingTitle));
        }

        setUiViews();
        if (rotaTaskModel != null) {
            setHeaderUnitName(rotaTaskModel.getUnitName());
        }
        getStepperViews(mStepperViewParent);
        getTaskExecutionResult();
        dutyRegisterCheck = new DutyRegisterCheck();
        dutyRegisterCheck.setAvailableAtPost(getResources().getString(R.string.YES));
        dutyRegisterCheck.setUptoDate(getResources().getString(R.string.YES));

        imageList = new ArrayList<String>();
        //removeAlreadyTakenImages();
    }
/*
    public void removeAlreadyTakenImages() {
        if(attachmentMetaDataModelHashMap != null && attachmentMetaDataModelHashMap.size() != 0){
            for(String key : AttachmentMetaDataKeys.GUARD_CHECK){
                if(attachmentMetaDataModelHashMap.containsKey(key)){
                    attachmentMetaDataModelHashMap.remove(key);
                }
            }
        }
    }*/

    private void setUiViews() {
        //ButterKnife.bind(this,upToDateView);
        // ButterKnife.bind(this,availableAtPostView);
        availableAtPostTextView = (TextView) availableAtPostView.findViewById(R.id.radioGroupHeader);
        availableAtPostTextView.setText(getResources().getString(R.string.AVAILABLE_AT_POST_AND_UPTO_DATE));
        availableAtPostRadioButtonYes = (RadioButton) availableAtPostView.findViewById(R.id.radioButtonYes);
        availableAtPostRadioButtonYes.setChecked(true);
        availableAtPostRadioGroup = (RadioGroup) availableAtPostView.findViewById(R.id.radioGroup);
        availableAtPostRadioGroup.setOnCheckedChangeListener(this);

        uptoDateTextView = (TextView) upToDateView.findViewById(R.id.radioGroupHeader);
        uptoDateTextView.setText(getResources().getString(R.string.SOP_Available));
        uptoDateRadioButtonYes = (RadioButton) upToDateView.findViewById(R.id.radioButtonYes);
        uptoDateRadioButtonYes.setChecked(true);
        uptoDateRadioGroup = (RadioGroup) upToDateView.findViewById(R.id.radioGroup);
        uptoDateRadioGroup.setOnCheckedChangeListener(this);

        dutyRegisterPhotosHeaderTextview = (TextView) dutyRegisterPhotosLayout.findViewById(R.id.photosHeaderTv);
        dutyRegisterPhotosHeaderTextview.setText(getResources().getString(R.string.PHOTOS_OF_DUTY_HEADING));
        lastpages.setText(getResources().getString(R.string.lastTwoPages));
        dutyRegisterPicImageView = (ImageView) dutyRegisterPhotosLayout.findViewById(R.id.defaultCameraPicIv);
        dutyRegisterPicImageView.setTag(getResources().getString(R.string.photos));
        dutyRegisterPicImageView.setOnClickListener(this);
        firstPostImage = (ImageView) dutyRegisterPhotosLayout.findViewById(R.id.post_one_image);
        secondPostImage = (ImageView) dutyRegisterPhotosLayout.findViewById(R.id.post_two_image);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_duty_register, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.done_menu) {
            if ("YES".equalsIgnoreCase(dutyRegisterCheck.getAvailableAtPost()) && imageList != null && imageList.size() == 0) {
                snackBarWithMesg(scrollView, getResources().getString(R.string.valiadtion_msg));
                return true;
            } else {
                OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();
                mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(3).name(),DayCheckEnum.Duty_Register_Check.getValue());

                dayNightChecking.setDutyRegisterCheck(dutyRegisterCheck);
                dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
                setTaskExecutionResult();
                finish();
            }

            // showDialog();


            return true;
        } else if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {

        if (((String) view.getTag()).equalsIgnoreCase(getResources().getString(R.string.scan_check_points))) {

        } else {
            if (imageList != null && imageList.size() < 2) {
                Util.mSequenceNumber++;
                Intent capturePictureActivity = new Intent(DutyRegisterCheckActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.DUTY_REGISTER_CHECK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                startActivityForResult(capturePictureActivity, Constants.NUMBER_ONE);
            } else {
                snackBarWithMesg(scrollView, getResources().getString(R.string.max_pics_taken_msg));
            }

        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String imageUrl = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();

            setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            imageList.add(imageUrl);
           /* setAttachmentMetaDataModelInMap((AttachmentMetaDataKeys.DUTY_REGISTER_CHECK)[imageList
                            .size()-1],
                    (AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));*/
            setThumbnail(imageList);
        }
    }

    private void setThumbnail(ArrayList<String> imageList) {
        if (imageList != null && imageList.size() != 0) {

            if (imageList.size() == 1) {
                firstPostImage.setImageURI(Uri.parse(imageList.get(0)));
            } else {
                secondPostImage.setImageURI(Uri.parse(imageList.get(1)));
            }
        }
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        if(clickType == 0){

            mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(3).name(),DayCheckEnum.Duty_Register_Check.getValue());
            finish();
        }
        else{
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (availableAtPostRadioGroup == group) {
            dutyRegisterCheck.setAvailableAtPost(((RadioButton) findViewById(checkedId)).getText().toString());
        } else if (uptoDateRadioGroup == group) {
            dutyRegisterCheck.setUptoDate(((RadioButton) findViewById(checkedId)).getText().toString());
        }
    }
}
