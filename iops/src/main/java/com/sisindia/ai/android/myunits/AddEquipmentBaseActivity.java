package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.loadconfiguration.Equipments;
import com.sisindia.ai.android.myunits.models.AddEquipmentLocalMO;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.SyncingUnitEquipmentModel;
import com.sisindia.ai.android.utils.bottomsheet.CustomBottomSheetFragment;
import com.sisindia.ai.android.utils.bottomsheet.OnBottomSheetItemClickListener;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Saruchi on 10-05-2016.
 */
public class AddEquipmentBaseActivity extends BaseActivity implements View.OnClickListener, ShowBottomSheetListener, OnBottomSheetItemClickListener {
    protected static final int ADD_POST_REQUEST_CODE =11;
    public static final String ADD_EQUIPMENT_KEY = "add_equipment";

    @Bind(R.id.addEquipmentTypeRelativeLayout)
    RelativeLayout addEquipmentTypeRelativeLayout;
    @Bind(R.id.add_equipmentType_value)
    CustomFontTextview addEquipmentTypeValue;
    @Bind(R.id.addEquipmentCountRelativeLayout)
    RelativeLayout addEquipmentCountRelativeLayout;
    @Bind(R.id.add_equipment_count)
    CustomFontEditText addEquipmentCount;
    @Bind(R.id.addEquipmentProvidedByRelativeLayout)
    RelativeLayout addEquipmentProvidedByRelativeLayout;
    @Bind(R.id.add_Equipment_provided_by)
    CustomFontTextview addEquipmentProvidedBy;
    ShowBottomSheetListener showBottomSheetListener;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    ArrayList<Equipments> equipmentsList = new ArrayList<>();
    ArrayList<String> equipmentsNameList = new ArrayList<>();
    // TODO: Rename and change types of parameters
    private Context mContext;
    private ActionBar actionBar;
    private Resources mResources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_eqipment_activity);
        ButterKnife.bind(this);
        mContext = this;
        mResources =getResources();
        showBottomSheetListener = this;
        addEquipmentTypeRelativeLayout.setOnClickListener(this);
        addEquipmentProvidedByRelativeLayout.setOnClickListener(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        fetchEquipmentList();
        if (actionBar != null) {
            actionBar.setTitle("Add Equipment");
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.addEquipmentTypeRelativeLayout) {
            showBottomSheetListener.showBottomSheet(equipmentsNameList, view.getId());
        } else if (view.getId() == R.id.addEquipmentProvidedByRelativeLayout) {
            ArrayList<String> element = new ArrayList<String>();
            element.add(mResources.getString(R.string.add_provided_by_sis));
            element.add(mResources.getString(R.string.client_str));
            showBottomSheetListener.showBottomSheet(element, view.getId());
        }
    }

    public void setValue(String value, int viewId) {
        switch (viewId) {
            case R.id.addEquipmentTypeRelativeLayout:
                addEquipmentTypeValue.setText(value);
                break;
            case R.id.addEquipmentProvidedByRelativeLayout:
                addEquipmentProvidedBy.setText(value);
                break;
        }
    }

    @Override
    public void onSheetItemClick(String itemSelected, int viewId) {
        setValue(itemSelected, viewId);
    }

    @Override
    public void onUnitSheetItemClick(MyUnitHomeMO unitHomeMOList, int viewId) {

    }

    @Override
    public void showBottomSheet(ArrayList<String> elements, int viewId) {
        setBottomSheetFragment(elements, viewId);
    }

    @Override
    public void showUnitModelBottomSheet(List<MyUnitHomeMO> myUnitHomeMOs, int viewId) {

    }

    private void setBottomSheetFragment(ArrayList<String> elements, int viewId) {

        CustomBottomSheetFragment bottomSheetDialogFragment = new CustomBottomSheetFragment();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        bottomSheetDialogFragment.setSheetItemClickListener(this);
        bottomSheetDialogFragment.setBottomSheetData(elements, viewId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_post_save: {
                validateData();
                break;
            }
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchEquipmentList() {
        equipmentsList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                .getEquipmentList();
        for (Equipments equipments : equipmentsList) {
            equipmentsNameList.add(equipments.getEquipmentName());
        }
    }

    private int getEquipmentId(String equipmentName) {
        for (Equipments equipments : equipmentsList) {
            if (equipmentName.equalsIgnoreCase(equipments.getEquipmentName())) {
                return equipments.getId();
            }
        }
        return 0;
    }

    private void validateData() {
        if ((TextUtils.isEmpty(addEquipmentTypeValue.getText().toString())) ||
                (addEquipmentTypeValue.getText().toString()
                        .equalsIgnoreCase(mResources.getString(R.string.select_equipment_type))) ||
                (TextUtils.isEmpty(addEquipmentCount.getText().toString())) ||
                (TextUtils.isEmpty(addEquipmentProvidedBy.getText().toString()))) {
            snackBarWithMesg(addEquipmentTypeRelativeLayout, mResources
                    .getString(R.string.client_handshake_validation));
        } else {
            if(Integer.valueOf(addEquipmentCount.getText().toString())!=0) {
                SyncingUnitEquipmentModel model = new SyncingUnitEquipmentModel();
                AddEquipmentLocalMO addEquipmentLocalMO = new AddEquipmentLocalMO();
                addEquipmentLocalMO.setEquipmentCount(Integer.valueOf(addEquipmentCount.getText().toString()));
                addEquipmentLocalMO.setEquipmentType(addEquipmentTypeValue.getText().toString());
                int id = getEquipmentId(addEquipmentTypeValue.getText().toString());
                addEquipmentLocalMO.setEquipmentTypId(id);
                if (addEquipmentProvidedBy.getText().toString().equalsIgnoreCase(mResources.getString(R.string.client_str))) {
                    model.setIsCustomerProvided(1);

                } else {
                    model.setIsCustomerProvided(0);
                }
                model.setAddEquipmentLocalMO(addEquipmentLocalMO);
                Intent editIntent = new Intent();
                editIntent.putExtra(ADD_EQUIPMENT_KEY, model);
                setResult(RESULT_OK, editIntent);
                finish();
            }
            else{
                snackBarWithMesg(addEquipmentTypeRelativeLayout,
                        mResources.getString(R.string.error_enter_valid_count));
            }
        }
    }

}
