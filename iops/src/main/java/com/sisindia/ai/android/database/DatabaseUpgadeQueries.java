package com.sisindia.ai.android.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.myunits.models.UnitBarrack;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Saruchi on 02-02-2017.
 */

public class DatabaseUpgadeQueries {

    private static final String TEXT_TYPE = "TEXT";
    private static final String INTEGER_TYPE = "INTEGER";
    private TableStatement tableStatement;
    private List<UnitBarrack> unitBarrackList;

    public DatabaseUpgadeQueries() {
        tableStatement = new TableStatement();
    }

   /* protected void versionOneUpgrade(SQLiteDatabase db) {
        alterTable(TableNameAndColumnStatement.ADHOC_STATUS, TEXT_TYPE, TableNameAndColumnStatement.TASK_TABLE, db);
    }*/

    /**
     * older version
     *
     * @param db
     */

   /* protected void versionTwoUpgarde(SQLiteDatabase db) {
        if (!columnExist(TableNameAndColumnStatement.ADHOC_STATUS, TableNameAndColumnStatement.TASK_TABLE, db)) {
            alterTable(TableNameAndColumnStatement.ADHOC_STATUS, TEXT_TYPE, TableNameAndColumnStatement.TASK_TABLE, db);
        } else {
            Timber.d("is_adhoc column already there");
        }
        alterTable(TableNameAndColumnStatement.IS_TASK_STARTED, INTEGER_TYPE, TableNameAndColumnStatement.TASK_TABLE, db);
        db.execSQL(tableStatement.unitBillingCheckListTable());
    }*/

    /*protected void versionThreeUpgrade(SQLiteDatabase db) {
        if (!columnExist(TableNameAndColumnStatement.ID, TableNameAndColumnStatement.UNIT_BARRACK_TABLE, db)) {
            alterTable(TableNameAndColumnStatement.ID, INTEGER_TYPE, TableNameAndColumnStatement.UNIT_BARRACK_TABLE, db);
        } else {
            Timber.d("id column already there");
        }
    }*/

   /* protected void versionFourUpgrade(SQLiteDatabase db) {
        db.execSQL(tableStatement.dependentSyncTable());
    }*/


    /**
     * keep on adding  the upgrade db version changes in this method also
     * this is handled if user is instaling the fresh build .
     *
     * @param db
     */
    protected void onCreateUpgrade(SQLiteDatabase db) {
       /* versionOneUpgrade(db);
        versionTwoUpgarde(db);
        versionThreeUpgrade(db);
        versionFourUpgrade(db);
        versionFiveUpgrade(db);
        versionSixUpgrade(db);
        versionSevenUpgrade(db);
        versionEightUpgrade(db);
        versionNineUpgrade(db);
        versionTenUpgrade(db);*/
    }

    /*protected void versionSixUpgrade(SQLiteDatabase db) {
        alterTable(TableNameAndColumnStatement.IS_MANDATORY, INTEGER_TYPE, TableNameAndColumnStatement.BARRACK_INSPECTION_ANSWER_TABLE, db);
        alterTable(TableNameAndColumnStatement.CONTROL_TYPE, TEXT_TYPE, TableNameAndColumnStatement.BARRACK_INSPECTION_ANSWER_TABLE, db);
    }
*/
    private void insertIntoUnitBarrackTable() {
        if (unitBarrackList != null && unitBarrackList.size() != 0) {
            IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitBarrack(unitBarrackList, false);
        }
    }

   /* public void versionFiveUpgrade(SQLiteDatabase db) {
        if (!columnExist(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION, TableNameAndColumnStatement.TASK_TABLE, db)) {
            alterTable(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION, TEXT_TYPE, TableNameAndColumnStatement.TASK_TABLE, db);
        } else {
            Timber.d(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION + " column already there");
        }
        db.execSQL(tableStatement.notificationListTable());
        db.execSQL(tableStatement.syncRequest());
    }*/

    private static boolean columnExist(String columnName, String tableName, SQLiteDatabase sqlite) {
        Cursor cursor = null;
        boolean isExist = true;
        try {
            String updateQuery = "select " + columnName + " from " + tableName;
            cursor = sqlite.rawQuery(updateQuery, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    isExist = true;
                }
            }
            Timber.d("cursor detail object  %s", isExist);
        } catch (Exception exception) {
            exception.getCause();
            isExist = false;
        }
        return isExist;
    }
/*
    public void versionSevenUpgrade(SQLiteDatabase db) {
//        alterTable(TableNameAndColumnStatement.GUARD_CODE, TEXT_TYPE, TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, db);
//        alterTable(TableNameAndColumnStatement.GUARD_NAME, TEXT_TYPE, TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE, db);

        alterTable(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME, TEXT_TYPE, TableNameAndColumnStatement.UNIT_TABLE, db);
        alterTable(TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);
        alterTable(TableNameAndColumnStatement.UNIT_RAISING_ID, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);

        alterTable(TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);
        alterTable(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);
        alterTable(TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);

        db.execSQL(tableStatement.unitRaising());
        db.execSQL(tableStatement.unitRaisingDeferred());
        db.execSQL(tableStatement.unitRaisingCancellation());
        db.execSQL(tableStatement.getUnitRaisingStrengthDetail());
    }*/

    /*public void versionEightUpgrade(SQLiteDatabase db) {
        unitBarrackList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitBarracks(db);
        db.execSQL("drop table " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE);
        db.execSQL(tableStatement.unitBarrackTable());
        insertIntoUnitBarrackTable();
        //@Ashu
        db.execSQL(tableStatement.queryExecutorTable());
    }*/

    /*public void versionNineUpgrade(SQLiteDatabase db) {

        if (!columnExist(TableNameAndColumnStatement.NFC_EXEMPTED, TableNameAndColumnStatement.UNIT_TABLE, db)) {
            alterTable(TableNameAndColumnStatement.NFC_EXEMPTED, INTEGER_TYPE, TableNameAndColumnStatement.UNIT_TABLE, db);
        } else {
            Timber.d("nfc_exempted column already there");
        }
        db.execSQL(tableStatement.guardTable());
       *//* kitDistributionList = IOPSApplication.getInstance().getKitItemSelectionStatementDB().getKitDistributionList();
        db.execSQL("drop table " + TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE);
        db.execSQL(tableStatement.kitDistributionTable());
        insertIntoKitDistributionTables();*//*
    }*/

    /*private void insertIntoKitDistributionTables() {
        FcmAKRModelInsertion fcmAKRModelInsertion = IOPSApplication.getInstance().getFcmAkrModelInsertionDBInstance();
        if (kitDistributionList != null && kitDistributionList.size() != 0) {
            fcmAKRModelInsertion.insertAkrDistributionModel(kitDistributionList);
        }
    }*/

    //@Ashu : Adding below method on 16Nov17
   /* public void versionTenUpgrade(SQLiteDatabase db) {
        if (!columnExist(TableNameAndColumnStatement.DEVICE_NO, TableNameAndColumnStatement.BARRACK_TABLE, db))
            alterTable(TableNameAndColumnStatement.DEVICE_NO, TEXT_TYPE, TableNameAndColumnStatement.BARRACK_TABLE, db);
        if (!columnExist(TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED, TableNameAndColumnStatement.BARRACK_TABLE, db))
            alterTable(TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED, INTEGER_TYPE, TableNameAndColumnStatement.BARRACK_TABLE, db);
        alterTable(TableNameAndColumnStatement.BARRACK_ID, INTEGER_TYPE, TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, db);
    }*/

    //@Ashu : Adding below method on 04-JAN-2017
    /*public void versionElevenUpgrade(SQLiteDatabase db) {
        alterTable("TaskAssigned", TEXT_TYPE, TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE, db);
        alterTable("TaskCompleted", TEXT_TYPE, TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE, db);
    }
*/
    //@Ashu : Adding below method on 27-Sept-2018
   /* public void versionTwelveUpgrade(SQLiteDatabase db) {
        try {
            db.execSQL(tableStatement.unitTypeTable());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    //------------------Method to alter tables----------------------//
    /*private void alterTable(String columnname, String dataType, String tablename, SQLiteDatabase db) {
        try {
            String upgradeQuery = "ALTER TABLE " + tablename + " ADD COLUMN " + columnname + " " + dataType;
            Timber.d(" alterTable upgradeQuery   %s" + tablename, upgradeQuery);
            db.execSQL(upgradeQuery);
        } catch (SQLException e) {
            Crashlytics.logException(e);
            Timber.e("error alterTable tablename %s" + tablename, e.getCause());
        }
    }*/

}
