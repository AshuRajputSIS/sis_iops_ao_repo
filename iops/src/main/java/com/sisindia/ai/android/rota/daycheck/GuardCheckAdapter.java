package com.sisindia.ai.android.rota.daycheck;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 13-06-2016.
 */
public class GuardCheckAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private ArrayList<GuardDetail> guardDetailsList;


    public GuardCheckAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<GuardDetail> getGuardDetailsList() {
        return guardDetailsList;
    }

    public void setGuardDetailsList(ArrayList<GuardDetail> guardDetailsList) {
        this.guardDetailsList = guardDetailsList;
    }


    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    @Override
    public GuardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mGaurdView = LayoutInflater.from(mContext).inflate(R.layout.guard_row, parent, false);
        GuardViewHolder mGuardViewHolder = new GuardViewHolder(mGaurdView);
        return mGuardViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        GuardViewHolder guardViewHolder = (GuardViewHolder) holder;
        if(guardDetailsList.get(position).getGuardName() != null) {
            guardViewHolder.guardName.setText(guardDetailsList.get(position).getGuardName());
        }else{
            guardViewHolder.guardName.setText(guardDetailsList.get(position).getEmployeeNo());
        }
        if(guardDetailsList.get(position).getmGuardImageUri()!= null) {
            guardViewHolder.guardPhoto.setImageURI(Uri.parse(guardDetailsList.get(position).getmGuardImageUri()));
        }

    }

    @Override
    public int getItemCount() {

        int count = guardDetailsList == null ? 0 : guardDetailsList.size();
        return count;
    }

    public class GuardViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        @Bind(R.id.guard_photo)
        ImageView guardPhoto;
        @Bind(R.id.guard_name)
        TextView guardName;
        @Bind(R.id.imageView4)
        ImageView imageView4;


        public GuardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {

            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());

            return false;
        }
    }

    static class ViewHolder {


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
