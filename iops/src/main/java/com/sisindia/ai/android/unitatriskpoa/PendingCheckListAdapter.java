package com.sisindia.ai.android.unitatriskpoa;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.issues.improvements.EnterDateActivity;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 15/7/16.
 */

public class PendingCheckListAdapter extends RecyclerView.Adapter<PendingCheckListAdapter.UARACheckListViewHolder>  {
    private List<ActionPlanMO> pendingCheckList;
    Context mcontext;
    DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public void setPendingCheckListItemListener(PendingCheckListItemListener pendingCheckListItemListener) {
        this.pendingCheckListItemListener = pendingCheckListItemListener;
    }

    public PendingCheckListItemListener pendingCheckListItemListener;


    public PendingCheckListAdapter(Context context, List<ActionPlanMO> pendingActionPlanMOList) {
        this.mcontext = context;
        this.pendingCheckList =pendingActionPlanMOList;
        this.dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

    }




    @Override
    public int getItemCount() {

        return pendingCheckList.size();

    }


    @Override
    public UARACheckListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.action_plan_checklist, parent, false);
        UARACheckListViewHolder checkListViewHolder = new UARACheckListViewHolder(view);

        return checkListViewHolder;
    }

    @Override
    public void onBindViewHolder(UARACheckListViewHolder holder, int position) {



        UARACheckListViewHolder checkListViewHolder =holder;

        ActionPlanMO actionPlanMO =pendingCheckList.get(position);
        checkListViewHolder.itemName.setText("POA Name :"+actionPlanMO.getActionPlan());
        checkListViewHolder.assignName.setText("Assigned To : "+
                actionPlanMO.getAssignToName());
        checkListViewHolder.reasonLable.setText("Reason :"+actionPlanMO.getReason());
        checkListViewHolder.plannedDate.setText("Planned Date : "+
                dateTimeFormatConversionUtil.
                        convertDateTimeToDate(actionPlanMO.getPlanDate()));
        checkListViewHolder.completedDate.setVisibility(View.GONE);
        checkListViewHolder.checkBox.setClickable(actionPlanMO.getIsCompleted());
        checkListViewHolder.checkBox.setChecked(actionPlanMO.getIsCompleted());









    }

    public class UARACheckListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @Bind(R.id.checkBox_item)
        CheckBox checkBox;
        @Bind(R.id.planned_date)
        CustomFontTextview plannedDate;
        @Bind(R.id.item_name)
        CustomFontTextview itemName;
        @Bind(R.id.assigned_to)
        CustomFontTextview assignName;
        @Bind(R.id.complete_date)
        CustomFontTextview completedDate;
        @Bind(R.id.item_click)
        LinearLayout linearLayout;
        @Bind(R.id.reasonLable)
        CustomFontTextview reasonLable;



        public UARACheckListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            linearLayout.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {


            switch (v.getId()) {
                case R.id.item_click:
                    pendingCheckListItemListener.onItemClick(pendingCheckList.get(getLayoutPosition()));

                    break;


            }

        }


    }


}



