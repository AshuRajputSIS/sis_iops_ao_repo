package com.sisindia.ai.android.database.notificationDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationIR;

/**
 * Created by compass on 7/20/2017.
 */

public class NotificationsReceiptDB extends SISAITrackingDB {

    public NotificationsReceiptDB(Context context) {
        super(context);
    }

    public synchronized void notificationInsertion(NotificationIR notificationIR) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        if (notificationIR != null) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.RECIPIENT_ID, notificationIR.getRecipientId());
                values.put(TableNameAndColumnStatement.CALLBACK_URL, notificationIR.getCallbackUrl());
                values.put(TableNameAndColumnStatement.IS_SUCCESSFUL, notificationIR.getIsSuccessful());
                values.put(TableNameAndColumnStatement.NOTIFICATION_ID, notificationIR.getAppNotificationId());
                values.put(TableNameAndColumnStatement.NOTIFICATION_NAME, notificationIR.getNotificationName());
                values.put(TableNameAndColumnStatement.IS_SYNCED, notificationIR.isSynced());

                sqlite.insertOrThrow(TableNameAndColumnStatement.NOTIFICATION_RECEIPT, null, values);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

    }
}
