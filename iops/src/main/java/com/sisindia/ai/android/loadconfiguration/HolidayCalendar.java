package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class HolidayCalendar implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("HolidayDate")
    @Expose
    private String holidayDate;
    @SerializedName("HolidayDescription")
    @Expose
    private String holidayDescription;
    @SerializedName("IsRegionalHoliday")
    @Expose
    private Boolean isRegionalHoliday;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The holidayDate
     */
    public String getHolidayDate() {
        return holidayDate;
    }

    /**
     *
     * @param holidayDate
     * The HolidayDate
     */
    public void setHolidayDate(String holidayDate) {
        this.holidayDate = holidayDate;
    }

    /**
     *
     * @return
     * The holidayDescription
     */
    public String getHolidayDescription() {
        return holidayDescription;
    }

    /**
     *
     * @param holidayDescription
     * The HolidayDescription
     */
    public void setHolidayDescription(String holidayDescription) {
        this.holidayDescription = holidayDescription;
    }

    /**
     *
     * @return
     * The isRegionalHoliday
     */
    public Boolean getIsRegionalHoliday() {
        return isRegionalHoliday;
    }

    /**
     *
     * @param isRegionalHoliday
     * The IsRegionalHoliday
     */
    public void setIsRegionalHoliday(Boolean isRegionalHoliday) {
        this.isRegionalHoliday = isRegionalHoliday;
    }

    /**
     *
     * @return
     * The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
