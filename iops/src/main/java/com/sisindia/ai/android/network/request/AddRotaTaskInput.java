package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 20-06-2016.
 */

public class AddRotaTaskInput {
    @SerializedName("Id")
    private Integer id;
    @SerializedName("unitId")
    private Integer unitId;
    @SerializedName("TaskTypeId")
    private Integer TaskTypeId;
    @SerializedName("AssigneeId")
    private Integer AssigneeId;
    @SerializedName("EstimatedExecutionTime")
    private Integer EstimatedExecutionTime;
    @SerializedName("ActualExecutionTime")
    private Integer ActualExecutionTime;
    @SerializedName("TaskStatusId")
    private Integer TaskStatusId;
    @SerializedName("Description")
    private String Description;
    @SerializedName("EstimatedTaskExecutionStartDateTime")
    private String EstimatedTaskExecutionStartDateTime;
    @SerializedName("EstimatedTaskExecutionEndDateTime")
    private String EstimatedTaskExecutionEndDateTime;
    @SerializedName("ActualTaskExecutionStartDateTime")
    private String ActualTaskExecutionStartDateTime;
    @SerializedName("ActualTaskExecutionEndDateTime")
    private String ActualTaskExecutionEndDateTime;
    @SerializedName("IsAutoCreation")
    private Integer IsAutoCreation;
    @SerializedName("ExecutorId")
    private Integer ExecutorId;
    @SerializedName("CreatedDateTime")
    private String CreatedDateTime;
    @SerializedName("CreatedById")
    private Integer CreatedById;
    @SerializedName("ApprovedById")
    private Integer ApprovedById;
    @SerializedName("RelatedTaskId")
    private Integer RelatedTaskId;
    @SerializedName("UnitPostId")
    private Integer UnitPostId;
    @SerializedName("AdditionalComments")
    private String AdditionalComments;
    @SerializedName("AssignedById")
    private Integer AssignedById;
    @SerializedName("BarrackId")
    private Integer BarrackId;
    @SerializedName("ChangeRouteReasonId")
    private Integer changeRouteReasonId;
    @SerializedName("IsCreatedFromROCC")
    private Integer IsCreatedFromROCC;

    public Integer getOtherReasonId() {
        return otherReasonId;
    }

    public void setOtherReasonId(Integer otherReasonId) {
        this.otherReasonId = otherReasonId;
    }

    @SerializedName("OtherReasonId")
    private Integer otherReasonId;

    public String getActualTaskExecutionEndDateTime() {
        return ActualTaskExecutionEndDateTime;
    }

    public void setActualTaskExecutionEndDateTime(String actualTaskExecutionEndDateTime) {
        ActualTaskExecutionEndDateTime = actualTaskExecutionEndDateTime;
    }

    public Integer getIsAutoCreation() {
        return IsAutoCreation;
    }

    public void setIsAutoCreation(Integer isAutoCreation) {
        IsAutoCreation = isAutoCreation;
    }

    public Integer getExecutorId() {
        return ExecutorId;
    }

    public void setExecutorId(Integer executorId) {
        ExecutorId = executorId;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public Integer getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(Integer createdById) {
        CreatedById = createdById;
    }

    public Integer getApprovedById() {
        return ApprovedById;
    }

    public void setApprovedById(Integer approvedById) {
        ApprovedById = approvedById;
    }

    public Integer getRelatedTaskId() {
        return RelatedTaskId;
    }

    public void setRelatedTaskId(Integer relatedTaskId) {
        RelatedTaskId = relatedTaskId;
    }

    public Integer getUnitPostId() {
        return UnitPostId;
    }

    public void setUnitPostId(Integer unitPostId) {
        UnitPostId = unitPostId;
    }

    public String getAdditionalComments() {
        return AdditionalComments;
    }

    public void setAdditionalComments(String additionalComments) {
        AdditionalComments = additionalComments;
    }

    public Integer getAssignedById() {
        return AssignedById;
    }

    public void setAssignedById(Integer assignedById) {
        AssignedById = assignedById;
    }

    public Integer getBarrackId() {
        return BarrackId;
    }

    public void setBarrackId(Integer barrackId) {
        BarrackId = barrackId;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getTaskTypeId() {
        return TaskTypeId;
    }

    public void setTaskTypeId(Integer taskTypeId) {
        TaskTypeId = taskTypeId;
    }

    public Integer getAssigneeId() {
        return AssigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        AssigneeId = assigneeId;
    }

    public Integer getEstimatedExecutionTime() {
        return EstimatedExecutionTime;
    }

    public void setEstimatedExecutionTime(Integer estimatedExecutionTime) {
        EstimatedExecutionTime = estimatedExecutionTime;
    }

    public Integer getActualExecutionTime() {
        return ActualExecutionTime;
    }

    public void setActualExecutionTime(Integer actualExecutionTime) {
        ActualExecutionTime = actualExecutionTime;
    }

    public Integer getTaskStatusId() {
        return TaskStatusId;
    }

    public void setTaskStatusId(Integer taskStatusId) {
        TaskStatusId = taskStatusId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEstimatedTaskExecutionStartDateTime() {
        return EstimatedTaskExecutionStartDateTime;
    }

    public void setEstimatedTaskExecutionStartDateTime(String estimatedTaskExecutionStartDateTime) {
        EstimatedTaskExecutionStartDateTime = estimatedTaskExecutionStartDateTime;
    }

    public String getEstimatedTaskExecutionEndDateTime() {
        return EstimatedTaskExecutionEndDateTime;
    }

    public void setEstimatedTaskExecutionEndDateTime(String estimatedTaskExecutionEndDateTime) {
        EstimatedTaskExecutionEndDateTime = estimatedTaskExecutionEndDateTime;
    }

    public String getActualTaskExecutionStartDateTime() {
        return ActualTaskExecutionStartDateTime;
    }

    public void setActualTaskExecutionStartDateTime(String actualTaskExecutionStartDateTime) {
        ActualTaskExecutionStartDateTime = actualTaskExecutionStartDateTime;
    }

    public Integer getChangeRouteReasonId() {
        return changeRouteReasonId;
    }

    public void setChangeRouteReasonId(Integer changeRouteReasonId) {
        this.changeRouteReasonId = changeRouteReasonId;
    }

    public Integer getIsCreatedFromROCC() {
        return IsCreatedFromROCC;
    }

    @Override
    public String toString() {
        return "AddRotaTaskInput{" +
                "id=" + id +
                ", unitId=" + unitId +
                ", TaskTypeId=" + TaskTypeId +
                ", AssigneeId=" + AssigneeId +
                ", EstimatedExecutionTime=" + EstimatedExecutionTime +
                ", ActualExecutionTime=" + ActualExecutionTime +
                ", TaskStatusId=" + TaskStatusId +
                ", Description='" + Description + '\'' +
                ", EstimatedTaskExecutionStartDateTime='" + EstimatedTaskExecutionStartDateTime + '\'' +
                ", EstimatedTaskExecutionEndDateTime='" + EstimatedTaskExecutionEndDateTime + '\'' +
                ", ActualTaskExecutionStartDateTime='" + ActualTaskExecutionStartDateTime + '\'' +
                ", ActualTaskExecutionEndDateTime='" + ActualTaskExecutionEndDateTime + '\'' +
                ", IsAutoCreation=" + IsAutoCreation +
                ", ExecutorId=" + ExecutorId +
                ", CreatedDateTime='" + CreatedDateTime + '\'' +
                ", CreatedById=" + CreatedById +
                ", ApprovedById=" + ApprovedById +
                ", RelatedTaskId=" + RelatedTaskId +
                ", UnitPostId=" + UnitPostId +
                ", AdditionalComments='" + AdditionalComments + '\'' +
                ", AssignedById=" + AssignedById +
                ", BarrackId=" + BarrackId +
                ", changeRouteReasonId=" + changeRouteReasonId +
                ", IsCreatedFromROCC=" + IsCreatedFromROCC +
                ", otherReasonId=" + otherReasonId +
                '}';
    }

    public void setIsCreatedFromROCC(Integer isCreatedFromROCC) {
        IsCreatedFromROCC = isCreatedFromROCC;
    }
}