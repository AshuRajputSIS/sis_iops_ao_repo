package com.sisindia.ai.android.unitraising;

/**
 * Created by compass on 8/10/2017.
 */

 public class UnitRaisingDeferredOrCancelledMo {
    public  int unitId;
    public  String plannedRaisingDate;
    public  String reasonForCancel;
    public  String remarks;
    public String reasonForChange;
    public String newRaisingDate;
    public int unitRaisingCancellationId;
    public int unitRaisingDeferredId;
    public String createdDate;
}
