package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hannan Shaik on 29/06/16.
 */

public class AppInfo {
    @SerializedName("VersionNo")
    public int versionNumber;
    @SerializedName("PublishedDateTime")
    public String date;
    @SerializedName("ApkFileHostedUrl")
    public String downloadUrl;
    @SerializedName("isAIBlocked")
    public boolean isBlocked;
    @SerializedName("IsGrievance")
    public int isGrievance;
}
