package com.sisindia.ai.android.commons;

/**
 * Created by compass on 4/26/2017.
 */

public class UpdatedConfigurationApiCalls {
    public boolean isUpdateSecurityRiskOptions;
    public boolean isUpdatedMasterActionPlan;
    public boolean isUpdatedClientCoordinationIssue;
    public boolean isDeltedImprovementPlanForThisBuild;
    public boolean isUpdatedBarrackInspectionQuestions;
    public boolean isUpdatedBarrackInspectionQuestionAndOptions;
    public boolean isGuardInformationUpdated;
}
