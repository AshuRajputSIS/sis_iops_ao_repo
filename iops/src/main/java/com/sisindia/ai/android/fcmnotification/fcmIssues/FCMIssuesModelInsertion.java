package com.sisindia.ai.android.fcmnotification.fcmIssues;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.FcmIssuesActionPlanData;
import com.sisindia.ai.android.issues.FcmIssuesData;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.improvements.ImprovementPlanMO;
import com.sisindia.ai.android.issues.models.ActionPlanMO;
import com.sisindia.ai.android.issues.models.IssueClosureMo;
import com.sisindia.ai.android.network.request.IssueIMO;
import com.sisindia.ai.android.rota.models.RotaMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 01-12-2016.
 */

public class FCMIssuesModelInsertion extends SISAITrackingDB {
    private boolean isInsertedSuccessfully;
    private SQLiteDatabase sqlite=null;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public FCMIssuesModelInsertion(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public synchronized boolean insertIssuesModel(FcmIssuesActionPlanData fcmIssuesActionPlanData) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if(fcmIssuesActionPlanData.getIssueIMO() != null) {
                    sqlite.beginTransaction();
                    //deleteByIssueId(fcmIssuesActionPlanData.getIssueIMO() .getIssueId(),sqlite);
                    insertFCMIssuesModel(fcmIssuesActionPlanData.getIssueIMO(),fcmIssuesActionPlanData.getActionPlanMO());
                    if(fcmIssuesActionPlanData.getActionPlanMO() != null) {
                        insertFCMImprovementPlanModel(fcmIssuesActionPlanData.getActionPlanMO());
                    }
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }
    private void insertFCMIssuesModel(IssueIMO issuesMO, ActionPlanMO improvementPlanMO){

            if(issuesMO != null){
                try {
                        ContentValues values = new ContentValues();
                        values.put(TableNameAndColumnStatement.ISSUE_ID,issuesMO.getIssueId());
                        values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID,issuesMO.getIssueMatrixId());
                        values.put(TableNameAndColumnStatement.ISSUE_TYPE_ID,issuesMO.getIssueTypeId());
                        values.put(TableNameAndColumnStatement.SOURCE_TASK_ID,issuesMO.getSourceTaskId());
                        values.put(TableNameAndColumnStatement.UNIT_ID,issuesMO.getUnitId());
                        values.put(TableNameAndColumnStatement.BARRACK_ID,issuesMO.getBarrackId());
                        values.put(TableNameAndColumnStatement.GUARD_ID,issuesMO.getGuardId());
                        values.put(TableNameAndColumnStatement.ISSUE_CATEGORY_ID,issuesMO.getIssueCategoryId());
                        values.put(TableNameAndColumnStatement.ISSUE_MODE_ID,1);
                        values.put(TableNameAndColumnStatement.ISSUE_NATURE_ID,issuesMO.getIssueNatureId());
                        values.put(TableNameAndColumnStatement.ISSUE_CAUSE_ID,issuesMO.getIssueCauseId());
                        values.put(TableNameAndColumnStatement.ISSUE_SERVERITY_ID,issuesMO.getIssueSeverityId());
                        values.put(TableNameAndColumnStatement.ISSUE_STATUS_ID,issuesMO.getIssueStatus());
                        if(improvementPlanMO != null) {
                            values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanMO.getActionPlanId());
                        }
                        values.put(TableNameAndColumnStatement.CREATED_DATE_TIME,issuesMO.getRaisedDateTime());
                        values.put(TableNameAndColumnStatement.CREATED_BY_ID,issuesMO.getRaisedById());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID,issuesMO.getAssignedToId());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,issuesMO.getAssigneeName());
                        String date = dateTimeFormatConversionUtil.convertDateTimeToDate(issuesMO.getTargetActionDateTime());
                        values.put(TableNameAndColumnStatement.TARGET_ACTION_END_DATE,date);
                        values.put(TableNameAndColumnStatement.CLOSURE_END_DATE,issuesMO.getClosureDateTime());
                        values.put(TableNameAndColumnStatement.REMARKS,issuesMO.getClientRemarks());
                        values.put(TableNameAndColumnStatement.CLOSURE_REMARKS,issuesMO.getClientRemarks());
                        //values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,improvementPlanMO.getCli());
                        values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,issuesMO.getAssigneeName());
                        values.put(TableNameAndColumnStatement.UNIT_CONTACT_NAME,issuesMO.getClientContactName());
                        //values.put(TableNameAndColumnStatement.GUARD_NAME, issuesMO.getG());
                        values.put(TableNameAndColumnStatement.GUARD_CODE, issuesMO.getGuardCode());
                        values.put(TableNameAndColumnStatement.PHONE_NO, issuesMO.getClientPhoneNumber());
                        values.put(TableNameAndColumnStatement.IS_NEW,0);
                        values.put(TableNameAndColumnStatement.IS_SYNCED,1);
                        sqlite.insertOrThrow(TableNameAndColumnStatement.ISSUES_TABLE, null, values);
                }
                catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);
                }

        }
    }

    private void insertFCMImprovementPlanModel(ActionPlanMO improvementPlanIRModel ){

            if (improvementPlanIRModel != null) {
                try {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.ISSUE_MATRIX_ID, improvementPlanIRModel.getIssueMatrixId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID, improvementPlanIRModel.getActionPlanId());
                    values.put(TableNameAndColumnStatement.UNIT_ID, improvementPlanIRModel.getUnitId());
                    values.put(TableNameAndColumnStatement.BARRACK_ID, improvementPlanIRModel.getBarrackId());
                    values.put(TableNameAndColumnStatement.CREATED_DATE_TIME, improvementPlanIRModel.getRaisedDate());
                    values.put(TableNameAndColumnStatement.ISSUE_ID, improvementPlanIRModel.getIssueId());
                    values.put(TableNameAndColumnStatement.ACTION_PLAN_ID, improvementPlanIRModel.getMasterActionPlanId());
                    values.put(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE, improvementPlanIRModel.getExpectedClosureDate());
                    values.put(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID, improvementPlanIRModel.getTargetEmployeeId());
                    values.put(TableNameAndColumnStatement.STATUS_ID, improvementPlanIRModel.getActionPlanStatusId());
                    values.put(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID, improvementPlanIRModel.getImprovementPlanCategoryId());
                    values.put(TableNameAndColumnStatement.REMARKS, improvementPlanIRModel.getRemarks());
                    values.put(TableNameAndColumnStatement.BUDGET,improvementPlanIRModel.getBudget());
                    values.put(TableNameAndColumnStatement.CLOSURE_DATE_TIME, improvementPlanIRModel.getClosureDate());
                    // values.put(TableNameAndColumnStatement.CLOSED_BY_ID,improvementPlanIRModel.get);
                    values.put(TableNameAndColumnStatement.CLOSURE_COMMENT, improvementPlanIRModel.getClosureComment());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_NAME,improvementPlanIRModel.getAssigneeName());
                    values.put(TableNameAndColumnStatement.ASSIGNED_TO_ID, improvementPlanIRModel.getAssignedToId());
                    values.put(TableNameAndColumnStatement.TASK_ID, improvementPlanIRModel.getSourceTaskId());
                    values.put(TableNameAndColumnStatement.IS_NEW,0);
                    values.put(TableNameAndColumnStatement.IS_SYNCED,1);
                    sqlite.insertOrThrow(TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE, null, values);
                }
                catch (Exception exception) {
                    Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
                    Crashlytics.logException(exception);

            }
        }
    }

    private  void deleteByIssueId(int issueId,SQLiteDatabase sqlite){
        //SQLiteDatabase sqlite = this.getWritableDatabase();
        String deleteQuery = "delete from " +TableNameAndColumnStatement.ISSUES_TABLE+ " where "+ TableNameAndColumnStatement.ISSUE_ID + "="+ issueId +"";
        Cursor cursor = null;
        synchronized (sqlite) {
            try {
                cursor = sqlite.rawQuery(deleteQuery, null);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }


    private  void deleteByImprovementPlanId(int improvementPlanId,SQLiteDatabase sqlite){
        //SQLiteDatabase sqlite = this.getWritableDatabase();
        String deleteQuery = "delete from " +TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE+ " where "+ TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + "="+ improvementPlanId +"";
        Cursor cursor = null;

        synchronized (sqlite) {

            try {
                cursor = sqlite.rawQuery(deleteQuery, null);
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                if(null!=sqlite && sqlite.isOpen()){
                    sqlite.close();
                }
            }

        }
    }
}
