package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.SingleUnitBaseOR;
import com.sisindia.ai.android.myunits.models.SingleUnitMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;
import com.sisindia.ai.android.rota.SelectReasonActivity;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.net.HttpURLConnection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SingleUnitActivity extends BaseActivity implements OnViewHistoryClickListener {

    public static final String CHECKING_TYPE = "checkingType";
    @Bind(R.id.single_unit_recyclerview)
    RecyclerView singleUnitRecyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.singleUnitParentLayout)
    RelativeLayout singleUnitParentLayout;
    @Bind(R.id.notDataAvailable)
    CustomFontTextview notDataAvailable;

    private SingleUnitRecyclerAdapter singleUnitRecyclerAdapter;
    private Context mContext;
    private static final String UNIT_ID = "unit_id";
    private MyUnitHomeMO myUnitHomeMO;
    private List<SingleUnitMO> singleUnitList;
    private boolean isBarrackInspection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_unit);
        ButterKnife.bind(this);
        mContext = SingleUnitActivity.this;
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);

        }

        Object object = SingletonUnitDetails.getInstance().getUnitDetails();
        if (object instanceof MyUnitHomeMO) {
            myUnitHomeMO = (MyUnitHomeMO) object;
            getSupportActionBar().setTitle(myUnitHomeMO.getUnitName());
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        singleUnitRecyclerView.setLayoutManager(linearLayoutManager);
        singleUnitRecyclerView.setItemAnimator(new DefaultItemAnimator());
        if(myUnitHomeMO != null) {
            getSingleUnitApiCall();
        }
        else{
            updateRecyclerView();
        }
    }

    private void updateRecyclerView() {
        if(singleUnitRecyclerAdapter == null) {
            singleUnitRecyclerAdapter = new SingleUnitRecyclerAdapter(this);
        }
        if(singleUnitList != null) {
            if(singleUnitList.size() != 0) {
                singleUnitRecyclerAdapter.setSingleUnitDataList(singleUnitList);
                singleUnitRecyclerAdapter.notifyDataSetChanged();
            }
            else
            {
                notDataAvailable.setVisibility(View.VISIBLE);
                singleUnitRecyclerView.setVisibility(View.GONE);
            }
        }
        else {
            notDataAvailable.setVisibility(View.VISIBLE);
            singleUnitRecyclerView.setVisibility(View.GONE);
        }
        singleUnitRecyclerView.setAdapter(singleUnitRecyclerAdapter);
        singleUnitRecyclerAdapter.setOnViewHistoryClickListener(this);
    }

    private void getSingleUnitApiCall() {
        SISClient sisClient = new SISClient(this);
        showprogressbar();
        sisClient.getApi().getSingleUnitDetails(myUnitHomeMO.getUnitId(), new Callback<SingleUnitBaseOR>() {
            @Override
            public void success(SingleUnitBaseOR singleUnitBaseOR, Response response) {
                switch (singleUnitBaseOR.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        singleUnitList = singleUnitBaseOR.getData();
                        hideprogressbar();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateRecyclerView();
                            }
                        });

                        break;
                    default:
                        Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.API_FAIL_MSG));
                }

            }

            @Override
            public void failure(RetrofitError error) {
                hideprogressbar();
                Util.printRetorfitError("SingleUnitActivity", error);
                updateRecyclerView();
                if(NetworkUtil.isConnected)
                    Util.showToast(getApplicationContext(), getApplicationContext().getResources().getString(R.string.no_internet));

            }
        });
    }

    private void showprogressbar() {
        Util.showProgressBar(SingleUnitActivity.this, R.string.loading_please_wait);

    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_unit, menu);
        MenuItem item = menu.findItem(R.id.next);
        if(myUnitHomeMO.getUnitId() > 0 ){
            item.setVisible(true);
        }
        else{
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.next) {

            Intent sitePostsIntent = new Intent(mContext, UnitDetailsActivity.class);
            startActivity(sitePostsIntent);
            return true;
        } else if (id == android.R.id.home) {
            MyUnitsFragment.mUpdateFragmentListner.updateUnitsFragment();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewHistoryClick(View view, int position, int taskTypeId) {
        switch (view.getId()) {
            case R.id.singleunit_cardview_new_check:

                if (appPreferences.getDutyStatus()) {
                    RotaTaskListMO rotaTaskListMO = new RotaTaskListMO();
                    ++position;
//                RotaTaskTypeEnum.valueOf(position).name().toString().trim()
//                checkingTypes.get(position).getTaskType()
                    rotaTaskListMO.setRotaTaskName(RotaTaskTypeEnum.valueOf(taskTypeId).name().replace("_", " "));
                    rotaTaskListMO.setRotaTaskWhiteIcon(taskTypeId);
                    rotaTaskListMO.setRotaTaskBlackIcon(taskTypeId);
                    rotaTaskListMO.setRotaTaskActivityId(taskTypeId);
                    rotaTaskListMO.setUnitName(myUnitHomeMO.getUnitName());
                    rotaTaskListMO.setUnitId(myUnitHomeMO.getUnitId());

                    /*if(taskTypeId==3){

                         isBarrackInspection = true;
                    }*/
                    Intent checkingIntent = new Intent(SingleUnitActivity.this, SelectReasonActivity.class);
                    checkingIntent.putExtra(SingleUnitActivity.CHECKING_TYPE, rotaTaskListMO);
                    checkingIntent.putExtra(getResources().getString(R.string.single_unit_flag), true);
                    //checkingIntent.putExtra("FromMyBarrack",isBarrackInspection);
                    startActivity(checkingIntent);
                } else {
                    snackBarWithMesg(singleUnitParentLayout, getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE_FOR_CREATING_NEW_TASK));
                }
                break;
            case R.id.singleunit_cardview_view_history:
                Intent unitTaskHistoryIntent = new Intent(SingleUnitActivity.this, UnitTaskHistoryActivity.class);
                unitTaskHistoryIntent.putExtra(TableNameAndColumnStatement.TASK_TYPE_ID, taskTypeId);
                startActivity(unitTaskHistoryIntent);

                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyUnitsFragment.mUpdateFragmentListner.updateUnitsFragment();
    }
}
