package com.sisindia.ai.android.issues.grievances;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.issues.models.GrievanceModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Shushrut on 14-04-2016.
 */
public class CustomGrievanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    Context mcontext;
    ArrayList<Object> mData;
    private int mOpenIssues_layout = 0;
    private int mGreviance_layout = 1;
    boolean setVisible = false;
    View mDividerView;
    CustomFontTextview mTextView;
    CustomFontTextview mCreatedin;
    CustomFontTextview mCreatedintv;
    CustomFontTextview mdate;
    CustomFontTextview mdays;

    RelativeLayout improvePlanLayout;
    ImageView mCustomImageView;
    boolean viewDetailsButton;
    boolean mhideDivider;
    boolean mShowImprovePlan;
    LinearLayout mholderView;

    public CustomGrievanceAdapter(Context mcontext, ArrayList<Object> mData, boolean setVisible, boolean viewDetailsButton, boolean hideDivider, boolean showImprovePlan) {
        this.mcontext = mcontext;
        this.mData = mData;
        this.setVisible = setVisible;
        this.viewDetailsButton = viewDetailsButton;
        mhideDivider = hideDivider;
        mShowImprovePlan = showImprovePlan;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == mOpenIssues_layout) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.openissues_layout, parent, false);
            return new OpenIssuesHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.generic_cardview, parent, false);
            mDividerView = view.findViewById(R.id.checking_grievance_complaint_divider_one);
            if(mhideDivider) {

                mDividerView.setVisibility(View.VISIBLE);
            }else{
                mDividerView.setVisibility(View.GONE);
            }

            mTextView = (CustomFontTextview)view.findViewById(R.id.checking_grievance_complaint_view_details_txt);
            mholderView = (LinearLayout)view.findViewById(R.id.checking_grievance_complaint_view_details_image_layout);
            mTextView.setVisibility(View.GONE);
            mholderView.setOnClickListener(this);
            mCustomImageView = (ImageView)view.findViewById(R.id.checking_grievance_complaint_view_details_image);
            if(viewDetailsButton){
                mCustomImageView.setVisibility(View.VISIBLE);
                mTextView.setVisibility(View.VISIBLE);
                mCustomImageView.setImageResource(R.drawable.view_details);
            }else {
                mCustomImageView.setVisibility(View.GONE);
            }
            improvePlanLayout = (RelativeLayout) view.findViewById(R.id.checking_improve_plan_cardview_layout);
            mCreatedintv = (CustomFontTextview) view.findViewById(R.id.checking_improve_plan_created_cardview__type_txt);
            mCreatedin = (CustomFontTextview) view.findViewById(R.id.checking_improve_plan_createdin_cardview__type_txt);
            mdate = (CustomFontTextview) view.findViewById(R.id.issueTargetClosureDate);
            mdays = (CustomFontTextview) view.findViewById(R.id.timeDurationLeft);



            if (mShowImprovePlan){
                improvePlanLayout.setVisibility(View.VISIBLE);
                mCreatedintv.setVisibility(View.VISIBLE);
                mCreatedin.setVisibility(View.VISIBLE);
                mdays.setVisibility(View.INVISIBLE);
                mdate.setVisibility(View.INVISIBLE);


            }else {
                improvePlanLayout.setVisibility(View.GONE);
                mCreatedintv.setVisibility(View.GONE);
                mCreatedin.setVisibility(View.GONE);
                mdate.setVisibility(View.VISIBLE);
                mdays.setVisibility(View.VISIBLE);

            }

            return new GrevianceHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position)== mOpenIssues_layout){

        }else{
            GrevianceHolder mHolder = (GrevianceHolder)holder;
            Object object = null;
            if(position ==1) {
                object  = mData.get((position - 1));
            }else{
                object = mData.get(position);
            }
            mHolder.mguardname_txt.setText(((GrievanceModel)object).getGuardName());
            mHolder.complaint_type_txt.setText(((GrievanceModel)object).getGrevianceNature());
        }
    }

   /* @Override
    public void onBindViewHolder( holder, int position) {
if(getItemViewType(position) == mOpenIssues_layout){

}else{
    holder.m
}

    }*/

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && setVisible) {
            return mOpenIssues_layout;
        } else {
            return mGreviance_layout;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.checking_grievance_complaint_view_details_image_layout:
                Intent viewComplaintDetails = new Intent(mcontext,AddComplaintActivity.class);
                viewComplaintDetails.putExtra("isfromAddIssue",false);
                viewComplaintDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Util.showOptionMenu = false;
                mcontext.startActivity(viewComplaintDetails);
                break;
        }
    }


    private class OpenIssuesHolder extends RecyclerView.ViewHolder {
        public OpenIssuesHolder(View view) {
            super(view);
        }
    }

    private class GrevianceHolder extends RecyclerView.ViewHolder {
        public CustomFontTextview mguardname_txt;
        public CustomFontTextview complaint_type_txt;
        public GrevianceHolder(View view) {
            super(view);
            mguardname_txt = (CustomFontTextview)view.findViewById(R.id.guradName_or_clientName);
            complaint_type_txt = (CustomFontTextview)view.findViewById(R.id.checking_grievance_complaint_cardview_complaint_type_txt);
        }
    }
}

