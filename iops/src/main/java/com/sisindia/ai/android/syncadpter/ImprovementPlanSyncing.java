package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.issues.models.ImprovementPlanOR;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 05-08-2016.
 */

public class ImprovementPlanSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private HashMap<Integer, ImprovementPlanIR> improvementPlanIRHashMap;
    private boolean isAdhocImprovementPlan;
    private  int taskId;

    public ImprovementPlanSyncing(Context mcontext, boolean isAdhocImprovementPlan, int taskId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        this.isAdhocImprovementPlan = isAdhocImprovementPlan;
        this.taskId = taskId;
        ImprovementPlanSyncing();
    }

    private void ImprovementPlanSyncing() {
        improvementPlanIRHashMap = getAllPlansToSync();
        if (improvementPlanIRHashMap != null && improvementPlanIRHashMap.size() != 0) {
            updateImprovementPlanSyncCount(improvementPlanIRHashMap.size());
            for (Integer key : improvementPlanIRHashMap.keySet()) {
                ImprovementPlanIR improvementPlanIR = improvementPlanIRHashMap.get(key);
                improvementApiCall(improvementPlanIR, key);
                Timber.d(Constants.TAG_SYNCADAPTER+"ImprovementPlanSyncing   %s", "key: " + key + " value: " + improvementPlanIRHashMap.get(key));
            }

        } else {
            updateImprovementPlanSyncCount(0);
            Timber.d(Constants.TAG_SYNCADAPTER+"ImprovementPlanSyncing -->nothing to sync ");
        }

    }



    public void updateImprovementPlanSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(taskId == dependentSyncsStatus.taskId){
                    dependentSyncsStatus.isImprovementPlanSynced = count;
                    updateImprovementPlanSyncCount(dependentSyncsStatus);
                }
            }
        }
    }

    private void updateImprovementPlanSyncCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(dependentSyncsStatus.taskId,
                Constants.TASK_RELATED_SYNC
                , TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED,
                dependentSyncsStatus.isImprovementPlanSynced,dependentSyncsStatus.taskTypeId
        );
    }

    private void improvementApiCall(final ImprovementPlanIR improvementPlanIR, final int key) {
        sisClient.getApi().syncImprovementPlan(improvementPlanIR, new Callback<ApiResponse<ImprovementPlanOR>>() {
            @Override
            public void success(ApiResponse<ImprovementPlanOR> improvementPlanORApiResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"ImprovementPlanSyncing response  %s", improvementPlanORApiResponse);
                switch (improvementPlanORApiResponse.statusCode)
                {
                    case HttpURLConnection.HTTP_OK:
                        if(improvementPlanORApiResponse != null){
                            updateImprovementIdTable(improvementPlanORApiResponse.data, key);
                        }

                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                    Util.printRetorfitError("ImprovementPlanSyncing", error);

            }
        });
    }


    private  HashMap<Integer, ImprovementPlanIR> getAllPlansToSync(){
        HashMap<Integer, ImprovementPlanIR> contactInputRequest = new HashMap<>();
        // Select All Query
        String selectQuery = "";
        if(isAdhocImprovementPlan){
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE + " where " +
                    TableNameAndColumnStatement.TASK_ID + "  is null and " +
                    TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + " = " + 0 +" and ("+
                    TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                    TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' ) " ;
        }
        else {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE + " where " +
                    TableNameAndColumnStatement.ISSUE_ID + " is  null and "+
                    TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + " = " + 0 + " and "+
                    TableNameAndColumnStatement.TASK_ID + " =  "+ taskId +"  and ( "+
                    TableNameAndColumnStatement.IS_NEW + " = '" + 1 + "' or " +
                    TableNameAndColumnStatement.IS_SYNCED + " = '" + 0 + "' ) " ;

        }


        Timber.i("ImprovementPlan Sync :"+ selectQuery);

        SQLiteDatabase db=null;
        Cursor cursor = null;
        try {
             db = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ImprovementPlanIR improvementPlanIR = new ImprovementPlanIR();
                        improvementPlanIR.setIssueMatrixId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_MATRIX_ID)));
                        improvementPlanIR.setActionPlanId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID)));
                        improvementPlanIR.setRemarks(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.REMARKS)));
                        improvementPlanIR.setRaisedDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        improvementPlanIR.setMasterActionPlanId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTION_PLAN_ID)));
                        improvementPlanIR.setActionPlanStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.STATUS_ID)));
                        improvementPlanIR.setExpectedClosureDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE)));
                       // improvementPlanIR.setClientCoordinationQuestionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CLIENT_COORDINATION_QUESTION_ID))));
                        improvementPlanIR.setClosureComment(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_COMMENT)));
                        improvementPlanIR.setImprovementPlanCategoryId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID)));
                       // improvementPlanIR.setAssignedToId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNED_TO_ID))));
                        improvementPlanIR.setKitRequirmentId(null); // dont knw the context whats the use or fom where to insert the value
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID))==0){
                        improvementPlanIR.setBarrackId(null);
                        }
                        else{
                            improvementPlanIR.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));
                        }

                        if(TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_DATE_TIME)))){
                            improvementPlanIR.setClosureDate(null);
                        }
                        else{
                            improvementPlanIR.setClosureDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CLOSURE_DATE_TIME)));
                        }

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID))==0){
                            improvementPlanIR.setIssueId(null);
                        }
                        else{
                            improvementPlanIR.setIssueId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID)));
                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID))==0){
                            improvementPlanIR.setTargetEmployeeId(null);
                        }
                        else{
                            improvementPlanIR.setTargetEmployeeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TARGET_EMPLOYEE_ID)));
                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID))==0){
                            improvementPlanIR.setUnitId(null);
                        }
                        else{
                            improvementPlanIR.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID))==0){
                            improvementPlanIR.setTaskId(null);
                        }
                        else{
                            improvementPlanIR.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CLIENT_COORDINATION_QUESTION_ID))==0){
                            improvementPlanIR.setClientCoordinationQuestionId(null);
                        }
                        else{
                            improvementPlanIR.setClientCoordinationQuestionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CLIENT_COORDINATION_QUESTION_ID)));
                        }

                        contactInputRequest.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), improvementPlanIR);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER+"ImprovementPlanSyncing object  %s", IOPSApplication.getGsonInstance().toJson(contactInputRequest));
        }
        return contactInputRequest;
    }

    /**
     *
     * @param improvementPlanOR
     * @param id
     */
    protected void updateImprovementIdTable( ImprovementPlanOR improvementPlanOR, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE +
                " SET " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + "= '" + improvementPlanOR.getActionPlanId() +
                "' ," + TableNameAndColumnStatement.ASSIGNED_TO_NAME + "= '" + improvementPlanOR.getAssigneeName() +
                "' ," + TableNameAndColumnStatement.ASSIGNED_TO_ID + "= '" + improvementPlanOR.getAssignedToId() +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d("updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            if(taskId > 0){
                updateSyncCountAndStatus();
            }

        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!= sqlite &&  sqlite.isOpen()){
                sqlite.close();
            }
        }
    }




    private void updateSyncCountAndStatus() {
        if(taskId > 0){
            int count = dependentSyncDb.getSyncCount(taskId,TableNameAndColumnStatement.TASK_ID,
                    TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED);
            if (count != 0) {
                count = count -1;
            }
            Timber.i("NewSync %s"," ImprovementPlan Syncing count update in ----- "+taskId +"-------- "+count);
            updateImprovementPlanSyncCount(count);
        }
    }
}
