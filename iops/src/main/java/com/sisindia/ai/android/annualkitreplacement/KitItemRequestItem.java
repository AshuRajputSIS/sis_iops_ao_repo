package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

public class KitItemRequestItem {

    @SerializedName("KitItemId")
    private Integer kitItemId;
    @SerializedName("KitItemSizeId")
    private Integer kitItemSizeId;

    /**
     *
     * @return
     * The kitItemId
     */
    public Integer getKitItemId() {
        return kitItemId;
    }

    /**
     *
     * @param kitItemId
     * The KitItemId
     */
    public void setKitItemId(Integer kitItemId) {
        this.kitItemId = kitItemId;
    }

    /**
     *
     * @return
     * The kitItemSizeId
     */
    public Integer getKitItemSizeId() {
        return kitItemSizeId;
    }

    /**
     *
     * @param kitItemSizeId
     * The KitItemSizeId
     */
    public void setKitItemSizeId(Integer kitItemSizeId) {
        this.kitItemSizeId = kitItemSizeId;
    }

}