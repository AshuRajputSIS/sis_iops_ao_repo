package com.sisindia.ai.android.syncadpter;

/**
 * Created by compass on 5/9/2017.
 */

interface SyncUpdateListener {
    void successfulSyncUpdate(int staskId,int whichSync,int syncCount,String col);
}
