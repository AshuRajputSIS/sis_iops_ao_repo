package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.rotaDTO.RotaTempTaskIdUpdateStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.AddRotaTaskInput;
import com.sisindia.ai.android.network.request.RotaTaskInputRequestMO;
import com.sisindia.ai.android.network.response.TaskOutputMO;
import com.sisindia.ai.android.rota.CompletedRotaTaskMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by compass on 4/13/2017.
 */

public class NewRotaTaskSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    //private int count = 0;
    private int completedTasksCount;
    private HashMap<Integer, RotaTaskInputRequestMO> inputList;
    private RotaTempTaskIdUpdateStatementDB rotaTempTaskIdUpdateStatementDB;
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public NewRotaTaskSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mcontext);
        rotaTempTaskIdUpdateStatementDB = new RotaTempTaskIdUpdateStatementDB(mcontext);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        dependentFailureSync(0);
        Timber.d(Constants.TAG_SYNCADAPTER + "rotaTask failure dependent Syncs");
        taskSyncing();
    }

    private void taskSyncing() {
        CompletedRotaTaskMo completedRotaTaskMo = getAllTaskToSync();
        inputList = completedRotaTaskMo.getUnSyncedCompletedTasksMap();
        completedTasksCount = completedRotaTaskMo.getUnSyncedCompletedTaks();

        if (null != inputList && 0 != inputList.size()) {
            for (Integer key : inputList.keySet()) {
                RotaTaskInputRequestMO taskInputIMO = inputList.get(key);
                if (key < 0) {
                    if (taskInputIMO.getTaskStatusId() == 4) {
                        updateTaskApicall(taskInputIMO, key);
                    } else {
                        if (taskInputIMO.getTaskStatusId() == 1) {
                            final AddRotaTaskInput inputIMO = addTaskModel(taskInputIMO);
                            addTaskApiCall(inputIMO, key);
                        }
                        dependentFailureSync(taskInputIMO.getId());
                    }
                } else {
                    if (taskInputIMO.getTaskStatusId() == 4) {
                        updateTaskApicall(taskInputIMO, key);
                    }
                   /* else {
                        if(taskInputIMO.getTaskStatusId() == 3){
                            dependentFailureSync(key);
                        }
                    }*/
                }
                Timber.d(Constants.TAG_SYNCADAPTER + "taskSyncing   %s", "key: " + key + " value: " + inputList.get(key));
            }
        }
    }

    private void updateSyncStatus(int localTaskId) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.IS_NEW + " = 0 " +
                " ," + TableNameAndColumnStatement.IS_SYNCED + " = 1 " +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + " = " + localTaskId;
        Timber.d(Constants.TAG_SYNCADAPTER + "UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    /**
     * fecthing the task details from the database
     *
     * @return
     */
    private CompletedRotaTaskMo getAllTaskToSync() {
        HashMap<Integer, RotaTaskInputRequestMO> rotaTaskList = new HashMap<>();
        CompletedRotaTaskMo completedRotaTaskMo = new CompletedRotaTaskMo();
        int unsyncedCompletedCount = 0;
        String taskQuery = "";
        taskQuery = " select * from task where is_synced = 0 ";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(taskQuery, null);
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        RotaTaskInputRequestMO rotaTaskMO = new RotaTaskInputRequestMO();
                        rotaTaskMO.setIsTaskStarted(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_TASK_STARTED)));
                        rotaTaskMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        int unitId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID));
                        if (unitId == 0)
                            rotaTaskMO.setUnitId(null);
                        else
                            rotaTaskMO.setUnitId(unitId);

                        rotaTaskMO.setTaskTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID)));
                        rotaTaskMO.setAssigneeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setEstimatedExecutionTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME)));
                        rotaTaskMO.setActualExecutionTime(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME)));
                        rotaTaskMO.setTaskStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)));
                        rotaTaskMO.setDescription(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DESCRIPTIONS)));
                        rotaTaskMO.setEstimatedTaskExecutionStartDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setEstimatedTaskExecutionEndDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionStartDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME)));
                        rotaTaskMO.setActualTaskExecutionEndDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME)));

                        int barrackId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID));
                        if (barrackId != 0) {
                            rotaTaskMO.setBarrackId(barrackId);
                        } else {
                            rotaTaskMO.setBarrackId(null);
                        }
                        rotaTaskMO.setIsAutoCreation(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_AUTO)));
                        rotaTaskMO.setExecutorId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ASSIGNEE_ID)));
                        rotaTaskMO.setCreatedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        rotaTaskMO.setCreatedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_BY_ID)));
                        String taskExecutionResult = cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_EXECUTION_RESULT));
                        Timber.d(Constants.TAG_SYNCADAPTER + "taskExecutionResult jsonObj  %s", taskExecutionResult);
                        if (null != taskExecutionResult && taskExecutionResult.length() != 0) {
                            if (taskExecutionResult.equalsIgnoreCase("null")) {
                                Timber.d(Constants.TAG_SYNCADAPTER + "rotaTaskSyncing jsonObj  null %s", "null dont set");
                            } else {
                                JsonObject jsonObject = (new JsonParser()).parse(taskExecutionResult).getAsJsonObject();
                                rotaTaskMO.setTaskExecutionResult(jsonObject);
                                Timber.d(Constants.TAG_SYNCADAPTER + "rotaTaskSyncing jsonObj  %s", jsonObject.toString());
                            }
                        }
                        rotaTaskMO.setRelatedTaskId(0);
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)) == 0) {
                            rotaTaskMO.setChangeRouteReasonId(null);
                        } else {
                            rotaTaskMO.setChangeRouteReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SELECT_REASON_ID)));
                        }

                        rotaTaskMO.setUnitPostId(null);
                        rotaTaskMO.setAdditionalComments("");

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)) == 0)
                            rotaTaskMO.setApprovedById(null);
                        else
                            rotaTaskMO.setApprovedById(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_APPROVAL_REQUIRED)));

                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)) == 0)
                            rotaTaskMO.setOtherReasonId(null);
                        else
                            rotaTaskMO.setOtherReasonId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.OTHER_TASK_REASON_ID)));

                        if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION))))
                            rotaTaskMO.setTaskLocation(null);
                        else
                            rotaTaskMO.setTaskLocation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_SUBMITED_LOCATION)));

                        rotaTaskList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)), rotaTaskMO);
                        if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID)) == 4) {
                            unsyncedCompletedCount++;
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER + "rotaTaskSyncing object  %s", new Gson().toJson(rotaTaskList));
        }
        completedRotaTaskMo.setUnSyncedCompletedTaks(unsyncedCompletedCount);
        completedRotaTaskMo.setUnSyncedCompletedTasksMap(rotaTaskList);
        return completedRotaTaskMo;
    }

    /**
     * if the task id =0 then we to set the input request without task execution paramater
     *
     * @param taskInputIMO
     * @return
     */
    private AddRotaTaskInput addTaskModel(RotaTaskInputRequestMO taskInputIMO) {
        AddRotaTaskInput addTaskInputIMO = new AddRotaTaskInput();
        addTaskInputIMO.setId(taskInputIMO.getId());
        if (taskInputIMO.getUnitId() == null) {
            addTaskInputIMO.setUnitId(null);
        } else {
            addTaskInputIMO.setUnitId(taskInputIMO.getUnitId());
        }

        addTaskInputIMO.setTaskTypeId(taskInputIMO.getTaskTypeId());
        addTaskInputIMO.setAssigneeId(taskInputIMO.getAssigneeId());
        addTaskInputIMO.setEstimatedExecutionTime(taskInputIMO.getEstimatedExecutionTime());
        addTaskInputIMO.setActualExecutionTime(taskInputIMO.getActualExecutionTime());
        addTaskInputIMO.setTaskStatusId(taskInputIMO.getTaskStatusId());
        addTaskInputIMO.setDescription(taskInputIMO.getDescription());
        addTaskInputIMO.setEstimatedTaskExecutionStartDateTime(taskInputIMO.getEstimatedTaskExecutionStartDateTime());
        addTaskInputIMO.setEstimatedTaskExecutionEndDateTime(taskInputIMO.getEstimatedTaskExecutionEndDateTime());
        addTaskInputIMO.setBarrackId(taskInputIMO.getBarrackId());
        addTaskInputIMO.setIsAutoCreation(taskInputIMO.getIsAutoCreation());
        addTaskInputIMO.setExecutorId(taskInputIMO.getExecutorId());
        addTaskInputIMO.setCreatedDateTime(taskInputIMO.getCreatedDateTime());
        addTaskInputIMO.setCreatedById(taskInputIMO.getCreatedById());
        addTaskInputIMO.setRelatedTaskId(taskInputIMO.getRelatedTaskId());
        addTaskInputIMO.setUnitPostId(taskInputIMO.getUnitPostId());
        addTaskInputIMO.setAdditionalComments(taskInputIMO.getAdditionalComments());
        addTaskInputIMO.setApprovedById(taskInputIMO.getApprovedById());
        addTaskInputIMO.setAssignedById(taskInputIMO.getAssignedById());
        addTaskInputIMO.setChangeRouteReasonId(taskInputIMO.getChangeRouteReasonId());
        addTaskInputIMO.setOtherReasonId(taskInputIMO.getOtherReasonId());
        return addTaskInputIMO;
    }

    /**
     * Update the task id in the taskTable
     *
     * @param taskId
     * @param id
     */
    public void updateTaskid(int taskId, int id, int taskStatusD, int isnew, int isSync) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.TASK_TABLE +
                " SET " + TableNameAndColumnStatement.TASK_ID + "= '" + taskId +
                "' ," + TableNameAndColumnStatement.TASK_STATUS_ID + "= " + taskStatusD +
                " ," + TableNameAndColumnStatement.IS_NEW + "= " + isnew +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + isSync +
                " WHERE " + TableNameAndColumnStatement.TASK_ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER + "UpdateTaskidTaskTable updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    /**
     * syncing the  ADHOC ROTA task to server
     *
     * @param inputIMO
     * @param key
     */
    private void addTaskApiCall(final AddRotaTaskInput inputIMO, final int key) {
        sisClient.getApi().syncAddUnitTask(inputIMO, new Callback<TaskOutputMO>() {
            @Override
            public void success(TaskOutputMO taskOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "rotaTaskSyncing response  %s", taskOutputMO);
                switch (taskOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        if (taskOutputMO.getData() != null && taskOutputMO.getData().getTaskId() != null) {
                            if (taskOutputMO.getData().getTaskId() != 0) {
                                updateTaskid(taskOutputMO.getData().getTaskId(), key, 1, 1, 0);
                            }
                        }
                        break;
                }

            }

            @Override
            public void failure(RetrofitError error) {
                //successiveSyncs();
                Crashlytics.logException(error);
            }
        });
    }


    private void updateTaskApicall(final RotaTaskInputRequestMO inputIMO, final int key) {
        Gson gson = new Gson();

        String request = gson.toJson(inputIMO);
        Timber.d(Constants.TAG_SYNCADAPTER + "RotaTaskSync :" + request.toString());

        sisClient.getApi().syncUnitTask(inputIMO, new Callback<TaskOutputMO>() {
            @Override
            public void success(TaskOutputMO taskOutputMO, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "rotaTaskSyncing response  %s", taskOutputMO);
                switch (taskOutputMO.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:

                        rotaTempTaskIdUpdateStatementDB.updateTables(taskOutputMO.getData().getTaskId(), key, TableNameAndColumnStatement.TASK_TABLE,
                                TableNameAndColumnStatement.TASK_ID);
                        if (key < 0) {
                            updateTaskRelatedTables(taskOutputMO.getData().getTaskId(), key);
                        }
                        //createJsonMap(inputIMO,taskOutputMO);
                        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
                        dependentSyncsStatus.isUnitStrengthSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isIssuesSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isImprovementPlanSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isKitRequestSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isMetaDataSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isImagesSynced = Constants.NEG_DEFAULT_COUNT;
                        dependentSyncsStatus.isUnitEquipmentSynced = Constants.NEG_DEFAULT_COUNT;

                        dependentSyncsStatus.taskId = taskOutputMO.getData().getTaskId();
                        dependentSyncsStatus.taskTypeId = inputIMO.getTaskTypeId();
                        dependentSyncsStatus.taskStatusId = 4;
                        insertDependentSyncData(dependentSyncsStatus);
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                // successiveSyncs();
                Crashlytics.logException(error);
            }
        });

    }

    private void insertDependentSyncData(DependentSyncsStatus dependentSyncsStatus) {
        if (!dependentSyncDb.isTaskAlreadyExist(dependentSyncsStatus.taskId, TableNameAndColumnStatement.TASK_ID, true)) {
            dependentSyncDb.insertDependentTaskSyncDetails(dependentSyncsStatus, 1);
            successiveSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.taskId);
        }
    }


    public void updateTaskRelatedTables(int serverTaskId, int localTaskId) {
        RotaTempTaskIdUpdateStatementDB rotaTempTaskIdUpdateStatementDB
                = new RotaTempTaskIdUpdateStatementDB(mContext);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE,
                TableNameAndColumnStatement.TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.ISSUES_TABLE,
                TableNameAndColumnStatement.SOURCE_TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE,
                TableNameAndColumnStatement.SOURCE_TASK_ID);
        rotaTempTaskIdUpdateStatementDB.sdCardFileNameUpdate(serverTaskId, localTaskId);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId,
                TableNameAndColumnStatement.ACTIVITY_LOG_TABLE,
                TableNameAndColumnStatement.TASK_ID);
        rotaTempTaskIdUpdateStatementDB.updateTables(serverTaskId, localTaskId, TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE,
                TableNameAndColumnStatement.TASK_ID);
    }

    private void successiveSyncs(Integer taskTypeId, int taskId) {
        dependentFailureSync(taskId);
    }

    public void dependentFailureSync(int taskId) {
        if (taskId == 0) {
            List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatus();
            if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
                for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                    if (dependentSyncsStatus.taskId != 0) {
                        dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.taskId, dependentSyncsStatus);
                    }
                }
            }
        } else {
            List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatusWithTaskId(taskId);
            if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
                for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                    if (dependentSyncsStatus.taskId != 0) {
                        dependentSyncs(dependentSyncsStatus.taskTypeId,
                                dependentSyncsStatus.taskId,
                                dependentSyncsStatus);
                    }
                }
            }
        }
    }

    private void dependentSyncs(int taskTypeId, int taskId, DependentSyncsStatus dependentSyncsStatus) {
        if (taskTypeId == 1 || taskTypeId == 2) {
            Timber.i("NewSync %s", "DAY/NIGHT CHECK ----- Failure sync due to network failure " + taskId);
            if (dependentSyncsStatus != null) {
                if (dependentSyncsStatus.isUnitStrengthSynced != Constants.DEFAULT_SYNC_COUNT) {
                    new UnitStrengthSyncing(mContext, taskId, taskTypeId);
                }
                if (dependentSyncsStatus.isIssuesSynced != Constants.DEFAULT_SYNC_COUNT) {
                    new IssuesSyncing(mContext, false, taskId);
                }
                if (dependentSyncsStatus.isKitRequestSynced != Constants.DEFAULT_SYNC_COUNT) {
                    new KitRequestSyncing(mContext, taskId);
                }
                if (dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
                    syncMetaData(dependentSyncsStatus, taskId);
                }
            }
        } else if (taskTypeId == 3) {
            Timber.i("NewSync %s", "BARRACK INSPECTION ----- Failure sync due to network failure " + taskId);
            if (dependentSyncsStatus.isUnitStrengthSynced != Constants.DEFAULT_SYNC_COUNT) {
                new UnitStrengthSyncing(mContext, taskId, taskTypeId);
            }
            if (dependentSyncsStatus.isImprovementPlanSynced != Constants.DEFAULT_SYNC_COUNT) {
                new ImprovementPlanSyncing(mContext, false, taskId);
            }
            if (dependentSyncsStatus.isIssuesSynced != Constants.DEFAULT_SYNC_COUNT) {
                new IssuesSyncing(mContext, false, taskId);
            }
            if (dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
                syncMetaData(dependentSyncsStatus, taskId);
            }
        } else if (taskTypeId == 4) {
            Timber.i("NewSync %s", "CLIENT COORDINATION ----- Failure sync due to network failure " + taskId);
            if (dependentSyncsStatus.isImprovementPlanSynced != Constants.DEFAULT_SYNC_COUNT) {
                new ImprovementPlanSyncing(mContext, false, taskId);
            }
            if (dependentSyncsStatus.isIssuesSynced != Constants.DEFAULT_SYNC_COUNT) {
                new IssuesSyncing(mContext, false, taskId);
            }
            syncMetaData(dependentSyncsStatus, taskId);
        } else if (taskTypeId == 5 || taskTypeId == 6 || taskTypeId == 7) {
            Timber.i("NewSync %s", "BIll sub/coll/others ----- Failure sync due to network failure " + taskId);
            syncMetaData(dependentSyncsStatus, taskId);
        }
    }

    private void syncMetaData(DependentSyncsStatus dependentSyncsStatus, int taskId) {

        if (dependentSyncsStatus.isMetaDataSynced != Constants.DEFAULT_SYNC_COUNT) {
            MetaDataSyncing metaDataSyncing = new MetaDataSyncing(mContext);
            metaDataSyncing.setTaskId(taskId);
        } else {
            if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setTaskId(taskId);
            }
        }
    }
}


