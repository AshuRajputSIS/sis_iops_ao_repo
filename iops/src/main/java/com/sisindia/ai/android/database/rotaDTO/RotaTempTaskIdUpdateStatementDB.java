package com.sisindia.ai.android.database.rotaDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaStartTaskEnum;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by compass on 4/13/2017.
 */

public class RotaTempTaskIdUpdateStatementDB extends SISAITrackingDB {
    private Util util;
    private Context context;

    public RotaTempTaskIdUpdateStatementDB(Context context) {
        super(context);
        util = new Util();
        this.context = context;
    }

    public void sdCardFileNameUpdate(int taskId, int tempId) {
        List<AttachmentMetaDataModel> tempFileNames = getAttachmentNames(tempId);
        List<String> updatedFileNames = new ArrayList<>();
        List<String> updatedFilePaths = new ArrayList<>();
        for (AttachmentMetaDataModel attachmentMetaDataModel : tempFileNames) {
            String updatedFileName = attachmentMetaDataModel.getAttachmentFileName().replace(String.valueOf(tempId), String.valueOf(taskId));
            String updatedFilePath = attachmentMetaDataModel.getAttachmentPath().replace(attachmentMetaDataModel.getAttachmentFileName(), updatedFileName);
            updatedFileNames.add(updatedFileName);
            updatedFilePaths.add(updatedFilePath);
            util.changeImageNames(context, attachmentMetaDataModel.getAttachmentFileName(), updatedFilePath, attachmentMetaDataModel.getAttachmentTypeId());
            updateAttachmentTable(taskId, tempId, updatedFileName, updatedFilePath, attachmentMetaDataModel.getAttachmentId());
        }
    }

    public void updateAttachmentTable(int taskId, int tempId, String actualFileName, String actualFilePath, Integer attachmentLocalId) {

        SQLiteDatabase sqlite = null;
        String updateDquery = " update " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " set " + TableNameAndColumnStatement.TASK_ID + " = " + taskId + "," +
                TableNameAndColumnStatement.FILE_NAME + " = '" + actualFileName + "'" + ", " +
                TableNameAndColumnStatement.FILE_PATH + " = '" + actualFilePath + "'" +
                " where " + TableNameAndColumnStatement.TASK_ID + " = " + tempId +
                " and " + TableNameAndColumnStatement.ID + " = " + attachmentLocalId;
        Cursor cursor = null;
        sqlite = getWritableDatabase();
        Timber.i("Update attachment table %s", updateDquery);
        try {
            cursor = sqlite.rawQuery(updateDquery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        updatePostidMetadataTable(TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE, taskId
                , taskId, tempId, Util.getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE,
                        context));
    }

    public void updatePostidMetadataTable(String Tablename, int taskId, int attachmentReferenceId,
                                          int tempTaskId, int attachmentRefreenceType) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + Tablename +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + attachmentReferenceId + "' " +
                " WHERE " + TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + "= '" + tempTaskId + "' and " +
                TableNameAndColumnStatement.TASK_ID + "= '" + taskId + "' ";
        ;
        Timber.d("UpdatePostidMetadataTable updateQuery %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public List<AttachmentMetaDataModel> getAttachmentNames(int tempTaskId) {

        List<AttachmentMetaDataModel> attachmentMetaData = new ArrayList<>();

        String query = " select " + TableNameAndColumnStatement.FILE_NAME + "," +
                TableNameAndColumnStatement.ID + ", " +
                TableNameAndColumnStatement.FILE_PATH + ", " +
                TableNameAndColumnStatement.ATTACHMENT_TYPE_ID +
                " from " +
                TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " where " + TableNameAndColumnStatement.TASK_ID + " = " + tempTaskId;
        Timber.i("AttachmentName query %s", query);

        Cursor cursor;

        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        AttachmentMetaDataModel attachmentMetaDataModel = new
                                AttachmentMetaDataModel();
                        attachmentMetaDataModel.setAttachmentFileName(cursor
                                .getString(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.FILE_NAME)));
                        attachmentMetaDataModel.setAttachmentId(cursor
                                .getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.ID)));
                        attachmentMetaDataModel.setAttachmentPath(cursor
                                .getString(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.FILE_PATH)));
                        attachmentMetaDataModel.setAttachmentTypeId(cursor
                                .getInt(cursor.getColumnIndex(
                                        TableNameAndColumnStatement.ATTACHMENT_TYPE_ID)));
                        attachmentMetaData.add(attachmentMetaDataModel);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return attachmentMetaData;
    }

    public void updateTables(int taskId, int tempTaskId, String tableName, String columnName) {
        SQLiteDatabase sqlite = null;
        String updatedQuery = "";
        if (TableNameAndColumnStatement.TASK_TABLE.equalsIgnoreCase(tableName)) {
            updatedQuery = " update " + tableName +
                    " set " + TableNameAndColumnStatement.TASK_ID + "= '" + taskId +
                    "' ," + TableNameAndColumnStatement.TASK_STATUS_ID + "= " +
                    RotaTaskStatusEnum.InActive.getValue() +
                    ", " + TableNameAndColumnStatement.IS_TASK_STARTED + " = " +
                    RotaStartTaskEnum.InActive.getValue() +
                    ", " + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                    ", " + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 +
                    " where " + TableNameAndColumnStatement.TASK_ID + " = " + tempTaskId;
        } else {
            updatedQuery = " update " + tableName +
                    " set " + columnName + " = " + taskId +
                    " where " + columnName + " = " + tempTaskId;
        }
        Cursor cursor = null;
        Timber.i("Task query %s", updatedQuery);

        sqlite = getWritableDatabase();
        try {
            cursor = sqlite.rawQuery(updatedQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
        } catch (Exception exception) {
            exception.getCause();
        }
    }


}
