package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Shushrut on 21-06-2016.
 */
public interface OnStepperUpdateListener {
    void updateStepper(String activityName,int position);
    void downGradeStepper(String activityName,int position);

}
