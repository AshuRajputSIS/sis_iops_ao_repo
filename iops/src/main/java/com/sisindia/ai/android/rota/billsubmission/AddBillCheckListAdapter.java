package com.sisindia.ai.android.rota.billsubmission;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.CheckBox;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionCheckListItemMO;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;


/**
 * Created by Shruti Garg.
 */
public class AddBillCheckListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<BillSubmissionCheckListItemMO> checkListItemMOList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    Set<String> checkList;


    public AddBillCheckListAdapter(Context context) {
        this.context = context;
        checkListItemMOList = new ArrayList<>();
        checkList = new TreeSet<>();
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setCheckListData(ArrayList<BillSubmissionCheckListItemMO> checkListItemMOList) {
        this.checkListItemMOList = checkListItemMOList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;

        View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_addchecklist_row, parent, false);
        recycleViewHolder = new CheckListViewHolder(itemView);


        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        CheckListViewHolder checkListViewHolder = (CheckListViewHolder) holder;

        checkListViewHolder.checkingTextview.setText(checkListItemMOList.get(position).getCheckListItem());
        checkListViewHolder.CheckBox.setChecked(checkListItemMOList.get(position).isChecked());
        checkListViewHolder.CheckBox.setTag(checkListItemMOList.get(position));
  //      checkListViewHolder.CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                String value =checkListItemMOList.get(position).getCheckListItem()+" : "+ isChecked;
//                checkList.add(value);
//            }
//        });
        checkListViewHolder.CheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb=(CheckBox)v;
                BillSubmissionCheckListItemMO billSubmissionCheckListItemMO= (BillSubmissionCheckListItemMO) cb.getTag();
                billSubmissionCheckListItemMO.setChecked(cb.isChecked());
                checkListItemMOList.get(position).setChecked(cb.isChecked());


            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return  checkListItemMOList.size();
    }

    private Object getObject(int position) {
        return checkListItemMOList.get(position);
    }

    public ArrayList<BillSubmissionCheckListItemMO> getUpdatedList(){
        return checkListItemMOList;
    }

    public class CheckListViewHolder extends RecyclerView.ViewHolder {
        TextView checkingTextview;
        CheckBox CheckBox;

        public CheckListViewHolder(View view) {
            super(view);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkListItem);
            this.CheckBox = (CheckBox) view.findViewById(R.id.checkBox);
        }

    }

}
