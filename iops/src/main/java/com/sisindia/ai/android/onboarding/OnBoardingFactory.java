package com.sisindia.ai.android.onboarding;

import android.content.Intent;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.home.LanguageActivity;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.issues.grievances.AddIssuesGrievanceActivity;
import com.sisindia.ai.android.issues.improvements.AddImprovementPlanActivity;
import com.sisindia.ai.android.login.SendOTPActivity;
import com.sisindia.ai.android.myperformance.GenerateConveyanceReport;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.rota.SelectReasonActivity;
import com.sisindia.ai.android.rota.billcollection.BillCollectionActivity;
import com.sisindia.ai.android.rota.billsubmission.BillSubmissionActivity;
import com.sisindia.ai.android.rota.daycheck.LastInspectionReviewActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.settings.Settings;

import java.util.Arrays;

/**
 * Created by shivam on 23/8/16.
 */

public class OnBoardingFactory {

    // Key constants
    public static final String MODULE_NAME_KEY = "module_name";
    public static final String NO_OF_IMAGES = "no_of_images";
    private static final String ROTA_MODEL = "RotaModel";
    public static final String FROM_ONBOADING = "from_onboarding";

    private static final String MODULE_TITLE_POSTFIX = "_title";
    private static final String MODULE_DESC_POSTFIX = "_desc";

    /**
     * Name the modules constants here. name the images like that only
     * e.g if the module name is Client coordination name it as
     * client_coordination
     * and the images you want to show in onboarding as
     * module_name(number) --> client_coordination1 --> client_coordination2 etc
     * <p>
     * put your images in drawable-nodpi
     * NOTE : Don't put it in any other drawable folder
     */

    public static final String LANGUAGE_SELECTION = "language_selection";
    public static final String REGISTRATION = "registration";
    public static final String POST_LOGIN = "post_login";
    public static final String CLIENT_COORDINATION = "client_coordination";
    public static final String DAY_CHECK = "day_check";
    public static final String NIGHT_CHECK = "night_check";
    public static final String BARRACK_INSPECTION = "barrack_inspection";
    public static final String BILL_SUBMISSION = "bill_submission";
    public static final String BILL_COLLECTION = "bill_collection";
    public static final String SETTINGS = "settings";
    public static final String ISSUES = "issues";
    public static final String UNITAT_RISK = "unitat_risk";
    public static final String MY_UNIT = "my_unit";
    public static final String MY_BARRACK = "my_barrack";
    public static final String MY_PERFORMANCE = "my_performance";
    public static final String RECRUITMENT = "recuritment";
    public static final String ANNUAL_KIT_REPLACMENT = "annual_kit_replacement";
    public static final String IMPROVEMENT_PLAN = "improvement_plan";
    public static final String ADD_TASK = "add_task";
    public static final String CONVEYANCE_REPORT = "conveyance_report";
    public static final String GRIEVANCE = "grievance";
    public static final String COMPLINT = "complaint";


    private static Intent getDefaultIntent(int posititon, boolean fromOnBoarding) {
        Intent homeIntent = new Intent(IOPSApplication.getInstance(), HomeActivity.class);
        homeIntent.putExtra(HomeActivity.FRAGMENT_TO_LOAD, posititon);
        homeIntent.putExtra(FROM_ONBOADING, fromOnBoarding);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return homeIntent;
    }


    /**
     * @param moduleName --> provide your module name in the format of module_name
     *                   Define a constant above like for Client Coordination use
     *                   client_coordination
     *                   <p>
     *                   NOTE : Add the extras you want to pass with intent
     * @return --> An intent of that Module
     */
    public static Intent getIntent(final String moduleName) {
        Intent intent = null;
        switch (moduleName) {
            case LANGUAGE_SELECTION:
                intent = getModuleIntent(LanguageActivity.class);
                break;
            case REGISTRATION:
                intent = getModuleIntent(SendOTPActivity.class);
                break;
            case POST_LOGIN:
                intent = getDefaultIntent(HomeActivity.ROTA_FRAGMENT, false);
                break;
            case CLIENT_COORDINATION:
                intent = getModuleIntent(LastInspectionReviewActivity.class);
                break;
            case DAY_CHECK:
                intent = getModuleIntent(LastInspectionReviewActivity.class);
                break;
            case NIGHT_CHECK:
                intent = getModuleIntent(LastInspectionReviewActivity.class);
                break;
            case BARRACK_INSPECTION:
                intent = getModuleIntent(LastInspectionReviewActivity.class);
                break;
            case BILL_SUBMISSION:
                intent = getRotaIntent(BillSubmissionActivity.class);
                break;
            case BILL_COLLECTION:
                intent = getRotaIntent(BillCollectionActivity.class);
                break;
            case ADD_TASK:
                intent = getModuleIntent(SelectReasonActivity.class);
                break;
            case CONVEYANCE_REPORT:
//                intent = getModuleIntent(ConveyanceReportActivity.class);
                intent = getModuleIntent(GenerateConveyanceReport.class);
                break;

            case SETTINGS:
                intent = getModuleIntent(Settings.class);
                break;
            case ISSUES:
                intent = getDefaultIntent(HomeActivity.ISSUE_FRAGMENT, true);
                break;
            case UNITAT_RISK:
                intent = getDefaultIntent(HomeActivity.UNIT_AT_RISK_FRAGMENT, true);
                break;
            case MY_UNIT:
                intent = getDefaultIntent(HomeActivity.MY_UNIT_FRAGMENT, true);
                break;
            case MY_BARRACK:
                intent = getDefaultIntent(HomeActivity.MY_BARRACK_FRAGMENT, true);
                break;
            case MY_PERFORMANCE:
                intent = getDefaultIntent(HomeActivity.PERFORMANCE_FRAGMENT, true);
                break;
            case RECRUITMENT:
                intent = getDefaultIntent(HomeActivity.RECRUITMENT_FRAGMENT, true);
                break;
            case ANNUAL_KIT_REPLACMENT:
                intent = getDefaultIntent(HomeActivity.ANNUAL_KIT_REPLACEMENT_FRAGMENT, true);
                break;
            case IMPROVEMENT_PLAN:
                intent = getModuleIntent(AddImprovementPlanActivity.class);
                break;
            case GRIEVANCE:
                intent = getIssuesIntent(AddIssuesGrievanceActivity.class, TableNameAndColumnStatement.ISSUES_FRAGMENT, true);
                break;
            case COMPLINT:
                intent = getIssuesIntent(AddComplaintActivity.class, TableNameAndColumnStatement.CHECKING_TYPE_DAY_CHECKING, false);
                break;
            // add your cases here
            default:
                intent = getDefaultIntent(HomeActivity.ROTA_FRAGMENT, false);
                break;
        }
        return intent;
    }

    // Use this method if you want to pass just RotaTaskModel in extras
    private static Intent getRotaIntent(final Class clazz) {
        Intent intent = getModuleIntent(clazz);
        RotaTaskModel rotaModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        intent.putExtra(ROTA_MODEL, rotaModel);
        return intent;
    }

    private static Intent getIssuesIntent(final Class clazz, String key, boolean value) {
        Intent intent = getModuleIntent(clazz);
        intent.putExtra(key, value);
        return intent;
    }

    /**
     * @param clazz --> Provide the class name of which you want to create an intent
     * @return --> the Intent of desired class
     */
    private static Intent getModuleIntent(final Class clazz) {
        return new Intent(IOPSApplication.getInstance(), clazz);
    }

    /**
     * @param moduleName
     * @param nOfImages
     * @return A list of drawables (int) based on hindi or english
     */

    static OnboardingModel getOnboardingModel(final String moduleName, final int nOfImages) {
        OnboardingModel onboardingModel = new OnboardingModel();
        addImagesToModel(moduleName, nOfImages, onboardingModel);
        addTitlesToModel(moduleName, onboardingModel);
        addDescriptionToModel(moduleName, onboardingModel);
        return onboardingModel;
    }

    private static void addImagesToModel(String moduleName, int nOfImages, OnboardingModel onboardingModel) {
        for (int i = 0; i < nOfImages; i++) {
            onboardingModel.imagesList.add(IOPSApplication.getInstance().getResources()
                    .getIdentifier(moduleName + (i + 1), "drawable",
                            IOPSApplication.getInstance().getPackageName()));
        }
    }

    private static void addTitlesToModel(String moduleName, OnboardingModel onboardingModel) {
        String[] titles = IOPSApplication.getInstance().getResources()
                .getStringArray(IOPSApplication.getInstance().getResources()
                        .getIdentifier(moduleName + MODULE_TITLE_POSTFIX,
                                "array", IOPSApplication.getInstance().getPackageName()));

        onboardingModel.titlesList = (Arrays.asList(titles));
    }

    private static void addDescriptionToModel(String moduleName, OnboardingModel onboardingModel) {
        String[] desc = IOPSApplication.getInstance().getResources()
                .getStringArray(IOPSApplication.getInstance().getResources()
                        .getIdentifier(moduleName + MODULE_DESC_POSTFIX,
                                "array", IOPSApplication.getInstance().getPackageName()));
        onboardingModel.descList = Arrays.asList(desc);
    }
}