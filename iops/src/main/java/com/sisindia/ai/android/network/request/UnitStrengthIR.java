package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitStrengthIR {

    @SerializedName("UnitId")
    @Expose
    private Integer unitId;
    @SerializedName("ActualStrength")
    @Expose
    private Integer actualStrength;
    @SerializedName("CheckedDateTime")
    @Expose
    private String checkedDateTime;
    @SerializedName("LeaveCount")
    @Expose
    private Integer leaveCount;
    @SerializedName("BarrackId")
    @Expose
    private Integer barrackId;
    @SerializedName("RankId")
    @Expose
    private Integer rankId;
    @SerializedName("UnitTaskId")
    @Expose
    private Integer unitTaskId;

    /**
     * @return The unitId
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * @param unitId The UnitId
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * @return The actualStrength
     */
    public Integer getActualStrength() {
        return actualStrength;
    }

    /**
     * @param actualStrength The ActualStrength
     */
    public void setActualStrength(Integer actualStrength) {
        this.actualStrength = actualStrength;
    }

    /**
     * @return The checkedDateTime
     */
    public String getCheckedDateTime() {
        return checkedDateTime;
    }

    /**
     * @param checkedDateTime The CheckedDateTime
     */
    public void setCheckedDateTime(String checkedDateTime) {
        this.checkedDateTime = checkedDateTime;
    }

    /**
     * @return The leaveCount
     */
    public Integer getLeaveCount() {
        return leaveCount;
    }

    /**
     * @param leaveCount The LeaveCount
     */
    public void setLeaveCount(Integer leaveCount) {
        this.leaveCount = leaveCount;
    }

    /**
     * @return The barrackId
     */
    public Integer getBarrackId() {
        return barrackId;
    }

    /**
     * @param barrackId The BarrackId
     */
    public void setBarrackId(Integer barrackId) {
        this.barrackId = barrackId;
    }

    /**
     * @return The rankId
     */
    public Integer getRankId() {
        return rankId;
    }

    /**
     * @param rankId The RankId
     */
    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    /**
     * @return The unitTaskId
     */
    public Integer getUnitTaskId() {
        return unitTaskId;
    }

    /**
     * @param unitTaskId The UnitTaskId
     */
    public void setUnitTaskId(Integer unitTaskId) {
        this.unitTaskId = unitTaskId;
    }

    @Override
    public String toString() {
        return "UnitStrengthIR{" +
                "unitId=" + unitId +
                ", actualStrength=" + actualStrength +
                ", checkedDateTime='" + checkedDateTime + '\'' +
                ", leaveCount=" + leaveCount +
                ", barrackId=" + barrackId +
                ", rankId=" + rankId +
                ", unitTaskId=" + unitTaskId +
                '}';
    }
}