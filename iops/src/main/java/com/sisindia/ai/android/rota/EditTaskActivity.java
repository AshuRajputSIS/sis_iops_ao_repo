package com.sisindia.ai.android.rota;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.commons.SelectTimeFragment;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.request.RotaTaskInputRequestMO;
import com.sisindia.ai.android.network.response.AppRole;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.text.ParseException;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;


public class EditTaskActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "EditTaskActivity";
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ShowBottomSheetListener showBottomSheetListener;
    @Bind(R.id.barrack_name)
    CustomFontTextview barrackName;
    @Bind(R.id.tv_barrack_name)
    CustomFontTextview tvBarrackName;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Resources resources;
    private CustomFontTextview unitValue;
    private CustomFontTextview startTaskDate;
    private CustomFontTextview endTaskDate;
    private CustomFontTextview taskStartTime;
    private CustomFontTextview taskEndTime;
    private CustomFontTextview enterLocation;
    private Toolbar toolbar;
    private ImageView checkTypeImage;
    private RelativeLayout mparentRelativeLayout;
    /*private ArrayList<String> unitNames;
    private ArrayList<String> unitAddresses;*/
    private SelectDateFragment selectDateFragment;

    private String unitAddress;
    private CustomFontTextview taskType;
    private RotaTaskModel rotaTaskModel;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private TextView locationTextView;
    // private ArrayList<MyUnitHomeMO> unitDetails;
    //private Map<String,String> unitNamesAddressMap;
    private String selectStartTime;
    private String selectEndTime;
    private RelativeLayout unitLayout;
    private RelativeLayout barrackLayout;
    private AppRole appUserDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        ButterKnife.bind(this);
        dateTimeFormatConversionUtil  = new DateTimeFormatConversionUtil();

        rotaTaskModel  = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if(rotaTaskModel == null){
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
        }
        initViews();
    }


    /*
    * Initialize the views
    *
    * */
    private void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mparentRelativeLayout = (RelativeLayout) findViewById(R.id.parent_relative_layout);
        unitLayout = (RelativeLayout) findViewById(R.id.editUnitBaseLayout);
        barrackLayout = (RelativeLayout) findViewById(R.id.edit_barrack_layout);
        taskType = (CustomFontTextview) findViewById(R.id.task_type_name);
        unitValue = (CustomFontTextview) findViewById(R.id.tv_unitName_value);
        startTaskDate = (CustomFontTextview) findViewById(R.id.tv_start_date);
        endTaskDate = (CustomFontTextview) findViewById(R.id.tv_end_date);
        locationTextView = (TextView) findViewById(R.id.edit_enter_location);
        taskStartTime = (CustomFontTextview) findViewById(R.id.tv_task_start_time);
        taskEndTime = (CustomFontTextview) findViewById(R.id.tv_task_end_time);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        locationTextView.setText(rotaTaskModel.getAddress());
        checkTypeImage = (ImageView) findViewById(R.id.checkType_image);
        resources = getResources();
        //showBottomSheetListener = this;
        // Set the Toolbar and Back Button Enabled in Toolbar
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        // unitDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().getUnitDetails();

        if (rotaTaskModel != null) {
            startTaskDate.setText(dateTimeFormatConversionUtil.convertDateToDayMonthDateYearFormat(rotaTaskModel.getStartDate()));
            endTaskDate.setText(dateTimeFormatConversionUtil.convertDateToDayMonthDateYearFormat(rotaTaskModel.getStartDate()));
            collapsingToolbarLayout.setTitle(rotaTaskModel.getTypeChecking());
            checkTypeImage.setImageResource(rotaTaskModel.getLightTypeCheckingIcon());
            taskType.setText(rotaTaskModel.getTypeChecking());
            taskStartTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(rotaTaskModel.getStartTime()));
            taskEndTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(rotaTaskModel.getEndTime()));
            if (rotaTaskModel.getTaskTypeId() == 3) {
                unitLayout.setVisibility(View.GONE);
                barrackLayout.setVisibility(View.VISIBLE);
                /*if (rotaTaskModel.getUnitName() != null && !rotaTaskModel.getUnitName().isEmpty()) {
                    unitValue.setText(rotaTaskModel.getUnitName());
                } else {
                    locationTextView.setText(getResources().getString(R.string.NOT_AVAILABLE));
                }*/
                if (rotaTaskModel.getBarrackAddress() != null && !rotaTaskModel.getBarrackAddress().isEmpty()) {
                    locationTextView.setText(rotaTaskModel.getBarrackAddress());
                } else {
                    locationTextView.setText(getResources().getString(R.string.NOT_AVAILABLE));
                }
                if (rotaTaskModel.getBarrackName() != null && !rotaTaskModel.getBarrackName().isEmpty()) {
                    tvBarrackName.setText(rotaTaskModel.getBarrackName());
                } else {
                    tvBarrackName.setText(getResources().getString(R.string.NOT_AVAILABLE));
                }
            } else {
                unitLayout.setVisibility(View.VISIBLE);
                barrackLayout.setVisibility(View.GONE);
                if (rotaTaskModel.getAddress() != null && !rotaTaskModel.getAddress().isEmpty()) {
                    locationTextView.setText(rotaTaskModel.getAddress());
                } else {
                    locationTextView.setText(getResources().getString(R.string.NOT_AVAILABLE));
                }
                if (rotaTaskModel.getUnitName() != null && !rotaTaskModel.getUnitName().isEmpty()) {
                    unitValue.setText(rotaTaskModel.getUnitName());
                } else {
                    locationTextView.setText(getResources().getString(R.string.NOT_AVAILABLE));
                }
            }
        }
        // unitValue.setOnClickListener(this);
        startTaskDate.setOnClickListener(this);
        endTaskDate.setOnClickListener(this);
        taskStartTime.setOnClickListener(this);
        taskEndTime.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            validateAllDate();
            return true;
        }
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void validateAllDate() {
        String startdateString = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(startTaskDate.getText().toString());
        String enddateString = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(endTaskDate.getText().toString());
        appUserDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails();
       /* if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                isHolidayAndLeaveAvailable(appUserDetails.getEmployeeId(), startdateString, enddateString) == 0) {*/
            if (selectStartTime != null && selectEndTime != null) {
                updateEditTask(startdateString, enddateString, selectStartTime, selectEndTime);
            } else if (selectStartTime != null && selectEndTime == null) {
                updateEditTask(startdateString, enddateString, selectStartTime, rotaTaskModel.getEndTime());
            } else if (selectStartTime == null && selectEndTime != null) {
                updateEditTask(startdateString, enddateString, rotaTaskModel.getStartTime(), selectEndTime);
            } else {
                updateEditTask(startdateString, enddateString, rotaTaskModel.getStartTime(), rotaTaskModel.getEndTime());
            }
       /* } else {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.LEAVE_HOLIDAY));
        }*/
    }

    private void updateEditTask(String startdateString, String enddateString, String stringStartTime, String stringEndTime) {
        String startTime = dateTimeFormatConversionUtil.timeConverstionFrom12To24(stringStartTime);
        String endTime = dateTimeFormatConversionUtil.timeConverstionFrom12To24(stringEndTime);
        String changedFormatStartTime = startdateString + " " + startTime;
        String changedFormatEndTime = enddateString + " " + endTime;
        if (dateTimeFormatConversionUtil.dateTimeChecker(dateTimeFormatConversionUtil.getCurrentDateTime(), changedFormatStartTime) > 0) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_date));
            return;
        }
        else if (dateTimeFormatConversionUtil.dateTimeChecker(changedFormatStartTime, changedFormatEndTime) > 0) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_date));
            return;
        }
        if(startdateString != null && startTime != null && startdateString.equals(enddateString) && startTime.equals(endTime)){
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.same_start_end_time));
            return;
        }

        if(rotaTaskModel.getTaskTypeId() == RotaTaskTypeEnum.Night_Checking.getValue()){
            try {
                if(dateTimeFormatConversionUtil.nightCheckValidation(changedFormatStartTime,changedFormatEndTime)){
                    editRotaTask(changedFormatStartTime,changedFormatEndTime);
                }
                else{
                    snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.night_check_validation));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            editRotaTask(changedFormatStartTime, changedFormatEndTime);
        }
    }

    private void editRotaTask(String changedFormatStartTime, String changedFormatEndTime) {

        if(IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                getTasksWithSameData(rotaTaskModel.getUnitId(),
                        rotaTaskModel.getTaskTypeId(),changedFormatStartTime,changedFormatEndTime)){
            snackBarWithMesg(mparentRelativeLayout, getResources().
                    getString(R.string.task_already_created_msg));
            return;
        }
        else{
            if(dateTimeFormatConversionUtil.validateTimeDiffBetweenStartAndEndDateTime
                    (changedFormatStartTime,changedFormatEndTime)){
                snackBarWithMesg(mparentRelativeLayout, getResources().
                        getString(R.string.start_end_time_difference_msg));
                return;
            }
            else{
                if(NetworkUtil.isConnected){
                    editaTaskApiCall(updatedTaskDetails(changedFormatStartTime,changedFormatEndTime,
                            RotaTaskStatusEnum.Active.getValue()));
                    Intent homeIntent = new Intent(EditTaskActivity.this, HomeActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);

                }
                else{
                    Util.showToast(getApplicationContext(), getApplicationContext().getResources()
                            .getString(R.string.edittask_warning_msg));
                }
            }

        }


    }

    private void editaTaskApiCall(final RotaTaskInputRequestMO rotaTaskInputRequestMO) {
        Timber.i("EditTask",  IOPSApplication.getGsonInstance().toJson(rotaTaskInputRequestMO,RotaTaskInputRequestMO.class));
        showprogressbar();
        sisClient.getApi().editTask(rotaTaskInputRequestMO, new Callback<CommonResponse>() {
            @Override
            public void success(CommonResponse commonResponse, Response response) {
                if(commonResponse != null && commonResponse.getStatusCode() == 200){
                    IOPSApplication.getInstance().getUpdateStatementDBInstance().
                            updateEditTaskDetails(rotaTaskModel.getTaskId(), dateTimeFormatConversionUtil.getCurrentDate(),
                                    rotaTaskInputRequestMO.getEstimatedTaskExecutionStartDateTime(),
                                    rotaTaskInputRequestMO.getEstimatedTaskExecutionEndDateTime(),
                                    RotaTaskStatusEnum.Active.getValue());
                }
                hideprogressbar();

            }

            @Override
            public void failure(RetrofitError error) {
              hideprogressbar();
            }
        });
    }

    private void showprogressbar() {
        Util.showProgressBar(EditTaskActivity.this, R.string.loading_please_wait);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(!isFinishing());
    }

    private RotaTaskInputRequestMO updatedTaskDetails(String changedFormatStartTime, String changedFormatEndTime, int taskStatusId) {
        RotaTaskInputRequestMO updateRotaTaskInputRequestMO = new RotaTaskInputRequestMO();
        updateRotaTaskInputRequestMO.setId(rotaTaskModel.getTaskId());
        if (rotaTaskModel.getUnitId() != 0) {
            updateRotaTaskInputRequestMO.setUnitId(rotaTaskModel.getUnitId());
        } else {
            updateRotaTaskInputRequestMO.setUnitId(null);
        }
        if (rotaTaskModel.getBarrackId() != 0) {
            updateRotaTaskInputRequestMO.setBarrackId(rotaTaskModel.getBarrackId());
        } else {
            updateRotaTaskInputRequestMO.setBarrackId(null);
        }
        updateRotaTaskInputRequestMO.setTaskTypeId(rotaTaskModel.getTaskTypeId());
        updateRotaTaskInputRequestMO.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime().trim());
        updateRotaTaskInputRequestMO.setEstimatedTaskExecutionStartDateTime(changedFormatStartTime);
        updateRotaTaskInputRequestMO.setEstimatedTaskExecutionEndDateTime(changedFormatEndTime);
        updateRotaTaskInputRequestMO.setEstimatedExecutionTime(null);
        updateRotaTaskInputRequestMO.setTaskStatusId(taskStatusId);
        updateRotaTaskInputRequestMO.setActualExecutionTime(0);
        updateRotaTaskInputRequestMO.setActualTaskExecutionEndDateTime("");
        updateRotaTaskInputRequestMO.setActualTaskExecutionStartDateTime("");
        updateRotaTaskInputRequestMO.setAdditionalComments("");
        updateRotaTaskInputRequestMO.setUnitPostId(null);
        updateRotaTaskInputRequestMO.setExecutorId(appUserDetails.getEmployeeId());
        updateRotaTaskInputRequestMO.setApprovedById(null);
        updateRotaTaskInputRequestMO.setAssigneeId(appUserDetails.getEmployeeId());
        updateRotaTaskInputRequestMO.setAssigneeId(appUserDetails.getEmployeeId());
        updateRotaTaskInputRequestMO.setAssignedById(appUserDetails.getEmployeeId());
        updateRotaTaskInputRequestMO.setIsAutoCreation(0);
        updateRotaTaskInputRequestMO.setCreatedById(appUserDetails.getEmployeeId());
        Timber.i("Edit Task :"+IOPSApplication.getGsonInstance().toJson(updateRotaTaskInputRequestMO));
        return updateRotaTaskInputRequestMO;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_start_date:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                selectDateFragment = new SelectDateFragment(mparentRelativeLayout, null);
                selectDateFragment.show(fragmentTransaction, resources.getString(R.string.DATE_PICKER_FRAGMENT_NAME));
                selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        startTaskDate.setText(updateData);
                        //rotaTaskModel.setStartDate(updateData);
                    }
                });
                break;
            case R.id.tv_end_date:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                if (startTaskDate.getText().toString() == null || startTaskDate.getText().toString().isEmpty()) {
                    selectDateFragment = new SelectDateFragment(mparentRelativeLayout, null);
                } else {
                    selectDateFragment = new SelectDateFragment(mparentRelativeLayout, startTaskDate.getText().toString());

                }
                selectDateFragment.show(fragmentTransaction, resources.getString(R.string.DATE_PICKER_FRAGMENT_NAME));
                selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        endTaskDate.setText(updateData);
                        //rotaTaskModel.set(updateData);
                    }
                });
                break;
            case R.id.tv_task_start_time:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();

                SelectTimeFragment selectTimeFragment = new SelectTimeFragment(mparentRelativeLayout);
                selectTimeFragment.setTime(dateTimeFormatConversionUtil.timeConverstionFrom12To24(rotaTaskModel.getStartTime()));
                selectTimeFragment.show(fragmentTransaction, resources.getString(R.string.TIME_PICKER_FRAGMENT_NAME));
                selectTimeFragment.setCheckingType(rotaTaskModel.getTypeChecking());
//                selectTimeFragment.setCheckingType(checkingType);
                selectTimeFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        selectStartTime = updateData;
                        taskStartTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(updateData));
                        //rotaTaskModel.setStartTime(updateData);
                    }
                });
                break;
            case R.id.tv_task_end_time:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                SelectTimeFragment selectEndTimeFragment = new SelectTimeFragment(mparentRelativeLayout);
                selectEndTimeFragment.setTime(dateTimeFormatConversionUtil.timeConverstionFrom12To24(rotaTaskModel.getEndTime()));
                selectEndTimeFragment.show(fragmentTransaction, resources.getString(R.string.TIME_PICKER_FRAGMENT_NAME));
                selectEndTimeFragment.setCheckingType(rotaTaskModel.getTypeChecking());
                selectEndTimeFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        selectEndTime = updateData;
                        taskEndTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(updateData));
                        //rotaTaskModel.setEndTime(updateData);
                    }
                });

        }
    }
}