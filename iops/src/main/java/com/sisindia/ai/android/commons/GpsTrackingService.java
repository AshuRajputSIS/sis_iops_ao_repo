package com.sisindia.ai.android.commons;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.GpsBatteryInputRequestBaseMO;
import com.sisindia.ai.android.network.request.GpsBatteryInputRequestMOLocation;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by shankar on 16/2/16.
 */
public final class GpsTrackingService extends Service {

    private static final String TAG = GpsTrackingService.class.getSimpleName();
    private static final String MY_LAT_LNG = "MYLATLNG";
    private static final long LOCATION_SERVER_POLLING_INTERVAL = 30000;
    public static double latitude;
    public static double longitude;
    private Location location;
    private boolean isItFirstPing;

    @Override
    public void onCreate() {
        super.onCreate();
        startLocationUpdates();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null) {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }

        final Notification notification = createNotification();
        final Handler handler = new Handler();
        final PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);

        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, TAG);
        final int NOTIFICATION_ID = 1337;
        startForeground(NOTIFICATION_ID, notification);
        final AppPreferences mAppPreference = new AppPreferences(this);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mAppPreference.getDutyStatus()) {
                    batchGpsData();
                    if (wakeLock != null && !wakeLock.isHeld()) {
                        wakeLock.acquire();
                    }
                    handler.postDelayed(this, LOCATION_SERVER_POLLING_INTERVAL);
                } else {
                    final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                    if (wakeLock != null && wakeLock.isHeld()) {
                        wakeLock.release();
                    }
                    stopLocationUpdates();
                    stopForeground(true);
                    stopSelf();
                }
            }
        };
        handler.post(runnable);
        return START_STICKY;
    }

    private Notification createNotification() {
        final Intent homeIntent = new Intent(GpsTrackingService.this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        final PendingIntent pendingIntent = PendingIntent.getActivity(GpsTrackingService.this,
                (int) System.currentTimeMillis(), homeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap logoIcon = BitmapFactory.decodeResource(getResources(), R.drawable.iops);
        return new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.tracking_your_gps_location))
                .setSmallIcon(R.drawable.iops)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setLargeIcon(Bitmap.createScaledBitmap(logoIcon, 128, 128, false))
                .build();
    }

    private void batchGpsData() {
        if (!isItFirstPing) {
            insertGpsDataIntoDb();
        }
        GpsBatteryInputRequestBaseMO gpsDatabaseInfo = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchGpsBatteryData();
        isItFirstPing = false;

        while (gpsDatabaseInfo.getLocation().size() > 0) {
            int size = gpsDatabaseInfo.getLocation().size();
            //Timber.i("GPS Ping count : %s", size);
            GpsBatteryInputRequestBaseMO batchedGpsData = new GpsBatteryInputRequestBaseMO();
            if (size > 50) {
                size = 50;
            }
            batchGpsPings(size, batchedGpsData, gpsDatabaseInfo);
        }
    }

    private void batchGpsPings(int size, GpsBatteryInputRequestBaseMO batchedGpsData,
                               GpsBatteryInputRequestBaseMO gpsDatabaseInfo) {
        ArrayList<GpsBatteryInputRequestMOLocation> batchFiftyList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            batchFiftyList.add(gpsDatabaseInfo.getLocation().get(i));
        }
        batchedGpsData.setLocation(batchFiftyList);
        sendGpsDataToServer(batchedGpsData);
        gpsDatabaseInfo.getLocation().subList(0, size).clear();

    }

    //code for saving gps in db
    private void insertGpsDataIntoDb() {
        if (latitude != 0 && longitude != 0) {
            IOPSApplication.getInstance().getInsertionInstance().
                    insertInGPSTable(latitude, longitude, String.valueOf(Util.getBatteryLevel(this)),
                            new DateTimeFormatConversionUtil().getCurrentDateTime(),
                            false, false, false);

        }
    }

    private void sendGpsDataToServer(final GpsBatteryInputRequestBaseMO batchGpsData) {
        if (!SmartLocation.with(this).location().state().locationServicesEnabled()
                || batchGpsData == null || batchGpsData.getLocation().size() == 0) {
            return;
        }

        //checking the Internet connection
        if (Util.isNetworkConnected()) {
//            String request = new Gson().toJson(batchGpsData);
            //Timber.d("GPS Request Json : %s", request);

            new SISClient(this).getApi().postLocationData(batchGpsData, new Callback<CommonResponse>() {
                @Override
                public void success(CommonResponse commonResponse, Response response) {
                    //Timber.d("Response Status = %s", commonResponse.getStatusCode());
                    if (commonResponse.getStatusCode() == 200) {
                        //Timber.d("GPS Successfully pushed %s records", batchGpsData.getLocation().size());
                        IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                .deleteRecordFromTable(TableNameAndColumnStatement.GPS_BATTERY_HEALTH_TABLE,
                                        TableNameAndColumnStatement.ID, batchGpsData.getLocation().size());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Crashlytics.logException(error);
                }
            });
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationParams params = LocationParams.NAVIGATION;
        SmartLocation.with(this).location().config(params).start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                GpsTrackingService.this.location = location;
                latitude = location.getLatitude();
                longitude = location.getLongitude();

//                Log.e("LatLong","latLib : "+location.getLatitude() +", LongLib : "+ location.getLongitude());
//                Log.e("LatLong","lat : "+latitude +", Long : "+ longitude);

            }
        });
        isItFirstPing = true;
        sendBroadcastMessage();
    }

    protected void stopLocationUpdates() {
        SmartLocation.with(this).location().stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendBroadcastMessage() {
        Intent intent = new Intent();
        intent.setAction(MY_LAT_LNG);
        intent.putExtra("mLastLocation", location);
        sendBroadcast(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }
}