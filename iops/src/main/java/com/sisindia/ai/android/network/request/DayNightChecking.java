package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.daycheck.taskExecution.GuardDetail;

import java.util.ArrayList;
import java.util.List;

public class DayNightChecking {

    @SerializedName("GuardDetails")
    private List<GuardDetail> guardDetails = new ArrayList<GuardDetail>();
    @SerializedName("DutyRegisterCheck")
    private DutyRegisterCheck dutyRegisterCheck;
    @SerializedName("ClientRegisterCheck")
    private ClientRegisterCheck clientRegisterCheck;
    @SerializedName("SecurityRiskobservation")
    private SecurityRiskobservation securityRiskobservation;
    @SerializedName("ClientHandShakeMO")

    private ClientHandShake clientHandShake;

    /**
     * @return The guardDetails
     */
    public List<GuardDetail> getGuardDetails() {
        return guardDetails;
    }

    /**
     * @param guardDetails The GuardDetails
     */
    public void setGuardDetails(List<GuardDetail> guardDetails) {
        this.guardDetails = guardDetails;
    }

    /**
     * @return The dutyRegisterCheck
     */
    public DutyRegisterCheck getDutyRegisterCheck() {
        return dutyRegisterCheck;
    }

    /**
     * @param dutyRegisterCheck The DutyRegisterCheck
     */
    public void setDutyRegisterCheck(DutyRegisterCheck dutyRegisterCheck) {
        this.dutyRegisterCheck = dutyRegisterCheck;
    }

    /**
     * @return The clientRegisterCheck
     */
    public ClientRegisterCheck getClientRegisterCheck() {
        return clientRegisterCheck;
    }

    /**
     * @param clientRegisterCheck The ClientRegisterCheck
     */
    public void setClientRegisterCheck(ClientRegisterCheck clientRegisterCheck) {
        this.clientRegisterCheck = clientRegisterCheck;
    }

    /**
     * @return The securityRiskobservation
     */
    public SecurityRiskobservation getSecurityRiskobservation() {
        return securityRiskobservation;
    }

    /**
     * @param securityRiskobservation The SecurityRiskobservation
     */
    public void setSecurityRiskobservation(SecurityRiskobservation securityRiskobservation) {
        this.securityRiskobservation = securityRiskobservation;
    }

    /**
     * @return The clientHandShake
     */
    public ClientHandShake getClientHandShake() {
        return clientHandShake;
    }

    /**
     * @param clientHandShake The ClientHandShakeMO
     */
    public void setClientHandShake(ClientHandShake clientHandShake) {
        this.clientHandShake = clientHandShake;
    }

    @Override
    public String toString() {
        return "DayNightChecking{" +
                "guardDetails=" + guardDetails +
                ", dutyRegisterCheck=" + dutyRegisterCheck +
                ", clientRegisterCheck=" + clientRegisterCheck +
                ", securityRiskobservation=" + securityRiskobservation +
                ", clientHandShake=" + clientHandShake +
                '}';
    }
}