package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 14/7/16.
 */

public class DataMO implements Serializable {

    @SerializedName("RiskDetailModels")
    @Expose
    private List<RiskDetailModelMO> riskDetailModels = new ArrayList<RiskDetailModelMO>();
    @SerializedName("UnitRiskCount")
    @Expose
    private UnitRiskCountMO unitRiskCount;

    /**
     *
     * @return
     * The riskDetailModels
     */
    public List<RiskDetailModelMO> getRiskDetailModels() {
        return riskDetailModels;
    }

    /**
     *
     * @param riskDetailModels
     * The RiskDetailModels
     */
    public void setRiskDetailModels(List<RiskDetailModelMO> riskDetailModels) {
        this.riskDetailModels = riskDetailModels;
    }

    /**
     *
     * @return
     * The unitRiskCount
     */
    public UnitRiskCountMO getUnitRiskCount() {
        return unitRiskCount;
    }

    /**
     *
     * @param unitRiskCount
     * The UnitRiskCount
     */
    public void setUnitRiskCount(UnitRiskCountMO unitRiskCount) {
        this.unitRiskCount = unitRiskCount;
    }
}
