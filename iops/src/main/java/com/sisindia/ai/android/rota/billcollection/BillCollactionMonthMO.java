package com.sisindia.ai.android.rota.billcollection;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shruti on 14/7/16.
 */
public class BillCollactionMonthMO {


    int id;

    int unitId;

    String unitName;

    String billNo;

    int billMonth;

    int billYear;

    int outstandingAmount;

    int outstandingDays;

    String updatedDate;

    int isBillCollacted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public int getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(int billMonth) {
        this.billMonth = billMonth;
    }

    public int getBillYear() {
        return billYear;
    }

    public void setBillYear(int billYear) {
        this.billYear = billYear;
    }

    public int getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(int outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public int getOutstandingDays() {
        return outstandingDays;
    }

    public void setOutstandingDays(int outstandingDays) {
        this.outstandingDays = outstandingDays;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getIsBillCollacted() {
        return isBillCollacted;
    }

    public void setIsBillCollacted(int isBillCollacted) {
        this.isBillCollacted = isBillCollacted;
    }
}
