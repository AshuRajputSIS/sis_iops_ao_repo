package com.sisindia.ai.android.issues.models;

/**
 * Created by Shushrut on 19-05-2016.
 */
public class AddComplaintMO {
    private String clientName;
    private int modeOfComplaintId;
    private int natureOfComplaintId;
    private int causeOfComplaintId;
    private int actionTakenId;
    private String additionalRemarks;
    private int improvementPlanId;
    private int issueId;
    private int unitId;
    private int assignedToId;
    private int complaintStatus;
    private int employeeIdAI;
    private int taskId;

    public int getEmployeeIdAI() {
        return employeeIdAI;
    }

    public void setEmployeeIdAI(int employeeIdAI) {
        this.employeeIdAI = employeeIdAI;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getModeOfComplaintId() {
        return modeOfComplaintId;
    }

    public void setModeOfComplaintId(int modeOfComplaintId) {
        this.modeOfComplaintId = modeOfComplaintId;
    }

    public int getNatureOfComplaintId() {
        return natureOfComplaintId;
    }

    public void setNatureOfComplaintId(int natureOfComplaintId) {
        this.natureOfComplaintId = natureOfComplaintId;
    }

    public int getCauseOfComplaintId() {
        return causeOfComplaintId;
    }

    public void setCauseOfComplaintId(int causeOfComplaintId) {
        this.causeOfComplaintId = causeOfComplaintId;
    }

    public int getActionTakenId() {
        return actionTakenId;
    }

    public void setActionTakenId(int actionTakenId) {
        this.actionTakenId = actionTakenId;
    }

    public String getAdditionalRemarks() {
        return additionalRemarks;
    }

    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }

    public int getImprovementPlanId() {
        return improvementPlanId;
    }

    public void setImprovementPlanId(int improvementPlanId) {
        this.improvementPlanId = improvementPlanId;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getAssignedToId() {
        return assignedToId;
    }

    public void setAssignedToId(int assignedToId) {
        this.assignedToId = assignedToId;
    }

    public int getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(int complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
