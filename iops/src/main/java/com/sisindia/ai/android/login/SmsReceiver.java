package com.sisindia.ai.android.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.AppPreferences;

import timber.log.Timber;

public class SmsReceiver extends BroadcastReceiver {
    private static final String TAG = SmsReceiver.class.getSimpleName();
    private AppPreferences mAppPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            final Bundle bundle = intent.getExtras();
            mAppPreferences = new AppPreferences(context);
            try {
                if (bundle != null) {
                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (Object aPdusObj : pdusObj) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                        String senderAddress = currentMessage.getDisplayOriginatingAddress();
                        String message = currentMessage.getDisplayMessageBody();
                        Timber.d(TAG + " %s", "Received SMS: " + message + ", Sender: " + senderAddress);

                        // if the SMS is not from our gateway, ignore the message
//                        if (!senderAddress.toLowerCase().contains(Constants.SMS_ORIGIN.toLowerCase())) {
//                            Timber.e(TAG + " %s", "SMS is not for our app!");
//                            return;
//                        }

                        if (!message.contains("SISAI")) {
                            Timber.e(TAG + " %s", "SMS is not for our app!");
                            return;
                        }
                        // verification code from sms
                        String verificationCode = getVerificationCode(message);
                        mAppPreferences.setOtp(verificationCode);
                        Timber.e(TAG + " %s", "OTP received: " + verificationCode);
                        LoginActivity.onSMSReceivedListener.onSMSReceived(verificationCode);

                    }
                }
            } catch (Exception e) {
                Timber.d(TAG + " %s", "Exception: " + e.getMessage());
            }
        }
    }

    /**
     * Getting the OTP from sms message body
     * ':' is the separator of OTP from the message
     *
     * @param message
     * @return
     */
    private String getVerificationCode(String message) {
        String code = null;
        int index = message.indexOf(Constants.OTP_DELIMITER);
        Timber.e(TAG + " %s", "OTP index: " + index);
        if (index != -1) {
            // int start = index + 2;
            int start = index + 1;
            int length = 6;
            code = message.substring(start, start + length);
            return code;
        }

        return code;
    }
}
