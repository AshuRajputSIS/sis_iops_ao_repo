package com.sisindia.ai.android.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.BarrackInspectionQuestionMO;
import com.sisindia.ai.android.loadconfiguration.IssueMatrixModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by compass on 7/10/2017.
 */

 public class BarrackQuestionBaseOR {
    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;
    @SerializedName("Data")
    @Expose
    public List<BarrackInspectionQuestionMO> barrackInspectionQuestions;
}
