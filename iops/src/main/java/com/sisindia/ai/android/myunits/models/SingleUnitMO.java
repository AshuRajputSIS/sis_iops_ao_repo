package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleUnitMO {

    @SerializedName("UnitName")
    @Expose
    private String unitName;
    @SerializedName("TaskType")
    @Expose
    private String taskType;
    @SerializedName("TaskTypeId")
    @Expose
    private Integer taskTypeId;
    @SerializedName("CurrentMonthAllTasksCount")
    @Expose
    private Integer currentMonthAllTasksCount;
    @SerializedName("CurrentMonthCompletedTasksCount")
    @Expose
    private Integer currentMonthCompletedTasksCount;
    @SerializedName("Grievances")
    @Expose
    private Integer grievances;
    @SerializedName("Compliants")
    @Expose
    private Integer compliants;
    @SerializedName("LastActivityDate")
    @Expose
    private String lastActivityDate;

    /**
     * @return The unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * @param unitName The UnitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * @return The taskType
     */
    public String getTaskType() {
        return taskType;
    }

    /**
     * @param taskType The TaskType
     */
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    /**
     * @return The taskTypeId
     */
    public Integer getTaskTypeId() {
        return taskTypeId;
    }

    /**
     * @param taskTypeId The TaskTypeId
     */
    public void setTaskTypeId(Integer taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    /**
     * @return The currentMonthAllTasksCount
     */
    public Integer getCurrentMonthAllTasksCount() {
        return currentMonthAllTasksCount;
    }

    /**
     * @param currentMonthAllTasksCount The CurrentMonthAllTasksCount
     */
    public void setCurrentMonthAllTasksCount(Integer currentMonthAllTasksCount) {
        this.currentMonthAllTasksCount = currentMonthAllTasksCount;
    }

    /**
     * @return The currentMonthCompletedTasksCount
     */
    public Integer getCurrentMonthCompletedTasksCount() {
        return currentMonthCompletedTasksCount;
    }

    /**
     * @param currentMonthCompletedTasksCount The CurrentMonthCompletedTasksCount
     */
    public void setCurrentMonthCompletedTasksCount(Integer currentMonthCompletedTasksCount) {
        this.currentMonthCompletedTasksCount = currentMonthCompletedTasksCount;
    }

    /**
     * @return The grievances
     */
    public Integer getGrievances() {
        return grievances;
    }

    /**
     * @param grievances The Grievances
     */
    public void setGrievances(Integer grievances) {
        this.grievances = grievances;
    }

    /**
     * @return The compliants
     */
    public Integer getCompliants() {
        return compliants;
    }

    /**
     * @param compliants The Compliants
     */
    public void setCompliants(Integer compliants) {
        this.compliants = compliants;
    }

    /**
     * @return The lastActivityDate
     */
    public String getLastActivityDate() {
        return lastActivityDate;
    }

    /**
     * @param lastActivityDate The LastActivityDate
     */
    public void setLastActivityDate(String lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

}

