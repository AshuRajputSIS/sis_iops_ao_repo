package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UnitPostChecking {

    @SerializedName("PostName")
    public String PostName;
    @SerializedName("PostId")
    public String PostId;
    @SerializedName("NFC")
    public String NFC;
    @SerializedName("SOPAvailable")
    public Boolean SOPAvailable;
    @SerializedName("AttendanceRegisterAvailable")
    public Boolean AttendanceRegisterAvailable;
    @SerializedName("EquipmentListIMO")
    public List<EquipmentList> EquipmentList = new ArrayList<EquipmentList>();

}

