package com.sisindia.ai.android.fcmnotification.fcmLookup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;

/**
 * Created by shankar on 19/7/16.
 */

public class LookupDeletionStatementDB extends SISAITrackingDB {

    public LookupDeletionStatementDB(Context context) {
        super(context);
    }

    public void deleteLookupTable(String tableName, SQLiteDatabase sqlite) {

        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }
}



