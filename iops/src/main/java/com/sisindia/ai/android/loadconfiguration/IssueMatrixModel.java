package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IssueMatrixModel {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("IssueTypeId")
    @Expose
    private Integer issueTypeId;
    @SerializedName("IssueCategoryId")
    @Expose
    private Integer issueCategoryId;
    @SerializedName("IssueSubCategoryId")
    @Expose
    private Integer issueSubCategoryId;
    @SerializedName("IssueCauseId")
    @Expose
    private Integer issueCauseId;
    @SerializedName("MasterActionPlanId")
    @Expose
    private Integer masterActionPlanId;
    @SerializedName("DefaultAssignmentRoleId")
    @Expose
    private Integer defaultAssignmentRoleId;
    @SerializedName("UnitTypeId")
    @Expose
    private Integer unitTypeId;
    @SerializedName("TurnAroundTime")
    @Expose
    private Integer turnAroundTime;
    @SerializedName("ProofControlType")
    @Expose
    private Integer proofControlType;
    @SerializedName("MinimumOptionalValue")
    @Expose
    private Integer minimumOptionalValue;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The issueTypeId
     */
    public Integer getIssueTypeId() {
        return issueTypeId;
    }

    /**
     * @param issueTypeId The IssueTypeId
     */
    public void setIssueTypeId(Integer issueTypeId) {
        this.issueTypeId = issueTypeId;
    }

    /**
     * @return The issueCategoryId
     */
    public Integer getIssueCategoryId() {
        return issueCategoryId;
    }

    /**
     * @param issueCategoryId The IssueCategoryId
     */
    public void setIssueCategoryId(Integer issueCategoryId) {
        this.issueCategoryId = issueCategoryId;
    }

    /**
     * @return The issueSubCategoryId
     */
    public Integer getIssueSubCategoryId() {
        return issueSubCategoryId;
    }

    /**
     * @param issueSubCategoryId The IssueSubCategoryId
     */
    public void setIssueSubCategoryId(Integer issueSubCategoryId) {
        this.issueSubCategoryId = issueSubCategoryId;
    }

    /**
     * @return The issueCauseId
     */
    public Integer getIssueCauseId() {
        return issueCauseId;
    }

    /**
     * @param issueCauseId The IssueCauseId
     */
    public void setIssueCauseId(Integer issueCauseId) {
        this.issueCauseId = issueCauseId;
    }

    /**
     * @return The masterActionPlanId
     */
    public Integer getMasterActionPlanId() {
        return masterActionPlanId;
    }

    /**
     * @param masterActionPlanId The MasterActionPlanId
     */
    public void setMasterActionPlanId(Integer masterActionPlanId) {
        this.masterActionPlanId = masterActionPlanId;
    }

    /**
     * @return The defaultAssignmentRoleId
     */
    public Integer getDefaultAssignmentRoleId() {
        return defaultAssignmentRoleId;
    }

    /**
     * @param defaultAssignmentRoleId The DefaultAssignmentRoleId
     */
    public void setDefaultAssignmentRoleId(Integer defaultAssignmentRoleId) {
        this.defaultAssignmentRoleId = defaultAssignmentRoleId;
    }

    /**
     * @return The unitTypeId
     */
    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    /**
     * @param unitTypeId The UnitTypeId
     */
    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    /**
     * @return The turnAroundTime
     */
    public Integer getTurnAroundTime() {
        return turnAroundTime;
    }

    /**
     * @param turnAroundTime The TurnAroundTime
     */
    public void setTurnAroundTime(Integer turnAroundTime) {
        this.turnAroundTime = turnAroundTime;
    }

    /**
     * @return The proofControlType
     */
    public Integer getProofControlType() {
        return proofControlType;
    }

    /**
     * @param proofControlType The ProofControlType
     */
    public void setProofControlType(Integer proofControlType) {
        this.proofControlType = proofControlType;
    }

    /**
     * @return The minimumOptionalValue
     */
    public Integer getMinimumOptionalValue() {
        return minimumOptionalValue;
    }

    /**
     * @param minimumOptionalValue The MinimumOptionalValue
     */
    public void setMinimumOptionalValue(Integer minimumOptionalValue) {
        this.minimumOptionalValue = minimumOptionalValue;
    }

    @Override
    public String toString() {
        return "IssueMatrixModel{" +
                "id=" + id +
                ", issueTypeId=" + issueTypeId +
                ", issueCategoryId=" + issueCategoryId +
                ", issueSubCategoryId=" + issueSubCategoryId +
                ", issueCauseId=" + issueCauseId +
                ", masterActionPlanId=" + masterActionPlanId +
                ", defaultAssignmentRoleId=" + defaultAssignmentRoleId +
                ", unitTypeId=" + unitTypeId +
                ", turnAroundTime=" + turnAroundTime +
                ", proofControlType=" + proofControlType +
                ", minimumOptionalValue=" + minimumOptionalValue +
                '}';
    }
}