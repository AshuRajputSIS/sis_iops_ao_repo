package com.sisindia.ai.android.database.tableSyncCountDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.AttachmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.BarrackTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.ImprovementPlanTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.IssuesTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitDistributionItemTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitItemRequestTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.RecruitmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.TaskTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitContactTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitEquipmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitPostTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitStrengthTableCountMO;

/**
 * Created by shankar on 19/10/16.
 */

public class TableSyncCount extends SISAITrackingDB {
    public TableSyncCount(Context context) {
        super(context);
    }


    public synchronized TaskTableCountMO getTaskTableCount(){
        TaskTableCountMO taskTableCountMO = new TaskTableCountMO();
         String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.TASK_STATUS_ID+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.TASK_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.TASK_TABLE + " where "+TableNameAndColumnStatement.TASK_STATUS_ID+" in (4)";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        taskTableCountMO.setSyncCount(syncedCount);
                        taskTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return taskTableCountMO;
    }

    public synchronized AttachmentTableCountMO getAttachmentTableCount(){
        AttachmentTableCountMO attachmentTableCountMO = new AttachmentTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        attachmentTableCountMO.setSyncCount(syncedCount);
                        attachmentTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return attachmentTableCountMO;
    }

    public synchronized BarrackTableCountMO getBarrackTableCount(){
        BarrackTableCountMO barrackTableCountMO = new BarrackTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.BARRACK_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.BARRACK_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        barrackTableCountMO.setSyncCount(syncedCount);
                        barrackTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return barrackTableCountMO;
    }

    public synchronized ImprovementPlanTableCountMO getImprovementPlanTableCount(){
        ImprovementPlanTableCountMO improvementPlanTableCountMO = new ImprovementPlanTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        improvementPlanTableCountMO.setSyncCount(syncedCount);
                        improvementPlanTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return improvementPlanTableCountMO;
    }

    public synchronized KitItemRequestTableCountMO getKitItemRequestTableCount(){
        KitItemRequestTableCountMO kitItemRequestTableCountMO = new KitItemRequestTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        kitItemRequestTableCountMO.setSyncCount(syncedCount);
                        kitItemRequestTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return kitItemRequestTableCountMO;
    }

    public synchronized KitDistributionItemTableCountMO getKitDistributionItemTableCount(){
        KitDistributionItemTableCountMO kitDistributionItemTableCountMO = new KitDistributionItemTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        kitDistributionItemTableCountMO.setSyncCount(syncedCount);
                        kitDistributionItemTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return kitDistributionItemTableCountMO;
    }

    public synchronized RecruitmentTableCountMO getRecruitmentTableCount(){
        RecruitmentTableCountMO recruitmentTableCountMO = new RecruitmentTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.RECRUITMENT_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.RECRUITMENT_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        recruitmentTableCountMO.setSyncCount(syncedCount);
                        recruitmentTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return recruitmentTableCountMO;
    }

    public synchronized UnitStrengthTableCountMO getUnitStrengthTableCount(){
        UnitStrengthTableCountMO unitStrengthTableCountMO = new UnitStrengthTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        unitStrengthTableCountMO.setSyncCount(syncedCount);
                        unitStrengthTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return unitStrengthTableCountMO;
    }

    public synchronized UnitContactTableCountMO getUnitContactTableCount(){
        UnitContactTableCountMO unitContactTableCountMO = new UnitContactTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.UNIT_CONTACT_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.UNIT_CONTACT_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);


            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {



                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        unitContactTableCountMO.setSyncCount(syncedCount);
                        unitContactTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return unitContactTableCountMO;
    }

    public synchronized UnitEquipmentTableCountMO getUnitEquipmentTableCount(){
        UnitEquipmentTableCountMO unitEquipmentTableCountMO = new UnitEquipmentTableCountMO();
        String selectQuery;
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";
        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        unitEquipmentTableCountMO.setSyncCount(syncedCount);
                        unitEquipmentTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return unitEquipmentTableCountMO;
    }

    public synchronized UnitPostTableCountMO getUnitPostTableCount(){
        UnitPostTableCountMO unitPostTableCountMO = new UnitPostTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.UNIT_POST_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.UNIT_POST_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        unitPostTableCountMO.setSyncCount(syncedCount);
                        unitPostTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return unitPostTableCountMO;
    }

    public synchronized IssuesTableCountMO getIssueTableCount(){
        IssuesTableCountMO issuesTableCountMO = new IssuesTableCountMO();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_SYNCED+") as syncedCount, (select count(id) from "
                + TableNameAndColumnStatement.ISSUES_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.ISSUES_TABLE + " where "+TableNameAndColumnStatement.IS_SYNCED+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int syncedCount = cursor.getInt(cursor.getColumnIndex("syncedCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        issuesTableCountMO.setSyncCount(syncedCount);
                        issuesTableCountMO.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return issuesTableCountMO;
    }

    public synchronized FileUploadCount getFileUploadCount(){
        FileUploadCount fileUploadCount = new FileUploadCount();
        String selectQuery;
        // Select All Query
        selectQuery = "select count("+TableNameAndColumnStatement.IS_FILE_Upload+") as fileUploadCount, (select count(id) from "
                + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE+" ) as totalCount from "
                + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where "+TableNameAndColumnStatement.IS_FILE_Upload+"='0'";

        SQLiteDatabase sqlite = null;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        int syncedCount = cursor.getInt(cursor.getColumnIndex("fileUploadCount"));
                        int totalCount = cursor.getInt(cursor.getColumnIndex("totalCount"));
                        fileUploadCount.setSyncCount(syncedCount);
                        fileUploadCount.setTotalCount(totalCount);

                        // Adding contact to list
                    } while (cursor.moveToNext());
                }
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return fileUploadCount;
    }

}
