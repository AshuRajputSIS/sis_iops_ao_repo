package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.myunits.models.FilterOR;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

public class FilterUnitsActivity extends BaseActivity implements OnRecyclerViewItemClickListener {
    public List<LookupModel> filterUnitsList;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.myunits_filter_units_recyclerview)
    RecyclerView filterUnitsRecyclerView;
    private Drawable dividerDrawable;
    private FilterUnitsAdapter filterUnitsAdapter;
    private Context mContext;
    private Resources resources;
    private AppPreferences mAppPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_units);
        ButterKnife.bind(this);
        mContext = FilterUnitsActivity.this;
        resources = getResources();
         mAppPreferences = new AppPreferences(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        getSupportActionBar().setTitle(getResources().getString(R.string.MYUNITS_FILTER_TITLE));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        filterUnitsRecyclerView.setLayoutManager(linearLayoutManager);
        filterUnitsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        filterUnitsAdapter = new FilterUnitsAdapter(this);
        setFilterUnitsData();

        filterUnitsAdapter.setOnRecyclerViewItemClickListener(this);
        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        filterUnitsRecyclerView.addItemDecoration(dividerItemDecoration);


    }

    public void setFilterUnitsData() {
        filterUnitsList = IOPSApplication.getInstance()
                .getUnitLevelSelectionQueryInstance()
                .fetchFilterList(resources.getString(R.string.unit_filter_lookup));
        filterUnitsAdapter.setFilterUnitsData(filterUnitsList);
        filterUnitsRecyclerView.setAdapter(filterUnitsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        if(NetworkUtil.isConnected) {
            filterApiCall(filterUnitsList.get(position).getLookupIdentifier());
        }
        else{
           snackBarWithMesg(filterUnitsRecyclerView,resources.getString(R.string.no_internet));
        }

    }

    /**
     * filter id nothing but the lookup identifier selected from
     * @param filterId
     */

    private void filterApiCall(int filterId) {
        showprogressbar(R.string.loading_please_wait);
        sisClient.getApi().getFilterUnits(filterId, new Callback<ApiResponse<FilterOR>>() {
            @Override
            public void success(ApiResponse<FilterOR> filterORApiResponse, Response response) {
                Timber.d("filterApiCall %s", filterORApiResponse);
                if(null != filterORApiResponse.data.getUnits()){
             String unitsIds = android.text.TextUtils.join(",", filterORApiResponse.data.getUnits());

                    storeInAppPrefrences(unitsIds);
                }
                else{
                    Util.showToast(mContext,resources.getString(R.string.filter_no_units_found));
                    mAppPreferences.setFilterIds(null);
                }
                hideprogressbar();
                FilterUnitsActivity.this.finish();

            }

            @Override
            public void failure(RetrofitError error) {
                hideprogressbar();
                Crashlytics.logException(error);
            }
        });


    }
    private void showprogressbar(int mesg) {
        Util.showProgressBar(mContext, mesg);
    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isDestroyed());
    }

    /**
     * Stroing the Ids in App prefrences as everytime on click on back newinstance of parent acitivity i.e
     * is home activity created everytime then nvigating it to unitFragmnet .
     * so whatever filter got selected gets cleared ad displaying all everytime.
     *
     * @param ids
     */
    private void storeInAppPrefrences(String ids){
        appPreferences.setFilterIds(ids);
    }
}
