package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga Prasad on 02-09-2016.
 */
public class KitItemRequestMO {

    @SerializedName("GuardId")
    private String guardId;

    @SerializedName("GuardCode")
    private String guardCode;

    @SerializedName("SourceTaskId")
    private int sourceTaskId;

    @SerializedName("RequestedOn")
    private String requestedOn;

    @SerializedName("RequestedStatus")
    private String requestedStatus;

    //This filed is used for storing json array as string in db.
    private String requestedItems;

    @SerializedName("KitItemRequestItems")
    private List<KitItemRequestItem> kitItemRequestItems = new ArrayList<>();

    private ArrayList<String> kitItemRequestNames;
    private int kitItemRequstedId;
    private int id;
    private String guardName;

    public ArrayList<String> getKitItemRequestNames() {
        return kitItemRequestNames;
    }

    public void setKitItemRequestNames(ArrayList<String> kitItemRequestNames) {
        this.kitItemRequestNames = kitItemRequestNames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuardCode() {
        return guardCode;
    }

    public void setGuardCode(String guardCode) {
        this.guardCode = guardCode;
    }

    public String getGuardId() {
        return guardId;
    }

    public void setGuardId(String guardId) {
        this.guardId = guardId;
    }

    public int getKitItemRequstedId() {
        return kitItemRequstedId;
    }

    public void setKitItemRequstedId(int kitItemRequstedId) {
        this.kitItemRequstedId = kitItemRequstedId;
    }

    public String getRequestedItems() {
        return requestedItems;
    }

    public void setRequestedItems(String requestedItems) {
        this.requestedItems = requestedItems;
    }

    public String getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(String requestedOn) {
        this.requestedOn = requestedOn;
    }

    public String getRequestedStatus() {
        return requestedStatus;
    }

    public void setRequestedStatus(String requestedStatus) {
        this.requestedStatus = requestedStatus;
    }

    public int getSourceTaskId() {
        return sourceTaskId;
    }

    public void setSourceTaskId(int sourceTaskId) {
        this.sourceTaskId = sourceTaskId;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public List<KitItemRequestItem> getKitItemRequestItems() {
        return kitItemRequestItems;
    }

    public void setKitItemRequestItems(List<KitItemRequestItem> kitItemRequestItems) {
        this.kitItemRequestItems = kitItemRequestItems;
    }
}
