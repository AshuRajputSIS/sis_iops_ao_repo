package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/10/16.
 */

public class PreviousMonth {
    @SerializedName("Selected")
    @Expose
    private Integer selected;
    @SerializedName("Rejected")
    @Expose
    private Integer rejected;
    @SerializedName("Inprocess")
    @Expose
    private Integer inprocess;
    @SerializedName("Recommended")
    @Expose
    private Integer recommended;
    @SerializedName("Month")
    @Expose
    private Integer month;
    @SerializedName("Year")
    @Expose
    private Integer year;
    @SerializedName("TotalRecruits")
    @Expose
    private Integer totalRecruits;
    @SerializedName("TotalRecruitsTill")
    @Expose
    private Integer totalRecruitsTill;

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }

    public Integer getRejected() {
        return rejected;
    }

    public void setRejected(Integer rejected) {
        this.rejected = rejected;
    }

    public Integer getInprocess() {
        return inprocess;
    }

    public void setInprocess(Integer inprocess) {
        this.inprocess = inprocess;
    }

    public Integer getRecommended() {
        return recommended;
    }

    public void setRecommended(Integer recommended) {
        this.recommended = recommended;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getTotalRecruits() {
        return totalRecruits;
    }

    public void setTotalRecruits(Integer totalRecruits) {
        this.totalRecruits = totalRecruits;
    }

    public Integer getTotalRecruitsTill() {
        return totalRecruitsTill;
    }

    public void setTotalRecruitsTill(Integer totalRecruitsTill) {
        this.totalRecruitsTill = totalRecruitsTill;
    }
}
