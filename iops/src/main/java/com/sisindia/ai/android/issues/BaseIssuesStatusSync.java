package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Durga Prasad on 01-12-2016.
 */
public class BaseIssuesStatusSync {

@SerializedName("StatusCode")
private Integer statusCode;
@SerializedName("StatusMessage")
private String statusMessage;
@SerializedName("Data")
private FcmIssuesData fcmIssuesData;

/**
*
* @return
* The statusCode
*/
public Integer getStatusCode() {
return statusCode;
}

/**
*
* @param statusCode
* The StatusCode
*/
public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

/**
*
* @return
* The statusMessage
*/
public String getStatusMessage() {
return statusMessage;
}

/**
*
* @param statusMessage
* The StatusMessage
*/
public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

    public FcmIssuesData getFcmIssuesData() {
        return fcmIssuesData;
    }

    public void setFcmIssuesData(FcmIssuesData fcmIssuesData) {
        this.fcmIssuesData = fcmIssuesData;
    }
}
