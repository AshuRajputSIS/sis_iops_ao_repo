package com.sisindia.ai.android.fcmnotification.notificationReceipt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 11/11/16.
 */

public class NotificationIR {
    @SerializedName("RecipientId")
    @Expose
    private Integer recipientId;
    @SerializedName("CallbackUrl")
    @Expose
    private String callbackUrl;
    @SerializedName("IsSuccessful")
    @Expose
    private Integer isSuccessful;
    @SerializedName("AppNotificationId")
    @Expose
    private Integer appNotificationId;

    private int id;
    private int isSynced;

    public String getNotificationName() {
        return notificationName;
    }

    public void setNotificationName(String notificationName) {
        this.notificationName = notificationName;
    }

    public int isSynced() {
        return isSynced;
    }

    public void setSynced(int synced) {
        isSynced = synced;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String notificationName;


    /**
     *
     * @return
     * The recipientId
     */
    public Integer getRecipientId() {
        return recipientId;
    }

    /**
     *
     * @param recipientId
     * The RecipientId
     */
    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    /**
     *
     * @return
     * The callbackUrl
     */
    public String getCallbackUrl() {
        return callbackUrl;
    }

    /**
     *
     * @param callbackUrl
     * The CallbackUrl
     */
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    /**
     *
     * @return
     * The isSuccessful
     */
    public Integer getIsSuccessful() {
        return isSuccessful;
    }

    /**
     *
     * @param isSuccessful
     * The IsSuccessful
     */
    public void setIsSuccessful(Integer isSuccessful) {
        this.isSuccessful = isSuccessful;
    }

    /**
     *
     * @return
     * The appNotificationId
     */
    public Integer getAppNotificationId() {
        return appNotificationId;
    }

    /**
     *
     * @param appNotificationId
     * The AppNotificationId
     */
    public void setAppNotificationId(Integer appNotificationId) {
        this.appNotificationId = appNotificationId;
    }
}
