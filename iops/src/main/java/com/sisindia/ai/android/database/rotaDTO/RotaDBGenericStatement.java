package com.sisindia.ai.android.database.rotaDTO;

import android.content.Context;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.rota.models.RotaMO;
import com.sisindia.ai.android.syncadpter.CommonSyncDbOperation;

/**
 * Created by shankar on 12/11/16.
 */

public abstract class RotaDBGenericStatement extends CommonSyncDbOperation {

    private static boolean isInsertedSuccessfully = false;
    private static boolean rotaInserted = false;
    private static AppPreferences appPreferences;

    public RotaDBGenericStatement(Context mcontext) {
        super(mcontext);
        appPreferences = new AppPreferences(mcontext);
    }

    //    public static synchronized void RotaModelInsertion(RotaMO rotaData, boolean isNotLoadConfig, NotificationReceivingMO notificationReceivingMO) {
    public static synchronized void RotaModelInsertion(RotaMO rotaData, boolean isNotLoadConfig, NotificationReceivingMO notificationReceivingMO) {
        if (rotaData != null) {
            if (null != rotaData.getRotaModelMO()) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertRotaTable(rotaData.getRotaModelMO(), isNotLoadConfig);
            }
            if (null != rotaData.getUnitTasks() && rotaData.getUnitTasks().size() != 0) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertTaskTable(rotaData.getUnitTasks(), 0, 1, isNotLoadConfig);
            }
            if (null != rotaData.getRotaTasks() && rotaData.getRotaTasks().size() != 0) {
                IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertRotaTaskTable(rotaData.getRotaTasks(), isNotLoadConfig);
            }

            if (null != rotaData.getUnitMonthlyBillDetail() && rotaData.getUnitMonthlyBillDetail().size() != 0) {
                IOPSApplication.getInstance().getInsertionInstance().insertBillCollactionApiTable(rotaData.getUnitMonthlyBillDetail());
            }
        }
    }
}
