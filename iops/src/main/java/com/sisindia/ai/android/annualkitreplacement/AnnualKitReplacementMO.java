package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnnualKitReplacementMO {

@SerializedName("DueLastMonth")
@Expose
private Integer dueLastMonth;
@SerializedName("AddedThisMonth")
@Expose
private Integer addedThisMonth;
@SerializedName("DistributedThisMonth")
@Expose
private Integer distributedThisMonth;
@SerializedName("Pending")
@Expose
private Integer pending;

/**
* 
* @return
* The dueLastMonth
*/
public Integer getDueLastMonth() {
return dueLastMonth;
}

/**
* 
* @param dueLastMonth
* The DueLastMonth
*/
public void setDueLastMonth(Integer dueLastMonth) {
this.dueLastMonth = dueLastMonth;
}

/**
* 
* @return
* The addedThisMonth
*/
public Integer getAddedThisMonth() {
return addedThisMonth;
}

/**
* 
* @param addedThisMonth
* The AddedThisMonth
*/
public void setAddedThisMonth(Integer addedThisMonth) {
this.addedThisMonth = addedThisMonth;
}

/**
* 
* @return
* The distributedThisMonth
*/
public Integer getDistributedThisMonth() {
return distributedThisMonth;
}

/**
* 
* @param distributedThisMonth
* The DistributedThisMonth
*/
public void setDistributedThisMonth(Integer distributedThisMonth) {
this.distributedThisMonth = distributedThisMonth;
}

/**
* 
* @return
* The pending
*/
public Integer getPending() {
return pending;
}

/**
* 
* @param pending
* The Pending
*/
public void setPending(Integer pending) {
this.pending = pending;
}

}