package com.sisindia.ai.android.login;

/**
 * Created by Saruchi on 12-04-2016.
 */
public interface OnSMSReceivedListener {

    void onSMSReceived(String otp);
}
