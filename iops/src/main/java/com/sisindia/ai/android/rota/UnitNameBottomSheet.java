package com.sisindia.ai.android.rota;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitDetails;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.utils.bottomsheet.BottomSheetAdapter;
import com.sisindia.ai.android.utils.bottomsheet.OnBottomSheetItemClickListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 28/11/16.
 */

public class UnitNameBottomSheet extends BottomSheetDialogFragment
        implements OnRecyclerViewItemClickListener {
    private OnBottomSheetItemClickListener onBottomSheetItemClickListener;
    private List<MyUnitHomeMO> bottomSheetList;
    private RecyclerView bottomSheetRecycleview;
    private UnitDetailsBottomSheetAdapter bottomSheetAdapter;
    private ArrayList<String> elements;
    private Resources resources;
    private int viewId;
    private BottomSheetBehavior<LinearLayout> bottomSheetBehaviorNew;
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }
        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.onBottomSheetItemClickListener = (OnBottomSheetItemClickListener) context;
    }

    public void setBottomSheetData(List<MyUnitHomeMO> bottomSheetList, int viewId) {
        this.bottomSheetList = bottomSheetList;
        this.viewId = viewId;
    }

    public void setSheetItemClickListener(OnBottomSheetItemClickListener onBottomSheetItemClickListener) {
        this.onBottomSheetItemClickListener = onBottomSheetItemClickListener;
    }
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
        dialog.setContentView(contentView);
        bottomSheetRecycleview = (RecyclerView) contentView.findViewById(R.id.bottom_sheet_recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        bottomSheetRecycleview.setLayoutManager(linearLayoutManager);
        bottomSheetRecycleview.setItemAnimator(new DefaultItemAnimator());
        bottomSheetAdapter = new UnitDetailsBottomSheetAdapter(getActivity());
        bottomSheetAdapter.setBottomSheetData(bottomSheetList);
        bottomSheetRecycleview.setAdapter(bottomSheetAdapter);
        bottomSheetAdapter.setOnRecyclerViewItemClickListener(this);
        showBottomSheet(contentView);
    }
    void showBottomSheet(View view) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        final CoordinatorLayout.Behavior bottomSheetBehavior = params.getBehavior();
        bottomSheetBehaviorNew = (BottomSheetBehavior) bottomSheetBehavior;
        if (bottomSheetBehavior != null && bottomSheetBehavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) bottomSheetBehavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            ((BottomSheetBehavior) bottomSheetBehavior).setPeekHeight(Util.dpToPx(250));
        }
        LinearLayout bottomSheetViewgroup = (LinearLayout) view.findViewById(R.id.bottom_sheet_layout_fragment);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        onBottomSheetItemClickListener.onUnitSheetItemClick(bottomSheetList.get(position), viewId);
        dismiss();
    }
}
