package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskQuestionOptionMappingMO {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("SecurityRiskQuestionId")
    @Expose
    private Integer securityRiskQuestionId;
    @SerializedName("SecurityRiskOptionId")
    @Expose
    private Integer securityRiskOptionId;
    @SerializedName("SecurityRiskOptionName")
    @Expose
    private String securityRiskOptionName;
    @SerializedName("IsMandatory")
    @Expose
    private Boolean isMandatory;
    @SerializedName("ControlType")
    @Expose
    private String controlType;
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The securityRiskQuestionId
     */
    public Integer getSecurityRiskQuestionId() {
        return securityRiskQuestionId;
    }

    /**
     *
     * @param securityRiskQuestionId
     * The SecurityRiskQuestionId
     */
    public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
        this.securityRiskQuestionId = securityRiskQuestionId;
    }

    /**
     *
     * @return
     * The securityRiskOptionId
     */
    public Integer getSecurityRiskOptionId() {
        return securityRiskOptionId;
    }

    /**
     *
     * @param securityRiskOptionId
     * The SecurityRiskOptionId
     */
    public void setSecurityRiskOptionId(Integer securityRiskOptionId) {
        this.securityRiskOptionId = securityRiskOptionId;
    }

    /**
     *
     * @return
     * The securityRiskOptionName
     */
    public String getSecurityRiskOptionName() {
        return securityRiskOptionName;
    }

    /**
     *
     * @param securityRiskOptionName
     * The SecurityRiskOptionName
     */
    public void setSecurityRiskOptionName(String securityRiskOptionName) {
        this.securityRiskOptionName = securityRiskOptionName;
    }

    /**
     *
     * @return
     * The isMandatory
     */
    public Boolean getIsMandatory() {
        return isMandatory;
    }

    /**
     *
     * @param isMandatory
     * The IsMandatory
     */
    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    /**
     *
     * @return
     * The controlType
     */
    public String getControlType() {
        return controlType;
    }

    /**
     *
     * @param controlType
     * The ControlType
     */
    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    /**
     *
     * @return
     * The isActive
     */
    public boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

}