package com.sisindia.ai.android.myunits.models;

/**
 * Created by Shushrut on 13-05-2016.
 */
public class EquipmentListModel {
    int unitEquipmentId;
    int uniqueNumber;
    int manufacturer;
    int isCustomerProvided;
    int equipmentStatus;
    int equipmentType;
    String equipmentName;
    int unit_id;

    public int getUnitEquipmentId() {
        return unitEquipmentId;
    }

    public void setUnitEquipmentId(int unitEquipmentId) {
        this.unitEquipmentId = unitEquipmentId;
    }

    public int getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(int uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public int getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(int manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getIsCustomerProvided() {
        return isCustomerProvided;
    }

    public void setIsCustomerProvided(int isCustomerProvided) {
        this.isCustomerProvided = isCustomerProvided;
    }

    public int getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(int equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

    public int getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(int equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(int unit_id) {
        this.unit_id = unit_id;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }
}
