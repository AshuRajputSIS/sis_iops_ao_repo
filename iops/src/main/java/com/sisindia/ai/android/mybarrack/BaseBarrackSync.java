package com.sisindia.ai.android.mybarrack;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.annualkitreplacement.FCMAkrDistributionData;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;

import java.util.List;

/**
 * Created by compass on 6/12/2017.
 */

class BaseBarrackSync {


    @SerializedName("StatusCode")
    private Integer statusCode;
    @SerializedName("StatusMessage")
    private String statusMessage;
    @SerializedName("Data")
    public List<Barrack> fcmBarrackData;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<Barrack> getFcmBarrackData() {
        return fcmBarrackData;
    }

    public void setFcmBarrackData(List<Barrack> fcmBarrackData) {
        this.fcmBarrackData = fcmBarrackData;
    }

    @Override
    public String toString() {
        return "BaseBarrackSync{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", fcmBarrackData=" + fcmBarrackData +
                '}';
    }
}
