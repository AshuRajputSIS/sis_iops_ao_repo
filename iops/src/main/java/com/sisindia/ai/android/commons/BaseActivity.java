package com.sisindia.ai.android.commons;

/**
 * Created by Ashu Rajput
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.login.OnAcceptPermissionListener;
import com.sisindia.ai.android.login.PermissionDialogFragment;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.rota.daycheck.taskExecution.DayNightChecking;
import com.sisindia.ai.android.rota.daycheck.taskExecution.DayNightCheckingBaseMO;
import com.sisindia.ai.android.utils.Util;

import java.util.Locale;

import timber.log.Timber;

public class BaseActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult>,
        OnAcceptPermissionListener {

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final String TAG = "BaseActivity";
    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    // Keys for storing activity state in the Bundle.
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    protected final static int TIME_SETTING_REQUEST_CODE = 3;
    protected final static int ADD_COMPLAINT_REQUEST_CODE = 4;
    private LocationManager locationManager;
    /**
     * Represents a geographical location.
     */
    public static Location mCurrentLocation;
    private static BaseActivity instance;
    public NetworkChangeReceiver networkCheck;
    public BaseActivity mActivity;
    public Snackbar mSnackbar;
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;
    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;
    private Context mContext;
    private boolean mResolvingError;
    public SISClient sisClient;
    protected DayNightCheckingBaseMO dayNightCheckingBaseMO;
    protected DayNightChecking dayNightChecking;
    private boolean isCollectionAPICalled = false;
    protected Util util;
    public AppPreferences appPreferences;
    private PermissionDialogFragment permissionDialogFragment;

    public static BaseActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        mActivity = this;
        mContext = this;
        util = new Util();
        appPreferences = new AppPreferences(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        initialiseData();
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and onDutyChange the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and onDutyChange the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast onDutyChange
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(BaseActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        if (permissionDialogFragment != null) {
                            hideGpsDialog();
                        }
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        //checkLocationSettings();


                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }

              /*  break;
            case TIME_SETTING_REQUEST_CODE:
                //   checkTimeSettings();
                break;
            case ADD_COMPLAINT_REQUEST_CODE:
                break;*/
        }
    }

    public void showGpsDialog(String message) {
        permissionDialogFragment = new PermissionDialogFragment();
        permissionDialogFragment.setMessage(message, true);
        permissionDialogFragment.setHeaderTitle(getString(R.string.PermissionGpsTitle));
        permissionDialogFragment.show(getSupportFragmentManager(), "");
    }

    public void hideGpsDialog() {
        if (permissionDialogFragment != null && permissionDialogFragment.isVisible()) {
            permissionDialogFragment.dismiss();
        }
    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
                // permission has been granted, continue as usual
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient,
                        mLocationRequest,
                        this
                ).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        mRequestingLocationUpdates = true;
                    }
                });
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    private void initialiseData() {
        sisClient = new SISClient(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupLanguage();
        boolean flag = Util.isautomaticTimeChaged(this);
        if (flag) {
            showAlertDialog(this);
        }


        try {
            networkCheck = new NetworkChangeReceiver(mActivity);
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkCheck, filter);

        } catch (Exception e) {
            finish();
        }
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }

    }

    private void setupLanguage() {
        int languageId = appPreferences.getLanguageId();
        String localeString = Locale.getDefault().getLanguage();

        if (languageId != 0) {
            switch (languageId) {
                case Constants.ENGLISH_LANGUAGE_ID:
                    break;
                case Constants.HINDI_LANGUAGE_ID:
                    if (!Constants.LOCALE_HINDI_INDENTIFIER.equalsIgnoreCase(localeString)) {
                        setLocale(Constants.LOCALE_HINDI_INDENTIFIER, Constants.HINDI_LANGUAGE_ID);
                    }
                    break;
            }
        } else {
            Timber.d(TAG + "%s", "onResume: Language is not selected yet");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        try {
            if (networkCheck != null)
                unregisterReceiver(networkCheck);
        } catch (Exception e) {

        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Util.dismissProgressBar();
    }

    public void snackBarWithMesg(View view, String mesg) {

        mSnackbar = Snackbar.make(view, mesg, Snackbar.LENGTH_LONG);
        snackBarColor();
        mSnackbar.show();
    }

    public void snackBarWithDuration(View view, String mesg, int duration) {

        mSnackbar = Snackbar.make(view, mesg, duration);
        snackBarColor();
        mSnackbar.show();
    }

    private void snackBarColor() {
        View view = mSnackbar.getView();
        mSnackbar.setActionTextColor(Color.WHITE);
        view.setBackgroundResource(R.color.colorPrimary);
    }

    protected void initializeToolbar(String title, boolean backBtn) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(backBtn);
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        createLocationRequest();
        buildLocationSettingsRequest();
        //checkLocationSettings();
        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGpsDialog(getString(R.string.gps_configure_msg));
        } else {
            hideGpsDialog();
        }
        Log.i(TAG, "Connected to GoogleApiClient");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            // permission has been granted, continue as usual
            mCurrentLocation =LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            updateUI(mCurrentLocation);
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {

    }

    public void updateUI(Location mCurrentLocation) {
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            // showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    void showAlertDialog(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle(getResources().getString(R.string.time_settings));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getResources().getString(R.string.time_settings_msg));
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                Intent dateSystemIntent = new Intent(Settings.ACTION_DATE_SETTINGS);
                dateSystemIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(dateSystemIntent, TIME_SETTING_REQUEST_CODE);
            }
        });
        alertDialog.show();
    }

    public void startGpsTracking() {
        Intent intent = new Intent(this, GpsTrackingService.class);
        startService(intent);
    }

    /*
    * The below method is  used for setting
    * the toolbar in activity.
    *
    * include Toolbar in the Activity layout
    * before calling this method
    *
    * @param1 toolbar -- pass this parameter(toolbar object) from ChildActivity
    * @param2 title   -- Toolbar title
    *
    *
    * */

    protected void setBaseToolbar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(title);
        }
    }

    protected void showFragmentDialog(int layout) {
        CustomDialogFragment customDialogFragment = CustomDialogFragment.newInstance();

        if (customDialogFragment != null) {
            customDialogFragment.setLayout(layout);
            customDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    protected void showFragmentDialog(int layout, String messgae) {
        CustomDialogFragment customDialogFragment = CustomDialogFragment.newInstance();

        if (customDialogFragment != null) {
            customDialogFragment.setMessage(messgae);
            customDialogFragment.setLayout(layout);
            customDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    protected void showConfirmationDialog(int layout) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance();
        Util.refreshCheckedGuardIdHolder();
        if (confirmationDialogFragment != null) {
            confirmationDialogFragment.setLayout(layout);
            confirmationDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    protected void showConfirmationDialogForNFCCheck(int layout) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance();
        if (confirmationDialogFragment != null) {
            confirmationDialogFragment.setLayout(layout);
            confirmationDialogFragment.show(getSupportFragmentManager(), "");
        }
    }

    protected void setLocale(String localeString, int languageId) {
        Locale locale = new Locale(localeString);
        if (languageId == Constants.HINDI_LANGUAGE_ID) {
            locale.setDefault(locale);
            Configuration configuration = getApplicationContext().getResources().getConfiguration();
            configuration.locale = locale;
            getApplicationContext().getResources().updateConfiguration(configuration,
                    getApplicationContext().getResources().getDisplayMetrics());
        }

    }

    @Override
    public void onPermissionsAccepted() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, REQUEST_CHECK_SETTINGS);
    }
}