package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 12-07-2016.
 */

public class UnitContactOR {
    @SerializedName("UnitContactId")
    @Expose
    private Integer unitContactId;

    /**
     *
     * @return
     * The unitContactId
     */
    public Integer getUnitContactId() {
        return unitContactId;
    }

    /**
     *
     * @param unitContactId
     * The UnitContactId
     */
    public void setUnitContactId(Integer unitContactId) {
        this.unitContactId = unitContactId;
    }

    @Override
    public String toString() {
        return "UnitContactOR{" +
                "unitContactId=" + unitContactId +
                '}';
    }
}
