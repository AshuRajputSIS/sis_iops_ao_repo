package com.sisindia.ai.android.fcmnotification.job;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;
import com.sisindia.ai.android.commons.IOPSApplication;

/**
 * Created by shankar on 10/11/16.
 */

public class MyJobService extends FrameworkJobSchedulerService {
    @NonNull
    @Override
    protected JobManager getJobManager() {
        return IOPSApplication.getInstance().getJobManager();
    }
}
