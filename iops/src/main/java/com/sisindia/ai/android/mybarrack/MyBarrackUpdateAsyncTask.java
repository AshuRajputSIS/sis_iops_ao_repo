package com.sisindia.ai.android.mybarrack;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.DeletionStatementDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.mybarrack.modelObjects.MyBarrack;
import com.sisindia.ai.android.mybarrack.modelObjects.MyBarrackBaseMo;
import com.sisindia.ai.android.utils.Util;

import timber.log.Timber;

/**
 * Created by compass on 6/12/2017.
 */

public class MyBarrackUpdateAsyncTask extends AsyncTask<String, String, String> {

    private final BarrackSyncListener barrackSyncListener;
    private Context mContext;
    private MyBarrackBaseMo myBarrackBaseMo;


    public MyBarrackUpdateAsyncTask(Context context, MyBarrackBaseMo myBarrackBaseMo,
                                    BarrackSyncListener barrackSyncListener) {
        this.mContext = context;
        this.barrackSyncListener = barrackSyncListener;
        this.myBarrackBaseMo = myBarrackBaseMo;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        barrackSyncListener.barrackSyncListener(true);
    }

    @Override
    protected String doInBackground(String... params) {
        SQLiteDatabase sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance()
                .getWritableDatabase();
        sqlite.beginTransaction();
        boolean isBarrackRowsDeleted = barrackTableDeletion(mContext,
                TableNameAndColumnStatement.BARRACK_TABLE,
                sqlite);

        if (isBarrackRowsDeleted) {
            boolean isInsertSuccess = insertBarrackTable(sqlite);
            if (isInsertSuccess) {
                sqlite.setTransactionSuccessful();
            } else {
                Timber.e("Error While Inserting barrack tables");
            }

        } else {
            Timber.e("Error While Deleting barrack tables");
        }

        sqlite.endTransaction();
        closeDb(sqlite);

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        hideprogressbar(true);
        barrackSyncListener.barrackSyncListener(true);
    }


    private void hideprogressbar(boolean isCompleted) {
        Util.dismissProgressBar(isCompleted);
    }

    public boolean insertBarrackTable(SQLiteDatabase sqlite) {
        boolean isInsertedSuccessfully = false;
        synchronized (sqlite) {
            try {
                ContentValues values = new ContentValues();
                if (myBarrackBaseMo != null && myBarrackBaseMo.data != null
                        && myBarrackBaseMo.data.size() != 0) {
                    for (MyBarrack barrack : myBarrackBaseMo.data) {
                        values.put(TableNameAndColumnStatement.BARRACK_ID, barrack.id);
                        values.put(TableNameAndColumnStatement.BARRACK_NAME, barrack.name);
                        values.put(TableNameAndColumnStatement.DESCRIPTIONS, barrack.description);
                        values.put(TableNameAndColumnStatement.IS_MESS_AVAILABLE, barrack.isMessAvailable);
                        values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, barrack.messVendorId);
                        values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, barrack.isCustomerProvided);
                        values.put(TableNameAndColumnStatement.BARRACK_FULL_ADDRESS, barrack.barrackAddress);
                        values.put(TableNameAndColumnStatement.BARRACK_OWNER_NAME, barrack.ownerContactName);
                        values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, barrack.areaInspectorName);
                        values.put(TableNameAndColumnStatement.BARRACK_INCHARGE_NAME, barrack.inChargeName);
                        values.put(TableNameAndColumnStatement.BRANCH_ID, barrack.branchId);
                        values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                        values.put(TableNameAndColumnStatement.GEO_LATITUDE, barrack.geoLatitude);
                        values.put(TableNameAndColumnStatement.GEO_LONGITUDE, barrack.geoLongitude);
                        if (barrack.DeviceNo == null)
                            values.put(TableNameAndColumnStatement.DEVICE_NO, "");
                        else
                            values.put(TableNameAndColumnStatement.DEVICE_NO, barrack.DeviceNo);
                        sqlite.insert(TableNameAndColumnStatement.BARRACK_TABLE, null, values);
                        isInsertedSuccessfully = true;
                    }
                }
            } catch (Exception exception) {
                isInsertedSuccessfully = false;
                Crashlytics.logException(exception);
            }

        }
        return isInsertedSuccessfully;
    }

    private static synchronized boolean barrackTableDeletion(Context mContext,
                                                             String tableName,
                                                             SQLiteDatabase sqlite) {
        DeletionStatementDB deletionStatementDB = new DeletionStatementDB(mContext);
        boolean isDeletionSuccess = true;
        if (!deletionStatementDB.IsTableDeleted(tableName, sqlite)) {
            isDeletionSuccess = false;
        }
        return isDeletionSuccess;
    }

    private static void closeDb(SQLiteDatabase sqlite) {
        if (null != sqlite && sqlite.isOpen()) {
            sqlite.close();
        }
    }

}