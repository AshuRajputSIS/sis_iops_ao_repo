package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

public class KitItemModel {

@SerializedName("Id")
private Integer id;
@SerializedName("KitItemCode")
private String kitItemCode;
@SerializedName("Name")
private String name;
@SerializedName("Description")
private String description;
@SerializedName("IsActive")
private Boolean isActive;
    private Integer kitDistributionId;
    private int kitDistributionItemId;
    private Integer isIssued;
    private String kitSizeName;


    /**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The kitItemCode
*/
public String getKitItemCode() {
return kitItemCode;
}

/**
* 
* @param kitItemCode
* The KitItemCode
*/
public void setKitItemCode(String kitItemCode) {
this.kitItemCode = kitItemCode;
}

/**
* 
* @return
* The name
*/
public String getName() {
return name;
}

/**
* 
* @param name
* The Name
*/
public void setName(String name) {
this.name = name;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The Description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The isActive
*/
public Boolean IsActive() {
return isActive;
}

/**
* 
* @param isActive
* The IsActive
*/
public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

    public void setKitDistributionId(Integer kitDistributionId) {
        this.kitDistributionId = kitDistributionId;
    }

    public Integer getKitDistributionId() {
        return kitDistributionId;
    }


    public void setKitDistributionItemId(int kitDistributionItemId) {
        this.kitDistributionItemId = kitDistributionItemId;
    }

    public int getKitDistributionItemId() {
        return kitDistributionItemId;
    }

    public void setIsIssued(Integer isIssued) {
        this.isIssued = isIssued;
    }

    public Integer getIsIssued() {
       return isIssued;
    }

    public void setKitSizeName(String kitSizeName) {
        this.kitSizeName = kitSizeName;
    }

    public String getKitSizeName() {
        return kitSizeName;
    }
}