package com.sisindia.ai.android.rota.daycheck;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.daycheck.taskExecution.ClientRegisterCheck;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;

public class ClientRegisterCheckActivity extends DayCheckBaseActivity implements View.OnClickListener, DialogClickListener
        , RadioGroup.OnCheckedChangeListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.availableAtPost)
    View availableAtPostView;
    @Bind(R.id.uptoDate)
    View upToDateView;
    TextView availableAtPostTextView;
    TextView uptoDateTextView;
    @Bind(R.id.horizontal_photos)
    View clientRegisterPhotosLayout;
    TextView dutyRegisterPhotosHeaderTextview;
    @Bind(R.id.radioButtonNA)
    RadioButton radioButtonNA;
    @Bind(R.id.radioButtonNotPermitted)
    RadioButton radioButtonNotPermitted;
    @Bind(R.id.dayCheckHeader)
    View mStepperViewParent;
    @Bind(R.id.lastpages)
    CustomFontTextview lastpages;
    @Bind(R.id.clientRegisterCheckParentLayout)
    LinearLayout clientRegisterCheckParentLayout;
    private RadioButton uptoDateRadioButtonYes;
    private RadioButton availableAtPostRadioButtonYes;
    private ArrayList<View> viewList;
    private TextView clientRegisterPhotosHeaderTextview;
    private ImageView clientRegisterPicImageView;
    private ImageView materialRegisterImageView;
    private ImageView vehicleRegisterImageView;
    private ImageView maintenanceRegisterImageView;
    private ClientRegisterCheck clientRegisterCheck;
    private RadioGroup availableAtPostRadioGroup;
    private RadioGroup uptoDateRadioGroup;
    private boolean isTakenAtleastOnepic;
    private ArrayList<AttachmentMetaDataModel> attachmentMetaArrayList = new ArrayList<>();
    private Resources mResources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_register_check);
        ButterKnife.bind(this);
        mResources = getResources();
        setBaseToolbar(toolbar, getResources().getString(R.string.dayCheckingTitle));
        setUiViews();
        if (rotaTaskModel != null) {
            setHeaderUnitName(rotaTaskModel.getUnitName());
        }
        getStepperViews(mStepperViewParent);
        getTaskExecutionResult();
        clientRegisterCheck = new ClientRegisterCheck();
        clientRegisterCheck.setAvailableAtPost(getResources().getString(R.string.YES));
        clientRegisterCheck.setUptoDate(getResources().getString(R.string.YES));
    }

    private void setUiViews() {
        availableAtPostTextView = (TextView) availableAtPostView.findViewById(R.id.radioGroupHeader);
        uptoDateTextView = (TextView) upToDateView.findViewById(R.id.radioGroupHeader);
        uptoDateRadioButtonYes = (RadioButton) upToDateView.findViewById(R.id.radioButtonYes);
        availableAtPostRadioButtonYes = (RadioButton) availableAtPostView.findViewById(R.id.radioButtonYes);
        uptoDateRadioButtonYes.setChecked(true);
        availableAtPostRadioButtonYes.setChecked(true);
        availableAtPostRadioGroup = (RadioGroup) availableAtPostView.findViewById(R.id.radioGroup);
        uptoDateRadioGroup = (RadioGroup) upToDateView.findViewById(R.id.radioGroup);
        availableAtPostRadioGroup.setOnCheckedChangeListener(this);
        uptoDateRadioGroup.setOnCheckedChangeListener(this);
        clientRegisterPhotosHeaderTextview = (TextView) clientRegisterPhotosLayout.findViewById(R.id.photosHeaderTv);
        clientRegisterPhotosHeaderTextview.setText(getResources().getString(R.string.PHOTOS_OF_CLIENT_REGISTER_HEADING));
        lastpages.setText(getResources().getString(R.string.lastTwoPages));
        clientRegisterPicImageView = (ImageView) clientRegisterPhotosLayout.findViewById(R.id.defaultCameraPicIv);
        clientRegisterPicImageView.setTag(getResources().getString(R.string.photos));
        clientRegisterPicImageView.setOnClickListener(this);
        if (radioButtonNA != null && radioButtonNA.getVisibility() == View.GONE) {
            radioButtonNA.setVisibility(View.VISIBLE);
        }
        if (radioButtonNotPermitted != null && radioButtonNotPermitted.getVisibility() == View.GONE) {
            radioButtonNotPermitted.setVisibility(View.VISIBLE);
        }
        materialRegisterImageView = (ImageView) clientRegisterPhotosLayout.findViewById(R.id.materialImage);
        vehicleRegisterImageView = (ImageView) clientRegisterPhotosLayout.findViewById(R.id.vehicleImage);
        maintenanceRegisterImageView = (ImageView) clientRegisterPhotosLayout.findViewById(R.id.maintenanceImage);
        materialRegisterImageView.setOnClickListener(this);
        vehicleRegisterImageView.setOnClickListener(this);
        maintenanceRegisterImageView.setOnClickListener(this);
        availableAtPostTextView.setText(getResources().getString(R.string.AVAILABLE_AT_POST));
        uptoDateTextView.setText(getResources().getString(R.string.UPTO_DATE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_duty_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done_menu) {
            if (clientRegisterCheck.getAvailableAtPost().equalsIgnoreCase(getResources().getString(R.string.YES))) {
                if (checkImageCapturedOrNot()) {
                    updateClientRegisteration();
                } else {
                    snackBarWithMesg(clientRegisterCheckParentLayout, getResources().getString(R.string.valiadtion_msg));
                }
            } else {
                updateClientRegisteration();
            }

            dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
            return true;
        } else if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkImageCapturedOrNot() {

        if ((materialRegisterImageView.getDrawable().getConstantState() ==
                getResources().getDrawable(R.drawable.camera_pic).getConstantState()) &&
                (vehicleRegisterImageView.getDrawable().getConstantState() ==
                        getResources().getDrawable(R.drawable.camera_pic).getConstantState()) &&
                (maintenanceRegisterImageView.getDrawable().getConstantState()
                        == getResources().getDrawable(R.drawable.camera_pic).getConstantState()) &&
                (clientRegisterPicImageView.getDrawable().getConstantState() ==
                        getResources().getDrawable(R.drawable.camera_pic).getConstantState())) {
            isTakenAtleastOnepic = false;
        } else {
            isTakenAtleastOnepic = true;
        }
        return isTakenAtleastOnepic;
    }

    private void updateClientRegisteration() {
        OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();
        mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(4).name(),DayCheckEnum.Client_Register_Check.getValue());
        dayNightChecking.setClientRegisterCheck(clientRegisterCheck);
        setTaskExecutionResult();
        this.finish();
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        Intent capturePictureActivity = null;
        Util.mSequenceNumber++;
        switch (viewId) {
            case R.id.defaultCameraPicIv:
                capturePictureActivity = new Intent(ClientRegisterCheckActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.VISITOR_REGISTER_CHECK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                startActivityForResult(capturePictureActivity, getResources().getInteger(R.integer.VISITOR_IMAGE_CODE));
                break;
            case R.id.materialImage:
                capturePictureActivity = new Intent(ClientRegisterCheckActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.MATERIAL_REGISTER_CHECK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                startActivityForResult(capturePictureActivity, getResources().getInteger(R.integer.MATERIAL_IMAGE_CODE));
                break;
            case R.id.vehicleImage:
                capturePictureActivity = new Intent(ClientRegisterCheckActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.VEHICLE_REGISTER_CHECK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                startActivityForResult(capturePictureActivity, getResources().getInteger(R.integer.VEHICLE_IMAGE_CODE));
                break;
            case R.id.maintenanceImage:
                capturePictureActivity = new Intent(ClientRegisterCheckActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.MAINTENANCE_REGISTER_CHECK);
                Util.initMetaDataArray(true);
                capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
                capturePictureActivity.putExtra(Constants.CHECKING_TYPE, getResources().getString(R.string.CHECKING_TYPE_DAY_CHECKING));
                startActivityForResult(capturePictureActivity, getResources().getInteger(R.integer.MAINTENANCE_IMAGE_CODE));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
            if (requestCode == getResources().getInteger(R.integer.MATERIAL_IMAGE_CODE)) {
                materialRegisterImageView.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));

            } else if (requestCode == getResources().getInteger(R.integer.VEHICLE_IMAGE_CODE)) {
                vehicleRegisterImageView.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            } else if (requestCode == getResources().getInteger(R.integer.MAINTENANCE_IMAGE_CODE)) {
                maintenanceRegisterImageView.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            } else {
                clientRegisterPicImageView.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            }

        }
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        if(clickType == 0){

            mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(4).name(),DayCheckEnum.Client_Register_Check.getValue());
            finish();
        }
        else{
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        String selectedData = ((RadioButton) findViewById(checkedId)).getText().toString();
        if (availableAtPostRadioGroup == group) {
            clientRegisterCheck.setAvailableAtPost(selectedData);
        } else if (uptoDateRadioGroup == group) {
            clientRegisterCheck.setUptoDate(selectedData);
        }
    }
}
