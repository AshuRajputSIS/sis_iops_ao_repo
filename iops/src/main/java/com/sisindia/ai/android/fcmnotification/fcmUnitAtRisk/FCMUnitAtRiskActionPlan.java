package com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.ActionPlanMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.RiskDetailModelMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskCountMO;

import java.util.List;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMUnitAtRiskActionPlan extends SISAITrackingDB {

    private SQLiteDatabase sqlite;
    private boolean isInsertedSuccessfully;

    public FCMUnitAtRiskActionPlan(Context context) {
        super(context);
    }
    public boolean UnitAtRiskActionPlan(List<RiskDetailModelMO> riskDetailModelMOList) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {
                sqlite.beginTransaction();
                UnitAtRiskActionPlanModel(riskDetailModelMOList);

                sqlite.setTransactionSuccessful();
                isInsertedSuccessfully = true;
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
            return isInsertedSuccessfully;
        }
    }

    private void UnitAtRiskActionPlanModel(List<RiskDetailModelMO> riskDetailModelMOList) {
        ContentValues unitRiskValue = new ContentValues();
        ContentValues actionPlanValue = new ContentValues();
        for (RiskDetailModelMO reRiskDetailModelMO: riskDetailModelMOList
                ) {
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_RISK_ID,
                    reRiskDetailModelMO.getUnitRiskId());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_ID,
                    reRiskDetailModelMO.getUnitId());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_NAME,
                    reRiskDetailModelMO.getUnitName());
            unitRiskValue.put(TableNameAndColumnStatement.CURRENT_MONTH,
                    reRiskDetailModelMO.getCurrentMonth());
            unitRiskValue.put(TableNameAndColumnStatement.CURRENT_YEAR,
                    reRiskDetailModelMO.getCurrentYear());
            unitRiskValue.put(TableNameAndColumnStatement.UNIT_RISK_STATUS_ID,
                    reRiskDetailModelMO.getUnitRiskStatusId());
            unitRiskValue.put(TableNameAndColumnStatement.CLOSED_AT,
                    reRiskDetailModelMO.getCompletionDate());

           // UnitRiskCount is not coming
            /*unitRiskValue.put(TableNameAndColumnStatement.RISK_COUNT,
                    unitRiskCount.getRiskCount());
            unitRiskValue.put(TableNameAndColumnStatement.RISK_COUNT_BY_AI,
                    unitRiskCount.getRiskCountByAreaInspector());
            unitRiskValue.put(TableNameAndColumnStatement.ACTION_COUNT,
                    unitRiskCount.getActionCount());
            unitRiskValue.put(TableNameAndColumnStatement.ACTION_PENDING_COUNT,
                    unitRiskCount.getActionPendingCount());*/
            unitRiskValue.put(TableNameAndColumnStatement.TOTAL_ITEM_COUNT,
                    reRiskDetailModelMO.getTotalItems());
            for (ActionPlanMO actionPlanMO: reRiskDetailModelMO.getActionPlans()
                    ) {
                actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID,
                        actionPlanMO.getActionPlanId());
                actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ID,
                        reRiskDetailModelMO.getUnitRiskId());
                actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_REASON,
                        actionPlanMO.getReason());
                actionPlanValue.put(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN,
                        actionPlanMO.getActionPlan());
                actionPlanValue.put(TableNameAndColumnStatement.ACTION_PLAN_DATE,
                        actionPlanMO.getPlanDate());
                actionPlanValue.put(TableNameAndColumnStatement.ASSIGNED_ID,
                        actionPlanMO.getAssignTo());
                actionPlanValue.put(TableNameAndColumnStatement.ASSIGNED_TO,
                        actionPlanMO.getAssignToName());
                actionPlanValue.put(TableNameAndColumnStatement.COMPLETION_DATE,
                        actionPlanMO.getCompletedDate());
                actionPlanValue.put(TableNameAndColumnStatement.IS_COMPLETED,
                        actionPlanMO.getIsCompleted());
                actionPlanValue.put(TableNameAndColumnStatement.REMARKS,
                        actionPlanMO.getRemarks());
                actionPlanValue.put(TableNameAndColumnStatement.CLOSED_DATE,
                        actionPlanMO.getClosedDate());
                actionPlanValue.put(TableNameAndColumnStatement.IS_SYNCED,
                        1);
                sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE, null, actionPlanValue);
            }
            sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_RISK_TABLE, null, unitRiskValue);
        }
    }

}
