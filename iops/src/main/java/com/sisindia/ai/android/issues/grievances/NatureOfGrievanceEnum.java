package com.sisindia.ai.android.issues.grievances;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum NatureOfGrievanceEnum {

    Wage_Not_Done(1),
    Salary_Not_Received(2),
    Less_Salary(3),
    Unit_Change_Request(4),
    Leave_Not_Granted(5),
    Client_Misbehave(6),
    Poor_Barrack_Condition(7),
    Non_Cooperation_By_Others(8),
    Misbehave_By_Seniors(9),
    Others(10);

    private static Map map = new HashMap<>();

    static {
        for (NatureOfGrievanceEnum postStatusEM : NatureOfGrievanceEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    NatureOfGrievanceEnum(int value) {
        this.value = value;
    }

    public static NatureOfGrievanceEnum valueOf(int postStatus) {
        return (NatureOfGrievanceEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
