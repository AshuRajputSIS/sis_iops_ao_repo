package com.sisindia.ai.android.fcmnotification.fcmBillCheckList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FcmBaseBillCheckListMO {

@SerializedName("StatusCode")
@Expose
private Integer statusCode;
@SerializedName("StatusMessage")
@Expose
private String statusMessage;
@SerializedName("Data")
@Expose
private BillingCheckListData data;

public Integer getStatusCode() {
return statusCode;
}

public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

public String getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

public BillingCheckListData getData() {
return data;
}

public void setData(BillingCheckListData data) {
this.data = data;
}

}