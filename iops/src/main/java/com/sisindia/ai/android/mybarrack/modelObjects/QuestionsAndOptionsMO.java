package com.sisindia.ai.android.mybarrack.modelObjects;

/**
 * Created by shankar on 29/7/16.
 */

public class QuestionsAndOptionsMO {


    private Integer questionId;
    private Integer optionId;
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }
}
