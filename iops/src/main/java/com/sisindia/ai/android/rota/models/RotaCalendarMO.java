package com.sisindia.ai.android.rota.models;

/**
 * Created by shankar on 12/7/16.
 */

public class RotaCalendarMO {

    private int taskCount;
    private String calendarDate;
    private String estimatedStartDateTime;
    private int totalTime;


    public String getEstimatedStartDateTime() {
        return estimatedStartDateTime;
    }

    public void setEstimatedStartDateTime(String estimatedStartDateTime) {
        this.estimatedStartDateTime = estimatedStartDateTime;
    }

    public String getCalendarDate() {
        return calendarDate;
    }

    public void setCalendarDate(String calendarDate) {
        this.calendarDate = calendarDate;
    }



    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }





}
