package com.sisindia.ai.android.fcmnotification;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.network.SISClient;

import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by shankar on 10/11/16.
 */

public class IOPSFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = IOPSFirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        System.out.println("refreshedToken :"+refreshedToken);

        // Saving reg id to shared preferences
        AppPreferences maAppPreferences = new AppPreferences(this);
        maAppPreferences.setFCMRegToken(refreshedToken);
        if (!TextUtils.isEmpty(maAppPreferences.getAccessToken())) {
            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
            sendRegistrationToServer(refreshedToken);
            // Notify UI that registration has completed, so the progress indicator can be hidden.
            Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
            registrationComplete.putExtra("token", refreshedToken);
            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        }
    }
// [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        SISClient sisClient = new SISClient(this);

        FCMTokenRegistration fcmTokenRegistration = new FCMTokenRegistration();
        fcmTokenRegistration.setUniqueDeviceId(token);

        sisClient.getApi().updateFCMToken(fcmTokenRegistration, new Callback<FCMTokenUpdateMO>() {
            @Override
            public void success(FCMTokenUpdateMO fcmTokenUpdateMO, Response response) {
                if (null != fcmTokenUpdateMO) {
                    switch (fcmTokenUpdateMO.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                           // Toast.makeText(getApplicationContext(), getString(R.string.FCM_TOKEN_UPDATE), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Crashlytics.logException(error);
            }
        });
    }
}

