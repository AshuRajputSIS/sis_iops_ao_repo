package com.sisindia.ai.android.issues.complaints;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 25-05-2016.
 */
public enum ModeOfComplaintEnum {

    Phone(1),
    Email(2),
    Verbal(3);


    private static Map map = new HashMap<>();

    static {
        for (ModeOfComplaintEnum modeOfComplaintEm : ModeOfComplaintEnum.values()) {
            map.put(modeOfComplaintEm.value, modeOfComplaintEm);
        }
    }

    private int value;

    ModeOfComplaintEnum(int value) {
        this.value = value;
    }

    public static ModeOfComplaintEnum valueOf(int postStatus) {
        return (ModeOfComplaintEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
