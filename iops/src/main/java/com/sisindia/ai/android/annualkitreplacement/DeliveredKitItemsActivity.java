package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.response.UnitEmployee;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.daycheck.SignatureActivity;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DeliveredKitItemsActivity extends DayCheckBaseActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tv_guard_name)
    CustomFontTextview tvGuardName;
    @Bind(R.id.unit_name)
    CustomFontTextview unitNameTv;

    @Bind(R.id.overAllTurnoutImage)
    RoundedCornerImageView kitReplaceImage;
    @Bind(R.id.add_signature_guard_level)
    RoundedCornerImageView addSignatureGuardLevel;
    @Bind(R.id.parent_layout)
    LinearLayout parentLayout;
    @Bind(R.id.replaceItemLayout)
    LinearLayout replacItemLayout;

    @Bind(R.id.guard_parent_layout)
    LinearLayout guardParentLayout;

    @Bind(R.id.tv_guard_name_label)
    CustomFontTextview tvGuardNameLabel;
    @Bind(R.id.unitNameOrBarrackNameLabel)
    CustomFontTextview unitNameOrBarrackNameLabel;
    @Bind(R.id.unPaidCheckBox)
    CheckBox unPaidcheckBox;
    @Bind(R.id.checkListItem)
    CustomFontTextview checkListItem;
    @Bind(R.id.non_issue_reason_id)
    CustomFontTextview nonIssueReasonId;
    @Bind(R.id.spinner_non_issue_reason)
    Spinner spinnerNonIssueReason;
    @Bind(R.id.unpaidLayout)
    LinearLayout unpaidLayout;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;

    @Bind(R.id.nonissue_reasonLayout)
    LinearLayout nonissueReasonLayout;
    private int unitId;
    private int employeeId;
    private String unitName;
    private int taskId;
    private String guardName;
    private String employeeNo;
    private ArrayList<KitItemModel> kitItemList;
    private Set<Integer> kitDistributedItemIdList;
    private Set<String> requestedItems;

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<UnitEmployee> arrayListGuardIds;
    private InputMethodManager imm;
    private boolean isSignatureMandatory;
    private boolean enableUnpaid;
    private ArrayList<LookupModel> lookupModelNonIssueReasons;
    private LookUpSpinnerAdapter nonIssueReasonadapter;
    private int kitTypeId;
    private boolean isselectedZeroPosition;
    private boolean hasDrawable;
    private int kitDistributionId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivered_kit_items);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.KIT_ITEM_REQUEST));
        kitDeliveredItemsIR = new KitDeliveredItemsIR();
        setupData();
        createCheckboxes(false);
    }


    private void setupData() {

        kitItemRequestMO = new KitItemRequestMO();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        kitDistributedItemIdList = new HashSet<>();
        requestedItems = new HashSet<>();

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                employeeId = intent.getIntExtra(TableNameAndColumnStatement.employee_id, 0);
                guardName = intent.getStringExtra(TableNameAndColumnStatement.guard_name);
                unitName = intent.getStringExtra(TableNameAndColumnStatement.unitName);
                unitId = intent.getIntExtra(TableNameAndColumnStatement.UNIT_ID, 0);
                kitTypeId = intent.getIntExtra(TableNameAndColumnStatement.KIT_TYPE_ID, 0);
                kitDistributionId = intent.getIntExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, 0);
            }
        }


        kitReplaceImage.setOnClickListener(this);
        addSignatureGuardLevel.setOnClickListener(this);


        //kitItemRequestMO.setRequestedItems();
        lookupModelNonIssueReasons = new ArrayList<>();
        LookupModel lookupModel = new LookupModel();
        lookupModel.setName(TableNameAndColumnStatement.select_non_issue_reason_id);
        lookupModelNonIssueReasons.add(lookupModel);
        ArrayList<LookupModel> nonIssuesReasonList = IOPSApplication.getInstance()
                .getSelectionStatementDBInstance().getLookupData(TableNameAndColumnStatement.KitNonIssueReason, null);
        if (nonIssuesReasonList != null && nonIssuesReasonList.size() != 0) {
            lookupModelNonIssueReasons.addAll(nonIssuesReasonList);
        }
        nonIssueReasonadapter = new LookUpSpinnerAdapter(this);
        nonIssueReasonadapter.setSpinnerData(lookupModelNonIssueReasons);
        if (spinnerNonIssueReason != null && spinnerNonIssueReason.getVisibility() == View.VISIBLE) {
            spinnerNonIssueReason.setAdapter(nonIssueReasonadapter);
            spinnerNonIssueReason.setOnItemSelectedListener(this);
        }


        unitNameTv.setText(unitName);
        tvGuardName.setText(guardName);
        unPaidcheckBox.setOnCheckedChangeListener(this);

    }

    private void createCheckboxes(boolean isChecked) {
        kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
        if (replacItemLayout.getChildCount() != 0) {
            replacItemLayout.removeAllViews();
        }
        if (unitId != 0 && employeeId != 0) {
            kitItemList = kitItemSelectionStatementDB.getKitDeliveredItemList(unitId, employeeId, kitDistributionId);
        }
        if (kitItemList != null && kitItemList.size() != 0) {
            for (int index = 0; index < kitItemList.size(); index++) {
                CheckBox customCheckbox = new CheckBox(this);
                customCheckbox.setText(kitItemList.get(index).getName());
                customCheckbox.setId(kitItemList.get(index).getKitDistributionItemId());
                customCheckbox.setChecked(isChecked);
                int marginBottom = Util.dpToPx(getResources().getInteger(R.integer.number_4));
                int marginLeft = Util.dpToPx(16);
                int marginRight = Util.dpToPx(16);
                int marginTop = Util.dpToPx(getResources().getInteger(R.integer.number_4));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                params.setMargins(marginLeft, marginTop, marginRight, marginBottom);
                customCheckbox.setLayoutParams(params);
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(customCheckbox);

                if (!TextUtils.isEmpty(kitItemList.get(index).getKitSizeName())) {
                    CustomFontTextview customFontTextview = new CustomFontTextview(this);
                    customFontTextview.setText("( " + kitItemList.get(index).getKitSizeName() + " )");
                    customFontTextview.setTextColor(getResources().getColor(R.color.black));
                    customFontTextview.setId(kitItemList.get(index).getKitDistributionItemId() +
                            kitItemList.get(index).getKitDistributionId());
                    LinearLayout.LayoutParams textViewparams = new LinearLayout.LayoutParams(
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT));
                    textViewparams.setMargins(marginLeft / 2, marginTop, marginRight, marginBottom);
                    customFontTextview.setLayoutParams(textViewparams);
                    linearLayout.addView(customFontTextview);
                }
                replacItemLayout.addView(linearLayout);
                customCheckbox.setOnCheckedChangeListener(this);
            }

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_kit_replace, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.kit_details_done) {
            validateFileds();

        }

        return super.onOptionsItemSelected(item);
    }

    private void validateFileds() {


        if (isselectedZeroPosition && kitDeliveredItemsIR.getIsUnPaid()) {
            snackBarWithMesg(guardParentLayout, getResources().getString(R.string.select_non_issue_reason));
        } else {

            if (kitDistributedItemIdList != null && kitDistributedItemIdList.size() != 0) {
                if (kitDistributedItemIdList.size() == kitItemList.size()) {
                    if (isSignatureMandatory) {
                        snackBarWithMesg(guardParentLayout, getResources().getString(R.string.Signature_Mandatory_Msg));
                        return;
                    }
                    List<Integer> kitDistributedItemIds = new ArrayList<>(kitDistributedItemIdList);
                    for (int index = 0; index < kitItemList.size(); index++) {
                        kitDeliveredItemsIR.setIssuedDate(dateTimeFormatConversionUtil.getCurrentDateTime());
                        kitDeliveredItemsIR.setIsIssued(true);
                        kitDeliveredItemsIR.setIsUnPaid(false);
                        kitDeliveredItemsIR.setNonIssueReasonId(0);
                        updateKitDistribution(index);
                        updateKitDistributionItem(kitDistributedItemIds.get(index));
                    }

                } else {
                    snackBarWithMesg(parentLayout, getResources().getString(R.string.select_non_issue_or_all_kit_item));
                    return;
                }
            } else if (unPaidcheckBox != null && unPaidcheckBox.isChecked()) {
                kitDeliveredItemsIR.setIsUnPaid(true);
                updateKitDistributionItemStatus(kitDeliveredItemsIR);

            } else {
                snackBarWithMesg(parentLayout, getResources().getString(R.string.select_non_issue_or_all_kit_item));
                return;
            }
            insertMetaData();
            triggerSyncAdapter();

            Intent intent = new Intent(DeliveredKitItemsActivity.this, AnnualKitReplacementTabsActivity.class);
            intent.putExtra(TableNameAndColumnStatement.UNIT_ID, unitId);
            intent.putExtra(TableNameAndColumnStatement.KIT_TYPE_ID, kitTypeId);
            intent.putExtra(TableNameAndColumnStatement.UNIT_NAME, unitName);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            //setResult(RESULT_OK,new Intent());
            finish();

        }
    }

    private void updateKitDistributionItemStatus(KitDeliveredItemsIR kitDeliveredItemsIR) {
        IOPSApplication.getInstance().getKitItemUpdateStatementDB().updateKitDistributionItemStatus(kitDeliveredItemsIR.getNonIssueReasonId(), kitDistributionId);
    }

    private synchronized void updateKitDistribution(int index) {
        IOPSApplication.getInstance().getKitItemUpdateStatementDB().
                updateKitDistributionStatus(kitItemList.get(index).getKitDistributionId().intValue()
                        , 3, dateTimeFormatConversionUtil.getCurrentDateTime());

    }

    private synchronized void updateKitDistributionItem(int kitDistributionItemId) {
        IOPSApplication.getInstance().getKitItemUpdateStatementDB().updateKitDistributionItemId(kitDistributionItemId, kitDeliveredItemsIR);
    }


    /*private void updateStatus(int kitDistributionId) {
        IOPSApplication.getInstance().getKitItemUpdateStatementDB().
                updateStatusOnAllKitDistributionItemsIssued(kitDistributionId,3,dateTimeFormatConversionUtil.getCurrentDateTime());
    }*/


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        int id = buttonView.getId();
        if (R.id.unPaidCheckBox == id) {
            if (isChecked) {
                createCheckboxes(false);
                kitDeliveredItemsIR.setIsUnPaid(!isChecked);
                nonissueReasonLayout.setVisibility(View.VISIBLE);
                isSignatureMandatory = false;
                isselectedZeroPosition = true;
                kitDeliveredItemsIR.setIssuedDate("");
                kitDeliveredItemsIR.setIsUnPaid(true);
                if (kitDistributedItemIdList != null) {
                    kitDistributedItemIdList.clear();
                }
            } else {
                unPaidcheckBox.setChecked(false);
                nonissueReasonLayout.setVisibility(View.GONE);
                isSignatureMandatory = true;
                isselectedZeroPosition = false;
                kitDeliveredItemsIR.setNonIssueReasonId(0);
                spinnerNonIssueReason.setSelection(0, false);
                kitDeliveredItemsIR.setIsUnPaid(false);
            }


        } else {
            kitDeliveredItemsIR.setIsUnPaid(false);
            unPaidcheckBox.setChecked(false);
            nonissueReasonLayout.setVisibility(View.GONE);
            if (hasDrawable)
                isSignatureMandatory = false;
            else
                isSignatureMandatory = true;
            if (isChecked) {
                kitDistributedItemIdList.add(buttonView.getId());
            } else {
                kitDistributedItemIdList.remove(buttonView.getId());
            }

        }

    }


    @Override
    public void onClick(View v) {

        Util.mSequenceNumber++;
        Util.mGuard_Id = employeeId;
        switch (v.getId()) {
            case R.id.add_signature_guard_level:

                Intent signatureActivity = new Intent(this, SignatureActivity.class);
                signatureActivity.putExtra(TableNameAndColumnStatement.employee_id, Util.mGuard_Id);
                signatureActivity.putExtra(TableNameAndColumnStatement.TASK_ID, taskId);
                signatureActivity.putExtra(TableNameAndColumnStatement.EMPLOYEE_NO, employeeNo);
                signatureActivity.putExtra(TableNameAndColumnStatement.UNIT_NAME, unitName);
                signatureActivity.putExtra(TableNameAndColumnStatement.UNIT_ID, unitId);
                signatureActivity.putExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionId);
                Util.setSendPhotoImageTo(Util.KIT_ITEM_SIGNATURE);
                signatureActivity.putExtra("SignatureCapture", true);
                startActivityForResult(signatureActivity, Util.CAPTURE_SIGNATURE);
                break;
            case R.id.overAllTurnoutImage:
                Intent capturePictureActivity = new Intent(DeliveredKitItemsActivity.this, CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                capturePictureActivity.putExtra(TableNameAndColumnStatement.employee_id, Util.mGuard_Id);
                capturePictureActivity.putExtra(TableNameAndColumnStatement.EMPLOYEE_NO, employeeNo);
                capturePictureActivity.putExtra(TableNameAndColumnStatement.UNIT_NAME, unitName);
                capturePictureActivity.putExtra(TableNameAndColumnStatement.UNIT_ID, unitId);
                capturePictureActivity.putExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitDistributionId);
                Util.setSendPhotoImageTo(Util.KIT_DISTRIBUTION_ITEM_IMAGE);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.KIT_DISTRIBUTION_IMAGE_REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (Util.CAPTURE_SIGNATURE == requestCode) {
                Bitmap bmp = data.getParcelableExtra(getResources().getString(R.string.signature));
                addSignatureGuardLevel.setImageBitmap(Util.getmSignature());
                hasDrawable = (addSignatureGuardLevel.getDrawable() != null);
                isSignatureMandatory = !hasDrawable;
                if (isSignatureMandatory) {
                    snackBarWithMesg(addSignatureGuardLevel, getResources().getString(R.string.signature_mandatory));
                    return;
                }

            } else if (requestCode == Constants.KIT_DISTRIBUTION_IMAGE_REQUEST_CODE) {
                String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
                kitReplaceImage.setImageURI(Uri.parse(imagePath));
                setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position != 0) {
            isselectedZeroPosition = false;
            if (kitDeliveredItemsIR.getIsUnPaid()) {
                kitDeliveredItemsIR.setNonIssueReasonId(lookupModelNonIssueReasons.get(position).getLookupIdentifier());
            }
        } else {
            isselectedZeroPosition = true;
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void triggerSyncAdapter() {
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);
        attachmentMetaArrayList.clear();
    }
}
