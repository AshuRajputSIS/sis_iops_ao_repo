package com.sisindia.ai.android.rota.barrackInspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 3/8/16.
 */

public class BarrackInspectionMO {


    @SerializedName("QuestionsAndOptions")
    @Expose
    private List<QuestionsAndOptionMO> questionsAndOptions = new ArrayList<QuestionsAndOptionMO>();

    /**
     *
     * @return
     * The questionsAndOptions
     */
    public List<QuestionsAndOptionMO> getQuestionsAndOptions() {
        return questionsAndOptions;
    }

    /**
     *
     * @param questionsAndOptions
     * The QuestionsAndOptions
     */
    public void setQuestionsAndOptions(List<QuestionsAndOptionMO> questionsAndOptions) {
        this.questionsAndOptions = questionsAndOptions;
    }

    @Override
    public String toString() {
        return "BarrackInspectionMO{" +
                "questionsAndOptions=" + questionsAndOptions +
                '}';
    }
}
