package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 14/7/16.
 */

public class ActionPlanMO implements Serializable{

    @SerializedName("ActionPlanId")
    @Expose
    private Integer actionPlanId;
    @SerializedName("ActionPlan")
    @Expose
    private String actionPlan;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("AssignToName")
    @Expose
    private String assignToName;
    @SerializedName("AssignTo")
    @Expose
    private Integer assignTo;
    @SerializedName("PlanDate")
    @Expose
    private String planDate;
    @SerializedName("CompletedDate")
    @Expose
    private String completedDate;
    @SerializedName("IsCompleted")
    @Expose
    private Boolean isCompleted;
    private Integer id;
    private String remarks;

    public Integer getUnitRiskId() {
        return unitRiskId;
    }

    public void setUnitRiskId(Integer unitRiskId) {
        this.unitRiskId = unitRiskId;
    }

    private Integer unitRiskId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public Integer getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Integer closedDate) {
        this.closedDate = closedDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private Integer closedDate;

    /**
     *
     * @return
     * The actionPlanId
     */
    public Integer getActionPlanId() {
        return actionPlanId;
    }

    /**
     *
     * @param actionPlanId
     * The ActionPlanId
     */
    public void setActionPlanId(Integer actionPlanId) {
        this.actionPlanId = actionPlanId;
    }

    /**
     *
     * @return
     * The actionPlan
     */
    public String getActionPlan() {
        return actionPlan;
    }

    /**
     *
     * @param actionPlan
     * The ActionPlan
     */
    public void setActionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
    }

    /**
     *
     * @return
     * The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The Reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     *
     * @return
     * The assignToName
     */
    public String getAssignToName() {
        return assignToName;
    }

    /**
     *
     * @param assignToName
     * The AssignToName
     */
    public void setAssignToName(String assignToName) {
        this.assignToName = assignToName;
    }

    /**
     *
     * @return
     * The assignTo
     */
    public Integer getAssignTo() {
        return assignTo;
    }

    /**
     *
     * @param assignTo
     * The AssignTo
     */
    public void setAssignTo(Integer assignTo) {
        this.assignTo = assignTo;
    }

    /**
     *
     * @return
     * The planDate
     */
    public String getPlanDate() {
        return planDate;
    }

    /**
     *
     * @param planDate
     * The PlanDate
     */
    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    /**
     *
     * @return
     * The completedDate
     */
    public String getCompletedDate() {
        return completedDate;
    }

    /**
     *
     * @param completedDate
     * The CompletedDate
     */
    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    /**
     *
     * @return
     * The isCompleted
     */
    public Boolean getIsCompleted() {
        return isCompleted;
    }

    /**
     *
     * @param isCompleted
     * The IsCompleted
     */
    public void setIsCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
    }
}