package com.sisindia.ai.android.unitraising;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.unitRaisingDTO.UnitRaisingStrengthMo;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 7/25/2017.
 */

public class UnitRaisingStrengthFragment extends BaseFragment {

    private List<EditText> editTextList = Collections.emptyList();
    private int actualStrengthTotal = 0;
    @Bind(R.id.totalStrength)
    TextView totalStrength;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<UnitRaisingStrengthMo> updatedActualStrength;
    private List<EmployeeAuthorizedStrengthMO> employeeAuthorizedStrengthMOList;
    private RaisingDetailsMo raisingDetailsMo;

    public static UnitRaisingStrengthFragment newInstance() {
        UnitRaisingStrengthFragment fragment = new UnitRaisingStrengthFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        raisingDetailsMo = (RaisingDetailsMo) (getArguments()).getSerializable(TableNameAndColumnStatement.RAISING_DETAILS);
    }

    @Override
    protected int getLayout() {
        return R.layout.unit_raising_strength_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.unit_raising_strength_fragment, container, false);
        ButterKnife.bind(this, view);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        dynamicallyCreateLayout(view);
        return view;
    }

    private void dynamicallyCreateLayout(View view) {

        LinearLayout superLayout = (LinearLayout) view.findViewById(R.id.unitRaisingStrengthLayout);
        employeeAuthorizedStrengthMOList = IOPSApplication.getInstance().
                getRotaLevelSelectionInstance().
                getUnitEmpAuthorizedRank(raisingDetailsMo.unitId, false, 0);
        if (employeeAuthorizedStrengthMOList != null && employeeAuthorizedStrengthMOList.size() != 0) {
            editTextList = new ArrayList<>(employeeAuthorizedStrengthMOList.size());
            int indexLength = employeeAuthorizedStrengthMOList.size();

            TextView strengthTypeTV[] = new TextView[indexLength];
            TextView authorizedSizeTV[] = new TextView[indexLength];
            EditText actualSizeEditText[] = new EditText[indexLength];

            LayoutParams strengthTypeParam = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
            strengthTypeParam.setMargins(15, 0, 0, 0);
            LayoutParams authorizedSizeParam = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
            LayoutParams actualSizeParam = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.5f);
            actualSizeParam.setMargins(0, 0, 15, 0);

            for (byte b = 0; b < employeeAuthorizedStrengthMOList.size(); b++) {

                LinearLayout parent = new LinearLayout(getActivity());
                parent.setOrientation(LinearLayout.HORIZONTAL);
                LayoutParams innerLayout = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                innerLayout.setMargins(15, 10, 15, 10);
                parent.setLayoutParams(innerLayout);

                // CREATING STRENGTH TYPE TEXT VIEW
                strengthTypeTV[b] = new TextView(getActivity());
                strengthTypeTV[b].setTextSize(15);
                strengthTypeTV[b].setLayoutParams(strengthTypeParam);
                strengthTypeTV[b].setText(employeeAuthorizedStrengthMOList.get(b).getRankAbbrevation());

                // CREATING AUTHORIZED STRENGTH TEXT VIEW
                authorizedSizeTV[b] = new TextView(getActivity());
                authorizedSizeTV[b].setTextSize(15);
                authorizedSizeTV[b].setLayoutParams(authorizedSizeParam);
                authorizedSizeTV[b].setText(String.valueOf(employeeAuthorizedStrengthMOList.get(b).getStrength()));

                // CREATING ACTUAL STRENGTH TYPE 'EDIT TEXT'
                actualSizeEditText[b] = new EditText(getActivity());
                actualSizeEditText[b].setId(b);
                actualSizeEditText[b].setTextSize(15);
                actualSizeEditText[b].setLayoutParams(actualSizeParam);
                actualSizeEditText[b].setHint("-");
                actualSizeEditText[b].setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                actualSizeEditText[b].setInputType(InputType.TYPE_CLASS_NUMBER);
                actualSizeEditText[b].setBackground(getActivity().getResources().getDrawable(R.drawable.border_editext));

                // SETTING TEXT WATCHER ON EDIT TEXTs
                actualSizeEditText[b].addTextChangedListener(new GenericTextWatcher());

                editTextList.add(actualSizeEditText[b]);
                parent.addView(strengthTypeTV[b]);
                parent.addView(authorizedSizeTV[b]);
                parent.addView(actualSizeEditText[b]);

                superLayout.addView(parent);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * method: Below method is used to validate actual strength edit text is empty or not
     */
    public UnitRaisingStrength validateActualStrengthFields() {
        updatedActualStrength = new ArrayList<>();
        UnitRaisingStrength unitRaisingStrength = new UnitRaisingStrength();
        for (int index = 0; index < employeeAuthorizedStrengthMOList.size(); index++) {
            if (editTextList.get(index).getText().toString().isEmpty()) {
                editTextList.get(index).setError("enter valid strength");
                editTextList.get(index).requestFocus();
                unitRaisingStrength.validationStatus = false;
                break;
            } else {
                String actulaCountString = editTextList.get(index).getText().toString();
                UnitRaisingStrengthMo unitRaisingStrengthMo = new UnitRaisingStrengthMo();
                unitRaisingStrengthMo.actualStrength = Integer.parseInt(actulaCountString);
                unitRaisingStrengthMo.rankId = employeeAuthorizedStrengthMOList.get(index).getRankId();
                updatedActualStrength.add(unitRaisingStrengthMo);
                if (index == employeeAuthorizedStrengthMOList.size() - 1) {
                    unitRaisingStrength.validationStatus = true;
                }
            }
        }
        unitRaisingStrength.updatedActualStrength = updatedActualStrength;
        return unitRaisingStrength;
    }

    private class GenericTextWatcher implements TextWatcher {

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            actualStrengthTotal = 0;
            calculateActualStrength();
        }
    }

    private void calculateActualStrength() {
        if (null != editTextList && editTextList.size() > 0) {
            for (byte b = 0; b < editTextList.size(); b++) {
                int getStrengthFromET = 0;
                if (!editTextList.get(b).getText().toString().isEmpty()) {
                    try {
                        getStrengthFromET = Integer.parseInt(editTextList.get(b).getText().toString());
                    } catch (Exception e) {
                    }
                }
                actualStrengthTotal = actualStrengthTotal + getStrengthFromET;
            }
            totalStrength.setText("" + actualStrengthTotal);
        }
    }
}
