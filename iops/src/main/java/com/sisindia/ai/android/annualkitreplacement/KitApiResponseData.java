package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Durga Prasad on 08-09-2016.
 */
public class KitApiResponseData {

    @SerializedName("KitRequestId")
    @Expose
    private Integer kitItemRequestedId;

    public Integer getKitItemRequestedId() {
        return kitItemRequestedId;
    }

    public void setKitItemRequestedId(Integer kitItemRequestedId) {
        this.kitItemRequestedId = kitItemRequestedId;
    }


}
