package com.sisindia.ai.android.database.unitDTO;

import android.content.Context;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.syncadpter.CommonSyncDbOperation;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 12/11/16.
 */

public abstract class UnitDBGenericStatement extends CommonSyncDbOperation {

    public static AppPreferences appPreferences;

    public UnitDBGenericStatement(Context mcontext) {
        super(mcontext);
        appPreferences = new AppPreferences(mcontext);
    }

    public static synchronized void UnitModelInsertion(List<Unit> units, boolean isNotLoadConfig, NotificationReceivingMO notificationReceivingMO) {
        if (null != units && units.size() != 0) {
            IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitTable(units, isNotLoadConfig);
            /**
             * Unit Level model insertion
             */
            for (Unit unit : units) {
                /**
                 * Unit Authorized strength table insertion
                 */
                if (null != unit.getUnitShiftRank() && unit.getUnitShiftRank().size() != 0) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitAuthorizedStrength(unit.getUnitShiftRank(), isNotLoadConfig);
                }
                /**
                 * Unit barrack table insertion
                 */
                if (null != unit.getUnitBarrack() && unit.getUnitBarrack().size() != 0) {

                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitBarrack(unit.getUnitBarrack(), isNotLoadConfig);
                }
                if (null != unit.getUnitEmployees() && unit.getUnitEmployees().size() != 0) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitEmployees(unit.getUnitEmployees(), isNotLoadConfig);
                }
                if (null != unit.getUnitMessVendors()) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitMessVendor(unit.getUnitMessVendors(), isNotLoadConfig);
                }
                if (null != unit.getUnitContacts() && unit.getUnitContacts().size() != 0) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitContacts(unit.getUnitContacts(), isNotLoadConfig);
                }
                if (null != unit.getUnitCustomers()) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitCustomer(unit.getUnitCustomers(), isNotLoadConfig);
                }
                if (null != unit.getUnitBillingCheckList() && unit.getUnitBillingCheckList().size() != 0) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitBillingCheckList(unit.getUnitBillingCheckList(), isNotLoadConfig);
                }
                UnitPostModelInsertion(unit.getUnitPosts(), isNotLoadConfig);
            }
        } else {
            Timber.d("insertUpdateUnitTable  %s", "Null");
        }
    }

    private static void UnitPostModelInsertion(List<UnitPost> unitPostList, boolean isNotLoadConfig) {
        if (null != unitPostList && unitPostList.size() != 0) {
            IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().insertUnitPost(unitPostList, isNotLoadConfig);
            for (UnitPost unitPost : unitPostList) {
                if (null != unitPost.getUnitEquipments() && unitPost.getUnitEquipments().size() != 0) {
                    IOPSApplication.getInstance().getUnitLevelInsertionDBInstance().
                            insertUnitEquipments(unitPost.getUnitEquipments(), isNotLoadConfig);
                }
            }
        }
    }
}
