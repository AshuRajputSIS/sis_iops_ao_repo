package com.sisindia.ai.android.rota;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SelectReasonActivity extends BaseActivity implements OnRecyclerViewItemClickListener {

    @Bind(R.id.selectReasonRecyclerView)
    RecyclerView selectReasonRecyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private Drawable dividerDrawable;
    private SelectReasonAdapter selectReasonAdapter;
    private ArrayList<String> selectReasonsList;
    private ArrayList<Object> lookupModelArrayList;
    private RotaTaskListMO rotaTaskListMO;
    private boolean isFromSingleUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_reason);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar,getResources().getString(R.string.SELECT_REASON));

        createRecycleView();
    }

    private void createRecycleView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        selectReasonRecyclerView.setLayoutManager(linearLayoutManager);
        selectReasonRecyclerView.setItemAnimator(new DefaultItemAnimator());
        selectReasonAdapter = new SelectReasonAdapter(this);

        //rotaTaskActivityListMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAllRotaTaskActivity();
        lookupModelArrayList = new ArrayList<>();
        lookupModelArrayList.add(getResources().getString(R.string.SELECT_REASON));
        Intent intent = getIntent();
        if(intent != null){
            rotaTaskListMO = (RotaTaskListMO) intent.getSerializableExtra(getResources().getString(R.string.ROTA_MODEL_OBJ));
            isFromSingleUnit = intent.getBooleanExtra(getResources().getString(R.string.single_unit_flag), false);
        }
        lookupModelArrayList.addAll(IOPSApplication.getInstance().
                getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.RouteChangeReason), null));


        if(lookupModelArrayList != null && lookupModelArrayList.size() != 0){

            selectReasonAdapter.setReasonDataList(lookupModelArrayList);
            selectReasonRecyclerView.setAdapter(selectReasonAdapter);

            dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider);
            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
            selectReasonRecyclerView.addItemDecoration(dividerItemDecoration);
            selectReasonAdapter.setOnRecyclerViewItemClickListener(this);
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {

        if(rotaTaskListMO != null){
            Intent addTaskIntent = new Intent(SelectReasonActivity.this, AddRotaTaskActivity.class);
            addTaskIntent.putExtra("FromMyBarrack",false);
            addTaskIntent.putExtra(getResources().getString(R.string.ROTA_MODEL_OBJ),rotaTaskListMO);
            addTaskIntent.putExtra(getResources().getString(R.string.single_unit_flag),true);
            addTaskIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(addTaskIntent);
        }
        else{
            Intent rotaTaskLIntent = new Intent(SelectReasonActivity.this, RotaTaskActivityListActivity.class);
            rotaTaskLIntent.putExtra(getResources().getString(R.string.select_reason),(((LookupModel)lookupModelArrayList.get(position))).getLookupIdentifier());
            rotaTaskLIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(rotaTaskLIntent);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }
}
