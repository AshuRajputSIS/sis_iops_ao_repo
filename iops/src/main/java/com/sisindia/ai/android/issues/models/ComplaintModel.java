package com.sisindia.ai.android.issues.models;

import java.io.Serializable;

/**
 * Created by Shushrut on 19-04-2016.
 */
public class ComplaintModel implements Serializable {
    String unitName;
    String complaintType;
    String date;
    String assignedTo;
    String status;
    String issue_id;
    String unit_id;
    String complaint_nature_id;
    String complaint_cause_id;
    String action_plan_id;
    String remarks;
    String complaint_status_id;
    String createdAt;
    String clientRemark;
    String closedAt;
    String improvementPlanId;
    String isNew;
    String isSynced;
    String complaintModeID;
    String mTask_Id;

    public String getBranchName() {
        return unitName;
    }

    public void setBranchName(String unitName) {
        this.unitName = unitName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String complaintType) {
        this.complaintType = complaintType;
    }

    public String getIsSynced() {
        return isSynced;
    }

    public void setIsSynced(String isSynced) {
        this.isSynced = isSynced;
    }

    public String getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(String issue_id) {
        this.issue_id = issue_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getComplaint_nature_id() {
        return complaint_nature_id;
    }

    public void setComplaint_nature_id(String complaint_nature_id) {
        this.complaint_nature_id = complaint_nature_id;
    }

    public String getComplaint_cause_id() {
        return complaint_cause_id;
    }

    public void setComplaint_cause_id(String complaint_cause_id) {
        this.complaint_cause_id = complaint_cause_id;
    }

    public String getAction_plan_id() {
        return action_plan_id;
    }

    public void setAction_plan_id(String action_plan_id) {
        this.action_plan_id = action_plan_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getComplaint_status_id() {
        return complaint_status_id;
    }

    public void setComplaint_status_id(String complaint_status_id) {
        this.complaint_status_id = complaint_status_id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getClientRemark() {
        return clientRemark;
    }

    public void setClientRemark(String clientRemark) {
        this.clientRemark = clientRemark;
    }

    public String getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    public String getImprovementPlanId() {
        return improvementPlanId;
    }

    public void setImprovementPlanId(String improvementPlanId) {
        this.improvementPlanId = improvementPlanId;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getComplaintModeID() {
        return complaintModeID;
    }

    public void setComplaintModeID(String complaintModeID) {
        this.complaintModeID = complaintModeID;
    }

    public String getmTask_Id() {
        return mTask_Id;
    }

    public void setmTask_Id(String mTask_Id) {
        this.mTask_Id = mTask_Id;
    }
}
