package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 22/6/16.
 */

public class RotaTask {

    @SerializedName("RotaId")
    @Expose
    private Integer rotaId;
    @SerializedName("RotaTaskId")
    @Expose
    private Integer rotaTaskId;
    @SerializedName("SourceGeoLatitude")
    @Expose
    private String sourceGeoLatitude;
    @SerializedName("SourceGeoLongitude")
    @Expose
    private String sourceGeoLongitude;
    @SerializedName("DestinationGeoLatitude")
    @Expose
    private String destinationGeoLatitude;
    @SerializedName("DestinationGeoLongitude")
    @Expose
    private String destinationGeoLongitude;
    @SerializedName("EstimatedDistance")
    @Expose
    private double estimatedDistance;
    @SerializedName("EstimatedTravelTime")
    @Expose
    private double estimatedTravelTime;
    @SerializedName("TaskSequenceNo")
    @Expose
    private Integer taskSequenceNo;
    @SerializedName("EstimatedTaskExecutionStartTime")
    @Expose
    private String estimatedTaskExecutionStartTime;
    @SerializedName("EstimatedTaskExecutionEndTime")
    @Expose
    private String estimatedTaskExecutionEndTime;
    @SerializedName("TotalEstimatedTime")
    @Expose
    private Integer totalEstimatedTime;
    @SerializedName("ActualTaskExecutionStartTime")
    @Expose
    private String actualTaskExecutionStartTime;
    @SerializedName("ActualTaskExecutionEndTime")
    @Expose
    private String actualTaskExecutionEndTime;
    @SerializedName("TaskId")
    @Expose
    private Integer taskId;
    @SerializedName("ActualTravelTime")
    @Expose
    private Integer actualTravelTime;
    @SerializedName("ActualDistance")
    @Expose
    private Integer actualDistance;
    @SerializedName("TaskRank")
    @Expose
    private Integer taskRank;
    @SerializedName("SourceGeoLocation")
    @Expose
    private double sourceGeoLocation;
    @SerializedName("DestinationGeoLocation")
    @Expose
    private double destinationGeoLocation;
    @SerializedName("SourceAddressId")
    @Expose
    private Integer sourceAddressId;
    @SerializedName("DestinationAddressId")
    @Expose
    private Integer destinationAddressId;

    /**
     * @return The rotaId
     */
    public Integer getRotaId() {
        return rotaId;
    }

    /**
     * @param rotaId The RotaId
     */
    public void setRotaId(Integer rotaId) {
        this.rotaId = rotaId;
    }

    /**
     * @return The rotaTaskId
     */
    public Integer getRotaTaskId() {
        return rotaTaskId;
    }

    /**
     * @param rotaTaskId The RotaTaskId
     */
    public void setRotaTaskId(Integer rotaTaskId) {
        this.rotaTaskId = rotaTaskId;
    }

    /**
     * @return The sourceGeoLatitude
     */
    public String getSourceGeoLatitude() {
        return sourceGeoLatitude;
    }

    /**
     * @param sourceGeoLatitude The SourceGeoLatitude
     */
    public void setSourceGeoLatitude(String sourceGeoLatitude) {
        this.sourceGeoLatitude = sourceGeoLatitude;
    }

    /**
     * @return The sourceGeoLongitude
     */
    public String getSourceGeoLongitude() {
        return sourceGeoLongitude;
    }

    /**
     * @param sourceGeoLongitude The SourceGeoLongitude
     */
    public void setSourceGeoLongitude(String sourceGeoLongitude) {
        this.sourceGeoLongitude = sourceGeoLongitude;
    }

    /**
     * @return The destinationGeoLatitude
     */
    public String getDestinationGeoLatitude() {
        return destinationGeoLatitude;
    }

    /**
     * @param destinationGeoLatitude The DestinationGeoLatitude
     */
    public void setDestinationGeoLatitude(String destinationGeoLatitude) {
        this.destinationGeoLatitude = destinationGeoLatitude;
    }

    /**
     * @return The destinationGeoLongitude
     */
    public String getDestinationGeoLongitude() {
        return destinationGeoLongitude;
    }

    /**
     * @param destinationGeoLongitude The DestinationGeoLongitude
     */
    public void setDestinationGeoLongitude(String destinationGeoLongitude) {
        this.destinationGeoLongitude = destinationGeoLongitude;
    }

    /**
     * @return The estimatedDistance
     */
    public double getEstimatedDistance() {
        return estimatedDistance;
    }

    /**
     * @param estimatedDistance The EstimatedDistance
     */
    public void setEstimatedDistance(double estimatedDistance) {
        this.estimatedDistance = estimatedDistance;
    }

    /**
     * @return The estimatedTravelTime
     */
    public double getEstimatedTravelTime() {
        return estimatedTravelTime;
    }

    /**
     * @param estimatedTravelTime The EstimatedTravelTime
     */
    public void setEstimatedTravelTime(double estimatedTravelTime) {
        this.estimatedTravelTime = estimatedTravelTime;
    }

    /**
     * @return The taskSequenceNo
     */
    public Integer getTaskSequenceNo() {
        return taskSequenceNo;
    }

    /**
     * @param taskSequenceNo The TaskSequenceNo
     */
    public void setTaskSequenceNo(Integer taskSequenceNo) {
        this.taskSequenceNo = taskSequenceNo;
    }

    /**
     * @return The estimatedTaskExecutionStartTime
     */
    public String getEstimatedTaskExecutionStartTime() {
        return estimatedTaskExecutionStartTime;
    }

    /**
     * @param estimatedTaskExecutionStartTime The EstimatedTaskExecutionStartTime
     */
    public void setEstimatedTaskExecutionStartTime(String estimatedTaskExecutionStartTime) {
        this.estimatedTaskExecutionStartTime = estimatedTaskExecutionStartTime;
    }

    /**
     * @return The estimatedTaskExecutionEndTime
     */
    public String getEstimatedTaskExecutionEndTime() {
        return estimatedTaskExecutionEndTime;
    }

    /**
     * @param estimatedTaskExecutionEndTime The EstimatedTaskExecutionEndTime
     */
    public void setEstimatedTaskExecutionEndTime(String estimatedTaskExecutionEndTime) {
        this.estimatedTaskExecutionEndTime = estimatedTaskExecutionEndTime;
    }

    /**
     * @return The totalEstimatedTime
     */
    public Integer getTotalEstimatedTime() {
        return totalEstimatedTime;
    }

    /**
     * @param totalEstimatedTime The TotalEstimatedTime
     */
    public void setTotalEstimatedTime(Integer totalEstimatedTime) {
        this.totalEstimatedTime = totalEstimatedTime;
    }

    /**
     * @return The actualTaskExecutionStartTime
     */
    public String getActualTaskExecutionStartTime() {
        return actualTaskExecutionStartTime;
    }

    /**
     * @param actualTaskExecutionStartTime The ActualTaskExecutionStartTime
     */
    public void setActualTaskExecutionStartTime(String actualTaskExecutionStartTime) {
        this.actualTaskExecutionStartTime = actualTaskExecutionStartTime;
    }

    /**
     * @return The actualTaskExecutionEndTime
     */
    public String getActualTaskExecutionEndTime() {
        return actualTaskExecutionEndTime;
    }

    /**
     * @param actualTaskExecutionEndTime The ActualTaskExecutionEndTime
     */
    public void setActualTaskExecutionEndTime(String actualTaskExecutionEndTime) {
        this.actualTaskExecutionEndTime = actualTaskExecutionEndTime;
    }

    /**
     * @return The taskId
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * @param taskId The TaskId
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * @return The actualTravelTime
     */
    public Integer getActualTravelTime() {
        return actualTravelTime;
    }

    /**
     * @param actualTravelTime The ActualTravelTime
     */
    public void setActualTravelTime(Integer actualTravelTime) {
        this.actualTravelTime = actualTravelTime;
    }

    /**
     * @return The actualDistance
     */
    public Integer getActualDistance() {
        return actualDistance;
    }

    /**
     * @param actualDistance The ActualDistance
     */
    public void setActualDistance(Integer actualDistance) {
        this.actualDistance = actualDistance;
    }

    /**
     * @return The taskRank
     */
    public Integer getTaskRank() {
        return taskRank;
    }

    /**
     * @param taskRank The TaskRank
     */
    public void setTaskRank(Integer taskRank) {
        this.taskRank = taskRank;
    }

    /**
     * @return The sourceGeoLocation
     */
    public double getSourceGeoLocation() {
        return sourceGeoLocation;
    }

    /**
     * @param sourceGeoLocation The SourceGeoLocation
     */
    public void setSourceGeoLocation(double sourceGeoLocation) {
        this.sourceGeoLocation = sourceGeoLocation;
    }

    /**
     * @return The destinationGeoLocation
     */
    public double getDestinationGeoLocation() {
        return destinationGeoLocation;
    }

    /**
     * @param destinationGeoLocation The DestinationGeoLocation
     */
    public void setDestinationGeoLocation(double destinationGeoLocation) {
        this.destinationGeoLocation = destinationGeoLocation;
    }

    /**
     * @return The sourceAddressId
     */
    public Integer getSourceAddressId() {
        return sourceAddressId;
    }

    /**
     * @param sourceAddressId The SourceAddressId
     */
    public void setSourceAddressId(Integer sourceAddressId) {
        this.sourceAddressId = sourceAddressId;
    }

    /**
     * @return The destinationAddressId
     */
    public Integer getDestinationAddressId() {
        return destinationAddressId;
    }

    /**
     * @param destinationAddressId The DestinationAddressId
     */
    public void setDestinationAddressId(Integer destinationAddressId) {
        this.destinationAddressId = destinationAddressId;
    }

}