package com.sisindia.ai.android.onboarding;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.R;

import butterknife.Bind;

/**
 * Created by shivam on 23/8/16.
 */

public class OnBoardingItemFragment extends BaseFragment {

    private static final String IMAGE_KEY = "image_key";
    private static final String TITLE_KEY = "title_key";
    private static final String DESC_KEY = "desc_key";

    @Bind(R.id.iv_onboarding)
    ImageView ivOnboarding;

    @Bind(R.id.tv_boarding_title)
    TextView tvTitle;

    @Bind(R.id.tv_boarding_text)
    TextView tvDescription;

    @Override
    protected int getLayout() {
        return R.layout.on_boarding_item;
    }

    public static OnBoardingItemFragment newInstance(int imageDrawable, String title, String desc) {
        OnBoardingItemFragment fragment = new OnBoardingItemFragment();
        Bundle args = new Bundle();
        args.putInt(IMAGE_KEY, imageDrawable);
        args.putString(TITLE_KEY, title);
        args.putString(DESC_KEY, desc);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = getArguments().getString(TITLE_KEY);
        String desc = getArguments().getString(DESC_KEY);

        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        } else {
            tvTitle.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(desc)) {
            tvDescription.setText(desc);
        } else {
            tvDescription.setVisibility(View.GONE);
        }

        setImageUsingPicasso();
    }

    private void setImageUsingPicasso() {
        int drawable = getArguments().getInt(IMAGE_KEY);
        Glide.with(getActivity()).load(drawable).into(ivOnboarding);
    }
}