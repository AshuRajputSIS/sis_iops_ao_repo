package com.sisindia.ai.android.commons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.utils.Util;

import static com.sisindia.ai.android.database.unitDTO.UnitDBGenericStatement.appPreferences;

/**
 * Created by compass on 9/12/2017.
 */

public class DutyAttendanceConfigureReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        appPreferences = new AppPreferences(context);
        Util.setAlarmManager(context, Constants.ATTENDANCE_SCHEDULE);
        appPreferences.setFirstTimeAlarmConfiguredForAttendance(true);
    }
}
