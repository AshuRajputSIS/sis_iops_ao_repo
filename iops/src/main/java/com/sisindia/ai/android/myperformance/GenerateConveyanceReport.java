package com.sisindia.ai.android.myperformance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.syncadpter.NewRotaTaskSyncing;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ashu Rajput on 4/11/2018.
 */

public class GenerateConveyanceReport extends AppCompatActivity {

    /*@Bind(R.id.toolbar)
    Toolbar toolbar;*/
    @Bind(R.id.conveyanceTimeLineRV)
    RecyclerView conveyanceTimeLineRV;
    private ConveyanceTimeLineAdapter conveyanceTimeLineAdapter = null;
    @Bind(R.id.timeLineSyncButton)
    TextView timeLineSyncButton;
    @Bind(R.id.timeLineCurrentDate)
    TextView timeLineCurrentDate;
    @Bind(R.id.generateConveyanceButton)
    TextView generateConveyanceButton;
    @Bind(R.id.noTaskMessage)
    TextView noTaskMessage;

    @Bind(R.id.totalTaskCountVsDistanceTravelled)
    TextView totalTaskCountVsDistanceTravelled;

    boolean isUnSyncedTaskThere;
    private String currentDate = "";
    private AppPreferences appPreferences = null;
    private boolean isComingFromMenu = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generate_conveyance_report);
        ButterKnife.bind(this);

        currentDate = new DateTimeFormatConversionUtil().getCurrentDate();
        timeLineCurrentDate.setText(currentDate);

       /* noTaskMessage.setVisibility(View.VISIBLE);
        noTaskMessage.setText("Coming Soon...");
        generateConveyanceButton.setVisibility(View.GONE);*/

        isUnSyncedTaskThere = IOPSApplication.getInstance().getActivityLogDB().isUnSyncedTasksAreThere();
        if (isUnSyncedTaskThere)
            timeLineSyncButton.setVisibility(View.VISIBLE);

        Bundle receivedBundle = getIntent().getExtras();
        if (receivedBundle != null) {
            if (receivedBundle.containsKey("IS_FROM_MENU")) {
                isComingFromMenu = receivedBundle.getBoolean("IS_FROM_MENU");
                generateConveyanceButton.setVisibility(View.GONE);
                totalTaskCountVsDistanceTravelled.setVisibility(View.VISIBLE);
                getGeneratedTimeLine();
            }
        }

        if (!isComingFromMenu)
            setUpRecyclerView(IOPSApplication.getInstance().getActivityLogDB().getConveyanceTimeLine(currentDate));

    }

    private void setUpRecyclerView(final List<TimeLineModel> timeLineModel) {

//        List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getActivityLogDB().getConveyanceTimeLine(currentDate);
        if (timeLineModel != null && timeLineModel.size() > 0) {

            calculateTotalTaskAndDistance(timeLineModel);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    conveyanceTimeLineRV.setLayoutManager(new LinearLayoutManager(GenerateConveyanceReport.this));
                    conveyanceTimeLineAdapter = new ConveyanceTimeLineAdapter(GenerateConveyanceReport.this, timeLineModel, isComingFromMenu);
                    conveyanceTimeLineRV.setAdapter(conveyanceTimeLineAdapter);
                }
            }, 100);


        } else {
            noTaskMessage.setVisibility(View.VISIBLE);
            conveyanceTimeLineRV.setVisibility(View.GONE);
            generateConveyanceButton.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.conveyanceBackButton)
    public void conveyanceBackButton() {
        this.finish();
    }

    @OnClick(R.id.generateConveyanceButton)
    public void generateConveyanceButton() {

        if (isUnSyncedTaskThere) {
            Toast.makeText(this, "Please sync your completed task(s) first", Toast.LENGTH_LONG).show();
        } else {
            if (Util.isNetworkConnected()) {
                showUserAlert("Please ensure that you have completed your all task of the day, before generating timeline.\n\nDo you want to proceed ?",
                        false, true);
            } else
                Util.showToast(this, getResources().getString(R.string.no_internet));
        }
    }

    private void getGeneratedTimeLine() {
        if (Util.isNetworkConnected()) {

            showProgressBar();

            if (appPreferences == null)
                appPreferences = new AppPreferences(this);

            new SISClient(this).getApi().getGeneratedTimeLine(currentDate, appPreferences.getAppUserId(), new Callback<TimeLineBaseMO>() {
                @Override
                public void success(TimeLineBaseMO timeLineBaseMO, Response response) {

                    hideProgressBar();

                    if (timeLineBaseMO.getStatusCode() == 200) {

                        List<TimeLineModel> timeLineModels = timeLineBaseMO.getTimeLineModel();

                        if (timeLineModels != null && timeLineModels.size() > 0) {
                            List<TimeLineModel> revisedTimeLineModelList = new ArrayList<>();
                            int receivedTaskTypeId, taskCounter = 0;

                            for (TimeLineModel model : timeLineModels) {
                                receivedTaskTypeId = model.getTaskTypeId();
                                if (receivedTaskTypeId > 0 || receivedTaskTypeId == -1 || receivedTaskTypeId == -2) {
                                    taskCounter = taskCounter + 1;
                                    model.setTaskCounter(taskCounter);
                                }
                                revisedTimeLineModelList.add(model);
                            }

                            if (revisedTimeLineModelList != null && revisedTimeLineModelList.size() > 0)
                                setUpRecyclerView(revisedTimeLineModelList);
                            else {
                                noTaskMessage.setVisibility(View.VISIBLE);
                                noTaskMessage.setText("Your timeline is not yet generated, please wait!!!");
                            }
                        } else {
                            noTaskMessage.setVisibility(View.VISIBLE);
                            noTaskMessage.setText("Your timeline is not yet generated, please wait!!!");
                        }
                    } else {
                        noTaskMessage.setVisibility(View.VISIBLE);
                        noTaskMessage.setText("Your timeline is not yet generated, please wait!!!");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideProgressBar();
                }
            });
        } else
            Util.showToast(this, getResources().getString(R.string.no_internet));
    }

    @OnClick(R.id.timeLineSyncButton)
    public void timeLineSyncButton() {

        // need to check whether internet connection is there or not
        if (Util.isNetworkConnected()) {
            new NewRotaTaskSyncing(this);
            showProgressBar();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideProgressBar();
                    isUnSyncedTaskThere = IOPSApplication.getInstance().getActivityLogDB().isUnSyncedTasksAreThere();
                }
            }, 4000);
        } else {
            Util.showToast(this, getResources().getString(R.string.no_internet));
        }

       /* if (NetworkUtil.isConnected) {

        } */
    }

    private void showProgressBar() {
        Util.showProgressBar(this, R.string.loading_please_wait);
    }

    private void hideProgressBar() {
        Util.dismissProgressBar(true);
    }

    private void showUserAlert(String message, boolean hideCancelButton, final boolean needToCallAPI) {

        String buttonMessage = "Ok";
        if (needToCallAPI)
            buttonMessage = "Yes";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(buttonMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (needToCallAPI)
                    callGenerateTimeLineAPI();
                else
                    GenerateConveyanceReport.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        if (hideCancelButton)
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(View.GONE);
    }

    private void callGenerateTimeLineAPI() {

        if (Util.isNetworkConnected()) {

            showProgressBar();

            if (appPreferences == null)
                appPreferences = new AppPreferences(this);

            new SISClient(this).getApi().generateTimeLine(currentDate, appPreferences.getAppUserId(), new Callback<CommonResponse>() {
                @Override
                public void success(CommonResponse commonResponse, Response response) {

                    hideProgressBar();

                    if (commonResponse.getStatusCode() == 200) {
                        showUserAlert("Your timeline is processed in the background.\nKindly check after 15 minutes from the 'Timeline' option in the menu.",
                                true, false);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    hideProgressBar();
                    Util.showToast(GenerateConveyanceReport.this, "No internet connection, retry again.");
                }
            });
        } else
            Util.showToast(this, getResources().getString(R.string.no_internet));
    }

    private void calculateTotalTaskAndDistance(List<TimeLineModel> timeLineModel) {

        int totalTask = 0, taskTypeId;
        double distance = 0.0;

        for (int i = 0; i < timeLineModel.size(); i++) {
            taskTypeId = timeLineModel.get(i).getTaskTypeId();
            if (taskTypeId > 0 || taskTypeId == -1 || taskTypeId == -2) {
                totalTask = totalTask + 1;
            }

            if (taskTypeId > 0 || taskTypeId == -1 || taskTypeId == -2 || taskTypeId == -3) {
                try {
                    distance = distance + timeLineModel.get(i).getDistance();
                } catch (Exception e) {
                }
            }
        }

        totalTaskCountVsDistanceTravelled.setText("Total Task / Distance Travelled : ( " + totalTask + "/" + formatDoubleToTwoDigits(distance) + " Km )");
    }

    private String formatDoubleToTwoDigits(double totalDistance) {
        try {
            return String.format("%.1f", totalDistance);
        } catch (Exception e) {
        }
        return "" + totalDistance;
    }

}
