package com.sisindia.ai.android.rota.models;

/**
 * Created by shankar on 21/6/16.
 */

public class RotaTaskActvityCheckMO {

    private int Id;
    private int TaskStatusId;
    private int TaskId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getTaskStatusId() {
        return TaskStatusId;
    }

    public void setTaskStatusId(int taskStatusId) {
        TaskStatusId = taskStatusId;
    }

    public int getTaskId() {
        return TaskId;
    }

    public void setTaskId(int taskId) {
        TaskId = taskId;
    }

}