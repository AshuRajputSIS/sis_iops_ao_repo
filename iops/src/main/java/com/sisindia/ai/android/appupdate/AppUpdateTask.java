package com.sisindia.ai.android.appupdate;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.SplashScreenActivity;
import com.sisindia.ai.android.commons.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 * Created by hannan on 01-04-2014.
 */
public class AppUpdateTask extends AsyncTask<String,Integer,String> {
    private static final String TAG = AppUpdateTask.class.getSimpleName();
    private ProgressDialog progressDialog;
    private SplashScreenActivity callingActivity;

    public void setup(SplashScreenActivity callingActivity,ProgressDialog progressDialog){
        this.callingActivity = callingActivity;
        this.progressDialog=progressDialog;
    }

    @Override
    protected String doInBackground(String... arg0) {
        String status;
        try {
            String PATH = Constants.APP_PATH;
            File file=new File(PATH);
            if(!file.exists()) {
                file.mkdirs();
            }
            File outputFile = new File(file, Constants.FILE_NAME);
            if(outputFile.exists()){
                outputFile.delete();
            }

            URL url = new URL(arg0[0]);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            FileOutputStream fos = new FileOutputStream(outputFile);
            long fileLength = connection.getContentLength();
            InputStream is = connection.getInputStream();

            byte[] buffer = new byte[4096];
            int len1 = 0,total=0;
            while ((len1 = is.read(buffer)) > 0) {
                fos.write(buffer, 0, len1);
                total+=len1;
                if (fileLength > 0)
                    publishProgress((int) (total * 100 / fileLength));

            }
            fos.close();
            is.close();
            status="success";
        }catch (SocketTimeoutException e) {
            status="failed";
            Crashlytics.logException(e);
        }
        catch (Exception e) {
            status="failed";
            Crashlytics.logException(e);
            return status;
        }
        return status;
    }

    @Override
    protected void onPostExecute(String result) {

        if (result.equalsIgnoreCase("success")) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            callingActivity.onDownloadSuccess();
        } else {
            progressDialog.dismiss();
            callingActivity.onDownloadFailure(result);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressDialog.setProgress(values[0]);
    }
}