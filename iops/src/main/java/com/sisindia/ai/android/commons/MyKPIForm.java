package com.sisindia.ai.android.commons;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sisindia.ai.android.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 5/31/2018.
 */

public class MyKPIForm extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.myKPIRecyclerView)
    RecyclerView myKPIRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_kpi_form);
        ButterKnife.bind(this);
        setUpToolBar();
        updateKPIToRecyclerView();
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My KPI");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateKPIToRecyclerView() {

        myKPIRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myKPIRecyclerView.setAdapter(new MyKPIAdapter(this));

    }


}

