package com.sisindia.ai.android.commons;

/**
 * Created by Hannan Shaik on 15/07/16.
 */

public class IdCardMO {
    private String name;
    private String regNumber;
    private String dateOfBirth;
    private String education;
    private String height;
    private String weight;
    private String bloodGroup;
    private String dateOfJoining;
    private String experience;
    private String dateOfIdExpiry;

    public IdCardMO(String scannedCodeValue){
       constructData(scannedCodeValue);
    }

    private void constructData(String scannedCodeValue) {
        String[] fields = scannedCodeValue.split("\r\n");
        String[] metaInfo;
        String[] experienceInfo;
        if(fields != null && fields.length != 0){
            switch (fields.length){
                case 1:  this.name = fields[0];
                    break;
                case 2:  this.name = fields[0];
                    this.regNumber = fields[1];
                    break;
                case 3:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    break;
                case 4:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    this.education = fields[3];
                    break;
                case 5:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    this.education = fields[3];
                    metaInfo = fields[4].split(",");
                    if(metaInfo != null && metaInfo.length != 0) {
                        this.height = metaInfo[0];
                        if(metaInfo.length == 2){
                            this.weight = metaInfo[1];
                        }
                        else if(metaInfo.length == 3){
                            this.weight = metaInfo[1];
                            this.bloodGroup = metaInfo[2];
                        }
                    }
                    break;
                case 6:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    this.education = fields[3];
                    metaInfo = fields[4].split(",");
                    if(metaInfo != null && metaInfo.length != 0) {
                        this.height = metaInfo[0];
                        if(metaInfo.length == 2){
                            this.weight = metaInfo[1];
                        }
                        else if(metaInfo.length == 3){
                            this.weight = metaInfo[1];
                            this.bloodGroup = metaInfo[2];
                        }
                    }
                    experienceInfo = fields[5].split(",");
                    if(experienceInfo != null && experienceInfo.length != 0) {
                        this.dateOfJoining = experienceInfo[0];
                        if(experienceInfo.length == 2) {
                            this.experience = experienceInfo[1];
                        }
                    }
                    break;
                case 7:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    this.education = fields[3];
                    metaInfo = fields[4].split(",");
                    if(metaInfo != null && metaInfo.length != 0) {
                        this.height = metaInfo[0];
                        if(metaInfo.length == 2){
                            this.weight = metaInfo[1];
                        }
                        else if(metaInfo.length == 3){
                            this.weight = metaInfo[1];
                            this.bloodGroup = metaInfo[2];
                        }
                    }
                    experienceInfo = fields[5].split(",");
                    if(experienceInfo != null && experienceInfo.length != 0) {
                        this.dateOfJoining = experienceInfo[0];
                        if(experienceInfo.length == 2) {
                            this.experience = experienceInfo[1];
                        }
                    }
                    this.dateOfIdExpiry = fields[6];
                    break;
                default:
                    this.name = fields[0];
                    this.regNumber = fields[1];
                    this.dateOfBirth = fields[2];
                    this.education = fields[3];
                    metaInfo = fields[4].split(",");
                    if(metaInfo != null && metaInfo.length != 0) {
                        this.height = metaInfo[0];
                        if(metaInfo.length == 2){
                            this.weight = metaInfo[1];
                        }
                        else if(metaInfo.length == 3){
                            this.weight = metaInfo[1];
                            this.bloodGroup = metaInfo[2];
                        }
                    }
                    experienceInfo = fields[5].split(",");
                    if(experienceInfo != null && experienceInfo.length != 0) {
                        this.dateOfJoining = experienceInfo[0];
                        if(experienceInfo.length == 2) {
                            this.experience = experienceInfo[1];
                        }
                    }
                    this.dateOfIdExpiry = fields[6];
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getDateOfIdExpiry() {
        return dateOfIdExpiry;
    }

    public void setDateOfIdExpiry(String dateOfIdExpiry) {
        this.dateOfIdExpiry = dateOfIdExpiry;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdCardMO{");
        sb.append("name='").append(name).append('\'');
        sb.append(", regNumber='").append(regNumber).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", education='").append(education).append('\'');
        sb.append(", height='").append(height).append('\'');
        sb.append(", weight='").append(weight).append('\'');
        sb.append(", bloodGroup='").append(bloodGroup).append('\'');
        sb.append(", dateOfJoining='").append(dateOfJoining).append('\'');
        sb.append(", experience='").append(experience).append('\'');
        sb.append(", dateOfIdExpiry='").append(dateOfIdExpiry).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
