package com.sisindia.ai.android.rota.billsubmission;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillAcceptedMO;

public class BillSubmission {

@SerializedName("BillAccepted")
@Expose
private BillAcceptedMO billAccepted;
@SerializedName("BillHandOverTO")
@Expose
private String billHandOverTO;


/**
* 
* @return
* The billAccepted
*/
public BillAcceptedMO getBillAccepted() {
return billAccepted;
}

/**
* 
* @param billAccepted
* The BillAccepted
*/
public void setBillAccepted(BillAcceptedMO billAccepted) {
this.billAccepted = billAccepted;
}

/**
* 
* @return
* The billHandOverTO
*/
public String getBillHandOverTO() {
return billHandOverTO;
}

/**
* 
* @param billHandOverTO
* The BillHandOverTO
*/
public void setBillHandOverTO(String billHandOverTO) {
this.billHandOverTO = billHandOverTO;
}


}



