package com.sisindia.ai.android.onboarding;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;

/**
 * Pass bundle data in the module name in the format module_name
 * Pass bundle data with number of images for that module
 * Put the images in the drawable-nodpi folder in the format module_name(number)
 * like client_coordination1
 * Define the module_name as constant in OnBoardingFactory
 */

public class OnBoardingActivity extends BaseActivity {

    private static final String TAG = OnBoardingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_boarding_activity);
        addOnboardingFragment();
    }

    private void addOnboardingFragment() {
        String moduleName = getIntent().getStringExtra(OnBoardingFactory.MODULE_NAME_KEY);
        int noOfImages = getIntent().getIntExtra(OnBoardingFactory.NO_OF_IMAGES, 0);
        OnBoardingFragment onBoardingFragment = OnBoardingFragment.newInstance(moduleName, noOfImages);
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().add(R.id.ll_onboarding_fragment, onBoardingFragment, TAG).commit();
    }
}