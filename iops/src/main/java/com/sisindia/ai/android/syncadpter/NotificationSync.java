package com.sisindia.ai.android.syncadpter;

import android.content.Context;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationIR;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationOR;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by shankar on 11/11/16.
 */

public class NotificationSync extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private List<NotificationIR> notificationIRList;

    public NotificationSync(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        notificationTableSyncing();
    }

    private void notificationTableSyncing() {
        notificationIRList = IOPSApplication.getInstance().getNotificationStatementDB()
                .getNotificationDataToSync();
        if (notificationIRList != null && notificationIRList.size() != 0) {
            for (NotificationIR notificationIR : notificationIRList) {
                notificationSyncAPI(notificationIR);
            }
        }
    }

    private void notificationSyncAPI(final NotificationIR notificationIR) {

        sisClient.getApi().notificationSync(notificationIR, new Callback<ApiResponse<NotificationOR>>() {
            @Override
            public void success(ApiResponse<NotificationOR> notificationORApiResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "notificationSync response  %s", notificationORApiResponse.statusCode);
                switch (notificationORApiResponse.statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        //IOPSApplication.getInstance().getNotificationStatementDB()
                         //       .updateSyncForNotification(notificationIR.getId());
                        IOPSApplication.getInstance().getNotificationStatementDB().deleteRow(notificationIR.getId());
                        Timber.d(Constants.TAG_SYNCADAPTER + "NotificationSync count   %s", "count: ");
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.d(Constants.TAG_SYNCADAPTER + "NotificationSync count   %s", "count: ");
                Util.printRetorfitError("NotificationSync", error);
            }
        });

    }
}


