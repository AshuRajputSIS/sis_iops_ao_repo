package com.sisindia.ai.android.issues.improvements;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.grievances.CustomGrievanceAdapter;
import com.sisindia.ai.android.issues.models.ActionPlanCount;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImprovementPlansFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImprovementPlansFragment extends BaseFragment implements UpdateAdapterListner , SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    @Bind(R.id.add_grievance)
    Button addPlan;
    @Bind(R.id.grievance_recyclerview)
    RecyclerView mImprovementRecyclerview;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ArrayList<Object> mDataItems = new ArrayList<>();
    CustomGrievanceAdapter customIssuesAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ImprovementPlansAdapter mImprovementPlansAdapter;
    private Resources resources;
    private IssueLevelSelectionStatementDB mIssueLevelSelectionStatementDB;
    protected static UpdateAdapterListner listner;
    private ActionPlanCount actionPlanCount;
    private Context mContext;
    private ArrayList<Object> openImprovementList;

    public ImprovementPlansFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ImprovementPlansFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImprovementPlansFragment newInstance(String param1, String param2) {

            ImprovementPlansFragment fragment = new ImprovementPlansFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.issues_homepage_grievance, container, false);
        ButterKnife.bind(this,view);
        listner = this;
        resources= getResources();
        mIssueLevelSelectionStatementDB= IOPSApplication.getInstance().getIssueSelectionDBInstance();
        addPlan.setText(resources.getString(R.string.addImprovementPlans));
        mImprovementRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mSwipeRefreshLayout.setOnRefreshListener(this);
        initImprovementPlans();
        return view;
    }

    @Override
    protected int getLayout() {
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void initImprovementPlans() {
        //if(openImprovementList != null && openImprovementList.size() != 0)
        mDataItems.clear();
        ImprovementPlanMO improvementPlanMO = mIssueLevelSelectionStatementDB.getImprovementPlanList();

        if(improvementPlanMO != null){
             openImprovementList =  improvementPlanMO.getImprovementPlanList();
             actionPlanCount = IOPSApplication.getInstance().getIssueSelectionDBInstance().getImprovementPlanOpenCount();
        }
        if(actionPlanCount !=null) {
            mDataItems.add(actionPlanCount);
        }
        else{
            actionPlanCount = new ActionPlanCount();
            actionPlanCount.setCountLessthan2Days(0);
            actionPlanCount.setCountMorethan2Days(0);
            actionPlanCount.setCountMorethan7Days(0);
            mDataItems.add(actionPlanCount);
        }
        mDataItems.addAll(improvementPlanMO.getImprovementPlanList());
        if(mImprovementPlansAdapter == null) {
            mImprovementPlansAdapter = new ImprovementPlansAdapter(getContext());
        }
        mImprovementPlansAdapter.setImprovementDataList(mDataItems);
        mImprovementRecyclerview.setAdapter(mImprovementPlansAdapter);
        mImprovementPlansAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mDataItems != null && mDataItems.size() > 0) {
            mImprovementPlansAdapter.notifyDataSetChanged();
        }
    }

    public void updateRecyclerview(String ss) {
        initImprovementPlans();
    }

    @OnClick(R.id.add_grievance)
    public void onAddImprovementPlan() {

        if(appPreferences.getDutyStatus()) {
            appPreferences.setIssueType(Constants.NavigationFlags.TYPE_IMPROVEMENT_PLAN);
            openOnBardingActivity(OnBoardingFactory.IMPROVEMENT_PLAN, 2);
        }
        else{

            snackBarWithMessage( getResources().getString(R.string.TURN_DUTY));
        }

    }
    private void openOnBardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(getActivity(), OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }

    @Override
    public void updateAdapter() {
        initImprovementPlans();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    public void setOpenActiontData(ActionPlanCount actionPlanCount) {
        this.actionPlanCount =actionPlanCount;
    }

    @Override
    public void onRefresh() {
        initImprovementPlans();
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(mContext, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }
}
