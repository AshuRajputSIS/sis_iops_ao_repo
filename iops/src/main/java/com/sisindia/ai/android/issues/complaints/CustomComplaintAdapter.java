package com.sisindia.ai.android.issues.complaints;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.ComplaintCount;
import com.sisindia.ai.android.issues.models.GrievanceModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;



/**
 * Created by Shushrut on 14-04-2016.
 */
public class CustomComplaintAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private Context mcontext;
    private ArrayList<Object> mData;
    private int mOpenIssues_layout = 0;
    private int mGreviance_layout = 1;
    private boolean setVisible = false;
    private View mDividerView;
    private CustomFontTextview mCreatedin;
    private CustomFontTextview mCreatedintv;
    private CustomFontTextview mdate;
    private CustomFontTextview mdays;
    private RelativeLayout improvePlanLayout;
    private ImageView mCustomImageView;
    private boolean viewDetailsButton;
    private boolean mhideDivider;
    private boolean mShowImprovePlan;
    private LinearLayout mholderView;
    private View mViewDetails;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private AppPreferences appPreferences;


    public CustomComplaintAdapter(Context mcontext, ArrayList<Object> mData, boolean setVisible, boolean viewDetailsButton, boolean hideDivider, boolean showImprovePlan) {
        this.mcontext = mcontext;
        this.mData = mData;
        this.setVisible = setVisible;
        this.viewDetailsButton = viewDetailsButton;
        mhideDivider = hideDivider;
        mShowImprovePlan = showImprovePlan;
        dateTimeFormatConversionUtil= new DateTimeFormatConversionUtil();
        appPreferences = new AppPreferences(mcontext);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == mOpenIssues_layout) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.openissues_layout, parent, false);
            return new OpenIssuesHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_item, parent, false);
            mDividerView = view.findViewById(R.id.checking_grievance_complaint_divider_one);
            if (mhideDivider) {

                mDividerView.setVisibility(View.VISIBLE);
            } else {
                mDividerView.setVisibility(View.GONE);
            }
            improvePlanLayout = (RelativeLayout) view.findViewById(R.id.checking_improve_plan_cardview_layout);
            mCreatedintv = (CustomFontTextview) view.findViewById(R.id.checking_improve_plan_created_cardview__type_txt);
            mCreatedin = (CustomFontTextview) view.findViewById(R.id.checking_improve_plan_createdin_cardview__type_txt);
            mdate = (CustomFontTextview) view.findViewById(R.id.issueTargetClosureDate);
            mdays = (CustomFontTextview) view.findViewById(R.id.timeDurationLeft);
            return new ComplaintDataHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        Object object = mData.get(position);

        if (holder instanceof OpenIssuesHolder && object instanceof ComplaintCount) {
            OpenIssuesHolder mOpenIssuesHolder = (OpenIssuesHolder) holder;
            mOpenIssuesHolder.mGrtThanSevenDays.setText(String.valueOf(((ComplaintCount) object).getCountMorethan7Days()));
            mOpenIssuesHolder.mLessThanFourtyEight.setText(String.valueOf(((ComplaintCount) object).getCountLessthan2Days()));
            mOpenIssuesHolder.mGrtThanFourtyEight.setText(String.valueOf(((ComplaintCount) object).getCountMorethan2Days()));
        }
        else if (holder instanceof ComplaintDataHolder && object instanceof IssuesMO) {
            ComplaintDataHolder mHolder = (ComplaintDataHolder) holder;
            mHolder.mBranchName.setText(((IssuesMO) mData.get(position)).getUnitName());
            mHolder.mComplaintAssignedTo.setText(((IssuesMO) mData.get(position)).getAssignedToName());
            mHolder.mComplaintRaisedBy.setText(((IssuesMO) mData.get(position)).getUnitContactName());
            //mHolder.mComplaintReported.setText(Long.toString(Util.getDateTimeDifference(((IssuesMO) mData.get(position)).getCreatedDateTime())) + " days ago");
            mHolder.mComplaintStatus.setText(((IssuesMO) mData.get(position)).getStatus());
            String dateString = ((IssuesMO) mData.get(position)).getTargetActionEndDate();

            if(dateString.length() > 10){
                mHolder.mComplaintTargetDate.setText(dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(dateString));
            }
            else {
                mHolder.mComplaintTargetDate.setText(dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(dateString));
            }

            mHolder.mComplaintType.setText(((IssuesMO) mData.get(position)).getCauseOfComplaint());
            long millis =dateTimeFormatConversionUtil.convertStringToDate(((IssuesMO) mData.get(position)).getCreatedDateTime()).getTime();

            mHolder.mComplaintReported.setReferenceTime(millis);
            mHolder.mView.setTag(position);
            mHolder.mView.setOnClickListener(this);
        }
    }

    @Override
    public int getItemCount() {
        int count = mData == null ? 0 : mData.size();
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return mOpenIssues_layout;
        } else {
            return mGreviance_layout;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checking_grievance_complaint_view_details_image_layout:
                appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
                Intent viewComplaintDetails = new Intent(mcontext, AddComplaintActivity.class);
                viewComplaintDetails.putExtra("isfromAddIssue", false);
                viewComplaintDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Util.showOptionMenu = false;
                mcontext.startActivity(viewComplaintDetails);
                break;
            case R.id.complaint_view_details:
                appPreferences.setIssueType(Constants.NavigationFlags.TYPE_COMPLAINTS);
                Intent viewcomplaint = new Intent(mcontext, ViewComplaintActivity.class);
                viewcomplaint.putExtra(mcontext.getResources().getString(R.string.complaint_details), (Parcelable) mData.get((int) v.getTag()));
                mcontext.startActivity(viewcomplaint);
                break;
        }
    }


    private class OpenIssuesHolder extends RecyclerView.ViewHolder {
        public CustomFontTextview mLessThanFourtyEight;
        public CustomFontTextview mGrtThanFourtyEight;
        public CustomFontTextview mGrtThanSevenDays;

        public OpenIssuesHolder(View view) {
            super(view);
            mLessThanFourtyEight = (CustomFontTextview) view.findViewById(R.id.lessThan48HoursTv);
            mGrtThanFourtyEight = (CustomFontTextview) view.findViewById(R.id.greaterThan48HoursTV);
            mGrtThanSevenDays = (CustomFontTextview) view.findViewById(R.id.greaterThan7Days);
        }
    }

    private class ComplaintDataHolder extends RecyclerView.ViewHolder {
        public CustomFontTextview mBranchName;
        public CustomFontTextview mComplaintType;
        public CustomFontTextview mComplaintRaisedBy;
        public CustomFontTextview mComplaintAssignedTo;
        public CustomFontTextview mComplaintStatus;
        public CustomFontTextview mComplaintTargetDate;
        public RelativeTimeTextView mComplaintReported;
        public View mView;

        public ComplaintDataHolder(View view) {
            super(view);
            mBranchName = (CustomFontTextview) view.findViewById(R.id.complaint_unitname);
            mComplaintType = (CustomFontTextview) view.findViewById(R.id.complaint_nature);
            mComplaintRaisedBy = (CustomFontTextview) view.findViewById(R.id.complaint_by);
            mComplaintAssignedTo = (CustomFontTextview) view.findViewById(R.id.assignee_name);
            mComplaintStatus = (CustomFontTextview) view.findViewById(R.id.current_status);
            mComplaintTargetDate = (CustomFontTextview) view.findViewById(R.id.complaint_target_datevalue);
            mComplaintReported = (RelativeTimeTextView) view.findViewById(R.id.complaint_reported_value);
            mView = view.findViewById(R.id.complaint_view_details);

        }
    }

}

