package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 16/9/16.
 */

public class EmployeeLeaveCalendar {
    @SerializedName("EmployeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("LeaveDate")
    @Expose
    private String leaveDate;

    /**
     *
     * @return
     * The employeeId
     */
    public Integer getEmployeeId() {
        return employeeId;
    }

    /**
     *
     * @param employeeId
     * The EmployeeId
     */
    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    /**
     *
     * @return
     * The leaveDate
     */
    public String getLeaveDate() {
        return leaveDate;
    }

    /**
     *
     * @param leaveDate
     * The LeaveDate
     */
    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

}
