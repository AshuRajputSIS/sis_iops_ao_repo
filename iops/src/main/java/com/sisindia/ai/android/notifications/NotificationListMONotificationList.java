package com.sisindia.ai.android.notifications;

import java.io.Serializable;

public class NotificationListMONotificationList implements Serializable {
    private static final long serialVersionUID = 5693907983728762721L;
    private String notificationReceivedTime;
    private int notificationTypeImage;
    private String notificationMessage;

    public String getNotificationReceivedTime() {
        return this.notificationReceivedTime;
    }

    public void setNotificationReceivedTime(String notificationReceivedTime) {
        this.notificationReceivedTime = notificationReceivedTime;
    }

    public int getNotificationTypeImage() {
        return this.notificationTypeImage;
    }

    public void setNotificationTypeImage(int notificationTypeImage) {
        this.notificationTypeImage = notificationTypeImage;
    }

    public String getNotificationMessage() {
        return this.notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }
}
