package com.sisindia.ai.android.commons;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.sisindia.ai.android.settings.ManualImageSyncing;
import com.sisindia.ai.android.syncadpter.SyncStatusNotification;

/**
 * Created by compass on 4/3/2017.
 */

public class ScheduleSyncJobService extends IntentService {
    private static  final String TAG = ScheduleSyncJobService.class.getSimpleName();
    private SyncStatusNotification syncStatusNotification;


    public ScheduleSyncJobService(){
        super(TAG);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        syncStatusNotification = new SyncStatusNotification(ScheduleSyncJobService.this);
             if(syncStatusNotification.isAnyRecordsToSync()){
                 triggerSyncAdapter();
             }

    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }


}
