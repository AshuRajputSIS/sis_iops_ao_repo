package com.sisindia.ai.android.rota;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shankar on 24/5/16.
 */
public enum RotaTaskStatusEnum {


    Active(1),
    InProgress(2),
    InActive(3),
    Completed(4);

    private static Map map = new HashMap<>();

    static {
        for (RotaTaskStatusEnum rotaTaskStatus : RotaTaskStatusEnum.values()) {
            map.put(rotaTaskStatus.value, rotaTaskStatus);
        }
    }

    private int value;

    RotaTaskStatusEnum(int value) {
        this.value = value;
    }

    public static RotaTaskStatusEnum valueOf(int rotaTaskStatus) {
        return (RotaTaskStatusEnum) map.get(rotaTaskStatus);
    }

    public int getValue() {
        return value;
    }

}
