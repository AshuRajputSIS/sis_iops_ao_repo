package com.sisindia.ai.android.myunits.models;

/**
 * Created by shankar on 19/8/16.
 */
public class UnitList {

    private Integer unitId;
    private String unitName;
    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }



    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }



}
