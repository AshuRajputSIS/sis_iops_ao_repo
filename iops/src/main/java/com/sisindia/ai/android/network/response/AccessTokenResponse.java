package com.sisindia.ai.android.network.response;

import java.io.Serializable;

public class AccessTokenResponse implements Serializable {
    private static final long serialVersionUID = -6800710636820812252L;
    private String access_token;
    private String token_type;
    private int expires_in;

    public String getAccess_token() {
        return this.access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return this.token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return this.expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }
}
