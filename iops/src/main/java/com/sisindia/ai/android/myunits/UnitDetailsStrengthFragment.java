package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitStrengthBaseOR;
import com.sisindia.ai.android.myunits.models.UnitStrengthMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import java.net.HttpURLConnection;
import java.util.List;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UnitDetailsStrengthFragment extends Fragment implements OnRecyclerViewItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    CustomFontTextview noDataAvailable;

    // TODO: Rename and change types of parameters
    private int unitId;
    private String mParam2;
    private FloatingActionButton postAddFloattingButton;
    private UnitDetailsStrengthAdapter unitDetailsStrengthAdapter;
    private SISAITrackingDB sisaiTrackingDBInstance;
    private MyUnitHomeMO unitDetails;
    private List<UnitStrengthMO> unitStrengthMOList;
    private RecyclerView StrengthsRecyclerView;
    private AppPreferences appPreferences;


    public UnitDetailsStrengthFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param unitId Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UnitDetailsStrengthFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UnitDetailsStrengthFragment newInstance(int unitId, String param2) {
        UnitDetailsStrengthFragment fragment = new UnitDetailsStrengthFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, unitId);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            unitId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.unit_details_strength_tab, container, false);
        ButterKnife.bind(this, view);
        appPreferences = new AppPreferences(getActivity());
        unitDetails = (MyUnitHomeMO) SingletonUnitDetails.getInstance().getUnitDetails();
        if(unitDetails == null){
            unitDetails =  appPreferences.getUnitJsonData();
        }
        StrengthsRecyclerView = (RecyclerView) view.findViewById(R.id.unitdetials_strengthtab_recycleview);
        noDataAvailable = (CustomFontTextview) view.findViewById(R.id.noDataAvailable);

        getUnitStrengthApiCall();


        return view;
    }

    private void updateRecycleView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        StrengthsRecyclerView.setLayoutManager(linearLayoutManager);
        StrengthsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        unitDetailsStrengthAdapter = new UnitDetailsStrengthAdapter(getActivity());
        sisaiTrackingDBInstance = IOPSApplication.getInstance().getSISAITrackingDBInstance();


        if (unitStrengthMOList != null && unitStrengthMOList.size() != 0) {
            noDataAvailable.setVisibility(View.GONE);
            unitDetailsStrengthAdapter.setStrengthDataList(unitStrengthMOList);
            StrengthsRecyclerView.setAdapter(unitDetailsStrengthAdapter);
            unitDetailsStrengthAdapter.setOnRecyclerViewItemClickListener(this);
        } else {
            noDataAvailable.setVisibility(View.VISIBLE);
            StrengthsRecyclerView.setVisibility(View.GONE);
        }

    }

    private void getUnitStrengthApiCall() {
        SISClient sisClient = new SISClient(getActivity());
        sisClient.getApi().getUnitStrengthDetails(unitDetails.getUnitId(), new Callback<UnitStrengthBaseOR>() {
            @Override
            public void success(UnitStrengthBaseOR unitStrengthBaseOR, Response response) {
                switch (unitStrengthBaseOR.getStatusCode()){

                    case HttpURLConnection.HTTP_OK:
                        unitStrengthMOList = unitStrengthBaseOR.getData();
                        updateRecycleView();
                        break;
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UnitDetailsStrength", error);

            }
        });
    }

    // TODO: Rename method, onDutyChange argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
