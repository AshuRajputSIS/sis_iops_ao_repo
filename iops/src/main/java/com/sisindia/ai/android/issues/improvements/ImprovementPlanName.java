package com.sisindia.ai.android.issues.improvements;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.IssuesViewPagerAdapter;
import com.sisindia.ai.android.utils.Util;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shushrut on 19-04-2016.
 */
public class ImprovementPlanName extends BaseActivity implements TabLayout.OnTabSelectedListener {
    @Bind(R.id.improvement_plans_tabLayout)
    TabLayout issuesTabLayout;
    @Bind(R.id.improvement_plans_viewpager)
    ViewPager issuesViewpager;
    @Bind(R.id.improvement_plans_toolbar)
    Toolbar toolbar;
    boolean isFromUnitRisk;
    int currentTab = 0;
    private ColorStateList colorStateList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.improvement_plan_layout);

        if (getIntent().hasExtra(getResources().getString(R.string.isfromunitRisk))) {
            enableAssignButton(getIntent().getExtras().getBoolean(getResources().getString(R.string.isfromunitRisk)));

        }
        updateUi();
    }

    private void enableAssignButton(boolean enable) {

        if (enable) {
            isFromUnitRisk = enable;
        } else {
            isFromUnitRisk = enable;
        }
    }

    private void updateUi() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!Util.showAssignedTo) {
            getSupportActionBar().setTitle(R.string.improvement_plan_name);
        } else {
            getSupportActionBar().setTitle(R.string.unit_at_risk);
        }
        initViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        colorStateList = ContextCompat.getColorStateList(this, R.drawable.tab_label_indicator);
        setupViewPager(issuesViewpager);
        issuesTabLayout.setupWithViewPager(issuesViewpager);
        issuesTabLayout.setOnTabSelectedListener(this);
        issuesTabLayout.setTabTextColors(colorStateList);
    }


    private void setupViewPager(ViewPager viewPager) {
        final IssuesViewPagerAdapter issuesViewPagerAdapter = new IssuesViewPagerAdapter(getSupportFragmentManager());
       /* //issuesViewPagerAdapter.*//*setTabTitles(viewPagerTabTiles);
        issuesViewPagerAdapter.setIssuesData(postData);*/

        issuesViewPagerAdapter.addFragment(new PendingPlanFragment().newInstance("", "", Util.showAssignedTo, currentTab), TableNameAndColumnStatement.Pending);
        issuesViewPagerAdapter.addFragment(new CompletedTaskFragment().newInstance("", "", Util.showAssignedTo, currentTab), TableNameAndColumnStatement.COMPLETED);
        // issuesViewPagerAdapter.addFragment(new ImprovementPlansFragment().newInstance("", ""), getString(R.string.issues_homepage_improvementPlans_tab));
        viewPager.setAdapter(issuesViewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                Util.currentTab = position;
                issuesViewPagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        issuesViewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
