package com.sisindia.ai.android.issues.improvements;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;


public class EnterDateActivity extends BaseActivity {
    @Bind(R.id.toolbar_post)
    Toolbar mToolBar;
    @Bind(R.id.closure_date)
    CustomFontTextview closureDate;
    @Bind(R.id.additionalRemarks)
    TextView additionalRemarks;
    @Bind(R.id.editTextTypeRemark)
    EditText editTextTypeRemark;
    @Bind(R.id.Parentlayout)
    CoordinatorLayout Parentlayout;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_date);
        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        closureDate.setText(dateTimeFormatConversionUtil.convertDateToDayMonthDateYearFormat(dateTimeFormatConversionUtil.getCurrentDate()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.enter_date);
        //initViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_post_save:


                this.finish();
                break;
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }




}
