package com.sisindia.ai.android.myunits.models;

import com.sisindia.ai.android.myunits.UpdatePostTabListener;

/**
 * Created by Shushrut on 12-04-2016.
 */
public class PostFragmentUpdateModel {
    private static PostFragmentUpdateModel mupdateModel = new PostFragmentUpdateModel();
    private UpdatePostTabListener mListener;

    private PostFragmentUpdateModel() {
    }

    public static PostFragmentUpdateModel getInstance() {

        return mupdateModel;
    }

    public UpdatePostTabListener getmListener() {
        return mListener;
    }

    public void setmListener(UpdatePostTabListener mListener) {
        this.mListener = mListener;
    }
}
