package com.sisindia.ai.android.rota;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DutyAttendanceIR {

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("DutyOnTime")
    @Expose
    private String dutyOnTime;

    @SerializedName("DutyOffTime")
    @Expose
    private Object dutyOffTime;

    @SerializedName("TasksAssigned")
    @Expose
    private Integer TasksAssigned;

    @SerializedName("TasksCompleted")
    @Expose
    private Integer TasksCompleted;

    @SerializedName("EmployeeId")
    @Expose
    private Integer EmployeeId;

    private Integer seqId;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The dutyOnTime
     */
    public String getDutyOnTime() {
        return dutyOnTime;
    }

    /**
     * @param dutyOnTime The DutyOnTime
     */
    public void setDutyOnTime(String dutyOnTime) {
        this.dutyOnTime = dutyOnTime;
    }

    /**
     * @return The dutyOffTime
     */
    public Object getDutyOffTime() {
        return dutyOffTime;
    }

    /**
     * @param dutyOffTime The DutyOffTime
     */
    public void setDutyOffTime(Object dutyOffTime) {
        this.dutyOffTime = dutyOffTime;
    }

    public Integer getSeqId() {
        return seqId;
    }

    public void setSeqId(Integer seqId) {
        this.seqId = seqId;
    }

    public Integer getTasksAssigned() {
        return TasksAssigned;
    }

    public void setTasksAssigned(Integer tasksAssigned) {
        TasksAssigned = tasksAssigned;
    }

    public Integer getTasksCompleted() {
        return TasksCompleted;
    }

    public void setTasksCompleted(Integer tasksCompleted) {
        TasksCompleted = tasksCompleted;
    }

    public Integer getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        EmployeeId = employeeId;
    }
}