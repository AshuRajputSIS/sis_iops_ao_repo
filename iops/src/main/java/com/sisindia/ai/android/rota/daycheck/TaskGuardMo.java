package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 25-11-2016.
 */
public class TaskGuardMo {
    private  int taskId;
    private String  guardCode;

    public String getGuardCode() {
        return guardCode;
    }

    public void setGuardCode(String guardCode) {
        this.guardCode = guardCode;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}
