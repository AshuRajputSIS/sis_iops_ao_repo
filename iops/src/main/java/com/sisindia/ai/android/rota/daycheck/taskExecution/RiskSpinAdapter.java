package com.sisindia.ai.android.rota.daycheck.taskExecution;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.rota.models.SecurityRiskOptionMO;

import java.util.ArrayList;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class RiskSpinAdapter extends ArrayAdapter<SecurityRiskOptionMO> {

    // Your sent context
    private Context context;
    private LayoutInflater inflater;
    private String tempValues = null;
    private ArrayList<SecurityRiskOptionMO> values = new ArrayList<SecurityRiskOptionMO>();
    //private ContactMO[] values;
    ArrayList<SecurityRiskModelMO> modelObjectlist;

    public RiskSpinAdapter(Context context, int textViewResourceId,
                           ArrayList<SecurityRiskOptionMO> values, ArrayList<SecurityRiskModelMO> modelObject) {
        super(context, textViewResourceId);

        this.context = context;
        this.values = values;
        this.modelObjectlist = modelObject;
        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void updateAdapter(ArrayList<SecurityRiskOptionMO> value) {
        values.addAll(value);
        notifyDataSetChanged();
    }


    public int getCount() {
        return (values.size());
    }

    public SecurityRiskOptionMO getItem(int position) {
        return values.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        return getCustomView(position, convertView, parent);
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        TextView label;
        CustomViewHolder mCustomViewHolder;
        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.commom_textview_row, parent, false);
            mCustomViewHolder = new CustomViewHolder();
            mCustomViewHolder.mItem = (TextView) convertView.findViewById(R.id.commonTV);
            convertView.setTag(mCustomViewHolder);
        } else {
            mCustomViewHolder = (CustomViewHolder) convertView.getTag();
        }
        // Set values for spinner each row
        if (position <= values.size()) {
            /*if(position == 0) {
                mCustomViewHolder.mItem.setText("SELECT OPTION ");
                modelObjectlist.get(position).setmOptionNameId(0);
                modelObjectlist.get(position).setmIsMandatory(0);
                modelObjectlist.get(position).setmControlType(null);
                Util.setmSecurityRiskModelHolder(modelObjectlist);
            }else {*/
                tempValues = values.get(position).getSecurityRiskOptionName();
                mCustomViewHolder.mItem.setText(tempValues);

            }

        return convertView;
    }

    static class CustomViewHolder {
        TextView mItem;

    }
}