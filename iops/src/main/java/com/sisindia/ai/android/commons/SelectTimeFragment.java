package com.sisindia.ai.android.commons;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SelectTimeFragment extends DialogFragment implements
        TimePickerDialog.OnTimeSetListener {
    final static String DAY_CHECK = "Day Checking";
    final static String NIGHT_CHECK = "Night Checking";
    EditText mEdit;
    DateTimeUpdateListener dateTimeUpdateListener;
    View rootLayout;
    private String checkingTye;
    private String time;

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public SelectTimeFragment(View mparentRelativeLayout) {
        rootLayout = mparentRelativeLayout;

    }

    public void setTime(String time) {
        this.time = time;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();

        try {
            if (time != null && !time.isEmpty())
                calendar.setTime(new SimpleDateFormat("hh:mm:ss").parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.MyDatePickerDialogTheme, this, hours, min, false);

        //timePickerDialog.get
        return timePickerDialog;
    }

    public void setTimeListener(DateTimeUpdateListener dateTimeUpdateListener) {
        this.dateTimeUpdateListener = dateTimeUpdateListener;
    }

    public void setCheckingType(String checkingType) {
        this.checkingTye = checkingType;
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        Calendar calender = Calendar.getInstance();
        calender.set(0, 0, 0, hourOfDay, minute);
        Date date = calender.getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
        String requiredFormat = dateFormat.format(date);
        typeCheck(hourOfDay, minute, requiredFormat);
    }


    private void typeCheck(int hourOfDay, int minute, String requiredFormat) {
        dateTimeUpdateListener.changeTime(requiredFormat);
        /*if (DAY_CHECK.equalsIgnoreCase(checkingTye)) {
            if (hourOfDay >= 9 && hourOfDay < 18) {
                dateTimeUpdateListener.changeTime(requiredFormat);
                //dateTimeUpdateListener.changeTime(dateTimeFormatConversionUtil.changeFormatOfAmPm(requiredFormat));
            } else if (hourOfDay == 18 && minute <= 0) {
                dateTimeUpdateListener.changeTime(requiredFormat);
            } else {
                snackBarWithMesg(rootLayout, getResources().getString(R.string.select_day_check_time));
            }

        } else if (NIGHT_CHECK.equalsIgnoreCase(checkingTye)) {
            if (hourOfDay > 17 && hourOfDay <= 23)
                dateTimeUpdateListener.changeTime(requiredFormat);
            else if (hourOfDay == 17 && minute <= 0) {
                dateTimeUpdateListener.changeTime(requiredFormat);
            } else
                snackBarWithMesg(rootLayout, getResources().getString(R.string.select_night_check_time));
        } else {
            dateTimeUpdateListener.changeTime(requiredFormat);
        }*/
    }

    public void snackBarWithMesg(View view, String mesg) {

        Snackbar mSnackbar = Snackbar.make(view, mesg, Snackbar.LENGTH_SHORT);
        snackBarColor(mSnackbar);
        mSnackbar.show();
    }

    private void snackBarColor(Snackbar mSnackbar) {
        View view = mSnackbar.getView();
        mSnackbar.setActionTextColor(Color.WHITE);
        view.setBackgroundResource(R.color.colorPrimary);
    }
}