package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UnitPost {

    @SerializedName("Id")
    public Integer Id;
    @SerializedName("UnitId")
    public Integer UnitId;
    @SerializedName("UnitPostName")
    public String UnitPostName;
    @SerializedName("IsActive")
    public Boolean IsActive;
    @SerializedName("GeoLocation")
    public GeoLocation GeoLocation;
    @SerializedName("Description")
    public String Description;
    @SerializedName("MainGateDistance")
    public Double MainGateDistance;
    @SerializedName("BarrackDistance")
    public Double BarrackDistance;
    @SerializedName("UnitOfficeDistance")
    public Double UnitOfficeDistance;
    @SerializedName("ArmedStrength")
    public Integer ArmedStrength;
    @SerializedName("UnarmedStrength")
    public Integer UnarmedStrength;
    @SerializedName("IsMainGate")
    public Boolean isMainGate;

    @SerializedName("UnitEquipments")

    public List<UnitEquipment> UnitEquipments = new ArrayList<>();
    @SerializedName("UnitPostGeoPoint")
    @Expose
    private List<UnitPostGeoPoint> UnitPostGeoPoint = new ArrayList<UnitPostGeoPoint>();
    @SerializedName("UnitPostLocation")
    private List<UnitPostLocation> UnitPostLocation = new ArrayList<UnitPostLocation>();

    public List<UnitEquipment> getUnitEquipments() {
        return UnitEquipments;
    }

    public void setUnitEquipments(List<UnitEquipment> unitEquipments) {
        UnitEquipments = unitEquipments;
    }

    public Integer getArmedStrength() {
        return ArmedStrength;
    }

    public void setArmedStrength(Integer armedStrength) {
        ArmedStrength = armedStrength;
    }

    public Double getBarrackDistance() {
        return BarrackDistance;
    }

    public void setBarrackDistance(Double barrackDistance) {
        BarrackDistance = barrackDistance;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public GeoLocation getGeoLocation() {
        return GeoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        GeoLocation = geoLocation;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Double getMainGateDistance() {
        return MainGateDistance;
    }

    public void setMainGateDistance(Double mainGateDistance) {
        MainGateDistance = mainGateDistance;
    }

    public Integer getUnarmedStrength() {
        return UnarmedStrength;
    }

    public void setUnarmedStrength(Integer unarmedStrength) {
        UnarmedStrength = unarmedStrength;
    }

    public Integer getUnitId() {
        return UnitId;
    }

    public void setUnitId(Integer unitId) {
        UnitId = unitId;
    }

    /**
     * @return The UnitPostGeoPoint
     */
    public List<UnitPostGeoPoint> getUnitPostGeoPoint() {
        return UnitPostGeoPoint;
    }

    /**
     * @param UnitPostGeoPoint The UnitPostGeoPoint
     */
    public void setUnitPostGeoPoint(List<UnitPostGeoPoint> UnitPostGeoPoint) {
        this.UnitPostGeoPoint = UnitPostGeoPoint;
    }

    /**
     * @return The UnitPostLocation
     */
    public List<UnitPostLocation> getUnitPostLocation() {
        return UnitPostLocation;
    }

    /**
     * @param UnitPostLocation The UnitPostLocation
     */
    public void setUnitPostLocation(List<UnitPostLocation> UnitPostLocation) {
        this.UnitPostLocation = UnitPostLocation;
    }


    public Double getUnitOfficeDistance() {
        return UnitOfficeDistance;
    }

    public void setUnitOfficeDistance(Double unitOfficeDistance) {
        UnitOfficeDistance = unitOfficeDistance;
    }

    public String getUnitPostName() {
        return UnitPostName;
    }

    public void setUnitPostName(String unitPostName) {
        UnitPostName = unitPostName;
    }

    /**
     * @return The isMainGate
     */
    public Boolean getIsMainGate() {
        return isMainGate;
    }

    /**
     * @param isMainGate The IsMainGate
     */
    public void setIsMainGate(Boolean isMainGate) {
        this.isMainGate = isMainGate;
    }
}