package com.sisindia.ai.android.mybarrack.modelObjects;

/**
 * Created by shankar on 28/7/16.
 */

public class BarrackInspectionQuestionOptionMO {
    private Integer optionId;
    private String options;
    private Integer questionId;
    private String question;
    private String controlType;
    private int isMandatory;

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }


    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }


    public void isMandatory(int isMandatory) {
        this.isMandatory = isMandatory;
    }

    public int getIsMandatory() {
        return isMandatory;
    }

    public void IsMandatory(int isMandatory) {
        this.isMandatory = isMandatory;
    }
}
