package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 17-05-2016.
 */
public class UnitDetailsPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Object> addUnitDetailsPostList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;

    public UnitDetailsPostAdapter(Context context) {
        this.context = context;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setPostListData(ArrayList<Object> addPostList) {
        this.addUnitDetailsPostList = addPostList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView = LayoutInflater.from(context).inflate(R.layout.unit_post_row, parent, false);
        recycleViewHolder = new RotaViewHolder(itemView);
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RotaViewHolder rowDataHolder = (RotaViewHolder) holder;
        Object object = getObject(position);
        rowDataHolder.checkingTextview.setText(((PostData) object).getPostName());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.POST_IMAGE_HEIGHT);
        params.width = Util.dpToPx(Constants.POST_IMAGE_HEIGHT);
        int marginsLeft = Util.dpToPx(Constants.POST_IMAGE_MARIN_LEFT);
        int marginsTop = Util.dpToPx(Constants.POST_IMAGE_MARIN_TOP);
        int marginsBottom = Util.dpToPx(Constants.POST_IMAGE_MARIN_BOTTOM);
        int marginRight = Util.dpToPx(Constants.POST_IMAGE_MARIN_LEFT);
        params.setMargins(marginsLeft, marginsTop, marginRight, marginsBottom);

        rowDataHolder.typeIcon.setLayoutParams(params);
        rowDataHolder.typeIcon.setImageResource(((PostData) object).getIcon());
        rowDataHolder.nextIcon.setImageResource(R.drawable.arrow_right);
        String nfcStr = ((PostData) object).getDeviceNo();
        if (!TextUtils.isEmpty(nfcStr))
            rowDataHolder.nfcIcon.setImageResource(R.drawable.scanned_qr_icon);
        else
            rowDataHolder.nfcIcon.setImageResource(R.drawable.unscanned_qr_icon);
    }

    @Override
    public int getItemCount() {
        int count = addUnitDetailsPostList == null ? 0 : addUnitDetailsPostList.size();
        return count;
    }

    public Object getObject(int position) {
        return addUnitDetailsPostList.get(position);
    }

    public class RotaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView checkingTextview;
        ImageView typeIcon, nextIcon, nfcIcon;

        public RotaViewHolder(View view) {
            super(view);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkType);
            this.typeIcon = (ImageView) view.findViewById(R.id.checkTypeImage);
            this.nextIcon = (ImageView) view.findViewById(R.id.nextarrow);
            this.nfcIcon = (ImageView) view.findViewById(R.id.nfcImg);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }
}



