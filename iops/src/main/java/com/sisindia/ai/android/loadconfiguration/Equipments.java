package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Equipments {

@SerializedName("Id")
@Expose
private Integer id;
@SerializedName("EquipmentName")
@Expose
private String equipmentName;
@SerializedName("EquipmentCode")
@Expose
private String equipmentCode;
@SerializedName("EquipmentTypeId")
@Expose
private Integer equipmentTypeId;
@SerializedName("IsActive")
@Expose
private Boolean isActive;
@SerializedName("Description")
@Expose
private String description;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The equipmentName
*/
public String getEquipmentName() {
return equipmentName;
}

/**
* 
* @param equipmentName
* The EquipmentName
*/
public void setEquipmentName(String equipmentName) {
this.equipmentName = equipmentName;
}

/**
* 
* @return
* The equipmentCode
*/
public String getEquipmentCode() {
return equipmentCode;
}

/**
* 
* @param equipmentCode
* The EquipmentCode
*/
public void setEquipmentCode(String equipmentCode) {
this.equipmentCode = equipmentCode;
}

/**
* 
* @return
* The equipmentTypeId
*/
public Integer getEquipmentTypeId() {
return equipmentTypeId;
}

/**
* 
* @param equipmentTypeId
* The EquipmentTypeId
*/
public void setEquipmentTypeId(Integer equipmentTypeId) {
this.equipmentTypeId = equipmentTypeId;
}

/**
* 
* @return
* The isActive
*/
public Boolean getIsActive() {
return isActive;
}

/**
* 
* @param isActive
* The IsActive
*/
public void setIsActive(Boolean isActive) {
this.isActive = isActive;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The Description
*/
public void setDescription(String description) {
this.description = description;
}

    @Override
    public String toString() {
        return "Equipments{" +
                "id=" + id +
                ", equipmentName='" + equipmentName + '\'' +
                ", equipmentCode='" + equipmentCode + '\'' +
                ", equipmentTypeId=" + equipmentTypeId +
                ", isActive=" + isActive +
                ", description='" + description + '\'' +
                '}';
    }
}