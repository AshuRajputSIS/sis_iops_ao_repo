package com.sisindia.ai.android.commons;

/**
 * Created by shankar.k on 02/03/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.sisindia.ai.android.utils.NetworkUtil;


public class NetworkChangeReceiver extends BroadcastReceiver {


    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private final BaseActivity mActivity;


    public NetworkChangeReceiver(BaseActivity activity) {

        this.mActivity = activity;

    }

    public static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI && activeNetwork.isConnectedOrConnecting())
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && activeNetwork.isConnectedOrConnecting())
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = Constants.INTERNETENABLED;
        } else if (conn == TYPE_MOBILE) {
            status = Constants.INTERNETENABLED;
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = Constants.NOINTERNET;
        }
        return status;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = getConnectivityStatusString(context);
        NetworkUtil.isConnected = !status.equals(Constants.NOINTERNET);
            /*if(NetworkUtil.isConnected){
                triggerSyncAdapter();
            }*/
    }
    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);

    }
}