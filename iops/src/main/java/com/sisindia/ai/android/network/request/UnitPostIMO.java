package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UnitPostIMO {

    @SerializedName("PostName")
    @Expose
    private String PostName;
    @SerializedName("PostId")
    @Expose
    private String PostId;
    @SerializedName("NFC")
    @Expose
    private String NFC;
    @SerializedName("SOPAvailable")
    @Expose
    private Boolean SOPAvailable;
    @SerializedName("AttendanceRegisterAvailable")
    @Expose
    private Boolean AttendanceRegisterAvailable;
    @SerializedName("EquipmentList")
    @Expose
    private List<UnitEquipmentIMO> EquipmentList = new ArrayList<UnitEquipmentIMO>();

    /**
     * @return The PostName
     */
    public String getPostName() {
        return PostName;
    }

    /**
     * @param PostName The PostName
     */
    public void setPostName(String PostName) {
        this.PostName = PostName;
    }

    /**
     * @return The PostId
     */
    public String getPostId() {
        return PostId;
    }

    /**
     * @param PostId The PostId
     */
    public void setPostId(String PostId) {
        this.PostId = PostId;
    }

    /**
     * @return The NFC
     */
    public String getNFC() {
        return NFC;
    }

    /**
     * @param NFC The NFC
     */
    public void setNFC(String NFC) {
        this.NFC = NFC;
    }

    /**
     * @return The SOPAvailable
     */
    public Boolean getSOPAvailable() {
        return SOPAvailable;
    }

    /**
     * @param SOPAvailable The SOPAvailable
     */
    public void setSOPAvailable(Boolean SOPAvailable) {
        this.SOPAvailable = SOPAvailable;
    }

    /**
     * @return The AttendanceRegisterAvailable
     */
    public Boolean getAttendanceRegisterAvailable() {
        return AttendanceRegisterAvailable;
    }

    /**
     * @param AttendanceRegisterAvailable The AttendanceRegisterAvailable
     */
    public void setAttendanceRegisterAvailable(Boolean AttendanceRegisterAvailable) {
        this.AttendanceRegisterAvailable = AttendanceRegisterAvailable;
    }

    /**
     * @return The EquipmentList
     */
    public List<UnitEquipmentIMO> getEquipmentList() {
        return EquipmentList;
    }

    /**
     * @param EquipmentList The EquipmentList
     */
    public void setEquipmentList(List<UnitEquipmentIMO> EquipmentList) {
        this.EquipmentList = EquipmentList;
    }

    @Override
    public String toString() {
        return "UnitPostIMO{" +
                "PostName='" + PostName + '\'' +
                ", PostId='" + PostId + '\'' +
                ", NFC='" + NFC + '\'' +
                ", SOPAvailable=" + SOPAvailable +
                ", AttendanceRegisterAvailable=" + AttendanceRegisterAvailable +
                ", EquipmentList=" + EquipmentList +
                '}';
    }
}
