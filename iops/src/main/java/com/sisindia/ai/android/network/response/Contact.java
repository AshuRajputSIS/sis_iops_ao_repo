package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("FirstName")

    public String FirstName;
    @SerializedName("LastName")

    public String LastName;
    @SerializedName("MiddleName")

    public String MiddleName;
    @SerializedName("Sex")

    public String Sex;
    @SerializedName("MobileNo")

    public String MobileNo;
    @SerializedName("PhoneNo")

    public String PhoneNo;
    @SerializedName("EmailId")

    public String EmailId;
    @SerializedName("AlternateEmailId")

    public String AlternateEmailId;
    @SerializedName("Designation")

    public String Designation;
    @SerializedName("AlternateMobileNo")

    public String AlternateMobileNo;

    public String getAlternateEmailId() {
        return AlternateEmailId;
    }

    public void setAlternateEmailId(String alternateEmailId) {
        AlternateEmailId = alternateEmailId;
    }

    public String getAlternateMobileNo() {
        return AlternateMobileNo;
    }

    public void setAlternateMobileNo(String alternateMobileNo) {
        AlternateMobileNo = alternateMobileNo;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }
}