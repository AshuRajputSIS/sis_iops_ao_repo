package com.sisindia.ai.android.utils;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.sisindia.ai.android.commons.ScheduleDeletionService;
import com.sisindia.ai.android.commons.ScheduleSyncJobService;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.ScheduleDeleteTimes;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.scheduleDelete.ScheduleDelete;
import com.sisindia.ai.android.syncadpter.DutyAttendanceSyncing;

/**
 * Created by compass on 9/5/2017.
 */

class ScheduleAttendanceService extends IntentService {
    private AppPreferences appPreferences;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private static final String TAG = ScheduleAttendanceService.class.getSimpleName();

    public ScheduleAttendanceService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        appPreferences = new AppPreferences(ScheduleAttendanceService.this);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        new DutyAttendanceSyncing(this);

    }
}
