package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.myperformance.TimeLineModel;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 16-09-2016.
 */
public class ActivityLogDB extends SISAITrackingDB {

    public ActivityLogDB(Context context) {
        super(context);
    }

    public synchronized void insertActivityLogDetails(TimeLineModel timeLineModel) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();

        synchronized (sqlite) {

            try {
                if (timeLineModel != null) {

                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TableNameAndColumnStatement.ACTIVITY, timeLineModel.getActivity());
                    contentValues.put(TableNameAndColumnStatement.UNIT_NAME, timeLineModel.getLocation());
                    contentValues.put(TableNameAndColumnStatement.TASK_ID, timeLineModel.getTaskId());
                    contentValues.put(TableNameAndColumnStatement.TASK_NAME, timeLineModel.getTaskName());
                    contentValues.put(TableNameAndColumnStatement.TASK_STATUS, timeLineModel.getStatus());
                    contentValues.put(TableNameAndColumnStatement.DATE, timeLineModel.getDate());
                    contentValues.put(TableNameAndColumnStatement.TIME, timeLineModel.getStartTime());


                    sqlite.insert(TableNameAndColumnStatement.ACTIVITY_LOG_TABLE, null, contentValues);
                    sqlite.setTransactionSuccessful();
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {

                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }

        }
    }

    public List<TimeLineModel> getActivityLogs() {

        List<TimeLineModel> timeLineModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String query = "select * from " + TableNameAndColumnStatement.ACTIVITY_LOG_TABLE;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        TimeLineModel timeLineModel = new TimeLineModel();

                        timeLineModel.setActivity(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ACTIVITY)));
                        timeLineModel.setLocation(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        timeLineModel.setStatus(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS)));
                        timeLineModel.setTaskId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        timeLineModel.setTaskName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_NAME)));
                        timeLineModel.setStartTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TIME)));
                        timeLineModel.setDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DATE)));
                        timeLineModelList.add(timeLineModel);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return timeLineModelList;
    }

    /*
    Values identification:
    1) 0 - Duty On
    2)-3 - Duty Off
    */
    public List<TimeLineModel> getConveyanceTimeLine(String currentDate) {

        List<TimeLineModel> timeLineModelList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        int taskCounter = 0;
        /*String query = "select ag.activity,ag.unit_name,case when activity='Duty on' then 'Duty on' else task_name end Task_Type,u.task_status_id,u.task_type_id, " +
                "substr(u.actual_execution_start_date_time,11,6) as startTime, substr(u.actual_execution_end_date_time ,11,6) as endTime from " +
                "activity_log ag left join Task u on ag.task_id=u.task_id where activity not in ('Task Creation','Check Out')";*/

        /*String query = "select * from (select '__' UnitName,Status TaskType, Timeline Date, '--' startTime, '--' EndTime, Timeline Timeline " +
                "from (select duty_on_date_time Timeline, 0 Status from duty_attendance where substr(duty_on_date_time ,0,11)='" + currentDate + "' union all " +
                "select duty_off_date_time, -1 Status from duty_attendance where substr(duty_off_date_time,0,11)='" + currentDate + "')A union all select " +
                "unit_name,task_type_id ,substr(actual_execution_end_date_time,0,11) Date,substr(actual_execution_start_date_time,11,6) StartTime, " +
                "substr(actual_execution_end_date_time,11,6) EndTime,actual_execution_end_date_time Timeline from task where " +
                "substr(actual_execution_end_date_time,0,11)='" + currentDate + "' and task_status_id in (3,4))A where substr(Timeline,0,11)='" + currentDate + "' order by Timeline ";*/

        String query = "select * from (select 'NA' UnitName,Status TaskTypeId,'' StartTime,'' EndTime,Timeline TimeLine, case when status=0 " +
                "then 'DutyOn' when status=-3 then 'DutyOff' end TaskTypeName from (select duty_on_date_time Timeline,0 Status from duty_attendance where " +
                "substr(duty_on_date_time ,0,11)='" + currentDate + "' union all select duty_off_date_time,-3 Status from duty_attendance where " +
                "substr(duty_off_date_time,0,11)='" + currentDate + "')A union all select unit_name,task_type_id," +
                "substr(actual_execution_start_date_time,11,6) StartTime, substr(actual_execution_end_date_time,11,6) EndTime,actual_execution_end_date_time " +
                "Timeline,taskList.rota_task_name TaskTypeName from task t inner join rota_task_activity_list taskList on t.Task_type_id=taskList.rota_task_activity_id where " +
                "substr(actual_execution_end_date_time,0,11)='" + currentDate + "' and task_status_id in (3,4))A where substr(Timeline,0,11)='" + currentDate + "' order by Timeline";

        Timber.d("TimeLine Query %s", query);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        TimeLineModel timeLineModel = new TimeLineModel();

                        timeLineModel.setLocation(cursor.getString(cursor.getColumnIndex("UnitName")));
                        timeLineModel.setStatus(cursor.getString(cursor.getColumnIndex("TaskTypeName")));
                        timeLineModel.setStartTime(cursor.getString(cursor.getColumnIndex("StartTime")));
                        timeLineModel.setEndTime(cursor.getString(cursor.getColumnIndex("EndTime")));
                        timeLineModel.setDutyOnOffDateTime(cursor.getString(cursor.getColumnIndex("TimeLine")));
                        int taskTypeId = cursor.getInt(cursor.getColumnIndex("TaskTypeId"));
                        timeLineModel.setTaskTypeId(taskTypeId);

                        if (taskTypeId > 0 || taskTypeId == -1 || taskTypeId == -2) {
                            taskCounter = taskCounter + 1;
                            timeLineModel.setTaskCounter(taskCounter);
                        }

                        timeLineModelList.add(timeLineModel);

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return timeLineModelList;
    }

    public boolean isUnSyncedTasksAreThere() {
        String selectQuery = "select count(1) as unSyncedCount from activity_log ag inner join Task u on ag.task_id=u.task_id where task_status_id=4";

        Cursor cursor = null;
        int count = 0;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            if (cursor != null && !cursor.isClosed()) {

                if (cursor.moveToFirst()) {
                    do {
                        count = cursor.getInt(cursor.getColumnIndex("unSyncedCount"));
                    } while (cursor.moveToNext());
                }

                if (count > 0)
                    return true;
                else
                    return false;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return false;
    }

    public void deleteActivityLogs() {
        SQLiteDatabase sqlite = this.getWritableDatabase();

        String deleteQuery = "delete from " + TableNameAndColumnStatement.ACTIVITY_LOG_TABLE;
        Cursor cursor = null;

        synchronized (sqlite) {

            try {
                cursor = sqlite.rawQuery(deleteQuery, null);
                Timber.d("cursor detail object  %s", cursor.getCount());
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }
    }

    public int getActivityLogsCount() {

        int count = 0;
        SQLiteDatabase sqlite = null;
        String query = "select * " + " from " + TableNameAndColumnStatement.ACTIVITY_LOG_TABLE;
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);

            if (cursor != null && !cursor.isClosed()) {
                if (cursor.moveToFirst()) {
                    do {
                        count = cursor.getCount();
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

        return count;
    }
}
