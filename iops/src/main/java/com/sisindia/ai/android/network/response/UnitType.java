package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class UnitType {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("Name")

    public String Name;
    @SerializedName("MinGuardSize")

    public Integer MinGuardSize;
    @SerializedName("MaxGuardSize")

    public Object MaxGuardSize;
    @SerializedName("Description")

    public String Description;
    @SerializedName("DayCheckFrequencyInMonth")

    public Integer DayCheckFrequencyInMonth;
    @SerializedName("NightCheckFrequencyInMonth")

    public Integer NightCheckFrequencyInMonth;
    @SerializedName("ClientCoordinationFrequencyInMonth")

    public Integer ClientCoordinationFrequencyInMonth;
    @SerializedName("MinInspectorSize")

    public Object MinInspectorSize;
    @SerializedName("MaxInspectorSize")

    public Object MaxInspectorSize;

}