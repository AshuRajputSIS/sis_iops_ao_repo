package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 12-05-2016.
 */
public class FileUploadData {
    @SerializedName("attachmentPath")
    @Expose
    private String attachmentPath;

    /**
     * @return The attachmentPath
     */
    public String getAttachmentPath() {
        return attachmentPath;
    }

    /**
     * @param attachmentPath The attachmentPath
     */
    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    @Override
    public String toString() {
        return "FileUploadData{" +
                "attachmentPath='" + attachmentPath + '\'' +
                '}';
    }
}
