package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shruti on 29/7/16.
 */
public class BarrackStrengthMO implements Serializable {

    @SerializedName("Capacity")
    @Expose
    private Integer capacity;
    @SerializedName("LastBarrackStrength")
    @Expose
    private Integer lastBarrackStrength;

    /**
     *
     * @return
     * The capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     *
     * @param capacity
     * The Capacity
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    /**
     *
     * @return
     * The lastBarrackStrength
     */
    public Integer getLastBarrackStrength() {
        return lastBarrackStrength;
    }

    /**
     *
     * @param lastBarrackStrength
     * The LastBarrackStrength
     */
    public void setLastBarrackStrength(Integer lastBarrackStrength) {
        this.lastBarrackStrength = lastBarrackStrength;
    }

}
