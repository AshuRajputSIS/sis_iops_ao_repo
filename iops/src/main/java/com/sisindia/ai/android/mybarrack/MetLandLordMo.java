package com.sisindia.ai.android.mybarrack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.barrackInspection.QuestionsAndOptionMO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by compass on 7/6/2017.
 */

 public class MetLandLordMo {
    @SerializedName("QuestionsAndOptions")
    @Expose
    private List<QuestionsAndOptionMO> questionsAndOptions = new ArrayList<QuestionsAndOptionMO>();

    /**
     *
     * @return
     * The questionsAndOptions
     */
    public List<QuestionsAndOptionMO> getQuestionsAndOptions() {
        return questionsAndOptions;
    }

    /**
     *
     * @param questionsAndOptions
     * The QuestionsAndOptions
     */
    public void setQuestionsAndOptions(List<QuestionsAndOptionMO> questionsAndOptions) {
        this.questionsAndOptions = questionsAndOptions;
    }




}
