package com.sisindia.ai.android.rota.rotaCompliance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Durga Prasad on 31-01-2017.
 */
public class MonthlyDailyRotaCompliance implements Parcelable {

    @SerializedName("MonthlyCompliance")
    public String monthlyCompliance;

    @SerializedName("DailyCompliance")
    public String DailyCompliance;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.monthlyCompliance);
        dest.writeString(this.DailyCompliance);
    }

    public MonthlyDailyRotaCompliance() {
    }

    protected MonthlyDailyRotaCompliance(Parcel in) {
        this.monthlyCompliance = in.readString();
        this.DailyCompliance = in.readString();
    }

    public static final Parcelable.Creator<MonthlyDailyRotaCompliance> CREATOR = new Parcelable.Creator<MonthlyDailyRotaCompliance>() {
        @Override
        public MonthlyDailyRotaCompliance createFromParcel(Parcel source) {
            return new MonthlyDailyRotaCompliance(source);
        }

        @Override
        public MonthlyDailyRotaCompliance[] newArray(int size) {
            return new MonthlyDailyRotaCompliance[size];
        }
    };
}
