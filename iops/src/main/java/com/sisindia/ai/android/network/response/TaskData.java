package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskData {

    @SerializedName("taskId")
    @Expose
    private Integer taskId;

    /**
     * @return The taskId
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * @param taskId The taskId
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "TaskData{" +
                "taskId=" + taskId +
                '}';
    }
}