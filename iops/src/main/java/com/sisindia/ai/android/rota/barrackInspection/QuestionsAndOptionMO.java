package com.sisindia.ai.android.rota.barrackInspection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 3/8/16.
 */

public class QuestionsAndOptionMO {


    @SerializedName("QuestionId")
    @Expose
    private Integer questionId;
    @SerializedName("OptionId")
    @Expose
    private Integer optionId;
    @SerializedName("Remarks")
    @Expose
    private String remarks;

    /**
     *
     * @return
     * The questionId
     */
    public Integer getQuestionId() {
        return questionId;
    }

    /**
     *
     * @param questionId
     * The QuestionId
     */
    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    /**
     *
     * @return
     * The optionId
     */
    public Integer getOptionId() {
        return optionId;
    }

    /**
     *
     * @param optionId
     * The OptionId
     */
    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    /**
     *
     * @return
     * The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     *
     * @param remarks
     * The remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


}
