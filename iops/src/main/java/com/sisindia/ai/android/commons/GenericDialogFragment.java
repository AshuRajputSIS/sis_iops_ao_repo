package com.sisindia.ai.android.commons;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.sisindia.ai.android.R;

/**
 * Created by Durga Prasad on 15-07-2016.
 */
public class GenericDialogFragment extends DialogFragment {
    public static final String TAG = "GenericDialogFragment";
    private View titleView;
    private String message="";
    private Context mContext;
    private DialogClickListener dialogClickListener;
    private String title;
    private View contentView;
    private String positiveButtonLabel;
    private String negativeButtonLabel;
    private AdapterView.OnItemClickListener onItemClickListener;
    private int color;
    private boolean isCancelable;


    public GenericDialogFragment() {
        // Required empty public constructor
    }


    public static GenericDialogFragment newInstance() {
        GenericDialogFragment genericDialogFragment = new GenericDialogFragment();
        Bundle args = new Bundle();
        genericDialogFragment.setArguments(args);
        return genericDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if(context instanceof DialogClickListener){
            dialogClickListener = (DialogClickListener) context;
        }
    }




    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;

    }

    public void setTitleView(View titleView,String title){

        this.titleView = titleView;
        this.title = title;

    }

    public void setContentView(View contentView){
         this.contentView = contentView;
    }

    public void setPositiveButtonLabel(String positiveButtonLabel){
       this.positiveButtonLabel = positiveButtonLabel;
    }

    public void setNegativeButtonLabel(String negativeButtonLabel){
       this.negativeButtonLabel = negativeButtonLabel;
    }

    public void setCancelable(boolean isCancelable){
        this.isCancelable = isCancelable;
    }

    public void setTitleTextColor(int color){
        this.color = color;
    }




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(isCancelable);
        TextView dialogTitle = (TextView) titleView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        dialogTitle.setTextColor(color);
        builder.setCustomTitle(titleView);
        if(positiveButtonLabel != null) {
            builder.setPositiveButton(positiveButtonLabel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        if(negativeButtonLabel != null){
            builder.setNeutralButton(positiveButtonLabel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //dialog.dismiss();
                }
            });
        }
        builder.setView(contentView);

        return builder.create();
    }

}
