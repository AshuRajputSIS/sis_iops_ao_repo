package com.sisindia.ai.android.rota;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.AnnualKitReplacementFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 6/25/2018.
 */

public class AKRActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.akr_activity);

        ButterKnife.bind(this);
        setUpToolBar();

        AnnualKitReplacementFragment kitReplacementFragment = AnnualKitReplacementFragment.newInstance("", "");
        addFragment(kitReplacementFragment, getResources().getString(R.string.kitReplacementFragment));

    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.annual_kit_replacement));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void addFragment(final Fragment fragment, final String fragmentName) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.container_fragment, fragment, fragmentName);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
