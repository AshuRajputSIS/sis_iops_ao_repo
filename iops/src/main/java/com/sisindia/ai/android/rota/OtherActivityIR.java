package com.sisindia.ai.android.rota;


import com.google.gson.annotations.SerializedName;

public class OtherActivityIR {

    @SerializedName("GEOLocation")
    private String geoLocation ;
@SerializedName("ReasonId")
private Integer reasonId;
@SerializedName("Description")
private String description;

/**
* 
* @return
* The reasonId
*/
public Integer getReasonId() {
return reasonId;
}

/**
* 
* @param reasonId
* The ReasonId
*/
public void setReasonId(Integer reasonId) {
this.reasonId = reasonId;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The Description
*/
public void setDescription(String description) {
this.description = description;
}


    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }
}