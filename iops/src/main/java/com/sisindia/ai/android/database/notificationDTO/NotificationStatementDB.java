package com.sisindia.ai.android.database.notificationDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationIR;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 11/11/16.
 */

public class NotificationStatementDB extends SISAITrackingDB {

    public NotificationStatementDB(Context context) {
        super(context);
    }

    public synchronized List<NotificationIR> getNotificationDataToSync() {
        SQLiteDatabase sqlite = null;

        List<NotificationIR> notificationIRList = new ArrayList<>();
        String selectQuery = " SELECT *" +
                " FROM " +
                TableNameAndColumnStatement.NOTIFICATION_RECEIPT +
                " where " +
                TableNameAndColumnStatement.IS_SYNCED + "='" + 0 + "'";


        Timber.d("getNotificationDataToSync selectQuery  %s", selectQuery);

        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        NotificationIR notificationIR = new NotificationIR();
                        notificationIR.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)));
                        notificationIR.setRecipientId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RECIPIENT_ID)));
                        notificationIR.setCallbackUrl(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CALLBACK_URL)));
                        notificationIR.setIsSuccessful(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_SUCCESSFUL)));
                        notificationIR.setAppNotificationId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.NOTIFICATION_ID)));
                        notificationIRList.add(notificationIR);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return notificationIRList;

    }

    public synchronized void deleteRow(int id) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            sqlite.delete(TableNameAndColumnStatement.NOTIFICATION_RECEIPT, TableNameAndColumnStatement.ID + "=" + id, null);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public synchronized void newNotificationInsertion(NotificationReceivingMO notificationReceivingMO) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        if (notificationReceivingMO != null) {
            try {
                ContentValues values = new ContentValues();
                values.put(TableNameAndColumnStatement.NOTIFICATION_ID, notificationReceivingMO.getId());
                values.put(TableNameAndColumnStatement.CALLBACK_URL, notificationReceivingMO.getCallbackUrl());
                values.put(TableNameAndColumnStatement.NAME, notificationReceivingMO.getName());
                values.put(TableNameAndColumnStatement.NOTIFICATION_TYPE, notificationReceivingMO.getType());
                values.put(TableNameAndColumnStatement.TITLE, notificationReceivingMO.getTitle());
                values.put(TableNameAndColumnStatement.RECEIVED_DATE_TIME, notificationReceivingMO.getRecievedDateTime());
                values.put(TableNameAndColumnStatement.DELETED_ID, notificationReceivingMO.getDeleteId());
                values.put(TableNameAndColumnStatement.APP_USER_ID, notificationReceivingMO.getAppUserId());
                values.put(TableNameAndColumnStatement.PRIORITY, notificationReceivingMO.getPriority());
                values.put(TableNameAndColumnStatement.MESSAGE, notificationReceivingMO.getMessage());
                values.put(TableNameAndColumnStatement.UNIT_LIST, notificationReceivingMO.getUnitList());

                sqlite.insertOrThrow(TableNameAndColumnStatement.NOTIFICATION_LIST, null, values);
                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

    }

    public List<NotificationReceivingMO> getNotificationToDisplay(SQLiteDatabase sqLiteDatabase) {
        List<NotificationReceivingMO> notificationReceivingMOList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = " SELECT * FROM" + TableNameAndColumnStatement.NOTIFICATION_LIST + " order by " + TableNameAndColumnStatement
                .RECEIVED_DATE_TIME + " desc ";

        Timber.d("Notifications selectQuery  %s", selectQuery);

        Cursor cursor = null;
        try {
            if (sqLiteDatabase == null) {
                sqlite = this.getReadableDatabase();
            }
            cursor = sqlite.rawQuery(selectQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        NotificationReceivingMO notificationReceivingMO = new NotificationReceivingMO();
                        notificationReceivingMO.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.NOTIFICATION_ID)));
                        notificationReceivingMO.setAppUserId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.APP_USER_ID)));
                        notificationReceivingMO.setCallbackUrl(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CALLBACK_URL)));
                        notificationReceivingMO.setDeleteId(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DELETED_ID)));
                        notificationReceivingMO.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.NAME)));
                        notificationReceivingMO.setUnitList(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_LIST)));
                        notificationReceivingMO.setPriority(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.PRIORITY)));
                        notificationReceivingMO.setRecievedDateTime(cursor.getLong(cursor.getColumnIndex(TableNameAndColumnStatement.RECEIVED_DATE_TIME)));
                        notificationReceivingMO.setTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.TITLE)));
                        notificationReceivingMO.setMessage(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MESSAGE)));
                        notificationReceivingMO.setType(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.NOTIFICATION_TYPE)));


                        notificationReceivingMOList.add(notificationReceivingMO);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {

            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return notificationReceivingMOList;
    }

    public void deleteNotificationAfterOneDay(int notificationId) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            sqlite.delete(TableNameAndColumnStatement.NOTIFICATION_LIST,
                    TableNameAndColumnStatement.NOTIFICATION_ID + "=" + notificationId, null);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    public void updateSyncForNotification(int notificationId) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.NOTIFICATION_RECEIPT +
                " SET " + TableNameAndColumnStatement.IS_SYNCED + " =  1" +
                " WHERE " + TableNameAndColumnStatement.NOTIFICATION_ID + " = " + notificationId;

        Timber.d("FCM-UpdateNotificationReceipt  %s", updateQuery);
        Cursor cursor = null;
        try {
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("FCM-UpdateImprovementQuery  %s", cursor.getCount());
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }
}
