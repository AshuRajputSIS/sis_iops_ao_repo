package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitContactIR {

@SerializedName("Id")
@Expose
private Integer id;
@SerializedName("UnitId")
@Expose
private Integer unitId;
@SerializedName("FirstName")
@Expose
private String firstName;
@SerializedName("LastName")
@Expose
private String lastName;
@SerializedName("EmailId")
@Expose
private String emailId;
@SerializedName("ContactNo")
@Expose
private String contactNo;
@SerializedName("Designation")
@Expose
private String designation;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The unitId
*/
public Integer getUnitId() {
return unitId;
}

/**
* 
* @param unitId
* The UnitId
*/
public void setUnitId(Integer unitId) {
this.unitId = unitId;
}

/**
* 
* @return
* The firstName
*/
public String getFirstName() {
return firstName;
}

/**
* 
* @param firstName
* The FirstName
*/
public void setFirstName(String firstName) {
this.firstName = firstName;
}

/**
* 
* @return
* The lastName
*/
public String getLastName() {
return lastName;
}

/**
* 
* @param lastName
* The LastName
*/
public void setLastName(String lastName) {
this.lastName = lastName;
}

/**
* 
* @return
* The emailId
*/
public String getEmailId() {
return emailId;
}

/**
* 
* @param emailId
* The EmailId
*/
public void setEmailId(String emailId) {
this.emailId = emailId;
}

/**
* 
* @return
* The contactNo
*/
public String getContactNo() {
return contactNo;
}

/**
* 
* @param contactNo
* The ContactNo
*/
public void setContactNo(String contactNo) {
this.contactNo = contactNo;
}

/**
* 
* @return
* The designation
*/
public String getDesignation() {
return designation;
}

/**
* 
* @param designation
* The Designation
*/
public void setDesignation(String designation) {
this.designation = designation;
}

    @Override
    public String toString() {
        return "UnitContactIR{" +
                "id='" + id + '\'' +
                ", unitId='" + unitId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailId='" + emailId + '\'' +
                ", contactNo='" + contactNo + '\'' +
                ", designation='" + designation + '\'' +
                '}';
    }
}