package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.MetaDataIMO;
import com.sisindia.ai.android.network.response.AddMetadataResponse;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */
public class MetaDataSyncing extends CommonSyncDbOperation {
    private Context mContext;
    private SISClient sisClient;
    private HashMap<Integer, MetaDataIMO> metadataInputRequestList;
    private int taskId;
    private AppPreferences appPreferences;
    private int issueId;
    private int kitDistributionId;
    private int unitPostId;
    private int lookUpType;
    private int unitRaisingDetailId;
    private int unitRaisingCancellationId;
    private int unitRaisingDeferredId;
    private int barrackId;

    public MetaDataSyncing(Context mcontext) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        appPreferences = new AppPreferences(mContext);
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
        metaDataSyncing();
    }

    public void setTaskWithIssues(int taskId, int lookUpType) {
        this.taskId = taskId;
        this.lookUpType = lookUpType;
        metaDataSyncing();
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
        metaDataSyncing();
    }

    public void setUnitPostId(int unitPostId) {
        this.unitPostId = unitPostId;
        metaDataSyncing();
    }

    public void setKitDistributionId(int kitDistributionId) {
        this.kitDistributionId = kitDistributionId;
        metaDataSyncing();
    }

    public void setUnitRaisingId(int unitRaisingDetailId) {
        this.unitRaisingDetailId = unitRaisingDetailId;
        metaDataSyncing();
    }

    public void setUnitRaisingCancellationId(int unitRaisingCancellationId) {
        this.unitRaisingCancellationId = unitRaisingCancellationId;
        metaDataSyncing();
    }

    public void setUnitRaisingDeferredId(int unitRaisingDeferredId) {
        this.unitRaisingDeferredId = unitRaisingDeferredId;
        metaDataSyncing();
    }

    public void setBarrackId(int barrackId) {
        this.barrackId = barrackId;
        metaDataSyncing();
    }

    public void updateMetaDataSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                dependentSyncsStatus.isMetaDataSynced = count;
                if (taskId != 0) {
                    if (taskId == dependentSyncsStatus.taskId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.TASK_RELATED_SYNC, taskId);
                    }
                }
                if (issueId != 0) {
                    if (issueId == dependentSyncsStatus.issueId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.ISSUE_RELATED_SYNC, issueId);
                    }
                }
                if (unitPostId != 0) {
                    if (unitPostId == dependentSyncsStatus.unitPostId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.UNIT_POST_RELATED_SYNC, unitPostId);
                    }
                }
                if (kitDistributionId != 0) {
                    if (kitDistributionId == dependentSyncsStatus.kitDistributionId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.KIT_DISTRIBUTION_RELATED_SYNC, kitDistributionId);
                    }
                }
                if (unitRaisingDetailId != 0) {
                    if (unitRaisingDetailId == dependentSyncsStatus.unitRaisingId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_SYNC, unitRaisingDetailId);
                    }
                }
                if (unitRaisingDeferredId != 0) {
                    if (unitRaisingDeferredId == dependentSyncsStatus.unitRaisingDeferredId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_DEFERRED_SYNC, unitRaisingDeferredId);
                    }
                }
                if (unitRaisingCancellationId != 0) {
                    if (unitRaisingCancellationId == dependentSyncsStatus.unitRaisingCancellationId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.UNIT_RAISING_CANCELLATION_SYNC, unitRaisingCancellationId);
                    }
                }
                if (barrackId != 0) {
                    if (barrackId == dependentSyncsStatus.barrackId) {
                        updateMetaDataSyncCount(dependentSyncsStatus, Constants.BARRACK_POST_SYNC, barrackId);
                    }
                }
            }
        }
    }

    private void updateMetaDataSyncCount(DependentSyncsStatus dependentSyncsStatus, int syncType, int id) {
        dependentSyncDb.updateDependentSyncDetails(id, syncType,
                TableNameAndColumnStatement.IS_METADATA_SYNCED,
                dependentSyncsStatus.isMetaDataSynced, dependentSyncsStatus.taskTypeId
        );
    }

    public MetaDataSyncing(Context mcontext, int issueId, boolean adhocIssue) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        this.issueId = issueId;
        appPreferences = new AppPreferences(mContext);
        metaDataSyncing();
    }

    private synchronized void metaDataSyncing() {
        metadataInputRequestList = getMetaDataDetails();
        if (metadataInputRequestList != null && metadataInputRequestList.size() != 0) {
            updateMetaDataSyncCount(metadataInputRequestList.size());
            for (Integer key : metadataInputRequestList.keySet()) {
                MetaDataIMO metaDataIMO = metadataInputRequestList.get(key);
                metaDataApiSync(metaDataIMO, key);

                Timber.d(Constants.TAG_SYNCADAPTER + "metaDataSyncing count   %s", "count: ");
                Timber.d(Constants.TAG_SYNCADAPTER + "metaDataSyncing   %s", "key: " + key + " value: " + metadataInputRequestList.get(key));
            }
        } else {
            updateMetaDataSyncCount(0);
            dependentFailureSync();
            Timber.d(Constants.TAG_SYNCADAPTER + "SyncingMetaData -->nothing to sync ");
        }
    }


    /**
     * being used in the postcheck fragment for syncing the add post data to server
     *
     * @return
     */
    private HashMap<Integer, MetaDataIMO> getMetaDataDetails() {
        HashMap<Integer, MetaDataIMO> metadataInputList = new HashMap<>();
        SQLiteDatabase sqlite = null;
        String selectQuery = "";
        String attachmentReferenceId = "";
        if (issueId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + issueId;
        } else if (unitPostId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitPostId;
        } else if (kitDistributionId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + kitDistributionId;
        } else if (unitRaisingDetailId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingDetailId;
        } else if (unitRaisingDeferredId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingDeferredId;
        } else if (unitRaisingCancellationId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + unitRaisingCancellationId;
        } else if (barrackId != 0) {
            attachmentReferenceId = TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " = " + barrackId;
        }

        if (taskId == 0 && !TextUtils.isEmpty(attachmentReferenceId)) {
            selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where " + attachmentReferenceId + " and (" +
                    TableNameAndColumnStatement.IS_NEW + " = 1 OR " + TableNameAndColumnStatement.IS_SYNCED + " = 0 ) ";
        } else {
            if (Constants.LOOKUP_TYPE_AUDIO == lookUpType) {
                selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where " +
                        TableNameAndColumnStatement.TASK_ID + " >  " + 0 + " and " + TableNameAndColumnStatement.TASK_ID + " =  " + taskId + " and " +
                        TableNameAndColumnStatement.ATTACHMENT_TYPE_ID + " =  " + Constants.LOOKUP_TYPE_AUDIO + " and ( " +
                        TableNameAndColumnStatement.IS_NEW + "= 1 OR " + TableNameAndColumnStatement.IS_SYNCED + " = 0 ) ";
            } else {
                selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + " where " +
                        TableNameAndColumnStatement.TASK_ID + " >  " + 0 + " and " + TableNameAndColumnStatement.TASK_ID + " =  " + taskId + " and  ( " +
                        TableNameAndColumnStatement.IS_NEW + "= 1 OR " + TableNameAndColumnStatement.IS_SYNCED + " = 0 ) ";
            }
        }

        Timber.d("getMetaDataDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            if (cursor != null && !cursor.isClosed()) {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        MetaDataIMO metaData = new MetaDataIMO();
                        metaData.setAttachmentTitle(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_NAME)));
                        metaData.setAttachmentExtension(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_EXTENSION)));
                        metaData.setSizeInKB(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.FILE_SIZE)));
                        metaData.setAttachmentSourceTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE)));
                        metaData.setAttachmentSourceId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID)));
                        metaData.setSequenceNo(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.SEQUENCE_NO)));
                        metaData.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        metaData.setUnitTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        metaData.setAttachmentTypeId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_TYPE_ID)));
                        metaData.setAttachmentSourceCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.ATTACHMENT_SOURCE_CODE)));
                        metadataInputList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), metaData);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if (sqlite != null) {
                sqlite.close();
            }
            Timber.d(Constants.TAG_SYNCADAPTER + "MetaDataIMO object  %s", IOPSApplication.getGsonInstance().toJson(metadataInputList));
        }
        return metadataInputList;
    }

    private void metaDataApiSync(final MetaDataIMO metaDataIMO, final int key) {

        Timber.d(Constants.TAG_SYNCADAPTER + "SyncingMetaData object  %s", metaDataIMO);
        sisClient.getApi().syncAttachmentMetadata(metaDataIMO, new Callback<AddMetadataResponse>() {
            @Override
            public void success(AddMetadataResponse unitResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER + "SyncingMetaData response  %s", unitResponse);

                switch (unitResponse.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        if (updateAttachmentIdMetadataTable(unitResponse.getData().getAttachmentId(),
                                unitResponse.getData().getAttachmentPath(),
                                key)) {
                            Timber.d(Constants.TAG_SYNCADAPTER + "metadata status  %s", "successfully");
                            updateStatus();
                        }
                        // uplaod  the Attachment id to Metadata tables
                        break;
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Util.printRetorfitError("SyncingMetaData Error", error);
            }
        });
    }

    private void updateStatus() {
        if (taskId != 0) {
            updateSyncCountAndStatus(taskId, TableNameAndColumnStatement.TASK_ID);
        } else if (issueId != 0) {
            updateSyncCountAndStatus(issueId, TableNameAndColumnStatement.ISSUE_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setIssueId(issueId);
        } else if (kitDistributionId != 0) {
            updateSyncCountAndStatus(kitDistributionId, TableNameAndColumnStatement.KIT_DISTRIBUTION_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setKitDistributionId(kitDistributionId);
        } else if (unitPostId != 0) {
            updateSyncCountAndStatus(unitPostId, TableNameAndColumnStatement.UNIT_POST_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setUnitPostId(unitPostId);
        } else if (unitRaisingDetailId != 0) {
            updateSyncCountAndStatus(unitRaisingDetailId, TableNameAndColumnStatement.UNIT_RAISING_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setUnitRaisingId(unitRaisingDetailId);
        } else if (unitRaisingCancellationId != 0) {
            updateSyncCountAndStatus(unitRaisingCancellationId, TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setUnitRaisingCancellationId(unitRaisingCancellationId);
        } else if (unitRaisingDeferredId != 0) {
            updateSyncCountAndStatus(unitRaisingDeferredId, TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setUnitRaisingDeferredId(unitRaisingDeferredId);
        } else if (barrackId != 0) {
            updateSyncCountAndStatus(barrackId, TableNameAndColumnStatement.BARRACK_ID);
            ImageSyncing imageSyncing = new ImageSyncing(mContext);
            imageSyncing.setBarrackId(barrackId);
        }
    }

    /**
     * @param attach_id
     * @param attachmentPath
     * @param id
     */

    private boolean updateAttachmentIdMetadataTable(int attach_id, String attachmentPath, int id) {
        SQLiteDatabase sqlite = null;
        boolean isUpdatedSuccesfully = false;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE +
                " SET " + TableNameAndColumnStatement.ATTACHMENT_ID + " = " + attach_id +
                " ," + TableNameAndColumnStatement.IS_NEW + " = " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + " = " + 1 +
                " WHERE " + TableNameAndColumnStatement.ID + " = " + id;
        Timber.d(Constants.TAG_SYNCADAPTER + "updateAttachmentIdMetadataTable  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER + "cursor detail object  %s", cursor.getCount());
            isUpdatedSuccesfully = true;

        } catch (Exception exception) {
            exception.getCause();
            isUpdatedSuccesfully = false;
        } finally {
            sqlite.close();
        }
        return isUpdatedSuccesfully;
    }


    public void dependentFailureSync() {
        List<DependentSyncsStatus> dependentSyncsStatusList = dependentSyncDb.getDependentTasksSyncStatus();
        if (dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0) {
            for (DependentSyncsStatus dependentSyncsStatus : dependentSyncsStatusList) {
                if (taskId == dependentSyncsStatus.taskId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.taskId,
                            dependentSyncsStatus);
                } else if (issueId == dependentSyncsStatus.issueId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.issueId,
                            dependentSyncsStatus);
                } else if (kitDistributionId == dependentSyncsStatus.kitDistributionId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.kitDistributionId,
                            dependentSyncsStatus);
                } else if (unitPostId == dependentSyncsStatus.unitPostId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.unitPostId,
                            dependentSyncsStatus);
                } else if (unitRaisingDetailId == dependentSyncsStatus.unitRaisingId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.unitRaisingId,
                            dependentSyncsStatus);
                } else if (unitRaisingDeferredId == dependentSyncsStatus.unitRaisingDeferredId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.unitRaisingDeferredId,
                            dependentSyncsStatus);
                } else if (unitRaisingCancellationId == dependentSyncsStatus.unitRaisingCancellationId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.unitRaisingCancellationId,
                            dependentSyncsStatus);
                } else if (barrackId == dependentSyncsStatus.barrackId) {
                    dependentSyncs(dependentSyncsStatus.taskTypeId, dependentSyncsStatus.barrackId,
                            dependentSyncsStatus);
                }
            }
        }
    }

    private void dependentSyncs(int taskTypeId, int taskLId, DependentSyncsStatus dependentSyncsStatus) {
        if (dependentSyncsStatus.isImagesSynced != Constants.DEFAULT_SYNC_COUNT) {
            if (taskId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setTaskId(taskId);

            } else if (issueId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setIssueId(issueId);
            } else if (kitDistributionId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setKitDistributionId(kitDistributionId);
            } else if (unitPostId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setUnitPostId(unitPostId);
            } else if (unitRaisingDetailId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setUnitRaisingId(unitRaisingDetailId);
            } else if (unitRaisingDeferredId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setUnitRaisingDeferredId(unitRaisingDeferredId);
            } else if (unitRaisingCancellationId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setUnitRaisingCancellationId(unitRaisingCancellationId);
            } else if (barrackId != 0) {
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setBarrackId(barrackId);
            }
        }
    }

    private void updateSyncCountAndStatus(int id, String columName) {
        if (taskId != 0) {
            int count = dependentSyncDb.getSyncCount(taskId, TableNameAndColumnStatement.TASK_ID,
                    TableNameAndColumnStatement.IS_METADATA_SYNCED);
            if (count != 0) {
                count = count - 1;
                Timber.i("NewSync %s", " MetaData count update in -----" + taskId + "--------" + count);
                updateMetaDataSyncCount(count);
                if (count == 0) {
                    ImageSyncing imageSyncing = new ImageSyncing(mContext);
                    imageSyncing.setTaskId(taskId);
                }
            } else {
                updateMetaDataSyncCount(count);
                ImageSyncing imageSyncing = new ImageSyncing(mContext);
                imageSyncing.setTaskId(taskId);

            }
        } else {
            int count = dependentSyncDb.getSyncCount(id, columName,
                    TableNameAndColumnStatement.IS_METADATA_SYNCED);
            if (count != 0) {
                count = count - 1;
            }
            updateMetaDataSyncCount(count);
        }

    }
}
