package com.sisindia.ai.android.issues.improvements;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Saruchi on 09-08-2016.
 */


public interface UpdateAdapterListner extends Serializable {
    void updateAdapter();
}
