package com.sisindia.ai.android.myperformance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.rota.RotaTaskBlackIconEnum;
import com.sisindia.ai.android.rota.RotaTaskTypeEnum;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ashu Rajput on 4/12/2018.
 */

public class ConveyanceTimeLineAdapter extends RecyclerView.Adapter<ConveyanceTimeLineAdapter.ConveyanceTimeLineViewHolder> {

    Context context;
    private LayoutInflater layoutInflater = null;
    private List<TimeLineModel> timeLineModel = null;
    private boolean isSelectedFromMenu = false;
//    private TotalTaskAndDistanceListener totalTaskAndDistanceListener = null;
    //    public boolean isAnyTaskNeedToBySynced = false;
//    private int taskCounter = 0;
//    private double totalDistanceTravelledCounter = 0.0;
//    private boolean isTotalDistanceAndTaskCalculated = false;

    public ConveyanceTimeLineAdapter(Context context, List<TimeLineModel> timeLineModel, boolean isSelectedFromMenu) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.timeLineModel = timeLineModel;
        this.isSelectedFromMenu = isSelectedFromMenu;
//        this.totalTaskAndDistanceListener = (TotalTaskAndDistanceListener) context;
    }

    @Override
    public ConveyanceTimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.timeline_row, parent, false);
        return new ConveyanceTimeLineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ConveyanceTimeLineViewHolder holder, int position) {

        try {

            double distanceInKM = timeLineModel.get(position).getDistance();
            holder.kilometerValue.setText(Html.fromHtml("<b>" + distanceInKM + "</b><small><small>KM</small></small>"));

            int receivedTaskTypeId = timeLineModel.get(position).getTaskTypeId();

            if (receivedTaskTypeId == 0 || receivedTaskTypeId == -3) {
                holder.timeLineTaskType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.others, 0, 0, 0);
                holder.taskCounterTV.setVisibility(View.GONE);
            } else if (receivedTaskTypeId == -1 || receivedTaskTypeId == -2) {
                /*taskCounter = taskCounter + 1;
                holder.taskCounterTV.setText("Task #" + taskCounter);*/
                holder.taskCounterTV.setText("Task #" + timeLineModel.get(position).getTaskCounter());
                holder.taskCounterTV.setVisibility(View.VISIBLE);
                holder.timeLineTaskType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.others, 0, 0, 0);
            } else {
                /*taskCounter = taskCounter + 1;
                holder.taskCounterTV.setText("Task #" + taskCounter);*/

                holder.taskCounterTV.setText("Task #" + timeLineModel.get(position).getTaskCounter());
                holder.taskCounterTV.setVisibility(View.VISIBLE);
                holder.timeLineTaskType.setCompoundDrawablesWithIntrinsicBounds(RotaTaskBlackIconEnum.valueOf(RotaTaskTypeEnum.valueOf(receivedTaskTypeId).name()).getValue(), 0, 0, 0);
            }

            // BELOW CONDITION IS USED TO SHOW COLOR FOR BOTH DUTY OFF AND DUTY ON (WITH RED AND GREEN RESPECTIVELY)
            if (timeLineModel.get(position).getTaskTypeId() == 0)
                holder.timeLineTaskType.setTextColor(context.getResources().getColor(R.color.prog_rota));
            else if (timeLineModel.get(position).getTaskTypeId() == -3) {
                holder.timeLineTaskType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                calculateTotalDistance(distanceInKM);
            } else {
                holder.timeLineTaskType.setTextColor(context.getResources().getColor(R.color.black));
//                calculateTotalDistance(distanceInKM);
            }

            //BELOW CONDITION IS USED TO CHECK AND DIFFERENCIATE AND SET VALUE TASK TYPE AND STATUS W.R.T DUTY OFF AND DUTY ON
            if (timeLineModel.get(position).getTaskTypeId() == 0 || timeLineModel.get(position).getTaskTypeId() == -3) {
                holder.timeLineStartEndTime.setText(timeLineModel.get(position).getDutyOnOffDateTime());
                holder.timeLineTaskType.setText(timeLineModel.get(position).getStatus());
                holder.timeLineUnitName.setVisibility(View.GONE);
                if (!isSelectedFromMenu || timeLineModel.get(position).getTaskTypeId() == 0) {
                    holder.kilometerValue.setVisibility(View.GONE);
                    holder.separatorLine.setVisibility(View.GONE);
                }

            } else {
                holder.timeLineUnitName.setVisibility(View.VISIBLE);
                holder.kilometerValue.setVisibility(View.VISIBLE);
                holder.separatorLine.setVisibility(View.VISIBLE);

                if (timeLineModel.get(position).getLocation() != null)
                    holder.timeLineUnitName.setText(timeLineModel.get(position).getLocation());
                else
                    holder.timeLineUnitName.setVisibility(View.GONE);

                if (!isSelectedFromMenu)
                    holder.timeLineTaskType.setText(timeLineModel.get(position).getStatus());
                else
                    holder.timeLineTaskType.setText(timeLineModel.get(position).getTaskName());

                holder.timeLineStartEndTime.setText(timeLineModel.get(position).getStartTime() + " - " + timeLineModel.get(position).getEndTime());
            }

            if (position == timeLineModel.size() - 1) {
                holder.verticalLine.setVisibility(View.GONE);
                /*totalTaskAndDistanceListener.totalTaskAndDistanceReceiver("Total Task / Distance Travelled : ( " + timeLineModel.get(position).getTaskCounter() + "/"
                        + formatDoubleToTwoDigits(totalDistanceTravelledCounter) + " Km )");*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return timeLineModel.size();
    }

    public class ConveyanceTimeLineViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.kilometerValue)
        TextView kilometerValue;
        @Bind(R.id.verticalLine)
        TextView verticalLine;
        @Bind(R.id.timeLineStartEndTime)
        TextView timeLineStartEndTime;
        @Bind(R.id.timeLineUnitName)
        TextView timeLineUnitName;
        @Bind(R.id.timeLineTaskType)
        TextView timeLineTaskType;
        @Bind(R.id.taskCounterTV)
        TextView taskCounterTV;
        @Bind(R.id.separatorLine)
        View separatorLine;

        public ConveyanceTimeLineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /*public interface TotalTaskAndDistanceListener {
        void totalTaskAndDistanceReceiver(String taskAndDistance);
    }*/

   /* private double calculateTotalDistance(double distance) {

        try {
            totalDistanceTravelledCounter = totalDistanceTravelledCounter + distance;
        } catch (Exception e) {
        }

        return totalDistanceTravelledCounter;
    }

    private String formatDoubleToTwoDigits(double totalDistance) {
        try {
            return String.format("%.1f", totalDistance);
        } catch (Exception e) {
        }
        return "" + totalDistance;
    }*/
}
