package com.sisindia.ai.android.home;

/**
 * Created by shankar on 6/4/16.
 */
public interface UpdateDutyStatusListener {
    void updateDutySwitch(boolean status);
}
