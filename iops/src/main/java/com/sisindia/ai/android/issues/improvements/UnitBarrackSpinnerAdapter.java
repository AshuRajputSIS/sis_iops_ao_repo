package com.sisindia.ai.android.issues.improvements;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;

import java.util.ArrayList;

/**
 * Created by Saruchi on 02-08-2016.
 */

public class UnitBarrackSpinnerAdapter extends ArrayAdapter<MyUnitHomeMO> {
    Context context;
    int resource;
    LayoutInflater inflator;
    ArrayList<Object> mData;
    public UnitBarrackSpinnerAdapter(Context context, int resource) {
        super(context, resource);
        inflator = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position,convertView,parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/

        View row = inflator.inflate(R.layout.spinner_row, parent, false);
        TextView mTextView = (TextView)row.findViewById(R.id.spinner_row_text);
        Object obj =mData.get(position);
        if(obj instanceof MyUnitHomeMO ){
            MyUnitHomeMO unitHomeMO =(MyUnitHomeMO)obj;
            mTextView.setText((unitHomeMO.getUnitName()));
        }
        else if (obj instanceof Barrack){
            Barrack barrackMo =(Barrack)obj;
            mTextView.setText((barrackMo.getName()));
        }
        else if (obj instanceof String){
            String txt =(String)obj;
            mTextView.setText(txt);
        }


        return row;
    }


    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = mData.size();
        return count > 0 ? count - 1 : count;
    }
    public  void setSpinnerData(ArrayList<Object> dataList){
        this.mData = dataList;
    }
}
