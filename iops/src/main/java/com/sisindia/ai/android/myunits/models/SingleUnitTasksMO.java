package com.sisindia.ai.android.myunits.models;

/**
 * Created by Durga Prasad on 05-05-2016.
 */
public class SingleUnitTasksMO {

    private String completedCount;
    private String pendingCount;
    private String lastCheckDate;
    private String unitTaskType;

    public String getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(String completedCount) {
        this.completedCount = completedCount;
    }

    public String getLastCheckDate() {
        return lastCheckDate;
    }

    public void setLastCheckDate(String lastCheckDate) {
        this.lastCheckDate = lastCheckDate;
    }

    public String getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(String pendingCount) {
        this.pendingCount = pendingCount;
    }

    public String getUnitTaskType() {
        return unitTaskType;
    }

    public void setUnitTaskType(String unitTaskType) {
        this.unitTaskType = unitTaskType;
    }
}
