package com.sisindia.ai.android.commons;

/**
 * Created by Durga Prasad on 23-03-2016.
 */
public interface DateTimeUpdateListener {
    void changeTime(String updateData);
}
