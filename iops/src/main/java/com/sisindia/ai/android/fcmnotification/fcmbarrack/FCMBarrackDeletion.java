package com.sisindia.ai.android.fcmnotification.fcmbarrack;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;

import java.util.ArrayList;

/**
 * Created by Saruchi on 06-12-2016.
 */

public class FCMBarrackDeletion extends SISAITrackingDB {
    private boolean isDeletedSuccessfully;
    private SQLiteDatabase sqlite;
    private ArrayList<String> barrackTableList ;
    public FCMBarrackDeletion(Context context) {
        super(context);
        barrackTableList();
    }
    public boolean deleteBarrackRecord(int barrackId) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {

                if(barrackId != 0) {
                    sqlite.beginTransaction();
                    for(String tableName :barrackTableList) {
                        IOPSApplication.getInstance().getDeletionStatementDBInstance()
                                .deleteRecordById(barrackId , TableNameAndColumnStatement.BARRACK_ID,tableName,sqlite);
                    }
                    sqlite.setTransactionSuccessful();


                    isDeletedSuccessfully = true;
                }else {
                    isDeletedSuccessfully = false;
                }
            } catch (Exception exception) {
                //sqlite.endTransaction();
                Crashlytics.logException(exception);
                isDeletedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
                if (null != sqlite && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
            return isDeletedSuccessfully;
        }
    }

    /**
     * order in which the records wll be deleted
     */
    private void barrackTableList(){
        barrackTableList = new ArrayList<>();
        barrackTableList.add(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE);
        barrackTableList.add(TableNameAndColumnStatement.BARRACK_TABLE);
    }

}


