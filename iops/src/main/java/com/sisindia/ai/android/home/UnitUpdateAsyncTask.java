package com.sisindia.ai.android.home;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.TaskTableCountMO;
import com.sisindia.ai.android.loadconfiguration.UnitBillingCheckList;
import com.sisindia.ai.android.myunits.models.EmployeeRank;
import com.sisindia.ai.android.myunits.models.Unit;
import com.sisindia.ai.android.myunits.models.UnitBarrack;
import com.sisindia.ai.android.myunits.models.UnitContacts;
import com.sisindia.ai.android.myunits.models.UnitCustomers;
import com.sisindia.ai.android.myunits.models.UnitEmployees;
import com.sisindia.ai.android.myunits.models.UnitEquipments;
import com.sisindia.ai.android.myunits.models.UnitMessVendors;
import com.sisindia.ai.android.myunits.models.UnitPost;
import com.sisindia.ai.android.myunits.models.UnitPostGeoPoint;
import com.sisindia.ai.android.myunits.models.UnitShiftRank;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Saruchi on 08-06-2016.
 */
public class UnitUpdateAsyncTask extends AsyncTask<String, String, String> {

    //    private SISClient sisClient;
    private List<Unit> units = new ArrayList<>();
    private Context mContext;
//    private UnitSychronizationDB unitSynchronization;
//    private RotaUnitSyncListener rotaUnitSyncListener;
//    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    public UnitUpdateAsyncTask(Context context, List<Unit> data, RotaUnitSyncListener rotaUnitSyncListener) {
        units = data;
        this.mContext = context;
//        this.rotaUnitSyncListener = rotaUnitSyncListener;
//        unitSynchronization = new UnitSychronizationDB(mContext);
//        sisClient = new SISClient(mContext);
//        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        // insertLoadConfigurationData(loadConfigurationBaseMO);
        if (null != units && units.size() != 0) {
            SQLiteDatabase sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
//            sqlite.beginTransaction();
            boolean isDeletionSuccess = unitTableDeletion(sqlite);

            if (isDeletionSuccess) {
//                boolean isInsertSuccess = insertUpdateUnitTable(sqlite);
                insertUpdateUnitTable(sqlite);
                closeDb(sqlite);

                /*if (isInsertSuccess) {
                    sqlite.setTransactionSuccessful();
                    sqlite.endTransaction();
                    closeDb(sqlite);
                } else {
                    sqlite.endTransaction();
                    closeDb(sqlite);
                    Timber.e("Error While Inserting Unit tables");
                }*/
            } else {
//                sqlite.endTransaction();
                closeDb(sqlite);
                Timber.e("Error While Deleting Unit tables");
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        hideprogressbar(true);
        super.onPostExecute(s);
    }

    private boolean insertUpdateUnitTable(SQLiteDatabase sqlite) {
        boolean isSuccessful = true;
        List<Integer> unitIds = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitIdsAfterDeletion(sqlite);
        try {
            if (null != units && units.size() != 0) {
                insertUnitTable(units, true, sqlite, unitIds);
                /**
                 * Unit Level model insertion
                 */
                for (Unit unit : units) {
                    /**
                     * Unit Authorized strength table insertion
                     */
                    if (null != unit.getUnitShiftRank() && unit.getUnitShiftRank().size() != 0) {
                        insertUnitAuthorizedStrength(unit.getUnitShiftRank(), true, sqlite);
                    }
                    /**
                     * Unit barrack table insertion
                     */
                    if (null != unit.getUnitBarrack() && unit.getUnitBarrack().size() != 0) {
                        insertUnitBarrack(unit.getUnitBarrack(), true, sqlite);
                    }
                    if (null != unit.getUnitEmployees() && unit.getUnitEmployees().size() != 0) {
                        insertUnitEmployees(unit.getUnitEmployees(), true, sqlite);
                    }

                    /*if (null != unit.getUnitMessVendors()) {
                        insertUnitMessVendor(unit.getUnitMessVendors(), true, sqlite);
                    }*/

                    if (null != unit.getUnitContacts() && unit.getUnitContacts().size() != 0) {
                        insertUnitContacts(unit.getUnitContacts(), true, sqlite);
                    }
                    /*if (null != unit.getUnitCustomers()) {
                        insertUnitCustomer(unit.getUnitCustomers(), true, sqlite);
                    }*/
                    if (null != unit.getUnitBillingCheckList() && unit.getUnitBillingCheckList().size() != 0) {
                        insertUnitBillingCheckList(unit.getUnitBillingCheckList(), true, sqlite);

                    }
                    UnitPostModelInsertion(unit.getUnitPosts(), sqlite);
                }
            } else {
                Timber.d("insertUpdateUnitTable  %s", "Null");
            }
        } catch (Exception exception) {
            isSuccessful = false;
            Crashlytics.logException(exception);
        }
        return isSuccessful;
    }

    private void hideprogressbar(boolean isCompleted) {
        Util.dismissProgressBar(isCompleted);
    }

    private void insertUnitTable(List<Unit> unitList, boolean isManualSync, SQLiteDatabase sqlite, List<Integer> unitIds) {
        synchronized (sqlite) {
            ContentValues values = new ContentValues();
            for (Unit unit : unitList) {
                if (!unitIds.contains(unit.getId())) {
                    values.put(TableNameAndColumnStatement.UNIT_ID, unit.getId());
                    values.put(TableNameAndColumnStatement.UNIT_NAME, unit.getName());
                    values.put(TableNameAndColumnStatement.UNIT_CODE, unit.getUnitCode());
                    values.put(TableNameAndColumnStatement.DESCRIPTIONS, unit.getDescription());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE_ID, unit.getUnitTypeId());
                    values.put(TableNameAndColumnStatement.AREA_ID, unit.getAreaId());
                    values.put(TableNameAndColumnStatement.CUSTOMER_ID, unit.getCustomerId());
                    values.put(TableNameAndColumnStatement.DAY_CHECK_FREQUENCY, unit.getDayCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.NIGHT_CHECK_FREQUENCY, unit.getNightCheckFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.CLIENT_COORDINATION_FREQUENCY, unit.getClientCoordinationFrequencyInMonth());
                    values.put(TableNameAndColumnStatement.BILLING_DATE, unit.getBillDate());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_DATE, unit.getBillCollectionDate());
                    values.put(TableNameAndColumnStatement.WAGE_DATE, unit.getWageDate());
                    values.put(TableNameAndColumnStatement.BILLING_MODE, unit.getBillingMode());
                    values.put(TableNameAndColumnStatement.MESS_VENDOR_ID, unit.getMessVendorId());
                    values.put(TableNameAndColumnStatement.IS_BARRACK_PROVIDED, unit.getIsBarrackProvided());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_ID, unit.getUnitCommanderId());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_ID, unit.getAreaInspectorId());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_ID, unit.getBillSubmissionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_ID, unit.getBillCollectionResponsibleId());
                    values.put(TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID, unit.getBillAuthoritativeUnitId());
                    values.put(TableNameAndColumnStatement.UNIT_TYPE, unit.getUnitType());
                    values.put(TableNameAndColumnStatement.UNIT_COMMANDER_NAME, unit.getUnitCommanderName());
                    values.put(TableNameAndColumnStatement.AREA_INSPECTOR_NAME, unit.getAreaInspectorName());
                    values.put(TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_NAME, unit.getBillSubmissionResponsibleName());
                    values.put(TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_NAME, unit.getBillCollectionResponsibleName());
                    values.put(TableNameAndColumnStatement.UNIT_FULL_ADDRESS, unit.getUnitFullAddress());
                    values.put(TableNameAndColumnStatement.UNIT_STATUS, unit.getUnitStatus());
                    values.put(TableNameAndColumnStatement.COLLECTION_STATUS, unit.getCollectionStatus());
                    values.put(TableNameAndColumnStatement.BILLING_TYPE_ID, unit.getBillingTypeId());
                    values.put(TableNameAndColumnStatement.BILL_GENERATION_DATE, unit.getBillGenerationDate());
                    values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unit.getArmedStrength());
                    values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unit.getUnarmedStrength());
                    values.put(TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL, unit.getMainGatePhotoUrl());
                    values.put(TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME, unit.getRaisingDate());
                    String unitBoundary = "";
                    if (null != unit.getUnitBoundary() && unit.getUnitBoundary().size() != 0) {
                        unitBoundary = IOPSApplication.getGsonInstance().toJson(unit.getUnitBoundary());
                    }
                    values.put(TableNameAndColumnStatement.UNIT_BOUNDARY, unitBoundary);
                    sqlite.insert(TableNameAndColumnStatement.UNIT_TABLE, null, values);
                }
            }
        }
    }

    public void insertUnitAuthorizedStrength(List<UnitShiftRank> unitShiftRankList, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            ContentValues values = new ContentValues();
            for (UnitShiftRank unitShiftRank : unitShiftRankList) {
                values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, unitShiftRank.getId());
                values.put(TableNameAndColumnStatement.UNIT_ID, unitShiftRank.getUnitId());
                values.put(TableNameAndColumnStatement.SHIFT_ID, unitShiftRank.getShiftId());
                values.put(TableNameAndColumnStatement.RANK_ID, unitShiftRank.getRankId());
                values.put(TableNameAndColumnStatement.RANK_COUNT, unitShiftRank.getRankCount());
                values.put(TableNameAndColumnStatement.RANK_ABBREVATION, unitShiftRank.getRankAbbrevation());
                values.put(TableNameAndColumnStatement.BARRACK_ID, unitShiftRank.getBarrackId());
                values.put(TableNameAndColumnStatement.UNIT_NAME, unitShiftRank.getUnitName());
                values.put(TableNameAndColumnStatement.SHIFT_NAME, unitShiftRank.getShiftName());
                values.put(TableNameAndColumnStatement.RANK_NAME, unitShiftRank.getRankName());
                values.put(TableNameAndColumnStatement.BARRACK_NAME, unitShiftRank.getBarrackName());
                values.put(TableNameAndColumnStatement.STRENGTH, unitShiftRank.getStrength());
                values.put(TableNameAndColumnStatement.ACTUAL, 0);
                values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
                values.put(TableNameAndColumnStatement.IS_ARMED, unitShiftRank.getIsArmed());
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                sqlite.insert(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
            }
        }
    }

    public void insertUnitBarrack(List<UnitBarrack> unitBarrackList, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            ContentValues values = new ContentValues();
            for (UnitBarrack unitBarrack : unitBarrackList) {
                values.put(TableNameAndColumnStatement.UNIT_ID, unitBarrack.getUnitId());
                values.put(TableNameAndColumnStatement.BARRACK_ID, unitBarrack.getBarrackId());
                values.put(TableNameAndColumnStatement.IS_ACTIVE, unitBarrack.getActive());
                values.put(TableNameAndColumnStatement.UNIT_BARRACK_ID, unitBarrack.getId());
                sqlite.insert(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, null, values);
            }
        }
    }

    public void insertUnitEmployees(List<UnitEmployees> unitEmployeesList, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            for (UnitEmployees unitEmployee : unitEmployeesList) {

                values.put(TableNameAndColumnStatement.UNIT_ID, unitEmployee.getUnitId());
                values.put(TableNameAndColumnStatement.EMPLOYEE_ID, unitEmployee.getEmployeeId());
                values.put(TableNameAndColumnStatement.EMPLOYEE_NO, unitEmployee.getEmployeeNo());
                values.put(TableNameAndColumnStatement.EMPLOYEE_FULL_NAME, unitEmployee.getEmployeeFullName());
                values.put(TableNameAndColumnStatement.EMPLOYEE_CONTACT_NO, unitEmployee.getEmployeContactNo());
                values.put(TableNameAndColumnStatement.IS_ACTIVE, unitEmployee.isActive());
                values.put(TableNameAndColumnStatement.DEPLOYMENT_DATE, unitEmployee.getDeploymentDate());
                values.put(TableNameAndColumnStatement.WEEKLY_OFF_DAY, unitEmployee.getWeeklyOffDay());
                if (null != unitEmployee.getEmployeeRank() && unitEmployee.getEmployeeRank().size() != 0) {
                    for (EmployeeRank employeeRank : unitEmployee.getEmployeeRank()) {
                        values.put(TableNameAndColumnStatement.EMPLOYEE_RANK_ID, employeeRank.getRankId());
                        values.put(TableNameAndColumnStatement.BRANCH_ID, employeeRank.getBranchId());
                    }
                }
                sqlite.insert(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, null, values);
            }
        }
    }

    public void insertUnitMessVendor(UnitMessVendors unitMessVendor, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            values.put(TableNameAndColumnStatement.VENDOR_ID, unitMessVendor.getId());
            values.put(TableNameAndColumnStatement.VENDOR_NAME, unitMessVendor.getVendorName());
            values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitMessVendor.getDescription());
            values.put(TableNameAndColumnStatement.VENDOR_CONTACT_NAME, unitMessVendor.getVendorContactName());
            values.put(TableNameAndColumnStatement.VENDOR_FULL_ADDRESS, unitMessVendor.getVendorAddress());

            sqlite.insert(TableNameAndColumnStatement.MESS_VENDOR_TABLE, null, values);
        }
    }

    public void insertUnitContacts(List<UnitContacts> unitContactList, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            for (UnitContacts unitContact : unitContactList) {
                values.put(TableNameAndColumnStatement.IS_NEW, 0);
                values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
                values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                        unitContact.getId());
                values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
                values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
                values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
                values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
                values.put(TableNameAndColumnStatement.UNIT_ID,
                        unitContact.getUnitId());
                values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
                sqlite.insert(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
            }
        }
    }

    public void insertUnitCustomer(UnitCustomers unitCustomer, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            values.put(TableNameAndColumnStatement.CUSTOMER_ID, unitCustomer.getId());
            values.put(TableNameAndColumnStatement.CUSTOMER_NAME, unitCustomer.getCustomerName());
            values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitCustomer.getCustomerDescription());
            values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_NAME, unitCustomer.getCustomerPrimaryContactName());
            values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_EMAIL, unitCustomer.getPrimaryContactEmail());
            values.put(TableNameAndColumnStatement.PRIMARY_CONTACT_PHONE, unitCustomer.getPrimaryContactPhone());
            values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_NAME, unitCustomer.getCustomerSecondaryContactName());
            values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_EMAIL, unitCustomer.getSecondaryContactEmail());
            values.put(TableNameAndColumnStatement.SECONDARY_CONTACT_PHONE, unitCustomer.getSecondaryContactPhone());
            values.put(TableNameAndColumnStatement.CAN_SEND_EMAIL, unitCustomer.isEmailToBeSent());
            values.put(TableNameAndColumnStatement.CAN_SEND_SMS, unitCustomer.isSmsToBeSent());
            values.put(TableNameAndColumnStatement.CUSTOMER_FULL_ADDRESS, unitCustomer.getCustomerHoAddress());
            sqlite.insert(TableNameAndColumnStatement.CUSTOMER_TABLE, null, values);
        }
    }

    public void insertUnitBillingCheckList(List<UnitBillingCheckList> unitBillingCheckList, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            ContentValues values = new ContentValues();
            for (UnitBillingCheckList unitBilling : unitBillingCheckList) {
                values.put(TableNameAndColumnStatement.ID, unitBilling.getId());
                values.put(TableNameAndColumnStatement.UNIT_ID, unitBilling.getUnitId());
                values.put(TableNameAndColumnStatement.BILL_CHECK_LIST_ID, unitBilling.getBillingCheckListId());
                sqlite.insert(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE, null, values);
            }
        }
    }

    public void insertUnitPost(List<UnitPost> unitPosts, boolean isManualSync, SQLiteDatabase sqlite) {
        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            String postName = "";
            for (UnitPost unitPost : unitPosts) {
                values.put(TableNameAndColumnStatement.UNIT_ID, unitPost.getUnitId());
                values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitPost.getId());
                if (unitPost.getUnitPostName().contains("'")) {
                    postName = unitPost.getUnitPostName();
                    postName = postName.replace("'", "\'");
                } else {
                    postName = unitPost.getUnitPostName();
                }
                values.put(TableNameAndColumnStatement.UNIT_POST_NAME, postName);
                values.put(TableNameAndColumnStatement.DESCRIPTIONS, unitPost.getDescription());
                values.put(TableNameAndColumnStatement.ARMED_STRENGTH, unitPost.getArmedStrength());
                values.put(TableNameAndColumnStatement.UNARMED_STRENGTH, unitPost.getUnarmedStrength());
                values.put(TableNameAndColumnStatement.MAIN_GATE_DISTANCE, unitPost.getMainGateDistance());
                values.put(TableNameAndColumnStatement.BARRACK_DISTANCE, unitPost.getBarrackDistance());
                values.put(TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE, unitPost.getUnitOfficeDistance());
                values.put(TableNameAndColumnStatement.IS_MAIN_GATE, unitPost.getIsMainGate());
                values.put(TableNameAndColumnStatement.DEVICE_NO, unitPost.deviceNo);
                values.put(TableNameAndColumnStatement.IS_NEW, unitPost.isNew());
                values.put(TableNameAndColumnStatement.IS_SYNCED, unitPost.isSynced());
                if (unitPost.getUnitPostGeoPoint() != null && unitPost.getUnitPostGeoPoint().size() != 0) {
                    for (UnitPostGeoPoint unitPostGeoPint : unitPost.getUnitPostGeoPoint()) {
                        values.put(TableNameAndColumnStatement.GEO_LATITUDE, unitPostGeoPint.getLatitude());
                        values.put(TableNameAndColumnStatement.GEO_LONGITUDE, unitPostGeoPint.getLongitude());
                    }
                }
                values.put(TableNameAndColumnStatement.POST_STATUS, unitPost.getIsActive());
                sqlite.insert(TableNameAndColumnStatement.UNIT_POST_TABLE, null, values);
            }
        }
    }

    public void insertUnitEquipments(List<UnitEquipments> unitEquipmentList, boolean isManualSync, SQLiteDatabase sqlite) {

        synchronized (sqlite) {

            ContentValues values = new ContentValues();
            for (UnitEquipments unitEquipment : unitEquipmentList) {
                values.put(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID, unitEquipment.getId());
                values.put(TableNameAndColumnStatement.UNIQUE_NO, unitEquipment.getEquipmentUniqueNo());
                values.put(TableNameAndColumnStatement.MANUFACTURER, unitEquipment.getEquipmentManufacturer());
                values.put(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED, unitEquipment.isCustomerProvided());
                values.put(TableNameAndColumnStatement.EQUIPMENT_STATUS, unitEquipment.getStatus());
                values.put(TableNameAndColumnStatement.EQUIPMENT_TYPE, unitEquipment.getEquipmentId());
                values.put(TableNameAndColumnStatement.UNIT_ID, unitEquipment.getUnitId());
                values.put(TableNameAndColumnStatement.UNIT_POST_ID, unitEquipment.getPostId());
                values.put(TableNameAndColumnStatement.COUNT, unitEquipment.getQuantity());
                values.put(TableNameAndColumnStatement.IS_NEW, Constants.IS_OLD); //  recored synced wih server
                values.put(TableNameAndColumnStatement.IS_SYNCED, Constants.IS_SYNCED); // recored synced wih server
                sqlite.insert(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, values);
            }
        }
    }

    private void UnitPostModelInsertion(List<UnitPost> unitPostList, SQLiteDatabase sqlite) {
        if (null != unitPostList && unitPostList.size() != 0) {
            insertUnitPost(unitPostList, true, sqlite);
            for (UnitPost unitPost : unitPostList) {
                if (null != unitPost.getUnitEquipments() && unitPost.getUnitEquipments().size() != 0) {
                    insertUnitEquipments(unitPost.getUnitEquipments(), true, sqlite);
                }
            }
        }
    }

    /**
     * order in which tables will be deleted
     */
    private boolean unitTableDeletion(SQLiteDatabase sqlite) {

        boolean isDeletionSuccess = true;
        try {
           /* sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_POST_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE);
            sqlite.execSQL("delete from " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE);
            sqlite.execSQL("delete from unit where unit_status in (1,2,0)");*/

            sqlite.delete(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_POST_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE, null, null);
            sqlite.delete(TableNameAndColumnStatement.UNIT_BARRACK_TABLE, null, null);
            String whereClause = TableNameAndColumnStatement.UNIT_STATUS + " in (1,2,0)";
            sqlite.delete(TableNameAndColumnStatement.UNIT_TABLE, whereClause, null);

        } catch (Exception exception) {
            isDeletionSuccess = false;
            exception.printStackTrace();
            Crashlytics.logException(exception);
        }

        /*ArrayList<String> unitTableList = new ArrayList<>();
        unitTableList.add(TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_POST_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_CONTACT_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_BARRACK_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE);
        unitTableList.add(TableNameAndColumnStatement.UNIT_TABLE);

        DeletionStatementDB deletionStatementDB = new DeletionStatementDB(mContext);
        boolean isDeletionSucess = true;

        for (String tableName : unitTableList) {

            if (TableNameAndColumnStatement.UNIT_TABLE.equalsIgnoreCase(tableName)) {
                if (!deletionStatementDB.isUnitTableDeleted(tableName, sqlite)) {
                    isDeletionSucess = false;
                    break;
                } else {
                    Log.e("Deletion Status", "Deleted Table -- is " + tableName);
                }
            } else {
                if (!deletionStatementDB.IsTableDeleted(tableName, sqlite)) {
                    isDeletionSucess = false;
                    break;
                } else {
                    Log.e("Deletion Status", "Deleted Table == is " + tableName);
                }
            }
        }*/
        return isDeletionSuccess;
    }

    private static void closeDb(SQLiteDatabase sqlite) {
        if (null != sqlite && sqlite.isOpen()) {
            sqlite.close();
        }
    }

    private synchronized static boolean tablesToSync() {
        TaskTableCountMO taskTableCountMO = IOPSApplication.getInstance().getTableSyncCount().getTaskTableCount();
        if (taskTableCountMO.getSyncCount() != 0) {
            return false;
        } else {
            return true;
        }
    }
}
