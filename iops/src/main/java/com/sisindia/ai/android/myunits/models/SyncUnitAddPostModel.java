package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Saruchi on 06-05-2016.
 */
public class SyncUnitAddPostModel {


    @SerializedName("UnitPostId")
    @Expose
    private Integer UnitPostId;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UnitId")
    @Expose
    private Integer UnitId;
    @SerializedName("UnitPostName")
    @Expose
    private String UnitPostName;
    @SerializedName("IsActive")
    @Expose
    private Integer IsActive;
    @SerializedName("GeoPoint")
    @Expose
    private String GeoPoint;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("MainGateDistance")
    @Expose
    private Double MainGateDistance;
    @SerializedName("BarrackDistance")
    @Expose
    private Double BarrackDistance;
    @SerializedName("UnitOfficeDistance")
    @Expose
    private Double UnitOfficeDistance;
    @SerializedName("ArmedStrength")
    @Expose
    private Integer ArmedStrength;
    @SerializedName("UnarmedStrength")
    @Expose
    private Integer UnarmedStrength;
    @SerializedName("IsMainGate")
    @Expose
    private Integer IsMainGate;

    /**
     * @return The UnitId
     */
    public Integer getUnitId() {
        return UnitId;
    }

    /**
     * @param UnitId The UnitId
     */
    public void setUnitId(Integer UnitId) {
        this.UnitId = UnitId;
    }

    /**
     * @return The UnitPostName
     */
    public String getUnitPostName() {
        return UnitPostName;
    }

    /**
     * @param UnitPostName The UnitPostName
     */
    public void setUnitPostName(String UnitPostName) {
        this.UnitPostName = UnitPostName;
    }

    /**
     * @return The IsActive
     */
    public Integer getIsActive() {
        return IsActive;
    }

    /**
     * @param IsActive The IsActive
     */
    public void setIsActive(Integer IsActive) {
        this.IsActive = IsActive;
    }

    /**
     * @return The GeoPoint
     */
    public String getGeoPoint() {
        return GeoPoint;
    }

    /**
     * @param GeoPoint The GeoPoint
     */
    public void setGeoPoint(String GeoPoint) {
        this.GeoPoint = GeoPoint;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description The Description
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return The MainGateDistance
     */
    public Double getMainGateDistance() {
        return MainGateDistance;
    }

    /**
     * @param MainGateDistance The MainGateDistance
     */
    public void setMainGateDistance(Double MainGateDistance) {
        this.MainGateDistance = MainGateDistance;
    }

    /**
     * @return The BarrackDistance
     */
    public Double getBarrackDistance() {
        return BarrackDistance;
    }

    /**
     * @param BarrackDistance The BarrackDistance
     */
    public void setBarrackDistance(Double BarrackDistance) {
        this.BarrackDistance = BarrackDistance;
    }

    /**
     * @return The UnitOfficeDistance
     */
    public Double getUnitOfficeDistance() {
        return UnitOfficeDistance;
    }

    /**
     * @param UnitOfficeDistance The UnitOfficeDistance
     */
    public void setUnitOfficeDistance(Double UnitOfficeDistance) {
        this.UnitOfficeDistance = UnitOfficeDistance;
    }

    /**
     * @return The ArmedStrength
     */
    public Integer getArmedStrength() {
        return ArmedStrength;
    }

    /**
     * @param ArmedStrength The ArmedStrength
     */
    public void setArmedStrength(Integer ArmedStrength) {
        this.ArmedStrength = ArmedStrength;
    }

    /**
     * @return The UnarmedStrength
     */
    public Integer getUnarmedStrength() {
        return UnarmedStrength;
    }

    /**
     * @param UnarmedStrength The UnarmedStrength
     */
    public void setUnarmedStrength(Integer UnarmedStrength) {
        this.UnarmedStrength = UnarmedStrength;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The IsMainGate
     */
    public Integer getIsMainGate() {
        return IsMainGate;
    }

    /**
     * @param IsMainGate The IsMainGate
     */
    public void setIsMainGate(Integer IsMainGate) {
        this.IsMainGate = IsMainGate;
    }

    public Integer getUnitPostId() {
        return UnitPostId;
    }

    public void setUnitPostId(Integer unitPostId) {
        UnitPostId = unitPostId;
    }

    @Override
    public String toString() {
        return "SyncUnitAddPostModel{" +
                "UnitPostId=" + UnitPostId +
                ", id=" + id +
                ", UnitId=" + UnitId +
                ", UnitPostName='" + UnitPostName + '\'' +
                ", IsActive=" + IsActive +
                ", GeoPoint='" + GeoPoint + '\'' +
                ", Description='" + Description + '\'' +
                ", MainGateDistance=" + MainGateDistance +
                ", BarrackDistance=" + BarrackDistance +
                ", UnitOfficeDistance=" + UnitOfficeDistance +
                ", ArmedStrength=" + ArmedStrength +
                ", UnarmedStrength=" + UnarmedStrength +
                ", IsMainGate=" + IsMainGate +
                '}';
    }
}
