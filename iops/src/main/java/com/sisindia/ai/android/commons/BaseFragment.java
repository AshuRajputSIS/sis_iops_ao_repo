package com.sisindia.ai.android.commons;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by shivam on 18/7/16.
 */

public abstract class BaseFragment extends Fragment {

    public Activity mActivity;
    public Snackbar mSnackbar;
    public AppPreferences appPreferences;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    public static ArrayList<AttachmentMetaDataModel> attachmentMetaArrayList = new ArrayList<>();
    private static final String TAG = "BaseFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        appPreferences = new AppPreferences(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View layout = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, layout);
        mActivity = getActivity();
        setupLanguage();
        return layout;
    }

    protected abstract int getLayout();

    public void snackBarWithMessage(String msg) {
        mSnackbar = Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG);
        snackBarColor();
        mSnackbar.show();
    }

    public void snackBarWithDuration(String msg, int duration) {
        mSnackbar = Snackbar.make(getView(), msg, duration);
        snackBarColor();
        mSnackbar.show();
    }

    private void snackBarColor() {
        View view = mSnackbar.getView();
        mSnackbar.setActionTextColor(Color.WHITE);
        view.setBackgroundResource(R.color.colorPrimary);
    }

    public void snackBarWithMesg(View view, String mesg) {
        try {
            if (view != null && view.isAttachedToWindow()) {
                mSnackbar = Snackbar.make(view, mesg, Snackbar.LENGTH_SHORT);
                snackBarColor();
                mSnackbar.show();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void setLocale(String localeString, int languageId) {
        Locale locale = new Locale(localeString);
        if (languageId == Constants.HINDI_LANGUAGE_ID) {
            locale.setDefault(locale);
            if (mActivity != null) {
                Configuration configuration = mActivity.getApplicationContext().getResources().getConfiguration();
                configuration.locale = locale;
                mActivity.getApplicationContext().getResources().updateConfiguration(configuration,
                        mActivity.getApplicationContext().getResources().getDisplayMetrics());
            }
        }
    }

    private void setupLanguage() {
        int languageId = appPreferences.getLanguageId();
        String localeString = Locale.getDefault().getLanguage();

        if (languageId != 0) {
            switch (languageId) {
                case Constants.ENGLISH_LANGUAGE_ID:
                    break;
                case Constants.HINDI_LANGUAGE_ID:
                    if (!Constants.LOCALE_HINDI_INDENTIFIER.equalsIgnoreCase(localeString)) {
                        setLocale(Constants.LOCALE_HINDI_INDENTIFIER, Constants.HINDI_LANGUAGE_ID);
                    }
                    break;
            }
        } else {
            Timber.d(TAG + "%s", "onResume: Language is not selected yet");
        }
    }
}