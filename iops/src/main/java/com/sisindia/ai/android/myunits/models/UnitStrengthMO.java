package com.sisindia.ai.android.myunits.models;


import com.google.gson.annotations.SerializedName;

public class UnitStrengthMO {

@SerializedName("Unit")

private Integer unit;
@SerializedName("Actual")

private Integer actual;
@SerializedName("Authorised")

private Integer authorised;
@SerializedName("Difference")

private Integer difference;
@SerializedName("RanKId")

private Integer ranKId;
@SerializedName("RankName")

private String rankName;

/**
*
* @return
* The unit
*/
public Integer getUnit() {
return unit;
}

/**
*
* @param unit
* The Unit
*/
public void setUnit(Integer unit) {
this.unit = unit;
}

/**
*
* @return
* The actual
*/
public Integer getActual() {
return actual;
}

/**
*
* @param actual
* The Actual
*/
public void setActual(Integer actual) {
this.actual = actual;
}

/**
*
* @return
* The authorised
*/
public Integer getAuthorised() {
return authorised;
}

/**
*
* @param authorised
* The Authorised
*/
public void setAuthorised(Integer authorised) {
this.authorised = authorised;
}

/**
*
* @return
* The difference
*/
public Integer getDifference() {
return difference;
}

/**
*
* @param difference
* The Difference
*/
public void setDifference(Integer difference) {
this.difference = difference;
}

/**
*
* @return
* The ranKId
*/
public Integer getRanKId() {
return ranKId;
}

/**
*
* @param ranKId
* The RanKId
*/
public void setRanKId(Integer ranKId) {
this.ranKId = ranKId;
}

/**
*
* @return
* The rankName
*/
public String getRankName() {
return rankName;
}

/**
*
* @param rankName
* The RankName
*/
public void setRankName(String rankName) {
this.rankName = rankName;
}

}

