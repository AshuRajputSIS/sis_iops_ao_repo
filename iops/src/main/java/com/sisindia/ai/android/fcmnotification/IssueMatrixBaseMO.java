package com.sisindia.ai.android.fcmnotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.IssueMatrixModel;

import java.util.List;

/**
 * Created by Ashu Rajput on 2/26/2018.
 */

public class IssueMatrixBaseMO {

    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;

    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;

    @SerializedName("Data")
    @Expose
    public List<IssueMatrixModel> data;
}
