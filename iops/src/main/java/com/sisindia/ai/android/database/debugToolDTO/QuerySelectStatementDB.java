package com.sisindia.ai.android.database.debugToolDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;

import org.json.JSONArray;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by Ashu Rajput on 8/31/2017.
 */

public class QuerySelectStatementDB extends SISAITrackingDB {

    String queryExecutorResult = "";

    public QuerySelectStatementDB(Context context) {
        super(context);
    }

    public String getQueryExecutorResult(String query) {

        SQLiteDatabase sqlite = null;
        Timber.d("SequencePostId selectedQuery  %s", query);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(query, null);
            if (cursor != null && !cursor.isClosed()) {
                JSONArray jsonArray = convertCursorToJson(cursor);
                if (jsonArray != null && jsonArray.length() > 0) {
                    queryExecutorResult = jsonArray.toString();
                } else
                    queryExecutorResult = "NULL_ARRAY";
            } else {
                queryExecutorResult = "NULL_CURSOR";
            }
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getMessage());
            Crashlytics.logException(exception);
            queryExecutorResult = "EXCEPTION/"+ exception.getMessage();
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return queryExecutorResult;
    }

    public JSONArray convertCursorToJson(Cursor cursor) {
        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                    } catch (Exception e) {
                        Log.e("Query", "Query Me execpetion aa gai he boss!!!");
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
        return resultSet;
    }

}
