package com.sisindia.ai.android.network.request;

import java.io.Serializable;

public class OtpInputRequestMO implements Serializable {
    private static final long serialVersionUID = 4287720719489872777L;
    private String IMEI1;
    private String IMEI2;
    private String phoneNumber;
    private boolean optFlag;
    private int LanguageId;
    private String DeviceToken;

    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    private String AppVersion;

    public boolean isOptFlag() {
        return optFlag;
    }

    public void setOptFlag(boolean optFlag) {
        this.optFlag = optFlag;
    }


    public String getIMEI1() {
        return this.IMEI1;
    }

    public void setIMEI1(String IMEI1) {
        this.IMEI1 = IMEI1;
    }

    public String getIMEI2() {
        return this.IMEI2;
    }

    public void setIMEI2(String IMEI2) {
        this.IMEI2 = IMEI2;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
