package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by compass on 3/5/2017.
 */

public class BaseIssuesSync {
    @SerializedName("StatusCode")
    public  int statusCode;
    @SerializedName("StatusMessage")
    public String statusMessage;
    @SerializedName("Data")
    public IssuesAndImprovementPlan issuesImprovementData;

}


