package com.sisindia.ai.android.rota.rotaCompliance;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RotaCompliance implements Parcelable {

    @SerializedName("AdhocTaskCount")
    public Integer AdhocTaskCount;

    @SerializedName("AdhocTaskApprovedCount")
    public Integer AdhocTaskApprovedCount;

    @SerializedName("RotaComplianceAndroidModelItem")
    public List<RotaComplianceTaskDetails> rotaComplianceModelItemList;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.AdhocTaskCount);
        dest.writeValue(this.AdhocTaskApprovedCount);
        dest.writeTypedList(this.rotaComplianceModelItemList);
    }

    public RotaCompliance() {
    }

    protected RotaCompliance(Parcel in) {
        this.AdhocTaskCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.AdhocTaskApprovedCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.rotaComplianceModelItemList = in.createTypedArrayList(RotaComplianceTaskDetails.CREATOR);
    }

    public static final Creator<RotaCompliance> CREATOR = new Creator<RotaCompliance>() {
        @Override
        public RotaCompliance createFromParcel(Parcel source) {
            return new RotaCompliance(source);
        }

        @Override
        public RotaCompliance[] newArray(int size) {
            return new RotaCompliance[size];
        }
    };
}

