package com.sisindia.ai.android.issues.models;

/**
 * Created by Durga Prasad on 04-08-2016.
 */
public class IssueClosureMo {

    private  int statusId;
    private  String closureRemarks;
    private  String closureEndDate;



    private  String statusName;




    public String getClosureEndDate() {
        return closureEndDate;
    }

    public void setClosureEndDate(String closureEndDate) {
        this.closureEndDate = closureEndDate;
    }

    public String getClosureRemarks() {
        return closureRemarks;
    }

    public void setClosureRemarks(String closureRemarks) {
        this.closureRemarks = closureRemarks;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
