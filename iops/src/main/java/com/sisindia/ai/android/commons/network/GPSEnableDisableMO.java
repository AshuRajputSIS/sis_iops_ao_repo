package com.sisindia.ai.android.commons.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by IOPS on 2/20/2018.
 */

public class GPSEnableDisableMO {

    @SerializedName("AreaInspectorId")
    public Integer areaInspectorId;
    @SerializedName("IsGpsActive")
    public Boolean isGpsActive;
    @SerializedName("CapturedDateTime")
    public String capturedDateTime;

    public void setAreaInspectorId(Integer areaInspectorId) {
        this.areaInspectorId = areaInspectorId;
    }

    public void setGpsActive(Boolean gpsActive) {
        isGpsActive = gpsActive;
    }

    public void setCapturedDateTime(String capturedDateTime) {
        this.capturedDateTime = capturedDateTime;
    }
}
