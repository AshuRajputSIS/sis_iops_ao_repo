package com.sisindia.ai.android.rota.billcollection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shruti on 8/7/16.
 */
public class BillCollectionMO {


    @SerializedName("BillAmount")
    @Expose
    private String billAmount;
    @SerializedName("ChequeNo")
    @Expose
    private String chequeNo;
    @SerializedName("CollectionAmount")
    @Expose
    private String collectionAmount;
    @SerializedName("CollectionMonth")
    @Expose
    private String collectionMonth;
    @SerializedName("DueDate")
    @Expose
    private String dueDate;
    @SerializedName("PaymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("RtgsDetails")
    @Expose
    private String rgtsDetails;
    @SerializedName("CollectionDate")
    @Expose
    private String collectionDate;
    @SerializedName("CollectionYear")
    private Integer collectionYear;

    /**
     *
     * @return
     * The billAmount
     */
    public String getBillAmount() {
        return billAmount;
    }

    /**
     *
     * @param billAmount
     * The BillAmount
     */
    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    /**
     *
     * @return
     * The chequeNo
     */
    public String getChequeNo() {
        return chequeNo;
    }

    /**
     *
     * @param chequeNo
     * The ChequeNo
     */
    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    /**
     *
     * @return
     * The collectionAmount
     */
    public String getCollectionAmount() {
        return collectionAmount;
    }

    /**
     *
     * @param collectionAmount
     * The CollectionAmount
     */
    public void setCollectionAmount(String collectionAmount) {
        this.collectionAmount = collectionAmount;
    }

    /**
     *
     * @return
     * The collectionMonth
     */
    public String getCollectionMonth() {
        return collectionMonth;
    }

    /**
     *
     * @param collectionMonth
     * The CollectionMonth
     */
    public void setCollectionMonth(String collectionMonth) {
        this.collectionMonth = collectionMonth;
    }

    /**
     *
     * @return
     * The dueDate
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     *
     * @param dueDate
     * The DueDate
     */
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    /**
     *
     * @return
     * The paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     *
     * @param paymentMode
     * The PaymentMode
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    /**
     *
     * @return
     * The rgtsDetails
     */
    public String getRgtsDetails() {
        return rgtsDetails;
    }

    /**
     *
     * @param rgtsDetails
     * The RgtsDetails
     */
    public void setRgtsDetails(String rgtsDetails) {
        this.rgtsDetails = rgtsDetails;
    }

    /**
     *
     * @return
     * The collectionDate
     */
    public String getCollectionDate() {
        return collectionDate;
    }

    /**
     *
     * @param collectionDate
     * The CollectionDate
     */
    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    @Override
    public String toString() {
        return "BillCollectionMO{" +
                "billAmount='" + billAmount + '\'' +
                ", chequeNo='" + chequeNo + '\'' +
                ", collectionAmount='" + collectionAmount + '\'' +
                ", collectionMonth='" + collectionMonth + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", rgtsDetails='" + rgtsDetails + '\'' +
                ", collectionDate='" + collectionDate + '\'' +
                '}';
    }

    public void setCollectionYear(Integer collectionYear) {
        this.collectionYear = collectionYear;
    }

    public Integer getCollectionYear() {
        return collectionYear;
    }
}
