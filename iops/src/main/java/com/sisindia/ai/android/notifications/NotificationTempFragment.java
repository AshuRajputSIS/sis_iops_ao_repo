package com.sisindia.ai.android.notifications;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;

/**
 * Created by Saruchi on 27-04-2016.
 */
public class NotificationTempFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private Context mContext;
    private String mParam1;
    private String mParam2;

    public static NotificationTempFragment newInstance(String param1, String param2) {
        NotificationTempFragment fragment = new NotificationTempFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_temp, container, false);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//for enable the menu in fragment
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }
}
