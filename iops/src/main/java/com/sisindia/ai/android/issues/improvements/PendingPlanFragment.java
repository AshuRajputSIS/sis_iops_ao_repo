package com.sisindia.ai.android.issues.improvements;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PendingPlanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PendingPlanFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String CURRENT_TAB = "current_tab";
    public static PendingPlanFragment fragment;
    static boolean isFromUnitRisk;
    ArrayList<Object> mDataValues = new ArrayList<>();
    CustomPlanTaskAdapter customIssuesAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int mCurrentPos;

    public PendingPlanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PendingPlanFragment newInstance(String param1, String param2, boolean isfromUnitRisk, int currentTab) {
        if (fragment == null) {
            fragment = new PendingPlanFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            args.putInt(CURRENT_TAB, currentTab);
            isFromUnitRisk = isfromUnitRisk;
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mCurrentPos = getArguments().getInt(CURRENT_TAB);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.pending_plans_fragment, container, false);
        initComplaintsViews(view);
        return view;
    }

    private void initComplaintsViews(View view) {
        RecyclerView mgrievance_recyclerview = (RecyclerView) view.findViewById(R.id.pending_improvement_plan_recyclerview);
        mgrievance_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));

        customIssuesAdapter = new CustomPlanTaskAdapter(getContext(), mDataValues, true, isFromUnitRisk, 0);
        mgrievance_recyclerview.setAdapter(customIssuesAdapter);
    }

    public void updateRecyclerview(ArrayList<Object> models) {
        mDataValues.addAll(models);
        customIssuesAdapter.notifyDataSetChanged();
    }
}
