package com.sisindia.ai.android.appuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shushrut on 16-09-2016.
 */
public class AppUserLocationMO {

    @SerializedName("AddressLine1")
    @Expose
    private String addressLine1;
    @SerializedName("AlternateContactNo")
    @Expose
    private String alternateContactNo;
    @SerializedName("Lat")
    @Expose
    private String lat;
    @SerializedName("Long")
    @Expose
    private String _long;

    /**
     *
     * @return
     * The addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     *
     * @param addressLine1
     * The AddressLine1
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     *
     * @return
     * The alternateContactNo
     */
    public String getAlternateContactNo() {
        return alternateContactNo;
    }

    /**
     *
     * @param alternateContactNo
     * The AlternateContactNo
     */
    public void setAlternateContactNo(String alternateContactNo) {
        this.alternateContactNo = alternateContactNo;
    }

    /**
     *
     * @return
     * The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The Lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The _long
     */
    public String getLong() {
        return _long;
    }

    /**
     *
     * @param _long
     * The Long
     */
    public void setLong(String _long) {
        this._long = _long;
    }

}

