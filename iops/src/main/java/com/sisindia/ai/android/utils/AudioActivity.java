package com.sisindia.ai.android.utils;
// https://github.com/remoorejr/cordova-plugin-media-with-compression/blob/master/src/android/AudioPlayer.java

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.models.RotaTaskModel;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import timber.log.Timber;

public class AudioActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int SAMPLE_RATE = 44100;
    private static final int BIT_RATE = 1000;
    private static final long MAX_RECORDING_LENGTH_SECONDS = 60;
    private static final long MAX_RECORDING_LENGTH_MILLIS = MAX_RECORDING_LENGTH_SECONDS * 1000;
    private static final int MY_PERMISSIONS_REQUEST_AUDIO = 1;
    private static final String TAG = "audio_play_failed";
    private final int STARTRECORDING = 0;
    private final int STOPRECORDING = 1;
    private final int PLAYRECORDING = 2;
    private final int STOPPLAYING = 3;
    public RotaTaskModel mRotaTaskModel;
    private TextView audioCounter;
    private TextView audioCounterProgStart;
    private ProgressBar audioProgressBar;
    private TextView audioCounterProgEnd;
    private ImageView btRecordPlayStop;
    private String timeFormat;
    private Button redoButton, submitAudio;
    private int type = 0;
    private MediaRecorder mediaRecorder;
    private long mediaRecordingStartTime;
    private MediaPlayer mediaPlayer;
    private long mediaPlaybackStartTime;
    private int permissionCheck;
    private String filename;
    private File mediaFile;
    private int attachmentCount = 0;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private int mReferenceId = 0; //used to set the reference type id based on navigation screen.
    private ArrayList<AttachmentMetaDataModel> attachmentMetaArrayList = new ArrayList<>();
    private byte[] audioBytes;
    private String currentPath;
    private int unitId, taskID, guardId;
    private Resources resource;
    private Toolbar toolbar;
    private AppPreferences appPreferences;
    private boolean fromIssueFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_audio_v2);
        timeFormat = getString(R.string.audio_timer);
        resource = getResources();
        appPreferences = new AppPreferences(this);
        audioCounter = (TextView) findViewById(R.id.audio_counter);
        audioCounterProgStart = (TextView) findViewById(R.id.audio_counter_prog_start);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        audioProgressBar = (ProgressBar) findViewById(R.id.audio_progress_bar);
        audioCounterProgEnd = (TextView) findViewById(R.id.audio_counter_prog_end);
        btRecordPlayStop = (ImageView) findViewById(R.id.bt_record_play_stop);
        btRecordPlayStop.setOnClickListener(this);
        redoButton = (Button) findViewById(R.id.bt_redo_audio);
        redoButton.setOnClickListener(this);
        submitAudio = (Button) findViewById(R.id.bt_submit_audio);
        submitAudio.setOnClickListener(this);
        setUpToolBar();
        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            unitId = bundle.getInt(TableNameAndColumnStatement.UNIT_ID, 0);
            taskID = bundle.getInt(TableNameAndColumnStatement.TASK_ID, 0);
            guardId = bundle.getInt(TableNameAndColumnStatement.GUARD_ID, 0);
            fromIssueFragment = bundle.getBoolean(TableNameAndColumnStatement.ISSUES_FRAGMENT, false);
        } else {
            unitId = 0;
            taskID = 0;
            guardId = 0;
        }

        if (!fromIssueFragment) {
            mRotaTaskModel = appPreferences.getRotaTaskJsonData();
            if (mRotaTaskModel != null) {
                unitId = mRotaTaskModel.getUnitId();
                taskID = mRotaTaskModel.getTaskId();
                guardId = Util.mGuard_Id;
            }
        }

        if (Util.incrementSequenceNumber()) {
            attachmentCount = attachmentCount + 1;
        } else {
            attachmentCount = 0;
        }
        attachmentMetaDataModel = new AttachmentMetaDataModel();

        //Set Layout for Recording basically prepare
        setLayoutForRecording();
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.grievance_audio_title));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_redo_audio:
                //Reset to Start

                setLayoutForRecording();
                break;
            case R.id.bt_submit_audio:
                //Submit your Audio
                submitAudio();
                break;
            case R.id.bt_record_play_stop:
                if (PackageManager.PERMISSION_GRANTED == permissionCheck) {
                    selectAudioAction();
                } else {
                    CheckAudioPermission();
                }
                break;
        }
    }


    private void setLayoutForRecording() {
        audioCounter.setText(String.format(timeFormat, 0));
        audioCounterProgStart.setText(String.format(timeFormat, 0));
        //Time Format
        long minutes = (MAX_RECORDING_LENGTH_SECONDS % 3600) / 60;
        long seconds = MAX_RECORDING_LENGTH_SECONDS % 60;
        String timeString = String.format("%02d:%02d", minutes, seconds);


        audioCounterProgEnd.setText(timeString);
        btRecordPlayStop.setImageResource(R.drawable.recordaudio);
        audioProgressBar.setProgress(0);
        submitAudio.setVisibility(View.GONE);
        redoButton.setVisibility(View.GONE);
        type = STARTRECORDING;
        audioProgressBar.setMax((int) MAX_RECORDING_LENGTH_SECONDS);

    }

    private void setLayoutForStoppingCapturedAudio() {
        btRecordPlayStop.setImageResource(R.drawable.stopaudio);
        audioProgressBar.setProgress(0);
        audioCounter.setText(String.format(timeFormat, 0));
        audioCounterProgStart.setText(String.format(timeFormat, 0));
        type = STOPPLAYING;
    }

    private void setLayoutForPlayingCapturedAudio() {
        type = PLAYRECORDING;
        audioProgressBar.setProgress(0);
        btRecordPlayStop.setImageResource(R.drawable.playaudio);
        redoButton.setVisibility(View.VISIBLE);
        submitAudio.setVisibility(View.VISIBLE);
    }


    private void setCapturedAudioData() {
        if (null != mediaFile) {


            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            int focusResult = am.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                }
            }, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

            if (focusResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                try {


                    byte[] voiceNoteBytes = FileUtils.readFileToByteArray(mediaFile);
                    audioBytes = voiceNoteBytes;
                    String absolutePath = mediaFile.getAbsolutePath();
                    String filePath = absolutePath.
                            substring(0, absolutePath.lastIndexOf(File.separator));
                    String filename = filePath.substring(filePath.lastIndexOf("/") + 1);
                    File tmpFile = new File(getFilesDir(), filename);
                    FileOutputStream fos = new FileOutputStream(tmpFile, false);
                    fos.write(voiceNoteBytes);
                    fos.flush();
                    fos.close();

                    //Read the file from SDCARD
                    FileInputStream fis = new FileInputStream(tmpFile);
                    int bytesAvailable = fis.available();
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(fis.getFD(), 0, bytesAvailable);
                    fis.close();
                    mediaPlayer.prepare();

                    //setValues , basically UI stuff
                    audioCounter.setText(String.format(timeFormat, 0));
                    long audioDuration = mediaPlayer.getDuration() / 1000;
                    long minutes = (audioDuration % 3600) / 60;
                    long seconds = audioDuration % 60;
                    String timeString = String.format("%02d:%02d", minutes, seconds);

                    audioCounterProgEnd.setText(timeString);
                    audioCounterProgStart.setText(String.format(timeFormat, 0));
                    audioProgressBar.setMax(mediaPlayer.getDuration() / 1000);
                    audioProgressBar.setProgress(0);

                    if (!tmpFile.delete()) {
                        Log.w(TAG, "failed to delete tmp playback file");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    am.abandonAudioFocus(null);
                }

            }
        } else {
            Util.customSnackBar(audioCounter, this, getResources().getString(R.string.audio_file_null));
        }
    }

    private void startPlayback() {

        if (mediaPlayer == null) {
            setCapturedAudioData();
        }
        mediaPlayer.start();
        mediaPlaybackStartTime = System.currentTimeMillis();
        btRecordPlayStop.postDelayed(new Runnable() {
            @Override
            public void run() {
                runCounterForPlayback();
            }
        }, 100);

    }

    private void startAudioCapture() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setAudioChannels(1); // single channel
        mediaRecorder.setAudioSamplingRate(SAMPLE_RATE); // 44.1 kHz for decent sound, similar to stock iOS media plugin
        mediaRecorder.setAudioEncodingBitRate(BIT_RATE); // low bit rate
        mediaRecorder.setOutputFile(getAudioFile().getAbsolutePath());
        try {
            mediaRecorder.prepare();

            mediaRecorder.start();
            mediaRecordingStartTime = System.currentTimeMillis();
            btRecordPlayStop.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runCounter();
                }
            }, 100);

        } catch (IOException e) {
            Log.e("audio failure", "prepare() failed", e);
            return;
        }
    }

    private void stopAudioCapture() {
        try {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        } catch (Exception e) {
            Log.e("audio_fail", "failed to stop recording", e);
        }


        //getAudioFile();
        //get and set CapturedAudioData
        setCapturedAudioData();
    }

    private void stopPlayback() {
        audioProgressBar.setProgress(0);
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }


    //Counters for Progress bar
    private void runCounter() {
        if (mediaRecorder == null) {
            return; //recording already stopped
        }

        long elapsedTimeMillis = System.currentTimeMillis() - mediaRecordingStartTime;
        long elapsedTimeSeconds = elapsedTimeMillis / 1000;


        long minutes = (elapsedTimeSeconds % 3600) / 60;
        long seconds = elapsedTimeSeconds % 60;

        long remSec = MAX_RECORDING_LENGTH_SECONDS - elapsedTimeSeconds;
        long minutesRem = (remSec % 3600) / 60;
        long secondsRem = remSec % 60;


        String timeString = String.format("%02d:%02d", minutes, seconds);
        String remainingTimeString = String.format("%02d:%02d", minutesRem, secondsRem);


        audioCounter.setText(timeString);
        audioCounterProgStart.setText(timeString);
        audioCounterProgEnd.setText(remainingTimeString);
        audioProgressBar.setProgress((int) elapsedTimeSeconds);
        if (elapsedTimeMillis > MAX_RECORDING_LENGTH_MILLIS) {
            setLayoutForPlayingCapturedAudio();
            stopAudioCapture();
            Toast.makeText(AudioActivity.this, "Max lenght reached", Toast.LENGTH_SHORT).show();
        } else {
            btRecordPlayStop.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runCounter();
                }
            }, 100);
        }
    }

    private void runCounterForPlayback() {
        if (mediaPlayer == null) {
            return;
        }
        long elapsedTimeMillis = System.currentTimeMillis() - mediaPlaybackStartTime;
        long elapsedTimeSeconds = elapsedTimeMillis / 1000;


        long minutes = (elapsedTimeSeconds % 3600) / 60;
        long seconds = elapsedTimeSeconds % 60;

        long remSec = (mediaPlayer.getDuration() / 1000) - elapsedTimeSeconds;
        long minutesRem = (remSec % 3600) / 60;
        long secondsRem = remSec % 60;


        String timeString = String.format("%02d:%02d", minutes, seconds);
        String remainingTimeString = String.format("%02d:%02d", minutesRem, secondsRem);


        audioCounter.setText(timeString);
        audioCounterProgStart.setText(timeString);
        audioCounterProgEnd.setText(remainingTimeString);

        audioProgressBar.setProgress((int) elapsedTimeSeconds);
        if (elapsedTimeMillis > mediaPlayer.getDuration()) {
            setLayoutForPlayingCapturedAudio();
            stopPlayback();
        } else {
            btRecordPlayStop.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runCounterForPlayback();
                }
            }, 100);
        }
    }

    //get AudioFile from dir
    private File getAudioFile() {
        Util.mAudioSequenceNumber++;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        Timber.d("timeStamp %s", timeStamp);
        // filename ="audiorecording_"+timeStamp +".m4a";
        File mediaStorageDir = new File(Constants.AUDIO_FILE_PATH);
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();

        }

        if (0 != taskID) {
            filename = Constants.FILE_TYPE_GREIVANCE + "_" + guardId + "_" + taskID + "_" + Util.mAudioSequenceNumber + "_" + "Audio_" + timeStamp + Constants.AUDIO_FILE_EXTENSION;

        } else {
            filename = Constants.FILE_TYPE_GREIVANCE + "_" + guardId + "_" + Util.mAudioSequenceNumber + "_" + "Audio_" + timeStamp + Constants.AUDIO_FILE_EXTENSION;

        }

        //fileName = taskType + "_1_"+ Util.getTime(Calendar.getInstance().getTimeInMillis(),"captureScreen")+ sequenceNo + ".jpeg";
        // stampName = Util.getmGuard_Name();
        mReferenceId = 0;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);
        currentPath = mediaStorageDir.getPath() + File.separator + filename;

        return mediaFile;

    }

    private void setLayoutForStoppingRecording() {
        btRecordPlayStop.setImageResource(R.drawable.stopaudio);
        audioProgressBar.setProgress(0);
        type = STOPRECORDING;
    }


    //Network Call to Submit

    private void submitAudio() {
        addFileToModel();
    }


    @Override
    protected void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        } else if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
                mediaRecorder.release();
                mediaRecorder = null;
            } catch (Exception e) {
                Log.e("audio_fail", "failed to stop recording", e);
            }
        }
        super.onDestroy();
    }


    private void CheckAudioPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_AUDIO);
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_AUDIO);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionCheck = PackageManager.PERMISSION_GRANTED;
                    selectAudioAction();

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void selectAudioAction() {
        switch (type) {
            case STARTRECORDING:
                //Set Layout to enable stopping the recording
                setLayoutForStoppingRecording();
                startAudioCapture();
                break;
            case STOPRECORDING:
                //Set Layout to enable the captured audio to be played
                setLayoutForPlayingCapturedAudio();
                stopAudioCapture();
                break;
            case PLAYRECORDING:
                //Set Layout to enable the captured audio to be paused/stopped
                setLayoutForStoppingCapturedAudio();
                startPlayback();
                break;
            case STOPPLAYING:
                //Set Layout to enable the captured audio to be played
                setLayoutForPlayingCapturedAudio();
                stopPlayback();
                break;
        }
    }

    private void addFileToModel() {
        String size = android.text.format.Formatter.formatFileSize(this, audioBytes.length);

        Timber.d("size  %s", size);
        size = size.replaceAll("[^\\d.]", "");
        int imagesize = (int) Math.round(Double.valueOf(size));
        Timber.d("size  %s", imagesize);
        attachmentMetaDataModel.setAttachmentPath(currentPath);
        attachmentMetaDataModel.setAttachmentFileName(filename.trim());
        attachmentMetaDataModel.setAttachmentTitle(filename.trim());
        attachmentMetaDataModel.setAttachmentFileExtension(Constants.AUDIO_FILE_EXTENSION);
        attachmentMetaDataModel.setAttachmentTypeId(Constants.LOOKUP_TYPE_AUDIO);// look up value for audio
        attachmentMetaDataModel.setAttachmentSourceId(mReferenceId);
        attachmentMetaDataModel.setGuradId(guardId); // added only for deletion
        attachmentMetaDataModel.setAttachmentFileSize(imagesize);
        attachmentMetaDataModel.setTaskId(taskID);
        attachmentMetaDataModel.setUnitId(unitId);
        attachmentMetaDataModel.setSequenceNo(Util.mAudioSequenceNumber);
        Intent barrackOthers = new Intent();
        barrackOthers.putExtra(Constants.META_DATA, attachmentMetaDataModel);
        setResult(RESULT_OK, barrackOthers);
        finish();

    }

    @Override
    public void onBackPressed() {
        Util.mAudioSequenceNumber--;
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
