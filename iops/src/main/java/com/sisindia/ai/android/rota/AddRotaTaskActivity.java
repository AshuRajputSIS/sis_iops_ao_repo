package com.sisindia.ai.android.rota;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.commons.SelectTimeFragment;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.mybarrack.BarrackNameAdapter;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myunits.SingleUnitActivity;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitDetails;
import com.sisindia.ai.android.myunits.models.UnitList;
import com.sisindia.ai.android.network.response.AppRole;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.models.UnitTask;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.bottomsheet.OnBottomSheetItemClickListener;
import com.sisindia.ai.android.utils.bottomsheet.ShowBottomSheetListener;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class AddRotaTaskActivity extends BaseActivity implements
        OnBottomSheetItemClickListener, ShowBottomSheetListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = "EditTaskActivity";
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ShowBottomSheetListener showBottomSheetListener;
    @Bind(R.id.tv_barrack_name)
    CustomFontTextview tvBarrackName;
    @Bind(R.id.barrack_name_spinner)
    Spinner barrack_name_spinner;
    @Bind(R.id.barrack_name)
    CustomFontTextview barrackName;
    @Bind(R.id.barrack_layout)
    RelativeLayout barrackLayout;
    @Bind(R.id.rl_unit_name)
    RelativeLayout rlUnitName;
    @Bind(R.id.reason_label)
    CustomFontTextview reasonLabel;
    @Bind(R.id.reasonSpinner)
    Spinner reasonSpinner;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private RotaTaskModel addedRotaModel;
    private RotaTaskListMO rotaTaskList;
    private String checkingType;
    private CustomFontTextview tvUnitName;
    private CustomFontTextview startTaskDate;
    private CustomFontTextview endTaskDate;
    private CustomFontTextview taskStartTime;
    private CustomFontTextview taskEndTime;
    private CustomFontTextview enterLocation;
    private Toolbar toolbar;
    private ImageView checkTypeImage;
    private RelativeLayout mparentRelativeLayout;
    private List<MyUnitHomeMO> unitDetailsList;
    private SelectDateFragment selectDateFragment;
    private String unitAddress;
    private Integer unitId = null;
    private Integer barrackId = null;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String estimatedStartTime;
    private String estimatedEndTime;
    private String selectedStartTime;
    private String selectedEndTime;
    private String startedDate = null;
    private List<UnitDetails> unitDetailsArrayList;
    private boolean canRotaTaskCreate;
    private boolean FromMyBarrack;
    private List<Barrack> barrackList;
    private int spinnerItemPosition;
    private BarrackNameAdapter adapter;
    private int taskTypeId;
    private Context mContext;
    private List<UnitList> unitNameListMO;
    private int bottomSheetViewId = 0;
    private int reasonId;
    private TimeLineModel timeLineModel;
    private AppRole appUserDetails;
    private boolean isFromSingleUnit;
    private LookUpSpinnerAdapter otherActivityReasonAdapter;
    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private ArrayList<LookupModel> lookupModelOtherActivityList;
    private UnitTask newRotaTask;
    private Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rotatask_detail_screen);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;
        resources = getResources();
        mparentRelativeLayout = (RelativeLayout) findViewById(R.id.parent_relative_layout);
        enterLocation = (CustomFontTextview) findViewById(R.id.tv_enter_location);
        barrackList = new ArrayList<>();
        if (getIntent() != null) {
            FromMyBarrack = getIntent().getBooleanExtra("FromMyBarrack", false);
            reasonId = getIntent().getExtras().getInt(getResources().getString(R.string.select_reason));
            rotaTaskList = (RotaTaskListMO) getIntent().getSerializableExtra(SingleUnitActivity.CHECKING_TYPE);
            isFromSingleUnit = getIntent().getBooleanExtra(getResources().getString(R.string.single_unit_flag), false);
            checkingType = rotaTaskList.getRotaTaskName();
            if (FromMyBarrack) {
                barrackId = getIntent().getIntExtra("BarrackId", 0);
                unitNameListMO = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getListOfUnitNameByBarrackId(barrackId);
            }
        }
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(checkingType);
        checkTypeImage = (ImageView) findViewById(R.id.checkType_image);
        if (null != rotaTaskList) {
            unitId = rotaTaskList.getUnitId();
            taskTypeId = rotaTaskList.getRotaTaskActivityId();
            collapsingToolbarLayout.setTitle("Add " + rotaTaskList.getRotaTaskName());
            checkTypeImage.setImageResource(RotaTaskWhiteIconEnum.valueOf(RotaTaskTypeEnum.
                    valueOf(rotaTaskList.getRotaTaskWhiteIcon()).name()).getValue());
        }
        showBottomSheetListener = this;
        initView();
    }

    private void initView() {
        tvUnitName = (CustomFontTextview) findViewById(R.id.tv_unit_value);
        startTaskDate = (CustomFontTextview) findViewById(R.id.tv_start_date);
        endTaskDate = (CustomFontTextview) findViewById(R.id.tv_end_date);
        taskStartTime = (CustomFontTextview) findViewById(R.id.tv_task_start_time);
        taskEndTime = (CustomFontTextview) findViewById(R.id.tv_task_end_time);
        tvUnitName.setGravity(Gravity.RIGHT);
        barrack_name_spinner.setOnItemSelectedListener(this);
        if (taskTypeId == 3) {
            barrackLayout.setVisibility(View.VISIBLE);
            barrack_name_spinner.setVisibility(View.GONE);
            barrackName.setVisibility(View.VISIBLE);
        } else {
            barrackLayout.setVisibility(View.GONE);
        }
        addedRotaModel = new RotaTaskModel();
        newRotaTask = new UnitTask();
        unitDetailsList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitDetails(0);
        if (FromMyBarrack) {
            tvUnitName.setClickable(false);
            barrackList = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackDetailsById(barrackId);
            setBarrackName(barrackList);
            canRotaTaskCreate = true;
            if (unitNameListMO != null && unitNameListMO.size() != 0) {
                if (unitNameListMO.size() == 1) {
                    tvUnitName.setText(unitNameListMO.get(0).getUnitName());
                } else {
                    tvUnitName.setText(getResources().getString(R.string.MULTIPLE));
                }
            } else {
                tvUnitName.setText(getResources().getString(R.string.NOT_AVAILABLE));
            }
        } else {
            if (isFromSingleUnit) {
                tvUnitName.setClickable(false);
            } else {
                tvUnitName.setClickable(true);
            }
            if (rotaTaskList != null) {
                if (rotaTaskList.getUnitName() != null && !rotaTaskList.getUnitName().isEmpty()) {
                    tvUnitName.setText(rotaTaskList.getUnitName());
                    unitDetailsList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                            .getUnitDetails(rotaTaskList.getUnitId());
                    if (null != unitDetailsList && unitDetailsList.size() != 0) {
                        setUnitNameAndLocation(unitDetailsList.get(0), bottomSheetViewId);
                    }
                }
            }
        }

        if (!FromMyBarrack) {
            if (!isFromSingleUnit) {
                tvUnitName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showBottomSheetListener.showUnitModelBottomSheet(unitDetailsList, R.id.tv_unit_value);
                    }
                });
            }
        }

        startTaskDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                selectDateFragment = new SelectDateFragment(mparentRelativeLayout, null);
                selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
                selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        startTaskDate.setText(updateData);
                    }
                });
            }
        });

        endTaskDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();

                selectDateFragment = new SelectDateFragment(mparentRelativeLayout, null);

                //Commenting as its causing exception
                /*if (startTaskDate.getText().toString().isEmpty() || startTaskDate.getText().toString() == null)
                    selectDateFragment = new SelectDateFragment(mparentRelativeLayout, null);
                else
                    selectDateFragment = new SelectDateFragment(mparentRelativeLayout, startTaskDate.getText().toString());*/

                selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
                selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        endTaskDate.setText(updateData);
//                        addedRotaModel.set(updateData);
                        addedRotaModel.setEndDate(updateData);
                    }
                });
            }
        });
        taskStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                SelectTimeFragment selectTimeFragment = new SelectTimeFragment(mparentRelativeLayout);
                selectTimeFragment.show(fragmentTransaction, "TimepickerFragment");
                selectTimeFragment.setCheckingType(checkingType);
                Timber.e("checkingType " + checkingType);
                selectTimeFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        selectedStartTime = updateData;
                        taskStartTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(updateData));
                        selectedStartTime = updateData;
                        taskStartTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(updateData));


                    }
                });
            }
        });
        taskEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                SelectTimeFragment selectTimeFragment = new SelectTimeFragment(mparentRelativeLayout);
                selectTimeFragment.show(fragmentTransaction, "TimepickerFragment");
                selectTimeFragment.setCheckingType(checkingType);
                selectTimeFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        selectedEndTime = updateData;
                        taskEndTime.setText(dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(updateData));
                    }
                });
            }
        });
        if (taskTypeId == 7) {
            otherActivityReasons();
        }
    }

    private void otherActivityReasons() {
        reasonLabel.setVisibility(View.VISIBLE);
        reasonSpinner.setVisibility(View.VISIBLE);
        otherActivityReasonAdapter = new LookUpSpinnerAdapter(this);
        issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
        LookupModel lookupModel = new LookupModel();
        lookupModel.setName(resources.getString(R.string.SELECT_REASON));
        lookupModelOtherActivityList = new ArrayList<>();
        lookupModelOtherActivityList.add(lookupModel);
        lookupModelOtherActivityList.addAll(IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.OtherActivity), null));
        otherActivityReasonAdapter.setSpinnerData(lookupModelOtherActivityList);
        reasonSpinner.setAdapter(otherActivityReasonAdapter);
        reasonSpinner.setOnItemSelectedListener(this);
        if (util != null) {
            reasonLabel.setText(util.createSpannableStringBuilder(getResources().getString(R.string.reason)));
        } else {
            Timber.d("Util :" + "Util object is null");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save) {
            validateAllDate();
            return true;
        }
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<UnitTask> createNewRotaTaskModel(String date, String estimatedStartDateTime, String estimatedEndDateTime) {
        List<UnitTask> newRotaTaskList = new ArrayList<>();
        newRotaTask.setId(0);
        if (unitId == null || unitId == 0) {
            newRotaTask.setUnitId(null);
        } else {
            newRotaTask.setUnitId(unitId);
        }
        newRotaTask.setUnitName(tvUnitName.getText().toString());
        newRotaTask.setBarrackId(barrackId);
        newRotaTask.setTaskTypeId(RotaTaskTypeEnum.valueOf(rotaTaskList.getRotaTaskWhiteIcon()).getValue());
        newRotaTask.setCreatedDateTime(dateTimeFormatConversionUtil.getCurrentDateTime().trim());
        newRotaTask.setEstimatedTaskExecutionStartDateTime(estimatedStartDateTime.trim());
        newRotaTask.setEstimatedTaskExecutionEndDateTime(estimatedEndDateTime.trim());
        newRotaTask.setEstimatedExecutionTime(null);
        newRotaTask.setTaskStatusId(1);
        newRotaTask.setActualExecutionTime(0);
        newRotaTask.setActualTaskExecutionEndDateTime("");
        newRotaTask.setActualTaskExecutionStartDateTime("");
        newRotaTask.setAdditionalComments("");
        newRotaTask.setUnitPostId(null);
        newRotaTask.setSelectReasonId(reasonId);
        newRotaTask.setExecutorId(appUserDetails.getEmployeeId());
        newRotaTask.setApprovedById(null);
        newRotaTask.setAssigneeId(appUserDetails.getEmployeeId());
        newRotaTask.setAssigneeId(appUserDetails.getEmployeeId());
        newRotaTask.setAssignedById(appUserDetails.getEmployeeId());
        newRotaTask.setIsAutoCreation(false);
        newRotaTask.setCreatedById(appUserDetails.getEmployeeId());
        newRotaTaskList.add(newRotaTask);
        return newRotaTaskList;
    }

    private void validateAllDate() {
        if (tvUnitName.getText().toString() == null || tvUnitName.getText().toString().isEmpty()) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_unit_name));
            return;
        } else if (startTaskDate.getText().toString() == null || startTaskDate.getText().toString().isEmpty()) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_start_date));
            return;
        } else if (taskStartTime.getText().toString() == null || taskStartTime.getText().toString().isEmpty()) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.enter_start_time));
            return;
        } else if (endTaskDate.getText().toString() == null || endTaskDate.getText().toString().isEmpty()) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_end_date));
            return;
        } else if (taskEndTime.getText().toString() == null || taskEndTime.getText().toString().isEmpty()) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.enter_end_time));
            return;
        } else if (reasonSpinner.getVisibility() == View.VISIBLE && newRotaTask != null
                && newRotaTask.getOtherReasonId() == null) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.select_reason_msg));
            return;
        } else {
            try {
                String startedDate = null;
                if (startTaskDate.getText().toString() != null && taskStartTime.getText().toString() != null &&
                        endTaskDate.getText().toString() != null && taskEndTime.getText().toString() != null) {
                    startedDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(startTaskDate.getText().toString());
                    String endDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(endTaskDate.getText().toString());
                    String startTime = dateTimeFormatConversionUtil.timeConverstionFrom12To24(selectedStartTime);
                    String endTime = dateTimeFormatConversionUtil.timeConverstionFrom12To24(selectedEndTime);
                    estimatedStartTime = startedDate + " " + startTime;
                    estimatedEndTime = endDate + " " + endTime;
                    if (startedDate != null && startTime != null && startedDate.equals(endDate) && startTime.equals(endTime)) {
                        snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.same_start_end_time));
                        return;
                    }
                    Timber.e("estimatedStartTime: " + estimatedStartTime + "estimatedEndTime" + estimatedEndTime);
                }
                SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String currentDateandTime = currentDate.format(new Date());
                Date currentDateTime = currentDate.parse(currentDateandTime);
                Date estimatedStartDateTime = currentDate.parse(estimatedStartTime);
                Date estimatedEndDateTime = currentDate.parse(estimatedEndTime);
                Timber.e("currentDateandTime" + currentDateandTime + "currentDateTime: " + currentDateTime + RotaTaskTypeEnum.valueOf(rotaTaskList.getRotaTaskWhiteIcon()).getValue());
                Timber.e("estimatedStartDateTime" + estimatedStartDateTime + "estimatedEndDateTime: " + estimatedEndDateTime);
                if (currentDateTime.after(estimatedStartDateTime)) {
                    snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.compare_with_current_time));
                    return;
                } else if (estimatedStartDateTime.after(estimatedEndDateTime)) {
                    snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.compare_time));
                    return;
                } else {
                    appUserDetails = IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserDetails();
                    if (taskTypeId == RotaTaskTypeEnum.Night_Checking.getValue()) {
                        if (dateTimeFormatConversionUtil.nightCheckValidation(estimatedStartTime, estimatedEndTime)) {
                            createRotaTask();
                        } else {
                            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.night_check_validation));
                        }
                    } else {
                        createRotaTask();
                    }

                /*    if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                            isHolidayAndLeaveAvailable(appUserDetails.getEmployeeId(), estimatedStartTime, estimatedEndTime) == 0) {

                    } else {
                        List<HolidayAndLeaveDateMO> holidayAndLeaveDateMOList = new ArrayList<>();
                        holidayAndLeaveDateMOList= IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                                getHolidayAndLeaveDate(appUserDetails.getEmployeeId(), estimatedStartTime, estimatedEndTime);
                        if(null!=holidayAndLeaveDateMOList && holidayAndLeaveDateMOList.size()!=0){
                            if(holidayAndLeaveDateMOList.contains(estimatedStartTime) || holidayAndLeaveDateMOList.contains(estimatedEndTime)){
                                createRotaTask();
                            }
                            else {
                                snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.LEAVE_HOLIDAY));
                            }
                        }else {
                            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.LEAVE_HOLIDAY));
                        }
                    }*/
                }
            } catch (ParseException e) {
                Crashlytics.logException(e);
            }
        }
    }

    private void createRotaTask() {
//        String[] startDate = estimatedStartTime.split(" ");
//        String[] endDate = estimatedEndTime.split(" ");
        if (!canRotaTaskCreate) {
            snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.NO_BARRACK_AVAILABLE));
        } else {
            //Plz don't delete the below code , we have to add this code in prod
            /*ArrayList<Integer> taskTypeIds;
            if (FromMyBarrack) {
                taskTypeIds = IOPSApplication.getInstance().
                        getSelectionStatementDBInstance().getTaskTypeIds(barrackId, startDate[0], FromMyBarrack);

            } else {
                taskTypeIds = IOPSApplication.getInstance().
                        getSelectionStatementDBInstance().getTaskTypeIds(unitId, startDate[0], FromMyBarrack);

            }

            //   ArrayList<Integer> otherActivityReasonIds = IOPSApplication.getInstance().getSelectionStatementDBInstance().getOtherTaskReasonIds(unitId,taskTypeId,startDate[0]);
            if (taskTypeIds.contains(RotaTaskTypeEnum.valueOf(rotaTaskList.getRotaTaskWhiteIcon()).getValue())) {
                snackBarWithDuration(mparentRelativeLayout, "Only One " + RotaTaskTypeEnum.valueOf(rotaTaskList.getRotaTaskWhiteIcon()).name().replace("_", " ") + " is Allowed", 10000);
                return;
            } else {}*/
            if (unitId != null) {
                if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getTasksWithSameData(unitId,
                        taskTypeId, estimatedStartTime, estimatedEndTime)) {
                    snackBarWithMesg(mparentRelativeLayout, getResources().
                            getString(R.string.task_already_created_msg));
                    return;
                } else {
                    if (dateTimeFormatConversionUtil.validateTimeDiffBetweenStartAndEndDateTime(estimatedStartTime, estimatedEndTime)) {
                        snackBarWithMesg(mparentRelativeLayout, getResources().
                                getString(R.string.start_end_time_difference_msg));
                        return;
                    } else {
                        saveTaskDetails();
                    }
                }
            } else {
                if (dateTimeFormatConversionUtil.validateTimeDiffBetweenStartAndEndDateTime(estimatedStartTime, estimatedEndTime)) {
                    snackBarWithMesg(mparentRelativeLayout, getResources().
                            getString(R.string.start_end_time_difference_msg));
                    return;
                } else {
                    saveTaskDetails();
                }
            }
        }
    }

    private void saveTaskDetails() {
        List<UnitTask> newRotaTask = createNewRotaTaskModel(startedDate, estimatedStartTime, estimatedEndTime);
        IOPSApplication.getInstance().getRotaLevelInsertionInstance().insertTaskTable(newRotaTask, 1, 0, false);
        insertLogActivity();
        if (timeLineModel != null) {
            IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
        }
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);

        Intent homeIntent = new Intent(AddRotaTaskActivity.this, HomeActivity.class);
        homeIntent.putExtra(OnBoardingFactory.FROM_ONBOADING, false);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
        finish();
    }

    private void setUnitNameAndLocation(MyUnitHomeMO unitHomeMO, int viewId) {
//        List<Integer> barrackIdList = new ArrayList<>();
        unitDetailsArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().
                getUnitDetailsFromUnitName(unitHomeMO.getUnitId());
        if (null != unitDetailsArrayList && unitDetailsArrayList.size() != 0) {
            for (UnitDetails unitDetail : unitDetailsArrayList) {
                if (unitHomeMO.getUnitName().equalsIgnoreCase(unitDetail.getUnitName())) {
                    unitId = unitDetail.getUnitID();
                    barrackList = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackDetailsByUnitId(unitId);
                    switch (RotaTaskTypeEnum.valueOf(rotaTaskList.getRotaTaskWhiteIcon()).getValue()) {
                        case 3:
                            if (FromMyBarrack) {
                                canRotaTaskCreate = true;
                                setBarrackName(barrackList);
                                if (unitDetail.getUnitName() != null && !unitDetail.getUnitName().isEmpty()) {
                                    tvUnitName.setText(unitDetail.getUnitName());
                                } else {
                                    tvUnitName.setText(getResources().getString(R.string.NOT_AVAILABLE));
                                }
                            } else {
                                if (barrackList.size() == 0) {
                                    tvUnitName.setText("");
                                    enterLocation.setText("");
                                    tvUnitName.setHint(getResources().getString(R.string.enter_unit_name));
                                    barrack_name_spinner.setVisibility(View.GONE);
                                    barrackName.setVisibility(View.VISIBLE);
                                    barrackName.setHint(getResources().getString(R.string.BARRACK_NAME_HINT));
                                    snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.NO_BARRACK_AVAILABLE));
                                    canRotaTaskCreate = false;
                                } else {
                                    canRotaTaskCreate = true;
                                    setBarrackName(barrackList);
                                    setUnitAndLocation(unitHomeMO, unitDetail);
                                }
                            }
                            break;
                        case 4:
                            if (unitDetail.getBillAuthoritativeUnit() != null) {
                                setUnitAndLocation(unitHomeMO, unitDetail);
                            } else {
                                snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.CLIENT_COORDINATION_TASK_VALIDATION));
                                canRotaTaskCreate = false;
                            }
                            break;
                        case 5:
                            if (unitDetail.getBillAuthoritativeUnit() != null) {
                                setUnitAndLocation(unitHomeMO, unitDetail);
                            } else {
                                snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.BILL_SUBMISSION_TASK_VALIDATION));
                                canRotaTaskCreate = false;
                            }
                            break;
                        case 6:
                            if (unitDetail.getBillAuthoritativeUnit() != null) {
                                setUnitAndLocation(unitHomeMO, unitDetail);
                            } else {
                                snackBarWithMesg(mparentRelativeLayout, getResources().getString(R.string.BILL_COLLECTION_TASK_VALIDATION));
                                canRotaTaskCreate = false;
                            }
                            break;
                        default:
                            setUnitAndLocation(unitHomeMO, unitDetail);
                            break;
                    }
                }
            }
        }
    }

    private void setBarrackAddress(Barrack barrackMO) {
        if (barrackMO.getBarrackAddress() != null && !barrackMO.getBarrackAddress().isEmpty()) {
            enterLocation.setText(barrackMO.getBarrackAddress());
        } else {
            enterLocation.setText(getResources().getString(R.string.NOT_AVAILABLE));
        }
    }

    private void setBarrackName(List<Barrack> barrackList) {
        if (null != barrackList && barrackList.size() != 0) {
            if (barrackList.size() == 1) {
                barrack_name_spinner.setVisibility(View.GONE);
                barrackName.setVisibility(View.VISIBLE);
                barrackName.setText(barrackList.get(0).getName().toString().trim());
                barrackId = barrackList.get(0).getId();
                enterLocation.setText(barrackList.get(0).getBarrackAddress());
            } else {
                barrack_name_spinner.setVisibility(View.VISIBLE);
                barrackName.setVisibility(View.GONE);
                adapter = new BarrackNameAdapter(mContext, barrackList);
                barrack_name_spinner.setGravity(Gravity.RIGHT);
                barrack_name_spinner.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        } else {
            barrackName.setText(getResources().getString(R.string.NOT_AVAILABLE));
            barrack_name_spinner.setVisibility(View.GONE);
            barrackName.setVisibility(View.VISIBLE);
        }
    }

    private void setUnitAndLocation(MyUnitHomeMO myUnitHomeMO, UnitDetails unitDetails) {
        canRotaTaskCreate = true;
        tvUnitName.setText(myUnitHomeMO.getUnitName());
        if (unitDetails.getUnitAddress() != null && !unitDetails.getUnitAddress().isEmpty()) {
            enterLocation.setText(unitDetails.getUnitAddress());
        } else {
            enterLocation.setText(getResources().getString(R.string.NOT_AVAILABLE));
        }
    }

    @Override
    public void onSheetItemClick(String itemSelected, int viewId) {

    }

    @Override
    public void onUnitSheetItemClick(MyUnitHomeMO unitHomeMOList, int viewId) {
        bottomSheetViewId = viewId;
        if (taskTypeId == 7) {
            setUnitNameAndLocation(unitHomeMOList, bottomSheetViewId);
        } else {
            if (unitHomeMOList.getUnitId() == -1 || unitHomeMOList.getUnitId() == -2) {
                tvUnitName.setText("");
                Toast.makeText(this, getResources().getString(R.string.task_creation_msg), Toast.LENGTH_LONG).show();
                return;
            } else {
                setUnitNameAndLocation(unitHomeMOList, bottomSheetViewId);
            }
        }

    }

    @Override
    public void showBottomSheet(ArrayList<String> unitNameList, int viewId) {
    }

    @Override
    public void showUnitModelBottomSheet(List<MyUnitHomeMO> myUnitHomeMOs, int viewId) {
        UnitNameBottomSheet bottomSheetDialogFragment = new UnitNameBottomSheet();
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        bottomSheetDialogFragment.setSheetItemClickListener(this);
        bottomSheetDialogFragment.setBottomSheetData(myUnitHomeMOs, viewId);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int spinnerId = parent.getId();
        switch (spinnerId) {
            case R.id.reasonSpinner:
                if (lookupModelOtherActivityList != null && lookupModelOtherActivityList.size() != 0) {
                    if (newRotaTask != null) {
                        if (position != 0) {
                            newRotaTask.setOtherReasonId(lookupModelOtherActivityList.get(position).getLookupIdentifier());
                        } else {
                            newRotaTask.setOtherReasonId(null);
                        }
                    }
                }
                break;
            case R.id.barrack_name_spinner:
                if (barrackList != null && barrackList.size() != 0) {
                    spinnerItemPosition = position;
                    setBarrackAddress(barrackList.get(position));
                    barrackId = barrackList.get(position).getId();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        /*barrackName.setVisibility(View.GONE);
        barrack_name_spinner.setVisibility(View.VISIBLE);*/
    }

    public void insertLogActivity() {
        timeLineModel = new TimeLineModel();
        timeLineModel.setActivity("Task Creation");
        timeLineModel.setLocation("");
        timeLineModel.setTaskId("null");
        timeLineModel.setStatus(RotaTaskStatusEnum.Active.name());
        timeLineModel.setTaskName(rotaTaskList.getRotaTaskName());
        timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
        timeLineModel.setEndTime("");
        timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());
    }
}
