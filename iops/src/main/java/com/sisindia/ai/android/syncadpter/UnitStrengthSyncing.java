package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.UnitStrengthIR;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 29-06-2016.
 */

public class UnitStrengthSyncing extends CommonSyncDbOperation {
    private final int taskTypeId;
    private Context mContext;
    private SISClient sisClient;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private int taskId;
    private AppPreferences appPreferences;
    HashMap<Integer, UnitStrengthIR> barrackStrengthMap;
    HashMap<Integer, UnitStrengthIR> unitStrengthIRHashMap;

    public UnitStrengthSyncing(Context mcontext, int taskId ,int taskTypeId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        this.taskId  = taskId;
        this.taskTypeId = taskTypeId;
        if(taskTypeId == 1 || taskTypeId == 2) {
            unitStrengthSync();
        }
        if(taskTypeId == 3) {
            barrackStrengthSync();
        }
    }

    public void updateUnitStrengthSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(taskId == dependentSyncsStatus.taskId){
                    dependentSyncsStatus.isUnitStrengthSynced = count;
                    updateUnitStrengthCount(dependentSyncsStatus);
                }
            }
        }
    }

    private void updateUnitStrengthCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(
                        dependentSyncsStatus.taskId,
                Constants.TASK_RELATED_SYNC, TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED,
                dependentSyncsStatus.isUnitStrengthSynced,dependentSyncsStatus.taskTypeId
                );
    }

    private synchronized void barrackStrengthSync() {
         barrackStrengthMap = getBarrackStrength();
         barrackStrengthMap.size();
        if (barrackStrengthMap != null && barrackStrengthMap.size() != 0) {
            updateUnitStrengthSyncCount(barrackStrengthMap.size());
            for (Integer key : barrackStrengthMap.keySet()) {
                UnitStrengthIR unitStrengthIR = barrackStrengthMap.get(key);
                Timber.d(Constants.TAG_SYNCADAPTER+"barrackStrengthSync barrackStrengthSync   %s", unitStrengthIR);
                    barrackStrengthApiSync(unitStrengthIR,key);
            }

        }
        else{
            updateUnitStrengthSyncCount(0);
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthSyncing -->nothing to sync ");
        }

    }

    private HashMap<Integer, UnitStrengthIR> getBarrackStrength() {
        SQLiteDatabase sqlite = null;
         unitStrengthIRHashMap = new HashMap<>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                " where "
                + TableNameAndColumnStatement.TASK_ID + " >  " + 0 + " and "
                + TableNameAndColumnStatement.IS_SYNCED + " = 0 and "
                + TableNameAndColumnStatement.TASK_ID + " = " + taskId +" and "
                + TableNameAndColumnStatement.RANK_ID +" is null";
        Timber.d(Constants.TAG_SYNCADAPTER+"BarrackStrengthDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitStrengthIR unitStrengthIR = new UnitStrengthIR();
                        unitStrengthIR.setActualStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL)));

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID))!=0){
                            unitStrengthIR.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        }

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID))!=0){
                            unitStrengthIR.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));

                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID))!=0){
                            unitStrengthIR.setRankId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID)));
                        }

                        unitStrengthIR.setUnitTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        unitStrengthIR.setLeaveCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LEAVE_COUNT)));
                        unitStrengthIR.setCheckedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        unitStrengthIRHashMap.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), unitStrengthIR);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
            exception.printStackTrace();
        } finally {
            sqlite.close();
            Timber.d(Constants.TAG_SYNCADAPTER+"BarrackStrengthIR object  %s", IOPSApplication.getGsonInstance().toJson(unitStrengthIRHashMap));
        }
        return unitStrengthIRHashMap;
    }

    private synchronized void unitStrengthSync(){
    unitStrengthIRHashMap = getUnitStrengthDetails();
    if (unitStrengthIRHashMap != null && unitStrengthIRHashMap.size() != 0) {
        updateUnitStrengthSyncCount(unitStrengthIRHashMap.size());
        for (Integer key : unitStrengthIRHashMap.keySet()) {
            UnitStrengthIR unitStrengthIR = unitStrengthIRHashMap.get(key);
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthSyncing UnitStrengthSyncing   %s", unitStrengthIR);
            unitStrengthApiSync(unitStrengthIR,key);
        }
    }
    else{
        Timber.d("LINK %s","US ----###########################################################################"+taskId);
        updateUnitStrengthSyncCount(0);
        Timber.d("LINK %s","US-----$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+taskId);
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthSyncing -->nothing to sync ");
    }
}

    private void barrackStrengthApiSync(final UnitStrengthIR unitStrengthIR, final Integer key) {


        sisClient.getApi().syncUnitStrength(unitStrengthIR, new Callback<CommonResponse>() {
            @Override
            public void success(CommonResponse unitResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"barrackStrengthApiSync response  %s", unitResponse);
                switch (unitResponse.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        updateIsSyncedBarrackStrength(key); // updating isSynced =1 to UNIT_AUTHORIZED_STRENGHT_TABLE
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("barrackStrengthApiSync", error);


            }
        });

    }

    private void updateIsSyncedBarrackStrength(Integer id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                " SET " + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            updateSyncCountAndStatus();
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            if(null!=sqlite && sqlite.isOpen()){
                sqlite.close();
            }

        }
    }

    /**
     *
     *
     * @return
     */
    public HashMap<Integer, UnitStrengthIR> getUnitStrengthDetails() {

        SQLiteDatabase sqlite = null;
        HashMap<Integer, UnitStrengthIR> unitStrengthIRHashMap = new HashMap<>();

        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                " where " +
                TableNameAndColumnStatement.TASK_ID + " >  " + 0 + " and " +
                TableNameAndColumnStatement.IS_SYNCED + " = 0 and "+
                TableNameAndColumnStatement.TASK_ID + " = "+ taskId +" and "+
                TableNameAndColumnStatement.UNIT_ID +" is not null and "+
                TableNameAndColumnStatement.RANK_ID +" is not null ";
        Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        UnitStrengthIR unitStrengthIR = new UnitStrengthIR();
                        unitStrengthIR.setActualStrength(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ACTUAL)));

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID))!=0){
                            unitStrengthIR.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        }

                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID))!=0){
                            unitStrengthIR.setBarrackId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID)));

                        }
                        if(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID))!=0){
                            unitStrengthIR.setRankId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.RANK_ID)));
                        }

                        unitStrengthIR.setUnitTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        unitStrengthIR.setLeaveCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.LEAVE_COUNT)));
                        unitStrengthIR.setCheckedDateTime(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.CREATED_DATE_TIME)));
                        unitStrengthIRHashMap.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), unitStrengthIR);
                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
            exception.printStackTrace();
        } finally {
            sqlite.close();
            Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthIR object  %s", IOPSApplication.getGsonInstance().toJson(unitStrengthIRHashMap));
        }

        return unitStrengthIRHashMap;
    }

    private void unitStrengthApiSync(final UnitStrengthIR unitStrengthIR, final int key) {

        sisClient.getApi().syncUnitStrength(unitStrengthIR, new Callback<CommonResponse>() {
            @Override
            public void success(CommonResponse unitResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"UnitStrengthSyncing response  %s", unitResponse);
                switch (unitResponse.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        updateIsSyncedUnitStrengthTable(key); // updating isSynced =1 to UNIT_AUTHORIZED_STRENGHT_TABLE
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("UnitStrengthSyncing", error);

            }
        });
    }


    /**
     *
     * @param id
     */
    public void updateIsSyncedUnitStrengthTable(int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE +
                " SET " + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            updateSyncCountAndStatus();
        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }

    private void updateSyncCountAndStatus() {
        if(taskId != 0){
            int count = dependentSyncDb.getSyncCount(taskId,TableNameAndColumnStatement.TASK_ID,
                    TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED);
            if (count != 0) {
                count = count -1;
            }
            updateUnitStrengthSyncCount(count);
        }
    }

}
