package com.sisindia.ai.android.syncadpter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.UnitEquipmentIMO;
import com.sisindia.ai.android.network.response.AddUnitEquipmentResponse;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by Saruchi on 24-06-2016.
 */

public class UnitEquipmentSyncing extends CommonSyncDbOperation {
    private final int unitPostId;
    private Context mContext;
    private SISClient sisClient;


    public UnitEquipmentSyncing(Context mcontext,int unitPostId) {
        super(mcontext);
        this.mContext = mcontext;
        sisClient = new SISClient(mcontext);
        this.unitPostId = unitPostId;
        unitEquipmentSyncing(unitPostId);
    }

    private synchronized void unitEquipmentSyncing(final int unitPostId) {
        Timber.d(Constants.TAG_SYNCADAPTER+"unitEquipmentSyncing unitEquipmentSyncing   %s", "unitEquipmentSyncing");
        HashMap<Integer, UnitEquipmentIMO> unitEquipmentInputList = getUnitEquipmensDetails(unitPostId);
        if (unitEquipmentInputList != null && unitEquipmentInputList.size() != 0) {
            updateEquipmentSyncCount(unitEquipmentInputList.size());
            for (Integer key : unitEquipmentInputList.keySet()) {
                UnitEquipmentIMO unitEquipmentIMO = unitEquipmentInputList.get(key);
                Timber.d(Constants.TAG_SYNCADAPTER+"UnitEquipmentIMO unitEquipmentSyncing   %s", "unitEquipmentSyncing");
                unitEquipmentApiSync(unitEquipmentIMO,key);
            }

        }
        else{
            updateEquipmentSyncCount(0);
            Timber.d(Constants.TAG_SYNCADAPTER+"unitEquipmentSyncing -->nothing to sync ");
        }
    }






    /**
     * being used in the postcheck fragment for syncing the add post data to server
     *
     * @return
     */
    public HashMap<Integer, UnitEquipmentIMO> getUnitEquipmensDetails(final int unitPostId) {

        SQLiteDatabase sqlite = null;
        HashMap<Integer, UnitEquipmentIMO> unitEquipmentInputList = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " where " + TableNameAndColumnStatement.UNIT_POST_ID + "= " + unitPostId +" AND ("
                + TableNameAndColumnStatement.IS_NEW + " = 1 OR "
                + TableNameAndColumnStatement.IS_SYNCED + "= 0)";


        Timber.d(Constants.TAG_SYNCADAPTER+"PostDetails selectQuery  %s", selectQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getReadableDatabase();
            cursor = sqlite.rawQuery(selectQuery, null);

            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        UnitEquipmentIMO unitEquipment = new UnitEquipmentIMO();
                        unitEquipment.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_EQUIPMENT_ID)));
                        unitEquipment.setEquipmentId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_TYPE)));
                        unitEquipment.setIsCustomerProvided(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED)));
                        unitEquipment.setStatus(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_STATUS)));
                        unitEquipment.setEquipmentUniqueNo(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIQUE_NO)));
                        unitEquipment.setEquipmentManufacturer(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.MANUFACTURER)));
                        unitEquipment.setPostId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID)));
                        unitEquipment.setQuantity(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.EQUIPMENT_COUNT)));
                        unitEquipment.setUnitId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        unitEquipmentInputList.put(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ID)), unitEquipment);

                    } while (cursor.moveToNext());
                }

            }
        } catch (Exception exception) {
            exception.getCause();
            exception.printStackTrace();
        } finally {
            sqlite.close();
            Timber.d(Constants.TAG_SYNCADAPTER+"equipmentList object  %s", IOPSApplication.getGsonInstance().toJson(unitEquipmentInputList));

        }
        return unitEquipmentInputList;
    }

    private void unitEquipmentApiSync(final UnitEquipmentIMO unitEquipmentIMO, final int key) {

        sisClient.getApi().syncUnitEquipment(unitEquipmentIMO, new Callback<AddUnitEquipmentResponse>() {
            @Override
            public void success(AddUnitEquipmentResponse unitResponse, Response response) {
                Timber.d(Constants.TAG_SYNCADAPTER+"unitEquipmentSyncing response  %s", unitResponse);
                switch (unitResponse.getStatusCode()) {
                    case HttpURLConnection.HTTP_OK:
                        updateEquipIdUnitEquipmentTable(
                                unitResponse.getData().getUnitEquipmentId(), key); // updating the unit equipment id in equipment table
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Util.printRetorfitError("SyncingUnitEquipmentModel", error);

            }
        });
    }

    /**
     * @param equip_id
     * @param id
     */
    public void updateEquipIdUnitEquipmentTable(int equip_id, int id) {
        SQLiteDatabase sqlite = null;
        String updateQuery = "UPDATE " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE +
                " SET " + TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + "= '" + equip_id +
                "' ," + TableNameAndColumnStatement.IS_NEW + "= " + 0 +
                " ," + TableNameAndColumnStatement.IS_SYNCED + "= " + 1 + "" +
                " WHERE " + TableNameAndColumnStatement.ID + "= '" + id + "'";
        Timber.d(Constants.TAG_SYNCADAPTER+"updateQuery  %s", updateQuery);
        Cursor cursor = null;
        try {
            sqlite = IOPSApplication.getInstance().getSISAITrackingDBInstance().getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d(Constants.TAG_SYNCADAPTER+"cursor detail object  %s", cursor.getCount());
            updateSyncCountAndStatus();

        } catch (Exception exception) {
            exception.getCause();
        } finally {
            sqlite.close();
        }
    }



    public void updateEquipmentSyncCount(int count) {
        List<DependentSyncsStatus> dependentSyncsStatusList =  dependentSyncDb.getDependentTasksSyncStatus();
        if(dependentSyncsStatusList != null && dependentSyncsStatusList.size() != 0){
            for(DependentSyncsStatus dependentSyncsStatus: dependentSyncsStatusList){
                if(unitPostId == dependentSyncsStatus.unitPostId){
                    dependentSyncsStatus.isUnitEquipmentSynced = count;
                    updateEquipmentSyncCount(dependentSyncsStatus);
                }
            }
        }
    }

    private void updateEquipmentSyncCount(DependentSyncsStatus dependentSyncsStatus) {
        dependentSyncDb.updateDependentSyncDetails(dependentSyncsStatus.unitPostId,
                Constants.UNIT_POST_RELATED_SYNC,
                TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED,
                dependentSyncsStatus.isUnitEquipmentSynced,dependentSyncsStatus.taskTypeId
        );
    }

    private void updateSyncCountAndStatus() {
        if(unitPostId != 0) {
            int count = dependentSyncDb.getSyncCount(unitPostId,
                    TableNameAndColumnStatement.UNIT_POST_ID,
                    TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED);

            if (count > 0) {
                count = count - 1;
            }
            //Timber.i("NewSync %s"," Image Sync count update in -----"+taskId +"--------"+count);
            updateEquipmentSyncCount(count);
        }
    }
}

