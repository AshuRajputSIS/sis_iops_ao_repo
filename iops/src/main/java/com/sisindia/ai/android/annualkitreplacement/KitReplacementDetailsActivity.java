package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.rota.daycheck.SignatureActivity;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shushrut on 20-04-2016.
 */
public class KitReplacementDetailsActivity extends BaseActivity {
    RecyclerView mkitItemRecyclerview;
    ImageView madd_signature;
    @Bind(R.id.kit_details_tabLayout)
    TabLayout issuesTabLayout;

    @Bind(R.id.addgaurd_photo)
    ImageView mGuardPhoto;
    @Bind(R.id.kit_details_toolbar)
    Toolbar toolbar;
    RelativeLayout mParentLayout;
    RelativeLayout mchildlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.replace_kit);
        initViews();
    }

    private void initViews() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mParentLayout = (RelativeLayout) findViewById(R.id.layout_header);
        CustomFontTextview mClientName = (CustomFontTextview) mParentLayout.findViewById(R.id.clienName);
        mClientName.setText(getResources().getString(R.string.guard_name));
        CustomFontTextview mSelectClient = (CustomFontTextview) mParentLayout.findViewById(R.id.selectClient);
        mSelectClient.setText(getResources().getString(R.string.gaurd_singh));
        mchildlayout = (RelativeLayout) findViewById(R.id.layout_child);
        CustomFontTextview mChildClientName = (CustomFontTextview) mchildlayout.findViewById(R.id.clienName);
        mChildClientName.setText(getResources().getString(R.string.unit_name));
        CustomFontTextview mChildSelectClient = (CustomFontTextview) mchildlayout.findViewById(R.id.selectClient);
        mChildSelectClient.setText(getResources().getString(R.string.select_unit));
        getSupportActionBar().setTitle(R.string.replace_kit);
        ScrollView mscrollView = (ScrollView) findViewById(R.id.kit_replacement_scrollview);
        mscrollView.smoothScrollTo(0, 0);
        mkitItemRecyclerview = (RecyclerView) findViewById(R.id.recyclerview_replace_list);
        madd_signature = (ImageView) findViewById(R.id.add_signature);
        madd_signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSignatureActivity();
            }
        });
        mkitItemRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        String[] mKitItemsList = new String[]{"Belt", "Lanyard", "Nylon", "Shoulder", "Uniform", "Whistle"};
        mkitItemRecyclerview.setAdapter(new KitReplacementAdapter(this, mKitItemsList));
    }

    private void startSignatureActivity() {
Intent signatureActivity =new Intent(KitReplacementDetailsActivity.this,SignatureActivity.class);
        signatureActivity.putExtra("SignatureCapture",true);
        startActivity(signatureActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_kit_replace, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.kit_details_done:
                this.finish();
                break;
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.addgaurd_photo)
    public void startCapturePhoto() {
        Intent capturePictureActivity = new Intent(this, CapturePictureActivity.class);
        capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), getResources().getString(R.string.replace_kit));

        Util.setSendPhotoImageTo(Util.REPLACE_KIT);
        capturePictureActivity.putExtra(Constants.PIC_TAKEN_FROM_SITEPOST_CHECK_KEY, Constants.PIC_TAKEN_FROM_SITEPOST);
        startActivity(capturePictureActivity);
    }
}
