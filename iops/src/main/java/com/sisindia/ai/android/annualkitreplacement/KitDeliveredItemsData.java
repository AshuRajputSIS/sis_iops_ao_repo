package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

public class KitDeliveredItemsData {

@SerializedName("KitDistributionId")
private Integer kitDistributionId;

/**
* 
* @return
* The kitDistributionId
*/
public Integer getKitDistributionId() {
return kitDistributionId;
}

/**
* 
* @param kitDistributionId
* The KitDistributionId
*/
public void setKitDistributionId(Integer kitDistributionId) {
this.kitDistributionId = kitDistributionId;
}

}