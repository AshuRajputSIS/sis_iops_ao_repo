package com.sisindia.ai.android.database.tableSyncCountDTO;

/**
 * Created by compass on 5/14/2017.
 */

public class FileUploadCount {

    private int totalCount;
    private int syncCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getSyncCount() {
        return syncCount;
    }

    public void setSyncCount(int syncCount) {
        this.syncCount = syncCount;
    }
}
