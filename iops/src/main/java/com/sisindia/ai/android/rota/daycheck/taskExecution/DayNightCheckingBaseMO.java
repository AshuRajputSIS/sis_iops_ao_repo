package com.sisindia.ai.android.rota.daycheck.taskExecution;

import com.google.gson.annotations.SerializedName;

public class DayNightCheckingBaseMO {

    @SerializedName("DayNightChecking")
    private DayNightChecking dayNightChecking;

    /**
     * @return The dayNightChecking
     */
    public DayNightChecking getDayNightChecking() {
        return dayNightChecking;
    }

    /**
     * @param dayNightChecking The DayNightChecking
     */
    public void setDayNightChecking(DayNightChecking dayNightChecking) {
        this.dayNightChecking = dayNightChecking;
    }

    @Override
    public String toString() {
        return "DayNightCheckingBaseMO{" +
                "dayNightChecking=" + dayNightChecking +
                '}';
    }
}