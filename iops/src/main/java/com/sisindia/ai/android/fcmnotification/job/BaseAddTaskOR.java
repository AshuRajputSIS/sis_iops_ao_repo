package com.sisindia.ai.android.fcmnotification.job;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.network.request.AddRotaTaskInput;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecuirtementMO;

/**
 * Created by compass on 5/29/2017.
 */

class BaseAddTaskOR {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;

    @SerializedName("Data")
    @Expose
    private AddRotaTaskFromRocc data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public AddRotaTaskFromRocc getData() {
        return data;
    }

    public void setData(AddRotaTaskFromRocc data) {
        this.data = data;
    }
}
