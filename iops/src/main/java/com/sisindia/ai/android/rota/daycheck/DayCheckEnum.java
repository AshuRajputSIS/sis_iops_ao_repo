package com.sisindia.ai.android.rota.daycheck;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shushrut on 10-06-2016.
 */
public enum DayCheckEnum {
    Strength_Check(1),
    Guard_Check(2),
    Duty_Register_Check(3),
    Client_Register_Check(4),
    Security_Risk_Observations(5),
    Client_Handshake(6);
    //Equipment_Check(7);

    private static Map map = new HashMap<>();

    static {
        for (DayCheckEnum mDayCheckValue : DayCheckEnum.values()) {
            map.put(mDayCheckValue.value, mDayCheckValue);
        }
    }

    private int value;

    DayCheckEnum(int i) {
        value = i;
    }

    public static DayCheckEnum valueOf(int positionValue) {
        return (DayCheckEnum) map.get(positionValue);
    }

    public int getValue() {
        return value;
    }
}
