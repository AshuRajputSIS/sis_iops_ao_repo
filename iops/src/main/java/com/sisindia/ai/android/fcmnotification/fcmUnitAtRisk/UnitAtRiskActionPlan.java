package com.sisindia.ai.android.fcmnotification.fcmUnitAtRisk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 2/12/16.
 */

public class UnitAtRiskActionPlan implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("IsCompleted")
    @Expose
    private Boolean isCompleted;
    @SerializedName("CompletedDate")
    @Expose
    private String completedDate;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     *
     * @param remarks
     * The Remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return
     * The isCompleted
     */
    public Boolean getIsCompleted() {
        return isCompleted;
    }

    /**
     *
     * @param isCompleted
     * The IsCompleted
     */
    public void setIsCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
    }

    /**
     *
     * @return
     * The completedDate
     */
    public String getCompletedDate() {
        return completedDate;
    }

    /**
     *
     * @param completedDate
     * The CompletedDate
     */
    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

}
