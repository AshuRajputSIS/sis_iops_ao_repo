package com.sisindia.ai.android.rota;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;

import java.util.List;

/**
 * Created by shankar on 28/11/16.
 */

public class UnitDetailsBottomSheetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private List<MyUnitHomeMO> bottomSheetData;


    public UnitDetailsBottomSheetAdapter(Context context) {
        this.context = context;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setBottomSheetData(List<MyUnitHomeMO> bottomSheetData) {
        this.bottomSheetData = bottomSheetData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_row, parent, false);
        RecyclerView.ViewHolder recycleViewHolder = new BottomSheetViewHolder(itemView);
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MyUnitHomeMO myUnitHomeMO = bottomSheetData.get(position);
        BottomSheetViewHolder bottomSheetHolder = (BottomSheetViewHolder) holder;
//        bottomSheetHolder.clientName.setText(myUnitHomeMO.getUnitName());
        bottomSheetHolder.clientName.setText(myUnitHomeMO.getUnitName() + " (" + myUnitHomeMO.getUnitCode() + ")");
    }


    @Override
    public int getItemCount() {
        int count = 0;
        if (bottomSheetData != null) {
            count = bottomSheetData.size() == 0 ? 0 : bottomSheetData.size();
        }
        return count;
    }

    private Object getObject(int position) {
        return bottomSheetData.get(position);
    }

    public class BottomSheetViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView clientName;

        public BottomSheetViewHolder(View view) {
            super(view);
            clientName = (TextView) view.findViewById(R.id.bottom_sheet_row_label_txt);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }
}
