package com.sisindia.ai.android.myunits.models;

/**
 * Created by Durga Prasad on 02-04-2016.
 */
public class UnitTaskHistoryHeader {

    String thisMonthCount, lastMonthCount;

    public String getLastMonthCount() {
        return lastMonthCount;
    }

    public void setLastMonthCount(String lastMonthCount) {
        this.lastMonthCount = lastMonthCount;
    }

    public String getThisMonthCount() {
        return thisMonthCount;
    }

    public void setThisMonthCount(String thisMonthCount) {
        this.thisMonthCount = thisMonthCount;
    }
}
