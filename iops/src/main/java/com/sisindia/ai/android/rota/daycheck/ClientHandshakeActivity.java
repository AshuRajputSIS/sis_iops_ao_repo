package com.sisindia.ai.android.rota.daycheck;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.myunits.ContactMO;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.rota.daycheck.taskExecution.ClientHandShakeMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import static com.sisindia.ai.android.utils.Util.mOnStepperUpdateListener;

/**
 * Created by Saruchi on 16-06-2016.
 */

public class ClientHandshakeActivity extends DayCheckBaseActivity implements RadioGroup.OnCheckedChangeListener,DialogClickListener {

    @Bind(R.id.reasonLayout)
    RelativeLayout reasonLayout;
    @Bind(R.id.parent_layout_handshake)
    RelativeLayout parentLayout;
    @Bind(R.id.child_layout_handshake)
    RelativeLayout childLayout;
    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;
    @Bind(R.id.radioButtonNO)
    RadioButton radioButtonNO;
    @Bind(R.id.radioButtonYes)
    RadioButton radioButtonYes;
    @Bind(R.id.radioGroupReason)
    RadioGroup radioGroupReason;
    @Bind(R.id.verySatisfiedRB)
    RadioButton rbVerySatisfied;
    @Bind(R.id.satisfiedRB)
    RadioButton rbSatisfied;
    @Bind(R.id.noSatisfiedRB)
    RadioButton rbNotSatisfied;
    @Bind(R.id.spinnerContact)
    Spinner contactSpinner;
    @Bind(R.id.reasonSpinner)
    Spinner reasonSpinner;
    @Bind(R.id.clientNameLabel)
    TextView nameTv;
    @Bind(R.id.designation_lable)
    TextView designationTV;
    @Bind(R.id.contactNoLable)
    TextView contactTV;
    @Bind(R.id.dayCheckHeader)
    View mStepperViewParent;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.addComplaintButton)
    CustomFontButton maddComplaintButton;
    private ContactSpinnerAdapter adapter;
    private ArrayAdapter<CharSequence> reasonAdapter;
    private ClientHandShakeMO clientHandshakeMO;
    private ArrayList<ContactMO> contactArrayList = new ArrayList<ContactMO>();
    private ArrayList<View> viewList;
    private boolean isMeetClient = false;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_handshake_activity);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.dayCheckingTitle));
        setUpView();
        getTaskExecutionResult();
        if (rotaTaskModel != null) {
            setHeaderUnitName(rotaTaskModel.getUnitName());
            getStepperViews(mStepperViewParent);
        }
    }


    private void setUpView() {

        appPreferences = new AppPreferences(this);
        clientHandshakeMO = new ClientHandShakeMO();
        clientHandshakeMO.setMeetClient(radioButtonNO.getText().toString()); // by default setting to yes
        clientHandshakeMO.setClientSatisfied(rbSatisfied.getText().toString());// by default setting to just satisfied
        rotaTaskModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if(rotaTaskModel == null){
            rotaTaskModel = appPreferences.getRotaTaskJsonData();
        }
        contactArrayList = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().getUnitContact(rotaTaskModel.getUnitId());
        radioGroup.setOnCheckedChangeListener(this);
        radioGroupReason.setOnCheckedChangeListener(this);
        if (null != contactArrayList) {
            adapter = new ContactSpinnerAdapter(ClientHandshakeActivity.this,
                    android.R.layout.simple_spinner_item,
                    contactArrayList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            contactSpinner.setAdapter(adapter);
        }
        ArrayList<LookupModel> lookupModelArrayList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.ReasonforDissatisfaction), null);
        String[] reasons = new String[lookupModelArrayList.size() + 1];
        reasons[0] = getResources().getString(R.string.SELECT_REASON);
        for (int index = 0; index < lookupModelArrayList.size(); index++) {
            reasons[index + 1] = lookupModelArrayList.get(index).getName();
        }

        final ArrayAdapter<String> reasonAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, reasons);


        /*final ArrayAdapter<CharSequence> reasonAdapter = ArrayAdapter.createFromResource(this,
                R.array.reasons_array, android.R.layout.simple_spinner_item);*/
        reasonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reasonSpinner.setAdapter(reasonAdapter);
        contactSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                // Here you can do the action you want to...
                if (position != 0) {

                    ContactMO contactMO = adapter.getItem(position);
                    nameTv.setText(contactMO.getName());
                    designationTV.setText(contactMO.getDesignation());
                    contactTV.setText(contactMO.getContactNo());
                    clientHandshakeMO.setClientName(contactMO.getName());
                    clientHandshakeMO.setDesignation(contactMO.getDesignation());
                    clientHandshakeMO.setContactNo(contactMO.getContactNo());
                } else {
                    nameTv.setText("");
                    designationTV.setText("");
                    contactTV.setText("");
                    clientHandshakeMO.setClientName("");
                    clientHandshakeMO.setDesignation("");
                    clientHandshakeMO.setContactNo("");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                // Here you can do the action you want to...
                if (position != 0) {
                    String reasonStr = reasonAdapter.getItem(position);
                    clientHandshakeMO.setReason(reasonStr);
                } else {
                    clientHandshakeMO.setReason("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds post_menu_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_menu_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.done_menu) {
            fieldValidation();
            return true;
        }
        if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
        }
        return super.onOptionsItemSelected(item);
    }

    private void fieldValidation() {
        if (null != clientHandshakeMO) {
            if (isMeetClient) {
                if ((null == clientHandshakeMO.getMeetClient()) || (0 == clientHandshakeMO.getMeetClient().length()) ||
                        (null == clientHandshakeMO.getClientName()) || (0 == clientHandshakeMO.getClientName().length()) ||
                        (null == clientHandshakeMO.getClientSatisfied()) || (0 == clientHandshakeMO.getClientSatisfied().length())
                        ) {
                    // show the validation dialog box
                    showFragmentDialog(1, getResources().getString(R.string.client_handshake_validation));
                    Timber.d("ClientHandshakeActivity %s", clientHandshakeMO);


                } else if (!clientHandshakeMO.getClientSatisfied().trim().equalsIgnoreCase(getResources().getString(R.string.very_satisfied)) &&
                        ((null == clientHandshakeMO.getReason()) || (0 == clientHandshakeMO.getReason().length()))) {
                    Util.customSnackBar(childLayout, this, getResources().getString(R.string.reason_validation));
                } else {
                    Timber.d("ClientHandshakeActivity %s", clientHandshakeMO);
                    dayNightChecking.setClientHandShakeMO(clientHandshakeMO);
                    dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
                    setTaskExecutionResult();
                    OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();
                    mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(6).name(),DayCheckEnum.Client_Handshake.getValue());
                    finish();
                }
            } else {
                clientHandshakeMO.setClientName(null);
                clientHandshakeMO.setDesignation(null);
                clientHandshakeMO.setContactNo(null);
                clientHandshakeMO.setClientSatisfied(null);
                clientHandshakeMO.setReason(null);
                Timber.d("ClientHandshakeActivity %s", clientHandshakeMO);
                dayNightChecking.setClientHandShakeMO(clientHandshakeMO);
                dayNightCheckingBaseMO.setDayNightChecking(dayNightChecking);
                setTaskExecutionResult();
                OnStepperUpdateListener mOnStepperUpdateListener = Util.getmHandlerInstance();
                mOnStepperUpdateListener.updateStepper(DayCheckEnum.valueOf(6).name(),DayCheckEnum.Client_Handshake.getValue());
                finish();
                Timber.d("ClientHandshakeActivity %s", "set other fields as null for clientHandshake");
            }
        } else {
            Timber.d("ClientHandshakeActivity %s", "NULL");
        }


    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.radioGroup:
                if (checkedId == R.id.radioButtonNO) {
                    clientHandshakeMO.setMeetClient(radioButtonNO.getText().toString());
                    isMeetClient = false;
                    childLayout.setVisibility(View.GONE);

                } else if (checkedId == R.id.radioButtonYes) {
                    clientHandshakeMO.setMeetClient(radioButtonYes.getText().toString());
                    isMeetClient = true;
                    childLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.radioGroupReason:
                if (checkedId == R.id.verySatisfiedRB) {
                    clientHandshakeMO.setClientSatisfied(rbVerySatisfied.getText().toString());
                    reasonLayout.setVisibility(View.INVISIBLE);

                } else if (checkedId == R.id.satisfiedRB) {
                    clientHandshakeMO.setClientSatisfied(rbSatisfied.getText().toString());
                    reasonLayout.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.noSatisfiedRB) {
                    clientHandshakeMO.setClientSatisfied(rbNotSatisfied.getText().toString());
                    reasonLayout.setVisibility(View.VISIBLE);

                }

                break;
        }
    }

    @OnClick(R.id.addComplaintButton)
    public void mAddComplaint() {
        Intent addComplaint = new Intent(this, AddComplaintActivity.class);
        Bundle complaintBundle = new Bundle();
        complaintBundle.putInt(TableNameAndColumnStatement.UNIT_ID, rotaTaskModel.getUnitId());
        complaintBundle.putString(TableNameAndColumnStatement.UNIT_NAME, rotaTaskModel.getUnitName());
        complaintBundle.putBoolean(TableNameAndColumnStatement.CHECKING_TYPE_DAY_CHECKING,true);
        addComplaint.putExtras(complaintBundle);
        startActivity(addComplaint);
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        if(clickType == 0){

            mOnStepperUpdateListener.downGradeStepper(DayCheckEnum.valueOf(6).name(),DayCheckEnum.Client_Handshake.getValue());
            finish();
        }
        else{
            finish();
        }
    }
}
