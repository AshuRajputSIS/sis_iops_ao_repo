package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class KitDistributionEmployeeModelsMO implements Serializable {

    @SerializedName("Id")
    private int Id;
    @SerializedName("KitDistributionId")
    private int KitDistributionId;
    @SerializedName("OrderNo")
    private String OrderNo;
    @SerializedName("RecipientId")
    private int RecipientId;
    @SerializedName("KitIssueTypeId")
    private int KitIssueTypeId;
    @SerializedName("IsIssued")
    private boolean IsIssued;
    @SerializedName("NonIssueReasonId")
    private int NonIssueReasonId;
    @SerializedName("IssuedDate")
    private String IssuedDate;
    @SerializedName("IsUnPaid")
    private boolean IsUnPaid;
    @SerializedName("KitItemId")
    private int KitItemId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getKitDistributionId() {
        return KitDistributionId;
    }

    public void setKitDistributionId(int kitDistributionId) {
        KitDistributionId = kitDistributionId;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public int getRecipientId() {
        return RecipientId;
    }

    public void setRecipientId(int recipientId) {
        RecipientId = recipientId;
    }

    public int getKitIssueTypeId() {
        return KitIssueTypeId;
    }

    public void setKitIssueTypeId(int kitIssueTypeId) {
        KitIssueTypeId = kitIssueTypeId;
    }

    public boolean issued() {
        return IsIssued;
    }

    public void setIssued(boolean issued) {
        IsIssued = issued;
    }

    public int getNonIssueReasonId() {
        return NonIssueReasonId;
    }

    public void setNonIssueReasonId(int nonIssueReasonId) {
        NonIssueReasonId = nonIssueReasonId;
    }

    public boolean isUnPaid() {
        return IsUnPaid;
    }

    public void setUnPaid(boolean unPaid) {
        IsUnPaid = unPaid;
    }

    public int getKitItemId() {
        return KitItemId;
    }

    public void setKitItemId(int kitItemId) {
        KitItemId = kitItemId;
    }

    public String getIssuedDate() {
        return IssuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        IssuedDate = issuedDate;
    }


}
