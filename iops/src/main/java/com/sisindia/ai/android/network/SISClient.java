package com.sisindia.ai.android.network;


import android.content.Context;
import android.text.TextUtils;

import com.sisindia.ai.android.BuildConfig;
import com.sisindia.ai.android.annualkitreplacement.BaseAkrSync;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsIR;
import com.sisindia.ai.android.annualkitreplacement.KitDeliveredItemsOR;
import com.sisindia.ai.android.annualkitreplacement.KitItemApiResponse;
import com.sisindia.ai.android.annualkitreplacement.KitItemRequestMO;
import com.sisindia.ai.android.annualkitreplacement.KitReplaceCountOR;
import com.sisindia.ai.android.appuser.AppUserLocationMO;
import com.sisindia.ai.android.appuser.AppuserResponseMO;
import com.sisindia.ai.android.commons.network.ClientCoordinationIssueMatrixBaseOR;
import com.sisindia.ai.android.commons.network.GPSEnableDisableMO;
import com.sisindia.ai.android.commons.network.MasterActionPlanBaseOR;
import com.sisindia.ai.android.commons.network.SecurityRiskOptionsBaseOR;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorBaseOR;
import com.sisindia.ai.android.debugingtool.models.QueryExecutorMO;
import com.sisindia.ai.android.fcmnotification.FCMTokenRegistration;
import com.sisindia.ai.android.fcmnotification.FCMTokenUpdateMO;
import com.sisindia.ai.android.fcmnotification.GuardForBranchBase;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationIR;
import com.sisindia.ai.android.fcmnotification.notificationReceipt.NotificationOR;
import com.sisindia.ai.android.issues.BaseIssuesSync;
import com.sisindia.ai.android.issues.IssueCountMO;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.issues.models.ImprovementPlanOR;
import com.sisindia.ai.android.issues.models.OpenIssuesBaseMO;
import com.sisindia.ai.android.loadconfiguration.LoadConfigBaseMO;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackStrengthIR;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackSyncMO;
import com.sisindia.ai.android.mybarrack.modelObjects.MyBarrackBaseMo;
import com.sisindia.ai.android.myperformance.MyPerformanceBase;
import com.sisindia.ai.android.myperformance.TimeLineBaseMO;
import com.sisindia.ai.android.myunits.models.FilterOR;
import com.sisindia.ai.android.myunits.models.SingleUnitBaseOR;
import com.sisindia.ai.android.myunits.models.UnitBarrackBaseMO;
import com.sisindia.ai.android.myunits.models.UnitContactIR;
import com.sisindia.ai.android.myunits.models.UnitHistoryBaseOR;
import com.sisindia.ai.android.myunits.models.UnitStrengthBaseOR;
import com.sisindia.ai.android.network.request.AddRotaTaskInput;
import com.sisindia.ai.android.network.request.GpsBatteryInputRequestBaseMO;
import com.sisindia.ai.android.network.request.IssueIMO;
import com.sisindia.ai.android.network.request.MetaDataIMO;
import com.sisindia.ai.android.network.request.OtpInputRequestMO;
import com.sisindia.ai.android.network.request.RotaTaskInputRequestMO;
import com.sisindia.ai.android.network.request.UnitEquipmentIMO;
import com.sisindia.ai.android.network.request.UnitPostIR;
import com.sisindia.ai.android.network.request.UnitStrengthIR;
import com.sisindia.ai.android.network.response.AccessTokenResponse;
import com.sisindia.ai.android.network.response.AddMetadataResponse;
import com.sisindia.ai.android.network.response.AddUnitEquipmentResponse;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.network.response.AppInfo;
import com.sisindia.ai.android.network.response.BarrackSyncOR;
import com.sisindia.ai.android.network.response.BaseRotaSync;
import com.sisindia.ai.android.network.response.BaseUnitSync;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.network.response.FileUploadResponse;
import com.sisindia.ai.android.network.response.IssuesOutputMO;
import com.sisindia.ai.android.network.response.NfcCheckResponse;
import com.sisindia.ai.android.network.response.TaskOutputMO;
import com.sisindia.ai.android.network.response.UnitAddPostResponse;
import com.sisindia.ai.android.network.response.UnitContactOR;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.AddRecruitmentIR;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.BaseRecruitmentMO;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.RecruitmentOR;
import com.sisindia.ai.android.rota.DutyAttendanceIR;
import com.sisindia.ai.android.rota.DutyAttendanceOR;
import com.sisindia.ai.android.rota.models.GuardFineRewardMO;
import com.sisindia.ai.android.rota.models.SecurityRiskIR;
import com.sisindia.ai.android.rota.models.SecurityRiskOR;
import com.sisindia.ai.android.rota.rotaCompliance.RotaComplianceOR;
import com.sisindia.ai.android.rota.rotaCompliance.RotaCompliancePercentageOR;
import com.sisindia.ai.android.syncadpter.UnitRaisingCancelResponse;
import com.sisindia.ai.android.syncadpter.UnitRaisingDeferredResponse;
import com.sisindia.ai.android.syncadpter.UnitRaisingResponse;
import com.sisindia.ai.android.syncadpter.UnitRaisingStrengthResponse;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitAtRiskBaseMO;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitAtRiskIR;
import com.sisindia.ai.android.unitatriskpoa.modelobjects.UnitRiskSyncOR;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingCancelOR;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingDeferOR;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingOR;
import com.sisindia.ai.android.unitraising.modelObjects.UnitRaisingStrengthOR;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by shankar on 2/3/16.
 */
public class SISClient {

    private static final String TAG = "SISClient";
    private static RestAdapter restAdapter;
    private Cache cache;
    private AppPreferences mAppPreferences;
    private ApiService mApiService;
    private Context context;
    private static final int READ_TIMEOUT = 2; // 2 min
    private static final int WRITE_TIMEOUT = 2; // 2 min
    private static final int CONNECTION_TIMEOUT = 2;  // 2 min
    private String apiMethod;
    private String apiName = "";
    private int specificApiTimeOut;

    public SISClient(Context context) {
        mAppPreferences = new AppPreferences(context);
        this.context = context;
        setUpCache();
        setUpHttpParameters();
        mApiService = restAdapter.create(ApiService.class);

    }

    public SISClient(Context context, String apiName, int apiTimeOut) {
        mAppPreferences = new AppPreferences(context);
        this.context = context;
        this.apiName = apiName;
        this.specificApiTimeOut = apiTimeOut;
        setUpCache();
        setUpHttpParameters();
        mApiService = restAdapter.create(ApiService.class);
    }

    public SISClient(Context context, String apiName) {
        mAppPreferences = new AppPreferences(context);
        this.context = context;
        this.apiName = apiName;
        setUpCache();
        setUpHttpParameters();
        mApiService = restAdapter.create(ApiService.class);
    }


    private OkHttpClient setGuardApiTimeOut() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new ApiOfflineInterceptor());
        okHttpClient.networkInterceptors().add(new ApiInterceptor());
        okHttpClient.setReadTimeout(specificApiTimeOut, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(specificApiTimeOut, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(specificApiTimeOut, TimeUnit.SECONDS);
        return okHttpClient;
    }

    private OkHttpClient setDefaultApiTimeOut() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new ApiOfflineInterceptor());
        okHttpClient.networkInterceptors().add(new ApiInterceptor());
        okHttpClient.setReadTimeout(READ_TIMEOUT, TimeUnit.MINUTES);
        okHttpClient.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.MINUTES);
        okHttpClient.setConnectTimeout(CONNECTION_TIMEOUT, TimeUnit.MINUTES);
        return okHttpClient;

    }

    private void setUpHttpParameters() {

        String endPointURL = BuildConfig.END_POINT;

        OkHttpClient okHttpClient = null;
        if (TextUtils.isEmpty(apiName)) {
            okHttpClient = setDefaultApiTimeOut();
        } else {
            if (apiName.equalsIgnoreCase("getGuardRewardFine")) {
                okHttpClient = setGuardApiTimeOut();
            }
            /*else if (apiName.equals("TEMP")) {
                okHttpClient = setDefaultApiTimeOut();
                endPointURL = "http://111.93.216.167:8080/Queryexe/api/QueryOps";
            }*/
        }
        if (cache != null) {
            okHttpClient.setCache(cache);
        }

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPointURL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Accept", "application/json");
                        if (mAppPreferences.getloginstatus()) {
                            request.addHeader("Authorization", mAppPreferences.getToken_type() + " " + mAppPreferences.getAccessToken());
                        }
                    }
                })
                .build();
    }

    private void setUpCache() {
        try {
            File httpCacheDirectory = new File(context.getCacheDir(), "responses");
            long cacheSize = 10 * 1024 * 1024;
            cache = new Cache(httpCacheDirectory, cacheSize);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public ApiService getApi() {
        return mApiService;
    }

    public interface ApiService {
        @Multipart
        @POST("/attachment/fileById")
        void fileUploads(@PartMap Map<String, TypedFile> files,
                         @Query("attachmentId") int attachmentId,
                         Callback<FileUploadResponse> response);

        @POST("/GPS")
        void postLocationData(@Body GpsBatteryInputRequestBaseMO locationData, Callback<CommonResponse> response);

        @POST("/login/sendOtp")
        void sendOTP(@Body OtpInputRequestMO otpInputRequestMO, Callback<CommonResponse> response);

        @FormUrlEncoded
        @POST("/otpvalidation")
        void login(@Field("Grant_type") String granttype,
                   @Field("username") String phoneno,
                   @Field("password") String otp,
                   Callback<AccessTokenResponse> Response);

        @GET("/Employee/SetDutyStatus")
        void dutyOnOff(@Query("status") boolean isOn, Callback<CommonResponse> response);

        @GET("/AOTimeline/add")
        void generateTimeLine(@Query("RequestDate") String requestDate, @Query("EmployeeId") int employeeId, Callback<CommonResponse> response);

        @GET("/AOTimeline/Get")
        void getGeneratedTimeLine(@Query("RequestDate") String requestDate, @Query("EmployeeId") int employeeId, Callback<TimeLineBaseMO> response);

        @GET("/Configuration/LoadConfiguration")
        void loadConfiguration(Callback<LoadConfigBaseMO> response);

        @POST("/unit/post")
        void syncUnitPost(@Body UnitPostIR postData, Callback<UnitAddPostResponse> unitAddPostResponseCallback);

        @POST("/unit/starttask")
        void startTask(@Body String taskId, Callback<CommonResponse> response);

        @POST("/unit/edittask")
        void editTask(@Body RotaTaskInputRequestMO editTaskINputRequestMo, Callback<CommonResponse> response);

        @POST("/Configuration/UpdateAppVersion")
        void updateAppVersion(@Body String appVersion, Callback<CommonResponse> response);

        @GET("/rota/GetRotas")
        void getRota(Callback<BaseRotaSync> response);

        @GET("/unit/getunit")
        void getUnit(Callback<BaseUnitSync> response);

        @POST("/unit/task")
        void syncUnitTask(@Body RotaTaskInputRequestMO taskInputIMO, Callback<TaskOutputMO> taskResponseCallback);

        @POST("/unit/task")
        void syncAddUnitTask(@Body AddRotaTaskInput taskInputIMO, Callback<TaskOutputMO> taskResponseCallback);

        // same API for Grievance Table syncing and complaint Table syncing
        @POST("/issue/compliant")
        void syncIssueData(@Body IssueIMO issueIMO, Callback<IssuesOutputMO> issuesResponseCallback);

        @POST("/unitequipment/unitequipment")
        void syncUnitEquipment(@Body UnitEquipmentIMO inputUnitEquipment, Callback<AddUnitEquipmentResponse> unitEqupmentPostResponseCallback);

        @POST("/attachment/metadata")
        void syncAttachmentMetadata(@Body MetaDataIMO metadataModel, Callback<AddMetadataResponse> metadataResponseCallback);

        @POST("/unit/strength")
        void syncUnitStrength(@Body UnitStrengthIR unitStrengthIR, Callback<CommonResponse> commonResponseCallback);

        @POST("/unit/contact")
        void syncUnitContact(@Body UnitContactIR unitContactIR, Callback<ApiResponse<UnitContactOR>> callback);

        @GET("/AppVersion/info")
        void getLatestAppVersionInfo(Callback<ApiResponse<AppInfo>> callback);

        @GET("/unit/history")
        void getSingleUnitDetails(@Query("unitid") int unitId, Callback<SingleUnitBaseOR> callback);

        @GET("/unit/taskdetails")
        void getUnitTaskHistoryDetails(@Query("unitid") int unitId, @Query("taskTypeId") int taskTypeId, Callback<UnitHistoryBaseOR> callback);

        @GET("/unit/getstrength")
        void getUnitStrengthDetails(@Query("unitId") int unitId, Callback<UnitStrengthBaseOR> callback);

        @POST("/SecurityRiskObservation/add")
        void syncUnitContact(@Body SecurityRiskIR mSecurityRiskIR, Callback<ApiResponse<SecurityRiskOR>> callback);


        @GET("/employee/getperformancescores")
        void getMyPerformanceInfo(@Query("isMonth") boolean isMonth,
                                  @Query("date") String date,
                                  Callback<MyPerformanceBase> callback);

        @GET("/unit/risk")
        void UnitAtRiskPOA(@Query("month") int month,
                           @Query("year") int year,
                           Callback<UnitAtRiskBaseMO> callback);

        @POST("/Unit/risk/addActionPlan")
        void unitAtRiskPOASync(@Body UnitAtRiskIR unitAtRiskIR, Callback<UnitRiskSyncOR> callback);

        @GET("/unit/barrack/strength")
        void getBarrackLastInspectStrength(@Query("barrackId") int barrackid, Callback<BarrackStrengthIR> callback);

        @GET("/actionplan/count")
        void getActionPlanCount(Callback<ApiResponse<IssueCountMO>> callback);

        @POST("/actionplan/add")
        void syncImprovementPlan(@Body ImprovementPlanIR improvementPlanIR, Callback<ApiResponse<ImprovementPlanOR>> callback);

        @GET("/issue/count")
        void getOpenIssuesCount(Callback<OpenIssuesBaseMO> callback);

        @POST("/barrack/update")
        void barrackGeoLocationSync(@Body BarrackSyncMO barrackSyncMO, Callback<ApiResponse<BarrackSyncOR>> callback);

        @GET("/recruitment/getdata")
        void getRecurimentData(Callback<ApiResponse<BaseRecruitmentMO>> callback);

        @POST("/recruitment/hire")
        void syncRecuriment(@Body AddRecruitmentIR addRecruitmentIR, Callback<ApiResponse<RecruitmentOR>> callback);

        @POST("/kitrequest/add")
        void syncKitItemRequest(@Body KitItemRequestMO kitItemRequestMO, Callback<KitItemApiResponse> callback);

        @POST("/employee/update")
        void syncAILocation(@Body AppUserLocationMO mAppUserLocationMO, Callback<AppuserResponseMO> callback);

        @GET("/employee/rewards")
        void getGuardRewardFine(@Query("employeeNo") String employeeNo, Callback<GuardFineRewardMO> callback);

        @GET("/kitreplace/count")
        void kitReplaceCount(Callback<KitReplaceCountOR> callback);

        @POST("/kitreplace/update")
        void syncKitDeliveredItemsUpdate(@Body KitDeliveredItemsIR kitDeliveredItemsIR, Callback<KitDeliveredItemsOR> callback);

        @GET("/unit/filter")
        void getFilterUnits(@Query("filterId") int filterId, Callback<ApiResponse<FilterOR>> callback);

        @POST("/employee/updatedevice")
        void updateFCMToken(@Body FCMTokenRegistration mAppUserLocationMO, Callback<FCMTokenUpdateMO> callback);

        @POST("/notification/receipt")
        void notificationSync(@Body NotificationIR notificationIR, Callback<ApiResponse<NotificationOR>> callback);

        @GET("/unit/nfcavailable")
        void checkNfcStatus(@QueryMap Map<String, Object> map, Callback<NfcCheckResponse> callback);

        //?deviceNo=045B84B2AEXXX
//        @FormUrlEncoded
        @GET("/barrack/nfcavailable")
        void checkBarrackNfcStatus(@Query("deviceNo") String deviceNo, Callback<NfcCheckResponse> callback);
//        void checkBarrackNfcStatus(@Query("deviceNo") String deviceNo, @Body String emptyString, Callback<NfcCheckResponse> callback);

        @POST("/employee/duty/update")
        void syncDutyAttendance(@Body DutyAttendanceIR dutyAttendanceIR, Callback<DutyAttendanceOR> callback);

        @GET("/rota/rotaCompliancePercentage")
        void getRotaCompliancePercentage(@Query("rotaDate") String date, Callback<RotaCompliancePercentageOR> callback);


        @GET("/Rota/RotaCompliance")
        void getRotaCompliance(@Query("rotaDate") String date, Callback<RotaComplianceOR> callback);

        @GET("/unitbarrack/getunitbarrack")
        void getRotaCompliance(@Query("id") int id, Callback<RotaComplianceOR> callback);

        @GET("/unitbarrack/getallunitbarracks")
        void getUnitBarracks(Callback<UnitBarrackBaseMO> callback);

        @GET("/issue/getissuesandactionplans")
        void getIssues(Callback<BaseIssuesSync> response);

        @GET("/akr/getallakr")
        void getAllAkr(Callback<BaseAkrSync> response);

        @GET("/configuration/getmasteractionplans")
        void getMasterActionPlan(Callback<MasterActionPlanBaseOR> response);

        @GET("/configuration/getsecurityriskmappings")
        void getSecurityActionPlan(Callback<SecurityRiskOptionsBaseOR> response);

        @GET("/configuration/getclientcoordinationissuematrix")
        void getClientCoordinationIssueMatrix(Callback<ClientCoordinationIssueMatrixBaseOR> response);

        @GET("/configuration/getbiquestionlist")
        void getBarrackQuestionList(Callback<BarrackQuestionBaseOR> response);

        @GET("/configuration/getbiquestionoptionmapping")
        void getBarrackQuestionOptionList(Callback<BarrackQuestionsAndOptionsBaseOR> response);

        @GET("/configuration/ccrquestions")
        void getCCRQuestions(Callback<CCRQuestionRequestMO> response);

        @GET("/unit/getunittype")
        void getUnitTypeList(Callback<UnitTypeRequestMO> response);

        @GET("/barrack/getlistbarracks")
        void getAllBarracks(Callback<MyBarrackBaseMo> response);

        @POST("/unitraising/save")
        void saveUnitRaising(@Body UnitRaisingOR unitRaisingOR, Callback<UnitRaisingResponse> response);

        @POST("/unitraising/defer")
        void deferredUnitRaising(@Body UnitRaisingDeferOR unitRaisingDeferOR, Callback<UnitRaisingDeferredResponse> response);

        @POST("/unitraising/cancel")
        void cancelUnitRaising(@Body UnitRaisingCancelOR unitRaisingCancelOR, Callback<UnitRaisingCancelResponse> response);

        @POST("/unitraising/savestrength")
        void saveUnitRaisingStrength(@Body UnitRaisingStrengthOR unitRaisingStrengthOR, Callback<UnitRaisingStrengthResponse> response);

        @GET("/employee/getnonexecqueries")
        void getQueryExecutorDetails(@Query("employeeId") int Id, Callback<QueryExecutorBaseOR> callback);

        @POST("/configuration/postquery")
        void postQueryExecutorDetails(@Body QueryExecutorMO queryExecutorMO, Callback<QueryExecutorBaseOR> response);

        //@Ashu: adding client for getting Guards Information
        @GET("/unit/getbranchguards")
        void getBranchGuardsInformation(@Query("branchId") int branchId, Callback<GuardForBranchBase> callback);

        //@ASHU: adding client for posting GPS status (On/Off) by user
        @POST("/GpsStatus/add")
        void postGpsStatus(@Body GPSEnableDisableMO gpsEnableDisableMO, Callback<CommonResponse> response);

    }
}

