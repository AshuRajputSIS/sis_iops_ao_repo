package com.sisindia.ai.android.myperformance;

import android.graphics.drawable.Drawable;

/**
 * Created by shaikatif on 5/7/16.
 */
public class ReportViewModel {

    private String reportHeading;
    private String reportValue;
    private int imageResource;
    private int progressMaxValue;
    private int progressValue;
    private Drawable progressDrawable;

    public boolean isRotaComplaince() {
        return isRotaCompliance;
    }

    public void setRotaComplaince(boolean rotaComplaince) {
        isRotaCompliance = rotaComplaince;
    }

    private boolean isRotaCompliance;
    private int approvedCount;
    private int nonApprovedCount;

    public String getReportHeading() {
        return reportHeading;
    }

    public void setReportHeading(String reportHeading) {
        this.reportHeading = reportHeading;
    }

    public String getReportValue() {
        return reportValue;
    }

    public void setReportValue(String reportValue) {
        this.reportValue = reportValue;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public int getProgressMaxValue() {
        return progressMaxValue;
    }

    public void setProgressMaxValue(int progressMaxValue) {
        this.progressMaxValue = progressMaxValue;
    }

    public int getProgressValue() {
        return progressValue;
    }

    public void setProgressValue(int progressValue) {
        this.progressValue = progressValue;
    }

    public Drawable getProgressDrawable() {
        return progressDrawable;
    }

    public void setProgressDrawable(Drawable progressDrawable) {
        this.progressDrawable = progressDrawable;
    }

    public int getApprovedCount() {
        return approvedCount;
    }

    public void setApprovedCount(int approvedCount) {
        this.approvedCount = approvedCount;
    }

    public int getNonApprovedCount() {
        return nonApprovedCount;
    }

    public void setNonApprovedCount(int nonApprovedCount) {
        this.nonApprovedCount = nonApprovedCount;
    }
}
