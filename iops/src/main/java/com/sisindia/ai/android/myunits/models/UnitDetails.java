package com.sisindia.ai.android.myunits.models;


import java.util.List;

public class UnitDetails {
    private String unitAddress;
    private int unitID;
    private List<Integer> barrackID;
    public Integer getBillAuthoritativeUnit() {
        return billAuthoritativeUnit;
    }

    public void setBillAuthoritativeUnit(Integer billAuthoritativeUnit) {
        this.billAuthoritativeUnit = billAuthoritativeUnit;
    }

    private Integer billAuthoritativeUnit;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    private String unitName;

    public String getUnitAddress() {
        return unitAddress;
    }

    public void setUnitAddress(String unitAddress) {
        this.unitAddress = unitAddress;
    }

    public int getUnitID() {
        return unitID;
    }

    public void setUnitID(int unitID) {
        this.unitID = unitID;
    }

    public List<Integer> getBarrackID() {
        return barrackID;
    }

    public void setBarrackID(List<Integer> barrackID) {
        this.barrackID = barrackID;
    }

}
