package com.sisindia.ai.android.database.annualkitreplacementDTO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.annualkitreplacement.AnnualOrAdhocKitItemMO;
import com.sisindia.ai.android.annualkitreplacement.FCMAkrDistributionData;
import com.sisindia.ai.android.annualkitreplacement.KitItemModel;
import com.sisindia.ai.android.annualkitreplacement.KitSizeMO;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 02-09-2016.
 */
public class KitItemSelectionStatementDB extends SISAITrackingDB {

    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public KitItemSelectionStatementDB(Context context) {
        super(context);
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }


    public ArrayList<KitItemModel> getKitItemList() {

        ArrayList<KitItemModel> kitItemModelList = new ArrayList<>();

        SQLiteDatabase sqlite = null;
        String kitItemQuery = "select * from " +
                TableNameAndColumnStatement.KIT_ITEM_TABLE;
        Timber.d("SequencePostId selectedQuery  %s", kitItemQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        KitItemModel kitItemModel = new KitItemModel();
                        kitItemModel.setId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)));
                        kitItemModel.setKitItemCode(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_CODE)));
                        kitItemModel.setName(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_NAME)));
                        if (cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.IS_ACTIVE)) == 0)
                            kitItemModel.setIsActive(false);
                        else
                            kitItemModel.setIsActive(true);

                        kitItemModelList.add(kitItemModel);
                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", kitItemModelList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s",
                    sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return kitItemModelList;

    }


    public ArrayList<AnnualOrAdhocKitItemMO> getAnnaulOrAdhocKitItems(int kitTypeId, String ids) {

        ArrayList<AnnualOrAdhocKitItemMO> annualOrAdhocKitItemMOList = new ArrayList<>();
        String kitItemQuery = "";
        if (TextUtils.isEmpty(ids)) {
            kitItemQuery = " SELECT unit_name, unit_id, total_kit_count, issued_kit_count, " +
                    " unpaid_kit_count, (total_kit_count - issued_kit_count - unpaid_kit_count) pending_kit_count " +
                    " FROM (SELECT base.unit_name, base.unit_id, " +
                    " ifnull(total_kit_count,0) as total_kit_count, " +
                    " ifnull(issued_kit_count,0) as issued_kit_count, " +
                    " ifnull(unpaid_kit_count,0) as unpaid_kit_count from (select distinct" +
                    " u.unit_name as unit_name, u.unit_id as unit_id from unit u " +
                    " inner join kit_distribution kd on u.unit_id = kd.unit_id and " +
                    " kit_type_id = " + kitTypeId + ")base left join (select count(kd.id) as total_kit_count, " +
                    " unit_id from kit_distribution kd where kit_type_id = " + kitTypeId + " group by unit_id)t " +
                    " on t.unit_id = base.unit_id left join " +
                    " (select count(id) issued_kit_count, unit_id from kit_distribution " +
                    " where distribution_status in (3,4) and kit_type_id =" + kitTypeId +
                    " group by unit_id)i on i.unit_id = base.unit_id " +
                    " left join (SELECT Unit_id, count(unit_id) unpaid_kit_count from " +
                    " (SELECT Unit_id, kd.kit_distribution_id from kit_distribution kd " +
                    " inner join kit_distribution_item kdi on kdi.kit_distribution_id = kd.kit_distribution_id " +
                    " where is_unpaid = 1 group by Unit_id, kd.kit_distribution_id )ukc " +
                    " group by unit_id)up on base.unit_id = up.unit_Id group by unit_name)";
        } else {
            kitItemQuery = " SELECT unit_name, unit_id, total_kit_count, issued_kit_count, " +
                    " unpaid_kit_count, (total_kit_count - issued_kit_count - unpaid_kit_count) pending_kit_count " +
                    " FROM (SELECT base.unit_name, base.unit_id, " +
                    " ifnull(total_kit_count,0) as total_kit_count, " +
                    " ifnull(issued_kit_count,0) as issued_kit_count, " +
                    " ifnull(unpaid_kit_count,0) as unpaid_kit_count from (select distinct" +
                    " u.unit_name as unit_name, u.unit_id as unit_id from unit u " +
                    " inner join kit_distribution kd on u.unit_id = kd.unit_id and " +
                    " kit_type_id = " + kitTypeId + ")base left join (select count(kd.id) as total_kit_count, " +
                    " unit_id from kit_distribution kd where kit_type_id = " + kitTypeId + " group by unit_id)t " +
                    " on t.unit_id = base.unit_id left join " +
                    " (select count(id) issued_kit_count, unit_id from kit_distribution " +
                    " where distribution_status in (3,4) and kit_type_id =" + kitTypeId +
                    " group by unit_id)i on i.unit_id = base.unit_id " +
                    " left join (SELECT Unit_id, count(unit_id) unpaid_kit_count from " +
                    " (SELECT Unit_id, kd.kit_distribution_id from kit_distribution kd " +
                    " inner join kit_distribution_item kdi on kdi.kit_distribution_id = kd.kit_distribution_id " +
                    " where is_unpaid = 1 group by Unit_id, kd.kit_distribution_id )ukc " +
                    " group by unit_id)up on base.unit_id = up.unit_Id group by unit_name)" +
                    " where unit_id in(" + ids + ")";
        }

        SQLiteDatabase sqlite = null;

        Timber.d("AnnualOrAdhocKitItemQuery  %s", kitItemQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        AnnualOrAdhocKitItemMO annualOrAdhocKitItemMO = new AnnualOrAdhocKitItemMO();
                        annualOrAdhocKitItemMO.setUnitId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        annualOrAdhocKitItemMO.setUnitName(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        annualOrAdhocKitItemMO.setIssuedKitCount(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.ISSUED_KIT_COUNT)));
                        annualOrAdhocKitItemMO.setPendingKitCount(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.PENDING_KIT_COUNT)));
                        annualOrAdhocKitItemMO.setTotalItems(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.TOTAL_KIT_COUNT)));
                        annualOrAdhocKitItemMO.setUnPaidKits(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.UN_PAID_COUNT)));
                        annualOrAdhocKitItemMOList.add(annualOrAdhocKitItemMO);
                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", annualOrAdhocKitItemMOList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s",
                    sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return annualOrAdhocKitItemMOList;

    }


    public ArrayList<AnnualOrAdhocKitItemMO> getAssignedOrDeliveredKitItems(int unitId, int kitTypeId, int status) {
        ArrayList<AnnualOrAdhocKitItemMO> annualOrAdhocKitItemMOList = new ArrayList<>();
        SQLiteDatabase sqlite = null;
        String kitItemQuery = " select kd.guard_code as guard_code,kd.guard_name as guard_name," +
                " kd.recipient_id as guard_id," +
                " u.unit_name,kd.distribution_date,kd.task_id,kd.kit_distribution_id," +
                " (select count(kdi.id) from kit_distribution_item kdi " +
                " where kd.kit_distribution_id = kdi.kit_distribution_id) as count," +
                " (select kdi.is_unpaid from kit_distribution_item kdi " +
                " where kd.kit_distribution_id = kdi.kit_distribution_id) as isUnPaid," +
                " kd.distribution_status from kit_distribution kd " +
                " inner join unit u on u.unit_id = kd.unit_id " +
                " where u.unit_id =" + unitId + " and kd.distribution_status in(" + status + ")" +
                " and kd.kit_type_id = " + kitTypeId;

        Timber.d("AssignedOrDeliveredKitItemQuery  %s", kitItemQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        AnnualOrAdhocKitItemMO annualOrAdhocKitItemMO = new AnnualOrAdhocKitItemMO();
                        annualOrAdhocKitItemMO.setDeliveryStatusId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.DISTRIBUTION_STATUS)));
                        annualOrAdhocKitItemMO.setCount(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.COUNT)));
                        annualOrAdhocKitItemMO.setGuardId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_ID)));
                        annualOrAdhocKitItemMO.setGuardCode(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        annualOrAdhocKitItemMO.setGuardName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        annualOrAdhocKitItemMO.setUnitId(unitId);
                        if (cursor.getInt(cursor.getColumnIndex("isUnPaid")) == 0) {
                            annualOrAdhocKitItemMO.setUnpiad(false);
                        } else {
                            annualOrAdhocKitItemMO.setUnpiad(true);
                        }

                        annualOrAdhocKitItemMO.setKitDistributionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID)));
                        annualOrAdhocKitItemMO.setTaskId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        annualOrAdhocKitItemMO.setUnitName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_NAME)));
                        String issuedDate = dateTimeFormatConversionUtil.convertDateTimeToDate(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.DISTRIBUTION_DATE)));
                        annualOrAdhocKitItemMO.setIssueDate(issuedDate);
                        annualOrAdhocKitItemMOList.add(annualOrAdhocKitItemMO);
                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", annualOrAdhocKitItemMOList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return annualOrAdhocKitItemMOList;

    }

    public ArrayList<KitItemModel> getKitDeliveredItemList(int unitId, int employeeId, int kitdistribution_id) {

        ArrayList<KitItemModel> kitItemModelList = new ArrayList<>();

        SQLiteDatabase sqlite = null;
        String kitDeliveredItemQuery = " select ki.kit_item_name,ki.kit_item_id," +
                " ks.kit_size_name ," +
                " kd.kit_distribution_id," +
                " kdi.kit_distribution_item_id, kdi.is_issued  from kit_item ki " +
                " join kit_distribution_item  kdi on ki.kit_item_id = kdi.kit_item_id " +
                " join kit_distribution  kd on kd.kit_distribution_id = kdi.kit_distribution_id " +
                " left join kit_size_table  ks on kdi.kit_size_id = ks.kit_size_id " +
                " where kd.unit_id= " + unitId + " and kd.recipient_id = " + employeeId +
                " and kd.kit_distribution_id = " + kitdistribution_id;
        Timber.d("kitDeliveredItem  SelectedQuery  %s", kitDeliveredItemQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitDeliveredItemQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        KitItemModel kitItemModel = new KitItemModel();
                        kitItemModel.setId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_ID)));
                        kitItemModel.setName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_ITEM_NAME)));
                        kitItemModel.setIsIssued(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUED)));
                        kitItemModel.setKitDistributionId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID)));
                        kitItemModel.setKitDistributionItemId(cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID)));
                        kitItemModel.setKitSizeName(cursor.getString(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_SIZE_NAME)));

                        kitItemModelList.add(kitItemModel);
                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", kitItemModelList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return kitItemModelList;

    }


    public ArrayList<KitSizeMO> getKitSizeList(int kitItemId) {

        ArrayList<KitSizeMO> kitSizeMOList = new ArrayList<>();

        SQLiteDatabase sqlite = null;
        String kitSizeQuery = "select ks.kit_size_name, ks.kit_size_id ,kis.kit_item_size_id from kit_size_table ks " +
                " inner join kit_item_size kis on ks.kit_size_id = kis.kit_size_id " +
                " inner join kit_item ki on kis.kit_item_id = ki.kit_item_id " +
                " where ki.kit_item_id = " + kitItemId;
        Timber.d("kitSizeQuery  %s", kitSizeQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitSizeQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        KitSizeMO kitSizeMO = new KitSizeMO();

                        kitSizeMO.setKitSizeId(cursor.getInt(cursor.getColumnIndex("kit_size_id")));
                        kitSizeMO.setKitItemSizeId(cursor.getInt(cursor.getColumnIndex("kit_item_size_id")));
                        kitSizeMO.setKitSizeName(cursor.getString(cursor.getColumnIndex("kit_size_name")));
                        kitSizeMOList.add(kitSizeMO);

                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", kitSizeMOList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s", sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return kitSizeMOList;

    }

    public ArrayList<FCMAkrDistributionData> getKitDistributionList() {

        ArrayList<FCMAkrDistributionData> kitDistributionsMOArrayList = new ArrayList<>();

        SQLiteDatabase sqlite = null;
        String kitDistributionQuery = "select * from " + TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE;
        Timber.d("getKitDistributionList  %s", kitDistributionQuery);
        Cursor cursor = null;
        try {
            sqlite = this.getReadableDatabase();
            cursor = sqlite.rawQuery(kitDistributionQuery, null);

            if (cursor != null && !cursor.isClosed()) {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        FCMAkrDistributionData fcmAkrDistributionData = new FCMAkrDistributionData();
                        fcmAkrDistributionData.setId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID)));
                        fcmAkrDistributionData.setUnitTaskId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.TASK_ID)));
                        fcmAkrDistributionData.setIssuingOfficerId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.ISSUE_OFFICER_ID)));
                        fcmAkrDistributionData.setUnitId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.UNIT_ID)));
                        fcmAkrDistributionData.setDistributionStatus(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.DISTRIBUTION_STATUS)));
                        fcmAkrDistributionData.setRecipientId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.RECIPIENT_ID)));
                        fcmAkrDistributionData.setKitTypeId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.KIT_TYPE_ID)));
                        fcmAkrDistributionData.setBranchId(cursor.getInt(cursor
                                .getColumnIndex(TableNameAndColumnStatement.BRANCH_ID)));
                        fcmAkrDistributionData.setDistributionDate(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.DISTRIBUTION_DATE)));
                        fcmAkrDistributionData.setGuardName(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.GUARD_NAME)));
                        fcmAkrDistributionData.setGuardNo(cursor.getString(cursor
                                .getColumnIndex(TableNameAndColumnStatement.GUARD_CODE)));
                        kitDistributionsMOArrayList.add(fcmAkrDistributionData);
                    } while (cursor.moveToNext());
                }

            }

            Timber.d("cursor detail object  %s", kitDistributionsMOArrayList);
        } catch (SQLiteConstraintException sqliteConstraintException) {
            Timber.e(Constants.SQLITE_CONSTRAINT_EXCEPTION + "%s",
                    sqliteConstraintException.getCause());
            Crashlytics.logException(sqliteConstraintException);
        } catch (SQLiteException sqliteException) {
            Timber.e(Constants.SQLITE_EXCEPTION + "%s", sqliteException.getCause());
            Crashlytics.logException(sqliteException);
        } catch (Exception exception) {
            Timber.e(Constants.EXCEPTION + "%s", exception.getCause());
            Crashlytics.logException(exception);
        } finally {
            if (cursor != null && sqlite != null) {
                cursor.close();
                sqlite.close();
            }
        }
        return kitDistributionsMOArrayList;

    }
}
