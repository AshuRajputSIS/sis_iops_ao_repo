package com.sisindia.ai.android.help;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;


public class OnlineVideosFragment extends BaseFragment {
    @Bind(R.id.webview)
    WebView webView;

    //http://50.31.147.142/IOPS/index.html?URL=hdtyughydtefghuit567895673hgt4y567ujhtg478dgtyh5ki&UID= --> previous
    //http://13.76.140.203/SISIOPS/Home/Index?URL=hdtyughydtefghuit567895673hgt4y567ujhtg478dgtyh5ki&UID="
    private static String ONLINE_VIDEO_LINK = "http://13.76.140.203/SISIOPS/Home/Index?URL=hdtyughydtefghuit567895673hgt4y567ujhtg478dgtyh5ki&UID=";

    public static OnlineVideosFragment newInstance() {
        OnlineVideosFragment fragment = new OnlineVideosFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online_videos, container, false);
        ButterKnife.bind(this, view);
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are open in new browser not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
//                Log.e("WebUrls", url);
            }

            public void onPageFinished(WebView view, String url) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                view.loadUrl("file:///android_asset/error.html");
                Toast.makeText(getActivity(), "Error occurred, please check network connectivity", Toast.LENGTH_SHORT).show();
                super.onReceivedError(view, request, error);
            }
        });
        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
//        webView.setScrollBarStyle(WebView.SCROLL_AXIS_NONE);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.loadUrl(ONLINE_VIDEO_LINK + appPreferences.getAppUserId());
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_online_videos;
    }

}
