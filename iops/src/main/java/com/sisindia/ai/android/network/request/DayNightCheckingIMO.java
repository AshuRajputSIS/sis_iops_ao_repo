package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DayNightCheckingIMO {

    @SerializedName("UnitPosts")
    @Expose
    private List<UnitPostIMO> UnitPosts = new ArrayList<UnitPostIMO>();
    @SerializedName("GuardDetails")
    @Expose
    private List<GuardDetailIMO> GuardDetails = new ArrayList<GuardDetailIMO>();
    @SerializedName("ClientMeetingInfoIMO")
    @Expose
    private ClientMeetingInfoIMO ClientMeetingInfo;

    /**
     * @return The UnitPosts
     */
    public List<UnitPostIMO> getUnitPosts() {
        return UnitPosts;
    }

    /**
     * @param UnitPosts The UnitPosts
     */
    public void setUnitPosts(List<UnitPostIMO> UnitPosts) {
        this.UnitPosts = UnitPosts;
    }

    /**
     * @return The GuardDetails
     */
    public List<GuardDetailIMO> getGuardDetails() {
        return GuardDetails;
    }

    /**
     * @param GuardDetails The GuardDetails
     */
    public void setGuardDetails(List<GuardDetailIMO> GuardDetails) {
        this.GuardDetails = GuardDetails;
    }

    /**
     * @return The ClientMeetingInfoIMO
     */
    public ClientMeetingInfoIMO getClientMeetingInfo() {
        return ClientMeetingInfo;
    }

    /**
     * @param ClientMeetingInfo The ClientMeetingInfoIMO
     */
    public void setClientMeetingInfo(ClientMeetingInfoIMO ClientMeetingInfo) {
        this.ClientMeetingInfo = ClientMeetingInfo;
    }

    @Override
    public String toString() {
        return "DayNightCheckingIMO{" +
                "UnitPosts=" + UnitPosts +
                ", GuardDetails=" + GuardDetails +
                ", ClientMeetingInfo=" + ClientMeetingInfo +
                '}';
    }
}