package com.sisindia.ai.android.recruitment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.BaseRecruitmentMO;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.TotalRecruite;
import com.sisindia.ai.android.views.BarChartView;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saruchi on 20-04-2016.
 */
public class RecuirtRecylerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    BaseRecruitmentMO mRecurimentModelObjec;
    List<TotalRecruite> mRecruitmentList ;
    private ArrayList<Object> objectArrayList;

    public RecuirtRecylerAdapter(Context context) {
        this.context = context;

    }

    public void setObjectArrayList(ArrayList<Object> objectArrayList) {
        this.objectArrayList = objectArrayList;
    }
    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder recycleViewHolder = null;
        View itemView;
        switch (viewType) {

            case TYPE_HEADER:
                itemView = LayoutInflater.from(context).inflate(R.layout.recuirt_headerview, parent, false);
                recycleViewHolder = new HeaderViewHolder(itemView);
                break;
            case TYPE_ROW:

                itemView = LayoutInflater.from(context).inflate(R.layout.recuirt_row, parent, false);
                recycleViewHolder = new RecuirtViewHolder(itemView);
                break;
        }
        return recycleViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.setBarChatData((BaseRecruitmentMO) objectArrayList.get(position));
                headerViewHolder.setTotalHeaderCount((BaseRecruitmentMO) objectArrayList.get(position));
                break;

            case TYPE_ROW:
                RecuirtViewHolder rowDataHolder = (RecuirtViewHolder) holder;
                TotalRecruite totalRecruite = (TotalRecruite) objectArrayList.get(position);
                if(null!=totalRecruite){
                    rowDataHolder.mRecuirtname.setText(totalRecruite.getFullName());
                    rowDataHolder.mCurrentstatus.setText(totalRecruite.getStatus());
                    if(totalRecruite.getActionTakenOn()!=null && !TextUtils.isEmpty(totalRecruite.getActionTakenOn().toString())) {
                        rowDataHolder.mDateaddded.setText(totalRecruite.getActionTakenOn());
                    }else {
                        rowDataHolder.mDateaddded.setText(context.getResources().getString(R.string.noaction_taken));
                    }
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ROW;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (objectArrayList != null) {
            count = objectArrayList.size() == 0 ? 0 : objectArrayList.size();
        }
        return count;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public class RecuirtViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mRecuirtname, mCurrentstatus, mDateaddded;

        public RecuirtViewHolder(View view) {
            super(view);
            this.mRecuirtname = (CustomFontTextview) view.findViewById(R.id.recuirt_name);
            this.mCurrentstatus = (CustomFontTextview) view.findViewById(R.id.current_status);
            this.mDateaddded = (CustomFontTextview) view.findViewById(R.id.date_addded);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        BarChartView bar;
        CustomFontTextview mRecommendedCount;
        CustomFontTextview mSelectedCount;
        CustomFontTextview mRejectedCount;
        CustomFontTextview mInProgressCount;

        public HeaderViewHolder(View view) {
            super(view);
            bar = (BarChartView)view.findViewById(R.id.barchartview);
            mRecommendedCount = (CustomFontTextview)view.findViewById(R.id.total_recommended);
            mSelectedCount = (CustomFontTextview)view.findViewById(R.id.total_selected);
            mRejectedCount = (CustomFontTextview)view.findViewById(R.id.total_rejected);
            mInProgressCount = (CustomFontTextview)view.findViewById(R.id.total_inprogress);
        }

        public void setBarChatData(BaseRecruitmentMO baseRecruitmentMO){
                bar.setBarChartData(baseRecruitmentMO.getCurrentMonth(),baseRecruitmentMO.getPreviousMonth());
        }

        public void setTotalHeaderCount(BaseRecruitmentMO baseRecruitmentMO){
                mRecommendedCount.setText(Integer.toString(baseRecruitmentMO.getCurrentYear().getRecommended()));
                mSelectedCount.setText(Integer.toString(baseRecruitmentMO.getCurrentYear().getSelected()));
                mRejectedCount.setText(Integer.toString(baseRecruitmentMO.getCurrentYear().getRejected()));
                mInProgressCount.setText(Integer.toString(baseRecruitmentMO.getCurrentYear().getInprocess()));
        }
    }
}

