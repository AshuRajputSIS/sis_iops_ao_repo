package com.sisindia.ai.android.issues.improvements;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.issues.complaints.AddComplaintActivity;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Shushrut on 20-04-2016.
 */
public class CustomPlanTaskAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    Context mcontext;
    ArrayList<Object> mData;
    boolean setVisible = false;
    View mDividerView;
    CheckBox mCheckBox;
    CustomFontTextview mAssignedTo, item_name;
    ImageView mCustomImageView;
    boolean assignedTo;
    int currentTab;
    private int mOpenIssues_layout = 0;
    private int mGreviance_layout = 1;

    public CustomPlanTaskAdapter(Context mcontext, ArrayList<Object> mData, boolean setVisible, boolean showassingedTo, int currentTab) {
        this.mcontext = mcontext;
        this.mData = mData;
        this.setVisible = setVisible;
        this.assignedTo = showassingedTo;
        this.currentTab = currentTab;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.improvement_plan_name_row, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.showAssignedTo) {
                    mcontext.startActivity(new Intent(mcontext, EnterDateActivity.class));
                }
            }
        });
        mCheckBox = (CheckBox) view.findViewById(R.id.checkbox_plan_task);
        mCheckBox.setChecked(false);
        mAssignedTo = (CustomFontTextview) view.findViewById(R.id.assigned_to_view);
        item_name = (CustomFontTextview) view.findViewById(R.id.item_name);
        if (setVisible) {
            mCheckBox.setVisibility(View.VISIBLE);
        } else {
            mCheckBox.setVisibility(View.GONE);
        }
        if (Util.showAssignedTo && currentTab == 1) {
            mAssignedTo.setVisibility(View.VISIBLE);
            mCheckBox.setVisibility(View.VISIBLE);
            mCheckBox.setChecked(true);
        } else if (Util.showAssignedTo && currentTab == 0) {
            mAssignedTo.setVisibility(View.VISIBLE);
            mCheckBox.setVisibility(View.VISIBLE);
            mCheckBox.setChecked(false);
        } else {
            mAssignedTo.setVisibility(View.GONE);

        }
        return new GrevianceHolder(view);


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (Util.showAssignedTo && currentTab == 1) {
            if (position == 0)
                item_name.setText("Strength fulfilment");
            else if (position == 1)
                item_name.setText("Replace kit");

            //item_name.setChecked(true);
        } else if (Util.showAssignedTo && currentTab == 0) {
            if (position == 0)
                item_name.setText("Onsite Tranning");
            else if (position == 1)
                item_name.setText("Visit by ED");
            else
                item_name.setText("Increase Checking Frequency");

        }


    }

   /* @Override
    public void onBindViewHolder( holder, int position) {
if(getItemViewType(position) == mOpenIssues_layout){

}else{
    holder.m
}

    }*/

    @Override
    public int getItemCount() {
        if (currentTab == 0) {
            return 3;
        } else {
            return 2;
        }

    }

    @Override
    public int getItemViewType(int position) {

        return mGreviance_layout;


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checking_grievance_complaint_view_details_txt:
                Intent viewComplaintDetails = new Intent(mcontext, AddComplaintActivity.class);
                viewComplaintDetails.putExtra("isfromAddIssue", false);
                viewComplaintDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Util.showOptionMenu = false;
                mcontext.startActivity(viewComplaintDetails);
                break;
            default:
                Intent addDate = new Intent(mcontext, EnterDateActivity.class);
                mcontext.startActivity(addDate);
                break;
        }
    }


    private class GrevianceHolder extends RecyclerView.ViewHolder {
        public CheckBox mguardname_txt;
        public CustomFontTextview complaint_type_txt;
        public CustomFontTextview mAssignedTo;

        public GrevianceHolder(View view) {
            super(view);
            mguardname_txt = (CheckBox) view.findViewById(R.id.checkbox_plan_task);

            complaint_type_txt = (CustomFontTextview) view.findViewById(R.id.checking_grievance_complaint_cardview_complaint_type_txt);
        }
    }
}


