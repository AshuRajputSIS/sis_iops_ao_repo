package com.sisindia.ai.android.annualkitreplacement;

import com.google.gson.annotations.SerializedName;

public class KitReplaceCountOR {

@SerializedName("StatusCode")
private Integer statusCode;
@SerializedName("StatusMessage")
private String statusMessage;
@SerializedName("Data")
private KitReplaceCountData kitReplaceCountData;

/**
* 
* @return
* The statusCode
*/
public Integer getStatusCode() {
return statusCode;
}

/**
* 
* @param statusCode
* The StatusCode
*/
public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

/**
* 
* @return
* The statusMessage
*/
public String getStatusMessage() {
return statusMessage;
}

/**
* 
* @param statusMessage
* The StatusMessage
*/
public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

/**
* 
* @return
* The data
*/
public KitReplaceCountData getData() {
return kitReplaceCountData;
}

/**
* 
* @param data
* The Data
*/
public void setData(KitReplaceCountData data) {
this.kitReplaceCountData = data;
}

}