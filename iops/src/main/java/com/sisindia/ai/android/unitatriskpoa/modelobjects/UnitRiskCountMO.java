package com.sisindia.ai.android.unitatriskpoa.modelobjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 14/7/16.
 */

public class UnitRiskCountMO implements Serializable {

    @SerializedName("RiskCount")
    private Integer riskCount;
    @SerializedName("RiskCountByAreaInspector")
    private Integer riskCountByAreaInspector;
    @SerializedName("ActionCount")
    private int actionCount;
    @SerializedName("ActionPendingCount")
    private Integer actionPendingCount;

    /**
     *
     * @return
     * The riskCount
     */
    public Integer getRiskCount() {
        return riskCount;
    }

    /**
     *
     * @param riskCount
     * The RiskCount
     */
    public void setRiskCount(Integer riskCount) {
        this.riskCount = riskCount;
    }

    /**
     *
     * @return
     * The riskCountByAreaInspector
     */
    public Integer getRiskCountByAreaInspector() {
        return riskCountByAreaInspector;
    }

    /**
     *
     * @param riskCountByAreaInspector
     * The RiskCountByAreaInspector
     */
    public void setRiskCountByAreaInspector(Integer riskCountByAreaInspector) {
        this.riskCountByAreaInspector = riskCountByAreaInspector;
    }

    /**
     *
     * @return
     * The actionCount
     */
    public Integer getActionCount() {
        return actionCount;
    }

    /**
     *
     * @param actionCount
     * The ActionCount
     */
    public void setActionCount(Integer actionCount) {
        this.actionCount = actionCount;
    }

    /**
     *
     * @return
     * The actionPendingCount
     */
    public Integer getActionPendingCount() {
        return actionPendingCount;
    }

    /**
     *
     * @param actionPendingCount
     * The ActionPendingCount
     */
    public void setActionPendingCount(Integer actionPendingCount) {
        this.actionPendingCount = actionPendingCount;
    }

}
