package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 12-05-2016.
 */
public class LastCheckInspectionGrievancesMO {

    private String unitName;
    private String guardName;
    private String grievanceNature;
    private String grievanceClosedDate;
    private String timeDurationLeft;
    private String assignedTo;
    private String grievanceStatus;
    private String createdDateTime;
    private String status;
    private int pId;
    private String employeeId;
    private String assignedToName;

    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }

    private String barrackName;


    public String getGrievanceNature() {
        return grievanceNature;
    }

    public void setGrievanceNature(String grievanceNature) {
        this.grievanceNature = grievanceNature;
    }



    public String getGrievanceClosedDate() {
        return grievanceClosedDate;
    }

    public void setGrievanceClosedDate(String grievanceClosedDate) {
        this.grievanceClosedDate = grievanceClosedDate;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getGrievanceStatus() {
        return grievanceStatus;
    }

    public void setGrievanceStatus(String grievanceStatus) {
        this.grievanceStatus = grievanceStatus;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    public String getTimeDurationLeft() {
        return timeDurationLeft;
    }

    public void setTimeDurationLeft(String timeDurationLeft) {
        this.timeDurationLeft = timeDurationLeft;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }


    public String getAssignedToName() {
        return assignedToName;
    }

    public void setAssignedToName(String assignedToName) {
        this.assignedToName = assignedToName;
    }
}
