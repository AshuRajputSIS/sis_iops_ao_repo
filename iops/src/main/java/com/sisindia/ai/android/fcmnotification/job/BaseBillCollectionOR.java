package com.sisindia.ai.android.fcmnotification.job;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.UnitMonthlyBillDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by compass on 8/10/2017.
 */

class BaseBillCollectionOR {

    @SerializedName("StatusCode")
    public Integer statusCode;

    @SerializedName("StatusMessage")
    public String statusMessage;

    @SerializedName("Data")
    public List<UnitMonthlyBillDetail> unitMonthlyBillDetail;
}
