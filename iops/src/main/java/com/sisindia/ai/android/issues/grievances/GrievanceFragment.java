package com.sisindia.ai.android.issues.grievances;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.issues.IssueTypeEnum;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.GrievanceCount;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GrievanceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GrievanceFragment extends BaseFragment implements OnRecyclerViewItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public RecyclerView mgrievance_recyclerview;
    public GrievancesCardviewAdapter grievancesCardviewAdapter;
    public ArrayList<Object> mDataValues = new ArrayList<>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<Object> issuesList;
    private GrievanceCount grievanceCountData;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Context mContext;
    private ArrayList<Object> openGrievanceList;


    public GrievanceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GrievanceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GrievanceFragment newInstance(String param1, String param2) {

        GrievanceFragment fragment = new GrievanceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.issues_homepage_grievance, container, false);
        ButterKnife.bind(this, view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mgrievance_recyclerview = (RecyclerView) view.findViewById(R.id.grievance_recyclerview);
        mgrievance_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        Button addGrievanceButton = (Button) view.findViewById(R.id.add_grievance);
        updateUI();
        grievancesCardviewAdapter.setOnRecyclerViewClicked(this);
        addGrievanceButton.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    protected int getLayout() {
        return 0;
    }


    public void updateUI() {

        issuesList = new ArrayList<>();
        GrievanceData grievanceData = IOPSApplication.getInstance().getIssueSelectionDBInstance().getGrievanceList(IssueTypeEnum.Grievance.getValue());
        if (grievanceData != null) {
            openGrievanceList = grievanceData.getGrievanceList();
            grievanceCountData = (GrievanceCount) IOPSApplication.getInstance().getIssueSelectionDBInstance().getOpenIssuesCount(1);
        }
        if (grievanceCountData != null) {
            issuesList.add(grievanceCountData);
        } else {
            grievanceCountData = new GrievanceCount();
            issuesList.add(grievanceCountData);
        }

        issuesList.addAll(openGrievanceList);
        if (grievancesCardviewAdapter == null) {
            grievancesCardviewAdapter = new GrievancesCardviewAdapter(getContext());
        }

        grievancesCardviewAdapter.setIssuesDataList(issuesList);
        mgrievance_recyclerview.setAdapter(grievancesCardviewAdapter);
        grievancesCardviewAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        appPreferences.setIssueType(Constants.NavigationFlags.TYPE_GRIEVANCE);
        Intent editGrievanceIntent = new Intent(getActivity(), EditGrievanceActivity.class);
        editGrievanceIntent.putExtra(getResources().getString(R.string.grievanceDataObj), (IssuesMO) issuesList.get(position));
        startActivityForResult(editGrievanceIntent, 100);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        switch (viewId) {
            case R.id.add_grievance:
                if (appPreferences.getDutyStatus()) {
                    appPreferences.setIssueType(Constants.NavigationFlags.TYPE_GRIEVANCE);
                    openOnBoardingActivity(OnBoardingFactory.GRIEVANCE, 3);
                } else {

                    snackBarWithMessage(getResources().getString(R.string.TURN_DUTY));
                }

        }
    }


    public void setOpenGrievanceData(GrievanceCount grievanceCount) {
        grievanceCountData = grievanceCount;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @Override
    public void onRefresh() {
        updateUI();
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(mContext, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        mContext.startActivity(intent);
    }

    private void showprogressbar() {
        Util.showProgressBar(getActivity(), R.string.loading_please_wait);

    }


    private void hideprogressbar() {
        Util.dismissProgressBar(isVisible());
    }
}
