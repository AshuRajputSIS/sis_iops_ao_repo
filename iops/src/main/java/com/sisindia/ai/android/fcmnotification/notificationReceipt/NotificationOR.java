package com.sisindia.ai.android.fcmnotification.notificationReceipt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 11/11/16.
 */

public class NotificationOR {
    @SerializedName("NotificationReceiptId")
    @Expose
    private Integer notificationReceiptId;

    /**
     *
     * @return
     * The notificationReceiptId
     */
    public Integer getNotificationReceiptId() {
        return notificationReceiptId;
    }

    /**
     *
     * @param notificationReceiptId
     * The NotificationReceiptId
     */
    public void setNotificationReceiptId(Integer notificationReceiptId) {
        this.notificationReceiptId = notificationReceiptId;
    }

    @Override
    public String toString() {
        return "NotificationOR{" +
                "notificationReceiptId=" + notificationReceiptId +
                '}';
    }
}
