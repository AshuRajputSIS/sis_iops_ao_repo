package com.sisindia.ai.android.views;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by Saruchi on 04-08-2016.
 */

public class CalenderTargateDateView extends  BaseFrame{
    private Context mcontext;
    @Bind(R.id.calenderLayout)
    LinearLayout calenderLayout;
    @Bind(R.id.edittext_target_date)
    TextView dateTxt;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private FragmentActivity mContextActivity;
    private String targetdateStr;

    public CalenderTargateDateView(Context context) {
        super(context);
        init(context, null);

    }

    public CalenderTargateDateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        Timber.d("init");
        mcontext = context;
        LayoutInflater.from(getContext()).inflate(R.layout.target_action_date, this);
        ButterKnife.bind(this);
        dateTimeFormatConversionUtil= new DateTimeFormatConversionUtil();
    }
    public void showCalendar() {
        Date mindate =dateTimeFormatConversionUtil.convertStringToDateFormat(dateTimeFormatConversionUtil.getCurrentDate());
        Date maxDate=dateTimeFormatConversionUtil.convertStringToDateFormat(getSelectedDate());
        fragmentManager = mContextActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        DateRangeCalender selectDateFragment = DateRangeCalender.newInstance(mindate, maxDate);
        selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                String updateDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                setDateTextview(updateDate);
                //issuesMO.setTargetActionEndDate(updateDate);
            }
        });
    }


    /**
     * set the number of days to add
     * days will be added to the calender date and will display the target date in the calender
     * @param days
     */
    public void setDaysToAdd(int days,FragmentActivity mActivityContext){
        mContextActivity=mActivityContext;
        displayActionDate(days);
    }
    private void displayActionDate(int days){
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil() ;
        targetdateStr=dateTimeFormatConversionUtil.addDaysToCurrentDate(days);
        setDateTextview(targetdateStr);
        calenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });

    }
    @OnClick(R.id.calenderLayout)
    public void onSelectDate() {
        showCalendar();
    }

    public String getSelectedDate() {
        return String.valueOf(dateTxt.getText().toString().trim());
    }
    public void setDateTextview(String dateStr) {

        dateTxt.setText(dateStr);

    }
}
