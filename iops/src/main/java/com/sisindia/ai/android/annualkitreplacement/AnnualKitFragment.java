package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AnnualKitFragment extends Fragment implements OnRecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.annualKitRecyclerView)
    RecyclerView annualKitRecyclerView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    private AnnualKitReplacementAdapter annualKitReplacementAdapter;
    private KitReplaceCountData annualKitCountData;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private ArrayList<AnnualOrAdhocKitItemMO> annualKitItemList;
    private ArrayList<Object> totalKitItemList;
    private AppPreferences appPreferences;

    public AnnualKitFragment() {
        // Required empty public constructor
    }

    public static AnnualKitFragment newInstance(String param1, String param2) {
        AnnualKitFragment fragment = new AnnualKitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_annual_kit_replacement, container, false);
        ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
        appPreferences = new AppPreferences(getActivity());
        updateUi();
        return view;
    }

    private void updateUi() {
        try {
            kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
            if (!TextUtils.isEmpty(appPreferences.getFilterIds())) {
                annualKitItemList = kitItemSelectionStatementDB.getAnnaulOrAdhocKitItems(Constants.ANNUALKIT_ITEM_TYPE, appPreferences.getFilterIds());
            } else {
                annualKitItemList = kitItemSelectionStatementDB.getAnnaulOrAdhocKitItems(Constants.ANNUALKIT_ITEM_TYPE, null);
            }
            totalKitItemList = new ArrayList<>();
            if (annualKitCountData != null) {
                List<AnnualKitReplacementMO> annualKitReplacementMOList = annualKitCountData.getAnnualKitReplacements();
                if (annualKitReplacementMOList != null && annualKitReplacementMOList.size() != 0) {
                    totalKitItemList.addAll(annualKitReplacementMOList);
                }
            } else {
                AnnualKitReplacementMO annualKitReplacementMO = new AnnualKitReplacementMO();
                totalKitItemList.add(annualKitReplacementMO);
            }

            if (annualKitItemList != null) {
                totalKitItemList.addAll(annualKitItemList);
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            annualKitRecyclerView.setLayoutManager(linearLayoutManager);
            annualKitRecyclerView.setItemAnimator(new DefaultItemAnimator());
            annualKitReplacementAdapter = new AnnualKitReplacementAdapter(getActivity());
            annualKitRecyclerView.setAdapter(annualKitReplacementAdapter);
            annualKitReplacementAdapter.setAnnualKitReplacemntDetails(totalKitItemList);
            annualKitReplacementAdapter.setOnRecyclerViewItemClickListener(this);
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        //&&  ((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getIssuedKitCount() != 0
        if (totalKitItemList != null) {
            Intent kitTabsIntent = new Intent(getActivity(), AnnualKitReplacementTabsActivity.class);
            kitTabsIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, ((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getUnitId());
            kitTabsIntent.putExtra(TableNameAndColumnStatement.KIT_TYPE_ID, 1);
            kitTabsIntent.putExtra(TableNameAndColumnStatement.UNIT_NAME, ((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getUnitName());
            startActivity(kitTabsIntent);
        }
    }

    public void setAnnualKitCountData(KitReplaceCountData annualKitCountData) {
        this.annualKitCountData = annualKitCountData;
    }

    @Override
    public void onRefresh() {
        getAnnualKitItemsCountCall();
    }

    private void getAnnualKitItemsCountCall() {
        if (getActivity() != null) {
            new SISClient(getActivity()).getApi().kitReplaceCount(
                    new Callback<KitReplaceCountOR>() {
                        @Override
                        public void success(KitReplaceCountOR kitReplaceCountOR, Response response) {
                            switch (kitReplaceCountOR.getStatusCode()) {
                                case HttpURLConnection.HTTP_OK:
                                    annualKitCountData = kitReplaceCountOR.getData();
                                    break;
                                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                                    Toast.makeText(getActivity(), getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    Toast.makeText(getActivity(), getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                                    break;
                            }
                            updateUi();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            updateUi();
                            Util.printRetorfitError("SingleUnitActivity", error);
                        }
                    });
        }
    }
}
