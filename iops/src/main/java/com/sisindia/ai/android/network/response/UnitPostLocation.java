package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class UnitPostLocation {

    @SerializedName("Latitude")
    private Float Latitude;
    @SerializedName("Longitude")
    private Float Longitude;

    /**
     * @return The Latitude
     */
    public Float getLatitude() {
        return Latitude;
    }

    /**
     * @param Latitude The Latitude
     */
    public void setLatitude(Float Latitude) {
        this.Latitude = Latitude;
    }

    /**
     * @return The Longitude
     */
    public Float getLongitude() {
        return Longitude;
    }

    /**
     * @param Longitude The Longitude
     */
    public void setLongitude(Float Longitude) {
        this.Longitude = Longitude;
    }

}