package com.sisindia.ai.android.commons;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.annualkitreplacement.KitReplacementDetailsActivity;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Saruchi on 07-03-2016.
 */
public class CapturePictureActivity extends BaseActivity {

    private final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    private final static int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE = 1;
    public RotaTaskModel mRotaTaskModel;
    private Uri imageUri = null;
    private byte[] imagebytes;
    private ArrayList<String> imagePaths;
    private boolean isPermissionGranted;
    private ContentValues values;
    private Context mContext;
    private String currentTakenImagePath;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private String fileName;
    private int attachmentCount = 0;
    private int referenceTypeId;
    private int mReferenceId = 0; //used to set the reference type id based on navigation screen.
    private String stampName;
    private String imageId;
    private int mSecurityRiskPosition = -1;
    private int mSecurityOptionId = 0;
    private int unitId;
    private int unitTaskId;
    private String unitName;
    private int unitPostId;
    private int kitDistributionId;
    private int barrackId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mContext = CapturePictureActivity.this;

        //retakePictureButton.setOnClickListener(this);
        imagePaths = new ArrayList<String>();
        Intent intent = getIntent();
        if (intent.hasExtra(getResources().getString(R.string.toolbar_title))) {
            setTitle(intent.getExtras().getString(getResources().getString(R.string.toolbar_title)));
            Util.setCaptureImageScreenTitle(intent.getExtras().getString(getResources().getString(R.string.toolbar_title)));
            if (intent.getExtras().getString(getResources().getString(R.string.toolbar_title)).equalsIgnoreCase("security_risk")) {
                mSecurityRiskPosition = intent.getExtras().getInt(getResources().getString(R.string.captureImagePosition));
                mSecurityOptionId = intent.getExtras().getInt(getResources().getString(R.string.security_option_id));
            }
        }

        if (intent.hasExtra(TableNameAndColumnStatement.UNIT_ID) &&
                intent.hasExtra(TableNameAndColumnStatement.UNIT_NAME)) {
            unitId = intent.getIntExtra(TableNameAndColumnStatement.UNIT_ID, 0);
            unitName = intent.getStringExtra(TableNameAndColumnStatement.UNIT_NAME);
            unitTaskId = intent.getIntExtra(TableNameAndColumnStatement.TASK_ID, 0);
        }
        if (intent.hasExtra(TableNameAndColumnStatement.POST_ID)) {
            unitPostId = intent.getIntExtra(TableNameAndColumnStatement.POST_ID, 0);
        }

        if (intent.hasExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID)) {
            kitDistributionId = intent.getIntExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, 0);

        }

        if (intent.hasExtra(TableNameAndColumnStatement.BARRACK_ID)) {
            barrackId = intent.getIntExtra(TableNameAndColumnStatement.BARRACK_ID, 0);
        }
        Object object = SingletonUnitDetails.getInstance().getUnitDetails();
        if (object instanceof RotaTaskModel) {
            mRotaTaskModel = (RotaTaskModel) object;
        }
        if (mRotaTaskModel == null) {
            mRotaTaskModel = appPreferences.getRotaTaskJsonData();
        }

        if (Util.incrementSequenceNumber()) {
            attachmentCount = attachmentCount + 1;
        } else {
            attachmentCount = 0;
        }

        attachmentMetaDataModel = new AttachmentMetaDataModel();
        capturingImage();
    }

    private void showprogressbar(int mesg) {
        if (!isDestroyed()) {
            Util.showProgressBar(CapturePictureActivity.this, mesg);
        }
    }

    private void hideprogressbar() {
        if (!isDestroyed()) {
            Util.dismissProgressBar(isDestroyed());
        }
    }

    private void capturingImage() {
        // Define the file-name to save photo taken by Camera activity
        int sequenceNo = attachmentCount + 1;
        // Create parameters for Intent with filename
        values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image capture by camera");
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(CapturePictureActivity.this, Manifest.permission.READ_CONTACTS)) {
                showMessageOKCancel("You need to access the External storage", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(CapturePictureActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                });
            }
        } else {
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            /**** EXTERNAL_CONTENT_URI : style URI for the "primary" external storage volume. ****/
            // Standard Intent action that can be sent to have the camera
            // application capture an image and return it.
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                /*********** Load Captured Image And Data Start ****************/
                imageId = convertImageUriToFile(imageUri, this);
                Timber.d("imageUri  %s", imageUri.toString());
                Timber.d("currentTakenImagePath  %s", currentTakenImagePath);
                new LoadImagesFromSDCard().execute(imageId);
                /*********** Load Captured Image And Data End ****************/
            } else if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }
    }

    /***********************************************
     * Convert Image Uri path to physical path
     ***********************************************/
    public String convertImageUriToFile(Uri imageUri, Activity activity) {
        Cursor cursor = null;
        int imageID = 0;
        try {
            /*********** Which columns values want to get *******/
            String[] proj = {
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Thumbnails._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION
            };
            cursor = activity.getContentResolver().query(
                    imageUri,         //  Get data for specific image URI
                    proj,             //  Which columns to return
                    null,             //  WHERE clause; which rows to return (all rows)
                    null,             //  WHERE clause selection arguments (none)
                    null              //  Order-by clause (ascending by name)
            );
            //  Get Query Data
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);

//            int columnIndexThumb = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
//            int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            int size = cursor.getCount();

            if (size != 0) {
//                int thumbID = 0;
                if (cursor.moveToFirst()) {
                    /**************** Captured image details ************/
                    /*****  Used to show image on view in LoadImagesFromSDCard class ******/
                    imageID = cursor.getInt(columnIndex);

                    //Commented by AR, as it not used @21June2018 6:33PM
                    /*thumbID = cursor.getInt(columnIndexThumb);
                    String Path = cursor.getString(file_ColumnIndex);
                    String CapturedImageDetails = " CapturedImageDetails : \n\n"
                            + " ImageID :" + imageID + "\n"
                            + " ThumbID :" + thumbID + "\n"
                            + " Path :" + Path + "\n";*/
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return "" + imageID;
    }

    public void deleteImages() {
        try {
            File files = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File[] allFiles = files.listFiles();//All files and folder
            if (allFiles != null) {
                for (File outerFile : allFiles) {
                    if (outerFile != null && outerFile.isDirectory()) {
                        File file = new File(outerFile.getAbsolutePath());
                        File[] internalFiles = file.listFiles();
                        for (File internalFile : internalFiles) {
                            {
                                if (internalFile != null && internalFile.isDirectory()) {
                                    internalFile.delete();
                                }
                            }
                        }
                    } else {
                        if (outerFile != null)
                            outerFile.delete();
                    }
                }
                getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.Images.Media._ID + "=?", new String[]{Long.toString(Long.parseLong(imageId))});
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    /**
     * This method compresses the file to 4 times
     * its size(please set  inSampleSize accordingly,I have set it to 4,
     * if you want more set 8 or 12)
     * <p>
     * <p>
     * <p>
     * Here ReferenceTypeId indicates some Natural sequence number(Always same) in local and server
     * side db for indentifying the attachment belong to  which module.
     * <p>
     * <p>
     * Here mReferenceId
     * for unitPost is unitPostId
     * for Guard  is  GuardId
     * for SecurityRisk is  OptionId
     * for AiPic is AppUserId
     * other  than above just take taskId
     *
     * @param imageCaptureUri
     * @return
     */
    private synchronized Bitmap compressImage(Uri imageCaptureUri) {
        ContentResolver cr = getContentResolver();
        InputStream is = null;
        try {
            is = cr.openInputStream(imageCaptureUri);
        } catch (Exception e) {
            Crashlytics.logException(e);
            finish();
        }
        FileOutputStream fos = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
        }

        switch (Util.getSendPhotoImageTo()) {

            case Util.POST_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_POST, this);
                fileName = Constants.FILE_TYPE_UNIT_POST + "_" + Util.getMpostName() + "_" + "0" + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = Util.getMpostName();
                mReferenceId = unitPostId;
                break;

            case Util.DUTY_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.DUTY_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_DUTY_REGISTER_CHECK + "_" + mRotaTaskModel.getTaskId() + "_" + "0" + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = Util.getMpostName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.CLIENT_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.CLIENT_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_CLIENT_REGISTER_CHECK + "_" + mRotaTaskModel.getTaskId() + "_" + "0" + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = Util.getMpostName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.SITE_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE, this);
                fileName = Constants.FILE_TYPE_UNIT_TASK + "_" + mRotaTaskModel.getTaskId() + "_" + mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.ADD_GREIVANCE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.GRIEVENCE, this);
                fileName = Constants.FILE_TYPE_GREIVANCE + "_" + "0" + "_" + mRotaTaskModel.getTaskId() + "_" + mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = Util.getmGuard_Name();
                mReferenceId = 0;
                break;

            case Util.GUARD:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.GUARD, this);
                if (null == Util.getmGuard_Name() || TextUtils.isEmpty(Util.getmGuard_Name())) {
                    fileName = Constants.FILE_TYPE_GUARD + "_" + Util.getGuardCode() + "_" + mRotaTaskModel.getTaskId() + "_" +
                            mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getGuardCode();
                    mReferenceId = Util.mGuard_Id;
                } else {
                    fileName = Constants.FILE_TYPE_GUARD + "_" + Util.getmGuard_Name() + "_" + mRotaTaskModel.getTaskId() + "_" +
                            mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getmGuard_Name();
                    mReferenceId = Util.mGuard_Id;
                }
                break;

            case Util.GUARD_DETAIL_FULL_IMAGE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.GUARD, this);
                if (null == Util.getmGuard_Name() || TextUtils.isEmpty(Util.getmGuard_Name())) {
                    fileName = Constants.FILE_TYPE_GUARD_FULL_IMAGE + "_" + Util.getGuardCode() + "_" + mRotaTaskModel.getTaskId() + "_" + mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getGuardCode();
                    mReferenceId = Util.mGuard_Id;
                } else {
                    fileName = Constants.FILE_TYPE_GUARD_FULL_IMAGE + "_" + Util.getmGuard_Name() + "_" + mRotaTaskModel.getTaskId() + "_" + mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getmGuard_Name();
                    mReferenceId = Util.mGuard_Id;
                }
                break;

            case Util.GUARD_DETAIL_OVER_TURN_OUT:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.GUARD, this);
                if (null == Util.getmGuard_Name() || TextUtils.isEmpty(Util.getmGuard_Name())) {
                    fileName = Constants.FILE_TYPE_GUARD_TURN_OUT + "_" + Util.getGuardCode() + "_" +
                            mRotaTaskModel.getTaskId() + "_" + mRotaTaskModel.getUnitId() + "_" +
                            Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getGuardCode();
                    mReferenceId = Util.mGuard_Id;
                } else {
                    fileName = Constants.FILE_TYPE_GUARD_TURN_OUT + "_" + Util.getmGuard_Name() + "_" + mRotaTaskModel.getTaskId() + "_" +
                            mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                    stampName = Util.getmGuard_Name();
                    mReferenceId = Util.mGuard_Id;
                }
                break;

            case Util.BILL_SUBMISSION:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BILL_SUBMISSION, this);
                fileName = Constants.BILL_SUBMISSION + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BILL_COLLECTION:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BILL_COLLECTION, this);
                fileName = Constants.BILL_COLLECTION + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_BEDDING:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_BEDDING, this);
                fileName = Constants.BARRACK_BEDDING + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_KIT:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_KIT, this);
                fileName = Constants.BARRACK_KIT + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_MESS:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_MESS, this);
                fileName = Constants.BARRACK_MESS + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_OTHERS:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_OTHERS, this);
                fileName = Constants.BARRACK_OTHERS + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_CUSTODIAN:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_CUSTODIAN, this);
                fileName = Constants.BARRACK_CUSTODIAN + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.BARRACK_LANDLORD:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK_LANDLORD, this);
                fileName = Constants.BARRACK_LANDLORD + "_" + mRotaTaskModel.getTaskId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;
            case Util.SECURITY_RISK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.SECURITY_RISK, this);
                fileName = Constants.SECURITY_RISK + "_" + Util.getmSecurityRiskModelHolder().get(mSecurityRiskPosition).getmRiskTypeHolder().get(mSecurityOptionId).getSecurityRiskQuestionId() + "_" +
                        mRotaTaskModel.getUnitId() + "_" + Util.getmSecurityRiskModelHolder().get(mSecurityRiskPosition).getmRiskTypeHolder().get(mSecurityOptionId).getSecurityRiskOptionId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = Util.getmSecurityRiskModelHolder().get(mSecurityRiskPosition).getmRiskTypeHolder().get(mSecurityOptionId).getSecurityRiskOptionId();
                break;

            case Util.CLIENT_COORDINATION:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.client_coordination, this);
                fileName = Constants.CLIENT_COORDINATION + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.CLIENT_COORDINATION_SELFIE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.selfie_bh, this);
                fileName = Constants.CLIENT_COORDINATION_SELFIE + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.NIGHT_CHECK_SELFIE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.selfie_bh, this);
                fileName = Constants.NC_SELFIE + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.OTHER_ACTIVITY:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.TASK_TABLE, this);
                fileName = Constants.OTHER_ACTIVITY + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.KIT_ITEM_REQUEST:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.KIT_ITEM_REQUEST, this);
                fileName = Constants.FILE_TYPE_KIT_ITEM_REQUEST + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.SETTINGS_AI_PIC:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.SETTINGS_AI_PIC, this);
                mReferenceId = appPreferences.getAppUserId();
                fileName = appPreferences.getAppUser() + "_" + mReferenceId + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = "AI NAME";
                break;

            case Util.KIT_ITEM_SIGNATURE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.KIT_ITEM_SIGNATURE, this);
                fileName = Constants.FILE_TYPE_KIT_ITEM_SIGNATURE + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.VEHICLE_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.VEHICLE_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_VEHICLE_REGISTER_CHECK + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.VISITOR_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.VISITOR_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_VISITOR_REGISTER_CHECK + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.MATERIAL_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.MATERIAL_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_MATERIAL_REGISTER_CHECK + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.MAINTENANCE_REGISTER_CHECK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.MAINTENANCE_REGISTER_CHECK, this);
                fileName = Constants.FILE_TYPE_MAINTENANCE_REGISTER_CHECK + "_" + mRotaTaskModel.getUnitId() + "_" + mRotaTaskModel.getTaskId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = mRotaTaskModel.getUnitName();
                mReferenceId = mRotaTaskModel.getTaskId();
                break;

            case Util.KIT_DISTRIBUTION_ITEM_IMAGE:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_IMAGE, this);
                fileName = Constants.FILE_TYPE_KIT_DISTRIBUTION_ITEM_IMAGE + "_" + unitId + "_" + kitDistributionId + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = unitName;
                mReferenceId = kitDistributionId;
                break;
            case Util.UNIT_RAISING_DETAILS:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING, this);
                fileName = Constants.FILE_TYPE_UNIT_RAISING_IMAGE + "_" + unitId + "_" + 0 + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = unitName;
                mReferenceId = 0;
                break;
            case Util.UNIT_RAISING_CANCELLED:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_CANCELLED, this);
                fileName = Constants.FILE_TYPE_UNIT_RAISING_CANCELLED_IMAGE + "_" + unitId + "_" + 0 + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = unitName;
                mReferenceId = 0;
                break;
            case Util.UNIT_RAISING_DEFERRED:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED, this);
                fileName = Constants.FILE_TYPE_UNIT_RAISING_DEFERRED_IMAGE + "_" + unitId + "_" + 0 + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = unitName;
                mReferenceId = 0;
                break;
            case Util.BARRACK:
                referenceTypeId = Util.getAttachmentSourceType(TableNameAndColumnStatement.BARRACK, this);
                fileName = Constants.BARRACK + "_" + barrackId + "_" + 0 + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
                stampName = unitName;
                mReferenceId = barrackId;
                break;
            default:
                break;
        }
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
        currentTakenImagePath = mediaStorageDir.getPath() + File.separator + fileName;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4; // 1/4 device compression
        options.outWidth = 500;
        options.outHeight = 500;

        Bitmap bitmap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (Build.MODEL.equalsIgnoreCase("XT1562")) {
                bitmap = BitmapFactory.decodeStream(is, null, options);
            } else {
                Bitmap bmp = BitmapFactory.decodeStream(is, null, options);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                if (bmp != null)
                    bmp.recycle();
                bmp = null;
            }
        } else {
            bitmap = BitmapFactory.decodeStream(is, null, options);
        }

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
            imagebytes = out.toByteArray();
            fos = new FileOutputStream(mediaFile);
            fos.write(out.toByteArray());
            fos.close();
        } catch (IOException e) {
            Crashlytics.logException(e);

        }
        return bitmap;
    }

    /**
     * @return text height
     */
    private float getTextHeight(String text, Paint paint) {

        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.height();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = true;
                    imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                    /**** EXTERNAL_CONTENT_URI : style URI for the "primary" external storage volume. ****/
                    // Standard Intent action that can be sent to have the camera
                    // application capture an image and return it.

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

                } else {
                    Toast.makeText(mContext, "Permission denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mContext)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

    }

    /**
     * Async task for loading the images from the SD card.
     *
     * @author Android Example
     */
    public class LoadImagesFromSDCard extends AsyncTask<String, Void, Boolean> {
        Bitmap mBitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showprogressbar((R.string.Loading_Image_Text));
        }

        protected Boolean doInBackground(String... urls) {
            boolean isError = false;
            Bitmap newBitmap = null;
            Uri uri = null;
            try {

                /**  Uri.withAppendedPath Method Description
                 * Parameters
                 *    baseUri  Uri to append path segment to
                 *    pathSegment  encoded path segment to append
                 * Returns
                 *    a new Uri based on baseUri with the given segment appended to the path
                 */
                uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + urls[0]);
                /********* Creates a new bitmap, scaled from an existing bitmap. ***********/
                newBitmap = compressImage(uri);
                if (newBitmap != null) {
                    mBitmap = newBitmap;
                } else {
                    isError = true;
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
                isError = true;
            }
            return isError;
        }

        @Override
        protected void onPostExecute(Boolean isError) {
            super.onPostExecute(isError);
            hideprogressbar();
            if (isError) {
                Util.showToast(CapturePictureActivity.this, getResources().getString(R.string.error_while_taking_image));
                return;
            } else {
                if (mBitmap != null) {
                    String size = android.text.format.Formatter.formatFileSize(mContext, imagebytes.length);
                    size = size.replaceAll("[^\\d.]", "");
                    int imagesize = (int) Math.round(Double.valueOf(size));
                    attachmentMetaDataModel.setAttachmentPath(currentTakenImagePath.trim());
                    if (fileName != null) {
                        attachmentMetaDataModel.setAttachmentFileName(fileName.trim());
                        attachmentMetaDataModel.setAttachmentTitle(fileName.trim());
                    }
                    attachmentMetaDataModel.setIsSave(0); // do not save file after uploading to server but can change the flag on receiving model in the respective class
                    attachmentMetaDataModel.setAttachmentFileExtension(".jpg");
                    attachmentMetaDataModel.setAttachmentTypeId(Constants.LOOKUP_TYPE_IMAGE);// picture
                    attachmentMetaDataModel.setAttachmentSourceId(mReferenceId);
                    attachmentMetaDataModel.setAttachmentFileSize(imagesize);
                    attachmentMetaDataModel.setAttachmentSourceTypeId(referenceTypeId);
                    if (mRotaTaskModel != null) {
                        attachmentMetaDataModel.setTaskId(mRotaTaskModel.getTaskId());
                        attachmentMetaDataModel.setUnitId(mRotaTaskModel.getUnitId());
                    } else {
                        attachmentMetaDataModel.setTaskId(unitTaskId);
                        attachmentMetaDataModel.setUnitId(unitId);
                    }
                    attachmentMetaDataModel.setSequenceNo(Util.getmSequenceNumber());
                    Util.addDataToMetaDataArray(attachmentMetaDataModel);

                    switch (Util.getSendPhotoImageTo()) {
                        case Util.REPLACE_KIT:
                            Intent kitreplacement = new Intent(CapturePictureActivity.this, KitReplacementDetailsActivity.class);
                            CapturePictureActivity.this.finish();
                            startActivity(kitreplacement);
                            break;

                        case Util.ADD_GREIVANCE:
                            Intent addGreivanceIntent = new Intent();
                            addGreivanceIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, addGreivanceIntent);
                            finish();
                            break;

                        case Util.POST_CHECK:

                            Intent intent = new Intent();
                            intent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_TWO, intent);
                            finish();
                            break;

                        case Util.SITE_CHECK:
                            Intent sitePostIntent = new Intent();
                            sitePostIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, sitePostIntent);
                            finish();
                            break;

                        case Util.GUARD:
                            Intent mGuardIntent = new Intent();
                            mGuardIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, mGuardIntent);
                            finish();
                            break;

                        case Util.CLIENT_REGISTER_CHECK:
                            Intent clientRegisterIntent = new Intent();
                            clientRegisterIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, clientRegisterIntent);
                            finish();
                            break;

                        case Util.DUTY_REGISTER_CHECK:
                            Intent dutyRegisterIntent = new Intent();
                            dutyRegisterIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, dutyRegisterIntent);
                            finish();
                            break;

                        case Util.GUARD_DETAIL_FULL_IMAGE:
                            Intent guardFullImage = new Intent();
                            attachmentMetaDataModel.setimageCode(CommonTags.GUARD_FULL_IMAGE_CODE);
                            guardFullImage.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, guardFullImage);
                            finish();
                            break;

                        case Util.GUARD_DETAIL_OVER_TURN_OUT:
                            Intent turnOutIntent = new Intent();
                            attachmentMetaDataModel.setimageCode(CommonTags.GUARD_TURN_OUT_IMAGE_CODE);
                            turnOutIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(Constants.NUMBER_ONE, turnOutIntent);
                            finish();
                            break;

                        case Util.BILL_SUBMISSION:
                            Intent billSubmission = new Intent();
                            billSubmission.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, billSubmission);
                            finish();
                            break;

                        case Util.BILL_COLLECTION:
                            Intent billCollection = new Intent();
                            billCollection.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, billCollection);
                            finish();
                            break;

                        case Util.SECURITY_RISK:
                            Intent securityrisk = new Intent();
                            securityrisk.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, securityrisk);
                            finish();
                            break;

                        case Util.CLIENT_COORDINATION:
                            Intent clientCoordination = new Intent();
                            clientCoordination.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, clientCoordination);
                            finish();
                            break;

                        case Util.CLIENT_COORDINATION_SELFIE:
                            Intent clientCoordinationSelfie = new Intent();
                            clientCoordinationSelfie.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, clientCoordinationSelfie);
                            finish();
                            break;

                        case Util.NIGHT_CHECK_SELFIE:
                            Intent ncSelfie = new Intent();
                            ncSelfie.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, ncSelfie);
                            finish();
                            break;

                        case Util.BARRACK_BEDDING:
                            Intent barrackBedding = new Intent();
                            barrackBedding.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackBedding);
                            finish();
                            break;

                        case Util.BARRACK_KIT:
                            Intent barrackKit = new Intent();
                            barrackKit.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackKit);
                            finish();
                            break;

                        case Util.BARRACK_MESS:
                            Intent barrackMess = new Intent();
                            barrackMess.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackMess);
                            finish();
                            break;

                        case Util.BARRACK_OTHERS:
                            Intent barrackOthers = new Intent();
                            barrackOthers.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackOthers);
                            finish();
                            break;

                        case Util.OTHER_ACTIVITY:
                            Intent otherActivity = new Intent();
                            otherActivity.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, otherActivity);
                            finish();
                            break;

                        case Util.KIT_ITEM_REQUEST:
                            Intent kitReplaceIntent = new Intent();
                            kitReplaceIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, kitReplaceIntent);
                            finish();
                            break;

                        case Util.SETTINGS_AI_PIC:
                            Intent settingsIntent = new Intent();
                            settingsIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, settingsIntent);
                            finish();
                            break;

                        case Util.VEHICLE_REGISTER_CHECK:
                            Intent vehicleIntent = new Intent();
                            vehicleIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, vehicleIntent);
                            finish();
                            break;

                        case Util.VISITOR_REGISTER_CHECK:
                            Intent visitorIntent = new Intent();
                            visitorIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, visitorIntent);
                            finish();
                            break;

                        case Util.MAINTENANCE_REGISTER_CHECK:
                            Intent maintenanceIntent = new Intent();
                            maintenanceIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, maintenanceIntent);
                            finish();
                            break;

                        case Util.MATERIAL_REGISTER_CHECK:
                            Intent materialIntent = new Intent();
                            materialIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, materialIntent);
                            finish();
                            break;

                        case Util.KIT_DISTRIBUTION_ITEM_IMAGE:
                            attachmentMetaDataModel.setTaskId(0);

                            Intent distributionItemIntent = new Intent();
                            distributionItemIntent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, distributionItemIntent);
                            finish();
                            break;
                        case Util.BARRACK_CUSTODIAN:
                            Intent barrackCustodian = new Intent();
                            barrackCustodian.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackCustodian);
                            finish();
                            break;
                        case Util.BARRACK_LANDLORD:
                            Intent barrackLandlord = new Intent();
                            barrackLandlord.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackLandlord);
                            finish();
                            break;
                        case Util.UNIT_RAISING_DETAILS:
                            Intent unitRaising = new Intent();
                            unitRaising.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, unitRaising);
                            finish();
                            break;
                        case Util.UNIT_RAISING_DEFERRED:
                            Intent unitRaisingDeferred = new Intent();
                            unitRaisingDeferred.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, unitRaisingDeferred);
                            finish();
                            break;
                        case Util.UNIT_RAISING_CANCELLED:
                            Intent unitRaisingCancelled = new Intent();
                            unitRaisingCancelled.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, unitRaisingCancelled);
                            finish();
                            break;
                        case Util.BARRACK:
                            Intent barrackPost = new Intent();
                            barrackPost.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                            setResult(RESULT_OK, barrackPost);
                            finish();
                            break;
                        default:
                            break;
                    }
                }
            }
            deleteImages();
        }
    }
}
