package com.sisindia.ai.android.commons;

/**
 * Created by Durga Prasad on 21-03-2016.
 */
public interface ToolbarTitleUpdateListener {
    void changeToolbarTitle(String title);
}
