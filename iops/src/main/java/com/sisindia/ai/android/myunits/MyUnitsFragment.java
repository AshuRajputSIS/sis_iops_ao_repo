package com.sisindia.ai.android.myunits;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.home.RotaUnitSyncListener;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.manualrefresh.ManualUnitRotaSync;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;

import java.util.ArrayList;


public class MyUnitsFragment extends Fragment implements ExpandableListView.OnGroupCollapseListener,
        ExpandableListView.OnGroupExpandListener, ExpandableListView.OnGroupClickListener,
        ExpandableListView.OnChildClickListener, RotaUnitSyncListener,UpdateMyUnitsFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String UNIT_ID = "unit_id";
    int previousPosition = -1;
    public static final String KEY_UNIT_FILTER_IDS="unit_filter_ids";
    private String mParam1;
    private String mUnitFilterIds;
    private ExpandableListView expandableListview;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private FragmentManager fragmentManager;
    private ArrayList<MyUnitHomeMO> myUnitNames;
    private Toolbar mToolbar;
    private SISClient sisClient;
    private RotaUnitSyncListener rotaUnitSyncListener;
    protected static UpdateMyUnitsFragment mUpdateFragmentListner;
    private AppPreferences mAppPreferences;
    private MyunitExpandableListviewAdapter myunitExpandableListviewAdapter;


    public MyUnitsFragment() {
        // Required empty public constructor
    }

    public static MyUnitsFragment newInstance(String param1, String param2) {
        MyUnitsFragment fragment = new MyUnitsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//for enable the menu in fragment
        mAppPreferences = new AppPreferences(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            //mUnitFilterIds = getArguments().getString(ARG_PARAM2);
        }
        mUnitFilterIds = mAppPreferences.getFilterIds(); // filter units
        mToolbar = ((HomeActivity) getActivity()).toolbar;
        rotaUnitSyncListener = this;
        mUpdateFragmentListner=this;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.myunits_homepage, container, false);
        expandableListview = (ExpandableListView)
                view.findViewById(R.id.myunit_homepage_expandableListview);
        expandableListview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        myUnitNames = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().
                getUnitDetails(0);
       /* ArrayList<UnitBoundary> ub = myUnitNames.get(0).getUnitBoundaries();
        String ubs = myUnitNames.get(0).getUnitBoundariesString();*/
        myunitExpandableListviewAdapter = new MyunitExpandableListviewAdapter(getActivity());
        myunitExpandableListviewAdapter.setMyUnitHomeData(myUnitNames);
        expandableListview.setAdapter(myunitExpandableListviewAdapter);
        //view.setOnClickListener(this);

        expandableListview.setOnGroupExpandListener(this);
        expandableListview.setOnGroupCollapseListener(this);
        sisClient = new SISClient(getActivity());
        unitFilterUpdate();
        return view;
    }

    private void setUnitData() {
        if (myunitExpandableListviewAdapter != null) {
            myunitExpandableListviewAdapter.setMyUnitHomeData(myUnitNames);
            myunitExpandableListviewAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }


    @Override
    public void onGroupCollapse(int groupPosition) {

    }

    @Override
    public void onGroupExpand(int groupPosition) {

        if (groupPosition != previousPosition) {
            expandableListview.collapseGroup(previousPosition);
            previousPosition = groupPosition;
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent singleUnitIntent = new Intent(getActivity(), SingleUnitActivity.class);
        singleUnitIntent.putExtra(UNIT_ID,myUnitNames.get(groupPosition).getUnitId());
        startActivity(singleUnitIntent);
        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        Intent singleUnitIntent = new Intent(getActivity(), SingleUnitActivity.class);
        singleUnitIntent.putExtra(UNIT_ID,myUnitNames.get(groupPosition).getUnitId());
        startActivity(singleUnitIntent);
        return true;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_myunits_homepage, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.filter) {
            Intent filterUnitsIntent = new Intent(getActivity(), FilterUnitsActivity.class);
            startActivity(filterUnitsIntent);
            return true;
        } else if (id == android.R.id.home) {
            fragmentManager.popBackStack();
        } else if (id == R.id.syncUnit) {
            if (NetworkUtil.isConnected) {
                ManualUnitRotaSync.getUnitApiCall(rotaUnitSyncListener,getActivity(),isVisible());
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void rotaUnitDbSyncing(boolean isSyned) {
        if(getActivity() != null) {
            if (isSyned) {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_str));
                myUnitNames.clear();
                myUnitNames = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().
                        getUnitDetails(0);
                setUnitData();
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_error_str));
            }
        }
    }


    @Override
    public void updateUnitsFragment() {
        if(myunitExpandableListviewAdapter!=null){
            myunitExpandableListviewAdapter.notifyDataSetChanged();
        }
    }

    /**
     * updating the adapter according to the UnitFilter Selection
     */

    private void unitFilterUpdate() {
        if(null!=mUnitFilterIds){
            myUnitNames.clear();
            myUnitNames = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance()
                    .getFilterUnitDetails(mUnitFilterIds);
            setUnitData();
        }
    }
}
