package com.sisindia.ai.android.mybarrack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.sisindia.ai.android.mybarrack.modelObjects.BarrackStrengthMO;


/**
 * Created by shruti on 27/7/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    private int taskId;
    private int barrackId;
    private int unitId;
    public String barrackName="";
    int mNumOfTabs;
    BarrackStrengthMO barrackStrengthMO;
    private Bundle myBundle;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, BarrackStrengthMO barrackStrengthMO,
                        String barrackname, int unitid, int barrackid, int taskid) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.barrackStrengthMO= barrackStrengthMO;
        this.barrackName =barrackname;
        this.unitId=unitid;
        this.barrackId =barrackid;
        this.taskId=taskid;
    }



    public PagerAdapter(FragmentManager supportFragmentManager, int tabCount, String barrackName,
                        int unitId,int barrackid,int taskid) {
        super(supportFragmentManager);
        this.mNumOfTabs = tabCount;
        this.barrackName =barrackName;
        this.unitId=unitId;
        this.barrackId =barrackid;
        this.taskId=taskid;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabStrengthFragment tab1 = new TabStrengthFragment();
                 myBundle = new Bundle();
                myBundle.putSerializable("barrackStrengthMO", barrackStrengthMO);
                myBundle.putString("barrackName",barrackName);
                myBundle.putInt("unitId",unitId);
                myBundle.putInt("barrackId",barrackId);
                tab1.setArguments( myBundle);
                return tab1;
            case 1:
                TabSpaceFragment tab2 = new TabSpaceFragment();
                /* myBundle = new Bundle();
                myBundle.putSerializable("barrackStrengthMO", barrackStrengthMO);
                myBundle.putString("barrackName",barrackName);
                myBundle.putInt("unitId",unitId);
                myBundle.putInt("barrackId",barrackId);
                tab1.setArguments( myBundle);*/

                return tab2;
            case 2:
                TabOthersFragments tab3 = new TabOthersFragments();
                 myBundle = new Bundle();
                myBundle.putSerializable("barrackStrengthMO", barrackStrengthMO);
                myBundle.putString("barrackName",barrackName);
                myBundle.putInt("unitId",unitId);
                myBundle.putInt("barrackId",barrackId);
                myBundle.putInt("taskId",taskId);


                tab3.setArguments( myBundle);
                return tab3;
            case 3:
                TabMetLandlordFragment tab4  = new TabMetLandlordFragment();
               /* myBundle = new Bundle();
                myBundle.putSerializable("barrackStrengthMO", barrackStrengthMO);
                myBundle.putString("barrackName",barrackName);
                myBundle.putInt("unitId",unitId);
                myBundle.putInt("barrackId",barrackId);
                myBundle.putInt("taskId",taskId);

                tab4.setArguments( myBundle);*/
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
