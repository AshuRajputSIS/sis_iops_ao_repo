package com.sisindia.ai.android.fcmnotification.fcmunits;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.UnitContacts;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMUnitContactModelInsertion extends SISAITrackingDB {

    private SQLiteDatabase sqlite=null;
    private boolean isInsertedSuccessfully;

    public FCMUnitContactModelInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertUnitContact(UnitContacts unitContactList) {
        sqlite = this.getWritableDatabase();
        synchronized (sqlite) {
            try {
                if(unitContactList != null ) {
                    sqlite.beginTransaction();
                    insertUnitContactModel(unitContactList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                }else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();

            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertUnitContactModel(UnitContacts unitContact) {

        ContentValues values = new ContentValues();

            values.put(TableNameAndColumnStatement.IS_NEW, 0);
            values.put(TableNameAndColumnStatement.IS_SYNCED, 1);
            values.put(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                    unitContact.getId());
            values.put(TableNameAndColumnStatement.FIRST_NAME, unitContact.getFirstName());
            values.put(TableNameAndColumnStatement.LAST_NAME, unitContact.getLastName());
            values.put(TableNameAndColumnStatement.EMAIL_ADDRESS, unitContact.getEmailId());
            values.put(TableNameAndColumnStatement.PHONE_NO, unitContact.getContactNo());
            values.put(TableNameAndColumnStatement.UNIT_ID,
                    unitContact.getUnitId());
            values.put(TableNameAndColumnStatement.DESIGNATION, unitContact.getDesignation());
            GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().
                    getSelectionStatementDBInstance().getTableSeqId(TableNameAndColumnStatement.UNIT_CONTACT_ID,
                        unitContact.getId(), TableNameAndColumnStatement.ID
                        , TableNameAndColumnStatement.UNIT_CONTACT_TABLE, TableNameAndColumnStatement.ID, sqlite);

            if (genericUpdateTableMO.getIsAvailable() != 0) {
                IOPSApplication.getInstance().getDeletionStatementDBInstance().
                        deleteTableById(TableNameAndColumnStatement.UNIT_CONTACT_TABLE,
                                genericUpdateTableMO.getId(),sqlite);
                sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
            } else {
                sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_CONTACT_TABLE, null, values);
            }

    }
}
