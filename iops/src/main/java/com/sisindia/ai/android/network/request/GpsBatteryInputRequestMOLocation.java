package com.sisindia.ai.android.network.request;

import java.io.Serializable;

public class GpsBatteryInputRequestMOLocation implements Serializable {
    private static final long serialVersionUID = -4891087159649946053L;
    private String CapturedDateTime;
    private String BatteryPercentage;
    private String GEOLocation;

    public String getCapturedDateTime() {
        return this.CapturedDateTime;
    }

    public void setCapturedDateTime(String CapturedDateTime) {
        this.CapturedDateTime = CapturedDateTime;
    }

    public String getBatteryPercentage() {
        return this.BatteryPercentage;
    }

    public void setBatteryPercentage(String BatteryPercentage) {
        this.BatteryPercentage = BatteryPercentage;
    }

    public String getGEOLocation() {
        return this.GEOLocation;
    }

    public void setGEOLocation(String GEOLocation) {
        this.GEOLocation = GEOLocation;
    }
}
