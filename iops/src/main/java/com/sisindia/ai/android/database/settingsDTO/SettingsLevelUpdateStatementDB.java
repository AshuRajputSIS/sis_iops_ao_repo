package com.sisindia.ai.android.database.settingsDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.rota.daycheck.EmployeeAuthorizedStrengthMO;

import java.util.List;

import timber.log.Timber;

/**
 * Created by shankar on 27/6/16.
 */

public class SettingsLevelUpdateStatementDB extends SISAITrackingDB {

    public SettingsLevelUpdateStatementDB(Context context) {
        super(context);
    }

    /**
     *
     * @param appUserId
     * @param currentLatitude
     * @param currentLongitute
     * @param alternateNumber
     */
    public void updateAppUserTable(int appUserId, String currentLatitude, String currentLongitute, String alternateNumber,String imagePath,String address) {

        SQLiteDatabase sqlite = null;
        String whereClause = TableNameAndColumnStatement.EMPLOYEE_ID + " = ?";
        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.GEO_LATITUDE,currentLatitude);
            contentValues.put(TableNameAndColumnStatement.GEO_LONGITUDE,currentLongitute);
            contentValues.put(TableNameAndColumnStatement.ALTERNATE_CONTACT_NO,alternateNumber);
            contentValues.put(TableNameAndColumnStatement.AI_PHOTO,imagePath);
            contentValues.put(TableNameAndColumnStatement.FULL_ADDRESS,address);
            sqlite.update(TableNameAndColumnStatement.APP_USER_TABLE,
                    contentValues, whereClause,
                    new String[]{" " + appUserId});


        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }  finally {
            if(null!=sqlite && sqlite.isOpen()){
                sqlite.close();
            }
        }
    }
    public void updatePrimaryAddressId(int id, int primaryAddressId) {

        String updateQuery = "UPDATE " + TableNameAndColumnStatement.APP_USER_TABLE +
                " SET " + TableNameAndColumnStatement.PRIMARY_ADDRESS_ID + " = " + "'" +primaryAddressId + "'" +
                " WHERE " +
                TableNameAndColumnStatement.ID + " = " + "'" +id + "'" ;

        Timber.d("updatePrimaryAddress  %s", updateQuery);
        Cursor cursor = null;
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            cursor = sqlite.rawQuery(updateQuery, null);
            Timber.d("updatePrimaryAddress QUERY  %s", cursor.getCount());
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }

    }


}
