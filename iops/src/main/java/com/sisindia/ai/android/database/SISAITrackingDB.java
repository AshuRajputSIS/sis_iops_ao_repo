package com.sisindia.ai.android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import timber.log.Timber;

/**
 * Created by shankar on 28/3/16.
 */
public class SISAITrackingDB extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "sisaitracking.db";
    private final TableStatement tableStatement = new TableStatement();
//    DatabaseUpgadeQueries databaseUpgadeQueries;

    public SISAITrackingDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        databaseUpgadeQueries = new DatabaseUpgadeQueries();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tableStatement.appUserTable());
        db.execSQL(tableStatement.areaTable());
        db.execSQL(tableStatement.attachmentMetadataTable());
        db.execSQL(tableStatement.branchTable());
        db.execSQL(tableStatement.gpsBatteryHealthTable());
        db.execSQL(tableStatement.holidayCalendarTable());
        db.execSQL(tableStatement.improvementPlanTable());
        db.execSQL(tableStatement.leaveCalendarTable());
        db.execSQL(tableStatement.messVendorTable());
        db.execSQL(tableStatement.recruitmentTable());
        db.execSQL(tableStatement.unitRiskTable());
        db.execSQL(tableStatement.unitRiskActionPlanTable());
        db.execSQL(tableStatement.rotaTable());
        db.execSQL(tableStatement.rotaTaskTable());
        db.execSQL(tableStatement.taskTable());
        db.execSQL(tableStatement.unitTable());
        db.execSQL(tableStatement.unitBarrackTable());
        db.execSQL(tableStatement.unitBarrackStrength());
        db.execSQL(tableStatement.unitcontactTable());
        db.execSQL(tableStatement.unitemployeeTable());
        db.execSQL(tableStatement.unitequipmentTable());
        db.execSQL(tableStatement.unitPostTable());
        db.execSQL(tableStatement.barrackTable());
        db.execSQL(tableStatement.customerTable());
        db.execSQL(tableStatement.RotaTaskActivityList());
        db.execSQL(tableStatement.RankMasterTable());
        db.execSQL(tableStatement.UnitAuthorizedStrengthTable());
        db.execSQL(tableStatement.securityRiskTable());
        db.execSQL(tableStatement.securityRiskOptionTable());
        db.execSQL(tableStatement.myPerformanceTable());
        db.execSQL(tableStatement.billCollactionApiTable());
        db.execSQL(tableStatement.LookUpModelTable());
        db.execSQL(tableStatement.clientCoordinationTable());
        db.execSQL(tableStatement.barrackInspectionQuestionTable());
        db.execSQL(tableStatement.barrackInspectionAnswerTable());
        db.execSQL(tableStatement.issuesTable());
        db.execSQL(tableStatement.masterActionPlan());
        db.execSQL(tableStatement.issueMatrixTable());
        db.execSQL(tableStatement.kitItemTable());
        db.execSQL(tableStatement.kitItemRequestTable());
        db.execSQL(tableStatement.activityLogTable());
        db.execSQL(tableStatement.kitSizeTable());
        db.execSQL(tableStatement.kitDistributionTable());
        db.execSQL(tableStatement.kitDistributionItemTable());
        db.execSQL(tableStatement.kitItemSizeTable());
        db.execSQL(tableStatement.notificationTable());
        db.execSQL(tableStatement.dutyAttendanceTable());
        db.execSQL(tableStatement.equipmentListTable());
        //-------------Adding below tables: earlier added DBUpdateQueries
        db.execSQL(tableStatement.dependentSyncTable());
        db.execSQL(tableStatement.notificationListTable());
        db.execSQL(tableStatement.syncRequest());
        db.execSQL(tableStatement.unitRaising());
        db.execSQL(tableStatement.unitRaisingDeferred());
        db.execSQL(tableStatement.unitRaisingCancellation());
        db.execSQL(tableStatement.getUnitRaisingStrengthDetail());
        db.execSQL(tableStatement.queryExecutorTable());
        db.execSQL(tableStatement.guardTable());
        db.execSQL(tableStatement.unitBillingCheckListTable());

        //ADDING EXTRA TABLE ['UNIT_TYPE']
        db.execSQL(tableStatement.unitTypeTable());

//        databaseUpgadeQueries.onCreateUpgrade(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Timber.d("SQLiteDatabase onUpgrade old Db version %s ", oldVersion + " NewVer - " + newVersion);
        /*switch (oldVersion) {
        }*/
    }

}