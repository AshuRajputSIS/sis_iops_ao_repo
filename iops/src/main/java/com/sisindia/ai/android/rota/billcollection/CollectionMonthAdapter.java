package com.sisindia.ai.android.rota.billcollection;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.text.DateFormatSymbols;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shankar on 19/8/16.
 */

public class CollectionMonthAdapter extends BaseAdapter {

    private final List<BillCollactionMonthMO> collectionMonthList;

    private Context context;
    private LayoutInflater layoutInflater;



    public CollectionMonthAdapter(Context addRotaTaskActivity, List<BillCollactionMonthMO> barrackList) {
        this.context = addRotaTaskActivity;
        this.collectionMonthList = barrackList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return collectionMonthList.size();
    }

    @Override
    public BillCollactionMonthMO getItem(int position) {
        return collectionMonthList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.bill_collection_spinner_item, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews(getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(BillCollactionMonthMO billCollactionMonthMO, ViewHolder holder) {
        //TODO implement
        String month_year = getMonth(billCollactionMonthMO.getBillMonth()) + "-" + billCollactionMonthMO.getBillYear();
        holder.collectionMonth.setText(month_year);
        holder.collectionMonth.setGravity(Gravity.CENTER);
    }
    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    static class ViewHolder {
        @Bind(R.id.tv_collection_month)
        CustomFontTextview collectionMonth;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
