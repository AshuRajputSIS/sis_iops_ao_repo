package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 12-05-2016.
 */
public class LastCheckInspectionImprovementpalnMO {

    private String improvementPlanDate;
    private String additionalNotes;

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public String getImprovementPlanDate() {
        return improvementPlanDate;
    }

    public void setImprovementPlanDate(String improvementPlanDate) {
        this.improvementPlanDate = improvementPlanDate;
    }
}
