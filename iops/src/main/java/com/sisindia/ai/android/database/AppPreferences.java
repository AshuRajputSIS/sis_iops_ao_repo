/*
 * Copyright (C) 2014 Mara online.
 */
package com.sisindia.ai.android.database;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.LocationManager;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.SyncedTableDetails;
import com.sisindia.ai.android.commons.UpdatedConfigurationApiCalls;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.rota.daycheck.AddKitRequestMo;
import com.sisindia.ai.android.rota.daycheck.CheckedGuardDetails;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.syncadpter.DependentSyncFlowBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AppPreferences {
    /* Shared preference keys */
    public static final String BATTERY_PROGRESS__KEY = "batteryProgress";
    public static final String LANGUAGE_SELECTION = "language_selection";
    public static final String LANGUAGE_SET = "language_set";
    public static final String BATTERY_LEVEL = "BatteryLevel";
    public static final String DEVCIE_IMEI_ID = "device_imei";
    public static final String TAKEN_PIC_SNO = "pic_sno";
    public static final String TAKEN_PIC_PATHS = "pic_paths";
    private static final String PREF_KEY_LASTTIME = "last_time_seen";
    private static final String LOGIN_STATUS = "loginstatus";
    private static final String GRIEVANCE_DATA = "grievanceData";
    private static final String COMPLAINT_DATA = "compliantData";
    private static final String FILTER_IDS = "filter_ids";
    private static final String ROTA_TASK_JSON_DATA = "rota_task_json_data";
    private static final String UNIT_JSON_DATA = "unit_json_data";
    private static final String CHECKED_GUARDS_MAP = "checked_guard_map";
    private static final String SYNCED_HASH_MAP_TABLES = "synced_hash_map_table";
    private static final String SET_ALARM_CONFIGURED = "alarm_configured";
    private static final String ALARM_TRIGGER_COUNT = "alarm_trigger_count";
    private static final String SET_UPDATED_CONFIGURATION_API_CALL = "update_configuration_api";
    private static final String DEPEND_SYNC_STATUS = "depend_sync_status";
    private static final String SCHEDULE_DELETED = "schedule_delete";
    private static final String SET_ALARM_CONFIGURED_FOR_ATTENDANCE_SYNC = "alarm_configured_for_attendance_sync";
    private static final String ATTACHMENT_IDS = "attachment_ids";
    private static final String TASK_EXECUTION_RESULT = "task_execution_result";

    private String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();
    private static SharedPreferences sharedPrefs;
    private Editor prefsEditor;
    private static Context mcontext;
    private int languageId;


    public AppPreferences(Context context) {
        this.sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefsEditor = sharedPrefs.edit();
        this.mcontext = context;

    }

    public boolean getloginstatus() {
        return sharedPrefs.getBoolean(LOGIN_STATUS, false);
    }

    public void saveloginstatus(boolean status) {
        prefsEditor.putBoolean(LOGIN_STATUS, status);
        prefsEditor.apply();
    }

    public boolean getDutyStatus() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.duty_status, false);
    }

    public void saveDutyStatus(boolean status) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.duty_status, status);
        prefsEditor.apply();
    }

    public void saveForcedGrievanceFlag(boolean status) {
        prefsEditor.putBoolean("ForcedGrievance", status).apply();
    }

    public boolean getForcedGrievanceFlag() {
        return sharedPrefs.getBoolean("ForcedGrievance", false);
    }


   /* public boolean getDutyStatusApiCall() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.duty_status_API, false);
    }

    public void saveDutyStatusApiCall(boolean status) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.duty_status_API, status);
        prefsEditor.apply();
    }*/

    public String getIMEIsim1() {
        return sharedPrefs.getString(TableNameAndColumnStatement.imei_one, "");
    }

    public void saveIMEIsim1(String imeiOne) {
        prefsEditor.putString(TableNameAndColumnStatement.imei_one, imeiOne);
        prefsEditor.apply();
    }

    public String getIMEIsim2() {
        return sharedPrefs.getString(TableNameAndColumnStatement.imei_two, "");
    }

    public void saveIMEIsim2(String imeiTwo) {
        prefsEditor.putString(TableNameAndColumnStatement.imei_two, imeiTwo);
        prefsEditor.apply();
    }

    public void clear() {
        prefsEditor.clear();
        prefsEditor.apply();
    }


    public long getLastSeen() {
        return sharedPrefs.getLong(PREF_KEY_LASTTIME, -1);
    }

    public void setLastSeen(long time) {
        prefsEditor.putLong(PREF_KEY_LASTTIME, time);
        prefsEditor.apply();
    }

    public int getBatteryProgress() {
        int progress = sharedPrefs.getInt(BATTERY_PROGRESS__KEY, 0);
        return progress;
    }

    public void setBatteryProgress(int progress) {
        prefsEditor.putInt(BATTERY_PROGRESS__KEY, progress);
        prefsEditor.apply();
    }

    public void setLanguage(String languageId) {
        prefsEditor.putString(LANGUAGE_SELECTION, languageId);
        prefsEditor.apply();
    }

    public String getLanguage(Context context) {
        return sharedPrefs.getString(LANGUAGE_SELECTION, "en");
    }

    public void setSelectedAppLanguage(Context context, boolean selectedFlag) {
        prefsEditor.putBoolean(LANGUAGE_SET, selectedFlag);
        prefsEditor.apply();
    }

    public boolean isSetSelectedLanguage(Context context) {
        return sharedPrefs.getBoolean(LANGUAGE_SET, false);
    }


    public void setDeviceId(Context context, String deviceId) {
        prefsEditor.putString(DEVCIE_IMEI_ID, deviceId);
        prefsEditor.apply();
    }

    public String getDeviceId(Context context) {
        return sharedPrefs.getString(DEVCIE_IMEI_ID, "");
    }

    public int getTakenPicSno() {
        return sharedPrefs.getInt(TAKEN_PIC_SNO, 0);
    }

    public void setTakenPicSno(int picSno) {
        prefsEditor.putInt(TAKEN_PIC_SNO, picSno);
        prefsEditor.apply();
    }

    public List<String> getTakenImagePaths() {
        Set<String> setImagePaths = sharedPrefs.getStringSet(TAKEN_PIC_PATHS, new HashSet<String>());
        ArrayList<String> imagePaths = null;
        if (setImagePaths != null)
            imagePaths = new ArrayList<>(setImagePaths);
        return imagePaths;
    }

    public void setTakenImagePaths(Set<String> imagePaths) {
        prefsEditor.putStringSet(TAKEN_PIC_PATHS, imagePaths);
        prefsEditor.apply();
    }

    public String getAccessToken() {
        return sharedPrefs.getString(TableNameAndColumnStatement.access_token, "");
    }

    public void saveAccessToken(String acesstoken) {
        prefsEditor.putString(TableNameAndColumnStatement.access_token, acesstoken);
        prefsEditor.apply();
    }

    public int getSaveAppVersion() {
        return sharedPrefs.getInt(TableNameAndColumnStatement.AppVersion, 0);
    }

    public void saveAppVersion(int AppVersion) {
        prefsEditor.putInt(TableNameAndColumnStatement.AppVersion, AppVersion);
        prefsEditor.apply();
    }

    public String getToken_type() {
        return sharedPrefs.getString(TableNameAndColumnStatement.token_type, "");
    }

    public void saveToken_type(String token_type) {
        prefsEditor.putString(TableNameAndColumnStatement.token_type, token_type);
        prefsEditor.apply();
    }

    public String getMobileNumber() {
        return sharedPrefs.getString(TableNameAndColumnStatement.otp, "");
    }

    public void saveMobileNumber(String mobileNumber) {
        prefsEditor.putString(TableNameAndColumnStatement.otp, mobileNumber);
        prefsEditor.apply();
    }

    public boolean getLoadConfigurationFlag() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.loadConfigurationDone, false);
    }

    public void setLoadConfigurationFlag(boolean isCompleted) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.loadConfigurationDone, isCompleted);
        prefsEditor.apply();
    }

    public String getEquipmentList() {
        return sharedPrefs.getString(TableNameAndColumnStatement.equipmentList, "");
    }

    public void setEquipmentList(String equipmentList) {
        prefsEditor.putString(TableNameAndColumnStatement.equipmentList, equipmentList);
        prefsEditor.apply();
    }

    public boolean getTaskStatus() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.taskStatus, false);
    }

    public void setTaskStatus(boolean status) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.taskStatus, status);
        prefsEditor.apply();
    }

    public String getOtp() {
        return sharedPrefs.getString(TableNameAndColumnStatement.taskStatus, "");
    }

    public void setOtp(String status) {
        prefsEditor.putString(TableNameAndColumnStatement.taskStatus, status);
        prefsEditor.apply();
    }

    static String getCommonImagePath() {
        return sharedPrefs.getString(TableNameAndColumnStatement.commonImagePath, "");
    }

    public void setCommonImagePath(String commonStoragePath) {
        prefsEditor.putString(TableNameAndColumnStatement.commonImagePath, commonStoragePath);
        prefsEditor.apply();
    }

    public String getAppUser() {
        return sharedPrefs.getString(TableNameAndColumnStatement.appUser, TableNameAndColumnStatement.NOT_AVAILABLE);
    }

    public void setAppUser(String appUserName) {
        prefsEditor.putString(TableNameAndColumnStatement.appUser, appUserName);
        prefsEditor.apply();
    }

    public int getAppUserId() {
        return sharedPrefs.getInt(TableNameAndColumnStatement.appUserID, mcontext.getResources().getInteger(R.integer.number_0));
    }

    public void setAppUserId(int appUserId) {
        prefsEditor.putInt(TableNameAndColumnStatement.appUserID, appUserId);
        prefsEditor.apply();
    }


    public void setFirstTimeLaunched(boolean isLaunched) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.isFirstTime, isLaunched);
        prefsEditor.apply();
    }

    public boolean isFirstTimeLaunched() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.isFirstTime, false);
    }

    public void setComplaintData(String data) {
        prefsEditor.putString(COMPLAINT_DATA, data);
        prefsEditor.apply();
    }

    public void setOnBoardingStatus(String moduleNameKey, boolean value) {
        prefsEditor.putBoolean(moduleNameKey, value).apply();
    }

    public void setSplashScreenFlag(boolean flag) {
        prefsEditor.putBoolean(TableNameAndColumnStatement.splashScreenFlag, flag);
        prefsEditor.apply();
    }

    public boolean getSplashScreenFlag() {
        return sharedPrefs.getBoolean(TableNameAndColumnStatement.splashScreenFlag, false);
    }

    public boolean getOnBoardingStatus(String moduleNameKey) {
        return sharedPrefs.getBoolean(moduleNameKey, false);
    }

    public void setLanguageId(int languageId) {
        prefsEditor.putInt(mcontext.getResources().getString(R.string.languageId), languageId);
        prefsEditor.apply();
    }

    public int getLanguageId() {
        return sharedPrefs.getInt(mcontext.getResources().getString(R.string.languageId), 0);
    }

    public void setFCMRegToken(String fcmRegToken) {
        prefsEditor.putString("regId", fcmRegToken);
        prefsEditor.apply();

    }

    public void setFilterIds(String ids) {
        prefsEditor.putString(FILTER_IDS, ids);
        prefsEditor.apply();
    }

    public String getFilterIds() {
        return sharedPrefs.getString(FILTER_IDS, null);
    }

    public String getFCMRegToken() {
        return sharedPrefs.getString("regId", "");
    }

    public void setIssueType(int issueType) {
        prefsEditor.putInt(mcontext.getResources().getString(R.string.issue_type), issueType);
        prefsEditor.apply();
    }

    public int getIssueType() {
        return sharedPrefs.getInt(mcontext.getResources().getString(R.string.issue_type), 0);
    }

    public void setAddKitRequest(String guardCode, AddKitRequestMo addKitRequestMo) {
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(guardCode)) {
            String jsondata = gson.toJson(addKitRequestMo);
            prefsEditor.putString(guardCode, jsondata);
            prefsEditor.apply();
        }
    }

    public AddKitRequestMo getAddKitRequest(String guardCode, int taskId) {
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString(guardCode, null);
        return gson.fromJson(jsonData, AddKitRequestMo.class);
    }

    public void setCheckedGuardDetails(CheckedGuardDetails checkedGuardDetails) {
        Gson gson = new Gson();
        String jsondata = gson.toJson(checkedGuardDetails);
        prefsEditor.putString(CHECKED_GUARDS_MAP, jsondata);
        prefsEditor.apply();
    }

    public CheckedGuardDetails getCheckedGuardDetails() {
        Gson gson = new Gson();
        CheckedGuardDetails checkedGuardDetails;
        String jsonData = sharedPrefs.getString(CHECKED_GUARDS_MAP, null);
        checkedGuardDetails = gson.fromJson(jsonData, CheckedGuardDetails.class);
        if (checkedGuardDetails == null) {
            checkedGuardDetails = new CheckedGuardDetails();
            checkedGuardDetails.checkedGuardDetailsMap = new HashMap<>();
        }
        return checkedGuardDetails;
    }

    public void setRotaTaskJsonData(Object rotaTaskModel) {
        String jsondata = IOPSApplication.getGsonInstance().toJson(rotaTaskModel);
        prefsEditor.putString(AppPreferences.ROTA_TASK_JSON_DATA, jsondata);
        prefsEditor.apply();
    }

    public RotaTaskModel getRotaTaskJsonData() {
        return IOPSApplication.getGsonInstance().fromJson(
                sharedPrefs.getString(AppPreferences.ROTA_TASK_JSON_DATA, null), RotaTaskModel.class);
    }

    public void setUnitJsonData(Object myUnitHome) {
        String jsondata = IOPSApplication.getGsonInstance().toJson(myUnitHome);
        prefsEditor.putString(AppPreferences.UNIT_JSON_DATA, jsondata);
        prefsEditor.apply();
    }

    public MyUnitHomeMO getUnitJsonData() {
        return IOPSApplication.getGsonInstance().fromJson(
                sharedPrefs.getString(AppPreferences.UNIT_JSON_DATA, null), MyUnitHomeMO.class);
    }

    public boolean isEnableGps(Context context) {
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void setFirstTimeAlarmConfigured(boolean isConfigured) {
        prefsEditor.putBoolean(SET_ALARM_CONFIGURED, isConfigured);
        prefsEditor.apply();
    }

    public boolean isFirstTimeAlarmConfigured() {
        return sharedPrefs.getBoolean(SET_ALARM_CONFIGURED, false);
    }


    public void setFirstTimeAlarmConfiguredForAttendance(boolean isConfigured) {
        prefsEditor.putBoolean(SET_ALARM_CONFIGURED_FOR_ATTENDANCE_SYNC, isConfigured);
        prefsEditor.apply();
    }

    public boolean isFirstTimeAlarmConfiguredForAttendance() {
        return sharedPrefs.getBoolean(SET_ALARM_CONFIGURED_FOR_ATTENDANCE_SYNC, false);
    }

    public void setAlarmTriggeredCount(int alarmTriggeredCount) {
        prefsEditor.putInt(ALARM_TRIGGER_COUNT, alarmTriggeredCount);
        prefsEditor.apply();
    }

    public int getAlarmTriggeredCount() {
        return sharedPrefs.getInt(ALARM_TRIGGER_COUNT, 0);
    }

    public void setSyncedHashMaptables(SyncedTableDetails syncedHashMaptables) {
        Gson gson = new Gson();
        String jsondata = gson.toJson(syncedHashMaptables);
        prefsEditor.putString(SYNCED_HASH_MAP_TABLES, jsondata);
        prefsEditor.apply();
    }

    public SyncedTableDetails getSyncedHashMaptables() {
        Gson gson = new Gson();
        SyncedTableDetails syncedTableDetails;
        String jsonData = sharedPrefs.getString(SYNCED_HASH_MAP_TABLES, null);
        syncedTableDetails = gson.fromJson(jsonData, SyncedTableDetails.class);
        if (syncedTableDetails == null) {
            syncedTableDetails = new SyncedTableDetails();
            syncedTableDetails.syncTablesMap = new HashMap<>();
        }
        return syncedTableDetails;
    }

    public void setUpdatedConfigurationApiCalls(UpdatedConfigurationApiCalls updatedConfigurationApiCalls) {
        if (updatedConfigurationApiCalls == null) {
            updatedConfigurationApiCalls = new UpdatedConfigurationApiCalls();
        }
        Gson gson = new Gson();
        String jsondata = gson.toJson(updatedConfigurationApiCalls);
        prefsEditor.putString(SET_UPDATED_CONFIGURATION_API_CALL, jsondata);
        prefsEditor.apply();
    }

    public UpdatedConfigurationApiCalls getUpdatedConfigurationApiCalls() {
        Gson gson = new Gson();
        UpdatedConfigurationApiCalls updatedConfigurationApiCalls;
        String jsonData = sharedPrefs.getString(SET_UPDATED_CONFIGURATION_API_CALL, null);
        updatedConfigurationApiCalls = gson.fromJson(jsonData, UpdatedConfigurationApiCalls.class);
        if (updatedConfigurationApiCalls == null) {
            updatedConfigurationApiCalls = new UpdatedConfigurationApiCalls();
        }
        return updatedConfigurationApiCalls;
    }


    public void setDepedentSyncStatus(DependentSyncFlowBase dependentSyncFlowBase) {
        Gson gson = new Gson();
        //String dd = "{\"syncStatusWithTaskId\":{\"129918\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":false,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":1},\"129927\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":false,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":1},\"129926\":{\"isImagesSynced\":true,\"isImprovementPlanSynced\":true,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":4},\"129928\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":false,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":5},\"129932\":{\"isImagesSynced\":true,\"isImprovementPlanSynced\":true,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":false,\"isUnitStrengthSynced\":false,\"taskTypeId\":3},\"129930\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":false,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":7},\"129929\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":true,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":true,\"isUnitStrengthSynced\":false,\"taskTypeId\":3},\"129931\":{\"isImagesSynced\":false,\"isImprovementPlanSynced\":false,\"isIssuesSynced\":false,\"isKitRequestSynced\":false,\"isMetaDataSynced\":false,\"isUnitStrengthSynced\":false,\"taskTypeId\":6}}}";
        String jsonData = gson.toJson(dependentSyncFlowBase);
        prefsEditor.putString(DEPEND_SYNC_STATUS, jsonData);
        prefsEditor.apply();
    }

    public DependentSyncFlowBase getDepedentSyncStatus() {
        Gson gson = new Gson();
        DependentSyncFlowBase dependentSyncFlowBase;
        String jsonData = sharedPrefs.getString(DEPEND_SYNC_STATUS, null);
        dependentSyncFlowBase = gson.fromJson(jsonData, DependentSyncFlowBase.class);
        if (dependentSyncFlowBase == null) {
            dependentSyncFlowBase = new DependentSyncFlowBase();

        }
        return dependentSyncFlowBase;
    }

    public void setScheduleDeleteTimes(ScheduleDeleteTimes scheduleDeleteTimes) {
        Gson gson = new Gson();
        String jsonData = gson.toJson(scheduleDeleteTimes);
        prefsEditor.putString(SCHEDULE_DELETED, jsonData);
        prefsEditor.apply();
    }

    public ScheduleDeleteTimes getScheduleDeleteTimes() {
        Gson gson = new Gson();
        String jsonData = sharedPrefs.getString(SCHEDULE_DELETED, null);
        return gson.fromJson(jsonData, ScheduleDeleteTimes.class);
    }

    public void clearSharedPreferences() {
        if (prefsEditor != null) {
            prefsEditor.clear();
            prefsEditor.apply();
        }
    }

    public void setAttachmentRequestIds(Set<String> attachmentIds) {
        prefsEditor.putStringSet(ATTACHMENT_IDS, attachmentIds);
        prefsEditor.apply();

    }

    public Set<String> getAttachmentRequestIds() {
        return sharedPrefs.getStringSet(ATTACHMENT_IDS, new HashSet<String>());
    }

    /*public void setTaskExecutionResult(DayNightCheckingBaseMO){
        prefsEditor.putStringSet(ATTACHMENT_IDS,attachmentIds);
        prefsEditor.apply();
    }

    public String getTaskExecutionResult(){
        return sharedPrefs.getString(TASK_EXECUTION_RESULT ,null);
    }*/

    //@ASHU : CHECKING AND UPDATING FLAG FOR CLIENT COORDINATION QUESTIONS
    public boolean isCCRQuestionsUpdated() {
        return sharedPrefs.getBoolean("IS_CCR_QUES_UPDATED", false);
    }

    public void updateCCRQuestionAPIFlag(boolean value) {
        prefsEditor.putBoolean("IS_CCR_QUES_UPDATED", value).apply();
    }

    //@ASHU : CHECKING AND UPDATING FLAG FOR MASTER_ACTION_PLAN
    public boolean isMasterActionPlanUpdated() {
        return sharedPrefs.getBoolean("MasterActionPlan", false);
    }

    public void updateMasterActionPlanAPIFlag(boolean value) {
        prefsEditor.putBoolean("MasterActionPlan", value).apply();
    }

    //@ASHU : CHECKING AND UPDATING FLAG FOR CCR - related Issue Matrix values
    public boolean isCCRIssueMatrixUpdated() {
        return sharedPrefs.getBoolean("CCR_IssueMatrix", false);
    }

    public void updateCCRIssueMatrixAPIFlag(boolean value) {
        prefsEditor.putBoolean("CCR_IssueMatrix", value).apply();
    }

    //@ASHU : CHECKING AND UPDATING FLAG FOR CCR - related Issue Matrix values
    public boolean isUnitTypeTableSynced() {
        return sharedPrefs.getBoolean("UNIT_TYPE", false);
    }

    public void updateUnitTypeTableSyncedFlag(boolean value) {
        prefsEditor.putBoolean("UNIT_TYPE", value).apply();
    }

    //@ASHU : CHECKING AND UPDATING FLAG FOR CCR - related Issue Matrix values
    public boolean isGuardDetailsInserted() {
        return sharedPrefs.getBoolean("GUARD_DETAILS", false);
    }

    public void updateGuardDetailsInsertedFlag(boolean value) {
        prefsEditor.putBoolean("GUARD_DETAILS", value).apply();
    }

    public void removeKeyFromSharedPref(String key) {
        try {
            if (prefsEditor != null)
                prefsEditor.remove(key).apply();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }
}