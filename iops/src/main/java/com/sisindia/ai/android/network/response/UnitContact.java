package com.sisindia.ai.android.network.response;

public class UnitContact {

    private Integer Id;
    private Integer UnitId;
    private String FirstName;
    private String LastName;
    private String EmailId;
    private String ContactNo;
    private String Designation;
    private Boolean IsActive;

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The UnitId
     */
    public Integer getUnitId() {
        return UnitId;
    }

    /**
     * @param UnitId The UnitId
     */
    public void setUnitId(Integer UnitId) {
        this.UnitId = UnitId;
    }

    /**
     * @return The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     * @param FirstName The FirstName
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     * @return The LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     * @param LastName The LastName
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     * @return The EmailId
     */
    public String getEmailId() {
        return EmailId;
    }

    /**
     * @param EmailId The EmailId
     */
    public void setEmailId(String EmailId) {
        this.EmailId = EmailId;
    }

    /**
     * @return The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     * @param ContactNo The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     * @return The Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * @param Designation The Designation
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     * @return The IsActive
     */
    public Boolean getIsActive() {
        return IsActive;
    }

    /**
     * @param IsActive The IsActive
     */
    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

}
