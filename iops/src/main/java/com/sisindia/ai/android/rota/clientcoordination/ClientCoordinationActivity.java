package com.sisindia.ai.android.rota.clientcoordination;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.clientcoordination.data.QuestionResponse;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shivam on 26/7/16.
 */

public class ClientCoordinationActivity extends DayCheckBaseActivity implements DialogClickListener {

    private static final String SCAN_KEY = "scan_key";

    private DateTimeFormatConversionUtil dateTimeUtil;
    private HashMap<String, AttachmentMetaDataModel> imageList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_coordination_activity);
        dateTimeUtil = new DateTimeFormatConversionUtil();
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initializeToolbar(getString(R.string.client_coordination), true);
        addFragment();
    }

    private void addFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ClientCoordinationFragment coordinationFragment = ClientCoordinationFragment.newInstance();
        fragmentManager.beginTransaction().add(R.id.ll_client_coordination_fragment, coordinationFragment, TAG).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_post_save:
                if (appPreferences.isEnableGps(ClientCoordinationActivity.this)) {
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);

                    if (fragment instanceof ClientCoordinationFragment) {
                        ClientCoordinationFragment coordinationFragment = (ClientCoordinationFragment) fragment;
                        QuestionResponse response = coordinationFragment.sendResponse();
                        if (response != null) {

                            Util.showProgressBar(ClientCoordinationActivity.this, R.string.saveMessage);
                            dbUpdates(IOPSApplication.getGsonInstance().toJson(response));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Util.dismissProgressBar(true);
                                    Intent homeActivityIntent = new Intent(ClientCoordinationActivity.this, HomeActivity.class);
                                    homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(homeActivityIntent);
                                    finish();
                                }
                            }, 2500);
                        }
                    }

                } else {
                    showGpsDialog(getString(R.string.gps_configure_msg));
                }

                break;
            case android.R.id.home:
                showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dbUpdates(String responseJson) {
        addComplaints();
        getMetaDataFromMap();
        updateRotaTaskTable(responseJson);
        updateMyPerformanceTable();
        insertMetaData();
        triggerSyncAdapter();
        insertLogActivity();

        if (rotaTaskModel.getTaskId() < 0) {
            if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                        .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
            }
        }

    }

    private void addComplaints() {
        if (Util.getmComplaintModelHolder() != null && 0 != Util.getmComplaintModelHolder().size()) {
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addIssueDetails(Util.getmComplaintModelHolder());
            Util.reSetComplaintModelHolder();
        }
    }

    public synchronized void getMetaDataFromMap() {
        if (imageList != null && imageList.size() > 0) {
            for (Map.Entry<String, AttachmentMetaDataModel> entry : imageList.entrySet()) {
                setAttachmentMetaDataModel(entry.getValue());
            }
        }
    }

    public synchronized void metaDataSetUp(AttachmentMetaDataModel attachmentModel) {
        imageList.put(SCAN_KEY, attachmentModel);
    }

    private synchronized void updateRotaTaskTable(String responseJson) {
        String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateRotaTask(rotaTaskModel.getTaskId(),
                responseJson, RotaTaskStatusEnum.Completed.getValue(),
                dateTimeUtil.getCurrentDateTime(),
                rotaTaskModel.getRemarks(),
                geoLocation
        );
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);
    }

    private synchronized void updateMyPerformanceTable() {
        IOPSApplication.getInstance().getMyPerformanceStatementsDB().
                UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(),
                        dateTimeUtil.getCurrentTime(), String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());
    }

    private void triggerSyncAdapter() {
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(new Bundle());
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        clearSdCardImages();
        finish();
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.CONFIRAMTION_DIALOG)));
    }

    @Override
    public void finish() {
        super.finish();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearSdCardImages() {
        for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
            if (attachmentMetaDataModel != null) {
                File file = new File(attachmentMetaDataModel.getAttachmentPath());
                if (file != null && file.exists()) {
                    file.delete();
                }
            }
        }
    }
}
