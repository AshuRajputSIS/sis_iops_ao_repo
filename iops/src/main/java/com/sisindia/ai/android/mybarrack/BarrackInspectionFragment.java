package com.sisindia.ai.android.mybarrack;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.manualrefresh.ManualBarrackSync;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by shruti on 25/7/16.
 */
public class BarrackInspectionFragment extends BaseFragment implements BarrackSyncListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.tv_activities_header)
    CustomFontTextview tvActivitiesHeader;
    @Bind(R.id.rv_barrack_view)
    RecyclerView rvBarrackView;
    @Bind(R.id.parent_layout)
    LinearLayout parentLayout;
    private String mParam1, mParam2;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private BarrackSyncListener barrackSyncListener;
    Context mContext;

    public BarrackInspectionFragment() {
        // Required empty public constructor
    }

    public static BarrackInspectionFragment newInstance(String param1, String param2) {
        BarrackInspectionFragment fragment = new BarrackInspectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_mybarrack_homepage, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sync_my_barrack) {
            if (NetworkUtil.isConnected) {
                ManualBarrackSync.getBarrackApiCall(barrackSyncListener, getActivity(), isVisible());
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.no_internet));
            }
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.barrack_layout, container, false);
        ButterKnife.bind(this, view);
        setUpUI();
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.barrack_layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//for enable the menu in fragment
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        barrackSyncListener = this;
    }

    private void setUpUI() {
        List<Barrack> barrackModel = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackDetails();

        if (barrackModel != null) {
            tvActivitiesHeader.setText(getResources().getString(R.string.select_barrack));
            rvBarrackView.setNestedScrollingEnabled(true);
            rvBarrackView.setLayoutManager(new LinearLayoutManager(mContext));
            rvBarrackView.setAdapter(new BarrackAdapter(barrackModel, mContext));
        } else {
            tvActivitiesHeader.setText(getResources().getString(R.string.NO_BARRACK_AVAILABLE));
        }
    }

    @Override
    public void barrackSyncListener(boolean isSynced) {
        if (getActivity() != null) {
            if (isSynced) {
                Util.showToast(getActivity(), getActivity().getResources()
                        .getString(R.string.refresh_apicall_str));
                setUpUI();
            } else {
                Util.showToast(getActivity(), getActivity().getResources().getString(R.string.refresh_apicall_error_str));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
