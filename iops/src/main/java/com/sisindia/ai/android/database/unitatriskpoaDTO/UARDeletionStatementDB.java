package com.sisindia.ai.android.database.unitatriskpoaDTO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.database.SISAITrackingDB;

/**
 * Created by shankar on 19/7/16.
 */

public class UARDeletionStatementDB extends SISAITrackingDB {
    public UARDeletionStatementDB(Context context) {
        super(context);
    }


    public void deleteUnitRiskTable(String tableName){

        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }


    }

    public void deleteUnitRiskActionPlanTable(String tableName){

        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        }catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }


    }
    }
}
