package com.sisindia.ai.android.database;


/**
 * Created by shankar on 28/3/16.
 */
public class TableStatement {

    public String billCollactionApiTable() {

        String BillCollactionApiTable = "CREATE TABLE " + TableNameAndColumnStatement.UnitBillingStatusTable + "(" +
                TableNameAndColumnStatement.Bill_COLLACTION_ID + " INTEGER PRIMARY KEY , " +
                TableNameAndColumnStatement.Bill_COLLACTION_UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.Bill_COLLACTION_UNIT_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.Bill_NUMBER + " TEXT(200)," +
                TableNameAndColumnStatement.Bill_MONTH + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.Bill_YEAR + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.OUTSTANDING_AMOUNT + " INTEGER , " +
                TableNameAndColumnStatement.OUTSTANDING_DAYS + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UPDATED_DATES + " TEXT(100), " +
                TableNameAndColumnStatement.IS_BILL_COLLECTED + " INTEGER " +
                ")";
        return BillCollactionApiTable;
    }

    public String myPerformanceTable() {

        String MyPerformanceTable = "CREATE TABLE " + TableNameAndColumnStatement.MY_PERFORMANCE_TABLE + "(" +
                TableNameAndColumnStatement.PERFORMANCE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.PERFORMANCE_ACTIVITY + " TEXT(100), " +
                TableNameAndColumnStatement.PERFORMANCE_LOCATION + " TEXT(100)," +
                TableNameAndColumnStatement.PERFORMANCE_TASK_ID + " TEXT(100)," +
                TableNameAndColumnStatement.PERFORMANCE_TASK_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.TASK_STATUS + " TEXT(100), " +
                TableNameAndColumnStatement.DATE + " DATETIME, " +
                TableNameAndColumnStatement.START_TIME + " DATETIME, " +
                TableNameAndColumnStatement.END_TIME + " DATETIME " +
                ")";
        return MyPerformanceTable;

    }

    public String appUserTable() {

        String appUserTable = "CREATE TABLE " + TableNameAndColumnStatement.APP_USER_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " INTEGER not null unique, " +
                TableNameAndColumnStatement.EMPLOYEE_NO + " TEXT(10) not null unique, " +
                TableNameAndColumnStatement.FIRST_NAME + " TEXT(50) , " +
                TableNameAndColumnStatement.LAST_NAME + " TEXT(50) , " +
                TableNameAndColumnStatement.MIDDLE_NAME + " TEXT(50)," +
                TableNameAndColumnStatement.ORGANIZATION_ID + " INTEGER," +
                TableNameAndColumnStatement.AI_PHOTO + " TEXT," +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER," +
                TableNameAndColumnStatement.PRIMARY_ADDRESS_ID + " INTEGER NULL," +
                TableNameAndColumnStatement.RESIDENTIAL_ADDRESS_ID + " INTEGER NULL," +
                TableNameAndColumnStatement.ADMIN_CONTROLLER_ID + " INTEGER NULL," +
                TableNameAndColumnStatement.IS_REFERRAL + " INTEGER NULL," +
                TableNameAndColumnStatement.REFERRAL_ID + " INTEGER ," +
                TableNameAndColumnStatement.REFERRAL_SOURCE + " TEXT(100)," +
                TableNameAndColumnStatement.EMPLOYEE_PROFILE_PIC_ID + " INTEGER," +
                TableNameAndColumnStatement.MINIATURE_PROFILE_PIC_ID + " INTEGER," +
                TableNameAndColumnStatement.COMPANY_CONTACT_NUMBER + " TEXT(15)," +
                TableNameAndColumnStatement.DATE_OF_BIRTH + " TEXT(25) ," +
                TableNameAndColumnStatement.DATE_OF_JOINING + " TEXT(25)," +
                TableNameAndColumnStatement.FUNCTIONAL_CONTROLLER_ID + " INTEGER ," +
                TableNameAndColumnStatement.EMERGENCY_CONTACT_NO + " TEXT(15)," +
                TableNameAndColumnStatement.CONTACT_NO + " TEXT(15) ," +
                TableNameAndColumnStatement.ALTERNATE_CONTACT_NO + "	TEXT(15)," +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.RANK_ID + " TEXT(50) ," +
                TableNameAndColumnStatement.EMPLOYEE_SIGNATURE_ID + " INTEGER ," +
                TableNameAndColumnStatement.EMAIL_ADDRESS + " TEXT(50) ," +
                TableNameAndColumnStatement.PREFERRED_LANGUAGE_ID + " INTEGER ," +
                TableNameAndColumnStatement.FULL_ADDRESS + " TEXT(700) , " +
                TableNameAndColumnStatement.GEO_LATITUDE + " REAL(9,6) ," +
                TableNameAndColumnStatement.GEO_LONGITUDE + " REAL(9,6)," +
                TableNameAndColumnStatement.REGION_ID + " INTEGER," +
                TableNameAndColumnStatement.IS_DEFAULT_PASSWORD + " INTEGER, " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_APP_USER_BRANCH +
                " FOREIGN KEY (" + TableNameAndColumnStatement.BRANCH_ID +
                " ) " +
                " REFERENCES " + TableNameAndColumnStatement.BRANCH_TABLE +
                " (" + TableNameAndColumnStatement.BRANCH_ID + ")" + "," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_EMPLOYEE_NO_APP_USER +
                " unique (" + TableNameAndColumnStatement.EMPLOYEE_NO + " ASC)" + "," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_EMPLOYEE_ID_APP_USER +
                " unique (" + TableNameAndColumnStatement.EMPLOYEE_ID + " ASC)" +
                ")";
        return appUserTable;
    }

    public String areaTable() {
        String areaTable = "CREATE TABLE " + TableNameAndColumnStatement.AREA_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.AREA_ID + " INTEGER , " +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER, " +
                TableNameAndColumnStatement.AREA_CODE + " TEXT(20), " +
                TableNameAndColumnStatement.AREA_NAME + " TEXT(50), " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_AREA_BRANCH +
                " FOREIGN KEY (" + TableNameAndColumnStatement.BRANCH_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.BRANCH_TABLE +
                "(" + TableNameAndColumnStatement.BRANCH_ID + " )," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_AREA_ID_AREA +
                " unique (" + TableNameAndColumnStatement.AREA_ID + " ASC) " +
                ")";
        return areaTable;
    }

    public String attachmentMetadataTable() {
        String attachmentMetadataTable = "CREATE TABLE " + TableNameAndColumnStatement.ATTACHMENT_METADATA_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ATTACHMENT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.FILE_NAME + " TEXT(250) NOT NULL, " +
                TableNameAndColumnStatement.FILE_SIZE + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.FILE_EXTENSION + " TEXT(5) NOT NULL, " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER NOT NULL , " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL , " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER , " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER , " +
                TableNameAndColumnStatement.ATTACHMENT_REFREENCE_TYPE + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ATTACHMENT_REFRENCE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ATTACHMENT_TYPE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.FILE_PATH + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IS_FILE_Upload + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ATTACHMENT_SOURCE_CODE + " TEXT(100) , " +
                TableNameAndColumnStatement.IS_SAVE + " INTEGER , " +
                TableNameAndColumnStatement.SEQUENCE_NO + " INTEGER NOT NULL " +
                ")";
        return attachmentMetadataTable;
    }

    public String branchTable() {
        String branchTable = "CREATE TABLE " + TableNameAndColumnStatement.BRANCH_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.BRANCH_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.BRANCH_CODE + " TEXT(20) NOT NULL, " +
                TableNameAndColumnStatement.BRANCH_FULL_ADDRESS + " TEXT(700), " +
                TableNameAndColumnStatement.GEO_LATITUDE + " REAL(9,6) , " +
                TableNameAndColumnStatement.GEO_LONGITUDE + " REAL(9,6) ," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_BRANCH_ID_BRANCH +
                " unique (" + TableNameAndColumnStatement.BRANCH_ID + " ASC) " + ")";
        return branchTable;
    }

    public String gpsBatteryHealthTable() {

        String gpsBatteryHealthTable = "CREATE TABLE " + TableNameAndColumnStatement.GPS_BATTERY_HEALTH_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.GPS_LATITUDE + " REAL(9,6) NOT NULL, " +
                TableNameAndColumnStatement.GPS_LONGITUDE + " REAL(9,6) NOT NULL, " +
                TableNameAndColumnStatement.BATTERY_PERCENTAGE + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.LOGGED_IN_DATETIME + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.DISTANCE_TRAVELLED + " INTEGER , " +
                TableNameAndColumnStatement.DISTANCE_TRAVELLED_IN_TIME + " INTEGER" +
                ")";
        return gpsBatteryHealthTable;
    }

    public String holidayCalendarTable() {

        String holidayCalendarTable = "CREATE TABLE " + TableNameAndColumnStatement.HOLIDAY_CALENDAR_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.HOLIDAY_DATE + " TEXT(20) NOT NULL, " +
                TableNameAndColumnStatement.HOLIDAY_DESCRIPTION + " TEXT(100), " +
                TableNameAndColumnStatement.IS_REGIONAL_HOLIDAY + " INTEGER , " +
                TableNameAndColumnStatement.HOLIDAY_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER, " +
                TableNameAndColumnStatement.CREATED_DATE
                + " TEXT(50) , " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_HOLIDAY_ID_HOLIDAY_CALENDAR +
                " unique (" + TableNameAndColumnStatement.HOLIDAY_ID + " ASC)" +
                ")";
        return holidayCalendarTable;
    }

    public String improvementPlanTable() {
        String improvementPlanTable = "CREATE TABLE " + TableNameAndColumnStatement.IMPROVEMENT_PLAN_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.IMPROVEMENT_PLAN_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER , " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER , " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_ID + " INTEGER, " +
                TableNameAndColumnStatement.ACTION_PLAN_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.EXPECTED_CLOSURE_DATE + " TEXT NOT NULL , " +
                TableNameAndColumnStatement.TARGET_EMPLOYEE_ID + " INTEGER, " +
                TableNameAndColumnStatement.STATUS_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IMPROVEMENT_PLAN_CATEGORY_ID + " INTEGER , " +
                TableNameAndColumnStatement.REMARKS + " TEXT(500), " +
                TableNameAndColumnStatement.CLOSURE_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.CLOSED_BY_ID + " INTEGER, " +
                TableNameAndColumnStatement.BUDGET + " INTEGER, " +
                TableNameAndColumnStatement.CLOSURE_COMMENT + " TEXT, " +
                TableNameAndColumnStatement.ASSIGNED_TO_NAME + " TEXT, " +
                TableNameAndColumnStatement.ASSIGNED_TO_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_MATRIX_ID + " INTEGER, " +
                TableNameAndColumnStatement.CLIENT_COORDINATION_QUESTION_ID + " INTEGER, " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER NOT NULL " +
                ")";
        return improvementPlanTable;
    }

    public String leaveCalendarTable() {
        String leaveCalendarTable = "CREATE TABLE " + TableNameAndColumnStatement.LEAVE_CALENDAR_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.LEAVE_DATE + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.CREATED_DATE + " INTEGER NOT NULL " +
                ")";
        return leaveCalendarTable;
    }

    public String messVendorTable() {
        String messVendorTable = "CREATE TABLE " + TableNameAndColumnStatement.MESS_VENDOR_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.VENDOR_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.VENDOR_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(700), " +
                TableNameAndColumnStatement.VENDOR_CONTACT_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.VENDOR_FULL_ADDRESS + " TEXT(700), " +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_VENDOR_ID_MESS_VENDOR +
                " UNIQUE (" + TableNameAndColumnStatement.VENDOR_ID + " ASC) " +
                ")";
        return messVendorTable;
    }

    public String unitRiskTable() {

        String unitRiskTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_RISK_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_RISK_ID
                + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_ID
                + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_NAME
                + " TEXT(200), " +
                TableNameAndColumnStatement.CURRENT_MONTH
                + " INTEGER, " +
                TableNameAndColumnStatement.CURRENT_YEAR
                + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_RISK_STATUS_ID + " INTEGER, " +
                TableNameAndColumnStatement.TOTAL_ITEM_COUNT + " INTEGER, " +
                TableNameAndColumnStatement.CLOSED_AT + " TEXT, " +
                TableNameAndColumnStatement.RISK_COUNT + " INTEGER, " +
                TableNameAndColumnStatement.RISK_COUNT_BY_AI + " INTEGER, " +
                TableNameAndColumnStatement.ACTION_COUNT + " INTEGER, " +
                TableNameAndColumnStatement.ACTION_PENDING_COUNT + " INTEGER " +
                ")";
        return unitRiskTable;
    }

    public String unitRiskActionPlanTable() {
        String unitRiskActionPlanTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN_ID
                + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RISK_ID
                + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RISK_REASON
                + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RISK_ACTION_PLAN
                + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.ACTION_PLAN_DATE
                + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.ASSIGNED_ID + " INTEGER, " +
                TableNameAndColumnStatement.ASSIGNED_TO + " TEXT, " +
                TableNameAndColumnStatement.COMPLETION_DATE
                + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.IS_COMPLETED
                + " INTEGER, " +
                TableNameAndColumnStatement.REMARKS + " TEXT(500), " +
                TableNameAndColumnStatement.CLOSED_DATE + " INTEGER," +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER" +
                ")";
        return unitRiskActionPlanTable;
    }

    public String recruitmentTable() {
        String recruitmentTable = "CREATE TABLE " + TableNameAndColumnStatement.RECRUITMENT_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.RECRUIT_ID + " INTEGER, " +
                TableNameAndColumnStatement.FULL_NAME + " TEXT(150) NOT NULL, " +
                TableNameAndColumnStatement.CONTACT_NO + " TEXT(15) NOT NULL, " +
                TableNameAndColumnStatement.DATE_OF_BIRTH + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.IS_SELECTED + " INTEGER, " +
                TableNameAndColumnStatement.IS_REJECTED + " INTEGER, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ACTION_TAKEN_ON + " INTEGER, " +
                TableNameAndColumnStatement.STATUS + " TEXT " +
                ")";
        return recruitmentTable;
    }

    public String rotaTable() {
        String rotaTable = "CREATE TABLE " + TableNameAndColumnStatement.ROTA_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ROTA_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ROTA_WEEK + " INTEGER, " +
                TableNameAndColumnStatement.ROTA_MONTH + " INTEGER, " +
                TableNameAndColumnStatement.ROTA_PUBLISHED_DATE_TIME + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ROTA_STATUS_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ROTA_YEAR + " INTEGER, " +
                TableNameAndColumnStatement.ROTA_DATE + " INTEGER NOT NULL, " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_ROTA_APP_USER +
                " FOREIGN KEY (" + TableNameAndColumnStatement.EMPLOYEE_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.APP_USER_TABLE + "," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_ROTA_ID_ROTA +
                " unique (" + TableNameAndColumnStatement.ROTA_ID + " ASC)" +
                ")";
        return rotaTable;
    }

    public String rotaTaskTable() {
        String rotaTaskTable = "CREATE TABLE " + TableNameAndColumnStatement.ROTA_TASK_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ROTA_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ROTA_TASK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.SOURCE_GEO_LATITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.SOURCE_GEO_LONGITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.DESTINATION_GEO_LONGITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.DESTINATION_GEO_LATITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.ESTIMATED_DISTANCE + " DOUBLE, " +
                TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME + " DOUBLE, " +
                TableNameAndColumnStatement.TASK_SEQUENCE_NO + " INTEGER, " +
                TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_START_TIME + " TEXT(100), " +
                TableNameAndColumnStatement.ESTIMATED_TASK_EXECUTION_END_TIME + " TEXT(100), " +
                TableNameAndColumnStatement.TOTAL_ESTIMATED_TIME + " INTEGER, " +
                TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_START_TIME + " TEXT(100), " +
                TableNameAndColumnStatement.ACTUAL_TASK_EXECUTION_END_TIME + " TEXT(100), " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ACTUAL_TRAVEL_TIME + " INTEGER, " +
                TableNameAndColumnStatement.ACTUAL_DISTANCE + " INTEGER, " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_ROTA_TASK_ROTA +
                " FOREIGN KEY (" + TableNameAndColumnStatement.ROTA_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.ROTA_TABLE +
                "(" + TableNameAndColumnStatement.ROTA_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_ROTA_TASK_TASK +
                " FOREIGN KEY (" + TableNameAndColumnStatement.TASK_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.TASK_TABLE +
                "(" + TableNameAndColumnStatement.TASK_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_ROTA_TASK_ID +
                " unique (" + TableNameAndColumnStatement.ROTA_TASK_ID + " ASC)" +
                ")";
        return rotaTaskTable;
    }

    public String barrackTable() {

        String barrackTable = "CREATE TABLE " + TableNameAndColumnStatement.BARRACK_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.BARRACK_NAME + " TEXT(50) , " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(500), " +
                TableNameAndColumnStatement.IS_MESS_AVAILABLE + " INTEGER, " +
                TableNameAndColumnStatement.MESS_VENDOR_ID + " INTEGER, " +
                TableNameAndColumnStatement.AREA_INSPECTOR_NAME + " INTEGER, " +
                TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED + " INTEGER, " +
                TableNameAndColumnStatement.BARRACK_FULL_ADDRESS + " TEXT(500), " +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER, " +
                TableNameAndColumnStatement.BARRACK_CODE + " INTEGER, " +
                TableNameAndColumnStatement.GEO_LATITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.GEO_LONGITUDE + " REAL(9,6), " +
                TableNameAndColumnStatement.BARRACK_OWNER_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.BARRACK_INCHARGE_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER ," +
                TableNameAndColumnStatement.BARRACK_IS_NFC_SCANNED + " INTEGER ," + // @Ashu: 22Nov17 [ADDING COLUMN FOR NFC TAGGING AGAINST DEVICE]
                TableNameAndColumnStatement.DEVICE_NO + " TEXT(500), " +  // @Ashu: 16Nov17 [ADDING COLUMN FOR NFC TAGGING AGAINST DEVICE]
                " CONSTRAINT " + TableNameAndColumnStatement.FK_BARRACK_MESS_VENDOR +
                " FOREIGN KEY (" + TableNameAndColumnStatement.MESS_VENDOR_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.MESS_VENDOR_TABLE +
                "(" + TableNameAndColumnStatement.VENDOR_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_BARRACK_ID_BARRACK +
                " unique (" + TableNameAndColumnStatement.BARRACK_ID + ") " +
                ")";
        return barrackTable;
    }

    public String customerTable() {

        String customerTable = "CREATE TABLE " + TableNameAndColumnStatement.CUSTOMER_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.CUSTOMER_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.CUSTOMER_NAME + " TEXT(100) NOT NULL, " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(500), " +
                TableNameAndColumnStatement.PRIMARY_CONTACT_NAME + " TEXT(150) NOT NULL, " +
                TableNameAndColumnStatement.PRIMARY_CONTACT_EMAIL + " TEXT(100), " +
                TableNameAndColumnStatement.PRIMARY_CONTACT_PHONE + " TEXT(15), " +
                TableNameAndColumnStatement.SECONDARY_CONTACT_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.SECONDARY_CONTACT_EMAIL + " TEXT(100), " +
                TableNameAndColumnStatement.SECONDARY_CONTACT_PHONE + " TEXT(15), " +
                TableNameAndColumnStatement.CAN_SEND_SMS + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.CAN_SEND_EMAIL + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.CUSTOMER_FULL_ADDRESS + " TEXT(500) NOT NULL, " +
                " CONSTRAINT " + TableNameAndColumnStatement.UQ_CUSTOMER_ID_CUSTOMER +
                " unique (" + TableNameAndColumnStatement.CUSTOMER_ID + " ASC)" +
                ")";
        return customerTable;
    }

    public String taskTable() {
        String taskTable = "CREATE TABLE " + TableNameAndColumnStatement.TASK_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_NAME + " TEXT(50), " +
                TableNameAndColumnStatement.TASK_TYPE_ID + " INTEGER , " +
                TableNameAndColumnStatement.ASSIGNEE_ID + " INTEGER , " +
                TableNameAndColumnStatement.ASSIGNEE_NAME + " TEXT(50) , " +
                TableNameAndColumnStatement.ESTIMATED_EXECUTION_TIME + " INTEGER , " +
                TableNameAndColumnStatement.ACTUAL_EXECUTION_TIME + " INTEGER , " +
                TableNameAndColumnStatement.TASK_STATUS_ID + " INTEGER , " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(300) , " +
                TableNameAndColumnStatement.ESTIMATED_EXECUTION_START_DATE_TIME + " TEXT(50) , " +
                TableNameAndColumnStatement.ACTUAL_EXECUTION_START_DATE_TIME + " TEXT(50) , " +
                TableNameAndColumnStatement.IS_AUTO + " INTEGER, " +
               /*  TableNameAndColumnStatement.EXECUTOR_ID)
                + " INTEGER, " +
                 TableNameAndColumnStatement.EXECUTOR_NAME)
                + " TEXT(50) , " +*/
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT(50) , " +
                TableNameAndColumnStatement.CREATED_BY_ID + " INTEGER, " +
                TableNameAndColumnStatement.CREATED_BY_NAME + " TEXT(50) , " +
                TableNameAndColumnStatement.IS_APPROVAL_REQUIRED + " INTEGER , " +
                TableNameAndColumnStatement.ASSIGNED_BY_ID + " INTEGER, " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER, " +
                TableNameAndColumnStatement.TASK_EXECUTION_RESULT + " TEXT, " +
                TableNameAndColumnStatement.ESTIMATED_EXECUTION_END_DATE_TIME + " TEXT , " +
                TableNameAndColumnStatement.ACTUAL_EXECUTION_END_DATE_TIME + " TEXT, " +
               /*  TableNameAndColumnStatement.ESTIMATED_TRAVEL_TIME + " INTEGER , " +
                 TableNameAndColumnStatement.ESTIMATED_DISTANCE + " DOUBLE , " +*/
                TableNameAndColumnStatement.UNIT_TYPE_ID + " INTEGER , " +
                TableNameAndColumnStatement.SELECT_REASON_ID + " INTEGER , " +
                TableNameAndColumnStatement.OTHER_TASK_REASON_ID + " INTEGER , " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER , " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER , " +
                TableNameAndColumnStatement.IS_TASK_STARTED + " INTEGER , " +
                TableNameAndColumnStatement.ADHOC_STATUS + " TEXT, " +
                TableNameAndColumnStatement.CREATED_DATE + " TEXT(50) , " +
                TableNameAndColumnStatement.TASK_SUBMITED_LOCATION + " TEXT(100) , " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_TASK_UNIT +
                " FOREIGN KEY (" + TableNameAndColumnStatement.UNIT_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.UNIT_TABLE +
                "(" + TableNameAndColumnStatement.UNIT_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_TASK_BARRACK +
                " FOREIGN KEY (" + TableNameAndColumnStatement.BARRACK_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.BARRACK_TABLE +
                "(" + TableNameAndColumnStatement.BARRACK_ID + ")" +

               /* " CONSTRAINT " +  TableNameAndColumnStatement.FK_TASK_UNIT_POST +
                " FOREIGN KEY (" +  TableNameAndColumnStatement.POST_ID + ") " +

                " REFERENCES " +  TableNameAndColumnStatement.UNIT_POST_TABLE +
                "(" +  TableNameAndColumnStatement.UNIT_POST_ID + ")" +*/
                ")";
        return taskTable;

    }

    public String unitTable() {
        String unitTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.UNIT_CODE + " TEXT(10) NOT NULL, " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(500), " +
                TableNameAndColumnStatement.UNIT_TYPE + " TEXT(50) , " +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " INTEGER  , " +
                TableNameAndColumnStatement.DAY_CHECK_FREQUENCY + " INTEGER , " +
                TableNameAndColumnStatement.NIGHT_CHECK_FREQUENCY + " INTEGER , " +
                TableNameAndColumnStatement.CLIENT_COORDINATION_FREQUENCY + " INTEGER , " +
                TableNameAndColumnStatement.BILLING_DATE + " INTEGER  , " +
                TableNameAndColumnStatement.BILL_COLLECTION_DATE + " INTEGER , " +
                TableNameAndColumnStatement.WAGE_DATE + " INTEGER , " +
                TableNameAndColumnStatement.BILLING_MODE + " INTEGER , " +
                TableNameAndColumnStatement.MESS_VENDOR_ID + " INTEGER , " +
                TableNameAndColumnStatement.IS_BARRACK_PROVIDED + " INTEGER , " +
                TableNameAndColumnStatement.UNIT_COMMANDER_ID + " INTEGER, " +
                TableNameAndColumnStatement.AREA_INSPECTOR_ID + " INTEGER, " +
                TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_ID + " INTEGER, " +
                TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_ID + " INTEGER, " +
                TableNameAndColumnStatement.BILL_AUTHORITATIVE_UNIT_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_COMMANDER_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.AREA_INSPECTOR_NAME + " TEXT(150) NOT NULL, " +
                TableNameAndColumnStatement.BILL_SUBMISSION_RESPONSIBLE_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.BILL_COLLECTION_RESPONSIBLE_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.UNIT_FULL_ADDRESS + " TEXT(500) , " +
                TableNameAndColumnStatement.UNIT_STATUS + " TEXT(500) , " +
                TableNameAndColumnStatement.COLLECTION_STATUS + " TEXT(150), " +
                TableNameAndColumnStatement.AREA_ID + " INTEGER , " +
                TableNameAndColumnStatement.CUSTOMER_ID + " INTEGER , " +
                TableNameAndColumnStatement.GEO_LATITUDE + " REAL(9,6) ," +
                TableNameAndColumnStatement.GEO_LONGITUDE + " REAL(9,6)," +
                TableNameAndColumnStatement.BILLING_TYPE_ID + " INTEGER,  " +
                TableNameAndColumnStatement.BILL_GENERATION_DATE + " INTEGER,  " +
                TableNameAndColumnStatement.MAIN_GATE_IMAGE_URL + " TEXT,  " +
                TableNameAndColumnStatement.ARMED_STRENGTH + " INTEGER,  " +
                TableNameAndColumnStatement.UNARMED_STRENGTH + " INTEGER,  " +

                TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.NFC_EXEMPTED + " INTEGER, " +

                TableNameAndColumnStatement.UNIT_BOUNDARY + " TEXT(1000),  " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_AREA +
                " FOREIGN KEY (" + TableNameAndColumnStatement.AREA_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.AREA_TABLE +
                "(" + TableNameAndColumnStatement.AREA_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_MESS_VENDOR +
                " FOREIGN KEY (" + TableNameAndColumnStatement.MESS_VENDOR_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.MESS_VENDOR_TABLE +
                "(" + TableNameAndColumnStatement.VENDOR_ID + ")," +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_CUSTOMER +
                " FOREIGN KEY (" + TableNameAndColumnStatement.CUSTOMER_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.CUSTOMER_TABLE +
                "(" + TableNameAndColumnStatement.CUSTOMER_ID + ")" +

               /* " CONSTRAINT " +  TableNameAndColumnStatement.UQ_UNIT_ID_UNIT +
                " unique (" +  TableNameAndColumnStatement.UNIT_ID + " ASC)," +
                " CONSTRAINT " +  TableNameAndColumnStatement.UQ_UNIT_CODE_UNIT +
                " unique (" +  TableNameAndColumnStatement.UNIT_CODE + " ASC)" +*/
                ")";


        return unitTable;

    }

    //New Db Version - 9  drop this , create new and old data

    public String unitBarrackTable() {
        String unitBarrackTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_BARRACK_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_BARRACK_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER )";
        return unitBarrackTable;
    }

    public String UnitAuthorizedStrengthTable() {

        String unitAuthorizedStrengthTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER, " +
                TableNameAndColumnStatement.SHIFT_ID + " INTEGER, " +
                TableNameAndColumnStatement.RANK_ID + " INTEGER, " +
                TableNameAndColumnStatement.RANK_ABBREVATION + " TEXT(100), " +
                TableNameAndColumnStatement.RANK_COUNT + " INTEGER , " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.LEAVE_COUNT + " INTEGER, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT(100), " +
                TableNameAndColumnStatement.SHIFT_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.RANK_NAME + " TEXT(300), " +
                TableNameAndColumnStatement.BARRACK_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.STRENGTH + " INTEGER , " +
                TableNameAndColumnStatement.ACTUAL + " INTEGER , " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER , " +
                TableNameAndColumnStatement.IS_ARMED + " INTEGER, " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER " +
                ")";

        return unitAuthorizedStrengthTable;

    }

    public String securityRiskTable() {
        String securityRiskquestionQuestionTable = "CREATE TABLE " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.SECURITY_QUESTIONS + " TEXT, " +
                TableNameAndColumnStatement.SECURITY_QUESTION_DESCRIPTION + " TEXT " +

                ")";


        return securityRiskquestionQuestionTable;
    }

    public String securityRiskOptionTable() {
        String securityRiskQuestionOptionTable = "CREATE TABLE " + TableNameAndColumnStatement.SECURITY_RISK_QUESTION_OPTIONS_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.SECURITY_RISK_QUESTIONS_ID + " INTEGER, " +
                TableNameAndColumnStatement.SECURITY_RISK_OPTION_ID + " INTEGER, " +
                TableNameAndColumnStatement.SECURITY_RISK_OPTION_NAME + " TEXT, " +
                TableNameAndColumnStatement.SECURITY_RISK_IS_MANDATORY + " INTEGER ," +
                TableNameAndColumnStatement.SECURITY_RISK_CONTROL_TYPE + " TEXT ," +
                TableNameAndColumnStatement.SECURITY_RISK_IS_ACTIVE + " INTEGER )";
        return securityRiskQuestionOptionTable;
    }


    public String unitcontactTable() {
        String unitcontactTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_CONTACT_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_CONTACT_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.FIRST_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.LAST_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.EMAIL_ADDRESS + " TEXT(100) NOT NULL, " +
                TableNameAndColumnStatement.PHONE_NO + " TEXT(15) NOT NULL, " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.DESIGNATION + " TEXT(50) NOT NULL, " +


                //PRIMARY KEY ("unit_id", "barrack_id" +


                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_CONTACT_UNIT +
                " FOREIGN KEY (" + TableNameAndColumnStatement.UNIT_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.UNIT_TABLE +
                "(" + TableNameAndColumnStatement.UNIT_ID + ")" +

                ")";


        return unitcontactTable;

    }

    public String unitemployeeTable() {

        String unitemployeeTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_EMPLOYEE_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER , " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " INTEGER , " +
                TableNameAndColumnStatement.EMPLOYEE_NO + " TEXT(15) , " +
                TableNameAndColumnStatement.EMPLOYEE_FULL_NAME + " TEXT(150), " +
                TableNameAndColumnStatement.EMPLOYEE_CONTACT_NO + " TEXT(10) , " +
                TableNameAndColumnStatement.EMPLOYEE_RANK_ID + " INTEGER , " +
                TableNameAndColumnStatement.DEPLOYMENT_DATE + " TEXT , " +
                TableNameAndColumnStatement.WEEKLY_OFF_DAY + " INTEGER, " +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER, " +


                //PRIMARY KEY ("unit_id", "barrack_id" +


                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_EMPLOYEE_UNIT +
                " FOREIGN KEY (" + TableNameAndColumnStatement.UNIT_ID + ") " +

                " REFERENCES " + TableNameAndColumnStatement.UNIT_TABLE +
                "(" + TableNameAndColumnStatement.UNIT_ID + ")," +

                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_EMPLOYEE_RANK +
                " FOREIGN KEY (" + TableNameAndColumnStatement.EMPLOYEE_RANK_ID + ") " +

                " REFERENCES " + TableNameAndColumnStatement.RANK_MASTER_TABLE +
                "(" + TableNameAndColumnStatement.RANK_ID + ")" +

                " CONSTRAINT " + TableNameAndColumnStatement.UQ_EMPLOYEE_NO_UNIT_EMPLOYEE +
                " unique (" + TableNameAndColumnStatement.EMPLOYEE_ID + " ASC," +
                TableNameAndColumnStatement.EMPLOYEE_NO + " ASC)" +
                ")";
        return unitemployeeTable;

    }

    public String unitequipmentTable() {

        String unitequipmentTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_EQUIPMENT_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_POST_ID + " INTEGER, " +
                TableNameAndColumnStatement.EQUIPMENT_COUNT + " INTEGER, " +
                TableNameAndColumnStatement.UNIQUE_NO + " TEXT(20), " +
                TableNameAndColumnStatement.MANUFACTURER + " TEXT(50), " +
                TableNameAndColumnStatement.EQUIPMENT_CODE + " TEXT(50), " +
                TableNameAndColumnStatement.IS_CUSTOMER_PROVIDED + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.EQUIPMENT_STATUS + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.EQUIPMENT_TYPE + " INTEGER(1), " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER , " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER , " +

                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_EQUIPMENT_UNIT +
                " FOREIGN KEY (" + TableNameAndColumnStatement.UNIT_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.UNIT_TABLE +
                "(" + TableNameAndColumnStatement.UNIT_ID + ")" +

               /* " CONSTRAINT " +  TableNameAndColumnStatement.UQ_UNIT_EQUIPMENT_ID +
                " unique (" +  TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + " ASC)" +*/
//                " CONSTRAINT " +  TableNameAndColumnStatement.UQ_UNIT_EQUIPMENT_ID +
//                " unique (" +  TableNameAndColumnStatement.UNIT_EQUIPMENT_ID + " ASC)" +
                ")";


        return unitequipmentTable;

    }

    public String unitPostTable() {
        String unitPostTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_POST_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_POST_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_POST_NAME + " TEXT(50) NOT NULL, " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(500), " +
                TableNameAndColumnStatement.ARMED_STRENGTH + " INTEGER, " +
                TableNameAndColumnStatement.UNARMED_STRENGTH + " INTEGER, " +
                TableNameAndColumnStatement.MAIN_GATE_DISTANCE + " REAL(7,3), " +
                TableNameAndColumnStatement.BARRACK_DISTANCE + " REAL(7,3), " +
                TableNameAndColumnStatement.UNIT_OFFICE_DISTANCE + " REAL(7,3), " +
                TableNameAndColumnStatement.IS_MAIN_GATE + " INTEGER(1) , " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER(1) NOT NULL, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.GEO_LATITUDE + " REAL(9,6) , " +
                TableNameAndColumnStatement.GEO_LONGITUDE + " REAL(9,6) , " +
                TableNameAndColumnStatement.POST_STATUS + " INTEGER , " +
                TableNameAndColumnStatement.DEVICE_NO + " TEXT(500), " +
                " CONSTRAINT " + TableNameAndColumnStatement.FK_UNIT_POST_UNIT +
                " FOREIGN KEY (" + TableNameAndColumnStatement.UNIT_ID + ") " +
                " REFERENCES " + TableNameAndColumnStatement.UNIT_TABLE +
                "(" + TableNameAndColumnStatement.UNIT_ID + ")" +
                ")";
        return unitPostTable;
    }

    public String unitBarrackStrength() {
        String unitBarrackStrength = " CREATE TABLE " + TableNameAndColumnStatement.UNIT_BARRACK_STRENGTH_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.GUARD_TYPE_ID + " INTEGER, " +
                TableNameAndColumnStatement.ACTUAL_STRENGTH + " INTEGER, " +
                TableNameAndColumnStatement.LEAVE + " INTEGER, " +
                TableNameAndColumnStatement.CHECKED_DATE_TIME + " INTEGER ," +
                TableNameAndColumnStatement.IS_NEW + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.TASK_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.IS_FILLED + " INTEGER NOT NULL" +

                ")";

        return unitBarrackStrength;
    }

    public String RotaTaskActivityList() {

        String rotaTaskActivityList = " CREATE TABLE " + TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_LIST + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ROTA_TASK_NAME + " TEXT(50), " +
                TableNameAndColumnStatement.ROTA_TASK_ACTIVITY_ID + " INTEGER, " +
                TableNameAndColumnStatement.ROTA_TASK_BLACK_ICON + " INTEGER, " +
                TableNameAndColumnStatement.ROTA_TASK_WHITE_ICON + " INTEGER " +

                ")";

        return rotaTaskActivityList;


    }

    public String RankMasterTable() {

        String rankMasterTable = " CREATE TABLE " + TableNameAndColumnStatement.RANK_MASTER_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.RANK_ID + " INTEGER NOT NULL , " +
                TableNameAndColumnStatement.RANK_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.RANK_ABBREVATION + " TEXT(50), " +
                TableNameAndColumnStatement.DESCRIPTIONS + " TEXT(500), " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER, " +
                TableNameAndColumnStatement.CAN_POSSESS_ARMS + " INTEGER, " +
                TableNameAndColumnStatement.RANK_CODE + " TEXT(100) )";

        return rankMasterTable;
    }

    public String LookUpModelTable() {

        String lookUpTable = " CREATE TABLE " + TableNameAndColumnStatement.LOOKUP_MODEL_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.LOOKUP_TYPE_ID + " INTEGER, " +
                TableNameAndColumnStatement.LOOKUP_IDENTIFIER + " INTEGER, " +
                TableNameAndColumnStatement.LOOKUP_CODE + " INTEGER, " +
                TableNameAndColumnStatement.LOOKUP_NAME + " TEXT(500), " +
                TableNameAndColumnStatement.LOOKUP_TYPE_NAME + " TEXT(500) " + ")";

        return lookUpTable;
    }

    public String issuesTable() {
        String issuesTable = " CREATE TABLE " + TableNameAndColumnStatement.ISSUES_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ISSUE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_MATRIX_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_TYPE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.SOURCE_TASK_ID + " INTEGER , " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER, " +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER, " +
                TableNameAndColumnStatement.GUARD_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_CATEGORY_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_MODE_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_NATURE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_CAUSE_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_SERVERITY_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_STATUS_ID + " INTEGER, " +
                TableNameAndColumnStatement.ACTION_PLAN_ID + " INTEGER, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.CREATED_BY_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ASSIGNED_TO_ID + " INTEGER, " +
                TableNameAndColumnStatement.TARGET_ACTION_END_DATE + " TEXT, " +
                TableNameAndColumnStatement.CLOSURE_END_DATE + " TEXT, " +
                TableNameAndColumnStatement.REMARKS + " TEXT, " +
                TableNameAndColumnStatement.CLOSURE_REMARKS + " TEXT ," +
                TableNameAndColumnStatement.UNIT_CONTACT_ID + " INTEGER, " +
                TableNameAndColumnStatement.UNIT_CONTACT_NAME + " TEXT, " +
                TableNameAndColumnStatement.ASSIGNED_TO_NAME + " TEXT, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER, " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER, " +
                TableNameAndColumnStatement.GUARD_CODE + " TEXT, " +
                TableNameAndColumnStatement.PHONE_NO + " TEXT(15), " +
                TableNameAndColumnStatement.GUARD_NAME + " TEXT (200) " +

                ")";
        return issuesTable;
    }

    public String barrackInspectionQuestionTable() {

        String barrackInspectionQuestionTable = "CREATE TABLE " + TableNameAndColumnStatement.BARRACK_INSPECTION_QUESTION_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.QUESTION_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.QUESTION + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.DESCRIPTION + " TEXT, " +
                TableNameAndColumnStatement.CONTROL_TYPE + " TEXT, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER " + ")";
        return barrackInspectionQuestionTable;
    }

    public String barrackInspectionAnswerTable() {
        String barrackInspectionAnswerTable = "CREATE TABLE " + TableNameAndColumnStatement.BARRACK_INSPECTION_ANSWER_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.QUESTION_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.OPTION_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.OPTION_NAME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.IS_MANDATORY + " INTEGER, " +
                TableNameAndColumnStatement.CONTROL_TYPE + " TEXT, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER " + ")";
        return barrackInspectionAnswerTable;
    }


    public String masterActionPlan() {

        String masterActionPlanTable = " CREATE TABLE " + TableNameAndColumnStatement.MASTER_ACTION_PLAN_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.NAME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER NOT NULL " +
                ")";

        return masterActionPlanTable;
    }

    public String issueMatrixTable() {
        String issuesMatrixTable = " CREATE TABLE " + TableNameAndColumnStatement.ISSUE_MATRIX_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ISSUE_TYPE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_CATEGORY_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ISSUE_SUB_CATEGORY_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUE_CAUSE_ID + " INTEGER, " +
                TableNameAndColumnStatement.MASTER_ACTION_PLAN_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.DEFAULT_ASSIGNMENNT_ROLE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_TYPE_ID + " INTEGER , " +
                TableNameAndColumnStatement.TURN_AROUND_TIME + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.PROOF_CONTROL_TYPE + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.MINIMUM_OPTIONAL_VALUE + " INTEGER " +
                ")";
        return issuesMatrixTable;

    }

    public String clientCoordinationTable() {
        String securityRiskquestionQuestionTable = "CREATE TABLE " + TableNameAndColumnStatement.client_coordination_table + "(" +
                TableNameAndColumnStatement.question_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.question + " TEXT, " +
                TableNameAndColumnStatement.is_active + " INTEGER " +
                ")";

        return securityRiskquestionQuestionTable;
    }


    public String kitItemTable() {
        String kitItemTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_ITEM_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.KIT_ITEM_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.KIT_ITEM_CODE + " TEXT, " +
                TableNameAndColumnStatement.KIT_ITEM_NAME + " TEXT, " +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER " +
                ")";

        return kitItemTable;
    }


    public String kitItemRequestTable() {
        String kitItemRequestTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_ITEM_REQUEST_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.GUARD_ID + " TEXT, " +
                TableNameAndColumnStatement.GUARD_CODE + " TEXT, " +
                TableNameAndColumnStatement.SOURCE_TASK_ID + " INTEGER, " +
                TableNameAndColumnStatement.REQUESTED_ON + " TEXT, " +
                TableNameAndColumnStatement.REQUESTED_STATUS + " TEXT, " +
                TableNameAndColumnStatement.REQUESTED_ITEMS + " TEXT, " +
                TableNameAndColumnStatement.KIT_ITEM_REQUEST_ID + " INTEGER, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER, " +
                TableNameAndColumnStatement.IS_NEW + " INTEGER " +
                ")";

        return kitItemRequestTable;
    }

    public String activityLogTable() {

        String activityLogTable = "CREATE TABLE " + TableNameAndColumnStatement.ACTIVITY_LOG_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.ACTIVITY + " TEXT, " +
                TableNameAndColumnStatement.UNIT_NAME + " TEXT, " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER, " +
                TableNameAndColumnStatement.TASK_NAME + " TEXT, " +
                TableNameAndColumnStatement.TASK_STATUS + " TEXT, " +
                TableNameAndColumnStatement.DATE + " TEXT, " +
                TableNameAndColumnStatement.TIME + " TEXT" +
                ")";

        return activityLogTable;
    }

    public String kitSizeTable() {

        String kitSizeTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_SIZE_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.KIT_SIZE_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.KIT_SIZE_CODE + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.KIT_SIZE_NAME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.KIT_SIZE_VALUE + " TEXT NOT NULL " +
                ")";

        return kitSizeTable;
    }


    public String kitDistributionTable() {

        String kitDistributionTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.TASK_ID + " INTEGER ," +
                TableNameAndColumnStatement.ISSUE_OFFICER_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.DISTRIBUTION_STATUS + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.RECIPIENT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.KIT_TYPE_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.BRANCH_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT , " +
                TableNameAndColumnStatement.GUARD_NAME + " TEXT , " +
                TableNameAndColumnStatement.GUARD_CODE + " TEXT , " +
                TableNameAndColumnStatement.DISTRIBUTION_DATE + " TEXT" +
                ")";

        return kitDistributionTable;
    }

    public String kitDistributionItemTable() {

        String kitDistributionItemTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ITEM_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.IS_ISSUED + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.IS_UNPAID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.NON_ISSUE_REASON_ID + " INTEGER, " +
                TableNameAndColumnStatement.ISSUED_DATE + " TEXT, " +
                TableNameAndColumnStatement.KIT_ITEM_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.KIT_SIZE_ID + " INTEGER," +
                TableNameAndColumnStatement.QUANTITY + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER" +
                ")";

        return kitDistributionItemTable;
    }

    public String kitItemSizeTable() {

        String kitItemSizeTable = "CREATE TABLE " + TableNameAndColumnStatement.KIT_ITEM_SIZE_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.KIT_ITEM_SIZE_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.KIT_ITEM_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.KIT_SIZE_ID + " INTEGER NOT NULL " +
                ")";

        return kitItemSizeTable;
    }

    public String notificationTable() {

        String notificationTable = "CREATE TABLE " + TableNameAndColumnStatement.NOTIFICATION_RECEIPT + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.RECIPIENT_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.CALLBACK_URL + " TEXT, " +
                TableNameAndColumnStatement.IS_SUCCESSFUL + " INTEGER, " +
                TableNameAndColumnStatement.NOTIFICATION_NAME + " TEXT, " +
                TableNameAndColumnStatement.NOTIFICATION_ID + " INTEGER, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER" +
                ")";

        return notificationTable;
    }

    public String dutyAttendanceTable() {

        String dutyAttendanceTable = "CREATE TABLE " + TableNameAndColumnStatement.DUTY_ATTENDANCE_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.DUTY_ON_DATE_TIME + " TEXT ," +
                TableNameAndColumnStatement.DUTY_OFF_DATE_TIME + " TEXT ," +
                TableNameAndColumnStatement.DUTY_REPONSE_ID + " TEXT ," +
                "TaskAssigned" + " TEXT ," +
                "TaskCompleted" + " TEXT ," +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER )";


        return dutyAttendanceTable;
    }

    public String equipmentListTable() {
        String equipmentListTable = "CREATE TABLE " + TableNameAndColumnStatement.EQUIPMENT_LIST_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.EQUIPMENT_NAME + " TEXT ," +
                TableNameAndColumnStatement.EQUIPMENT_CODE + " TEXT ," +
                TableNameAndColumnStatement.EQUIPMENT_TYPE_ID + " TEXT ," +
                TableNameAndColumnStatement.DESCRIPTION + " TEXT (500)," +
                TableNameAndColumnStatement.IS_ACTIVE + " INTEGER NOT NUll)";
        return equipmentListTable;
    }

    public String unitBillingCheckListTable() {
        String unitBillingCheckListTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_BILLING_CHECK_LIST_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NUll ," +
                TableNameAndColumnStatement.BILL_CHECK_LIST_ID + " INTEGER NOT NUll)";
        return unitBillingCheckListTable;
    }


    public String dependentSyncTable() {
        String dependentSyncTable = "CREATE TABLE " + TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.TASK_ID + " INTEGER  ," +
                TableNameAndColumnStatement.UNIT_POST_ID + " INTEGER  ," +
                TableNameAndColumnStatement.ISSUE_ID + " INTEGER  ," +
                TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " INTEGER  ," +
                TableNameAndColumnStatement.TASK_TYPE_ID + " INTEGER  ," +
                TableNameAndColumnStatement.TASK_STATUS_ID + " INTEGER  ," +
                TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED + " INTEGER  ," +
                TableNameAndColumnStatement.IS_ISSUES_SYNCED + " INTEGER  ," +
                TableNameAndColumnStatement.IS_IMAGES_SYNCED + " INTEGER  ," +
                TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED + " INTEGER  ," +
                TableNameAndColumnStatement.IS_METADATA_SYNCED + " INTEGER  ," +
                TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED + " INTEGER ," +
                TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED + " INTEGER ," +
                TableNameAndColumnStatement.CREATED_OR_EDITED + " INTEGER ," +
                TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED + " INTEGER ," +
                TableNameAndColumnStatement.UNIT_RAISING_ID + " INTEGER ," +
                TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " INTEGER ," +
                TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + " INTEGER ," +
                TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED + " INTEGER ," +
                TableNameAndColumnStatement.BARRACK_ID + " INTEGER ," +
                TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED + " INTEGER )";
        return dependentSyncTable;
    }


    public String notificationListTable() {

        String notificationListTable = " CREATE TABLE " + TableNameAndColumnStatement.NOTIFICATION_LIST + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.NOTIFICATION_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.NOTIFICATION_TYPE + " INTEGER, " +
                TableNameAndColumnStatement.CALLBACK_URL + " TEXT, " +
                TableNameAndColumnStatement.NAME + " TEXT, " +
                TableNameAndColumnStatement.TITLE + " TEXT, " +
                TableNameAndColumnStatement.RECEIVED_DATE_TIME + " INTEGER," +
                TableNameAndColumnStatement.UNIT_LIST + " TEXT, " +
                TableNameAndColumnStatement.DELETED_ID + " INTEGER, " +
                TableNameAndColumnStatement.PRIORITY + " TEXT, " +
                TableNameAndColumnStatement.MESSAGE + " TEXT, " +
                TableNameAndColumnStatement.APP_USER_ID + " INTEGER" + ")";

        return notificationListTable;
    }

    public String syncRequest() {

        String notificationListTable = " CREATE TABLE " + TableNameAndColumnStatement.SYNC_REQUEST
                + "(" + TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.SYNC_START_DATE_TIME + " TEXT NOT NULL )";

        return notificationListTable;
    }

    public String unitRaising() {

        String unitRaisingTable = " CREATE TABLE " + TableNameAndColumnStatement.UNIT_RAISING + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RAISING_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.RAISING_DETAILS + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RAISING_LOCATION + " TEXT(100), " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.REMARKS + " TEXT, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER " +
                ")";

        return unitRaisingTable;
    }

    public String unitRaisingDeferred() {

        String unitRaisingDeferredDetail = " CREATE TABLE " + TableNameAndColumnStatement.UNIT_RAISING_DEFERRED
                + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.DEFERRED_REASON + " TEXT, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.NEW_RAISING_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.REMARKS + " TEXT, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER " +
                ")";
        return unitRaisingDeferredDetail;
    }


    public String unitRaisingCancellation() {

        String unitRaisingCancellation = " CREATE TABLE " + TableNameAndColumnStatement.UNIT_RAISING_CANCELLED
                + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.EXPECTED_RAISING_DATE_TIME + " TEXT NOT NULL, " +
                TableNameAndColumnStatement.CANCELLATION_REASON + " TEXT, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.REMARKS + " TEXT, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER " +
                ")";

        return unitRaisingCancellation;
    }


    public String getUnitRaisingStrengthDetail() {

        String unitRaisingStrengthDetail = " CREATE TABLE " + TableNameAndColumnStatement.UNIT_RAISING_STRENGTH_DETAIL
                + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_RAISING_ID + " INTEGER NOT NULL," +
                TableNameAndColumnStatement.RANK_ID + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.ACTUAL_STRENGTH + " INTEGER NOT NULL, " +
                TableNameAndColumnStatement.CREATED_DATE_TIME + " TEXT, " +
                TableNameAndColumnStatement.IS_SYNCED + " INTEGER " +
                ")";

        return unitRaisingStrengthDetail;
    }

    //@Ashu
    public String queryExecutorTable() {

        String queryExecutor = " CREATE TABLE " + TableNameAndColumnStatement.QUERY_TABLE_NAME
                + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.QUERY_ID + " INTEGER , " +
                TableNameAndColumnStatement.QUERY_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.EMPLOYEE_ID + " INTEGER, " +
                TableNameAndColumnStatement.QUERY + " TEXT, " +
                TableNameAndColumnStatement.NOTIFICATION_STATUS + " TINYINT(1) default '2', " +
                TableNameAndColumnStatement.RESULT_STATUS + " TINYINT(1) default '2', " +
                TableNameAndColumnStatement.QUERY_RESULT + " TEXT, " +
                TableNameAndColumnStatement.QUERY_STATUS + " TINYINT(1) default '2' ," +
                TableNameAndColumnStatement.IS_SYNCED + " TINYINT(1) default '0' " +
                ")";

        return queryExecutor;
    }

    public String guardTable() {
        String guardTable = " CREATE TABLE " + TableNameAndColumnStatement.GUARDS_TABLE
                + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableNameAndColumnStatement.UNIT_ID + " INTEGER," +
                TableNameAndColumnStatement.GUARD_NAME + " TEXT(100), " +
                TableNameAndColumnStatement.GUARD_CODE + " TEXT, " +
                TableNameAndColumnStatement.GUARD_ID + " INTEGER, " +
                TableNameAndColumnStatement.FINE_INSTANCE + " INTEGER, " +
                TableNameAndColumnStatement.FINE_AMOUNT + " INTEGER, " +
                TableNameAndColumnStatement.REWARD_AMOUNT + " INTEGER, " +
                TableNameAndColumnStatement.LAST_FINE + " INTEGER " +
                ")";
        return guardTable;
    }

    public String unitTypeTable() {
        String unitTypeTable = "CREATE TABLE " + TableNameAndColumnStatement.UNIT_TYPE_TABLE + "(" +
                TableNameAndColumnStatement.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "UnitTypeId INTEGER, " +
                "Name TEXT(50), " +
                "Description TEXT, " +
                "MinGuardSize INTEGER, " +
                "MaxGuardSize INTEGER, " +
                "CheckLimit INTEGER)";
        return unitTypeTable;
    }
}
