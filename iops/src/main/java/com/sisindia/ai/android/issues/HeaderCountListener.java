package com.sisindia.ai.android.issues;

/**
 * Created by Shushrut on 11-08-2016.
 */
public interface HeaderCountListener {

    public void updateHeaderCount();
}
