package com.sisindia.ai.android.fcmnotification.fcmunits;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.GenericUpdateTableMO;
import com.sisindia.ai.android.database.SISAITrackingDB;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.myunits.models.UnitShiftRank;

/**
 * Created by shankar on 1/12/16.
 */

public class FCMUnitStrengthModelInsertion extends SISAITrackingDB {
    private SQLiteDatabase sqlite = null;
    private boolean isInsertedSuccessfully;

    public FCMUnitStrengthModelInsertion(Context context) {
        super(context);
    }

    public synchronized boolean insertUnitStrength(UnitShiftRank unitShiftRankList) {

        sqlite = this.getWritableDatabase();

        synchronized (sqlite) {
            try {
                if (unitShiftRankList != null) {
                    sqlite.beginTransaction();
                    insertUnitStrengthModel(unitShiftRankList);
                    sqlite.setTransactionSuccessful();
                    isInsertedSuccessfully = true;
                } else {
                    isInsertedSuccessfully = false;
                }
            } catch (Exception exception) {
                Crashlytics.logException(exception);
                isInsertedSuccessfully = false;
            } finally {
                sqlite.endTransaction();
            }
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            return isInsertedSuccessfully;
        }
    }

    private void insertUnitStrengthModel(UnitShiftRank unitShiftRank) {

        ContentValues values = new ContentValues();
        values.put(TableNameAndColumnStatement.UNIT_SHIFT_RANK_ID, unitShiftRank.getId());
        values.put(TableNameAndColumnStatement.UNIT_ID, unitShiftRank.getUnitId());
        values.put(TableNameAndColumnStatement.SHIFT_ID, unitShiftRank.getShiftId());
        values.put(TableNameAndColumnStatement.RANK_ID, unitShiftRank.getRankId());
        values.put(TableNameAndColumnStatement.RANK_COUNT, unitShiftRank.getRankCount());
        values.put(TableNameAndColumnStatement.RANK_ABBREVATION, unitShiftRank.getRankAbbrevation());
        values.put(TableNameAndColumnStatement.BARRACK_ID, unitShiftRank.getBarrackId());
        values.put(TableNameAndColumnStatement.UNIT_NAME, unitShiftRank.getUnitName());
        values.put(TableNameAndColumnStatement.SHIFT_NAME, unitShiftRank.getShiftName());
        values.put(TableNameAndColumnStatement.RANK_NAME, unitShiftRank.getRankName());
        values.put(TableNameAndColumnStatement.BARRACK_NAME, unitShiftRank.getBarrackName());
        values.put(TableNameAndColumnStatement.STRENGTH, unitShiftRank.getStrength());
        values.put(TableNameAndColumnStatement.ACTUAL, 0);
        values.put(TableNameAndColumnStatement.LEAVE_COUNT, 0);
        values.put(TableNameAndColumnStatement.IS_ARMED, unitShiftRank.getIsArmed());
        values.put(TableNameAndColumnStatement.IS_SYNCED, 1);

        GenericUpdateTableMO genericUpdateTableMO = IOPSApplication.getInstance().getSelectionStatementDBInstance().
                getTableSeqId(unitShiftRank.getUnitId(), unitShiftRank.getRankId(), TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, sqlite);
        if (genericUpdateTableMO.getIsAvailable() != 0) {
            /*IOPSApplication.getInstance().getDeletionStatementDBInstance().
                    deleteTableById(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, genericUpdateTableMO.getId(), sqlite);*/
            String whereClause = "unit_id=" + unitShiftRank.getUnitId() + " and rank_id=" + unitShiftRank.getRankId();
            sqlite.delete(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, whereClause, null);
            sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);

        } else {
            sqlite.insertOrThrow(TableNameAndColumnStatement.UNIT_AUTHORIZED_STRENGHT_TABLE, null, values);
        }
    }
}
