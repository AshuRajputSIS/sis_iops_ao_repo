package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hannan Shaik on 29/06/16.
 */

public class ApiResponse<T> {
    @SerializedName("StatusCode")
    public int statusCode;
    @SerializedName("StatusMessage")
    public String statusMessage;

    @SerializedName("Data")
    public T data;

    @Override
    public String toString() {
        return "ApiResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", data=" + data +
                '}';
    }
}
