package com.sisindia.ai.android.rota.daycheck;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;

import java.util.ArrayList;

/**
 * Created by shushruth on 27/3/16.
 */
public class GuardsRecyclerViewAdapter extends RecyclerView.Adapter<GuardsRecyclerViewAdapter.GuardViewHolder> {
    private ArrayList<String> feedItemList;
    private Context mContext;

    public GuardsRecyclerViewAdapter(Context context, ArrayList<String> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }


    @Override
    public GuardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.guard_row, null);
        GuardViewHolder viewHolder = new GuardViewHolder(view);
        viewHolder.mGuardName = (TextView) view.findViewById(R.id.guard_name);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GuardViewHolder customViewHolder, int i) {

        customViewHolder.mGuardName.setText(feedItemList.get(i));

        /*FeedItem feedItem = feedItemList.get(i);

        //Download image using picasso library
        Picasso.with(mContext).load(feedItem.getThumbnail())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(customViewHolder.imageView);

        //Setting text view title
        customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));*/
    }

    @Override
    public int getItemCount() {
        return feedItemList.size();
    }

    public class GuardViewHolder extends RecyclerView.ViewHolder {
        ImageView mGuardPic;
        TextView mGuardName;

        public GuardViewHolder(View v) {
            super(v);

        }

        public ImageView getmGuardPic() {
            return mGuardPic;
        }

        public void setmGuardPic(ImageView mGuardPic) {
            this.mGuardPic = mGuardPic;
        }

        public TextView getmGuardName() {
            return mGuardName;
        }

        public void setmGuardName(TextView mGuardName) {
            this.mGuardName = mGuardName;
        }
    }
}
