package com.sisindia.ai.android.unitatriskpoa.modelobjects;

/**
 * Created by shankar on 19/7/16.
 */

public class UnitRiskPendingCountMO {

    private int completedCount;
    private int totalCount;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(int completedCount) {
        this.completedCount = completedCount;
    }
}
