package com.sisindia.ai.android.mybarrack;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.grievances.AddIssuesGrievanceActivity;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by shruti on 27/7/16.
 */
public class TabOthersFragments extends BaseFragment implements View.OnClickListener {
    @Bind(R.id.tv_barrack_photos)
    CustomFontTextview tvBarrackPhotos;

    @Bind(R.id.barrack_bedding_take_photo)
    ImageView barrackBeddingTakePhoto;

    @Bind(R.id.barrack_kit_take_photo)
    ImageView barrackKitTakePhoto;

    @Bind(R.id.barrack_mess_take_photo)
    ImageView barrackMessTakePhoto;

    @Bind(R.id.barrack_others_take_photo)
    ImageView barrackOthersTakePhoto;

    @Bind(R.id.tv_barrack_remarks)
    CustomFontTextview tvBarrackRemarks;
    @Bind(R.id.edittext_rgts_remarks)
    CustomFontEditText edittextRgtsRemarks;

    @Bind(R.id.layout_parent)
    LinearLayout layoutParent;
    @Bind(R.id.remark_layout)
    RelativeLayout remarkLayout;
    @Bind(R.id.barrack_addGrivance)
    CustomFontButton barrackAddGrivance;
    @Bind(R.id.barrack_addImprovementPlant)
    CustomFontButton addImprovementPlant;
   /* @Bind(R.id.barrack_addImprovementPlant)
    CustomFontButton barrackAddImprovementPlant;*/

    //    private boolean isTakenAtleastOnepic;
//    private BarrackStrengthMO barrackStrengthMO;
//    private Resources resources;
    private String barrackName;
    private int unitId;
    private int barrackId;
    private ArrayList<IssuesMO> greivanceList = new ArrayList<>();
    private int taskId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_others_fragment, container, false);
        Util.hideKeyboard(getActivity());
//        resources= getResources();
        if (null != getArguments()) {
//            barrackStrengthMO = (BarrackStrengthMO) getArguments().getSerializable("barrackStrengthMO");
            barrackName = getArguments().getString("barrackName");
            unitId = getArguments().getInt("unitId");
            barrackId = getArguments().getInt("barrackId");
            taskId = getArguments().getInt("taskId");
        }
        setUpUI(view);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.tab_others_fragment;
    }

    private void setUpUI(View view) {

        ButterKnife.bind(this, view);
        edittextRgtsRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((BarrackInspectionTabActivity) getActivity()).checkOthersTabEntry(remarkLayout);
        barrackBeddingTakePhoto.setOnClickListener(this);
        barrackKitTakePhoto.setOnClickListener(this);
        barrackMessTakePhoto.setOnClickListener(this);
        barrackOthersTakePhoto.setOnClickListener(this);

        if (null != ((BarrackInspectionTabActivity) getActivity()).imageList && ((BarrackInspectionTabActivity) getActivity()).imageList.size() != 0) {

            Iterator<Map.Entry<Integer, AttachmentMetaDataModel>> iter = ((BarrackInspectionTabActivity) getActivity()).imageList.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<Integer, AttachmentMetaDataModel> entry = iter.next();
                switch (entry.getKey()) {
                    case Constants.BARRACK_BEDDING_IMAGE_CODE:
                        if (!entry.getValue().getAttachmentPath().isEmpty())
                            barrackBeddingTakePhoto.setImageURI(Uri.parse(entry.getValue().getAttachmentPath()));
                        break;
                    case Constants.BARRACK_KIT_IMAGE_CODE:
                        if (!entry.getValue().getAttachmentPath().isEmpty())
                            barrackKitTakePhoto.setImageURI(Uri.parse(entry.getValue().getAttachmentPath()));
                        break;
                    case Constants.BARRACK_MESS_IMAGE_CODE:
                        if (!entry.getValue().getAttachmentPath().isEmpty())
                            barrackMessTakePhoto.setImageURI(Uri.parse(entry.getValue().getAttachmentPath()));
                        break;
                    case Constants.BARRACK_OTHERS_IMAGE_CODE:
                        if (!entry.getValue().getAttachmentPath().isEmpty())
                            barrackOthersTakePhoto.setImageURI(Uri.parse(entry.getValue().getAttachmentPath()));
                        break;
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == Constants.BARRACK_GREIVANCE_CODE && resultCode == RESULT_OK) {
                greivanceList.clear();
                greivanceList = data.getParcelableArrayListExtra("Grievance");
                if (null != greivanceList && greivanceList.size() != 0) {
                    ((BarrackInspectionTabActivity) getActivity()).greivanceInsertion(greivanceList);

                    // UPDATING 'ADD GRIEVANCE' BUTTON ONCE, GRIEVANCE IS ADDED BY THE AI
                    if (data.hasExtra("IS_GRIEVANCE_ADDED")) {
                        if (data.getBooleanExtra("IS_GRIEVANCE_ADDED", false)) {
                            barrackAddGrivance.setText("GRIEVANCE ADDED");
                            barrackAddGrivance.setBackgroundResource(R.drawable.green_round_corner);
                        }
                    }
                }
                AttachmentMetaDataModel attachmentModel = null;
                attachmentModel = data.getParcelableExtra(Constants.META_DATA);
               /* attachmentModel.setAttachmentSourceId(taskId);
                attachmentModel.setAttachmentId(0);*/

                ((BarrackInspectionTabActivity) getActivity()).metaDataSetUp(requestCode, attachmentModel);

            } else {
                String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
                AttachmentMetaDataModel attachmentModel = null;
                attachmentModel = data.getParcelableExtra(Constants.META_DATA);
                ((BarrackInspectionTabActivity) getActivity()).metaDataSetUp(requestCode, attachmentModel);

                switch (requestCode) {

                    case Constants.BARRACK_BEDDING_IMAGE_CODE:
                        barrackBeddingTakePhoto.setImageURI(Uri.parse(attachmentModel.getAttachmentPath()));
                        break;
                    case Constants.BARRACK_KIT_IMAGE_CODE:
                        barrackKitTakePhoto.setImageURI(Uri.parse(attachmentModel.getAttachmentPath()));
                        break;
                    case Constants.BARRACK_MESS_IMAGE_CODE:
                        barrackMessTakePhoto.setImageURI(Uri.parse(attachmentModel.getAttachmentPath()));
                        break;
                    case Constants.BARRACK_OTHERS_IMAGE_CODE:
                        barrackOthersTakePhoto.setImageURI(Uri.parse(attachmentModel.getAttachmentPath()));
                        break;
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.barrack_addGrivance, R.id.barrack_bedding_take_photo, R.id.barrack_kit_take_photo,
            R.id.barrack_mess_take_photo, R.id.barrack_others_take_photo, R.id.barrack_addImprovementPlant})
    public void onClick(View view) {
        Intent capturePictureActivity = null;
        switch (view.getId()) {
            case R.id.barrack_addGrivance:
                Intent intent = new Intent(getActivity(), AddIssuesGrievanceActivity.class);
                intent.putExtra("FromMyBarrack", true);
                intent.putExtra("BarrackId", barrackId);
                intent.putExtra("taskId", taskId);
                intent.putExtra(TableNameAndColumnStatement.UNIT_ID, unitId);
                //intent.putExtra(Constants.CHECKING_TYPE, rotaTaskListMO);
                startActivityForResult(intent, Constants.BARRACK_GREIVANCE_CODE);
                break;

            case R.id.barrack_bedding_take_photo:
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_BEDDING);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_BEDDING_IMAGE_CODE);
                break;

            case R.id.barrack_kit_take_photo:
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_KIT);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_KIT_IMAGE_CODE);
                break;

            case R.id.barrack_mess_take_photo:
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_MESS);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_MESS_IMAGE_CODE);
                break;

            case R.id.barrack_others_take_photo:
                capturePictureActivity = new Intent(getContext(), CapturePictureActivity.class);
                capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                Util.setSendPhotoImageTo(Util.BARRACK_OTHERS);
                Util.initMetaDataArray(true);
                startActivityForResult(capturePictureActivity, Constants.BARRACK_OTHERS_IMAGE_CODE);
                break;

            case R.id.barrack_addImprovementPlant:
                Intent i = new Intent(getActivity(), BarrackAddImprovementPlanAcitvity.class);
                i.putExtra(TableNameAndColumnStatement.BARRACK_ID, barrackId);
                i.putExtra(TableNameAndColumnStatement.BARRACK_NAME, barrackName);
                i.putExtra(TableNameAndColumnStatement.SOURCE_TASK_ID, taskId);
                startActivity(i);
                break;
        }
    }
}
