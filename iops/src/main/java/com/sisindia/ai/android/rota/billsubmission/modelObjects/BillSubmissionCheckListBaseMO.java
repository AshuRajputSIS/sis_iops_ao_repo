package com.sisindia.ai.android.rota.billsubmission.modelObjects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shruti on 28/6/16.
 */
public class BillSubmissionCheckListBaseMO implements Serializable {

    private List<BillSubmissionCheckListItemMO> billSubmissionCheckListMO;

    public List<BillSubmissionCheckListItemMO> getBillSubmissionCheckListMO() {
        return billSubmissionCheckListMO;
    }

    public void setBillSubmissionCheckListMO(List<BillSubmissionCheckListItemMO> billSubmissionCheckListMO) {
        this.billSubmissionCheckListMO = billSubmissionCheckListMO;
    }


}
