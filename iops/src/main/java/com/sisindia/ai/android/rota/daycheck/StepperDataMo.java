package com.sisindia.ai.android.rota.daycheck;

import java.util.HashMap;

/**
 * Created by Shushrut on 21-06-2016.
 */
public class StepperDataMo {


    public static StepperDataMo mStepperDataMo;
    public static HashMap<String,Integer> mStepperDataHolder;

    public static StepperDataMo getmStepperDataMoInstance(){
        if(mStepperDataMo == null){
            mStepperDataMo = new StepperDataMo();
        }
        if(mStepperDataHolder == null || mStepperDataHolder.size()== 0){
            mStepperDataHolder = new HashMap<>();
        }
        return mStepperDataMo;
    }

    public  static void clearStepper(){
        mStepperDataMo = null;
        if(mStepperDataHolder != null){
            mStepperDataHolder.clear();
        }
    }

    public void addStepperUpdatePosition(String activity ,int position){
        mStepperDataHolder.put(activity,position);
    }
    public HashMap<String,Integer> getmStepperDataHolder(){

        return mStepperDataHolder;
    }

    public void removeItemFromStepper(String activityName){

        if(null!=mStepperDataHolder && mStepperDataHolder.size()!=0){
          if(mStepperDataHolder.containsKey(activityName)) {
              mStepperDataHolder.remove(activityName);
          }
        }
    }
}
