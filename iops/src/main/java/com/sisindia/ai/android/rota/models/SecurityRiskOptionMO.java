package com.sisindia.ai.android.rota.models;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskOptionMO {

    private Integer id;
    private Integer securityRiskQuestionId;
    private Integer securityRiskOptionId;
    private String securityRiskOptionName;
    private Integer isMandatory;
    private String controlType;
    private Integer isActive;
    private String mUserComments;

    public String getmUserComments() {
        return mUserComments;
    }

    public void setmUserComments(String mUserComments) {
        this.mUserComments = mUserComments;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The securityRiskQuestionId
     */
    public Integer getSecurityRiskQuestionId() {
        return securityRiskQuestionId;
    }

    /**
     * @param securityRiskQuestionId The SecurityRiskQuestionId
     */
    public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
        this.securityRiskQuestionId = securityRiskQuestionId;
    }

    /**
     * @return The securityRiskOptionId
     */
    public Integer getSecurityRiskOptionId() {
        return securityRiskOptionId;
    }

    /**
     * @param securityRiskOptionId The SecurityRiskOptionId
     */
    public void setSecurityRiskOptionId(Integer securityRiskOptionId) {
        this.securityRiskOptionId = securityRiskOptionId;
    }

    /**
     * @return The securityRiskOptionName
     */
    public String getSecurityRiskOptionName() {
        return securityRiskOptionName;
    }

    /**
     * @param securityRiskOptionName The SecurityRiskOptionName
     */
    public void setSecurityRiskOptionName(String securityRiskOptionName) {
        this.securityRiskOptionName = securityRiskOptionName;
    }

    /**
     * @return The isMandatory
     */
    public Integer getIsMandatory() {
        return isMandatory;
    }

    /**
     * @param isMandatory The IsMandatory
     */
    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }

    /**
     * @return The controlType
     */
    public String getControlType() {
        return controlType;
    }

    /**
     * @param controlType The ControlType
     */
    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    /**
     * @return The isActive
     */
    public Integer getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "SecurityRiskOptionMO{" +
                "id=" + id +
                ", securityRiskQuestionId=" + securityRiskQuestionId +
                ", securityRiskOptionId=" + securityRiskOptionId +
                ", securityRiskOptionName='" + securityRiskOptionName + '\'' +
                ", isMandatory=" + isMandatory +
                ", controlType='" + controlType + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}