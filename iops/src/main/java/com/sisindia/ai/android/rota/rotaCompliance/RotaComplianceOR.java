package com.sisindia.ai.android.rota.rotaCompliance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Durga Prasad on 27-01-2017.
 */

public class RotaComplianceOR implements Parcelable {

    @SerializedName("StatusMessage")
    public String statusMessage;

    @SerializedName("StatusCode")
    public  int statusCode;

    @SerializedName("Data")
    public RotaCompliance rotaComplianceData;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.statusMessage);
        dest.writeInt(this.statusCode);
        dest.writeParcelable(this.rotaComplianceData, flags);
    }

    public RotaComplianceOR() {
    }

    protected RotaComplianceOR(Parcel in) {
        this.statusMessage = in.readString();
        this.statusCode = in.readInt();
        this.rotaComplianceData = in.readParcelable(RotaCompliance.class.getClassLoader());
    }

    public static final Creator<RotaComplianceOR> CREATOR = new Creator<RotaComplianceOR>() {
        @Override
        public RotaComplianceOR createFromParcel(Parcel source) {
            return new RotaComplianceOR(source);
        }

        @Override
        public RotaComplianceOR[] newArray(int size) {
            return new RotaComplianceOR[size];
        }
    };
}
