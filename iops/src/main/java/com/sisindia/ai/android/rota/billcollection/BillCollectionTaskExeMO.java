package com.sisindia.ai.android.rota.billcollection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shruti on 8/7/16.
 */
public class BillCollectionTaskExeMO {

    @SerializedName("GEOLocation")
    private String geoLocation ;

    @SerializedName("BillCollection")
    @Expose
    private List<BillCollectionMO> billCollection = new ArrayList<BillCollectionMO>();

    /**
     *
     * @return
     * The billCollection
     */
    public List<BillCollectionMO> getBillCollection() {
        return billCollection;
    }

    /**
     *
     * @param billCollection
     * The BillCollection
     */
    public void setBillCollection(List<BillCollectionMO> billCollection) {
        this.billCollection = billCollection;
    }

    public String getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(String geoLocation) {
        this.geoLocation = geoLocation;
    }
}
