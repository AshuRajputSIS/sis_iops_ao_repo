package com.sisindia.ai.android.myunits;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Saruchi on 18-07-2016.
 */

public interface UpdateMyUnitsFragment  {
    void updateUnitsFragment();
}
