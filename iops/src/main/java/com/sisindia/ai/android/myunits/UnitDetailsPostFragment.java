package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Durga Prasad on 17-05-2016.
 */
public class UnitDetailsPostFragment extends Fragment implements OnRecyclerViewItemClickListener, UpdatePostTabListener {
    private static final String TITLE = "param1";
    private static final String POSITION = "param2";
    private static UnitDetailsPostFragment fragment;
    //    private String siteTabTitle;
//    private String screenName;
    private ArrayList<Object> postData = new ArrayList<>();
    private Drawable dividerDrawable;
    private UnitDetailsPostAdapter unitDetailsPostAdapter;
    private RecyclerView postCheckRecyler;
    private LinearLayout noPostLinearLayout;
    private MyUnitHomeMO unitDetails;
    private AppPreferences appPreferences;

    public UnitDetailsPostFragment() {
    }

    public static UnitDetailsPostFragment newInstance(String param1, int param2) {
        if (fragment == null) {
            fragment = new UnitDetailsPostFragment();
            Bundle args = new Bundle();
            args.putString(TITLE, param1);
            args.putInt(POSITION, param2);
            fragment.setArguments(args);
        }
        return fragment;
    }

    public void setPostData(ArrayList postData) {
        this.postData = postData;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            siteTabTitle = getArguments().getString(TITLE);
            appPreferences = new AppPreferences(getActivity());
            unitDetails = (MyUnitHomeMO) SingletonUnitDetails.getInstance().getUnitDetails();
            if (unitDetails == null) {
                appPreferences.getRotaTaskJsonData();

            }
            /*if(getArguments().getString(POSITION)!=null){
                position = Integer.parseInt(getArguments().getString(POSITION));
            }*/
            postData = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchUnitPosts(unitDetails.getUnitId());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_check, container, false);
        postCheckRecyler = (RecyclerView) view.findViewById(R.id.post_check_recyclerview);
        noPostLinearLayout = (LinearLayout) view.findViewById(R.id.no_posts_linear_layout);
        postCheckRecyler.setVisibility(View.GONE);
        noPostLinearLayout.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        postCheckRecyler.setLayoutManager(linearLayoutManager);
        postCheckRecyler.setItemAnimator(new DefaultItemAnimator());
        unitDetailsPostAdapter = new UnitDetailsPostAdapter(getActivity());
        unitDetailsPostAdapter.setPostListData(postData);
        postCheckRecyler.setAdapter(unitDetailsPostAdapter);
        dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
        postCheckRecyler.addItemDecoration(dividerItemDecoration);
        unitDetailsPostAdapter.setOnRecyclerViewItemClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (postData.size() > 0 && unitDetailsPostAdapter != null) {
            postCheckRecyler.setVisibility(View.VISIBLE);
            noPostLinearLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        PostData postdata = (PostData) unitDetailsPostAdapter.getObject(position);
        Timber.d("postdata  %s", postdata);
        Intent editPostIntent = new Intent(getActivity(), UnitPostAddEditActivity.class);
        editPostIntent.putExtra(getResources().getString(R.string.post_action_type), getResources().getString(R.string.post_edit));
        editPostIntent.putExtra(getResources().getString(R.string.post_entity), postdata);
        startActivityForResult(editPostIntent, Constants.MYUNITS_ADD_POST_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.MYUNITS_ADD_POST_REQUEST_CODE) {
            if (unitDetailsPostAdapter != null) {
                postData = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchUnitPosts(unitDetails.getUnitId());
                postCheckRecyler.setVisibility(View.VISIBLE);
                noPostLinearLayout.setVisibility(View.GONE);
                unitDetailsPostAdapter.setPostListData(postData);
                unitDetailsPostAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void updatePostTab() {
        if (unitDetailsPostAdapter != null) {
            postData = IOPSApplication.getInstance().getSelectionStatementDBInstance().fetchUnitPosts(unitDetails.getUnitId());
            postCheckRecyler.setVisibility(View.VISIBLE);
            noPostLinearLayout.setVisibility(View.GONE);
            unitDetailsPostAdapter.setPostListData(postData);
            unitDetailsPostAdapter.notifyDataSetChanged();
        }
    }
}