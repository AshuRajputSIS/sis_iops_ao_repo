package com.sisindia.ai.android.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.CurrentMonth;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.PreviousMonth;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Saruchi on 20-04-2016.
 */
public class BarChartView extends BaseFrame {
    @Bind(R.id.bar_chart)
    BarChart mChart;

    @Bind(R.id.current_month_graph)
    VerticalBarGraph mCurrentMonthData;
    @Bind(R.id.previous_month_graph)
    VerticalBarGraph mPreviousMonthData;


    @Bind(R.id.barLinearlayout)
    LinearLayout barLinearlayout;

    public BarChartView(Context context) {
        super(context);
        init(context, null);
    }

    public BarChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }



    private void init(Context context, AttributeSet attrs) {
        Timber.d("init");
        LayoutInflater.from(getContext()).inflate(R.layout.barchart, this);
        ButterKnife.bind(this);
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(4f, 0));
        entries.add(new BarEntry(8f, 1));
        entries.add(new BarEntry(6f, 2));
        entries.add(new BarEntry(12f, 3));
        entries.add(new BarEntry(18f, 4));
        entries.add(new BarEntry(9f, 5));
        BarDataSet dataset = new BarDataSet(entries, "# of Calls");
        // creating labels
        ArrayList<String> labels = new ArrayList<String>();
// creating labels
        labels.add("January");
        labels.add("February");
        labels.add("March");
        labels.add("April");
        labels.add("May");
        labels.add("June");
        BarData data = new BarData(labels, dataset);
        mChart.setData(data); // set the data and list of lables into chart
        mChart.setDescription("Description");  // set the description



    }
    public void setBarChartData(CurrentMonth currentMonth, PreviousMonth previousMonth){
    mCurrentMonthData.setRecommendedCount(currentMonth.getRecommended());
    mCurrentMonthData.setRejectedCount(currentMonth.getRejected());
    mCurrentMonthData.setSelectedCount(currentMonth.getSelected());
    mCurrentMonthData.setInProgressCount(currentMonth.getInprocess());
    mCurrentMonthData.setCurrentMonth(currentMonth.getMonth(),currentMonth.getYear());
    mPreviousMonthData.setRecommendedCount(previousMonth.getRecommended());
    mPreviousMonthData.setRejectedCount(previousMonth.getRejected());
    mPreviousMonthData.setSelectedCount(previousMonth.getSelected());
    mPreviousMonthData.setInProgressCount(previousMonth.getInprocess());
   mPreviousMonthData.setCurrentMonth(previousMonth.getMonth(),previousMonth.getYear());

}

}
