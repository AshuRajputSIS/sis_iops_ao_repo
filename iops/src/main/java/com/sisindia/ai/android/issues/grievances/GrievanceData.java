package com.sisindia.ai.android.issues.grievances;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Durga Prasad on 10-11-2016.
 */
public class GrievanceData{

    private  int openIssueCount;
    private ArrayList<Object> grievanceList;


    public ArrayList<Object> getGrievanceList() {
        return grievanceList;
    }

    public void setGrievanceList(ArrayList<Object> grievanceList) {
        this.grievanceList = grievanceList;
    }

    public int getOpenIssueCount() {
        return openIssueCount;
    }

    public void setOpenIssueCount(int openIssueCount) {
        this.openIssueCount = openIssueCount;
    }


}
