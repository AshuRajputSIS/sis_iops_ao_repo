package com.sisindia.ai.android.unitraising.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.unitraising.RaisingDetailsMo;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.sisindia.ai.android.R.string.unitRaisingDetail;

/**
 * Created by Ashu Rajput on 7/26/2017.
 */

public class UnitRaisingDetailsAdapter extends
        RecyclerView.Adapter<UnitRaisingDetailsAdapter.UnitRaisingViewHolder> {

    private LayoutInflater layoutInflater = null;
    private int selected_position = -1;
    private OnRaisingUnitsClickListener listener = null;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    public ArrayList<RaisingDetailsMo> unitRaisingDetails;
    private boolean isEnabled;

    public UnitRaisingDetailsAdapter(Context context, OnRaisingUnitsClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public void setUnitRaisingDetailsList(ArrayList<RaisingDetailsMo> unitRaisingDetails){
      this.unitRaisingDetails   = unitRaisingDetails;
    }
    @Override
    public UnitRaisingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.unit_raising_starting_option_row, parent, false);
        UnitRaisingViewHolder holder = new UnitRaisingViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(UnitRaisingViewHolder holder, int position) {

         holder.unitRaisingDate.setText(dateTimeFormatConversionUtil
                 .convertDateToDayMonthDateFormat(unitRaisingDetails.get(position).expectedRaisingDate));
        holder.unitRaisingUnitName.setText(unitRaisingDetails.get(position).unitName);
        holder.unitCodeTv.setText(unitRaisingDetails.get(position).unitCode);
        if(unitRaisingDetails.get(position).raisingStatus == 2) {
            if (selected_position == position) {
                holder.itemView.setBackgroundColor(Color.LTGRAY);
                holder.tickImage.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
                holder.tickImage.setVisibility(View.GONE);
            }
        }
        else if(unitRaisingDetails.get(position).raisingStatus == 3){
            holder.itemView.setAlpha(0.3f);
            holder.itemView.setClickable(false);
        }

    }

    @Override
    public int getItemCount() {
        return unitRaisingDetails != null ? unitRaisingDetails.size() : 0;
    }

    public class UnitRaisingViewHolder extends RecyclerView.ViewHolder  {

        @Bind(R.id.unitRaisingUnitName)
        CustomFontTextview unitRaisingUnitName;
        @Bind(R.id.unit_raising_date)
        CustomFontTextview unitRaisingDate;
        @Bind(R.id.unit_code_tv)
        CustomFontTextview unitCodeTv;
        @Bind(R.id.tickImage)
        ImageView tickImage;

        public UnitRaisingViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemChanged(selected_position);
                    selected_position = getAdapterPosition();
                    notifyItemChanged(selected_position);
                        listener.onUnitSelectEvent(true, selected_position);


                    //CAN PASS MORE VALUE OR ITEM AS PER REQUIREMENT

                }
            });
        }


    }

    public interface OnRaisingUnitsClickListener {
        void onUnitSelectEvent(boolean isUnitSelected,int position);
    }

}
