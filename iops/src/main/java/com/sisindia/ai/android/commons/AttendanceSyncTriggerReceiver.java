package com.sisindia.ai.android.commons;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.syncadpter.DutyAttendanceSyncing;

/**
 * Created by compass on 9/8/2017.
 */

public class AttendanceSyncTriggerReceiver extends BroadcastReceiver {
    AppPreferences appPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "Alarm triggered for sync startup ", Toast.LENGTH_LONG).show();
        appPreferences = new AppPreferences(context);
        appPreferences.setFirstTimeAlarmConfiguredForAttendance(true);
        new DutyAttendanceSyncing(context);
    }
}
