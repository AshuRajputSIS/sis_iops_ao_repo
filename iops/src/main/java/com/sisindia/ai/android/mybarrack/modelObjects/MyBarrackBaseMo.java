package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by compass on 6/13/2017.
 */
public class MyBarrackBaseMo {

    @SerializedName("StatusCode")
    public Integer statusCode;
    @SerializedName("StatusMessage")
    public String statusMessage;
    @SerializedName("Data")
    public List<MyBarrack> data;

    @Override
    public String toString() {
        return "MyBarrackBaseMo{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", data=" + data +
                '}';
    }

}
