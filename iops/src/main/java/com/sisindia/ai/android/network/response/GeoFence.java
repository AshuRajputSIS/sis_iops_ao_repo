package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class GeoFence {

    @SerializedName("Geography")
    private Geography Geography;

    /**
     * @return The Geography
     */
    public Geography getGeography() {
        return Geography;
    }

    /**
     * @param Geography The Geography
     */
    public void setGeography(Geography Geography) {
        this.Geography = Geography;
    }

}