package com.sisindia.ai.android.syncadpter;
//http://stackoverflow.com/questions/33628060/contentresolver-requestsync-in-sync-adapter-is-not-working-in-android
/**
 * Created by Saruchi on 10-03-2016.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import com.sisindia.ai.android.debugingtool.DebugToolQueryResultSyncing;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String SYNC_FINISHED = "SyncFinished";
    private static final String SYNC_STARTED = "SyncStarted";
    // Global variables
    // Define a variable to contain a content resolver instance
    private ContentResolver mContentResolver;
    private Context context;


    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
        this.context = context;

    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        this.context = context;
        mContentResolver = context.getContentResolver();


    }

    /*
        * Specify the code you want to run in the sync adapter. The entire
        * sync adapter runs in a background thread, so you don't have to set
        * up your own background processing.
        */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

//        Timber.d("SyncAdapter Bundle  %s", extras);
//        Timber.d("SyncAdapter authority  %s", authority);
        //new RotaTaskSyncing(context);
//        Timber.d("SyncAdapter    %s", "Started  ============================================================================");
        independentSyncs();
//        Timber.d("SyncAdapter   %s", "Finished ============================================================================");
        SyncStatusNotification syncStatusNotification = new SyncStatusNotification(context);
        syncStatusNotification.tablesToSync();
    }

    private void independentSyncs() {
        //Independent Syncs
        new DutyAttendanceSyncing(context);
        new ContactSyncing(context);
        new UnitAtRiskPOASyncing(context);
        new RecruitmentTableSyncing(context);
        new NotificationSync(context);
        new DebugToolQueryResultSyncing(context);

        //Status  changes of issues and improvementPlan
        new IssuesStatusSyncing(context);
        new ImprovementPlanStatusSyncing(context);

        // MetaData and Image syncs(Successive syncs), adhoc issues, improvementplan
        new IssuesSyncing(context, true, -1);
        new ImprovementPlanSyncing(context, true, -1);

        // KitDistribution ---> MetaData Sync  --> Image Sync (Successive syncs)
        new KitDistributionItemSyncing(context);
        new KitNonIssueItemSyncing(context);

        //Unit post and equipment successive sync
        new UnitPostSyncing(context);

        // UnitRaising Sync ---> MetaData Sync --> Image Sync
        new UnitRaisingSync(context);
        new UnitRaisingDeferredSync(context);
        new UnitRaisingCancellationSync(context);
        new BarrackSyncing(context);
        new NewRotaTaskSyncing(context);
    }
}




