package com.sisindia.ai.android.rota.rotaCompliance;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RoccReportViewActivity extends BaseActivity {

    @Bind(R.id.webView)
    WebView webView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rocc_report_view);
        ButterKnife.bind(this);
        setBaseToolbar(toolbar, getResources().getString(R.string.ROCC_REPORT_VIEW));
        reportView(webView);
    }

    @SuppressLint("WrongConstant")
    private void reportView(WebView webView) {

        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(RoccReportViewActivity.this);
                    progressDialog.setMessage("Report Loading...");
                    progressDialog.show();
                }
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
            }

            public void onPageFinished(WebView view, String url) {
                if (progressDialog != null && progressDialog.isShowing()) {
                    if (!isFinishing() || !isDestroyed()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }
            }
        });
        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLL_AXIS_NONE);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setSupportZoom(true);


        // based on the BUILD_TYPE URL will be changed for the Rocc report
       /* String baseUrl = "";
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("demo")) {
            baseUrl = "http://182.75.10.182:8080/ROCC_MASTER/areainspector/rota/";

        } else if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("prod")) {
            baseUrl = "https://rocc.sisindia.com/ROCC/areainspector/rota/";
        }*/

        webView.loadUrl("https://rocc.sisindia.com/ROCC/areainspector/rota/" + appPreferences.getAppUserId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rota_compliance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
