package com.sisindia.ai.android.fcmnotification.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sisindia.ai.android.fcmnotification.NotificationReceivingMO;
import com.sisindia.ai.android.network.SISClient;

/**
 * Created by shankar on 10/11/16.
 */

public class BackGroundSyncJob extends Job {

    public static final int PRIORITY = 1;
    private NotificationReceivingMO backGroundSyncMO;
    private SISClient sisClient;


    public BackGroundSyncJob(NotificationReceivingMO backGroundSync) {
        // This job requires network connectivity,
        // and should be persisted in case the application exits before job is completed.
        super(new Params(PRIORITY).requireNetwork().persist());

        this.backGroundSyncMO = backGroundSync;
    }
    @Override
    public void onAdded() {
        // Job has been saved to disk.
        // This is a good place to dispatch a UI event to indicate the job will eventually run.
        // In this example, it would be good to update the UI with the newly posted tweet.
    }
    @Override
    public void onRun() throws Throwable {
        // Job logic goes here. In this example, the network call to post to Twitter is done here.
        // All work done here should be synchronous, a job is removed from the queue once
        // onRun() finishes.

        if(null!=backGroundSyncMO){
                new DoJobBackground(getApplicationContext(),backGroundSyncMO);
        }



    }

    @Override
    protected void onCancel(@CancelReason int cancelReason, @Nullable Throwable throwable) {
        // Job has exceeded retry attempts or shouldReRunOnThrowable() has decided to cancel.
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }


}
