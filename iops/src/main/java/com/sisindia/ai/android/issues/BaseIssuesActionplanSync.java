package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Durga Prasad on 03-12-2016.
 */

public class BaseIssuesActionplanSync {

    @SerializedName("StatusCode")
    private Integer statusCode;
    @SerializedName("StatusMessage")
    private String statusMessage;
    @SerializedName("Data")
    private FcmIssuesActionPlanData fcmIssuesActionPlanData;

    public FcmIssuesActionPlanData getFcmIssuesActionPlanData() {
        return fcmIssuesActionPlanData;
    }

    public void setFcmIssuesActionPlanData(FcmIssuesActionPlanData fcmIssuesActionPlanData) {
        this.fcmIssuesActionPlanData = fcmIssuesActionPlanData;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "BaseIssuesActionplanSync{" +
                "fcmIssuesActionPlanData=" + fcmIssuesActionPlanData +
                ", statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                '}';
    }
}
