package com.sisindia.ai.android.annualkitreplacement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KitAssignedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KitAssignedFragment extends Fragment implements OnRecyclerViewItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CODE = 101;
    public static KitAssignedFragment fragment;
    ArrayList<Object> mDataValues = new ArrayList<>();
    CustomKitReplacementAdapter customIssuesAdapter;
    @Bind(R.id.unitName)
    CustomFontTextview unitNameLabel;
    @Bind(R.id.kit_assigned_recyclerview)
    RecyclerView kitAssignedRecyclerview;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private ArrayList<AnnualOrAdhocKitItemMO> kitAssignedList;
    private int unitId;
    private int kitTypeId;
    private String unitName;

    public KitAssignedFragment() {
        // Required empty public constructor
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public void setKitTypeId(int kitTypeId) {
        this.kitTypeId = kitTypeId;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ComplaintsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KitAssignedFragment newInstance(String param1, String param2) {
        if (fragment == null) {
            fragment = new KitAssignedFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this
        View view = inflater.inflate(R.layout.kit_assign_layout, container, false);
        ButterKnife.bind(this, view);
        unitNameLabel.setText(unitName);
        kitAssignedRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        setAdapter();
        return view;
    }

    private void setAdapter() {

        kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
        kitAssignedList = kitItemSelectionStatementDB.getAssignedOrDeliveredKitItems(unitId, kitTypeId, 2);

        if (customIssuesAdapter == null)
            customIssuesAdapter = new CustomKitReplacementAdapter(getContext());
        customIssuesAdapter.setKitDetails(kitAssignedList);
        customIssuesAdapter.setOnRecyclerViewItemClickListener(this);
        customIssuesAdapter.enableClick(true);
        kitAssignedRecyclerview.setAdapter(customIssuesAdapter);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        Intent deliveredIntent = new Intent(getActivity(), DeliveredKitItemsActivity.class);
        deliveredIntent.putExtra(TableNameAndColumnStatement.employee_id, kitAssignedList.get(position).getGuardId());
        deliveredIntent.putExtra(TableNameAndColumnStatement.guard_name, kitAssignedList.get(position).getGuardName());
        //deliveredIntent.putExtra(TableNameAndColumnStatement.EMPLOYEE_NO),kitAssignedList.get(position).getGuardId());
        deliveredIntent.putExtra(TableNameAndColumnStatement.unitName, kitAssignedList.get(position).getUnitName());
        deliveredIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, kitAssignedList.get(position).getUnitId());
        deliveredIntent.putExtra(TableNameAndColumnStatement.KIT_TYPE_ID, kitTypeId);
        deliveredIntent.putExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, kitAssignedList.get(position).getKitDistributionId());
        deliveredIntent.putExtra(TableNameAndColumnStatement.TASK_ID, kitAssignedList.get(position).getTaskId());

        startActivityForResult(deliveredIntent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            setAdapter();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
