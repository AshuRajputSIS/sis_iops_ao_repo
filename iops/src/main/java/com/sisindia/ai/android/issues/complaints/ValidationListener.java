package com.sisindia.ai.android.issues.complaints;

/**
 * Created by Shushrut on 10-08-2016.
 */
public interface ValidationListener {
    public void showSnackBar(String msg);
    public void setValidationStatus(boolean value);
    public void setActionPlanSelection(boolean value);
}
