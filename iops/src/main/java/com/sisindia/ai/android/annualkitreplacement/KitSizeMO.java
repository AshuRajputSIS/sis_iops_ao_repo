package com.sisindia.ai.android.annualkitreplacement;

/**
 * Created by Durga Prasad on 29-09-2016.
 */
public class KitSizeMO {

    private  String kitSizeName;
    private  int kitSizeId;
    private  int kitItemSizeId;

    public int getKitSizeId() {
        return kitSizeId;
    }

    public void setKitSizeId(int kitSizeId) {
        this.kitSizeId = kitSizeId;
    }

    public String getKitSizeName() {
        return kitSizeName;
    }

    public void setKitSizeName(String kitSizeName) {
        this.kitSizeName = kitSizeName;
    }

    public int getKitItemSizeId() {
        return kitItemSizeId;
    }

    public void setKitItemSizeId(int kitItemSizeId) {
        this.kitItemSizeId = kitItemSizeId;
    }
}
