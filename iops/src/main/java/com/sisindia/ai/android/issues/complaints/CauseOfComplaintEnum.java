package com.sisindia.ai.android.issues.complaints;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Durga Prasad on 01-05-2016.
 */
public enum CauseOfComplaintEnum {

    Leave(1),
    Desertion(2),
    Quality(3),
    Manpower(4),
    Others(5);


    private static Map map = new HashMap<>();

    static {
        for (CauseOfComplaintEnum postStatusEM : CauseOfComplaintEnum.values()) {
            map.put(postStatusEM.value, postStatusEM);
        }
    }

    private int value;

    CauseOfComplaintEnum(int value) {
        this.value = value;
    }

    public static CauseOfComplaintEnum valueOf(int postStatus) {
        return (CauseOfComplaintEnum) map.get(postStatus);
    }

    public int getValue() {
        return value;
    }
}
