package com.sisindia.ai.android.rota.models;

/**
 * Created by Shushrut on 11-07-2016.
 */
public class SecurityRiskQuestionValidationMappingMO {
    private Integer securityRiskQuestionId;
    private Integer securityRiskOptionId;
    private String securityRiskOptionName;
    private Integer isMandatory;
    private String controlType;
    private Integer isFilled;

    public Integer getIsFilled() {
        return isFilled;
    }

    public void setIsFilled(Integer isFilled) {
        this.isFilled = isFilled;
    }
    /**
     *
     * @return
     * The securityRiskQuestionId
     */
    public Integer getSecurityRiskQuestionId() {
        return securityRiskQuestionId;
    }

    /**
     *
     * @param securityRiskQuestionId
     * The SecurityRiskQuestionId
     */
    public void setSecurityRiskQuestionId(Integer securityRiskQuestionId) {
        this.securityRiskQuestionId = securityRiskQuestionId;
    }
    /**
     *
     * @return
     * The securityRiskOptionId
     */
    public Integer getSecurityRiskOptionId() {
        return securityRiskOptionId;
    }
    /**
     *
     * @param securityRiskOptionId
     * The SecurityRiskOptionId
     */
    public void setSecurityRiskOptionId(Integer securityRiskOptionId) {
        this.securityRiskOptionId = securityRiskOptionId;
    }
    /**
     *
     * @return
     * The securityRiskOptionName
     */
    public String getSecurityRiskOptionName() {
        return securityRiskOptionName;
    }
    /**
     *
     * @param securityRiskOptionName
     * The SecurityRiskOptionName
     */
    public void setSecurityRiskOptionName(String securityRiskOptionName) {
        this.securityRiskOptionName = securityRiskOptionName;
    }
    /**
     *
     * @return
     * The isMandatory
     */
    public Integer getIsMandatory() {
        return isMandatory;
    }
    /**
     *
     * @param isMandatory
     * The IsMandatory
     */
    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }
    /**
     *
     * @return
     * The controlType
     */
    public String getControlType() {
        return controlType;
    }
    /**
     *
     * @param controlType
     * The ControlType
     */
    public void setControlType(String controlType) {
        this.controlType = controlType;
    }


}