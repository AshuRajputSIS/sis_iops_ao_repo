package com.sisindia.ai.android.debugingtool.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashu Rajput on 8/31/2017.
 */

public class QueryExecutorMO {

    @SerializedName("Id")
    private String Id;
    @SerializedName("Name")
    private String Name;
    @SerializedName("EmployeeId")
    private String EmployeeId;
    @SerializedName("Query")
    private String Query;
    @SerializedName("QueryResult")
    private String QueryResult;
    @SerializedName("Status")
    private String Status;
    @SerializedName("NotificationStatus")
    private String NotificationStatus;
    @SerializedName("ResultStatus")
    private String ResultStatus;
    @SerializedName("QueryStatus")
    private String QueryStatus;

    public void setQueryStatus(String queryStatus) {
        QueryStatus = queryStatus;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(String employeeId) {
        EmployeeId = employeeId;
    }

    public String getQuery() {
        return Query;
    }

    public void setQuery(String query) {
        Query = query;
    }

    public String getQueryResult() {
        return QueryResult;
    }

    public void setQueryResult(String queryResult) {
        QueryResult = queryResult;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getNotificationStatus() {
        return NotificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        NotificationStatus = notificationStatus;
    }

    public String getResultStatus() {
        return ResultStatus;
    }

    public void setResultStatus(String resultStatus) {
        ResultStatus = resultStatus;
    }
}
