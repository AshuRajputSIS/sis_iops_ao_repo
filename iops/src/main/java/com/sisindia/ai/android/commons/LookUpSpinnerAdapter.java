package com.sisindia.ai.android.commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.rota.models.LookupModel;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 27-07-2016.
 */
public class LookUpSpinnerAdapter extends BaseAdapter {

    private  Context context;
    private  ArrayList<LookupModel> dataList;

     public LookUpSpinnerAdapter(Context context){
        this.context = context;

    }

    public  void setSpinnerData(ArrayList<LookupModel> dataList){
       this.dataList = dataList;
    }
    @Override
    public int getCount() {
        int count = dataList == null ? 0 : dataList.size();
        return count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SpinnerViewholder spinnerViewholder = null;

        if(convertView == null){
            spinnerViewholder = new SpinnerViewholder();
            convertView = LayoutInflater.from(context).inflate(R.layout.spinner_layout,null,false);
            spinnerViewholder.spinnerTextview = (TextView) convertView.findViewById(R.id.spinner_textview);
            spinnerViewholder.spinnerTextview.setText(dataList.get(position).getName());
            convertView.setTag(spinnerViewholder);

        }
        else{
            spinnerViewholder = (SpinnerViewholder) convertView.getTag();
            if(spinnerViewholder != null) {
                spinnerViewholder.spinnerTextview.setText(dataList.get(position).getName());
            }
        }
        return convertView;
    }

    class SpinnerViewholder{
        TextView spinnerTextview;
    }
}
