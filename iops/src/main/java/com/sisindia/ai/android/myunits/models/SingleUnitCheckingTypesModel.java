package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class SingleUnitCheckingTypesModel {

    @SerializedName("checkingTypes")
    private List<CheckingType> checkingTypes = new ArrayList<CheckingType>();

    /**
     * @return The checkingTypes
     */
    public List<CheckingType> getCheckingTypes() {
        return checkingTypes;
    }

    /**
     * @param checkingTypes The checkingTypes
     */
    public void setCheckingTypes(List<CheckingType> checkingTypes) {
        this.checkingTypes = checkingTypes;
    }

    public class CheckingType {

        @SerializedName("checkingType")
        private String taskType;

        private int imageId;
        private int taskTypeId;
        @SerializedName("checkingId")
        private String checkingId;
        @SerializedName("completedcount")
        private Integer completedcount;
        @SerializedName("pendingCount")
        private Integer pendingCount;
        @SerializedName("lastCheckDate")
        private String lastCheckDate;
        @SerializedName("viewHistory")
        private String viewHistory;
        @SerializedName("newCheckType")
        private String newCheckType;

        public int getImageId() {
            return imageId;
        }

        public void setImageId(int imageId) {
            this.imageId = imageId;
        }

        public String getTaskType() {
            return taskType;
        }

        public void setTaskType(String taskType) {
            this.taskType = taskType;
        }

        public int getTaskTypeId() {
            return taskTypeId;
        }

        public void setTaskTypeId(int taskTypeId) {
            this.taskTypeId = taskTypeId;
        }

        /**
         * @return The checkingId
         */
        public String getCheckingId() {
            return checkingId;
        }

        /**
         * @param checkingId The checkingId
         */
        public void setCheckingId(String checkingId) {
            this.checkingId = checkingId;
        }

        /**
         * @return The completedcount
         */
        public Integer getCompletedcount() {
            return completedcount;
        }

        /**
         * @param completedcount The completedcount
         */
        public void setCompletedcount(Integer completedcount) {
            this.completedcount = completedcount;
        }

        /**
         * @return The pendingCount
         */
        public Integer getPendingCount() {
            return pendingCount;
        }

        /**
         * @param pendingCount The pendingCount
         */
        public void setPendingCount(Integer pendingCount) {
            this.pendingCount = pendingCount;
        }

        /**
         * @return The lastCheckDate
         */
        public String getLastCheckDate() {
            return lastCheckDate;
        }

        /**
         * @param lastCheckDate The lastCheckDate
         */
        public void setLastCheckDate(String lastCheckDate) {
            this.lastCheckDate = lastCheckDate;
        }

        /**
         * @return The viewHistory
         */
        public String getViewHistory() {
            return viewHistory;
        }

        /**
         * @param viewHistory The viewHistory
         */
        public void setViewHistory(String viewHistory) {
            this.viewHistory = viewHistory;
        }

        /**
         * @return The newCheckType
         */
        public String getNewCheckType() {
            return newCheckType;
        }

        /**
         * @param newCheckType The newCheckType
         */
        public void setNewCheckType(String newCheckType) {
            this.newCheckType = newCheckType;
        }

    }

}