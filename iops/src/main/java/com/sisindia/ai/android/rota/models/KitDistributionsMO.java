package com.sisindia.ai.android.rota.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class KitDistributionsMO implements Serializable {

    @SerializedName("Id")
    private int Id;
    @SerializedName("Description")
    private String Description;
    @SerializedName("DeliveryDate")
    private String DeliveryDate;
    @SerializedName("IssuingOfficerId")
    private int IssuingOfficerId;
    @SerializedName("IssuingOfficerName")
    private String IssuingOfficerName;
    @SerializedName("UnitId")
    private int UnitId;
    @SerializedName("UnitName")
    private String UnitName;
    @SerializedName("KitDeliveryStatus")
    private int KitDeliveryStatus;
    @SerializedName("UnitTaskId")
    private Integer unitTaskId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public int getIssuingOfficerId() {
        return IssuingOfficerId;
    }

    public void setIssuingOfficerId(int issuingOfficerId) {
        IssuingOfficerId = issuingOfficerId;
    }

    public String getIssuingOfficerName() {
        return IssuingOfficerName;
    }

    public void setIssuingOfficerName(String issuingOfficerName) {
        IssuingOfficerName = issuingOfficerName;
    }

    public int getUnitId() {
        return UnitId;
    }

    public void setUnitId(int unitId) {
        UnitId = unitId;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public int getKitDeliveryStatus() {
        return KitDeliveryStatus;
    }

    public void setKitDeliveryStatus(int kitDeliveryStatus) {
        KitDeliveryStatus = kitDeliveryStatus;
    }

    public Integer getUnitTaskId() {
        return unitTaskId;
    }

    public void setUnitTaskId(Integer unitTaskId) {
        this.unitTaskId = unitTaskId;
    }
}
