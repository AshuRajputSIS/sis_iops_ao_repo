package com.sisindia.ai.android.mybarrack.modelObjects;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.network.response.NfcCheckResponse;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.MapView;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by shankar on 6/9/16.
 */
public class BarrackGeoLocation extends DayCheckBaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.barrack_name)
    CustomFontTextview barrackName;
    @Bind(R.id.tv_barrack_name)
    CustomFontTextview tvBarrackName;
    @Bind(R.id.barrackNfcStatusTV)
    CustomFontTextview barrackNfcStatusTV;
    @Bind(R.id.location)
    CustomFontTextview location;
    @Bind(R.id.barrack_geolocation)
    CustomFontTextview barrackGeolocation;
    @Bind(R.id.horizontal_divider)
    View horizontalDivider;
    @Bind(R.id.refreshLocation)
    CustomFontButton refreshLocation;
    @Bind(R.id.barrackNfcAddEditLayout)
    LinearLayout barrackNfcAddEditLayout;
    @Bind(R.id.barrackNfcStepsLayout)
    LinearLayout barrackNfcStepsLayout;
    @Bind(R.id.barrackNfcConfigMessageTV)
    TextView barrackNfcConfigMessageTV;
    @Bind(R.id.barrackNfcChangeButton)
    TextView barrackNfcChangeButton;
    @Bind(R.id.barrackNfcAddButton)
    TextView barrackNfcAddButton;

    @Bind(R.id.add_barrack_post_image)
    ImageView barrackImageView;

    @Bind(R.id.horizontal_images_scroll)
    LinearLayout horizontalImagesScroll;

    private Barrack barrackList;
    private String barrackLocation;
    private MapView mapview;
    private String nfcUniqueId = "";
    private boolean isNfcNeedsToUpdate = false;
    private List<String> mArrayImageHolder;
    private int barrackId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barrack_geolocation);

        ButterKnife.bind(this);
        Util.hideKeyboard(this);
        mapview = (MapView) findViewById(R.id.mapview);
        mArrayImageHolder = new ArrayList<>();
//        mContext = this;
//        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        setUpToolBar();
        barrackList = (Barrack) getIntent().getSerializableExtra("barrackList");
        if (null != barrackList) {
            tvBarrackName.setText(barrackList.getName());
            barrackId = barrackList.getId();
            barrackLocation = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackGeoLocation(barrackList.getId());
//            barrackGeolocation.setText(barrackLocation);
            if (barrackLocation != null && !barrackLocation.equalsIgnoreCase("NA")) {
                try {
                    String[] location = barrackLocation.split(",");
                    mapview.setisRelocate(true, Double.parseDouble(location[0]), Double.parseDouble(location[1]));
                } catch (Exception e) {
                }
            }

            if (barrackList.getBarrackNfcID() != null && !barrackList.getBarrackNfcID().isEmpty()) {
                updateNFCStatusOnUI();
            }
        }
    }

    private void updateNFCStatusOnUI() {
        barrackNfcStatusTV.setText("Configured");
        barrackNfcStatusTV.setTextColor(Color.GREEN);
        barrackNfcConfigMessageTV.setText("NFC configured successfully");
        barrackNfcAddButton.setVisibility(View.GONE);
        barrackNfcChangeButton.setVisibility(View.VISIBLE);
    }

    //initiating toolbar
    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.BARRACK_LOCATION));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_billsubmission_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit) {

            /*String location = barrackGeolocation.getText().toString().trim();
            if (!location.equalsIgnoreCase("NA")) {
                String[] geoLocation = location.split(",");
                if (geoLocation != null && geoLocation.length != 0) {
                }
            } else {
                Toast.makeText(this, "Please click refresh location button", Toast.LENGTH_SHORT).show();
            }*/

            if (mapview != null && mapview.getLatitude() != 0.0) {
                String latitude = String.valueOf(mapview.getLatitude());
                String longitude = String.valueOf(mapview.getLongitude());

                if (nfcUniqueId != null && nfcUniqueId.isEmpty()) {
                    if (barrackList != null)
                        nfcUniqueId = barrackList.getBarrackNfcID();
                }

                IOPSApplication.getInstance().getMyBarrackStatementDB().updateBarrackGeoLocation(barrackList.getId(), latitude, longitude, nfcUniqueId);
                IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTableFromHashMap(attachmentMetaArrayList);
                attachmentMetaArrayList.clear();
                triggerSyncAdapter();
                finish();
            } else {
                Toast.makeText(this, "Please click refresh location button", Toast.LENGTH_SHORT).show();
            }

        }
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*@OnClick(R.id.refreshLocation)
    public void onClick() {
        barrackGeolocation.setText(GpsTrackingService.latitude + "," + GpsTrackingService.longitude);
    }*/

    @OnClick(R.id.barrackNfcAddButton)
    public void barrackNfcAddButton() {
        registerReceiver(nfcUniqueIdReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
        barrackNfcAddEditLayout.setVisibility(View.GONE);
        barrackNfcStepsLayout.setVisibility(View.VISIBLE);
        isNfcNeedsToUpdate = false;
    }

    @OnClick(R.id.barrackNfcChangeButton)
    public void barrackNfcChangeButton() {
        registerReceiver(nfcUniqueIdReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
        barrackNfcAddEditLayout.setVisibility(View.GONE);
        barrackNfcStepsLayout.setVisibility(View.VISIBLE);
        isNfcNeedsToUpdate = true;
    }

    @OnClick(R.id.barrackNfcBackButton)
    public void barrackNfcBackButton() {
        unregisterReceiver(nfcUniqueIdReceiver);
        barrackNfcAddEditLayout.setVisibility(View.VISIBLE);
        barrackNfcStepsLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.add_barrack_post_image)
    public void captureBarrackPostImage() {
        Intent capturePictureIntent = new Intent(BarrackGeoLocation.this, CapturePictureActivity.class);
        if (mArrayImageHolder.size() < 1) {
            Util.mSequenceNumber++;
            Util.setSendPhotoImageTo(Util.BARRACK);
            Util.initMetaDataArray(true);
            capturePictureIntent.putExtra(TableNameAndColumnStatement.BARRACK_ID, barrackId);
            startActivityForResult(capturePictureIntent, Util.BARRACK_CODE);
        } else {
            Toast.makeText(this, getResources().getString(R.string.barrack_image_validation_msg), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    BroadcastReceiver nfcUniqueIdReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            nfcUniqueId = intent.getStringExtra(Constants.Nfc.NFC_UNIQUE_ID);
//            Timber.d(TAG, "NFC onReceive: Unique Id : %s", nfcUniqueId);
            if (TextUtils.isEmpty(nfcUniqueId)) {
//                llNfcLayout.setVisibility(View.GONE);
                Toast.makeText(BarrackGeoLocation.this, "NFC Device Id is empty or tampered", Toast.LENGTH_SHORT).show();
            } else {
                handleNfcUniqueId();
            }
        }
    };

    private void handleNfcUniqueId() {
        if (Util.isNetworkConnected()) {
            if (barrackList != null)
                checkNfcAddStatusViaApi(nfcUniqueId);
//                checkNfcAddStatusOnline(barrackList.getId(), nfcUniqueId);
        } else {
            if (isNfcNeedsToUpdate) {
                IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcDeviceNoOfBarrack(nfcUniqueId, barrackList.getId());
                updateNFCStatusOnUI();
                Toast.makeText(BarrackGeoLocation.this, getString(R.string.nfc_updated_successfully), Toast.LENGTH_SHORT).show();
            } else {
                boolean isNfcExists = IOPSApplication.getInstance().getMyBarrackStatementDB().isNfcAlreadyExists(nfcUniqueId);
                if (isNfcExists) {
                    Toast.makeText(this, R.string.nfc_already_configured, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = "";
                } else {
                    IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcDeviceNoOfBarrack(nfcUniqueId, barrackList.getId());
                    updateNFCStatusOnUI();
                    Toast.makeText(BarrackGeoLocation.this, getString(R.string.nfc_configured_successfully), Toast.LENGTH_SHORT).show();
                }

                /*Barrack barrackInfo = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackNFCInformation(nfcUniqueId, barrackList.getId());
                if (barrackInfo != null && barrackInfo.getId() != null) {
                    Toast.makeText(this, R.string.nfc_already_configured, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = null;
                } else {
                    IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcDeviceNoOfBarrack(nfcUniqueId, barrackList.getId());
                    updateNFCStatusOnUI();
                    Toast.makeText(BarrackGeoLocation.this, getString(R.string.nfc_configured_successfully), Toast.LENGTH_SHORT).show();
                }*/
            }
        }
    }

   /* private void checkNfcAddStatusOnline(int unitId, final String uniqueId) {
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.Nfc.UNIT_ID, unitId);
        map.put(Constants.Nfc.DEVICE_NO, uniqueId);
        checkNfcAddStatusViaApi(map);
    }*/

    private void checkNfcAddStatusViaApi(String nfcDeviceID) {
        new SISClient(this).getApi().checkBarrackNfcStatus(nfcDeviceID, new Callback<NfcCheckResponse>() {
            @Override
            public void success(NfcCheckResponse nfcCheckResponse, Response response) {
                Timber.d("NFC add status check @Barrack: %s", nfcCheckResponse);
                boolean isNFCAvailableForBarrack = nfcCheckResponse.nfcData.allowToAdd;
                boolean isNFCAlreadyUnitTagged = nfcCheckResponse.nfcData.isAlreadyUnitTagged;
                if (!isNFCAvailableForBarrack) {
                    Toast.makeText(BarrackGeoLocation.this, R.string.nfc_already_added, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = "";
                } else if (isNFCAlreadyUnitTagged) {
                    Toast.makeText(BarrackGeoLocation.this, R.string.nfc_already_added_unit, Toast.LENGTH_SHORT).show();
                    nfcUniqueId = "";
                } else {
                    IOPSApplication.getInstance().getMyBarrackStatementDB().updateNfcDeviceNoOfBarrack(nfcUniqueId, barrackList.getId());
                    updateNFCStatusOnUI();
                    Toast.makeText(BarrackGeoLocation.this, getString(R.string.nfc_configured_successfully), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error, "Error while getting add nfc status");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        registerReceiver(nfcUniqueIdReceiver, new IntentFilter(Constants.Nfc.NFC_UNIQUE_SCAN));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (nfcUniqueIdReceiver != null)
                unregisterReceiver(nfcUniqueIdReceiver);
        } catch (Exception e) {
        }
    }

    private void triggerSyncAdapter() {
        Bundle bundle = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundle);
    }

    private void loadThumbNailImages(List<String> arrayImageHolder) {
        if (horizontalImagesScroll != null) {
            horizontalImagesScroll.removeAllViews();
            horizontalImagesScroll.addView(barrackImageView);
        }

        if (arrayImageHolder != null && arrayImageHolder.size() > 0) {
            for (int index = arrayImageHolder.size() - 1; index >= 0; index--) {
                ImageView imageView = createImageView(Uri.parse(arrayImageHolder.get(index)));
                horizontalImagesScroll.addView(imageView);
            }

        }
    }

    public ImageView createImageView(Uri uri) {

        int marginBottom = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginLeft = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginRight = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);

        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.HEIGT);
        params.width = Util.dpToPx(Constants.HEIGT);
        params.setMargins(marginLeft, marginBottom, marginRight, marginBottom);

        RoundedCornerImageView takenImageView = new RoundedCornerImageView(this);
        takenImageView.setLayoutParams(params);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.camer_taken_pic_image_border);
        takenImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        takenImageView.setImageURI(uri);
        return takenImageView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Util.BARRACK_CODE && resultCode == RESULT_OK && data != null) {
            AttachmentMetaDataModel attachmentMetaDataModel = data.getParcelableExtra(Constants.META_DATA);
            String imagePath = attachmentMetaDataModel.getAttachmentPath();
            attachmentMetaArrayList.add(attachmentMetaDataModel);
            //setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            mArrayImageHolder.add(imagePath);
            loadThumbNailImages(mArrayImageHolder);
        } else {
            Util.mSequenceNumber--;
        }
    }
}


