package com.sisindia.ai.android.myunits.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class UnitStrengthBaseOR {

@SerializedName("StatusCode")

private Integer statusCode;
@SerializedName("StatusMessage")

private String statusMessage;
@SerializedName("Data")

private List<UnitStrengthMO> data = new ArrayList<UnitStrengthMO>();

/**
* 
* @return
* The statusCode
*/
public Integer getStatusCode() {
return statusCode;
}

/**
* 
* @param statusCode
* The StatusCode
*/
public void setStatusCode(Integer statusCode) {
this.statusCode = statusCode;
}

/**
* 
* @return
* The statusMessage
*/
public String getStatusMessage() {
return statusMessage;
}

    public List<UnitStrengthMO> getData() {

        return data;
    }

    public void setData(List<UnitStrengthMO> data) {
        this.data = data;
    }

    /**
* 
* @param statusMessage
* The StatusMessage
*/
public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

/**
* 
* @return
* The data
*/


}