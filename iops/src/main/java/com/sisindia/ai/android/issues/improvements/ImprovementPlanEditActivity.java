package com.sisindia.ai.android.issues.improvements;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.issues.IssuesFragment;
import com.sisindia.ai.android.issues.models.ImprovementPlanListMO;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Saruchi on 08-08-2016.
 */

public class ImprovementPlanEditActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    @Bind(R.id.toolbar)
    Toolbar mImprovementToolBar;
    @Bind(R.id.scrollview_addplan)
    ScrollView mscrollView;
    @Bind(R.id.source_text)
    TextView sourceTxt;
    @Bind(R.id.lastInspectionUnitName)
    TextView unitName;
    @Bind(R.id.category_txt)
    TextView categoryTxt;
    @Bind(R.id.remark)
    TextView remarks;
    @Bind(R.id.improvementTextview)
    TextView mImprovementTextview;
    @Bind(R.id.assignedTotxt)
    TextView txtAssignedTo;
    @Bind(R.id.spinner_status)
    Spinner spinnerStatus;
    @Bind(R.id.add_closure_remarks)
    EditText closureRemarks;
    @Bind(R.id.button_save)
    Button saveBtn;
    @Bind(R.id.button_cancel)
    Button cancelBtn;
    @Bind(R.id.parent_layout)
    LinearLayout buttonsLayout;
    @Bind(R.id.issueTargetClosureDate)
    TextView targetDateTxt;
    @Bind(R.id.timeDurationLeft)
    RelativeTimeTextView timeDuration;
    @Bind(R.id.statusTxt)
    TextView statusTxt;
    @Bind(R.id.closureLayout)
    RelativeLayout closureLayout;
    @Bind(R.id.remak_txt)
    CustomFontTextview remarkLable;
    @Bind(R.id.budget_txt)
    CustomFontTextview budgetTxt;
    private Resources resources;
    private ImprovementPlanListMO improvementPlanListMO;
    private ArrayList<LookupModel> issueStatus;
    private LookUpSpinnerAdapter statusAdapter;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.improvementplan_edit_activity);
        ButterKnife.bind(this);
        invalidateOptionsMenu();

        resources = getResources();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        // inside onCreate
        Bundle extras = getIntent().getExtras();
        // If the extras are not null
        if (extras != null) {
            improvementPlanListMO = extras.getParcelable(resources.getString(R.string.improvement_plan_edit));
            setUpView();
        }
    }


    private void setUpView() {
        setSupportActionBar(mImprovementToolBar);
        mscrollView.smoothScrollTo(0, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.improvement_plan_title);
        statusAdapter = new LookUpSpinnerAdapter(this);
        setUpViewVisibility();

        if (improvementPlanListMO.getBudget() != null && !improvementPlanListMO.getBudget().isEmpty()) {
            budgetTxt.setText(improvementPlanListMO.getBudget());

        } else {

            budgetTxt.setText(resources.getString(R.string.NOT_AVAILABLE));

        }
        if (improvementPlanListMO.getSourceType() != null && !improvementPlanListMO.getSourceType().isEmpty()) {
            sourceTxt.setText(improvementPlanListMO.getSourceType());

        } else {

            sourceTxt.setText(resources.getString(R.string.NOT_AVAILABLE));

        }
        if (sourceTxt.getText().toString().trim().equalsIgnoreCase("unit")) {
            if (improvementPlanListMO.getUnitName() != null && !improvementPlanListMO.getUnitName().isEmpty()) {
                unitName.setText(improvementPlanListMO.getUnitName());

            } else {

                unitName.setText(resources.getString(R.string.NOT_AVAILABLE));

            }
        } else if (sourceTxt.getText().toString().trim().equalsIgnoreCase("barrack")) {
            if (null == improvementPlanListMO.getUnitId() || 0 == improvementPlanListMO.getUnitId()) {
                if (improvementPlanListMO.getBarrackName() != null && !improvementPlanListMO.getBarrackName().isEmpty()) {
                    unitName.setText(improvementPlanListMO.getBarrackName());
                } else {
                    unitName.setText(resources.getString(R.string.NOT_AVAILABLE));

                }

            }
        } else {
            unitName.setText(resources.getString(R.string.NOT_AVAILABLE));
        }


        if (improvementPlanListMO.getImprovementPlanCategoryName() != null && !improvementPlanListMO.getImprovementPlanCategoryName().isEmpty()) {
            categoryTxt.setText(improvementPlanListMO.getImprovementPlanCategoryName());
        } else {
            categoryTxt.setText(resources.getString(R.string.NOT_AVAILABLE));
        }
        if (improvementPlanListMO.getActionPlanName() != null && !improvementPlanListMO.getActionPlanName().isEmpty()) {
            mImprovementTextview.setText(improvementPlanListMO.getActionPlanName());
        } else {
            mImprovementTextview.setText(resources.getString(R.string.NOT_AVAILABLE));
        }
        if (improvementPlanListMO.getAssignedToName() != null && !improvementPlanListMO.getAssignedToName().isEmpty()) {
            txtAssignedTo.setText(improvementPlanListMO.getAssignedToName());
        } else {
            txtAssignedTo.setText(resources.getString(R.string.NOT_AVAILABLE));
        }
        if (improvementPlanListMO.getRemarks() != null && !improvementPlanListMO.getRemarks().isEmpty()) {
            remarks.setVisibility(View.VISIBLE);
            remarkLable.setVisibility(View.VISIBLE);
            remarks.setText(improvementPlanListMO.getRemarks());
        } else {
            remarks.setText(resources.getString(R.string.NOT_AVAILABLE));
            remarks.setVisibility(View.GONE);
            remarkLable.setVisibility(View.GONE);
        }
        if (improvementPlanListMO.getExpectedClosureDate() != null && !improvementPlanListMO.getExpectedClosureDate().isEmpty()) {
            String date = dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(improvementPlanListMO.getExpectedClosureDate());
            targetDateTxt.setText(date);
        } else {
            targetDateTxt.setText(resources.getString(R.string.NOT_AVAILABLE));
        }
        if (improvementPlanListMO.getCreatedDateTime() != null && !improvementPlanListMO.getCreatedDateTime().isEmpty()) {
            long millis = dateTimeFormatConversionUtil.convertStringToDate(improvementPlanListMO.getCreatedDateTime()).getTime();
            timeDuration.setReferenceTime(millis);
        } else {
            timeDuration.setText(resources.getString(R.string.NOT_AVAILABLE));
        }

        issueStatus = IOPSApplication.getInstance().getSelectionStatementDBInstance().getLookupData(getResources().getString(R.string.ACTION_PLAN_STATUS), null);
        statusAdapter.setSpinnerData(issueStatus);
        spinnerStatus.setAdapter(statusAdapter);
        spinnerStatus.setSelection(Util.getIndex(improvementPlanListMO.getImprovementStatus(),spinnerStatus,issueStatus));
        spinnerStatus.setOnItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        MenuItem saveMenu = menu.findItem(R.id.add_post_save);
        if (appPreferences.getAppUserId() == improvementPlanListMO.getAssignedToId()) {
            if (resources.getString(R.string.completed_status).equalsIgnoreCase(improvementPlanListMO.getImprovementStatus()) ||
                    resources.getString(R.string.cancel_status).equalsIgnoreCase(improvementPlanListMO.getImprovementStatus())  ){
                saveMenu.setVisible(false);

            }
            else {
                saveMenu.setVisible(true);
            }

        } else {
            saveMenu.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String issueStatusString = issueStatus.get(position).getName();
        improvementPlanListMO.setImprovementStatus(issueStatusString);
        improvementPlanListMO.setImprovementStatusId(issueStatus.get(position).getLookupIdentifier());
        if (resources.getString(R.string.completed_status).equalsIgnoreCase(issueStatusString)) {
            closureLayout.setVisibility(View.VISIBLE);
//            improvementPlanListMO.setImprovementStatus(issueStatus.get(position).getLookupTypeName());
//            improvementPlanListMO.setImprovementStatusId(issueStatus.get(position).getLookupIdentifier());
            closureRemarks.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    improvementPlanListMO.setClosureRemarks(s.toString());

                }
            });
        } else {
            closureLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick(R.id.button_save)
    public void onSave() {

        vaidateFields();
    }

    @OnClick(R.id.button_cancel)
    public void onCancel() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.add_post_save:
                vaidateFields();
                break;
        }

        return super.onOptionsItemSelected(item);
    }




    /**
     * if improvement plan is assigned to the user who is logged In then the user can change the status
     * else if improvement plan is not assigned to the logged in user then he can only view the details
     * i.e user cannot edit anything
     *
     */
    private void setUpViewVisibility(){
        if(improvementPlanListMO.getAssignedToId()!=0){

    if (appPreferences.getAppUserId() == improvementPlanListMO.getAssignedToId()) {
        if ( improvementPlanListMO.getImprovementStatusId() == 2 || improvementPlanListMO.getImprovementStatusId() == 4 ){
            spinnerStatus.setVisibility(View.GONE);
            statusTxt.setVisibility(View.VISIBLE);
            statusTxt.setText(improvementPlanListMO.getImprovementStatus());
        }
        else{
            spinnerStatus.setVisibility(View.VISIBLE);
            statusTxt.setVisibility(View.GONE);
        }
    }
        else {
        spinnerStatus.setVisibility(View.GONE);
        statusTxt.setVisibility(View.VISIBLE);
        statusTxt.setText(improvementPlanListMO.getImprovementStatus());
    }
        }
        else{
            spinnerStatus.setVisibility(View.GONE);
            statusTxt.setVisibility(View.VISIBLE);
            statusTxt.setText(improvementPlanListMO.getImprovementStatus());
            snackBarWithMesg(buttonsLayout,getResources().getString(R.string.data_not_synced));
        }


}

    private void vaidateFields(){
        if (improvementPlanListMO != null && improvementPlanListMO.getImprovementStatusId()!=0){
            if (resources.getString(R.string.completed_status).equalsIgnoreCase(improvementPlanListMO.getImprovementStatus())) {
                if ( improvementPlanListMO.getClosureRemarks() != null) {
                    saveDetails();
                } else {
                    Util.customSnackBar(buttonsLayout, this, resources.getString(R.string.closure_remark_validation));
                }
            }

            else{
                saveDetails();
            }
        }

    }

    private void saveDetails(){
        improvementPlanListMO.setClosureDateTime(dateTimeFormatConversionUtil.getCurrentDateTime());
        IOPSApplication.getInstance().getIssueInsertionDBInstance().updateImprovementPlanDetails(improvementPlanListMO);
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
        appPreferences.setIssueType(Constants.NavigationFlags.TYPE_IMPROVEMENT_PLAN);
        IssuesFragment.newInstance(resources.getString(R.string.Issues),resources.getString(R.string.add_improvement_plans));
        finish();
    }
}
