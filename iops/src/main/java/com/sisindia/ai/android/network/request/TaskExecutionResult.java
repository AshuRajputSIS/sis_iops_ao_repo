package com.sisindia.ai.android.network.request;


import com.google.gson.annotations.SerializedName;

public class TaskExecutionResult {

    @SerializedName("DayNightChecking")
    private DayNightChecking dayNightChecking;

    /**
     * @return The dayNightChecking
     */
    public DayNightChecking getDayNightChecking() {
        return dayNightChecking;
    }

    /**
     * @param dayNightChecking The DayNightChecking
     */
    public void setDayNightChecking(DayNightChecking dayNightChecking) {
        this.dayNightChecking = dayNightChecking;
    }

    @Override
    public String toString() {
        return "TaskExecutionResult{" +
                "dayNightChecking=" + dayNightChecking +
                '}';
    }
}