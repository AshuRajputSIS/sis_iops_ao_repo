package com.sisindia.ai.android.appuser;

/**
 * Created by Shushrut on 16-09-2016.
 */
public class AppUserData {
    String mImageUrl;
    String mUserAddress;
    String mUserAlternateNumber;

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmUserAddress() {
        return mUserAddress;
    }

    public void setmUserAddress(String mUserAddress) {
        this.mUserAddress = mUserAddress;
    }

    public String getmUserAlternateNumber() {
        return mUserAlternateNumber;
    }

    public void setmUserAlternateNumber(String mUserAlternateNumber) {
        this.mUserAlternateNumber = mUserAlternateNumber;
    }
}
