package com.sisindia.ai.android.utils.bottomsheet;

import com.sisindia.ai.android.myunits.models.MyUnitHomeMO;
import com.sisindia.ai.android.myunits.models.UnitDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga Prasad on 29-04-2016.
 */
public interface ShowBottomSheetListener {

    void showBottomSheet(ArrayList<String> elements, int viewId);

    void showUnitModelBottomSheet(List<MyUnitHomeMO> myUnitHomeMOs, int viewId);


}
