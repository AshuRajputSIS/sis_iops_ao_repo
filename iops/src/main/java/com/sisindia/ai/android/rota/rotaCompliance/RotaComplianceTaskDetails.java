package com.sisindia.ai.android.rota.rotaCompliance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Durga Prasad on 31-01-2017.
 */
public class RotaComplianceTaskDetails implements Parcelable {

    @SerializedName("TaskTypeId")
    public Integer taskTypeId;

    @SerializedName("TaskType")
    public String taskType;

    @SerializedName("CurrentMonthCompletedTasksCount")
    public Integer currentMonthCompletedTasksCount;

    @SerializedName("MonthlyRotaTarget")
    public Integer MonthlyRotaTarget;

    @SerializedName("RotaCompliancePercentage")
    public Float RotaCompliancePercentage;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.taskTypeId);
        dest.writeString(this.taskType);
        dest.writeValue(this.currentMonthCompletedTasksCount);
        dest.writeValue(this.MonthlyRotaTarget);
        dest.writeValue(this.RotaCompliancePercentage);
    }

    public RotaComplianceTaskDetails() {
    }

    protected RotaComplianceTaskDetails(Parcel in) {
        this.taskTypeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.taskType = in.readString();
        this.currentMonthCompletedTasksCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.MonthlyRotaTarget = (Integer) in.readValue(Integer.class.getClassLoader());
        this.RotaCompliancePercentage = (Float) in.readValue(Float.class.getClassLoader());
    }

    public static final Creator<RotaComplianceTaskDetails> CREATOR = new Creator<RotaComplianceTaskDetails>() {
        @Override
        public RotaComplianceTaskDetails createFromParcel(Parcel source) {
            return new RotaComplianceTaskDetails(source);
        }

        @Override
        public RotaComplianceTaskDetails[] newArray(int size) {
            return new RotaComplianceTaskDetails[size];
        }
    };
}
