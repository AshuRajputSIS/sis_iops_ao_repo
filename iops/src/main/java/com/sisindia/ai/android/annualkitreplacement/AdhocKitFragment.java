package com.sisindia.ai.android.annualkitreplacement;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.annualkitreplacementDTO.KitItemSelectionStatementDB;
import com.sisindia.ai.android.issues.models.NoDataAvailable;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.utils.Util;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Durga Prasad on 22-09-2016.
 */

public class AdhocKitFragment extends Fragment implements OnRecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    // TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.annualKitRecyclerView)
    RecyclerView annualKitRecyclerView;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private AnnualKitReplacementAdapter annualKitReplacementAdapter;
    private Context mContext;
    private KitItemSelectionStatementDB kitItemSelectionStatementDB;
    private ArrayList<AnnualOrAdhocKitItemMO> adhocKitItemList;
    private ArrayList<Object> adhocKitList;
    private KitReplaceCountData adhocKitCountData;
    private ArrayList<Object> totalKitItemList;

    public AdhocKitFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AnnualKitReplacementFragment.
     */
// TODO: Rename and change types and number of parameters
    public static AdhocKitFragment newInstance(String param1, String param2) {
        AdhocKitFragment fragment = new AdhocKitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_annual_kit_replacement, container, false);
        ButterKnife.bind(this, view);
        swipeRefreshLayout.setOnRefreshListener(this);
        updateUi();
        return view;
    }

    private void updateUi() {
        try {
            kitItemSelectionStatementDB = IOPSApplication.getInstance().getKitItemSelectionStatementDB();
            adhocKitItemList = kitItemSelectionStatementDB.getAnnaulOrAdhocKitItems(Constants.ADHOC_KIT_ITEM_TYPE, null);
            totalKitItemList = new ArrayList<>();

            if (adhocKitCountData != null) {
                List<AnnualKitReplacementMO> annualKitReplacementMOList = adhocKitCountData.getAddHocKitList();
                if (annualKitReplacementMOList != null && annualKitReplacementMOList.size() != 0) {
                    totalKitItemList.addAll(annualKitReplacementMOList);
                }
            } else {
                AnnualKitReplacementMO annualKitReplacementMO = new AnnualKitReplacementMO();
                totalKitItemList.add(annualKitReplacementMO);
            }

            if (adhocKitItemList != null) {
                if (adhocKitItemList.size() == 0) {
                    NoDataAvailable noDataAvailable = new NoDataAvailable();
                    noDataAvailable.setMessage(getActivity().getResources().getString(R.string.NO_DATA_AVAILABLE));
                    totalKitItemList.add(noDataAvailable);
                } else {
                    totalKitItemList.addAll(adhocKitItemList);
                }
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            annualKitRecyclerView.setLayoutManager(linearLayoutManager);
            annualKitRecyclerView.setItemAnimator(new DefaultItemAnimator());
            annualKitReplacementAdapter = new AnnualKitReplacementAdapter(getActivity());
            annualKitReplacementAdapter.setAnnualKitReplacemntDetails(totalKitItemList);
            annualKitRecyclerView.setAdapter(annualKitReplacementAdapter);
            annualKitReplacementAdapter.setOnRecyclerViewItemClickListener(this);
            annualKitRecyclerView.setAdapter(annualKitReplacementAdapter);
            swipeRefreshLayout.setRefreshing(false);
        } catch (Exception exception) {
            exception.printStackTrace();
            Crashlytics.logException(exception);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        //&&((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getIssuedKitCount() != 0

        if (totalKitItemList != null) {
            Intent kitTabsIntent = new Intent(getActivity(), AnnualKitReplacementTabsActivity.class);
            kitTabsIntent.putExtra(TableNameAndColumnStatement.UNIT_ID, ((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getUnitId());
            kitTabsIntent.putExtra(TableNameAndColumnStatement.KIT_TYPE_ID, 2);
            kitTabsIntent.putExtra(TableNameAndColumnStatement.UNIT_NAME, ((AnnualOrAdhocKitItemMO) totalKitItemList.get(position)).getUnitName());
            startActivity(kitTabsIntent);
        }
    }

    public void setAdhocKitCountData(KitReplaceCountData adhocKitCountData) {
        this.adhocKitCountData = adhocKitCountData;
    }

    @Override
    public void onRefresh() {
        getAnnualKitItemsCountCall();
    }

    private void getAnnualKitItemsCountCall() {
        if (getActivity() != null) {
            SISClient sisClient = new SISClient(getActivity());
            sisClient.getApi().kitReplaceCount(new Callback<KitReplaceCountOR>() {
                @Override
                public void success(KitReplaceCountOR kitReplaceCountOR, Response response) {
                    switch (kitReplaceCountOR.getStatusCode()) {
                        case HttpURLConnection.HTTP_OK:
                            adhocKitCountData = kitReplaceCountOR.getData();
                            break;
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            Toast.makeText(getActivity(), getResources().getString(R.string.INTERNAL_SERVER_ERROR), Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(getActivity(), getResources().getString(R.string.ERROR), Toast.LENGTH_SHORT).show();
                            break;
                    }
                    updateUi();
                }

                @Override
                public void failure(RetrofitError error) {
                    updateUi();
                    Util.printRetorfitError("SingleUnitActivity", error);
                }
            });
        }
    }
}

