package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.UnitBillingCheckList;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shankar on 22/6/16.
 */

public class Unit implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("UnitCode")
    @Expose
    private String unitCode;
    @SerializedName("AddressId")
    @Expose
    private Integer addressId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("UnitTypeId")
    @Expose
    private Integer unitTypeId;
    @SerializedName("AreaId")
    @Expose
    private Integer areaId;
    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("DayCheckFrequencyInMonth")
    @Expose
    private Integer dayCheckFrequencyInMonth;
    @SerializedName("NightCheckFrequencyInMonth")
    @Expose
    private Integer nightCheckFrequencyInMonth;
    @SerializedName("ClientCoordinationFrequencyInMonth")
    @Expose
    private Integer clientCoordinationFrequencyInMonth;
    @SerializedName("WoNumber")
    @Expose
    private String woNumber;
    @SerializedName("SurveyDate")
    @Expose
    private String surveyDate;
    @SerializedName("RaisingBudget")
    @Expose
    private BigDecimal raisingBudget;
    @SerializedName("DeploymentDate")
    @Expose
    private String deploymentDate;
    @SerializedName("BillDate")
    @Expose
    private Integer billDate;
    @SerializedName("BillCollectionDate")
    @Expose
    private Integer billCollectionDate;
    @SerializedName("WageDate")
    @Expose
    private Integer wageDate;
    @SerializedName("BillingMode")
    @Expose
    private Integer billingMode;
    @SerializedName("MessVendorId")
    @Expose
    private Integer messVendorId;
    @SerializedName("IsBarrackProvided")
    @Expose
    private Boolean isBarrackProvided;
    @SerializedName("UnitCommanderId")
    @Expose
    private Integer unitCommanderId;
    @SerializedName("AreaInspectorId")
    @Expose
    private Integer areaInspectorId;
    @SerializedName("BillSubmissionResponsibleId")
    @Expose
    private Integer billSubmissionResponsibleId;
    @SerializedName("BillCollectionResponsibleId")
    @Expose
    private Integer billCollectionResponsibleId;
    @SerializedName("BillAuthoritativeUnitId")
    @Expose
    private Integer billAuthoritativeUnitId;
    @SerializedName("UnitType")
    @Expose
    private String unitType;
    @SerializedName("UnitCommanderName")
    @Expose
    private String unitCommanderName;
    @SerializedName("AreaInspectorName")
    @Expose
    private String areaInspectorName;
    @SerializedName("BillCollectionResponsibleName")
    @Expose
    private String billCollectionResponsibleName;
    @SerializedName("BillSubmissionResponsibleName")
    @Expose
    private String billSubmissionResponsibleName;
    @SerializedName("UnitFullAddress")
    @Expose
    private String unitFullAddress;
    @SerializedName("UnitStatus")
    @Expose
    private int unitStatus;
    @SerializedName("CollectionStatus")
    @Expose
    private Integer collectionStatus;
    @SerializedName("AreaInspector")
    @Expose
    private AreaInspector areaInspector;
    @SerializedName("UnitTypeModel")
    @Expose
    private UnitTypeModel unitTypeModel;
    @SerializedName("ArmedStrength")
    @Expose
    private Integer armedStrength;
    @SerializedName("UnarmedStrength")
    @Expose
    private Integer unarmedStrength;
    @SerializedName("BillingTypeId")
    @Expose
    private Integer billingTypeId;
    @SerializedName("BillGenerationDate")
    @Expose
    private Integer billGenerationDate;
    @SerializedName("UnitBarrack")
    @Expose
    private List<UnitBarrack> unitBarrack = new ArrayList<UnitBarrack>();
    @SerializedName("UnitEmployees")
    @Expose
    private List<UnitEmployees> unitEmployees = new ArrayList<UnitEmployees>();
    @SerializedName("UnitPosts")
    @Expose
    private List<UnitPost> unitPosts = new ArrayList<UnitPost>();
    @SerializedName("UnitContacts")
    @Expose
    private List<UnitContacts> unitContacts = new ArrayList<UnitContacts>();
    @SerializedName("UnitMessVendors")
    @Expose
    private UnitMessVendors unitMessVendors;
    @SerializedName("UnitCustomers")
    @Expose
    private UnitCustomers unitCustomers;
    @SerializedName("UnitAddressLatlong")
    @Expose
    private UnitAddressLatlong unitAddressLatlong;
    @SerializedName("UnitShiftRank")
    @Expose
    private List<UnitShiftRank> unitShiftRank = new ArrayList<UnitShiftRank>();

    @SerializedName("UnitBoundary")
    @Expose
    private List<UnitBoundary> unitBoundary = new ArrayList<UnitBoundary>();
    @SerializedName("MainGatePhotoUrl")
    @Expose
    private String mainGatePhotoUrl;


    @SerializedName("RaisingDate")
    private String raisingDate;

    public List<UnitBillingCheckList> getUnitBillingCheckList() {
        return unitBillingCheckList;
    }

    public void setUnitBillingCheckList(List<UnitBillingCheckList> unitBillingCheckList) {
        this.unitBillingCheckList = unitBillingCheckList;
    }

    @SerializedName("UnitBillingCheckList")
    @Expose
    private List<UnitBillingCheckList> unitBillingCheckList = new ArrayList<UnitBillingCheckList>();

    public String getMainGatePhotoUrl() {
        return mainGatePhotoUrl;
    }

    public void setMainGatePhotoUrl(String mainGatePhotoUrl) {
        this.mainGatePhotoUrl = mainGatePhotoUrl;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The unitCode
     */
    public String getUnitCode() {
        return unitCode;
    }

    /**
     * @param unitCode The UnitCode
     */
    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    /**
     * @return The addressId
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     * @param addressId The AddressId
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The unitTypeId
     */
    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    /**
     * @param unitTypeId The UnitTypeId
     */
    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    /**
     * @return The areaId
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     * @param areaId The AreaId
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    /**
     * @return The customerId
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId The CustomerId
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * @return The dayCheckFrequencyInMonth
     */
    public Integer getDayCheckFrequencyInMonth() {
        return dayCheckFrequencyInMonth;
    }

    /**
     * @param dayCheckFrequencyInMonth The DayCheckFrequencyInMonth
     */
    public void setDayCheckFrequencyInMonth(Integer dayCheckFrequencyInMonth) {
        this.dayCheckFrequencyInMonth = dayCheckFrequencyInMonth;
    }

    /**
     * @return The nightCheckFrequencyInMonth
     */
    public Integer getNightCheckFrequencyInMonth() {
        return nightCheckFrequencyInMonth;
    }

    /**
     * @param nightCheckFrequencyInMonth The NightCheckFrequencyInMonth
     */
    public void setNightCheckFrequencyInMonth(Integer nightCheckFrequencyInMonth) {
        this.nightCheckFrequencyInMonth = nightCheckFrequencyInMonth;
    }

    /**
     * @return The clientCoordinationFrequencyInMonth
     */
    public Integer getClientCoordinationFrequencyInMonth() {
        return clientCoordinationFrequencyInMonth;
    }

    /**
     * @param clientCoordinationFrequencyInMonth The ClientCoordinationFrequencyInMonth
     */
    public void setClientCoordinationFrequencyInMonth(Integer clientCoordinationFrequencyInMonth) {
        this.clientCoordinationFrequencyInMonth = clientCoordinationFrequencyInMonth;
    }

    /**
     * @return The woNumber
     */
    public String getWoNumber() {
        return woNumber;
    }

    /**
     * @param woNumber The WoNumber
     */
    public void setWoNumber(String woNumber) {
        this.woNumber = woNumber;
    }

    /**
     * @return The surveyDate
     */
    public String getSurveyDate() {
        return surveyDate;
    }

    /**
     * @param surveyDate The SurveyDate
     */
    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    /**
     * @return The raisingBudget
     */
    public BigDecimal getRaisingBudget() {
        return raisingBudget;
    }

    /**
     * @param raisingBudget The RaisingBudget
     */
    public void setRaisingBudget(BigDecimal raisingBudget) {
        this.raisingBudget = raisingBudget;
    }

    /**
     * @return The deploymentDate
     */
    public String getDeploymentDate() {
        return deploymentDate;
    }

    /**
     * @param deploymentDate The DeploymentDate
     */
    public void setDeploymentDate(String deploymentDate) {
        this.deploymentDate = deploymentDate;
    }

    /**
     * @return The billDate
     */
    public Integer getBillDate() {
        return billDate;
    }

    /**
     * @param billDate The BillDate
     */
    public void setBillDate(Integer billDate) {
        this.billDate = billDate;
    }

    /**
     * @return The billCollectionDate
     */
    public Integer getBillCollectionDate() {
        return billCollectionDate;
    }

    /**
     * @param billCollectionDate The BillCollectionDate
     */
    public void setBillCollectionDate(Integer billCollectionDate) {
        this.billCollectionDate = billCollectionDate;
    }

    /**
     * @return The wageDate
     */
    public Integer getWageDate() {
        return wageDate;
    }

    /**
     * @param wageDate The WageDate
     */
    public void setWageDate(Integer wageDate) {
        this.wageDate = wageDate;
    }

    /**
     * @return The billingMode
     */
    public Integer getBillingMode() {
        return billingMode;
    }

    /**
     * @param billingMode The BillingMode
     */
    public void setBillingMode(Integer billingMode) {
        this.billingMode = billingMode;
    }

    /**
     * @return The messVendorId
     */
    public Integer getMessVendorId() {
        return messVendorId;
    }

    /**
     * @param messVendorId The MessVendorId
     */
    public void setMessVendorId(Integer messVendorId) {
        this.messVendorId = messVendorId;
    }

    /**
     * @return The isBarrackProvided
     */
    public Boolean getIsBarrackProvided() {
        return isBarrackProvided;
    }

    /**
     * @param isBarrackProvided The IsBarrackProvided
     */
    public void setIsBarrackProvided(Boolean isBarrackProvided) {
        this.isBarrackProvided = isBarrackProvided;
    }

    /**
     * @return The unitCommanderId
     */
    public Integer getUnitCommanderId() {
        return unitCommanderId;
    }

    /**
     * @param unitCommanderId The UnitCommanderId
     */
    public void setUnitCommanderId(Integer unitCommanderId) {
        this.unitCommanderId = unitCommanderId;
    }

    /**
     * @return The areaInspectorId
     */
    public Integer getAreaInspectorId() {
        return areaInspectorId;
    }

    /**
     * @param areaInspectorId The AreaInspectorId
     */
    public void setAreaInspectorId(Integer areaInspectorId) {
        this.areaInspectorId = areaInspectorId;
    }

    /**
     * @return The billSubmissionResponsibleId
     */
    public Integer getBillSubmissionResponsibleId() {
        return billSubmissionResponsibleId;
    }

    /**
     * @param billSubmissionResponsibleId The BillSubmissionResponsibleId
     */
    public void setBillSubmissionResponsibleId(Integer billSubmissionResponsibleId) {
        this.billSubmissionResponsibleId = billSubmissionResponsibleId;
    }

    /**
     * @return The billCollectionResponsibleId
     */
    public Integer getBillCollectionResponsibleId() {
        return billCollectionResponsibleId;
    }

    /**
     * @param billCollectionResponsibleId The BillCollectionResponsibleId
     */
    public void setBillCollectionResponsibleId(Integer billCollectionResponsibleId) {
        this.billCollectionResponsibleId = billCollectionResponsibleId;
    }

    /**
     * @return The billAuthoritativeUnitId
     */
    public Integer getBillAuthoritativeUnitId() {
        return billAuthoritativeUnitId;
    }

    /**
     * @param billAuthoritativeUnitId The BillAuthoritativeUnitId
     */
    public void setBillAuthoritativeUnitId(Integer billAuthoritativeUnitId) {
        this.billAuthoritativeUnitId = billAuthoritativeUnitId;
    }

    /**
     * @return The unitType
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * @param unitType The UnitType
     */
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    /**
     * @return The unitCommanderName
     */
    public String getUnitCommanderName() {
        return unitCommanderName;
    }

    /**
     * @param unitCommanderName The UnitCommanderName
     */
    public void setUnitCommanderName(String unitCommanderName) {
        this.unitCommanderName = unitCommanderName;
    }

    /**
     * @return The areaInspectorName
     */
    public String getAreaInspectorName() {
        return areaInspectorName;
    }

    /**
     * @param areaInspectorName The AreaInspectorName
     */
    public void setAreaInspectorName(String areaInspectorName) {
        this.areaInspectorName = areaInspectorName;
    }

    /**
     * @return The billCollectionResponsibleName
     */
    public String getBillCollectionResponsibleName() {
        return billCollectionResponsibleName;
    }

    /**
     * @param billCollectionResponsibleName The BillCollectionResponsibleName
     */
    public void setBillCollectionResponsibleName(String billCollectionResponsibleName) {
        this.billCollectionResponsibleName = billCollectionResponsibleName;
    }

    /**
     * @return The billSubmissionResponsibleName
     */
    public String getBillSubmissionResponsibleName() {
        return billSubmissionResponsibleName;
    }

    /**
     * @param billSubmissionResponsibleName The BillSubmissionResponsibleName
     */
    public void setBillSubmissionResponsibleName(String billSubmissionResponsibleName) {
        this.billSubmissionResponsibleName = billSubmissionResponsibleName;
    }

    /**
     * @return The unitFullAddress
     */
    public String getUnitFullAddress() {
        return unitFullAddress;
    }

    /**
     * @param unitFullAddress The UnitFullAddress
     */
    public void setUnitFullAddress(String unitFullAddress) {
        this.unitFullAddress = unitFullAddress;
    }

    /**
     * @return The unitBarrack
     */
    public List<UnitBarrack> getUnitBarrack() {
        return unitBarrack;
    }

    /**
     * @param unitBarrack The UnitBarrack
     */
    public void setUnitBarrack(List<UnitBarrack> unitBarrack) {
        this.unitBarrack = unitBarrack;
    }

    /**
     * @return The unitEmployees
     */
    public List<UnitEmployees> getUnitEmployees() {
        return unitEmployees;
    }

    /**
     * @param unitEmployees The UnitEmployees
     */
    public void setUnitEmployees(List<UnitEmployees> unitEmployees) {
        this.unitEmployees = unitEmployees;
    }

    /**
     * @return The unitPosts
     */
    public List<UnitPost> getUnitPosts() {
        return unitPosts;
    }

    /**
     * @param unitPosts The UnitPosts
     */
    public void setUnitPosts(List<UnitPost> unitPosts) {
        this.unitPosts = unitPosts;
    }

    /**
     * @return The unitContacts
     */
    public List<UnitContacts> getUnitContacts() {
        return unitContacts;
    }

    /**
     * @param unitContacts The UnitContacts
     */
    public void setUnitContacts(List<UnitContacts> unitContacts) {
        this.unitContacts = unitContacts;
    }

    /**
     * @return The unitMessVendors
     */
    public UnitMessVendors getUnitMessVendors() {
        return unitMessVendors;
    }

    /**
     * @param unitMessVendors The UnitMessVendors
     */
    public void setUnitMessVendors(UnitMessVendors unitMessVendors) {
        this.unitMessVendors = unitMessVendors;
    }

    /**
     * @return The unitCustomers
     */
    public UnitCustomers getUnitCustomers() {
        return unitCustomers;
    }

    /**
     * @param unitCustomers The UnitCustomers
     */
    public void setUnitCustomers(UnitCustomers unitCustomers) {
        this.unitCustomers = unitCustomers;
    }

    /**
     * @return The unitAddressLatlong
     */
    public UnitAddressLatlong getUnitAddressLatlong() {
        return unitAddressLatlong;
    }

    /**
     * @param unitAddressLatlong The UnitAddressLatlong
     */
    public void setUnitAddressLatlong(UnitAddressLatlong unitAddressLatlong) {
        this.unitAddressLatlong = unitAddressLatlong;
    }

    /**
     * @return The unitShiftRank
     */
    public List<UnitShiftRank> getUnitShiftRank() {
        return unitShiftRank;
    }

    /**
     * @param unitShiftRank The UnitShiftRank
     */
    public void setUnitShiftRank(List<UnitShiftRank> unitShiftRank) {
        this.unitShiftRank = unitShiftRank;
    }

    /**
     * @return The unitStatus
     */
    public int getUnitStatus() {
        return unitStatus;
    }

    /**
     * @param unitStatus The UnitStatus
     */
    public void setUnitStatus(int unitStatus) {
        this.unitStatus = unitStatus;
    }

    /**
     * @return The unitBoundary
     */
    public List<UnitBoundary> getUnitBoundary() {
        return unitBoundary;
    }

    /**
     * @param unitBoundary The UnitBoundary
     */
    public void setUnitBoundary(List<UnitBoundary> unitBoundary) {
        this.unitBoundary = unitBoundary;
    }

    /**
     * @return The collectionStatus
     */
    public Integer getCollectionStatus() {
        return collectionStatus;
    }

    /**
     * @param collectionStatus The CollectionStatus
     */
    public void setCollectionStatus(Integer collectionStatus) {
        this.collectionStatus = collectionStatus;
    }

    /**
     * @return The areaInspector
     */
    public AreaInspector getAreaInspector() {
        return areaInspector;
    }

    /**
     * @param areaInspector The AreaInspector
     */
    public void setAreaInspector(AreaInspector areaInspector) {
        this.areaInspector = areaInspector;
    }

    /**
     * @return The unitTypeModel
     */
    public UnitTypeModel getUnitTypeModel() {
        return unitTypeModel;
    }

    /**
     * @param unitTypeModel The UnitTypeModel
     */
    public void setUnitTypeModel(UnitTypeModel unitTypeModel) {
        this.unitTypeModel = unitTypeModel;
    }

    /**
     * @return The armedStrength
     */
    public Integer getArmedStrength() {
        return armedStrength;
    }

    /**
     * @param armedStrength The ArmedStrength
     */
    public void setArmedStrength(Integer armedStrength) {
        this.armedStrength = armedStrength;
    }

    /**
     * @return The unarmedStrength
     */
    public Integer getUnarmedStrength() {
        return unarmedStrength;
    }

    /**
     * @param unarmedStrength The UnarmedStrength
     */
    public void setUnarmedStrength(Integer unarmedStrength) {
        this.unarmedStrength = unarmedStrength;
    }

    /**
     * @return The billingTypeId
     */
    public Integer getBillingTypeId() {
        return billingTypeId;
    }

    /**
     * @param billingTypeId The BillingTypeId
     */
    public void setBillingTypeId(Integer billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    /**
     * @return The billGenerationDate
     */
    public Integer getBillGenerationDate() {
        return billGenerationDate;
    }

    /**
     * @param billGenerationDate The BillGenerationDate
     */
    public void setBillGenerationDate(Integer billGenerationDate) {
        this.billGenerationDate = billGenerationDate;
    }

    public String getRaisingDate() {
        return raisingDate;
    }

    public void setRaisingDate(String raisingDate) {
        this.raisingDate = raisingDate;
    }
}
