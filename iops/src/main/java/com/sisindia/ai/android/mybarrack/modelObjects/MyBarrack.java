package com.sisindia.ai.android.mybarrack.modelObjects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by compass on 6/13/2017.
 */
public class MyBarrack {

    @SerializedName("Id")
    public Integer id;

    @SerializedName("Name")
    public String name;

    @SerializedName("Description")
    public String description;

    @SerializedName("AddressId")
    public int addressId;

    @SerializedName("OwnerContactId")
    public int ownerContactId;

    @SerializedName("OwnerAddressId")
    public int ownerAddressId;

    @SerializedName("InChargeId")
    public Integer inChargeId;

    @SerializedName("IsMessAvailable")
    public Boolean isMessAvailable;

    @SerializedName("MessVendorId")
    public int messVendorId;

    @SerializedName("AreaInspectorId")
    public Integer areaInspectorId;

    @SerializedName("IsCustomerProvided")
    public Boolean isCustomerProvided;

    @SerializedName("BarrackAddress")
    public String barrackAddress;

    @SerializedName("OwnerContactName")
    public String ownerContactName;

    @SerializedName("OwnerAddress")
    public String ownerAddress;

    @SerializedName("InChargeName")
    public String inChargeName;

    @SerializedName("AreaInspectorName")
    public String areaInspectorName;

    @SerializedName("GeoLatitude")
    public String geoLatitude;

    @SerializedName("GeoLongitude")
    public String geoLongitude;

    @SerializedName("BarrackCode")
    public String barrackCode;

    @SerializedName("BranchId")
    public Integer branchId;

    @SerializedName("GeoLocation")
    public Object geoLocation;

    @SerializedName("GeoRegion")
    public Object geoRegion;

    @SerializedName("BarrackStrength")
    public BarrackStrength barrackStrength;

    @SerializedName("DeviceNo")
    public String DeviceNo;

}
