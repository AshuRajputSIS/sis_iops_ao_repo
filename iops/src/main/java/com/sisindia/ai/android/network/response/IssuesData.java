package com.sisindia.ai.android.network.response;


import com.google.gson.annotations.SerializedName;

public class IssuesData {

    @SerializedName("IssueId")
    private Integer issueId;
    @SerializedName("JsonActionPlan")
    private String jsonActionPlan;
    @SerializedName("AssignedToId")
    private Integer assignedToId;
    @SerializedName("AssigneeName")
    private String assigneeName;
    @SerializedName("GuardId")
    private Integer guardId;
    @SerializedName("GuardName")
    private String guardName;




    /**
     *
     * @return
     * The issueId
     */
    public Integer getIssueId() {
        return issueId;
    }

    /**
     *
     * @param issueId
     * The IssueId
     */
    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    /**
     *
     * @return
     * The jsonActionPlan
     */
    public String getJsonActionPlan() {
        return jsonActionPlan;
    }

    /**
     *
     * @param jsonActionPlan
     * The JsonActionPlan
     */
    public void setJsonActionPlan(String jsonActionPlan) {
        this.jsonActionPlan = jsonActionPlan;
    }

    /**
     *
     * @return
     * The assignedToId
     */
    public Integer getAssignedToId() {
        return assignedToId;
    }

    /**
     *
     * @param assignedToId
     * The AssignedToId
     */
    public void setAssignedToId(Integer assignedToId) {
        this.assignedToId = assignedToId;
    }

    /**
     *
     * @return
     * The assigneeName
     */
    public String getAssigneeName() {
        return assigneeName;
    }

    /**
     *
     * @param assigneeName
     * The AssigneeName
     */
    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }


    public Integer getGuardId() {
        return guardId;
    }

    public void setGuardId(Integer guardId) {
        this.guardId = guardId;
    }

    public String getGuardName() {
        return guardName;
    }

    public void setGuardName(String guardName) {
        this.guardName = guardName;
    }

    @Override
    public String toString() {
        return "IssuesData{" +
                "issueId=" + issueId +
                ", jsonActionPlan='" + jsonActionPlan + '\'' +
                ", assignedToId=" + assignedToId +
                ", assigneeName='" + assigneeName + '\'' +
                ", guardId=" + guardId +
                ", guardName='" + guardName + '\'' +
                '}';
    }
}