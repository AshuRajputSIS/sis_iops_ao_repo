package com.sisindia.ai.android.syncadpter;

import android.accounts.Account;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.sisindia.ai.android.commons.Constants;

/**
 * Created by Saruchi on 14-03-2016.
 */
public class TableObserver extends ContentObserver {


    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public TableObserver(Handler handler) {
        super(handler);
    }


    /*
                 * Define a method that's called when data in the
                 * observed content provider changes.
                 * This method signature is provided for compatibility with
                 * older platforms.
                 */
    @Override
    public void onChange(boolean selfChange) {
            /*
             * Invoke the method signature available as of
             * Android platform version 4.1, with a null URI.
             */
        onChange(selfChange, null);
    }

    /*
     * Define a method that's called when data in the
     * observed content provider changes.
     */
    @Override
    public void onChange(boolean selfChange, Uri changeUri) {
/*
             * Ask the framework to run your sync adapter.
             * To maintain backward compatibility, assume that
             * changeUri is null.*/
        startForceSyncing();
        System.out.println("SyncAdapter==onChange");
    }

    private void startForceSyncing() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        Account account = new Account(Constants.ACCOUNT, Constants.ACCOUNT_TYPE);

        ContentResolver.requestSync(account, Constants.AUTHORITY, bundle);

        ContentResolver.setIsSyncable(account, Constants.AUTHORITY, 1);
        ContentResolver.setSyncAutomatically(account, Constants.AUTHORITY, true);
    }
}