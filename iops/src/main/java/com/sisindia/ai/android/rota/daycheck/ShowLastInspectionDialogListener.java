package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by Durga Prasad on 10-06-2016.
 */
public interface ShowLastInspectionDialogListener {

    void showLastInspectionDialog(int layout,Object object);
}
