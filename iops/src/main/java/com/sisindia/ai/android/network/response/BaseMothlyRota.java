package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.models.RotaMO;

public class BaseMothlyRota {

    @SerializedName("StatusCode")
    @Expose
    private Integer StatusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String StatusMessage;
    @SerializedName("Data")
    @Expose
    private RotaMO Data;

    /**
     * @return The StatusCode
     */
    public Integer getStatusCode() {
        return StatusCode;
    }

    /**
     * @param StatusCode The StatusCode
     */
    public void setStatusCode(Integer StatusCode) {
        this.StatusCode = StatusCode;
    }

    /**
     * @return The StatusMessage
     */
    public String getStatusMessage() {
        return StatusMessage;
    }

    /**
     * @param StatusMessage The StatusMessage
     */
    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    /**
     * @return The LoadConfigurationData
     */
    public RotaMO getData() {
        return Data;
    }

    /**
     * @param Data The LoadConfigurationData
     */
    public void setData(RotaMO Data) {
        this.Data = Data;
    }

}