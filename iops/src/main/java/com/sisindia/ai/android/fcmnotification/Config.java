package com.sisindia.ai.android.fcmnotification;

/**
 * Created by shankar on 10/11/16.
 */

public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String ROTA = "ROTA";
    public static final String UNIT = "UNIT";
    public static final String BARRACK = "BARRACK";
    public static final String UNIT_STRENGTH = "UNIT_STRENGTH";
    public static final String BARRACK_STRENGTH = "BARRACK_STRENGTH";
    public static final String UNIT_AT_RISK = "UNIT_AT_RISK";
    public static final String UNIT_RISK_ACTION_PLAN = "UNIT_RISK_ACTION_PLAN";
    public static final String ISSUE = "ISSUE";
    public static final String IMPROVEMENT_PLAN = "IMPROVEMENT_PLAN";
    public static final String HOLIDAY = "HOLIDAY";
    public static final String LEAVE = "LEAVE";
    public static final String UNIT_CONTACT = "UNIT_CONTACT";
    public static final String ADD_COMPLAINT = "ADD_COMPLAINT";
    public static final String AKR = "AKR";
    public static final String ADHOC = "ADHOC";
    public static final String BILL_SUBMISSION_CHECKLIST = "BILL_SUBMISSION_CHECKLIST";
    public static final String LOOKUP = "LOOKUP";
    public static final String UNIT_BARRACK = "UNITBARRACK";
    public static final String RECRUITMENT = "RECRUITMENT";
    public static final String BILL_SUBMISSION = "BILL_SUBMISSION";
    public static final String BILL_COLLECTION = "BILL_COLLECTION";
    public static final String UNIT_MONTHLY_BILLDETAIL = "UNIT_MONTHLY_BILLDETAIL";
    public static final String ADD_TASK = "ADD_TASK";
    public static final String BILL_COLLECTION_DUE = "BILL_COLLECTION_DUE";
    public static final String BILL_COLLECTION_OVERDUE = "BILL_COLLECTION_OVERDUE";
    public static final String AKR_DUE = "AKR_DUE";
    public static final String CUSTOM = "CUSTOM";
    public static final String LOGOUT = "LOG_OUT";
    public static final String GUARD_REFRESH = "GUARD_REFRESH";
    public static final String DIAGNOSTICS = "DIAGNOSTICS";
    public static final String POA_COUNT = "POA_COUNT";
    public static final String ISSUE_MATRIX = "ISSUE_MATRIX";
    public static final String MASTER_ACTION_PLAN = "MASTER_ACTION_PLAN";
    public static final String REMOVE_SHARED_PREF_KEY = "REMOVE_SHARED_PREF_KEY";
    public static final String UNIT_TYPE = "UNIT_TYPE";
}