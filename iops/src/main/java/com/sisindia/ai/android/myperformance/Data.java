package com.sisindia.ai.android.myperformance;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DistanceTravelled")

    private Float distanceTravelled;
    @SerializedName("Compliance")

    private Integer compliance;
    @SerializedName("DutyHours")

    private Float dutyHours;
    @SerializedName("TasksAssigned")

    private Integer tasksAssigned;
    @SerializedName("TasksCompleted")

    private Integer tasksCompleted;
    @SerializedName("AdhocTasksCreated")

    private Integer adhocTasksCreated;
    @SerializedName("AdhocTasksApproved")

    private Integer adhocTasksApproved;
    @SerializedName("TotalScore")

    private Integer totalScore;

    public Float getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(Float distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public Integer getCompliance() {
        return compliance;
    }

    public void setCompliance(Integer compliance) {
        this.compliance = compliance;
    }


    public Integer getTasksAssigned() {
        return tasksAssigned;
    }

    public void setTasksAssigned(Integer tasksAssigned) {
        this.tasksAssigned = tasksAssigned;
    }

    public Integer getTasksCompleted() {
        return tasksCompleted;
    }

    public void setTasksCompleted(Integer tasksCompleted) {
        this.tasksCompleted = tasksCompleted;
    }

    public Integer getAdhocTasksCreated() {
        return adhocTasksCreated;
    }

    public void setAdhocTasksCreated(Integer adhocTasksCreated) {
        this.adhocTasksCreated = adhocTasksCreated;
    }

    public Integer getAdhocTasksApproved() {
        return adhocTasksApproved;
    }

    public void setAdhocTasksApproved(Integer adhocTasksApproved) {
        this.adhocTasksApproved = adhocTasksApproved;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Float getDutyHours() {
        return dutyHours;
    }

    public void setDutyHours(Float dutyHours) {
        this.dutyHours = dutyHours;
    }
}