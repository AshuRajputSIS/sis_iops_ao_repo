package com.sisindia.ai.android.mybarrack;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.mybarrack.modelObjects.Barrack;
import com.sisindia.ai.android.mybarrack.modelObjects.BarrackGeoLocation;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.rota.AddRotaTaskActivity;
import com.sisindia.ai.android.rota.TaskDetailsActivity;
import com.sisindia.ai.android.rota.models.RotaTaskActivityListMO;
import com.sisindia.ai.android.rota.models.RotaTaskListMO;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shruti on 26/7/16.
 */
public class BarrackAdapter extends RecyclerView.Adapter {

    private List<Barrack> barrackList = new ArrayList<>();
    private Context context;
    private SingletonUnitDetails singletonUnitDetails;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private ArrayList<Object> rotaTaskList;
    private AppPreferences appPreferences;

    public BarrackAdapter(List<Barrack> barrackListMO, Context context) {
        this.barrackList = barrackListMO;
        this.context = context;
        this.dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        this.appPreferences = new AppPreferences(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.barrack_name_list, parent, false);
        return new BarrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        BarrackViewHolder barrackViewHolder = (BarrackViewHolder) holder;

        barrackViewHolder.barrackNameTextView.setText(barrackList.get(position).getName());
        barrackViewHolder.barrackFullAddress.setText(barrackList.get(position).getBarrackAddress());
        String lastInspectedDate = IOPSApplication.getInstance().getMyBarrackStatementDB().getBarrackLastInspectedDate(barrackList.get(position).getId());
        if (null == lastInspectedDate) {
            barrackViewHolder.barrackLastInspectedDate.setText(context.getResources().getString(R.string.LAST_INSPECTION_DATE) + context.getResources().getString(R.string.NOT_AVAILABLE));
        } else {
            String lastInspected[] = lastInspectedDate.split(" ");
            if (lastInspected != null) {
                barrackViewHolder.barrackLastInspectedDate.setText(context.getResources().getString(R.string.LAST_INSPECTION_DATE) + lastInspected[0]);
            } else {
                barrackViewHolder.barrackLastInspectedDate.setText(context.getResources().getString(R.string.LAST_INSPECTION_DATE) + context.getResources().getString(R.string.NOT_AVAILABLE));
            }
        }
        //@Commenting as per requirement
        /*if (barrackList.get(position).getGeoLatitude() == 0.0) {
            barrackViewHolder.mapIcon.setBackground(context.getResources().getDrawable(R.drawable.location_grey));
        } else {
            barrackViewHolder.mapIcon.setBackground(context.getResources().getDrawable(R.drawable.locationblue));
        }*/

        if (!TextUtils.isEmpty(barrackList.get(position).getBarrackNfcID()))
            barrackViewHolder.mapIcon.setImageResource(R.mipmap.nfc_on);
        else
            barrackViewHolder.mapIcon.setImageResource(R.mipmap.nfc_off);

        if (barrackList.get(position).getBelongsTo().equalsIgnoreCase("self")) {
            barrackViewHolder.barrackIndication.setVisibility(View.VISIBLE);
            barrackViewHolder.barrackIndication.setBackgroundColor(context.getResources().getColor(R.color.color_thick_green));
        } else {
            barrackViewHolder.barrackIndication.setVisibility(View.GONE);
        }
        barrackViewHolder.barrackIndication.setBackgroundColor(Color.GREEN);
        barrackViewHolder.barrackNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTabScreen(position);
            }
        });
        barrackViewHolder.icon_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTabScreen(position);
            }
        });
        barrackViewHolder.mapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateBarrackGeoLocation(position);
            }
        });
    }

    private void updateBarrackGeoLocation(int position) {
        Intent intent = new Intent(context, BarrackGeoLocation.class);
        intent.putExtra("barrackList", barrackList.get(position));
        context.startActivity(intent);
    }

    private void startTabScreen(int position) {
        rotaTaskList = IOPSApplication.getInstance().getRotaLevelSelectionInstance().
                getRotaTaskByDate(dateTimeFormatConversionUtil.getCurrentDate(), false, barrackList.get(position).getId());
        if (null != rotaTaskList && rotaTaskList.size() != 0) {
            singletonUnitDetails = SingletonUnitDetails.getInstance();
            singletonUnitDetails.setUnitDetails(rotaTaskList.get(0), 0);
            Intent taskDetailsIntent = new Intent(context, TaskDetailsActivity.class);
            context.startActivity(taskDetailsIntent);
        } else {
            boolean isBarrackAvailable = IOPSApplication.getInstance().getMyBarrackStatementDB().
                    checkBarrackAvailableStatus(barrackList.get(position).getId());
            if (isBarrackAvailable) {
                if (appPreferences.getDutyStatus()) {
                    RotaTaskActivityListMO rotaTaskActivityListMO = IOPSApplication.getInstance().
                            getSelectionStatementDBInstance().getAllRotaTaskActivity(3);
                    if (null != rotaTaskActivityListMO && rotaTaskActivityListMO.getRotaTaskListMO().size() != 0) {
                        RotaTaskListMO rotaTaskListMO = rotaTaskActivityListMO.getRotaTaskListMO().get(0);
                        Intent addTaskIntent = new Intent(context, AddRotaTaskActivity.class);
                        addTaskIntent.putExtra("FromMyBarrack", true);
                        addTaskIntent.putExtra(Constants.CHECKING_TYPE, rotaTaskListMO);
                        addTaskIntent.putExtra("BarrackId", barrackList.get(position).getId());
                        addTaskIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(addTaskIntent);
                    }
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.DUTY_ON_OFF_TOAST_MESSAGE_FOR_CREATING_NEW_TASK), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "No Rota Task Available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public int getItemCount() {
        return barrackList.size();
    }

    public class BarrackViewHolder extends RecyclerView.ViewHolder {
        private final TextView barrackFullAddress;
        ImageView mapIcon;
        TextView barrackNameTextView;
        ImageView icon_next;
        TextView barrackLastInspectedDate;
        View divider;
        View barrackIndication;

        public BarrackViewHolder(View itemView) {
            super(itemView);
            barrackNameTextView = (TextView) itemView.findViewById(R.id.barrack_name);
            barrackFullAddress = (TextView) itemView.findViewById(R.id.barrack_address);
            divider = itemView.findViewById(R.id.divider);
            icon_next = (ImageView) itemView.findViewById(R.id.icon_next);
            mapIcon = (ImageView) itemView.findViewById(R.id.mapIcon);
            barrackLastInspectedDate = (TextView) itemView.findViewById(R.id.last_inspection_date);
            barrackIndication = itemView.findViewById(R.id.barrack_indication_to_self);
        }
    }
}
