package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class AppRole {

    @SerializedName("Id")
    public Integer Id;

    @SerializedName("EmployeeId")
    public Integer EmployeeId;

    @SerializedName("RoleId")
    public Integer RoleId;

    @SerializedName("BranchId")
    public Integer BranchId;

    @SerializedName("AreaId")
    public Integer AreaId;

    @SerializedName("IsActive")
    public Boolean IsActive;

    @SerializedName("RoleName")
    public String RoleName;
    private int rankId;

    public Integer getAreaId() {
        return AreaId;
    }

    public void setAreaId(Integer areaId) {
        AreaId = areaId;
    }

    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        BranchId = branchId;
    }

    public Integer getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        EmployeeId = employeeId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Integer getRoleId() {
        return RoleId;
    }

    public void setRoleId(Integer roleId) {
        RoleId = roleId;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }


    public void setRankId(int rankId) {
        this.rankId = rankId;
    }

    public int getRankId() {
        return rankId;
    }
}