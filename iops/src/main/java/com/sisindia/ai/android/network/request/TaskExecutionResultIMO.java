package com.sisindia.ai.android.network.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskExecutionResultIMO {

    @SerializedName("DayNightCheckingIMO")
    @Expose
    private DayNightCheckingIMO DayNightChecking;

    /**
     * @return The DayNightCheckingIMO
     */
    public DayNightCheckingIMO getDayNightChecking() {
        return DayNightChecking;
    }

    /**
     * @param DayNightChecking The DayNightCheckingIMO
     */
    public void setDayNightChecking(DayNightCheckingIMO DayNightChecking) {
        this.DayNightChecking = DayNightChecking;
    }

    @Override
    public String toString() {
        return "TaskExecutionResultIMO{" +
                "DayNightChecking=" + DayNightChecking +
                '}';
    }
}