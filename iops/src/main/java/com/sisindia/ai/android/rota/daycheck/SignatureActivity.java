package com.sisindia.ai.android.rota.daycheck;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.simplify.ink.InkView;
import com.sisindia.ai.android.commons.CommonTags;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by Shushrut on 01-04-2016.
 */
public class SignatureActivity extends DayCheckBaseActivity {
    @Bind(R.id.toolbar)
    Toolbar  mToolBar;
    @Bind(R.id.retry_signature)
    TextView mretry_signature;
    @Bind(R.id.ink)
    InkView ink;
    private boolean isSigned = false;

    private  static final int REQUEST_TAKE_SIGNATURE = 2;
    private String fileName;
    private int unitId;
    private String unitName;
    private int unitTaskId;
    private boolean isFromKitDistribution;
    private AttachmentMetaDataModel attachmentMetaDataModel;
    private boolean fromGuardDetailActivity;
    private String guardCode;
    private int kitDistributionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.singature_view);
        ButterKnife.bind(this);

        ink.addInkListener(new InkView.InkListener() {
            @Override
            public void onInkClear() {
                isSigned = false;
            }

            @Override
            public void onInkDraw() {
                isSigned = true;
            }
        });
        Intent intent = getIntent();
        if (getIntent() != null) {

            fromGuardDetailActivity = getIntent().getBooleanExtra("SignatureCapture", false);
        }
        if (intent.hasExtra(TableNameAndColumnStatement.UNIT_ID) && intent.hasExtra(TableNameAndColumnStatement.UNIT_NAME)) {
            unitId = intent.getIntExtra(TableNameAndColumnStatement.UNIT_ID,0);
            unitName  = intent.getStringExtra(TableNameAndColumnStatement.UNIT_NAME);
            unitTaskId = intent.getIntExtra(TableNameAndColumnStatement.TASK_ID,0);
            guardCode = intent.getStringExtra(TableNameAndColumnStatement.GUARD_CODE);
            kitDistributionId = intent.getIntExtra(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID,0);


            isFromKitDistribution =true;
        }

       // final InkView ink = (InkView) findViewById(R.id.ink);
        ink.setFlags(InkView.FLAG_INTERPOLATION | InkView.FLAG_RESPONSIVE_WIDTH);

        TextView mretry_signature = (TextView)findViewById(R.id.retry_signature);

        mretry_signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ink.clear();
            }
        });
        setSupportActionBar(mToolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(rotaTaskModel != null)
        getSupportActionBar().setTitle(rotaTaskModel.getUnitName());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu_items, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.done_menu:
                if(isSigned) {
                    ink.getBitmap(getResources().getColor(R.color.white));
                    isSigned = false;
                    saveImage(ink.getBitmap(getResources().getColor(R.color.white)));
                    Intent intent = new Intent();
                    intent.putExtra(getResources().getString(R.string.signature), "sent");
                    Util.setmSignature(ink.getBitmap());
                    if(guardCode != null) {
                    }
                    intent.putExtra(Constants.META_DATA, attachmentMetaDataModel);
                    setResult(Util.CAPTURE_SIGNATURE, intent);
                    finish();
                }else{
                    snackBarWithMesg(mToolBar,getResources().getString(R.string.signature_mandatory));
                }
                break;
            case android.R.id.home :
                finish();
        }


        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.ink)
    public void retrySignature(){
        ink.clear();
    }
    private void saveImage(Bitmap savebitmap) {
        byte[] imagebytes=null;

        attachmentMetaDataModel = new AttachmentMetaDataModel();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());


        if(rotaTaskModel != null ){
           fileName = Constants.SIGNATURE + "_" +  Util.mGuard_Id + "_" + rotaTaskModel.getTaskId() + "_" + rotaTaskModel.getUnitId() + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
        }
        else{
            //for checking purpose I add null here,remove later
             if(unitTaskId != 0) {
                 fileName = Constants.DISTRIBUTION_SIGNATURE + "_" + Util.mGuard_Id + "_" +unitTaskId
                         + "_" + unitId + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
             }
            else{
                 fileName = Constants.DISTRIBUTION_SIGNATURE + "_" + Util.mGuard_Id + "_" + "0"
                         + "_" + unitId + "_" + Util.mSequenceNumber + "_" + "IMG_" + timeStamp + ".jpg";
             }

        }

      //  File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
             //   Environment.DIRECTORY_PICTURES), getString(R.string.app_name));
        File root = android.os.Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES);
        File mediaStorageDir = new File(root.getAbsolutePath() +"/"+ getString(R.string.app_name));
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();

        }

          File mediaFile = new File(mediaStorageDir + File.separator + fileName);
     String   currentTakenImagePath = mediaStorageDir + File.separator + fileName;
        Timber.d("currentTakenImagePath  %s", currentTakenImagePath);
        savebitmap = Util.resize(savebitmap,Constants.IMAGE_HEIGHT,Constants.IMAGE_WIDTH); // landscape



        try {

            FileOutputStream fos = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            savebitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
            imagebytes = out.toByteArray();
            fos = new FileOutputStream(mediaFile);
            fos.write(out.toByteArray());
            fos.close();
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        attachmentMetaDataModel.setAttachmentSourceCode(Util.getGuardCode());
        attachmentMetaDataModel.setAttachmentPath(currentTakenImagePath.trim());
        attachmentMetaDataModel.setAttachmentFileName(fileName.trim());
        attachmentMetaDataModel.setAttachmentTitle(fileName.trim());
        attachmentMetaDataModel.setAttachmentFileExtension(".jpg");
        attachmentMetaDataModel.setAttachmentTypeId(Constants.LOOKUP_TYPE_IMAGE);// picture
        attachmentMetaDataModel.setimageCode(CommonTags.GUARD_SIGNATURE);
        if(rotaTaskModel != null) {
            attachmentMetaDataModel.setAttachmentSourceId(rotaTaskModel.getTaskId());
        }


        String size = android.text.format.Formatter.formatFileSize(this, imagebytes.length);

        Timber.d("size  %s", size);
        size = size.replaceAll("[^\\d.]", "");
        int imagesize = (int) Math.round(Double.valueOf(size));
        Timber.d("size  %s", imagesize);

        attachmentMetaDataModel.setAttachmentFileSize(imagesize);
        if(isFromKitDistribution){
            attachmentMetaDataModel.setAttachmentSourceTypeId( Util.getAttachmentSourceType(TableNameAndColumnStatement.DISTRIBUTION_SIGANTURE, this));
        }
        else{
            attachmentMetaDataModel.setAttachmentSourceTypeId( Util.getAttachmentSourceType(TableNameAndColumnStatement.SIGNATURE, this));
        }



        if (rotaTaskModel != null) {
            attachmentMetaDataModel.setTaskId(rotaTaskModel.getTaskId());
            attachmentMetaDataModel.setUnitId(rotaTaskModel.getUnitId());
        }
        else{
            attachmentMetaDataModel.setTaskId(unitTaskId);
            attachmentMetaDataModel.setUnitId(unitId);
        }
        if(Util.KIT_ITEM_SIGNATURE == Util.getSendPhotoImageTo()){
            Util.setSendPhotoImageTo(null);
            attachmentMetaDataModel.setTaskId(0);
            if(kitDistributionId != 0){
                attachmentMetaDataModel.setAttachmentSourceId(kitDistributionId);
            }
        }
        attachmentMetaDataModel.setSequenceNo(Util.getmSequenceNumber());
        if(fromGuardDetailActivity){
            setAttachmentMetaDataModel(attachmentMetaDataModel);
        }

    }

}
