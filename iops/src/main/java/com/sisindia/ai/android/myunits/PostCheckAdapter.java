package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.PostData;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 30-04-2016.
 */
public class PostCheckAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    ArrayList<Object> addPostList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;


    public PostCheckAdapter(Context context) {
        //this.screenName = screenName;
        this.context = context;

    }


    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setPostListData(ArrayList<Object> addPostList) {
        this.addPostList = addPostList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;

        if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.header_row, parent, false);
            recycleViewHolder = new HeaderViewHolder(itemView);

        } else if (viewType == TYPE_ROW) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_addtask_row, parent, false);
            recycleViewHolder = new RotaViewHolder(itemView);

        }

        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.headerTextview.setText((String) addPostList.get(position));
                break;
            case TYPE_ROW:
                RotaViewHolder rowDataHolder = (RotaViewHolder) holder;
                Object object = getObject(position);

                rowDataHolder.checkingTextview.setText(((PostData) object).getPostName());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                params.height = Util.dpToPx(Constants.POST_IMAGE_HEIGHT);
                params.width = Util.dpToPx(Constants.POST_IMAGE_HEIGHT);
                int marginsLeft = Util.dpToPx(Constants.POST_IMAGE_MARIN_LEFT);
                int marginsTop = Util.dpToPx(Constants.POST_IMAGE_MARIN_TOP);
                int marginsBottom = Util.dpToPx(Constants.POST_IMAGE_MARIN_BOTTOM);
                int marginRight = Util.dpToPx(Constants.POST_IMAGE_MARIN_LEFT);
                params.setMargins(marginsLeft, marginsTop, marginRight, marginsBottom);

                rowDataHolder.typeIcon.setLayoutParams(params);
                rowDataHolder.typeIcon.setImageResource(((PostData) object).getIcon());
                rowDataHolder.nextIcon.setImageResource(R.drawable.arrow_right);


                break;
        }


    }

    @Override
    public int getItemViewType(int position) {

        if (addPostList.get(position) instanceof String)
            return TYPE_HEADER;
        if (addPostList.get(position) instanceof PostData)
            return TYPE_ROW;
        return -1;
    }

    @Override
    public int getItemCount() {
        int count = addPostList == null ? 0 : addPostList.size();
        return count;
    }

    private Object getObject(int position) {
        return addPostList.get(position);
    }

    public class RotaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView checkingTextview;
        ImageView typeIcon, nextIcon;

        public RotaViewHolder(View view) {
            super(view);
            this.checkingTextview = (CustomFontTextview) view.findViewById(R.id.checkType);
            this.typeIcon = (ImageView) view.findViewById(R.id.checkTypeImage);
            this.nextIcon = (ImageView) view.findViewById(R.id.nextarrow);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Object object = addPostList.get(getLayoutPosition());
            if (object instanceof PostData && ((PostData) object).getPostStatus() == 0) {
                onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
            }


        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView headerTextview;

        public HeaderViewHolder(View view) {
            super(view);
            this.headerTextview = (CustomFontTextview) view.findViewById(R.id.recycle_header);
        }

    }
}


