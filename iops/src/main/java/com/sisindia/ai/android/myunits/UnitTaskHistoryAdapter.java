package com.sisindia.ai.android.myunits;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.myunits.models.UnitHistoryMO;
import com.sisindia.ai.android.myunits.models.UnitTaskHistoryData;
import com.sisindia.ai.android.myunits.models.UnitTaskHistoryHeader;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.util.ArrayList;

/**
 * Created by Durga Prasad on 02-04-2016.
 */
public class UnitTaskHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_ROW = 2;
    ArrayList<Object> unitTaskHistoryList;
    Context context;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private  DateTimeFormatConversionUtil dateTimeFormatConversionUtil;


    public UnitTaskHistoryAdapter(Context context) {
        this.context = context;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener itemListener) {
        this.onRecyclerViewItemClickListener = itemListener;
    }

    public void setUnitTaskHistoryData(ArrayList<Object> unitTaskHistoryList) {
        this.unitTaskHistoryList = unitTaskHistoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder recycleViewHolder = null;

        if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.unit_task_history_header, parent, false);
            recycleViewHolder = new UnitTaskHistoryHeaderViewHolder(itemView);

        } else if (viewType == TYPE_ROW) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.unit_task_history_row, parent, false);
            recycleViewHolder = new UnitTaskHistoryRowViewHolder(itemView);

        }

        return recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                UnitTaskHistoryHeaderViewHolder headerViewHolder = (UnitTaskHistoryHeaderViewHolder) holder;
                headerViewHolder.unitTaskHistoryThisMonthCount.setText(((UnitTaskHistoryHeader) unitTaskHistoryList.get(position)).getThisMonthCount());
                headerViewHolder.unitTaskHistoryLastMonthCount.setText(((UnitTaskHistoryHeader) unitTaskHistoryList.get(position)).getLastMonthCount());
                break;
            case TYPE_ROW:
                UnitTaskHistoryRowViewHolder rowDataHolder = (UnitTaskHistoryRowViewHolder) holder;
                String dateAndTime =  ((UnitHistoryMO) unitTaskHistoryList.get(position)).getLastActivityDate();
                if(dateAndTime != null && dateAndTime.length() !=0){
                    String date = (dateAndTime.split(" "))[0];
                    String time = (dateAndTime.split(" "))[1];
                    String requiredDate = dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(date);
                    String requiredTime = dateTimeFormatConversionUtil.timeConverstionFrom24To12WithoutSeconds(time);
                    rowDataHolder.unitTaskHistoryDate.setText(requiredDate);
                    rowDataHolder.unitTaskHistoryTime.setText(requiredTime);

                }



                break;
        }


    }

    @Override
    public int getItemViewType(int position) {

        if (unitTaskHistoryList.get(position) instanceof UnitTaskHistoryHeader)
            return TYPE_HEADER;
        if (unitTaskHistoryList.get(position) instanceof UnitHistoryMO)
            return TYPE_ROW;
        return -1;
    }

    @Override
    public int getItemCount() {

        int count = unitTaskHistoryList == null ? 0 : unitTaskHistoryList.size();

        return count;
    }

    private Object getObject(int position) {
        return unitTaskHistoryList.get(position);
    }

    public class UnitTaskHistoryRowViewHolder extends RecyclerView.ViewHolder  {

        TextView unitTaskHistoryDate, unitTaskHistoryTime;

        public UnitTaskHistoryRowViewHolder(View view) {
            super(view);
            this.unitTaskHistoryDate = (CustomFontTextview) view.findViewById(R.id.unittask_history_date_txt);
            this.unitTaskHistoryTime = (CustomFontTextview) view.findViewById(R.id.unittask_history_time_txt);


        }



    }

    public class UnitTaskHistoryHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView unitTaskHistoryThisMonthCount;
        TextView unitTaskHistoryLastMonthCount;

        public UnitTaskHistoryHeaderViewHolder(View view) {
            super(view);
            this.unitTaskHistoryThisMonthCount = (CustomFontTextview) view.findViewById(R.id.unit_task_history_this_month_count_txt);
            this.unitTaskHistoryLastMonthCount = (CustomFontTextview) view.findViewById(R.id.unit_task_history_last_month_count_txt);
        }

    }
}

