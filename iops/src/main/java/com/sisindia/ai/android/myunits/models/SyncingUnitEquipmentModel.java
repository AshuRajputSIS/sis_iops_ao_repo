package com.sisindia.ai.android.myunits.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Saruchi on 10-05-2016.
 */
public class SyncingUnitEquipmentModel implements Parcelable {

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("UnitId")
    @Expose
    private Integer UnitId;
    @SerializedName("EquipmentId")
    @Expose
    private Integer EquipmentId;
    @SerializedName("IsCustomerProvided")
    @Expose
    private Integer IsCustomerProvided;
    @SerializedName("Status")
    @Expose
    private Integer Status;
    @SerializedName("EquipmentUniqueNo")
    @Expose
    private String EquipmentUniqueNo;
    @SerializedName("EquipmentManufacturer")
    @Expose
    private String EquipmentManufacturer;
    @SerializedName("EquipmentTypeId")
    @Expose
    private Integer EquipmentTypeId;
    @SerializedName("PostId")
    @Expose
    private Integer PostId;

    private   AddEquipmentLocalMO addEquipmentLocalMO = new AddEquipmentLocalMO();

    public AddEquipmentLocalMO getAddEquipmentLocalMO() {
        return addEquipmentLocalMO;
    }

    public void setAddEquipmentLocalMO(AddEquipmentLocalMO addEquipmentLocalMO) {
        this.addEquipmentLocalMO = addEquipmentLocalMO;
    }

    /**
     * @return The Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The UnitId
     */
    public Integer getUnitId() {
        return UnitId;
    }

    /**
     * @param UnitId The UnitId
     */
    public void setUnitId(Integer UnitId) {
        this.UnitId = UnitId;
    }

    /**
     * @return The EquipmentId
     */
    public Integer getEquipmentId() {
        return EquipmentId;
    }

    /**
     * @param EquipmentId The EquipmentId
     */
    public void setEquipmentId(Integer EquipmentId) {
        this.EquipmentId = EquipmentId;
    }

    /**
     * @return The IsCustomerProvided
     */
    public Integer getIsCustomerProvided() {
        return IsCustomerProvided;
    }

    /**
     * @param IsCustomerProvided The IsCustomerProvided
     */
    public void setIsCustomerProvided(Integer IsCustomerProvided) {
        this.IsCustomerProvided = IsCustomerProvided;
    }

    /**
     * @return The Status
     */
    public Integer getStatus() {
        return Status;
    }

    /**
     * @param Status The Status
     */
    public void setStatus(Integer Status) {
        this.Status = Status;
    }

    /**
     * @return The EquipmentUniqueNo
     */
    public String getEquipmentUniqueNo() {
        return EquipmentUniqueNo;
    }

    /**
     * @param EquipmentUniqueNo The EquipmentUniqueNo
     */
    public void setEquipmentUniqueNo(String EquipmentUniqueNo) {
        this.EquipmentUniqueNo = EquipmentUniqueNo;
    }

    /**
     * @return The EquipmentManufacturer
     */
    public String getEquipmentManufacturer() {
        return EquipmentManufacturer;
    }

    /**
     * @param EquipmentManufacturer The EquipmentManufacturer
     */
    public void setEquipmentManufacturer(String EquipmentManufacturer) {
        this.EquipmentManufacturer = EquipmentManufacturer;
    }

    /**
     * @return The EquipmentTypeId
     */
    public Integer getEquipmentTypeId() {
        return EquipmentTypeId;
    }

    /**
     * @param EquipmentTypeId The EquipmentTypeId
     */
    public void setEquipmentTypeId(Integer EquipmentTypeId) {
        this.EquipmentTypeId = EquipmentTypeId;
    }

    /**
     * @return The PostId
     */
    public Integer getPostId() {
        return PostId;
    }

    /**
     * @param PostId The PostId
     */
    public void setPostId(Integer PostId) {
        this.PostId = PostId;
    }


    @Override
    public String toString() {
        return "UnitEquipment{" +
                "Id=" + Id +
                ", UnitId=" + UnitId +
                ", EquipmentId=" + EquipmentId +
                ", IsCustomerProvided=" + IsCustomerProvided +
                ", Status=" + Status +
                ", EquipmentUniqueNo='" + EquipmentUniqueNo + '\'' +
                ", EquipmentManufacturer='" + EquipmentManufacturer + '\'' +
                ", EquipmentTypeId='" + EquipmentTypeId + '\'' +
                ", PostId=" + PostId +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Id);
        dest.writeValue(this.UnitId);
        dest.writeValue(this.EquipmentId);
        dest.writeValue(this.IsCustomerProvided);
        dest.writeValue(this.Status);
        dest.writeString(this.EquipmentUniqueNo);
        dest.writeString(this.EquipmentManufacturer);
        dest.writeValue(this.EquipmentTypeId);
        dest.writeValue(this.PostId);
        dest.writeParcelable(this.addEquipmentLocalMO, flags);
    }

    public SyncingUnitEquipmentModel() {
    }

    protected SyncingUnitEquipmentModel(Parcel in) {
        this.Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.UnitId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.EquipmentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.IsCustomerProvided = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.EquipmentUniqueNo = in.readString();
        this.EquipmentManufacturer = in.readString();
        this.EquipmentTypeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PostId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.addEquipmentLocalMO = in.readParcelable(AddEquipmentLocalMO.class.getClassLoader());
    }

    public static final Parcelable.Creator<SyncingUnitEquipmentModel> CREATOR = new Parcelable.Creator<SyncingUnitEquipmentModel>() {
        @Override
        public SyncingUnitEquipmentModel createFromParcel(Parcel source) {
            return new SyncingUnitEquipmentModel(source);
        }

        @Override
        public SyncingUnitEquipmentModel[] newArray(int size) {
            return new SyncingUnitEquipmentModel[size];
        }
    };
}
