package com.sisindia.ai.android.rota.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nischala on 20/5/16.
 */
public class RotaTaskActivityListMO implements Serializable {


    private List<RotaTaskListMO> rotaTaskListMO;

    public List<RotaTaskListMO> getRotaTaskListMO() {
        return rotaTaskListMO;
    }

    public void setRotaTaskListMO(List<RotaTaskListMO> rotaTaskListMO) {
        this.rotaTaskListMO = rotaTaskListMO;
    }


}
