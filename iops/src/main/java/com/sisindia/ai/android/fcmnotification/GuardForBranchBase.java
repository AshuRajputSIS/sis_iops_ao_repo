package com.sisindia.ai.android.fcmnotification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardForBranchBase {

    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;
    @SerializedName("Data")
    @Expose
    public Data data;

}