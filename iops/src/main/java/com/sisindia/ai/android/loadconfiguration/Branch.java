package com.sisindia.ai.android.loadconfiguration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */


public class Branch implements Serializable{

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("RegionId")
    @Expose
    private Integer regionId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("BranchCode")
    @Expose
    private String branchCode;
    @SerializedName("BranchAddressId")
    @Expose
    private Integer branchAddressId;
    @SerializedName("BranchAddress")
    @Expose
    private String branchAddress;
    @SerializedName("GeoLatitude")
    @Expose
    private double geoLatitude;
    @SerializedName("GeoLongitude")
    @Expose
    private double geoLongitude;
    /* @SerializedName("BranchCoordinates")
     @Expose
     private double branchCoordinates;*/
    @SerializedName("StrokeColor")
    @Expose
    private String strokeColor;
    @SerializedName("FillColor")
    @Expose
    private String fillColor;
    @SerializedName("FullAddress")
    @Expose
    private String fullAddress;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The regionId
     */
    public Integer getRegionId() {
        return regionId;
    }

    /**
     * @param regionId The RegionId
     */
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return The branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param branchCode The BranchCode
     */
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * @return The branchAddressId
     */
    public Integer getBranchAddressId() {
        return branchAddressId;
    }

    /**
     * @param branchAddressId The BranchAddressId
     */
    public void setBranchAddressId(Integer branchAddressId) {
        this.branchAddressId = branchAddressId;
    }

    /**
     * @return The branchAddress
     */
    public String getBranchAddress() {
        return branchAddress;
    }

    /**
     * @param branchAddress The BranchAddress
     */
    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    /**
     * @return The geoLatitude
     */
    public double getGeoLatitude() {
        return geoLatitude;
    }

    /**
     * @param geoLatitude The GeoLatitude
     */
    public void setGeoLatitude(double geoLatitude) {
        this.geoLatitude = geoLatitude;
    }

    /**
     * @return The geoLongitude
     */
    public double getGeoLongitude() {
        return geoLongitude;
    }

    /**
     * @param geoLongitude The GeoLongitude
     */
    public void setGeoLongitude(double geoLongitude) {
        this.geoLongitude = geoLongitude;
    }

  /*  *//**
     * @return The branchCoordinates
     *//*
    public double getBranchCoordinates() {
        return branchCoordinates;
    }

    *//**
     * @param branchCoordinates The BranchCoordinates
     *//*
    public void setBranchCoordinates(double branchCoordinates) {
        this.branchCoordinates = branchCoordinates;
    }*/

    /**
     * @return The strokeColor
     */
    public String getStrokeColor() {
        return strokeColor;
    }

    /**
     * @param strokeColor The StrokeColor
     */
    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    /**
     * @return The fillColor
     */
    public String getFillColor() {
        return fillColor;
    }

    /**
     * @param fillColor The FillColor
     */
    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * @return The fullAddress
     */
    public String getFullAddress() {
        return fullAddress;
    }

    /**
     * @param fullAddress The FullAddress
     */
    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

}
