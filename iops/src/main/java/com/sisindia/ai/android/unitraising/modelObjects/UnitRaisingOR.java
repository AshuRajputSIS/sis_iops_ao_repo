package com.sisindia.ai.android.unitraising.modelObjects;


import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.network.response.GeoLocation;

public class UnitRaisingOR {

    @SerializedName("UnitId")
    public Integer unitId;
    @SerializedName("UnitCode")
    public String unitCode;
    @SerializedName("RaisingDetail")
    public String raisingDetail;
    @SerializedName("UnitGeoLocation")
    public String unitGeoLocation;
    @SerializedName("UnitRaisedGeoLocation")
    public GeoLocation unitRaisedGeoLocation;
    @SerializedName("Remarks")
    public String remarks;
    @SerializedName("RaisedBy")
    public Integer raisedBy;
    @SerializedName("RaisedDateTime")
    public String raisedDateTime;

}