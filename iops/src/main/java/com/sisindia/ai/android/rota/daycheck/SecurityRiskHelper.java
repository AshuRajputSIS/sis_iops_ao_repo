package com.sisindia.ai.android.rota.daycheck;

import com.sisindia.ai.android.rota.daycheck.taskExecution.SecurityRisk;
import com.sisindia.ai.android.rota.daycheck.taskExecution.SecurityRiskAnswerMO;
import com.sisindia.ai.android.rota.models.SecurityRiskModelMO;
import com.sisindia.ai.android.rota.models.SecurityRiskOptionMO;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Shushrut on 14-07-2016.
 */
public class SecurityRiskHelper {
    private static SecurityRiskHelper instance;
    private List<SecurityRiskAnswerMO> securityRiskAnswerMOs = new ArrayList<>();
    public static SecurityRiskHelper getInstance(){
        if(instance == null){
            instance = new SecurityRiskHelper();
        }
        return instance;
    }
    public void setDefaultAnswers(List<SecurityRiskModelMO> securityQuestionModels){
        securityRiskAnswerMOs = new ArrayList<>();
        for(SecurityRiskModelMO securityRiskQuestion : securityQuestionModels){
            SecurityRiskAnswerMO securityRiskAnswerMO = new SecurityRiskAnswerMO();
            securityRiskAnswerMO.setSecurityRiskQuestionId(securityRiskQuestion.getSecurityRiskQuestionId());
            //if(securityRiskQuestion.getmRiskTypeHolder() != null) {
                SecurityRiskOptionMO option = securityRiskQuestion.getmRiskTypeHolder().get(0);
                securityRiskAnswerMO.setSecurityRiskOptionId(option.getSecurityRiskOptionId());
                securityRiskAnswerMO.setControlType(option.getControlType());
                securityRiskAnswerMO.setIsMandatory(option.getIsMandatory());
                securityRiskAnswerMO.setUserComments(option.getmUserComments());
                if (option.getIsMandatory() == 1 && !securityRiskAnswerMO.isFilled()) {
                    securityRiskAnswerMO.setFilled(false);
                }
           // }
            securityRiskAnswerMOs.add(securityRiskAnswerMO);
        }
    }
    public void updateAnswer(Integer questionId, SecurityRiskOptionMO option){
        for(SecurityRiskAnswerMO securityRiskAnswerMO: securityRiskAnswerMOs){
            if(securityRiskAnswerMO.getSecurityRiskQuestionId().equals(questionId)){
                securityRiskAnswerMO.setSecurityRiskOptionId(option.getSecurityRiskOptionId());
                securityRiskAnswerMO.setControlType(option.getControlType());
                securityRiskAnswerMO.setIsMandatory(option.getIsMandatory());
                // set the filled as false only if its not filled .
                if(option.getIsMandatory() == 1 && !securityRiskAnswerMO.isFilled()){
                    securityRiskAnswerMO.setFilled(false);
                }
                break;
            }
        }
    }
    public void completedAnswer(Integer questionId, SecurityRiskOptionMO option){
        for(SecurityRiskAnswerMO securityRiskAnswerMO: securityRiskAnswerMOs){
            if(securityRiskAnswerMO.getSecurityRiskQuestionId().equals(questionId)){
                securityRiskAnswerMO.setSecurityRiskOptionId(option.getSecurityRiskOptionId());
                securityRiskAnswerMO.setControlType(option.getControlType());
                securityRiskAnswerMO.setIsMandatory(option.getIsMandatory());
                if(option.getmUserComments()!= null) {
                    securityRiskAnswerMO.setUserComments(option.getmUserComments());
                }
                if(option.getIsMandatory() == 1){
                    securityRiskAnswerMO.setFilled(true);
                }
                break;
            }
        }
    }
    public boolean validateAnswers(){
        for (SecurityRiskAnswerMO securityRiskAnswerMO : securityRiskAnswerMOs){
            Timber.i("Validaton Values","Validation Values ::"+securityRiskAnswerMO);
            if(securityRiskAnswerMO.getIsMandatory().equals(1) && !securityRiskAnswerMO.isFilled())
                return false;
        }
        return true;
    }

    public ArrayList<SecurityRisk> getSecurityRiskData(){
        ArrayList<SecurityRisk> mSeurityRiskData = new ArrayList<>();
        for (SecurityRiskAnswerMO securityRiskAnswerMO : securityRiskAnswerMOs){
            SecurityRisk mSecurityRisk = new SecurityRisk();
            mSecurityRisk.setOptionId(securityRiskAnswerMO.getSecurityRiskOptionId());
            mSecurityRisk.setQuestionId(securityRiskAnswerMO.getSecurityRiskQuestionId());
            mSecurityRisk.setRemarks(securityRiskAnswerMO.getUserComments());
            mSeurityRiskData.add(mSecurityRisk);
        }
        return mSeurityRiskData;
    }


}
