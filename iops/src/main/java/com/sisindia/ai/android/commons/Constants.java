package com.sisindia.ai.android.commons;

import android.os.Environment;

import java.io.File;

/**
 * Created by shankar on 2/3/16.
 */
public interface Constants {
    // The account name
    String ACCOUNT = "SISTracking"; // if changed, change in stings.xml also
    // An account type, in the form of a domain name
    String ACCOUNT_TYPE = "sis.com";                            // if changed, change in stings.xml also
    String APP_PATH = Environment.getExternalStorageDirectory().getPath() + "/iops";
    String ARG_PARAM1 = "param1";
    String ARG_PARAM2 = "param2";
    String ATTACHMENT_DETAILS = "attachmentDetails";
    String AUTHORITY = "com.sisindia.ai.android.syncadpter.provider";   // if changed, change in stings.xml also
    String BILL_SUBMISSION = "BillSubmission";
    String BILL_COLLECTION = "BillCollection";


    String BARRACK_BEDDING = "BarrackBedding";
    String BARRACK_KIT = "BarrackKit";
    String BARRACK_MESS = "BarrackMess";
    String BARRACK_OTHERS = "Barrack(Outside)";
    String CLIENT_COORDINATION = "ClientCoordination";
    String CLIENT_COORDINATION_SELFIE = "ClientCoordinationSelfie";
    String NC_SELFIE = "NightCheckSelfie";
    String OTHER_ACTIVITY = "OtherActivity";

    String CHECKING_TYPE = "checkingType";
    String CHECKING_TYPE_IMAGE_POSITION = "position";
    String CURRENT_TAKEN_IMAGE_PATH = "currentTakenImagePath";
    String EXCEPTION = "exception";
    String FILE_NAME = "iops.apk";
    String FILE_TYPE_CLIENT_REGISTER_CHECK = "ClientRegisterCheck";
    String FILE_TYPE_VEHICLE_REGISTER_CHECK = "VehicleRegisterCheck";
    String FILE_TYPE_MATERIAL_REGISTER_CHECK = "MaterialRegisterCheck";
    String FILE_TYPE_VISITOR_REGISTER_CHECK = "VisitorRegisterCheck";
    String FILE_TYPE_MAINTENANCE_REGISTER_CHECK = "MaintenanceRegisterCheck";
    String FILE_TYPE_KIT_ITEM_REQUEST = "KitItemRequest";
    String FILE_TYPE_KIT_ITEM_SIGNATURE = "KitItemSignature";
    String FILE_TYPE_COMPLAINT = "Complaint";
    String FILE_TYPE_DUTY_REGISTER_CHECK = "DutyRegisterCheck";
    String FILE_TYPE_GREIVANCE = "Greivance";
    String FILE_TYPE_GUARD = "Guard";
    String FILE_TYPE_GUARD_FULL_IMAGE = "GuardFullImage";
    String FILE_TYPE_GUARD_TURN_OUT = "GuardTurnOut";
    String FILE_TYPE_UNIT_POST = "UnitPost";
    String FILE_TYPE_UNIT_TASK = "UnitTask";
    String FilePath = "";
    String GUARD_DETAILS = "guard_details";
    String INTERNETENABLED = "INTERNETENABLED";
    String IOPS_IMAGES = APP_PATH + "/" + "/images";
    String META_DATA = "meta_data";
    String NAVIGATION_FROM = "navigationFrom";
    String NEW_APK_PATH = APP_PATH + "/" + FILE_NAME;
    String NOINTERNET = "NOINTERNET";
    String NUMBER_STRING = "1";
    String OTP_DELIMITER = ":";
    String PIC_TAKEN_FROM_SITEPOST_CHECK_KEY = "takepic";
    String SCHEME = "content://";    // The authority for the sync adapter's content provider
    String SIGNATURE = "Signature";
    String SMS_ORIGIN = "";
    String SQLITE_CONSTRAINT_EXCEPTION = "sql_constraint_exception";
    String SQLITE_EXCEPTION = "sql_exception";
    String TAG = "RotaTaskActivityListActivity";
    int GUARD_DETAILS_CODE = 3;
    int HEIGT = 56;
    int IMAGE_SEQUENCE_NUMBER = 0;
    int MARGIN_BOTTOM = 32;
    int MARGIN_LEFT_RIGHT = 17;
    int MYUNITS_ADD_CONTACTS_REQUEST_CODE = 4;
    int MYUNITS_ADD_POST_REQUEST_CODE = 3;
    int NUMBER_ONE = 1;
    int NUMBER_TWO = 2;
    int PIC_TAKEN_FROM_SITEPOST = 1;
    int POST_IMAGE_HEIGHT = 48;
    int POST_IMAGE_MARIN_BOTTOM = 8;
    int POST_IMAGE_MARIN_LEFT = 16;
    int POST_IMAGE_MARIN_TOP = 8;
    int ZERO = 0;
    String SECURITY_RISK = "security_risk";
    int ISSUE_GREIVANCE_CODE = 106;
    int DAYCHECK_GREIVANCE_CODE = 107;
    int AUDIO_FILE_CODE = 107;
    String AUDIO_FILE_FOLDER = "iOPS AudioFiles";
    String AUDIO_FILE_PATH = Environment.getExternalStorageDirectory().toString() + File.separator + AUDIO_FILE_FOLDER;
    String AUDIO_FILE_EXTENSION = ".m4a";
    int BARRACK_BEDDING_IMAGE_CODE = 101;
    int BARRACK_KIT_IMAGE_CODE = 102;
    int BARRACK_MESS_IMAGE_CODE = 103;
    int BARRACK_OTHERS_IMAGE_CODE = 104;
    int BARRACK_GREIVANCE_CODE = 105;
    int MARGIN_TOP = 5;
    // Attachment Type Ids
    int LOOKUP_TYPE_IMAGE = 3;
    int LOOKUP_TYPE_AUDIO = 1;

    int KIT_REPLACE_IMAGE_REQUEST_CODE = 108;
    String KIT_REPLACMENT = "KitReplacementRequest";
    int IMAGE_HEIGHT = 960;
    int IMAGE_WIDTH = 540;
    String FILE_TYPE_KIT_DISTRIBUTION_ITEM_IMAGE = "KitDistribution";
    String DISTRIBUTION_SIGNATURE = "DistributionSignature";
    int KIT_DISTRIBUTION_IMAGE_REQUEST_CODE = 110;
    String TAG_SYNCADAPTER = "SyncAdapter ";
    String TRAINING_VIDEOS_FOLDER = "iops help videos";
    String VIDEOS_FILE_PATH = Environment.getExternalStorageDirectory().toString() +
            File.separator + TRAINING_VIDEOS_FOLDER;
    String LOCALE_ENGLISH_INDENTIFIER = "en";
    String LOCALE_HINDI_INDENTIFIER = "hi";
    int ENGLISH_LANGUAGE_ID = 1;
    int HINDI_LANGUAGE_ID = 2;
    int ADHOC_KIT_ITEM_TYPE = 2;
    int ANNUALKIT_ITEM_TYPE = 1;
    String VIDEO_LINK = "Video";
    int IS_OLD = 0;
    int IS_SYNCED = 1;
    int ALARM_SERVICE_ID = 1;
    int ALRAM_TRIGGER_TIME_INTERVAL = 30 * 60 * 1000;
    int MORNING_MAX_ALARM_TRIGGER_COUNT = 3;
    int MAX_ALARM_TRIGGER_COUNT = 8;
    int SYNC_ALARM_START_TIME_IN_HOURS = 20;
    int MORNING_SYNC_ALARM_START_TIME_IN_HOURS = 6;
    String MORNING_ALARM_SYNC_START_TIME = " 06:00:00 ";
    String MORNING_ALARM_SYNC_END_TIME = " 07:01:00 ";
    String ALARM_SYNC_START_TIME = " 20:00:00 ";
    String ALARM_SYNC_END_TIME = " 22:01:00 ";
    int DEFAULT_SYNC_COUNT = 0;

    int TASK_RELATED_SYNC = 1;
    int UNIT_POST_RELATED_SYNC = 2;
    int KIT_DISTRIBUTION_RELATED_SYNC = 3;
    int ISSUE_RELATED_SYNC = 4;
    int BARRACK_POST_SYNC = 6;

    int NEG_DEFAULT_COUNT = -5;
    int MET_LANLORD_FRAGMENT = 4;
    int SPACE_FRAGMENT = 3;
    int BARRACK_LANDLORD_IMAGE_CODE = 105;
    int BARRACK_CUSTODIAN_IMAGE_CODE = 106;
    String BARRACK_CUSTODIAN = "BarrackCustodian";
    String BARRACK_LANDLORD = "BarrackLandlord";
    String FILE_TYPE_UNIT_RAISING_IMAGE = "UnitRaisingDetails";
    String FILE_TYPE_UNIT_RAISING_CANCELLED_IMAGE = "UnitRaisingCancelled";
    String FILE_TYPE_UNIT_RAISING_DEFERRED_IMAGE = "UnitRaisingDeferred";
    int UNIT_RAISING_DEFERRED_OR_CANCELLED = 111;
    int UNIT_RAISING_SYNC = 5;
    int UNIT_RAISING_CANCELLATION_SYNC = 6;
    int UNIT_RAISING_DEFERRED_SYNC = 7;
    int ALARM_SERVICE_ID_FOR_ATTENDANCE = 100;
    int NIGHT_DUTY_ON_OFF_VARIABLE = 110;
    String SYNC_TRIGGER_TIME_AFTER_BLOCKING_AI = "sync_trigger_time";
    int ALARM_SERVICE_ID_FOR_SYNC_TRIGGER_TIME = 101;
    String BARRACK = "BARRACK";
    String NIGHT_DUTY_ON_OFF = "BARRACK";

    interface Nfc {
        String NFC_SCAN = "com.sisindia.ai.android.NFC_SCAN";
        String NFC_UNIQUE_SCAN = "com.sisindia.ai.android.NFC_UNIQUE_SCAN";
        String CHECKPOINT_SCAN = "com.sisindia.ai.android.CHECKPOINT_SCAN";
        String NFC_UNIQUE_ID = "nfc_unique_id";
        String UNIT_ID = "unitId";
        String DEVICE_NO = "deviceNo";
    }

    interface NavigationFlags {
        int TYPE_GRIEVANCE = 1;
        int TYPE_COMPLAINTS = 2;
        int TYPE_IMPROVEMENT_PLAN = 3;
    }

    interface TableNamesToSync {
        int ATTACHMENT_OR_METADATA = 1;
        int BARRACK = 2;
        int IMPROVEMENT_PLAN = 3;
        int KIT_ITEM_REQUEST = 4;
        int KIT_DISTRIBUTION = 5;
        int RECRUITMENT = 6;
        int ROTA_TASK = 7;
        int UNIT_STRENGTH = 8;
        int UNIT_CONTACT = 9;
        int UNIT_EQUIPMENT = 10;
        int UNIT_POST = 11;
        int ISSUES = 12;
        int FILE_UPLOAD = 13;

    }


    String ATTENDANCE_SCHEDULE = "attendance_schedule";
    String DELETE_SCHEDULE = "delete_schedule";

}
