package com.sisindia.ai.android.syncadpter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.database.tableSyncCountDTO.FileUploadCount;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.AttachmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.BarrackTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.ImprovementPlanTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.IssuesTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitDistributionItemTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.KitItemRequestTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.RecruitmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.TaskTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitContactTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitEquipmentTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitPostTableCountMO;
import com.sisindia.ai.android.database.tableSyncCountDTO.modelObjects.UnitStrengthTableCountMO;
import com.sisindia.ai.android.settings.TableSyncCountActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by compass on 5/15/2017.
 */

public class SyncStatusNotification {


    private final Context context;
    private Map<Integer,Boolean> syncTablesMap;

    public  SyncStatusNotification(Context context){
        this.context = context;
        syncTablesMap = new HashMap<>();

    }

    private NotificationCompat.InboxStyle inboxStyle;

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public boolean isAnyRecordsToSync(){
        tablesToSync();
        if(syncTablesMap != null && syncTablesMap.size() != 0){
            return syncTablesMap.values().contains(Boolean.FALSE);
        }
        return  false;
    }

    public synchronized  void tablesToSync() {

        String  failedSyncs = "";
        inboxStyle = buildNotification(context,failedSyncs,"");

        AttachmentTableCountMO attachmentTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getAttachmentTableCount();
        BarrackTableCountMO barrackTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getBarrackTableCount();
        ImprovementPlanTableCountMO improvementPlanTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getImprovementPlanTableCount();
        KitItemRequestTableCountMO kitItemRequestTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getKitItemRequestTableCount();
        KitDistributionItemTableCountMO kitDistributionItemTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getKitDistributionItemTableCount();
        RecruitmentTableCountMO recruitmentTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getRecruitmentTableCount();
        TaskTableCountMO taskTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getTaskTableCount();
        UnitStrengthTableCountMO unitStrengthTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getUnitStrengthTableCount();
        UnitContactTableCountMO unitContactTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getUnitContactTableCount();
        UnitEquipmentTableCountMO unitEquipmentTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getUnitEquipmentTableCount();
        UnitPostTableCountMO unitPostTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getUnitPostTableCount();
        IssuesTableCountMO issuesTableCountMO = IOPSApplication.getInstance()
                .getTableSyncCount().getIssueTableCount();
        FileUploadCount fileUploadCount = IOPSApplication.getInstance()
                .getTableSyncCount().getFileUploadCount();

        if(attachmentTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.ATTACHMENT_OR_METADATA,true);
        }
        else {
            inboxStyle.addLine("attachment");
            failedSyncs = failedSyncs +  "attachment \n";
            syncTablesMap.put(Constants.TableNamesToSync.ATTACHMENT_OR_METADATA,false);
        }
        if(barrackTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.BARRACK,true);
        }
        else {
            inboxStyle.addLine("barrack");
            failedSyncs = failedSyncs +  "barrack  \n";
            syncTablesMap.put(Constants.TableNamesToSync.BARRACK,false);
        }
        if(improvementPlanTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.IMPROVEMENT_PLAN,true);
        }
        else {
            inboxStyle.addLine("improvementplan \n");
            failedSyncs = failedSyncs +  "improvementplan \n";
            syncTablesMap.put(Constants.TableNamesToSync.IMPROVEMENT_PLAN,false);
        }
        if(kitItemRequestTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.KIT_ITEM_REQUEST,true);
        }
        else {
            inboxStyle.addLine("kit item request \n");
            failedSyncs = failedSyncs +  "kit item request \n";
            syncTablesMap.put(Constants.TableNamesToSync.KIT_ITEM_REQUEST,false);
        }
        if(kitDistributionItemTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.KIT_DISTRIBUTION,true);
        }
        else {
            inboxStyle.addLine("kit distribution \n");
            failedSyncs = failedSyncs +  "kit distribution \n";
            syncTablesMap.put(Constants.TableNamesToSync.KIT_DISTRIBUTION,false);
        }
        if(recruitmentTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.RECRUITMENT,true);
        }
        else {
            inboxStyle.addLine("recruitment \n");
            failedSyncs = failedSyncs +  "recruitment \n";
            syncTablesMap.put(Constants.TableNamesToSync.RECRUITMENT,false);
        }
        if(taskTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.ROTA_TASK,true);
        }
        else {
            inboxStyle.addLine("rota task \n");
            failedSyncs = failedSyncs +  "rota task \n";
            syncTablesMap.put(Constants.TableNamesToSync.ROTA_TASK,false);
        }
        if(unitStrengthTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_STRENGTH,true);
        }
        else {
            inboxStyle.addLine("unit strength \n");
            failedSyncs = failedSyncs +  "unit strength \n";
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_STRENGTH,false);
        }
        if(unitContactTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_CONTACT,true);
        }
        else {
            inboxStyle.addLine( "unit contact \n");
            failedSyncs = failedSyncs +  "unit contact \n";
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_CONTACT,false);
        }
        if(unitEquipmentTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_EQUIPMENT,true);
        }
        else {
            inboxStyle.addLine("unit equipment \n");
            failedSyncs = failedSyncs +  "unit equipment \n";
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_EQUIPMENT,false);
        }
        if(unitPostTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_POST,true);
        }
        else {
            inboxStyle.addLine("unit post \n");
            failedSyncs = failedSyncs +  "unit post \n";
            syncTablesMap.put(Constants.TableNamesToSync.UNIT_POST,false);
        }
        if(issuesTableCountMO.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.ISSUES,true);
        }
        else {
            inboxStyle.addLine("issues \n");
            failedSyncs = failedSyncs +  "issues \n";
            syncTablesMap.put(Constants.TableNamesToSync.ISSUES,false);
        }
        if(fileUploadCount.getSyncCount() == 0){
            syncTablesMap.put(Constants.TableNamesToSync.FILE_UPLOAD,true);
        }
        else {
            inboxStyle.addLine(" fileupload \n");
            failedSyncs = failedSyncs +  " fileupload \n";
            syncTablesMap.put(Constants.TableNamesToSync.FILE_UPLOAD,false);
        }
        createNotification(context,inboxStyle.build());
    }
    NotificationCompat.InboxStyle buildNotification(Context context, String content, String msg){

        Intent intent = new Intent(context, TableSyncCountActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 101, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Bitmap logoIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.iops);
        builder.setContentTitle("Sync Notification")
                .setStyle(inboxStyle)
                .setContentIntent(pendingIntent)
                .setTicker(msg)
                .setAutoCancel(false)
                .setSmallIcon(R.drawable.iops)
                .setLargeIcon(Bitmap.createScaledBitmap(logoIcon, 128, 128, false));
        inboxStyle = new NotificationCompat.InboxStyle(builder);
        inboxStyle.setBigContentTitle(content);
        if(syncTablesMap != null && syncTablesMap.size() != 0){
            inboxStyle.setSummaryText(String.valueOf(syncTablesMap.size()));
        }
        else{
            inboxStyle.setSummaryText("Nothing to sync");
        }
        return inboxStyle;
    }

    void createNotification(Context context,Notification notification){

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}
