package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmployeeDevice {

    @SerializedName("Id")
    @Expose
    public Integer Id;
    @SerializedName("AppUserId")
    @Expose
    public Integer AppUserId;
    @SerializedName("DeviceId")
    @Expose
    public Integer DeviceId;
    @SerializedName("IsAOAppInstalled")
    @Expose
    public Boolean IsAOAppInstalled;
    @SerializedName("IsReportingAppInstalled")
    @Expose
    public Boolean IsReportingAppInstalled;
    @SerializedName("Sim1PhoneNo")
    @Expose
    public String Sim1PhoneNo;
    @SerializedName("Sim2PhoneNo")
    @Expose
    public Object Sim2PhoneNo;
    @SerializedName("IMEI1")
    @Expose
    public String IMEI1;
    @SerializedName("IMEI2")
    @Expose
    public String IMEI2;

    public Integer getAppUserId() {
        return AppUserId;
    }

    public void setAppUserId(Integer appUserId) {
        AppUserId = appUserId;
    }

    public Integer getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(Integer deviceId) {
        DeviceId = deviceId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getIMEI1() {
        return IMEI1;
    }

    public void setIMEI1(String IMEI1) {
        this.IMEI1 = IMEI1;
    }

    public String getIMEI2() {
        return IMEI2;
    }

    public void setIMEI2(String IMEI2) {
        this.IMEI2 = IMEI2;
    }

    public Boolean getAOAppInstalled() {
        return IsAOAppInstalled;
    }

    public void setAOAppInstalled(Boolean AOAppInstalled) {
        IsAOAppInstalled = AOAppInstalled;
    }

    public Boolean getReportingAppInstalled() {
        return IsReportingAppInstalled;
    }

    public void setReportingAppInstalled(Boolean reportingAppInstalled) {
        IsReportingAppInstalled = reportingAppInstalled;
    }

    public String getSim1PhoneNo() {
        return Sim1PhoneNo;
    }

    public void setSim1PhoneNo(String sim1PhoneNo) {
        Sim1PhoneNo = sim1PhoneNo;
    }

    public Object getSim2PhoneNo() {
        return Sim2PhoneNo;
    }

    public void setSim2PhoneNo(Object sim2PhoneNo) {
        Sim2PhoneNo = sim2PhoneNo;
    }
}
