package com.sisindia.ai.android.commons.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.loadconfiguration.MasterActionPlanModel;

import java.util.ArrayList;

/**
 * Created by compass on 4/26/2017.
 */

public class MasterActionPlanBaseOR {
    @SerializedName("StatusCode")
    @Expose
    public Integer statusCode;

    @SerializedName("StatusMessage")
    @Expose
    public String statusMessage;

    @SerializedName("Data")
    @Expose
    public ArrayList<MasterActionPlanModel> masterActionPlanModelList;
}
