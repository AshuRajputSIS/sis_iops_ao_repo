/*
package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;
import com.sisindia.ai.android.rota.daycheck.DayNightCheckingBaseMO;

public class UnitTask {

    @SerializedName("Id")

    public Integer Id;
    @SerializedName("UnitId")

    public Integer UnitId;
    @SerializedName("TaskTypeId")

    public Integer TaskTypeId;
    @SerializedName("AssigneeId")

    public Integer AssigneeId;
    @SerializedName("EstimatedExecutionTime")

    public Integer EstimatedExecutionTime;
    @SerializedName("ActualExecutionTime")

    public Integer ActualExecutionTime;
    @SerializedName("TaskStatusId")

    public Integer TaskStatusId;
    @SerializedName("Description")

    public String Description;
    @SerializedName("EstimatedTaskExecutionStartDateTime")

    public String EstimatedTaskExecutionStartDateTime;
    @SerializedName("ActualTaskExecutionStartDateTime")

    public String ActualTaskExecutionStartDateTime;
    @SerializedName("IsAutoCreation")

    public boolean IsAutoCreation;
    @SerializedName("ExecutorId")

    public Integer ExecutorId;
    @SerializedName("CreatedDateTime")

    public String CreatedDateTime;
    @SerializedName("CreatedById")

    public Integer CreatedById;
    @SerializedName("ApprovedById")

    public Integer ApprovedById;
    @SerializedName("RelatedTaskId")

    public Integer RelatedTaskId;
    @SerializedName("UnitPostId")

    public Integer UnitPostId;
    @SerializedName("AdditionalComments")

    public String AdditionalComments;
    @SerializedName("AssignedById")

    public Integer AssignedById;
    @SerializedName("BarrackId")

    public Integer BarrackId;
    @SerializedName("TaskExecutionResult")

    public String TaskExecutionResult;
    @SerializedName("EstimatedTaskExecutionEndDateTime")

    public String EstimatedTaskExecutionEndDateTime;
    @SerializedName("ActualTaskExecutionEndDateTime")

    public String ActualTaskExecutionEndDateTime;

    @SerializedName("IsPostMandatory")
    public boolean IsPostMandatory;

    @SerializedName("CreatedDate")
    public String CreatedDate;


    private Integer isStarted = 0;



    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public Integer getIsStarted() {
        return isStarted;
    }

    public void setIsStarted(Integer isStarted) {
        this.isStarted = isStarted;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getUnitId() {
        return UnitId;
    }

    public void setUnitId(Integer unitId) {
        UnitId = unitId;
    }

    public Integer getTaskTypeId() {
        return TaskTypeId;
    }

    public void setTaskTypeId(Integer taskTypeId) {
        TaskTypeId = taskTypeId;
    }

    public Integer getAssigneeId() {
        return AssigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        AssigneeId = assigneeId;
    }

    public Integer getEstimatedExecutionTime() {
        return EstimatedExecutionTime;
    }

    public void setEstimatedExecutionTime(Integer estimatedExecutionTime) {
        EstimatedExecutionTime = estimatedExecutionTime;
    }

    public Integer getActualExecutionTime() {
        return ActualExecutionTime;
    }

    public void setActualExecutionTime(Integer actualExecutionTime) {
        ActualExecutionTime = actualExecutionTime;
    }

    public Integer getTaskStatusId() {
        return TaskStatusId;
    }

    public void setTaskStatusId(Integer taskStatusId) {
        TaskStatusId = taskStatusId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEstimatedTaskExecutionStartDateTime() {
        return EstimatedTaskExecutionStartDateTime;
    }

    public void setEstimatedTaskExecutionStartDateTime(String estimatedTaskExecutionStartDateTime) {
        EstimatedTaskExecutionStartDateTime = estimatedTaskExecutionStartDateTime;
    }

    public String getActualTaskExecutionStartDateTime() {
        return ActualTaskExecutionStartDateTime;
    }

    public void setActualTaskExecutionStartDateTime(String actualTaskExecutionStartDateTime) {
        ActualTaskExecutionStartDateTime = actualTaskExecutionStartDateTime;
    }

    public boolean getIsAutoCreation() {
        return IsAutoCreation;
    }

    public void setIsAutoCreation(boolean isAutoCreation) {
        IsAutoCreation = isAutoCreation;
    }

    public Integer getExecutorId() {
        return ExecutorId;
    }

    public void setExecutorId(Integer executorId) {
        ExecutorId = executorId;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        CreatedDateTime = createdDateTime;
    }

    public Integer getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(Integer createdById) {
        CreatedById = createdById;
    }

    public Integer getApprovedById() {
        return ApprovedById;
    }

    public void setApprovedById(Integer approvedById) {
        ApprovedById = approvedById;
    }

    public Integer getRelatedTaskId() {
        return RelatedTaskId;
    }

    public void setRelatedTaskId(Integer relatedTaskId) {
        RelatedTaskId = relatedTaskId;
    }

    public Integer getUnitPostId() {
        return UnitPostId;
    }

    public void setUnitPostId(Integer unitPostId) {
        UnitPostId = unitPostId;
    }

    public String getAdditionalComments() {
        return AdditionalComments;
    }

    public void setAdditionalComments(String additionalComments) {
        AdditionalComments = additionalComments;
    }

    public Integer getAssignedById() {
        return AssignedById;
    }

    public void setAssignedById(Integer assignedById) {
        AssignedById = assignedById;
    }

    public Integer getBarrackId() {
        return BarrackId;
    }

    public void setBarrackId(Integer barrackId) {
        BarrackId = barrackId;
    }

    public String getTaskExecutionResult() {
        return TaskExecutionResult;
    }

    public void setTaskExecutionResult(String taskExecutionResult) {
        TaskExecutionResult = taskExecutionResult;
    }

    public String getEstimatedTaskExecutionEndDateTime() {
        return EstimatedTaskExecutionEndDateTime;
    }

    public void setEstimatedTaskExecutionEndDateTime(String estimatedTaskExecutionEndDateTime) {
        EstimatedTaskExecutionEndDateTime = estimatedTaskExecutionEndDateTime;
    }

    public String getActualTaskExecutionEndDateTime() {
        return ActualTaskExecutionEndDateTime;
    }

    public void setActualTaskExecutionEndDateTime(String actualTaskExecutionEndDateTime) {
        ActualTaskExecutionEndDateTime = actualTaskExecutionEndDateTime;
    }


    public boolean getIsPostMandatory() {
        return IsPostMandatory;
    }

    public void setIsPostMandatory(boolean isPostMandatory) {
        IsPostMandatory = isPostMandatory;
    }


}*/
