package com.sisindia.ai.android.mybarrack;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.ImprovementActionPlanMO;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.DateRangeCalender;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.LookUpSpinnerAdapter;
import com.sisindia.ai.android.database.TableNameAndColumnStatement;
import com.sisindia.ai.android.database.issueDTO.IssueLevelSelectionStatementDB;
import com.sisindia.ai.android.issues.ActionPlanAdapter;
import com.sisindia.ai.android.issues.models.ImprovementPlanIR;
import com.sisindia.ai.android.rota.models.LookupModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;

import java.util.ArrayList;
import java.util.Date;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Saruchi on 15-09-2016.
 */

public class BarrackAddImprovementPlanAcitvity extends BaseActivity  {

    private final static int BARRACK_LOOKUP = 11;
    private final static int IMPROVEMENTPLAN_ISSUE_TYPEID = 4;
    @Bind(R.id.toolbar)
    Toolbar mImprovementToolBar;
    @Bind(R.id.scrollview_addplan)
    ScrollView mscrollView;
    @Bind(R.id.category_layout)
    View categoryLayout;
    @Bind(R.id.improvement_layout)
    View improvementLayout;
    @Bind(R.id.target_action_date_view)
    View mcalenderView;
    @Bind(R.id.remarks_view)
    View remarksView;
    @Bind(R.id.barrack_name)
    TextView barracknameTxt;
    @Bind(R.id.budgetEdtTxt)
    CustomFontEditText budgetEdtTxt;

    ArrayList<ImprovementActionPlanMO> improvementActionPlanList = new ArrayList<>();
    private TextView mTargetDate;
    private LinearLayout mCalenderLayout;
    private EditText remarksTxt;
    private TextView categoryName, improvementName;
    private Resources resources;
    private IssueLevelSelectionStatementDB issueLevelSelectionStatementDB;
    private LookUpSpinnerAdapter mLookUpSpinnerAdapter;
    private Spinner categorgySpinner;
    private Spinner improvementSpinner;
    private ActionPlanAdapter actionPlanAdapter;
    private int mBarrackId = -1;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String targetdateStr;
    private ImprovementPlanIR improvementPlanIRModel;
    private int taskId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barrack_improvement_plan_activity);
        ButterKnife.bind(this);
        Util.hideKeyboard(this);
        resources = getResources();
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        issueLevelSelectionStatementDB = new IssueLevelSelectionStatementDB(this);
        improvementPlanIRModel = new ImprovementPlanIR();
        setUpViews();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_complaint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_complaint_done:
                validation();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpViews() {
        setSupportActionBar(mImprovementToolBar);
        mscrollView.smoothScrollTo(0, 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_improvement_plans);
        categoryName = (TextView) categoryLayout.findViewById(R.id.spinner_title);
        improvementName = (TextView) improvementLayout.findViewById(R.id.spinner_title);
        categorgySpinner = (Spinner) categoryLayout.findViewById(R.id.issue_spinner);
        improvementSpinner = (Spinner) improvementLayout.findViewById(R.id.issue_spinner);
        mTargetDate = (TextView) mcalenderView.findViewById(R.id.edittext_target_date);
        mCalenderLayout = (LinearLayout) mcalenderView.findViewById(R.id.calenderLayout);
        remarksTxt = (EditText) remarksView.findViewById(R.id.editTextTypeRemark);
        categoryName.setText(resources.getString(R.string.category_str));
        improvementName.setText(TableNameAndColumnStatement.IMPROVEMENT_PLAN);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mBarrackId = extras.getInt(TableNameAndColumnStatement.BARRACK_ID);
            barracknameTxt.setText(extras.getString(TableNameAndColumnStatement.BARRACK_NAME));
            taskId = extras.getInt(TableNameAndColumnStatement.SOURCE_TASK_ID,0);
        }


        // default values for the input request model
        improvementPlanIRModel.setActionPlanStatusId(1); // pending staus
        improvementPlanIRModel.setActionPlanId(0);
        improvementPlanIRModel.setTaskId(taskId);
        populateLookUpSpinner(BARRACK_LOOKUP); // 11 is for barrack in lookuptable
        improvementPlanIRModel.setBarrackId(mBarrackId);
    }



    private void populateLookUpSpinner(final int lookupCode) {
        final ArrayList<LookupModel> lookupModelArrayList = new ArrayList<>();
        LookupModel lookupModel = new LookupModel();
        lookupModel.setName(resources.getString(R.string.select_category));
        lookupModelArrayList.add(lookupModel);
        lookupModelArrayList.addAll(issueLevelSelectionStatementDB.getImprovementLookupData(TableNameAndColumnStatement.lookup_improvementPlanCategory, lookupCode));
        if (mLookUpSpinnerAdapter == null) {
            mLookUpSpinnerAdapter = new LookUpSpinnerAdapter(this);
            mLookUpSpinnerAdapter.setSpinnerData(lookupModelArrayList);
            categorgySpinner.setAdapter(mLookUpSpinnerAdapter);
        } else {
            mLookUpSpinnerAdapter.setSpinnerData(lookupModelArrayList);
            categorgySpinner.setAdapter(mLookUpSpinnerAdapter);
        }

        categorgySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    int sub_category_id = lookupModelArrayList.get(position).getLookupIdentifier();
                    improvementPlanIRModel.setImprovementPlanCategoryId(sub_category_id);
                    populateImprovementPlanSpinner(lookupCode, sub_category_id);
                } else {
                    populateImprovementPlanSpinner(-1, -1); /// invalidate
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }



    private void populateImprovementPlanSpinner(int issuecategoryId, int categorySubId) {
        improvementActionPlanList.clear();
        if (!((-1 == issuecategoryId) && (-1 == categorySubId))) {
                improvementActionPlanList.addAll(issueLevelSelectionStatementDB.getBarrackImprovementActionPlan(IMPROVEMENTPLAN_ISSUE_TYPEID, issuecategoryId, categorySubId));

        } else {
            // display only select improvement plan in the list until the category the selected
        }

        if (actionPlanAdapter == null) {
            actionPlanAdapter = new ActionPlanAdapter(this);
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            improvementSpinner.setAdapter(actionPlanAdapter);
        } else {
            actionPlanAdapter.setSpinnerData(improvementActionPlanList);
            improvementSpinner.setAdapter(actionPlanAdapter);
        }

        improvementSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (0 != position) {
                    mcalenderView.setVisibility(View.VISIBLE);
                    improvementPlanIRModel.setMasterActionPlanId(improvementActionPlanList.get(position).getId());
                    improvementPlanIRModel.setIssueMatrixId(improvementActionPlanList.get(position).getIssueMatrixId());
                    displayActionDate(improvementActionPlanList.get(position).getTurnAroundTime());
                } else {
                    mcalenderView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void displayActionDate(int days) {

        targetdateStr = dateTimeFormatConversionUtil.addDaysToCurrentDate(days);
        mTargetDate.setText(targetdateStr);
        mCalenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });

    }

    public void showCalendar() {
        Date mindate = dateTimeFormatConversionUtil.convertStringToDateFormat(dateTimeFormatConversionUtil.getCurrentDate());
        Date maxDate = dateTimeFormatConversionUtil.convertStringToDateFormat(targetdateStr);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        DateRangeCalender selectDateFragment = DateRangeCalender.newInstance(mindate, maxDate);
        selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
        selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
            @Override
            public void changeTime(String updateData) {
                String updateDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                mTargetDate.setText(updateDate);
                //issuesMO.setTargetActionEndDate(updateDate);
            }
        });
    }


    private void validation() {
        if (!TextUtils.isEmpty(budgetEdtTxt.getText().toString())) {
            improvementPlanIRModel.setBudget(budgetEdtTxt.getText().toString().trim());
        } else {
            improvementPlanIRModel.setBudget(null);
        }
        if (mBarrackId == -1) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.client_handshake_validation));
            return;
        } else if (null == mTargetDate || 0 == mTargetDate.getText().toString().trim().length()) {
            Util.customSnackBar(mCalenderLayout, this, resources.getString(R.string.client_handshake_validation));
            return;
        } else {
            improvementPlanIRModel.setExpectedClosureDate(mTargetDate.getText().toString().trim() + " 00:00:00");
            improvementPlanIRModel.setRaisedDate(dateTimeFormatConversionUtil.getCurrentDateTime());
            improvementPlanIRModel.setRemarks(remarksTxt.getText().toString().trim());
            IOPSApplication.getInstance().getIssueInsertionDBInstance().addImprovementPlanDetails(improvementPlanIRModel);
            finish();
        }
    }

}
