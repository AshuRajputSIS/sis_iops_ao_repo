package com.sisindia.ai.android.network.response;

import com.google.gson.annotations.SerializedName;

public class GeoLocation {

    @SerializedName("lattitude")
    private Float lattitude;
    @SerializedName("longitude")
    private Float longitude;

    /**
     * @return The lattitude
     */
    public Float getLattitude() {
        return lattitude;
    }

    /**
     * @param lattitude The lattitude
     */
    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    /**
     * @return The longitude
     */
    public Float getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

}