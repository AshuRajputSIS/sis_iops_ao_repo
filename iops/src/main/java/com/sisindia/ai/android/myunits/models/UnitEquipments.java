package com.sisindia.ai.android.myunits.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by shankar on 22/6/16.
 */

public class UnitEquipments implements Serializable {
    @SerializedName("Id")
    private int Id;
    @SerializedName("UnitId")
    private int UnitId;
    @SerializedName("EquipmentId")
    private int EquipmentId;
    @SerializedName("IsCustomerProvided")
    private boolean IsCustomerProvided;
    @SerializedName("Status")
    private int Status;
    @SerializedName("EquipmentUniqueNo")
    private String EquipmentUniqueNo;
    @SerializedName("EquipmentManufacturer")
    private  String EquipmentManufacturer;
    @SerializedName("PostId")
    private int PostId;
    @SerializedName("Quantity")
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUnitId() {
        return UnitId;
    }

    public void setUnitId(int unitId) {
        UnitId = unitId;
    }

    public int getEquipmentId() {
        return EquipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        EquipmentId = equipmentId;
    }

    public boolean isCustomerProvided() {
        return IsCustomerProvided;
    }

    public void setCustomerProvided(boolean customerProvided) {
        IsCustomerProvided = customerProvided;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getEquipmentUniqueNo() {
        return EquipmentUniqueNo;
    }

    public void setEquipmentUniqueNo(String equipmentUniqueNo) {
        EquipmentUniqueNo = equipmentUniqueNo;
    }

    public String getEquipmentManufacturer() {
        return EquipmentManufacturer;
    }

    public void setEquipmentManufacturer(String equipmentManufacturer) {
        EquipmentManufacturer = equipmentManufacturer;
    }

    public int getPostId() {
        return PostId;
    }

    public void setPostId(int postId) {
        PostId = postId;
    }
}
