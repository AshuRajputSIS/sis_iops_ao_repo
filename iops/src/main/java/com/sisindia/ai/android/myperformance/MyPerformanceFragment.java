package com.sisindia.ai.android.myperformance;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.appuser.AppUserData;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.DateTimeUpdateListener;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.SelectDateFragment;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.database.AppPreferences;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.rota.DateRotaModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontButton;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Shruti on 11-07-2016.
 */
public class MyPerformanceFragment extends BaseFragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int TOTAL_PERCENTAGE = 100;

    @Bind(R.id.iv_user_photo)
    CircleImageView aiProfilePicture;
    @Bind(R.id.parent_layout)
    LinearLayout parent_layout;
    @Bind(R.id.rv_report_view)
    RecyclerView reportView;
    @Bind(R.id.tv_user_name)
    CustomFontTextview tv_user_name;
    @Bind(R.id.tv_current_date_time)
    CustomFontTextview tv_current_date_time;
    @Bind(R.id.btnDay)
    CustomFontButton btnDay;
    @Bind(R.id.btnMonth)
    CustomFontButton btnMonth;
    @Bind(R.id.layout_calendar)
    LinearLayout layout_calendar;
    @Bind(R.id.tv_inside_datepicker_btn)
    CustomFontTextview tv_inside_datepicker_btn;
    @Bind(R.id.report_layout)
    LinearLayout reportLinearLayout;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SelectDateFragment selectDateFragment;
    private int TOTAL_ROTA_COMPLIANCE = 100;
    private int TOTAL_WORK_TIME_PER_DAY_MINUTES = 24 * 60;
    private int totalTimeInMins;

    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private String mParam1;
    private Context mContext;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private String selectedDate;
    private int performanceRating = 0;
    private int approvedTasks = 0, nonApprovedTasks = 0;
    private List<Object> combineList;
    private AppPreferences appPreferences = null;
    private int totalTasks;

    public MyPerformanceFragment() {
        // Required empty public constructor
    }

    public static MyPerformanceFragment newInstance(String param1, String param2) {
        MyPerformanceFragment fragment = new MyPerformanceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myperformance, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
        setUpUI();
        setUpUserProfilePicture();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_myperformance;
    }

    private void setUpUI() {

        String first = "Hi ";
        String next = "<font color='#EE0000'>" + IOPSApplication.getInstance().getSelectionStatementDBInstance().getAppUserName() + "</font>";
        String end = ", here is your performance report";
        tv_user_name.setText(Html.fromHtml(first + next + end));

//        DateFormat df = new SimpleDateFormat("EEEE, dd MMM yyyy, HH:mm a");
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        tv_current_date_time.setText(date);
        tv_inside_datepicker_btn.setText(date);
        layout_calendar.setOnClickListener(this);

        btnDay.setOnClickListener(this);
        btnMonth.setOnClickListener(this);

        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();

        getPerformanceApiCall(dateTimeFormatConversionUtil.getCurrentDate(), false);
//        updateMyPerformanceUI(dateTimeFormatConversionUtil.getCurrentDate(), 1);

    }

    private void setUpUserProfilePicture() {
        try {
            if (appPreferences == null)
                appPreferences = new AppPreferences(getActivity());

            AppUserData appUserData = IOPSApplication.getInstance().getSettingsLevelSelectionStatmentDB().getAIPhotoWithAddress(appPreferences.getAppUserId());
            if (appUserData != null) {
                String aiCapturedPicture = appUserData.getmImageUrl();
                if (null != aiCapturedPicture && !TextUtils.isEmpty(aiCapturedPicture)) {
                    if (!(aiCapturedPicture.contains("http"))) {
                        aiProfilePicture.setImageURI(Uri.parse(aiCapturedPicture));
                    } else {
                        Glide.with(getActivity())
                                .load(aiCapturedPicture)
                                .placeholder(R.drawable.default_icon_ai)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(aiProfilePicture);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void updateMyPerformanceUI(String date, int reportType) {

        try {
            reportView.setLayoutManager(new LinearLayoutManager(mContext));
            combineList = new ArrayList<>();
            combineList.addAll(generateReportValues(date));

            //-----------------SETTING VALUES IN HEADER and ACTIVITIES----------------//
            List<TimeLineModel> timeLineModel = IOPSApplication.getInstance().getMyPerformanceStatementsDB().getActivityTimeline(date);
            if (timeLineModel != null && timeLineModel.size() > 0) {
                combineList.add(getResources().getString(R.string.activities_header));
                combineList.addAll(timeLineModel);
            }
            reportView.setAdapter(new ReportViewAdapter(combineList, mContext));
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private List<ReportViewModel> generateReportValues(String date) {

        //-----------------SETTING VALUES IN HEADER PERFORMANCE----------------//
        ReportViewModel reportViewModelPerformance = new ReportViewModel();
        reportViewModelPerformance.setReportHeading("PERFORMANCE (Month Till Date)");
        reportViewModelPerformance.setProgressMaxValue(10);
        reportViewModelPerformance.setProgressValue(performanceRating);
        reportViewModelPerformance.setReportValue(String.valueOf(performanceRating));
        reportViewModelPerformance.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prog_performance));

        //-----------------SETTING VALUES IN HEADER WORK TIME----------------//
        ReportViewModel reportViewModelWorkTime = new ReportViewModel();
        reportViewModelWorkTime.setReportHeading("WORK TIME");
//        totalTimeInMins = convertToMinutes(totalTimeInHours);
        totalTimeInMins = IOPSApplication.getInstance().getMyPerformanceStatementsDB().getPerformanceWorkTime(date);

        if (totalTimeInMins <= 24 * 60) {
            reportViewModelWorkTime.setProgressMaxValue(TOTAL_PERCENTAGE);
            if (totalTimeInMins != 0)
                reportViewModelWorkTime.setProgressValue((totalTimeInMins * 100) / TOTAL_WORK_TIME_PER_DAY_MINUTES);
            else
                reportViewModelWorkTime.setProgressValue(0);
        } else if (totalTimeInMins > 24 * 60 && totalTimeInMins <= 100 * 60) {
            reportViewModelWorkTime.setProgressMaxValue(TOTAL_PERCENTAGE);
            reportViewModelWorkTime.setProgressValue(totalTimeInMins / 60);
        } else if (totalTimeInMins > 100 * 60 && totalTimeInMins <= 1000 * 60) {
            reportViewModelWorkTime.setProgressMaxValue(TOTAL_PERCENTAGE);
            reportViewModelWorkTime.setProgressValue(totalTimeInMins / (10 * 60));
        } else if (totalTimeInMins > 1000 * 60) {
            reportViewModelWorkTime.setProgressMaxValue(TOTAL_PERCENTAGE);
            reportViewModelWorkTime.setProgressValue(totalTimeInMins / (100 * 60));
        }
        if (totalTimeInMins != 0) {
//            String totalTimeInMinsStirng = convertToHrMinutes(totalTimeInHours);
            reportViewModelWorkTime.setReportValue(convertMinutesToHrMinFormat(totalTimeInMins));
        } else
            reportViewModelWorkTime.setReportValue("0Hr 0Min");

        reportViewModelWorkTime.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prog_work));

        //-----------------SETTING VALUES IN HEADER TASKS----------------//
//        DateRotaModel dateRotaModel = IOPSApplication.getInstance().getMyPerformanceStatementsDB().getPerformanceTaskAndComplianceCount(date);
        DateRotaModel dateRotaModel = IOPSApplication.getInstance().getMyPerformanceStatementsDB().getPerformanceTaskCount(date);
        ReportViewModel reportViewModelTasks = new ReportViewModel();
        try {
            reportViewModelTasks.setReportHeading("TASKS");
            reportViewModelTasks.setProgressMaxValue(dateRotaModel.getRotaTotalCount());
            try {
                reportViewModelTasks.setProgressValue((dateRotaModel.getRotaSyncedCount() / dateRotaModel.getRotaCompletedCount()) * 100);
            } catch (Exception e) {
            }
            String reportData = dateRotaModel.getRotaCompletedCount() + " Completed / " + dateRotaModel.getRotaSyncedCount() + " Synced (Out of total "
                    + dateRotaModel.getRotaTotalCount() + ")";
            reportViewModelTasks.setReportValue(reportData);
            reportViewModelTasks.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prog_assignment));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //-----------------SETTING VALUES IN HEADER TASK COMPLIANCE----------------//
        float rotaCompliancePercentage = 0;
        try {
            if (null != dateRotaModel && dateRotaModel.getRotaTotalCount() != 0) {
                rotaCompliancePercentage = ((float) dateRotaModel.getRotaCompletedCount() / (float) dateRotaModel.getRotaTotalCount()) * 100;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ReportViewModel reportViewModelRotaCompliance = new ReportViewModel();
        reportViewModelRotaCompliance.setReportHeading("TASK COMPLIANCE");
        reportViewModelRotaCompliance.setProgressMaxValue(TOTAL_ROTA_COMPLIANCE);
        reportViewModelRotaCompliance.setProgressValue((int) rotaCompliancePercentage);
        if (rotaCompliancePercentage != 0)
            reportViewModelRotaCompliance.setReportValue(new DecimalFormat("#.##").format(rotaCompliancePercentage) + "%");
        else
            reportViewModelRotaCompliance.setReportValue(" 0% ");
        reportViewModelRotaCompliance.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prog_rota));
        reportViewModelRotaCompliance.setRotaComplaince(true);
        reportViewModelRotaCompliance.setApprovedCount(approvedTasks);
        reportViewModelRotaCompliance.setNonApprovedCount(nonApprovedTasks);

        //-----------------SETTING VALUES IN HEADER UNIT COMPLIANCE----------------//
        ReportViewModel reportUnitCompliance = new ReportViewModel();
        reportUnitCompliance.setReportHeading("UNIT COMPLIANCE (Month Till Date)");
//        reportViewModelPerformance.setProgressMaxValue(10);
//        reportViewModelPerformance.setProgressValue(performanceRating);
//        reportViewModelPerformance.setReportValue(String.valueOf(performanceRating));
        reportUnitCompliance.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_prog_performance));

        //-----------------ADDING OR COMBINING ALL ABOVE CREATED MODEL VALUES IN ARRAY LIST----------------//
        List<ReportViewModel> reportViewModelList = new ArrayList<>();
        reportViewModelList.add(reportViewModelPerformance);
        //reportViewModelList.add(reportViewModelConveyance);
        reportViewModelList.add(reportViewModelWorkTime);
        reportViewModelList.add(reportViewModelTasks);
        reportViewModelList.add(reportViewModelRotaCompliance);
        reportViewModelList.add(reportUnitCompliance);

        return reportViewModelList;
    }

    private String convertMinutesToHrMinFormat(int minutes) {
        int hours = minutes / 60; //since both are ints, you get an int
        int min = minutes % 60;
        return hours + "Hr" + min + "Min";
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.layout_calendar:
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                selectDateFragment = new SelectDateFragment(parent_layout, null);
                selectDateFragment.setPerformanceCalendar(true);
                selectDateFragment.show(fragmentTransaction, "DatepickerFragment");
                selectDateFragment.setTimeListener(new DateTimeUpdateListener() {
                    @Override
                    public void changeTime(String updateData) {
                        selectedDate = dateTimeFormatConversionUtil.convertDayMonthDateYearToDateFormat(updateData);
                        tv_inside_datepicker_btn.setText(selectedDate);
                        tv_current_date_time.setText(updateData);
                        getPerformanceApiCall(selectedDate, false);
//                        updateMyPerformanceUI(selectedDate, 3);

                    }
                });
                break;
            case R.id.btnDay:
                selectedDate = null;
                String todayDate = dateTimeFormatConversionUtil.getCurrentDate();
                tv_current_date_time.setText(todayDate);
                getPerformanceApiCall(dateTimeFormatConversionUtil.getCurrentDate(), false);
//                updateMyPerformanceUI(todayDate, 1);
                break;

            // Note: button month is now changed to : yesterday date button as per new requirement
            case R.id.btnMonth:
                selectedDate = null;
//                getPerformanceApiCall(null, 2, true);
//                getPerformanceApiCall(getYesterdayDate(dateTimeFormatConversionUtil.getCurrentDate()), 1, false);

                String yesterdayDate = getYesterdayDate(dateTimeFormatConversionUtil.getCurrentDate());
                tv_current_date_time.setText(yesterdayDate);

                getPerformanceApiCall(yesterdayDate, false);

//                updateMyPerformanceUI(yesterdayDate, 3);
                break;
        }
    }

    public String getYesterdayDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(date));
            c.add(Calendar.DATE, -1);
            date = sdf.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    private void getPerformanceApiCall(final String date, boolean isMonthly) {

        SISClient sisClient = new SISClient(getActivity());

        showprogressbar();
        sisClient.getApi().getMyPerformanceInfo(isMonthly, date, new Callback<MyPerformanceBase>() {
            @Override
            public void success(MyPerformanceBase myPerformanceBase, Response response) {
                hideprogressbar();

                if (myPerformanceBase.getStatusCode() == 200 && myPerformanceBase.getData() != null) {

                    approvedTasks = myPerformanceBase.getData().getAdhocTasksApproved();
                    totalTasks = myPerformanceBase.getData().getAdhocTasksCreated();
                    if (totalTasks != 0 && totalTasks > approvedTasks) {
                        nonApprovedTasks = totalTasks - approvedTasks;
                    }

                } else {
                    snackBarWithMesg(parent_layout, getResources().getString(R.string.API_FAIL_MSG));
                }

                updateMyPerformanceUI(date, 1);

            }

            @Override
            public void failure(RetrofitError error) {
                hideprogressbar();
                updateMyPerformanceUI(date, 1);
            }
        });
    }

    private void showprogressbar() {
        Util.showProgressBar(getActivity(), R.string.loading_please_wait);

    }

    private void hideprogressbar() {
        Util.dismissProgressBar(isVisible());
    }

}
