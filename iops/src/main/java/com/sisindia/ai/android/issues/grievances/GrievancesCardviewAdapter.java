package com.sisindia.ai.android.issues.grievances;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.issues.IssuesMO;
import com.sisindia.ai.android.issues.models.GrievanceCount;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Durga Prasad on 23-07-2016.
 */
public class GrievancesCardviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final  String TAG = GrievancesCardviewAdapter.class.getSimpleName();
    Context mcontext;
    ArrayList<Object> grievancesListData;
    private static final int GRIEVANCES_HEADER = 1;
    private static final int GRIEVANCES_CARDVIEW = 2;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;

    public GrievancesCardviewAdapter(Context mcontext) {
        this.mcontext = mcontext;
    }

    public void setIssuesDataList(ArrayList<Object> grievancesListData) {
        this.grievancesListData = grievancesListData;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
    }

    public void setOnRecyclerViewClicked(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder recycleViewHolder = null;
        switch (viewType) {
            case GRIEVANCES_HEADER :
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.openissues_layout, parent, false);
                recycleViewHolder =  new OpenGrievancesHeaderHolder(view);
            break;

            case GRIEVANCES_CARDVIEW :
                view = LayoutInflater.from(mcontext).inflate(R.layout.issues_grievance_cardview, null, false);
                recycleViewHolder = new GrievancesCardviewHolder(view);

            break;

        }
        return  recycleViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Object object = grievancesListData.get(position);

            if ( holder instanceof OpenGrievancesHeaderHolder && object instanceof GrievanceCount) {
                        OpenGrievancesHeaderHolder openGrievancesHeaderHolder = (OpenGrievancesHeaderHolder) holder;
                        GrievanceCount grievanceCount =  (GrievanceCount) object;
                        if(grievanceCount !=null) {
                            if(grievanceCount.getCountLessthan2Days() != null)
                            openGrievancesHeaderHolder.lessThan48HoursTv.setText(String.valueOf(grievanceCount.getCountLessthan2Days()));
                            if(grievanceCount.getCountMorethan2Days() != null)
                            openGrievancesHeaderHolder.greaterThan48HoursTV.setText(String.valueOf(grievanceCount.getCountMorethan2Days()));
                            if(grievanceCount.getCountMorethan7Days() != null)
                            openGrievancesHeaderHolder.greaterThan7Days.setText(String.valueOf(grievanceCount.getCountMorethan7Days()));
                        }
                    }

                   else if (holder instanceof  GrievancesCardviewHolder && object instanceof IssuesMO) {
                        GrievancesCardviewHolder grievancesCardviewHolder = (GrievancesCardviewHolder) holder;
                        grievancesCardviewHolder.lastInspectionUnitName.setText(((IssuesMO) grievancesListData.get(position)).getUnitName());//grievancesListData.get(position).getU
                        grievancesCardviewHolder.guardName.setText(((IssuesMO) grievancesListData.get(position)).getGuardName());
                        grievancesCardviewHolder.natureOfGrievance.setText(((IssuesMO) grievancesListData.get(position)).getNatureOfGrievance());
                        String targetClosureEndDate = ((IssuesMO) grievancesListData.get(position)).getTargetActionEndDate();
                        String createdDateTime = ((IssuesMO) grievancesListData.get(position)).getCreatedDateTime();
                        if (!targetClosureEndDate.isEmpty()) {
                            if(targetClosureEndDate.length() > 10){
                                grievancesCardviewHolder.grievancesTargetClosureDate.setText(dateTimeFormatConversionUtil.convertionDateTimeToDateMonthNameYearFormat(targetClosureEndDate));
                            }
                            else{
                                grievancesCardviewHolder.grievancesTargetClosureDate.setText(dateTimeFormatConversionUtil.convertionDateToDateMonthNameYearFormat(targetClosureEndDate));
                            }


                        } else {
                            grievancesCardviewHolder.grievancesTargetClosureDate.setText(mcontext.getResources().getString(R.string.NOT_AVAILABLE));
                        }

                        if (createdDateTime != null && !createdDateTime.isEmpty()) {
                            long millis = dateTimeFormatConversionUtil.convertStringToDate(createdDateTime).getTime();
                            grievancesCardviewHolder.timeDurationLeft.setReferenceTime(millis);
                        } else {
                            grievancesCardviewHolder.timeDurationLeft.setText(mcontext.getResources().getString(R.string.NOT_AVAILABLE));
                        }

                        String assignedName = ((IssuesMO) grievancesListData.get(position)).getAssignedToName();
                       if (assignedName != null && assignedName.length() > 0){
                           grievancesCardviewHolder.lastInspectionAssignedTo.setText(assignedName);//we have to replace the dynamcic data
                       }
                       else {
                           grievancesCardviewHolder.lastInspectionAssignedTo.setText(mcontext.getResources().getString(R.string.NOT_AVAILABLE));//we have to replace the dynamcic data
                       }

                        grievancesCardviewHolder.lastInspectionStatus.setText(((IssuesMO) grievancesListData.get(position)).getStatus());
                    }
             }







    @Override
    public int getItemCount() {
        int count = grievancesListData == null ? 0 : grievancesListData.size();
        return count;
    }

    @Override
    public int getItemViewType(int position) {

        if (grievancesListData.get(position)instanceof  GrievanceCount) {
            return GRIEVANCES_HEADER;
        } else {
            return GRIEVANCES_CARDVIEW;
        }
    }

    class GrievancesCardviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.grievanceUnitName)
        public TextView lastInspectionUnitName;
        @Bind(R.id.grievance_guardName)
        public TextView guardName;
        @Bind(R.id.grievanceTargetClosureDate)
        public TextView grievancesTargetClosureDate;
        @Bind(R.id.grievance_timeDurationLeft)
        public RelativeTimeTextView timeDurationLeft;
        @Bind(R.id.grievance_assiignedTo)
        public TextView lastInspectionAssignedTo;
        @Bind(R.id.grievance_status)
        public TextView lastInspectionStatus;
        @Bind(R.id.viewDetailsImage)
        public ImageView viewDetailsImage;
        @Bind(R.id.natureOfGrievance)
        public TextView natureOfGrievance;
        @Bind(R.id.viewDetailsTv)
        TextView viewDetails;


        public GrievancesCardviewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
            viewDetailsImage.setOnClickListener(this);
            viewDetails.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onRecyclerViewItemClickListener.onRecyclerViewItemClick(v, getLayoutPosition());
        }
    }


 class OpenGrievancesHeaderHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.lessThan48HoursTv)
        CustomFontTextview lessThan48HoursTv;
        @Bind(R.id.greaterThan48HoursTV)
        CustomFontTextview greaterThan48HoursTV;
        @Bind(R.id.greaterThan7Days)
        CustomFontTextview greaterThan7Days;

        public OpenGrievancesHeaderHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this,itemView);
        }


    }


}
