package com.sisindia.ai.android.rota.models;

import java.io.Serializable;

/**
 * Created by Durga Prasad on 19-03-2016.
 */
public class RotaTaskModel implements Serializable {

    private String address;
    private String typeChecking;
    private String startTime;
    private String endTime;
    private String startDate;
    private String endDate;
    private String time;
    private int lightTypeCheckingIcon;
    private String unitName;
    private int icon;
    private int taskId;
    private int unitId;
    private int taskTypeId;
    private int barrackId;
    private int Id;
    private String barrackName;
    private String barrackAddress;
    private String taskStatus;
    private Integer otherTaskReasonId;
    private String OtherTaskReason;
    private String unitCode;
    private int rotaWeek;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private String remarks;

    public String getBarrackAddress() {
        return barrackAddress;
    }

    public void setBarrackAddress(String barrackAddress) {
        this.barrackAddress = barrackAddress;
    }


    public String getBarrackName() {
        return barrackName;
    }

    public void setBarrackName(String barrackName) {
        this.barrackName = barrackName;
    }



    public int getTaskStatusId() {
        return taskStatusId;
    }

    public void setTaskStatusId(int taskStatusId) {
        this.taskStatusId = taskStatusId;
    }

    private int taskStatusId;



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }



    public int getBarrackId() {
        return barrackId;
    }

    public void setBarrackId(int barrackId) {
        this.barrackId = barrackId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getLightTypeCheckingIcon() {
        return lightTypeCheckingIcon;
    }

    public void setLightTypeCheckingIcon(int lightTypeCheckingIcon) {
        this.lightTypeCheckingIcon = lightTypeCheckingIcon;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeChecking() {
        return typeChecking;
    }

    public void setTypeChecking(String typeCheckking) {
        this.typeChecking = typeCheckking;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskStatus() {
        return taskStatus;
    }


    public Integer getOtherTaskReasonId() {
        return otherTaskReasonId;
    }

    public void setOtherTaskReasonId(Integer otherTaskReasonId) {
        this.otherTaskReasonId = otherTaskReasonId;
    }

    public String getOtherTaskReason() {
        return OtherTaskReason;
    }

    public void setOtherTaskReason(String otherTaskReason) {
        OtherTaskReason = otherTaskReason;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getUnitCode() {
        return unitCode;
    }


    public void setRotaWeek(int rotaWeek) {
        this.rotaWeek = rotaWeek;
    }

    public int getRotaWeek() {
        return rotaWeek;
    }
}
