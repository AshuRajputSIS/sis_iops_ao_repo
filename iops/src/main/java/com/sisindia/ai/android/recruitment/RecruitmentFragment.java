package com.sisindia.ai.android.recruitment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseFragment;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.commons.OnRecyclerViewItemClickListener;
import com.sisindia.ai.android.commons.ToolbarTitleUpdateListener;
import com.sisindia.ai.android.network.SISClient;
import com.sisindia.ai.android.network.response.ApiResponse;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.BaseRecruitmentMO;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.CurrentMonth;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.CurrentYear;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.PreviousMonth;
import com.sisindia.ai.android.recruitment.recuirtModelObjects.TotalRecruite;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.views.DividerItemDecoration;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Saruchi on 20-04-2016.
 */
public class RecruitmentFragment extends BaseFragment implements OnRecyclerViewItemClickListener, View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.recuirt_recyclerview)
    RecyclerView mRecuirtRecylerview;
    @Bind(R.id.recuirt_add_floatting_button)
    FloatingActionButton mfloatingbtn;
    private ToolbarTitleUpdateListener toolbarTitleUpdateListener;
    private Context mContext;
    private String mParam1;
    private String mParam2;
    private RecuirtRecylerAdapter adapter;
    private SwipeRefreshLayout mSwipeRefresh;
    private BaseRecruitmentMO mRecurimentModelObject;
    private Resources mResources;
    private int maxCount; // max recuritment count for the current month
    private List<TotalRecruite> recruitmentIRList;

    public RecruitmentFragment() {
        // Required empty public constructor
    }

    public static RecruitmentFragment newInstance(String param1, String param2) {
        RecruitmentFragment fragment = new RecruitmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recuirt_fragment, container, false);
        ButterKnife.bind(this, view);
        mSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recuriment);
        mResources = getResources();
        maxCount = 0;

        if (NetworkUtil.isConnected)
            makeAPICall();
        else
            updateUI();

        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtil.isConnected) {
                    mSwipeRefresh.setRefreshing(true);
                    makeAPICall();
                } else {
                    mSwipeRefresh.setRefreshing(false);
                    updateUI();
                    Toast.makeText(getActivity(), mResources.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    @Override
    protected int getLayout() {
        return R.layout.recuirt_fragment;
    }

    private void updateUI() {
        recruitmentIRList = IOPSApplication.getInstance().getRecurimentUpdateStatementDB().getRecruitmentList();
        if (recruitmentIRList != null && recruitmentIRList.size() != 0) {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            mRecuirtRecylerview.setLayoutManager(linearLayoutManager);
            mRecuirtRecylerview.setItemAnimator(new DefaultItemAnimator());
            ArrayList<Object> objectArrayList = new ArrayList<>();
            if (null == mRecurimentModelObject) {
                mRecurimentModelObject = new BaseRecruitmentMO();
                CurrentMonth currentMonth = new CurrentMonth();
                currentMonth.setRecommended(0);
                currentMonth.setRejected(0);
                currentMonth.setSelected(0);
                currentMonth.setInprocess(0);
                currentMonth.setMonth(0);
                currentMonth.setYear(0);

                PreviousMonth previousMonth = new PreviousMonth();
                previousMonth.setRecommended(0);
                previousMonth.setRejected(0);
                previousMonth.setSelected(0);
                previousMonth.setInprocess(0);
                previousMonth.setMonth(0);
                previousMonth.setYear(0);

                CurrentYear currentYear = new CurrentYear();
                currentYear.setRecommended(0);
                currentYear.setRejected(0);
                currentYear.setSelected(0);
                currentYear.setInprocess(0);
                currentYear.setMonth(0);
                currentYear.setYear(0);
                mRecurimentModelObject.setCurrentMonth(currentMonth);
                mRecurimentModelObject.setCurrentYear(currentYear);
                mRecurimentModelObject.setPreviousMonth(previousMonth);
                objectArrayList.add(0, mRecurimentModelObject);
            } else {
                objectArrayList.add(0, mRecurimentModelObject);
            }
            objectArrayList.addAll(recruitmentIRList);
            adapter = new RecuirtRecylerAdapter(getActivity());
            adapter.setObjectArrayList(objectArrayList);
            mRecuirtRecylerview.setAdapter(adapter);
            Drawable dividerDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.divider);
            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(dividerDrawable);
            mRecuirtRecylerview.addItemDecoration(dividerItemDecoration);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarTitleUpdateListener)
            toolbarTitleUpdateListener = (ToolbarTitleUpdateListener) context;
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);//for enable the menu in fragment
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbarTitleUpdateListener.changeToolbarTitle(mParam1);
    }

    @OnClick(R.id.recuirt_add_floatting_button)
    public void addRecuirtment() {
        if (appPreferences.getDutyStatus()) {
            if (NetworkUtil.isConnected) {
                Intent i = new Intent(getActivity(), AddRecruitActivity.class);
                startActivity(i);
               /* if(recruitmentIRList.size() < maxCount ){
                    Intent i = new Intent(getActivity(), AddRecruitActivity.class);
                    startActivity(i);
                }
                else{
                    snackBarWithMessage( mResources.getString(R.string.error_recruit_add_new_validation));
                }*/
            } else {
                // add in offline
                Intent i = new Intent(getActivity(), AddRecruitActivity.class);
                startActivity(i);
            }


        } else {
            snackBarWithMessage(mResources.getString(R.string.TURN_DUTY));
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            updateUI();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void makeAPICall() {
        SISClient sisClient = new SISClient(getActivity());
        sisClient.getApi().getRecurimentData(new Callback<ApiResponse<BaseRecruitmentMO>>() {
            @Override
            public void success(ApiResponse<BaseRecruitmentMO> baseRecruitmentMOApiResponse, Response response) {
                if (null != baseRecruitmentMOApiResponse) {
                    switch (baseRecruitmentMOApiResponse.statusCode) {
                        case HttpURLConnection.HTTP_OK:
                            if (null != baseRecruitmentMOApiResponse.data) {
                                mRecurimentModelObject = baseRecruitmentMOApiResponse.data;
                                maxCount = mRecurimentModelObject.getCurrentMonth().getTotalRecruits();
                                if (mRecurimentModelObject != null) {
                                    if (mSwipeRefresh != null) {
                                        mSwipeRefresh.setRefreshing(false);
                                    }
                                    if (null != mRecurimentModelObject.getTotalRecruites() &&
                                            mRecurimentModelObject.getTotalRecruites().size() != 0) {
                                        IOPSApplication.getInstance().getRecurimentUpdateStatementDB().deleteRecruitmentTable();
                                        IOPSApplication.getInstance().getRecurimentUpdateStatementDB().addRecurimentDetails(mRecurimentModelObject.getTotalRecruites());
                                    }
                                    updateUI();
                                }
                            }
                            break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                updateUI();
            }
        });
    }
}
