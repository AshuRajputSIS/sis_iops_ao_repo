package com.sisindia.ai.android.rota.daycheck;

/**
 * Created by shankar on 18/11/16.
 */

public class DayCheckActivityList {

    public String activityName;
    public int activityId;
    public boolean activityStatus;

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public boolean isActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(boolean activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
