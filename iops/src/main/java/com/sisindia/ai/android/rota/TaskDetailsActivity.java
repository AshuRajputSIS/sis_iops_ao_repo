package com.sisindia.ai.android.rota;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.BaseActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.myperformance.TimeLineModel;
import com.sisindia.ai.android.myunits.SingleUnitActivity;
import com.sisindia.ai.android.myunits.SingletonUnitDetails;
import com.sisindia.ai.android.network.response.CommonResponse;
import com.sisindia.ai.android.onboarding.OnBoardingActivity;
import com.sisindia.ai.android.onboarding.OnBoardingFactory;
import com.sisindia.ai.android.rota.models.RotaTaskActvityCheckMO;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.rota.nightcheck.NighCheckValidation;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.NetworkUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontTextview;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TaskDetailsActivity extends BaseActivity implements View.OnClickListener {
    //    final static String EDIT_TASK_ACTIVITY = "EditTaskActivity";
    LayoutInflater layoutInflater;
    String[] LabelsWithBarrack = {"Task", "Unit Name", "Barrack Name", "Date", "Time", "Location"};
    String[] LabelsWithOutBarrack = {"Task", "Unit Name", "Date", "Time", "Location"};
    ArrayList<String> taskDetails = new ArrayList<>();
    @Bind(R.id.dayCheckBtn)
    Button dayCheckBtn;
    @Bind(R.id.unit_image)
    ImageView unitImageView;
    @Bind(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    private LinearLayout linear_task_details;
    //    private NestedScrollView nestedScrollView;
    private Toolbar toolbar;
    private LinearLayout linear_data;
    private FloatingActionButton floatingActionButton;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Context mContext;
    private ImageView taskTypeImage;
    private RotaTaskModel rotaModel;
    //    private int taskId;
//    private int taskStatusId;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private RotaTaskActvityCheckMO rotaTaskActvityCheck;
//    private SingletonUnitDetails singletonUnitDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        ButterKnife.bind(this);
        mContext = TaskDetailsActivity.this;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
//        singletonUnitDetails = SingletonUnitDetails.getInstance();

        layoutInflater = getLayoutInflater();
        linear_task_details = (LinearLayout) findViewById(R.id.linear_task_details);

//        nestedScrollView = (NestedScrollView) findViewById(R.id.nested_scroll);
        taskTypeImage = (ImageView) findViewById(R.id.taskTypeImage);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        linear_data = (LinearLayout) findViewById(R.id.linear_data);
        linear_data.getChildCount();
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        rotaModel = (RotaTaskModel) SingletonUnitDetails.getInstance().getUnitDetails();
        if (rotaModel == null) {
            rotaModel = appPreferences.getRotaTaskJsonData();
        }
        loadUnitImage();

        if (rotaModel != null) {
            taskDetails.add(rotaModel.getTypeChecking());

            if (null != rotaModel.getUnitName() && !rotaModel.getUnitName().isEmpty())
                taskDetails.add(rotaModel.getUnitName());
            else
                taskDetails.add(getResources().getString(R.string.NOT_AVAILABLE));

            if (rotaModel.getTaskTypeId() == 3) {
                if (null != rotaModel.getBarrackName() && !rotaModel.getBarrackName().isEmpty())
                    taskDetails.add(rotaModel.getBarrackName());
                else
                    taskDetails.add(getResources().getString(R.string.NOT_AVAILABLE));
            }

            taskDetails.add(dateTimeFormatConversionUtil.convertDateToDayMonthDateYearFormat(rotaModel.getStartDate()));
            String startTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(rotaModel.getStartTime());
            String endTime = dateTimeFormatConversionUtil.changeFormatOfampmToAMPM(rotaModel.getEndTime());
            taskDetails.add(startTime + " - " + endTime);

            if (rotaModel.getTaskTypeId() == 3) {
                if (null != rotaModel.getBarrackAddress() && !rotaModel.getBarrackAddress().isEmpty()) {
                    taskDetails.add(rotaModel.getBarrackAddress());
                } else {
                    taskDetails.add(getResources().getString(R.string.NOT_AVAILABLE));
                }
            } else {
                if (null != rotaModel.getAddress() && !rotaModel.getAddress().isEmpty()) {
                    taskDetails.add(rotaModel.getAddress());
                } else {
                    taskDetails.add(getResources().getString(R.string.NOT_AVAILABLE));
                }
            }

//            taskId = rotaModel.getTaskId();
        }
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        getRotaTaskStatus();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (appPreferences.getDutyStatus()) {
                    if (rotaTaskActvityCheck.getTaskStatusId() == 0) {
                        snackBarWithDuration(coordinatorLayout, getResources().getString(R.string.TASK_STATUS_ID_ZERO), getResources().getInteger(R.integer.number_4));
                    } else if (rotaTaskActvityCheck.getTaskStatusId() == 1) {
                        Intent editTaskActivity = new Intent(TaskDetailsActivity.this, EditTaskActivity.class);
                        editTaskActivity.putExtra(Constants.NAVIGATION_FROM, TAG);
                        editTaskActivity.putExtra(SingleUnitActivity.CHECKING_TYPE, rotaModel);
                        startActivity(editTaskActivity);

                    } else {
                        snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.TASK_NOT_EDITABLE_MSG));

                    }
                } else {
                    Util.showToast(TaskDetailsActivity.this, getResources().getString(R.string.DUTY_ON_MSG));
                }
            }
        });

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        if (rotaModel != null) {
            taskTypeImage.setImageResource(rotaModel.getLightTypeCheckingIcon());
            if (null != rotaModel.getUnitName() && !rotaModel.getUnitName().isEmpty()) {
                collapsingToolbarLayout.setTitle(rotaModel.getUnitName());
            } else {
                collapsingToolbarLayout.setTitle(rotaModel.getBarrackName());
            }

            dayCheckBtn.setText("START " + rotaModel.getTypeChecking().toUpperCase());
        } else {
            dayCheckBtn.setText(getResources().getString(R.string.NOT_AVAILABLE));
        }

        setScrollviewChilds();
        dayCheckBtn.setOnClickListener(this);
    }

    private void getRotaTaskStatus() {
        rotaTaskActvityCheck = new RotaTaskActvityCheckMO();
        if (rotaModel != null) {
            rotaTaskActvityCheck = IOPSApplication.getInstance().getSelectionStatementDBInstance().checkActivityStatus(rotaModel.getId());
        }
    }

    private void setScrollviewChilds() {

        if (rotaModel.getTaskTypeId() == 3) {
            for (int index = 0; index < LabelsWithBarrack.length; index++) {
                View view = layoutInflater.inflate(R.layout.task_details_layout, linear_task_details, false);
                TextView labelsTxt = (CustomFontTextview) view.findViewById(R.id.details1);
                TextView valuesTxt = (CustomFontTextview) view.findViewById(R.id.details2);
                labelsTxt.setText(LabelsWithBarrack[index]);
                valuesTxt.setText(taskDetails.get(index));
                linear_data.addView(view);
            }
        } else {
            for (int index = 0; index < LabelsWithOutBarrack.length; index++) {
                View view = layoutInflater.inflate(R.layout.task_details_layout, linear_task_details, false);
                TextView labelsTxt = (CustomFontTextview) view.findViewById(R.id.details1);
                TextView valuesTxt = (CustomFontTextview) view.findViewById(R.id.details2);
                labelsTxt.setText(LabelsWithOutBarrack[index]);
                valuesTxt.setText(taskDetails.get(index));
                linear_data.addView(view);
            }
        }
    }

    @Override
    public void onClick(View v) {
//        int dateFlag = dateTimeFormatConversionUtil.dateComparison(dateTimeFormatConversionUtil.getCurrentDate().toString(), rotaModel.getStartDate());
        getRotaTaskStatus();
        if (null != rotaTaskActvityCheck) {

            switch (v.getId()) {
                case R.id.dayCheckBtn:

                    if (appPreferences.getDutyStatus()) {

                        switch (rotaTaskActvityCheck.getTaskStatusId()) {
                            case 1:
                                startTaskValidation();
                                break;
                            case 2:
                                startTaskValidation();
                                break;
                            case 3:
                                snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.TASK_COMPLETED_AND_SYNC_MSG));
                                break;
                            case 4:
                                snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.TASK_COMPLETED_YET_TO_SYNC_MSG));
                                break;
                            case 5:
                                startTaskValidation();
                                break;
                        }
                    } else {
                        Toast.makeText(TaskDetailsActivity.this, getResources().getString(R.string.DUTY_ON_MSG), Toast.LENGTH_SHORT).show();
                    }
            }
        }
    }

    private void startTaskValidation() {

        if (rotaModel.getTaskTypeId() == RotaTaskTypeEnum.Night_Checking.getValue()) {
            if (rotaModel.getRotaWeek() == dateTimeFormatConversionUtil.getWeekOfTheYear(null)) {
                try {
                    NighCheckValidation nighCheckValidation = dateTimeFormatConversionUtil.nightCheckValidationBeforeStart(rotaModel.getStartDate(),
                            rotaModel.getEndDate(), rotaModel.getStartTime(), rotaModel.getEndTime());
                    if (nighCheckValidation.isBeforeMinTime || nighCheckValidation.isAftermaxTime) {
                        if (nighCheckValidation.isBeforeMinTime) {
                            snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.night_check_validation_starttime));
                        } else if (nighCheckValidation.isAftermaxTime) {
                            snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.night_check_validation_endtime));
                        }
                    } else {
                        startActivityAfterValidating();
                    }
//                    startActivityAfterValidating();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.PAST_FUTURE_BLOCK_MSG));
            }
        } else if (rotaModel.getTaskTypeId() == RotaTaskTypeEnum.Bill_Submission.getValue() ||
                (rotaModel.getTaskTypeId() == RotaTaskTypeEnum.Others.getValue() && rotaModel.getOtherTaskReasonId() == 10)) {

            int dateComparisonResult = dateTimeFormatConversionUtil.currentDateVs25thOfCurrentMonthDate(dateTimeFormatConversionUtil.getCurrentDate());
//            int dateComparisonResult = dateTimeFormatConversionUtil.currentDateVs25thOfCurrentMonthDate(rotaModel.getStartDate());
            // IF CURRENT DATE < 25th DATE OF CURRENT MONTH = -1
            // IF CURRENT DATE > 25th DATE OF CURRENT MONTH = 1
            // IF CURRENT DATE = 25th DATE OF CURRENT MONTH = 0

            if (dateComparisonResult < 0) {

                /*int selectedDateVsPreviousMonth24 = dateTimeFormatConversionUtil.dateComparison(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(rotaModel.getStartDate(),-1, "24"));
                int selectedDateVsCurrentMonth25 = dateTimeFormatConversionUtil.dateComparison(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(rotaModel.getStartDate(),"25"));*/

                int selectedDateVsPreviousMonth25 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(dateTimeFormatConversionUtil.getCurrentDate(), -1, "25"));
                int selectedDateVsCurrentMonth25 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(dateTimeFormatConversionUtil.getCurrentDate(), "25"));

                if (selectedDateVsPreviousMonth25 >= 0 && selectedDateVsCurrentMonth25 < 0) {
                    startActivityAfterValidating();
                } else {
                    snackBarWithMesg(coordinatorLayout, "You cannot start the activity");
                }

            } else if (dateComparisonResult >= 0) {

                /*int selectedDateVsCurrentMonth24 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(rotaModel.getStartDate(),"24"));
                int selectedDateVsNextMonth25 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(rotaModel.getStartDate(),1, "25"));*/

                int selectedDateVsCurrentMonth25 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getCurrentMonthDateFromDayWRTSelectedDate(dateTimeFormatConversionUtil.getCurrentDate(), "25"));
                int selectedDateVsNextMonth25 = dateTimeFormatConversionUtil.yymmddDateComparator(rotaModel.getStartDate(),
                        dateTimeFormatConversionUtil.getPreOrNextMonth24Or25DateWRTSelectedDate(rotaModel.getStartDate(), 1, "25"));

                if (selectedDateVsCurrentMonth25 >= 0 && selectedDateVsNextMonth25 < 0) {
                    startActivityAfterValidating();
                } else {
                    snackBarWithMesg(coordinatorLayout, "You cannot start the activity of previous month");
                }
            }

        } else {
            if (rotaModel.getRotaWeek() == dateTimeFormatConversionUtil.getWeekOfTheYear(null)) {

                //ADDING VALIDATION TO CHECK (DC) IF AI IS ALREADY DONE TASK ON SAME UNIT ON SAME DAY
                /*if (rotaModel.getTaskTypeId() == RotaTaskTypeEnum.Day_Checking.getValue()) {
                    if (IOPSApplication.getInstance().getSelectionStatementDBInstance().isSameUnitTaskAlreadyDone(rotaModel.getUnitId()))
                        snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.sameUnitTwiceTaskMsg));
                    else
                        startActivityAfterValidating();
                } else
                    startActivityAfterValidating();*/

                startActivityAfterValidating();
            } else {
                snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.PAST_FUTURE_BLOCK_MSG));
            }
        }
    }

    private void startActivityAfterValidating() {
        //updateRotaTaskIdSingleTon() : [ updating Rota task id ]
        rotaModel.setTaskId(rotaTaskActvityCheck.getTaskId());

        myPerformanceActivityTracking(rotaModel, rotaTaskActvityCheck.getTaskId());
        updatingTaskStatus(rotaTaskActvityCheck.getTaskId());
        startIntent();
    }

   /* private void updateRotaTaskIdSingleTon() {
        rotaModel.setTaskId(rotaTaskActvityCheck.getTaskId());
    }*/

    private void updatingTaskStatus(int rotaTaskId) {
        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().
                updateAcutualStartDateTime(rotaTaskId, dateTimeFormatConversionUtil.getCurrentDateTime().toString(),
                        RotaTaskStatusEnum.InProgress.getValue());
        if (NetworkUtil.isConnected) {
            startTaskApiCall(rotaTaskId);
        }
    }

    private void startIntent() {
        /*if (!util.isNearToUnitPost(rotaModel.getUnitId())) {
            snackBarWithMesg(coordinatorLayout, getResources().getString(R.string.TASK_STARTED_VALIDATION_WITH_CURRENT_LOCATION));
        }*/
        Intent typeCheckingIntent = null;
        switch (rotaModel.getTaskTypeId()) {
            case 1:
                dayNightLastInspection(OnBoardingFactory.DAY_CHECK, 19);
                break;
            case 2:
                dayNightLastInspection(OnBoardingFactory.NIGHT_CHECK, 13);
                break;
            case 3:
                openOnBoardingActivity(OnBoardingFactory.BARRACK_INSPECTION, 5);
                break;
            case 4:
                dayNightLastInspection(OnBoardingFactory.CLIENT_COORDINATION, 4);
                break;
            case 5:
                openOnBoardingActivity(OnBoardingFactory.BILL_SUBMISSION, 4);
                break;
            case 6:
                openOnBoardingActivity(OnBoardingFactory.BILL_COLLECTION, 3);
                break;
            case 7:
                typeCheckingIntent = new Intent(mContext, OthersActivity.class);
                typeCheckingIntent.putExtra("RotaModel", rotaModel);
                startActivity(typeCheckingIntent);
                break;
        }
    }

    private void openOnBoardingActivity(String moduleName, int noOfImages) {
        Intent intent = new Intent(mContext, OnBoardingActivity.class);
        intent.putExtra(OnBoardingFactory.MODULE_NAME_KEY, moduleName);
        intent.putExtra(OnBoardingFactory.NO_OF_IMAGES, noOfImages);
        startActivity(intent);
    }

    private void myPerformanceActivityTracking(RotaTaskModel rotaModel, int rotaTaskId) {
        TimeLineModel timeLineModel = new TimeLineModel();
        timeLineModel.setActivity("Check in");

        timeLineModel.setTaskId(String.valueOf(rotaTaskId));
        timeLineModel.setTaskName(RotaTaskTypeEnum.valueOf(rotaModel.getTaskTypeId()).name().replace("_", " "));
        if (rotaModel.getTaskTypeId() == 3) {
            timeLineModel.setLocation(rotaModel.getBarrackName());
        } else {
            timeLineModel.setLocation(rotaModel.getUnitName());
        }
        timeLineModel.setStatus(RotaTaskStatusEnum.valueOf(rotaModel.getTaskStatusId()).name());
        timeLineModel.setDate(dateTimeFormatConversionUtil.getCurrentDate());
        timeLineModel.setEndTime("");
        timeLineModel.setStartTime(dateTimeFormatConversionUtil.getCurrentTime());//"09:30"

        IOPSApplication.getInstance().getMyPerformanceStatementsDB().insertIntoMyPerformanceTable(timeLineModel);
        IOPSApplication.getInstance().getActivityLogDB().insertActivityLogDetails(timeLineModel);
    }

    private void dayNightLastInspection(String moduleName, int noOfImages) {
        openOnBoardingActivity(moduleName, noOfImages);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
    }

    /**
     * Loading  the Unit image . If image is coming from LoadConfig then need to download and  display
     * else
     * Local uri and if local local uri is also not there then display the default image
     * <p>
     * this image is the IsMain Gate image which the user has captured while creaitng the post for that
     * particular unit  and making the IsMain gate as true
     */
    private void loadUnitImage() {
        String imagePath = IOPSApplication.getInstance().getUnitLevelSelectionQueryInstance().fetchUnitMainGateImage(rotaModel.getUnitId());
        if (!TextUtils.isEmpty(imagePath)) {
            if (!(imagePath.contains("http"))) {
                Glide.with(mContext)
                        .load(new File(imagePath))
                        .placeholder(R.drawable.mountains)
                        .into(unitImageView);
            } else {
                Glide.with(mContext)
                        .load(imagePath)
                        .placeholder(R.drawable.mountains)
                        .into(unitImageView);
            }
        } else {
            unitImageView.setImageResource(R.drawable.mountains);
        }
    }

    private void startTaskApiCall(final int startedTaskId) {
        sisClient.getApi().startTask(String.valueOf(startedTaskId), new Callback<CommonResponse>() {
            @Override
            public void success(CommonResponse commonResponse, Response response) {
                switch (commonResponse.getStatusCode()) {
                    case 200:
                        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateIsTaskStarted(startedTaskId, RotaStartTaskEnum.OnCompleted.getValue());
                        break;
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }
}