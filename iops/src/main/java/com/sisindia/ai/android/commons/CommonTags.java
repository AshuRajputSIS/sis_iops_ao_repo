package com.sisindia.ai.android.commons;

/**
 * Created by shankar on 3/10/16.
 */

public class CommonTags {

    public static final String API_ERROR_MESG = "No response from API";
    public static final String ADD_KIT_REQUEST = "AddKitRequest";
    public static final String ADD_KIT_REQUEST_IMAGE = "AddKitRequestImage";
    public static int FINE_AMOUNT = 100;
    public static String GOOGLE_API_KEY = "AIzaSyAQ8xn0ANLG76ePZjqXGTCZlMbHG9bGhKY";
    public static int CLIENT_HANDSHAKE_DIALOG = 2;
    public static int GUARD_CHECK_CODE = 5;
    public static int GUARD_DETAIL_CHECK_TURN_OUT_CODE = 3;
    public static String GUARD_FULL_IMAGE_CODE = "GuardImage";
    public static String GUARD_TURN_OUT_IMAGE_CODE = "GuardTurnOut";
    public static String GUARD_SIGNATURE = "GuardSignature";
    public static String GUARD_CHECK_SCREEN = "GuardCheckScreen";
    public static int Metadata_Attachment = 200;
    public static int number_11 = 11;
    public static int number_20 = 20;
    public static int number_40 = 40;
    public static int number_1000 = 1000;
    public static int number_16 = 16;

}
