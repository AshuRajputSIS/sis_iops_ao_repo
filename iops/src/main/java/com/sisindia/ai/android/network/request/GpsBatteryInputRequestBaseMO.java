package com.sisindia.ai.android.network.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GpsBatteryInputRequestBaseMO implements Serializable {
    private static final long serialVersionUID = 2299403310644278660L;
    private List<GpsBatteryInputRequestMOLocation> Location = new ArrayList<>();

    public List<GpsBatteryInputRequestMOLocation> getLocation() {
        return this.Location;
    }

    public void setLocation(List<GpsBatteryInputRequestMOLocation> Location) {
        this.Location = Location;
    }
}
