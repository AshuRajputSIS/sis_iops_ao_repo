package com.sisindia.ai.android.issues;

import com.google.gson.annotations.SerializedName;

public class FcmIssuesData {

    @SerializedName("Id")
    private Integer id;
    @SerializedName("Status")
    private Integer status;
    @SerializedName("ClosureDateTime")
    private String closureDateTime;

/**
* 
* @return
* The id
*/
public Integer getId() {
return id;
}

/**
* 
* @param id
* The Id
*/
public void setId(Integer id) {
this.id = id;
}

/**
* 
* @return
* The status
*/
public Integer getStatus() {
return status;
}

/**
* 
* @param status
* The Status
*/
public void setStatus(Integer status) {
this.status = status;
}

    public String getClosureDateTime() {
        return closureDateTime;
    }

    public void setClosureDateTime(String closureDateTime) {
        this.closureDateTime = closureDateTime;
    }
}

