package com.sisindia.ai.android.rota.billsubmission;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.sisindia.ai.android.R;
import com.sisindia.ai.android.commons.CapturePictureActivity;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.commons.DialogClickListener;
import com.sisindia.ai.android.commons.GpsTrackingService;
import com.sisindia.ai.android.commons.IOPSApplication;
import com.sisindia.ai.android.home.HomeActivity;
import com.sisindia.ai.android.network.request.AttachmentMetaDataModel;
import com.sisindia.ai.android.rota.RotaTaskStatusEnum;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillAcceptedMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionCheckListBaseMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionCheckListItemMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.BillSubmissionTaskExeMO;
import com.sisindia.ai.android.rota.billsubmission.modelObjects.LookUpCheckListItem;
import com.sisindia.ai.android.rota.daycheck.DayCheckBaseActivity;
import com.sisindia.ai.android.rota.models.RotaTaskModel;
import com.sisindia.ai.android.utils.DateTimeFormatConversionUtil;
import com.sisindia.ai.android.utils.Util;
import com.sisindia.ai.android.views.CustomFontEditText;
import com.sisindia.ai.android.views.CustomFontTextview;
import com.sisindia.ai.android.views.RoundedCornerImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Shruti Garg on 06-04-2016.
 */
public class BillSubmissionActivity extends DayCheckBaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, DialogClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.unit_name_text)
    CustomFontTextview unit_name_text;

    @Bind(R.id.bill_submission_radio_group)
    RadioGroup bill_submission_radio_group;

    @Bind(R.id.client)
    RadioButton client;

    @Bind(R.id.sisemployee)
    RadioButton sisemployee;

    @Bind(R.id.bill_submission_item_pending)
    CustomFontTextview bill_submission_item_pending;

    @Bind(R.id.edit_bill_submission_name)
    CustomFontEditText edit_bill_submission_name;

    @Bind(R.id.edit_bill_designation_name)
    CustomFontEditText edit_bill_designation_name;

    ArrayList<BillSubmissionCheckListItemMO> resultCheckedListItemMOList;
    List<BillSubmissionCheckListItemMO> uncheckListItemMOList;
    int count = 0;
    @Bind(R.id.billsubmission_parent)
    RelativeLayout billsubmissionParent;

    @Bind(R.id.photosLabel)
    CustomFontTextview photosLabel;
    @Bind(R.id.bill_submission_bill_givento)
    CustomFontTextview billSubmissionBillGivento;
    @Bind(R.id.max_image_txt)
    CustomFontTextview maxImageTxt;

    private Context mContext;
    private RotaTaskModel rotaTaskModel;
    private BillSubmissionCheckListBaseMO checkListBaseMO;
    private String taskExecutionResult = "";
    private BillSubmissionMO billSubmissionMO;
    private String billHandoverToClient = "client";
    private BillAcceptedMO billAcceptedMO;
    //    private BillingCheckListMO billingCheckListMO;
    private BillSubmissionTaskExeMO billSubmissionTaskExeMO;
    private int REQUEST_CODE = 101;
    private DateTimeFormatConversionUtil dateTimeFormatConversionUtil;
    private int BILL_SUBMISSION_IMAGE_CODE = 102;
    private LinearLayout linearLayout;
    private ImageView takePhoto;
    private RoundedCornerImageView takenImageView;
    //    private Drawable dividerDrawable;
    private ArrayList<String> mArrayImageHolder = new ArrayList<>();
    private ArrayList<LookUpCheckListItem> checkListItemList;
//    private List<String> billingCheckList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_submission);
        ButterKnife.bind(this);
        setUpToolBar();
        mContext = BillSubmissionActivity.this;
        dateTimeFormatConversionUtil = new DateTimeFormatConversionUtil();
        bill_submission_radio_group.setOnCheckedChangeListener(this);
        rotaTaskModel = (RotaTaskModel) getIntent().getSerializableExtra("RotaModel");
        checkListItemList = IOPSApplication.getInstance().getSelectionStatementDBInstance().getBillSubmissionCheckList(rotaTaskModel.getUnitId());
        unit_name_text.setText(rotaTaskModel.getUnitName());
        checkListBaseMO = createCheckListModelData();
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout_photos);
        takePhoto = (ImageView) findViewById(R.id.takePhoto);
        bill_submission_item_pending.setText(checkListItemList.size() + " items pending");

        if (util != null) {
            photosLabel.setText(util.createSpannableStringBuilder(getResources().getString(R.string.photos)));
            billSubmissionBillGivento.setText(util.createSpannableStringBuilder(getResources().getString(R.string.BILL_GIVEN_TO)));
        } else {
            Timber.d("Util :" + "Util object is null");
        }
        takePhoto.setOnClickListener(this);
        if (count == 0) {
            String macPicsCount = String.format(getResources().getString(R.string.MIN_PIC_MESG), 1);
            maxImageTxt.setText(macPicsCount);
        } else {
            String macPicsCount = String.format(getResources().getString(R.string.MAX_PIC_MESG), 1, count);
            maxImageTxt.setText(macPicsCount);
        }
    }

    //initiating toolbar
    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.BILL_SUBMISSION));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_billsubmission_items, menu);
        return true;
    }

    public void onClickNextChecklist(View v) {
        checkListBaseMO.setBillSubmissionCheckListMO(resultCheckedListItemMOList);
        Intent typeCheckingIntent = new Intent(mContext, BillChecklistActivity.class);
        typeCheckingIntent.putExtra("checklistItem", checkListBaseMO);
        startActivityForResult(typeCheckingIntent, REQUEST_CODE);
    }

    private BillSubmissionCheckListBaseMO createCheckListModelData() {
        BillSubmissionCheckListBaseMO checkListBaseMO = new BillSubmissionCheckListBaseMO();
        uncheckListItemMOList = new ArrayList<>();
        for (int i = 0; i < checkListItemList.size(); i++) {
            BillSubmissionCheckListItemMO checkListItemMO = new BillSubmissionCheckListItemMO();
            checkListItemMO.setChecked(false);
            checkListItemMO.setCheckListItem(checkListItemList.get(i).getCheckListItem());
            checkListItemMO.setCheckListItemId(checkListItemList.get(i).getCheckListItemId());
            uncheckListItemMOList.add(checkListItemMO);
        }
        if (resultCheckedListItemMOList == null) {
            resultCheckedListItemMOList = (ArrayList<BillSubmissionCheckListItemMO>) uncheckListItemMOList;
        }
        checkListBaseMO.setBillSubmissionCheckListMO(resultCheckedListItemMOList);
        return checkListBaseMO;

    }

    private void loadThumbNailImages(ArrayList<String> arrayImageHolder) {
        if (linearLayout != null) {
            linearLayout.removeAllViews();
            linearLayout.addView(takePhoto);
        }

        if (arrayImageHolder != null && arrayImageHolder.size() > 0) {
            for (int index = arrayImageHolder.size() - 1; index >= 0; index--) {
                ImageView imageView = createImageView(Uri.parse(arrayImageHolder.get(index)));
                linearLayout.addView(imageView);
            }
        }
    }

    public ImageView createImageView(Uri uri) {

        int marginBottom = Util.dpToPx(Constants.MARGIN_BOTTOM);
        int marginLeft = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);
        int marginRight = Util.dpToPx(Constants.MARGIN_LEFT_RIGHT);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        params.height = Util.dpToPx(Constants.HEIGT);
        params.width = Util.dpToPx(Constants.HEIGT);
        params.setMargins(marginLeft, Constants.ZERO, marginRight, marginBottom);

        takenImageView = new RoundedCornerImageView(this);
        takenImageView.setLayoutParams(params);
//        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.camer_taken_pic_image_border);
        takenImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        takenImageView.setImageURI(uri);
        return takenImageView;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BILL_SUBMISSION_IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            String imagePath = ((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA)).getAttachmentPath();
            setAttachmentMetaDataModel((AttachmentMetaDataModel) data.getParcelableExtra(Constants.META_DATA));
            mArrayImageHolder.add(imagePath);
            loadThumbNailImages(mArrayImageHolder);
        } else if (requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            if (mArrayImageHolder != null) {
                mArrayImageHolder.clear();
            }
            loadThumbNailImages(mArrayImageHolder);
            clearSdCardImages();
            count = 0;
            resultCheckedListItemMOList = (ArrayList<BillSubmissionCheckListItemMO>) data.getSerializableExtra("checkList");
            if (resultCheckedListItemMOList == null) {
                resultCheckedListItemMOList = (ArrayList<BillSubmissionCheckListItemMO>) uncheckListItemMOList;
            }
            for (int i = 0; i < resultCheckedListItemMOList.size(); i++) {
                if (resultCheckedListItemMOList.get(i).isChecked()) {
                    count++;
                }
            }
            if (count == 0) {
                String macPicsCount = String.format(getResources().getString(R.string.MIN_PIC_MESG), 1);
                maxImageTxt.setText(macPicsCount);
            } else {

                String macPicsCount = String.format(getResources().getString(R.string.MAX_PIC_MESG), 1, count);
                maxImageTxt.setText(macPicsCount);
            }

            int pendingItems = resultCheckedListItemMOList.size() - count;
            bill_submission_item_pending.setText(pendingItems + " items pending");

        } else if (requestCode == REQUEST_CODE && resultCode == RESULT_CANCELED) {

        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        Intent capturePictureActivity = null;
        Util.mSequenceNumber++;
        switch (viewId) {
            case R.id.takePhoto:
                if (count != 0 && count <= mArrayImageHolder.size()) {
                    String macPicsCount = String.format(getResources().getString(R.string.MAX_PIC_MESG), 1, count);
                    snackBarWithMesg(billsubmissionParent, macPicsCount);
                    return;
                } else {
                    capturePictureActivity = new Intent(BillSubmissionActivity.this, CapturePictureActivity.class);
                    capturePictureActivity.putExtra(getResources().getString(R.string.toolbar_title), "");
                    Util.setSendPhotoImageTo(Util.BILL_SUBMISSION);
                    Util.initMetaDataArray(true);
                    startActivityForResult(capturePictureActivity, BILL_SUBMISSION_IMAGE_CODE);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit) {
            if (mArrayImageHolder != null && mArrayImageHolder.size() == 0) {
                snackBarWithMesg(billsubmissionParent, getResources().getString(R.string.MSG_ADD_IMAGES));
            } else if (null == edit_bill_submission_name || edit_bill_submission_name.getText().toString().isEmpty()) {
                snackBarWithMesg(billsubmissionParent, getResources().getString(R.string.MSG));
            } else if (null == edit_bill_designation_name || edit_bill_designation_name.getText().toString().isEmpty()) {
                snackBarWithMesg(billsubmissionParent, getResources().getString(R.string.ENTER_DESIGNATION));
            } else if (count < mArrayImageHolder.size()) {
                if (count == 0) {
                    String macPicsCount = String.format(getResources().getString(R.string.MIN_PIC_MESG), 1);
                    maxImageTxt.setText(macPicsCount);
                } else {
                    String macPicsCount = String.format(getResources().getString(R.string.MAX_PIC_MESG), 1, count);
                    maxImageTxt.setText(macPicsCount);
                }
            } else {
                //billSubmissionChecklist();
                if (appPreferences.isEnableGps(BillSubmissionActivity.this)) {
                    taskExecutionResult = createTaskExecutionResult();
                    if (!taskExecutionResult.isEmpty()) {

                        Util.showProgressBar(this, R.string.saveMessage);
                        dbUpdates();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Util.dismissProgressBar(true);
                                Intent homeActivityIntent = new Intent(BillSubmissionActivity.this, HomeActivity.class);
                                homeActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(homeActivityIntent);
                                finish();
                            }
                        }, 2500);

                    } else {
                        showGpsDialog(getString(R.string.gps_configure_msg));
                    }
                }
            }
        }
        if (id == android.R.id.home) {
            showConfirmationDialog((getResources().getInteger(R.integer.Bill_Submission_CONFIRAMTION_DIALOG)));
        }

        return super.onOptionsItemSelected(item);
    }

    private void dbUpdates() {
        updateRotaTaskTable();
        insertMetaData();
        triggerSyncAdapter();

        updateMyPerformanceTable();
        insertLogActivity();

        if (rotaTaskModel.getTaskId() < 0) {
            if (IOPSApplication.getInstance().getRotaLevelSelectionInstance().getServerTaskId(rotaTaskModel.getTaskId()) > 0) {
                updateTaskRelatedTables(IOPSApplication.getInstance().getRotaLevelSelectionInstance()
                        .getServerTaskId(rotaTaskModel.getTaskId()), rotaTaskModel.getTaskId());
            }
        }

    }

    private synchronized void updateMyPerformanceTable() {
        IOPSApplication.getInstance().getMyPerformanceStatementsDB().
                UpdateMYPerformanceTable(RotaTaskStatusEnum.Completed.name(),
                        dateTimeFormatConversionUtil.getCurrentTime(), String.valueOf(rotaTaskModel.getTaskId()), rotaTaskModel.getId());
    }

    private void triggerSyncAdapter() {
        Bundle bundel = new Bundle();
        IOPSApplication.getInstance().getSyncAdapterInstance().startForceSyncing(bundel);
    }

    private synchronized void insertMetaData() {
        IOPSApplication.getInstance().getInsertionInstance().insertMetaDataTable(attachmentMetaArrayList);
        attachmentMetaArrayList.clear();
    }

    private synchronized void updateRotaTaskTable() {
        String geoLocation = GpsTrackingService.latitude + "," + GpsTrackingService.longitude;
        IOPSApplication.getInstance().getRotaLevelUpdateDBInstance().updateRotaTask(rotaTaskModel.getTaskId(),
                taskExecutionResult, RotaTaskStatusEnum.Completed.getValue(),
                dateTimeFormatConversionUtil.getCurrentDateTime(),
                rotaTaskModel.getRemarks(),
                geoLocation
        );
    }

    private synchronized String createTaskExecutionResult() {
        String geoLocation = GpsTrackingService.latitude + " ," + GpsTrackingService.longitude;

        if (resultCheckedListItemMOList != null && resultCheckedListItemMOList.size() == checkListItemList.size()) {
            billSubmissionTaskExeMO = new BillSubmissionTaskExeMO();
            billSubmissionTaskExeMO.setGeoLocation(geoLocation);
            billSubmissionMO = new BillSubmissionMO();
            billSubmissionMO.setBillHandOverTO(billHandoverToClient);
            billAcceptedMO = new BillAcceptedMO();
            billAcceptedMO.setName(edit_bill_submission_name.getText().toString());
            billAcceptedMO.setDesignation(edit_bill_designation_name.getText().toString());
            billSubmissionMO.setBillAccepted(billAcceptedMO);
            billSubmissionMO.setBillingCheckList(billSubmissionChecklist());
            billSubmissionTaskExeMO.setBillSubmission(billSubmissionMO);
            if (null != billSubmissionTaskExeMO) {
                taskExecutionResult = IOPSApplication.getGsonInstance().toJson(billSubmissionTaskExeMO);
            } else {
                taskExecutionResult = "";
            }
        }
        return taskExecutionResult;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (group.getId()) {
            case R.id.bill_submission_radio_group:
                if (checkedId == R.id.client) {
                    billHandoverToClient = String.valueOf(client.getText().toString());

                } else if (checkedId == R.id.sisemployee) {
                    billHandoverToClient = String.valueOf(sisemployee.getText().toString());
                }
                break;
        }
    }

    @Override
    public void onPositiveButtonClick(int clickType) {
        clearSdCardImages();
        finish();
    }

    @Override
    public void onBackPressed() {
        showConfirmationDialog((getResources().getInteger(R.integer.Bill_Submission_CONFIRAMTION_DIALOG)));
    }

    @Override
    public void finish() {
        super.finish();
        if (attachmentMetaArrayList != null) {
            attachmentMetaArrayList.clear();
        }
    }

    private void clearSdCardImages() {
        if (attachmentMetaArrayList != null && attachmentMetaArrayList.size() != 0) {
            for (AttachmentMetaDataModel attachmentMetaDataModel : attachmentMetaArrayList) {
                if (attachmentMetaDataModel != null) {
                    File file = new File(attachmentMetaDataModel.getAttachmentPath());
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                }
            }
            attachmentMetaArrayList.clear();
        }
    }

    private String billSubmissionChecklist() {
        ArrayList<String> values = new ArrayList<>();
        for (int i = 0; i < resultCheckedListItemMOList.size(); i++) {
            values.add(resultCheckedListItemMOList.get(i).getCheckListItem() + " : " + resultCheckedListItemMOList.get(i).isChecked());
        }
        String resultList = values.toString();
        String resultStr = resultList.substring(1, resultList.length() - 1).replace(", ", ",");
        Timber.d("csv == %s", resultStr);
        return resultStr;
    }
}
