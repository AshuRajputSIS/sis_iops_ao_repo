package com.sisindia.ai.android.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;

import java.lang.reflect.Field;

import timber.log.Timber;


/**
 * Created by shankar on 18/5/16.
 */
public class DeletionStatementDB extends SISAITrackingDB {
    public DeletionStatementDB(Context context) {
        super(context);
    }

    /**
     * @param tableName
     */
    public synchronized void deleteTable(String tableName) {
        SQLiteDatabase sqlite = null;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }
        }
    }

    /**
     * @param tableName   --> Name of the table from where you want to delete the records
     * @param recordCount --> Number of records you want to delete from table
     */

    public synchronized void deleteRecordFromTable(String tableName, String primaryKey, int recordCount) {
        String deleteQuery = "DELETE from " + tableName +
                " WHERE " + primaryKey +
                " IN (SELECT " + primaryKey +
                " from " + tableName +
                " order by " + primaryKey +
                " limit " + recordCount + ")";

        Timber.d("deleteRecordFromTable Query : %s", deleteQuery);

        try (SQLiteDatabase sqlite = this.getWritableDatabase()) {
            sqlite.execSQL(deleteQuery);
        } catch (Exception e) {
            Timber.e(e, "Error while deleting records : ");
        }
    }

    public void deleteTable(String tableName, SQLiteDatabase sqlite) {
        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }


    public void deleteBySpecificId(String tableName, String columnName, int id, SQLiteDatabase sqlite) {
        String whereClause = columnName + " = " + id;
        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, whereClause, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }

    public void deleteTableById(String tableName, int id, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            try {
                if (TableNameAndColumnStatement.TASK_TABLE.equalsIgnoreCase(tableName)) {
                    sqlite.delete(tableName, TableNameAndColumnStatement.ID + " = ? and " + TableNameAndColumnStatement.
                            TASK_STATUS_ID + " not in(?,?)", new String[]{String.valueOf(id), String.valueOf(3), String.valueOf(4)});
                } else {
                    if (TableNameAndColumnStatement.UNIT_TABLE.equalsIgnoreCase(tableName)) {
                        sqlite.delete(tableName, TableNameAndColumnStatement.UNIT_STATUS + " in (1,2,0)  and "
                                + TableNameAndColumnStatement.ID + " = " + id, null);
                    } else {
                        sqlite.delete(tableName, TableNameAndColumnStatement.ID + " = ? ", new String[]{String.valueOf(id)});
                    }
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }

    public int deleteTableByIdStatus(String tableName, int id, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            try {
                if (TableNameAndColumnStatement.TASK_TABLE.equalsIgnoreCase(tableName)) {
                    return sqlite.delete(tableName,
                            TableNameAndColumnStatement.ID + " = ? and " + TableNameAndColumnStatement.
                                    TASK_STATUS_ID + " not in(?,?)",
                            new String[]{String.valueOf(id), String.valueOf(3), String.valueOf(4)});
                } else {
                    return sqlite.delete(tableName,
                            TableNameAndColumnStatement.ID + " = ? ",
                            new String[]{String.valueOf(id)});
                }

            } catch (Exception exception) {
                Crashlytics.logException(exception);
                return -1;
            }
        }
    }

    public void deleteRotaTaskTableById(String tableName, int id, SQLiteDatabase sqlite) {
        String deleteQuery = " delete from rota_task where rota_id = " + id + " and  rota_task_id in (select rt.rota_task_id  " +
                " from rota_task rt join task t on rt.task_id = t.task_id where t.task_status_id not in (3,4))";
        Timber.d("deleteRotaTaskTableById Query : %s", deleteQuery);
        try {
            sqlite.execSQL(deleteQuery);
        } catch (Exception e) {
            Timber.e(e, "Error while deleting records : ");
        }
    }

    public void deletePreviousWeekRotaTaskExceptBSAndMI(SQLiteDatabase sqlite) {
        String deleteQuery = "delete from rota_task where id in (select RT.Id from Rota_Task RT inner join Task TSK on RT.Task_ID=TSK.Task_Id where " +
                "TSK.Task_Type_Id not in (5) or (TSK.Other_Task_Reason_Id !=10 and TSK.Task_Type_Id not in (7)) )";

        Timber.d("DeletePreviousWeekRotaTaskExceptBSAndMI Query : %s", deleteQuery);
        try {
            sqlite.execSQL(deleteQuery);
        } catch (Exception e) {
            Timber.e(e, "Error while deleting records : ");
        }
    }

    public void deleteRecordById(int unitId, String columnName, String tableName, SQLiteDatabase sqlite) {
        synchronized (sqlite) {
            try {
                int count = sqlite.delete(tableName, columnName + "=?", new String[]{String.valueOf(unitId)});
                Timber.d("deleteRecordByUnitID Count%s", count);

            } catch (Exception exception) {
                Crashlytics.logException(exception);
            }
        }
    }

    public boolean IsTableDeleted(String tableName, SQLiteDatabase sqlite) {
        boolean isDeleted = true;

        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, null, null);
            }
        } catch (Exception exception) {
            isDeleted = false;
            Crashlytics.logException(exception);
        }
        return isDeleted;
    }


    public boolean isUnitTableDeleted(String tableName, SQLiteDatabase sqlite) {
        boolean isDeleted = true;
        String whereClause = TableNameAndColumnStatement.UNIT_STATUS + " in (1,2,0)";
        try {
            synchronized (sqlite) {
                sqlite.delete(tableName, whereClause, null);
            }
        } catch (Exception exception) {
            isDeleted = false;
            Crashlytics.logException(exception);
        }
        return isDeleted;
    }


    public boolean deleteSpecifiedkTable(String tableName, int issueTypeId) {

        SQLiteDatabase sqlite = null;
        boolean isSuccessfullyDeleted = false;
        try {
            sqlite = this.getWritableDatabase();
            synchronized (sqlite) {
                if (issueTypeId == 0) {
                    sqlite.delete(tableName, null, null);
                    isSuccessfullyDeleted = true;
                } else if (issueTypeId == 3) {
                    sqlite.delete(tableName, TableNameAndColumnStatement.ISSUE_TYPE_ID + " = " + issueTypeId, null);
                    isSuccessfullyDeleted = true;
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
            isSuccessfullyDeleted = false;
        } finally {
            if (sqlite != null && sqlite.isOpen()) {
                sqlite.close();
            }

        }

        return isSuccessfullyDeleted;
    }


    public void dropTable() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String query = " select * from  sqlite_master where type like 'table'  and name  not like" +
                " 'sqlite_sequence' and name not like 'android_metadata' ";
        try {
            Cursor cursor = sqLiteDatabase.rawQuery(query, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Timber.d("Drop Table: " + cursor.getString(cursor.getColumnIndex("name")));
                        sqLiteDatabase.execSQL(" drop table if exists " + cursor
                                .getString(cursor.getColumnIndex("name")));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (sqLiteDatabase != null &&
                    sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }

    }


    public boolean deleteTableData() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String query = " select * from  sqlite_master where type like 'table'  and name  not like" +
                " 'sqlite_sequence' and name not like 'android_metadata' ";
        try {
            Cursor cursor = sqLiteDatabase.rawQuery(query, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Timber.d(" Delete Table: " + cursor.getString(cursor.getColumnIndex("name")));
                        sqLiteDatabase.execSQL("delete from " + cursor
                                .getString(cursor.getColumnIndex("name")));
                    } while (cursor.moveToNext());
                    return true;
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        } finally {
            if (sqLiteDatabase != null &&
                    sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
        return false;
    }
}
