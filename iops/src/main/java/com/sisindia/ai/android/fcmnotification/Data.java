package com.sisindia.ai.android.fcmnotification;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("GuardsForBranch")
    @Expose
    public List<GuardsForBranch> guardsForBranch = null;

}

