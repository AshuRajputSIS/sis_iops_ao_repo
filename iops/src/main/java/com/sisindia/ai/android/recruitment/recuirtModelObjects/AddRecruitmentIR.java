package com.sisindia.ai.android.recruitment.recuirtModelObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/10/16.
 */

public class AddRecruitmentIR {

    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("IsSelected")
    @Expose
    private Integer isSelected;
    @SerializedName("SourceTaskId")
    @Expose
    private Integer sourceTaskId;
    @SerializedName("SelectedEmployeeId")
    @Expose
    private Integer selectedEmployeeId;
    @SerializedName("IsRejected")
    @Expose
    private Integer isRejected;
    @SerializedName("ApplicationFormNo")
    @Expose
    private Integer applicationFormNo;
    @SerializedName("ActionTakenOn")
    @Expose
    private Integer actionTakenOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getRecruit_id() {
        return recruit_id;
    }

    public void setRecruit_id(Integer recruit_id) {
        this.recruit_id = recruit_id;
    }

    private Integer recruit_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;
    /**
     *
     * @return
     * The fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     *
     * @param fullName
     * The FullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     *
     * @return
     * The dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     *
     * @param dateOfBirth
     * The DateOfBirth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     *
     * @return
     * The contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     *
     * @param contactNo
     * The ContactNo
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     *
     * @return
     * The isSelected
     */
    public Integer getIsSelected() {
        return isSelected;
    }

    /**
     *
     * @param isSelected
     * The IsSelected
     */
    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    /**
     *
     * @return
     * The sourceTaskId
     */
    public Integer getSourceTaskId() {
        return sourceTaskId;
    }

    /**
     *
     * @param sourceTaskId
     * The SourceTaskId
     */
    public void setSourceTaskId(Integer sourceTaskId) {
        this.sourceTaskId = sourceTaskId;
    }

    /**
     *
     * @return
     * The selectedEmployeeId
     */
    public Integer getSelectedEmployeeId() {
        return selectedEmployeeId;
    }

    /**
     *
     * @param selectedEmployeeId
     * The SelectedEmployeeId
     */
    public void setSelectedEmployeeId(Integer selectedEmployeeId) {
        this.selectedEmployeeId = selectedEmployeeId;
    }

    /**
     *
     * @return
     * The isRejected
     */
    public Integer getIsRejected() {
        return isRejected;
    }

    /**
     *
     * @param isRejected
     * The IsRejected
     */
    public void setIsRejected(Integer isRejected) {
        this.isRejected = isRejected;
    }

    /**
     *
     * @return
     * The applicationFormNo
     */
    public Integer getApplicationFormNo() {
        return applicationFormNo;
    }

    /**
     *
     * @param applicationFormNo
     * The ApplicationFormNo
     */
    public void setApplicationFormNo(Integer applicationFormNo) {
        this.applicationFormNo = applicationFormNo;
    }

    /**
     *
     * @return
     * The actionTakenOn
     */
    public Integer getActionTakenOn() {
        return actionTakenOn;
    }

    /**
     *
     * @param actionTakenOn
     * The ActionTakenOn
     */
    public void setActionTakenOn(Integer actionTakenOn) {
        this.actionTakenOn = actionTakenOn;
    }

    @Override
    public String toString() {
        return "AddRecruitmentIR{" +
                "fullName='" + fullName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", contactNo='" + contactNo + '\'' +
                ", isSelected=" + isSelected +
                ", sourceTaskId=" + sourceTaskId +
                ", selectedEmployeeId=" + selectedEmployeeId +
                ", isRejected=" + isRejected +
                ", applicationFormNo=" + applicationFormNo +
                ", actionTakenOn=" + actionTakenOn +
                ", id=" + id +
                '}';
    }
}
