package com.sisindia.ai.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.crashlytics.android.Crashlytics;
import com.sisindia.ai.android.commons.Constants;
import com.sisindia.ai.android.syncadpter.DependentSyncsStatus;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by compass on 5/13/2017.
 */

public class DependentSyncDb extends SISAITrackingDB {

    private Context context;

    public DependentSyncDb(Context context) {
        super(context);
        this.context = context;
    }

    public List<DependentSyncsStatus> getDependentTasksSyncStatus() {
        List<DependentSyncsStatus> dependentSyncsStatusList = new ArrayList<>();
        String query = " select  *  from " +
                TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED +
                " = 0  ";
        SQLiteDatabase sqlite = null;
        Timber.d("DependentSync Log%s", "DependentSyncWithout knowing taskId");
        Timber.d("DependentSync%s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
                        dependentSyncsStatus.taskId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID));
                        dependentSyncsStatus.taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                        dependentSyncsStatus.taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        dependentSyncsStatus.unitPostId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID));
                        dependentSyncsStatus.issueId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID));
                        dependentSyncsStatus.kitDistributionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID));
                        dependentSyncsStatus.isUnitStrengthSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED));
                        dependentSyncsStatus.isIssuesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED));
                        dependentSyncsStatus.isImprovementPlanSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED));
                        dependentSyncsStatus.isKitRequestSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED));
                        dependentSyncsStatus.isMetaDataSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED));
                        dependentSyncsStatus.isImagesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED));
                        dependentSyncsStatus.isUnitEquipmentSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED));
                        dependentSyncsStatus.unitRaisingId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RAISING_ID));
                        dependentSyncsStatus.unitRaisingDeferredId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID));
                        dependentSyncsStatus.unitRaisingCancellationId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID));
                        dependentSyncsStatus.isUnitRaisingStrengthSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED));
                        dependentSyncsStatus.barrackId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.BARRACK_ID));
                        dependentSyncsStatusList.add(dependentSyncsStatus);
                        //updateDependentSyncState(dependentSyncsStatus.taskId,1,sqlite);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dependentSyncsStatusList;

    }


    public List<DependentSyncsStatus> getDependentTasksSyncStatusWithTaskId(int taskId) {
        List<DependentSyncsStatus> dependentSyncsStatusList = new ArrayList<>();
        String query = " select  *  from " +
                TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED +
                " = 0  and " + TableNameAndColumnStatement.TASK_ID + " = " + taskId;
        //" and "+TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED + " =  0";
        SQLiteDatabase sqlite = null;
        Timber.d("DependentSync Log%s", "DependentSyncWith knowing taskid----" + taskId);
        Timber.d(" GetDependentTasksSyncStatusWithTaskId %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
                        dependentSyncsStatus.taskId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID));
                        dependentSyncsStatus.taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                        dependentSyncsStatus.taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        dependentSyncsStatus.unitPostId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID));
                        dependentSyncsStatus.issueId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID));
                        dependentSyncsStatus.kitDistributionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID));
                        dependentSyncsStatus.isUnitStrengthSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED));
                        dependentSyncsStatus.isIssuesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED));
                        dependentSyncsStatus.isImprovementPlanSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED));
                        dependentSyncsStatus.isKitRequestSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED));
                        dependentSyncsStatus.isMetaDataSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED));
                        dependentSyncsStatus.isImagesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED));
                        dependentSyncsStatus.isUnitEquipmentSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED));
                        //updateDependentSyncState(dependentSyncsStatus.taskId,1,sqlite);
                        dependentSyncsStatusList.add(dependentSyncsStatus);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dependentSyncsStatusList;

    }


    public List<DependentSyncsStatus> getDependentTasksSyncStatusWithId(int id, String columnName) {
        List<DependentSyncsStatus> dependentSyncsStatusList = new ArrayList<>();
        String query = " select  *  from " +
                TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED +
                " = 0  and " + columnName + " = " + id;
        //" and "+TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED + " =  0";
        SQLiteDatabase sqlite = null;
        Timber.d("DependentSync Log%s", "DependentSyncWith knowing taskid----" + id);
        Timber.d(" getDependentTasksSyncStatusWithId %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        DependentSyncsStatus dependentSyncsStatus = new DependentSyncsStatus();
                        dependentSyncsStatus.taskId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_ID));
                        dependentSyncsStatus.taskStatusId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_STATUS_ID));
                        dependentSyncsStatus.taskTypeId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.TASK_TYPE_ID));
                        dependentSyncsStatus.unitPostId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.UNIT_POST_ID));
                        dependentSyncsStatus.issueId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.ISSUE_ID));
                        dependentSyncsStatus.kitDistributionId = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID));
                        dependentSyncsStatus.isUnitStrengthSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED));
                        dependentSyncsStatus.isIssuesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED));
                        dependentSyncsStatus.isImprovementPlanSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED));
                        dependentSyncsStatus.isKitRequestSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED));
                        dependentSyncsStatus.isMetaDataSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED));
                        dependentSyncsStatus.isImagesSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED));
                        dependentSyncsStatus.isUnitEquipmentSynced = cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED));
                        //updateDependentSyncState(dependentSyncsStatus.taskId,1,sqlite);
                        dependentSyncsStatusList.add(dependentSyncsStatus);
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return dependentSyncsStatusList;

    }

    public void insertDependentTaskSyncDetails(DependentSyncsStatus dependentSyncsStatus, int isCreatedOrEdited) {

        SQLiteDatabase sqlite = this.getWritableDatabase();
        sqlite.beginTransaction();
        synchronized (sqlite) {
            try {
                // Inserting Row
                if (dependentSyncsStatus != null) {
                    ContentValues values = new ContentValues();
                    values.put(TableNameAndColumnStatement.TASK_ID, dependentSyncsStatus.taskId);
                    values.put(TableNameAndColumnStatement.ISSUE_ID, dependentSyncsStatus.issueId);
                    values.put(TableNameAndColumnStatement.UNIT_POST_ID, dependentSyncsStatus.unitPostId);
                    values.put(TableNameAndColumnStatement.KIT_DISTRIBUTION_ID, dependentSyncsStatus.kitDistributionId);
                    values.put(TableNameAndColumnStatement.TASK_TYPE_ID, dependentSyncsStatus.taskTypeId);
                    values.put(TableNameAndColumnStatement.TASK_STATUS_ID, dependentSyncsStatus.taskStatusId);
                    values.put(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED, dependentSyncsStatus.isUnitStrengthSynced);
                    values.put(TableNameAndColumnStatement.IS_ISSUES_SYNCED, dependentSyncsStatus.isIssuesSynced);
                    values.put(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED, dependentSyncsStatus.isImprovementPlanSynced);
                    values.put(TableNameAndColumnStatement.IS_METADATA_SYNCED, dependentSyncsStatus.isMetaDataSynced);
                    values.put(TableNameAndColumnStatement.IS_IMAGES_SYNCED, dependentSyncsStatus.isImagesSynced);
                    values.put(TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED, dependentSyncsStatus.isKitRequestSynced);
                    values.put(TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED, dependentSyncsStatus.isUnitEquipmentSynced);
                    values.put(TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED, dependentSyncsStatus.isAllDepemdentModilesSynced);
                    values.put(TableNameAndColumnStatement.UNIT_RAISING_ID, dependentSyncsStatus.unitRaisingId);
                    values.put(TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID, dependentSyncsStatus.unitRaisingDeferredId);
                    values.put(TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID, dependentSyncsStatus.unitRaisingCancellationId);
                    values.put(TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED, dependentSyncsStatus.isUnitRaisingStrengthSynced);
                    values.put(TableNameAndColumnStatement.BARRACK_ID, dependentSyncsStatus.barrackId);
                    values.put(TableNameAndColumnStatement.CREATED_OR_EDITED, isCreatedOrEdited);
                    //values.put(TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED,0);
                    sqlite.insert(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW, null, values);

                }

                sqlite.setTransactionSuccessful();
            } catch (Exception exception) {
                Crashlytics.logException(exception);
            } finally {
                sqlite.endTransaction();
                if (sqlite != null && sqlite.isOpen()) {
                    sqlite.close();
                }
            }
        }

    }


    public void updateDependentSyncDetails(int id, int syncTYpe, String columnName, int count, int taskTypeId) {
        String whereClause = "";
        if (syncTYpe == Constants.TASK_RELATED_SYNC) {

            whereClause = TableNameAndColumnStatement.TASK_ID + " = " + id;
        } else if (syncTYpe == Constants.ISSUE_RELATED_SYNC) {
            whereClause = TableNameAndColumnStatement.ISSUE_ID + " = " + id;
        } else if (syncTYpe == Constants.KIT_DISTRIBUTION_RELATED_SYNC) {
            whereClause = TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " = " + id;
        } else if (syncTYpe == Constants.UNIT_POST_RELATED_SYNC) {
            whereClause = TableNameAndColumnStatement.UNIT_POST_ID + " = " + id;
        } else if (syncTYpe == Constants.UNIT_RAISING_SYNC) {
            whereClause = TableNameAndColumnStatement.UNIT_RAISING_ID + " = " + id;
        } else if (syncTYpe == Constants.UNIT_RAISING_CANCELLATION_SYNC) {
            whereClause = TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " = " + id;
        } else if (syncTYpe == Constants.UNIT_RAISING_DEFERRED_SYNC) {
            whereClause = TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + " = " + id;
        } else if (syncTYpe == Constants.BARRACK_POST_SYNC) {
            whereClause = TableNameAndColumnStatement.BARRACK_ID + " = " + id;
        }

        SQLiteDatabase sqlite = null;

        try {
            sqlite = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(columnName, count);
            sqlite.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW,
                    contentValues, whereClause, null);

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
            updateAllDependentModuleSynced(id, syncTYpe, whereClause, taskTypeId);
        }
    }

    private void updateAllDependentModuleSynced(int id, int syncTYpe, String whereClause, int taskTypeId) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        try {
            if (getDependentSyncCount(id, syncTYpe, taskTypeId)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED, 1);
                //contentValues.put(TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED,0);
                sqLiteDatabase.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW,
                        contentValues, whereClause, null);
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
    }

    private boolean getDependentSyncCount(int id, int syncType, int taskTypeId) {
        SQLiteDatabase sqlite = this.getWritableDatabase();
        String columnName = "";
        int count = 0;
        boolean isAllDependentModuleSynced = false;
        if (syncType == Constants.TASK_RELATED_SYNC) {

            columnName = TableNameAndColumnStatement.TASK_ID + " = " + id;
        } else if (syncType == Constants.ISSUE_RELATED_SYNC) {
            columnName = TableNameAndColumnStatement.ISSUE_ID + " = " + id;
        } else if (syncType == Constants.KIT_DISTRIBUTION_RELATED_SYNC) {
            columnName = TableNameAndColumnStatement.KIT_DISTRIBUTION_ID + " = " + id;
        } else if (syncType == Constants.UNIT_POST_RELATED_SYNC) {
            columnName = TableNameAndColumnStatement.UNIT_POST_ID + " = " + id;
        } else if (syncType == Constants.UNIT_RAISING_SYNC) {
            columnName = TableNameAndColumnStatement.UNIT_RAISING_ID + " = " + id;
        } else if (syncType == Constants.UNIT_RAISING_CANCELLATION_SYNC) {
            columnName = TableNameAndColumnStatement.UNIT_RAISING_CANCELLATION_ID + " = " + id;
        } else if (syncType == Constants.UNIT_RAISING_DEFERRED_SYNC) {
            columnName = TableNameAndColumnStatement.UNIT_RAISING_DEFERRED_ID + " = " + id;
        } else if (syncType == Constants.BARRACK_POST_SYNC) {
            columnName = TableNameAndColumnStatement.BARRACK_ID + " = " + id;
        }
        String query = " select  * from " +
                TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + columnName;
        Timber.d(" GetDependentSyncCount %s", query);
        try {

            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {

                        if (syncType == Constants.TASK_RELATED_SYNC) {

                            if (taskTypeId == 1 || taskTypeId == 2) {
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_KITREQUESTED_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (count == 5) {
                                    isAllDependentModuleSynced = true;
                                }
                            } else if (taskTypeId == 3) {

                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNITSTENGTH_SYNCED)) == 0) {
                                    count = count + 1;
                                }

                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (count == 5) {
                                    isAllDependentModuleSynced = true;
                                }
                            } else if (taskTypeId == 4) {

                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMPROVEMNTPLANS_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_ISSUES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (count == 4) {
                                    isAllDependentModuleSynced = true;
                                }
                            } else if (taskTypeId == 5 || taskTypeId == 6 || taskTypeId == 7) {

                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                    count = count + 1;
                                }
                                if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                    count = count + 1;
                                }

                                if (count == 2) {
                                    isAllDependentModuleSynced = true;
                                }
                            }

                        } else if (syncType == Constants.UNIT_POST_RELATED_SYNC) {
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_EQUIPMENT_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (count == 3) {
                                isAllDependentModuleSynced = true;
                            }
                        } else if (syncType == Constants.ISSUE_RELATED_SYNC ||
                                syncType == Constants.KIT_DISTRIBUTION_RELATED_SYNC) {
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (count == 2) {
                                isAllDependentModuleSynced = true;
                            }
                        } else if (syncType == Constants.UNIT_RAISING_SYNC) {
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_UNIT_RAISING_STRENGTH_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (count == 3) {
                                isAllDependentModuleSynced = true;
                            }
                        } else if (syncType == Constants.UNIT_RAISING_CANCELLATION_SYNC ||
                                syncType == Constants.UNIT_RAISING_DEFERRED_SYNC ||
                                syncType == Constants.BARRACK_POST_SYNC) {
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_METADATA_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (cursor.getInt(cursor.getColumnIndex(TableNameAndColumnStatement.IS_IMAGES_SYNCED)) == 0) {
                                count = count + 1;
                            }
                            if (count == 2) {
                                isAllDependentModuleSynced = true;
                            }
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
        return isAllDependentModuleSynced;
    }


    public int getSyncCount(int id, String columnName, String requiredColumnName) {


        SQLiteDatabase sqlite = this.getWritableDatabase();
        int count = 0;
        String query = " select  " + requiredColumnName + "  from " +
                TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + columnName +
                " =   " + id;
        Timber.d("GetSyncCount %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        count = cursor.getInt(cursor.getColumnIndex(requiredColumnName));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return count;
    }


    public boolean isTaskAlreadyExist(int id, String columName, boolean isNewlyCreated) {

        boolean istaskAlreadySynced = false;
        String query = "";
        if (isNewlyCreated) {
            query = " select  " + columName + "  from " +
                    TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                    " where " + columName +
                    " =   " + id + " and " + TableNameAndColumnStatement.CREATED_OR_EDITED + " = " + 1;
        } else {
            query = " select  " + columName + "  from " +
                    TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                    " where " + columName +
                    " =   " + id + " and (" + TableNameAndColumnStatement.CREATED_OR_EDITED + " in(2,4))";
        }
        SQLiteDatabase sqlite = this.getWritableDatabase();
        Timber.d("isTaskAlreadyExist %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                istaskAlreadySynced = true;
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return istaskAlreadySynced;

    }


    public void updateDependentTaskSyncDetails(int issueId, int issueStatus) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.IS_ALL_DEPENDENT_MODULES_SYNCED, 0);
            contentValues.put(TableNameAndColumnStatement.IS_METADATA_SYNCED, -1);
            contentValues.put(TableNameAndColumnStatement.IS_IMAGES_SYNCED, -1);
            contentValues.put(TableNameAndColumnStatement.CREATED_OR_EDITED, issueStatus);
            sqLiteDatabase.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW,
                    contentValues, TableNameAndColumnStatement.ISSUE_ID + " = " + issueId, null);

        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (sqLiteDatabase != null && sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
    }

    public boolean isSpecificColumnSynced(int id, String columnName, String requiredColumnName) {
        boolean isMetaDataSynced = false;
        SQLiteDatabase sqlite = null;
        String query = " select " + requiredColumnName +
                " from " + TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW +
                " where " + columnName + " = " + id;
        Timber.d(" IsSpecificColumnSynced %s", query);
        try {
            sqlite = this.getWritableDatabase();
            Cursor cursor = sqlite.rawQuery(query, null);
            if (cursor != null && cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    if (cursor.getInt(cursor.getColumnIndex(requiredColumnName)) == 0) {
                        isMetaDataSynced = true;
                    }
                }
            }
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        } finally {
            if (null != sqlite && sqlite.isOpen()) {
                sqlite.close();
            }
        }
        return isMetaDataSynced;
    }


    public void updateDependentSyncState(int taskId, int syncStatus, SQLiteDatabase sqlite) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TableNameAndColumnStatement.IS_SYNC_REQUESTS_STARTED, syncStatus);
            sqlite.update(TableNameAndColumnStatement.DEPENDENT_SYNC_FLOW,
                    contentValues, TableNameAndColumnStatement.TASK_ID + " = " + taskId, null);
        } catch (Exception exception) {
            Crashlytics.logException(exception);
        }
    }
}
